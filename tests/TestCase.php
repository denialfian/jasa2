<?php

namespace Tests;

use App\Services\UserService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Role;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
        Artisan::call('db:seed --class=PermissionSeed');
        Artisan::call('passport:install --force');
    }

    public $token = null;
    public $user = null;

    public function getToken()
    {
        if ($this->token == null) {
            // create user
            $user = (new UserService)->dontSendNotif()->create([
                'name' => $this->faker->name,
                'email' => $this->faker->email,
                'role_id' => Role::first()->id,
                'password' => 'password',
            ]);

            // generate token
            $token = $user->createToken('JasaWeb')->accessToken;
            $this->token = $token;
            $this->user = $user;
            return $token;
        }

        return $this->token;
    }

    public function getHeaderReq()
    {
        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'X-Requested-With' => 'XMLHttpRequest',
            'Authorization' => "Bearer " . $this->getToken(),
        ];
    }
}
