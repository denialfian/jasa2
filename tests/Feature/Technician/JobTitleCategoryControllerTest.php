<?php

namespace Tests\Feature\Technician;

use App\Model\Technician\JobTitleCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class JobTitleCategoryControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'name', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'name' => $this->faker->name,
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(JobTitleCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('job_title_categories.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(JobTitleCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('job_title_categories.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(JobTitleCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('job_title_categories.select2.api'))
            ->assertStatus(200);
    }

    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('job_title_categories.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('job_title_categories.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $category = factory(JobTitleCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('job_title_categories.store.api'), $this->getPlayload(['name' => $category->name]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $category = factory(JobTitleCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('job_title_categories.show.api', ['category' => $category->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('job_title_categories.show.api', ['category' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $category = factory(JobTitleCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('job_title_categories.update.api', ['category' => $category->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('job_title_categories.update.api', ['category' => 'test']), $this->getPlayload())
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_sukses()
    {
        $category = factory(JobTitleCategory::class)->create();

        $playload = $this->getPlayload([
            'id' => $category->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('job_title_categories.update.api', ['category' => $category->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_fail()
    {
        $category = factory(JobTitleCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json(
                'PUT',
                route('job_title_categories.update.api', ['category' => $category->id]),
                $this->getPlayload([
                    'name' => $category->name
                ])
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name()
    {
        $category = factory(JobTitleCategory::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('job_title_categories.update.api', ['category' => $category->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_update_uniq_name()
    {
        $category = factory(JobTitleCategory::class)->create();

        $playload = $this->getPlayload([
            'name' => $category->name
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('job_title_categories.update.api', ['category' => $category->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    # test delete
    # 
    # ==============

    /** @test */
    public function test_destroy()
    {
        $category = factory(JobTitleCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('job_title_categories.destroy.api', ['category' => $category->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('job_title_categories.destroy.api', ['category' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
