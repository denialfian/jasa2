<?php

namespace Tests\Feature\Technician;

use App\Model\Technician\JobExperience;
use App\Model\Technician\JobTitle;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class JobExperienceControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return [
            'id',
            'company_name',
            'position',
            'period_start',
            'period_end',
            'type',
            'user_id',
            'job_title_id',
            'created_at',
            'updated_at'
        ];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'position' => $this->faker->name,
            'company_name' => $this->faker->name,
            'period_start' => date('Y-m-d'),
            'period_end' => date('Y-m-d'),
            'type' => 1,
            'user_id' => factory(User::class)->create()->id,
            'job_title_id' => factory(JobTitle::class)->create()->id,
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(JobExperience::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('job_experiences.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(JobExperience::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('job_experiences.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(JobExperience::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('job_experiences.select2.api'))
            ->assertStatus(200);
    }

    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('job_experiences.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('job_experiences.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'company_name',
                ]
            ]);
    }

    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $experience = factory(JobExperience::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('job_experiences.update.api', ['experience' => $experience->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('job_experiences.update.api', ['experience' => 'test']), $this->getPlayload())
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required()
    {
        $experience = factory(JobExperience::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('job_experiences.update.api', ['experience' => $experience->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'company_name',
                ]
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $experience = factory(JobExperience::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('job_experiences.show.api', ['experience' => $experience->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('job_experiences.show.api', ['experience' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    # test delete
    # 
    # ==============

    /** @test */
    public function test_destroy()
    {
        $experience = factory(JobExperience::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('job_experiences.destroy.api', ['experience' => $experience->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('job_experiences.destroy.api', ['experience' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
