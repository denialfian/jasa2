<?php

namespace Tests\Feature\Product;

use App\Model\Product\ProductBrand;
use App\Model\Product\ProductPartCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductBrandControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'name', 'code', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'name' => $this->faker->name,
            'code' => $this->faker->name,
            'product_part_category_id' => factory(ProductPartCategory::class)->create()->id
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(ProductBrand::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('product_brand.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(ProductBrand::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('product_brand.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(ProductBrand::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('product_brand.select2.api'))
            ->assertStatus(200);
    }

    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('product_brand.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('product_brand.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $brand = factory(ProductBrand::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('product_brand.store.api'), $this->getPlayload(['name' => $brand->name]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $brand = factory(ProductBrand::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('product_brand.show.api', ['brand' => $brand->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('product_brand.show.api', ['brand' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $brand = factory(ProductBrand::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_brand.update.api', ['brand' => $brand->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_brand.update.api', ['brand' => 'test']), $this->getPlayload())
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_sukses()
    {
        $brand = factory(ProductBrand::class)->create();

        $playload = $this->getPlayload([
            'id' => $brand->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_brand.update.api', ['brand' => $brand->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_fail()
    {
        $brand = factory(ProductBrand::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json(
                'PUT',
                route('product_brand.update.api', ['brand' => $brand->id]),
                $this->getPlayload([
                    'name' => $brand->name
                ])
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name()
    {
        $brand = factory(ProductBrand::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_brand.update.api', ['brand' => $brand->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_update_uniq_name()
    {
        $brand = factory(ProductBrand::class)->create();

        $playload = $this->getPlayload([
            'name' => $brand->name
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_brand.update.api', ['brand' => $brand->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    # test delete
    # 
    # ==============

    /** @test */
    public function test_destroy()
    {
        $brand = factory(ProductBrand::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('product_brand.destroy.api', ['brand' => $brand->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('product_brand.destroy.api', ['brand' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
