<?php

namespace Tests\Feature\Product;

use App\Model\Product\ProductPartCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductPartCategoryControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'part_category', 'part_code', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'part_category' => $this->faker->name,
            'part_code' => $this->faker->name,
            'part_code' => $this->faker->name,
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(ProductPartCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('product_part_category.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(ProductPartCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('product_part_category.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(ProductPartCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('product_part_category.select2.api'))
            ->assertStatus(200);
    }

    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('product_part_category.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('product_part_category.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'part_category',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $category = factory(ProductPartCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('product_part_category.store.api'), $this->getPlayload(['part_category' => $category->part_category]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'part_category',
                ]
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $category = factory(ProductPartCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('product_part_category.show.api', ['category' => $category->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('product_part_category.show.api', ['category' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $category = factory(ProductPartCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_part_category.update.api', ['category' => $category->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_part_category.update.api', ['category' => 'test']), $this->getPlayload())
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_sukses()
    {
        $category = factory(ProductPartCategory::class)->create();

        $playload = $this->getPlayload([
            'id' => $category->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_part_category.update.api', ['category' => $category->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_fail()
    {
        $category = factory(ProductPartCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json(
                'PUT',
                route('product_part_category.update.api', ['category' => $category->id]),
                $this->getPlayload([
                    'part_category' => $category->part_category
                ])
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name()
    {
        $category = factory(ProductPartCategory::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_part_category.update.api', ['category' => $category->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'part_category',
                ]
            ]);
    }

    /** @test */
    public function test_update_uniq_name()
    {
        $category = factory(ProductPartCategory::class)->create();

        $playload = $this->getPlayload([
            'part_category' => $category->part_category
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_part_category.update.api', ['category' => $category->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'part_category',
                ]
            ]);
    }

    # test delete
    # 
    # ==============

    /** @test */
    public function test_destroy()
    {
        $category = factory(ProductPartCategory::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('product_part_category.destroy.api', ['category' => $category->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('product_part_category.destroy.api', ['category' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
