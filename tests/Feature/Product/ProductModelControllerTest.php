<?php

namespace Tests\Feature\Product;

use App\Model\Product\ProductAdditional;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductModelControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'additional_code', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'additional_code' => $this->faker->name,
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(ProductAdditional::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('product_additional.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(ProductAdditional::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('product_additional.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(ProductAdditional::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('product_additional.select2.api'))
            ->assertStatus(200);
    }

    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('product_additional.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('product_additional.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'additional_code',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $product_additional = factory(ProductAdditional::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('product_additional.store.api'), $this->getPlayload(['additional_code' => $product_additional->additional_code]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'additional_code',
                ]
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $product_additional = factory(ProductAdditional::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('product_additional.show.api', ['product_additional' => $product_additional->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('product_additional.show.api', ['product_additional' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $product_additional = factory(ProductAdditional::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_additional.update.api', ['product_additional' => $product_additional->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_additional.update.api', ['product_additional' => 'test']), $this->getPlayload())
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_uniq_name_sukses()
    {
        $product_additional = factory(ProductAdditional::class)->create();

        $playload = $this->getPlayload([
            'id' => $product_additional->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_additional.update.api', ['product_additional' => $product_additional->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_uniq_name_fail()
    {
        $product_additional = factory(ProductAdditional::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json(
                'PUT',
                route('product_additional.update.api', ['product_additional' => $product_additional->id]),
                $this->getPlayload([
                    'additional_code' => $product_additional->additional_code
                ])
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name()
    {
        $product_additional = factory(ProductAdditional::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_additional.update.api', ['product_additional' => $product_additional->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'additional_code',
                ]
            ]);
    }

    /** @test */
    public function test_update_uniq_name()
    {
        $product_additional = factory(ProductAdditional::class)->create();

        $playload = $this->getPlayload([
            'additional_code' => $product_additional->additional_code
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('product_additional.update.api', ['product_additional' => $product_additional->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'additional_code',
                ]
            ]);
    }
}
