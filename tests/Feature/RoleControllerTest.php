<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class RoleControllerTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function test_index_role()
    {
        $this->withHeaders($this->getHeaderReq())
            ->get(route('roles.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    '*' => [
                        'id',
                        'name',
                        'guard_name',
                        'created_at',
                        'updated_at',
                        'permissions' => [
                            '*' => [
                                'id',
                                'name',
                                'guard_name',
                                'created_at',
                                'updated_at',
                            ]
                        ]
                    ]
                ],
            ]);
    }

    /** @test */
    public function store_role()
    {
        $permission = factory(Permission::class)->create([
            'guard_name' => 'api'
        ]);

        $playload = [
            'name' => $this->faker->word,
            'permissions' => [$permission->id]
        ];

        // $this->withHeaders($this->getHeaderReq())
        //     ->json('POST', route('roles.store.api'), $playload)->dump();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('roles.store.api'), $playload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'id',
                    'name',
                    'guard_name',
                    'created_at',
                    'updated_at',
                    'permissions' => [
                        '*' => [
                            'id',
                            'name',
                            'guard_name',
                            'created_at',
                            'updated_at',
                        ]
                    ]
                ],
            ]);
    }

    /** @test */
    public function test_show_role()
    {
        $permission = factory(Permission::class)->create();

        // create role
        $role = Role::create([
            'name' => $this->faker->name,
        ]);

        // sync Permissions
        $role->syncPermissions($permission);

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('roles.show.api', ['id' => $role->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'id',
                    'name',
                    'guard_name',
                    'created_at',
                    'updated_at',
                ],
            ]);
    }

    /** @test */
    public function test_update_role()
    {
        $permission = factory(Permission::class)->create();

        // create role
        $role = Role::create([
            'name' => $this->faker->name,
        ]);

        // sync Permissions
        $role->syncPermissions($permission);

        $playload = [
            'name' => $this->faker->word,
            'permissions' => [$permission->id]
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('roles.update.api', ['id' => $role->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_destroy_role()
    {
        // create role
        $role = Role::create([
            'name' => $this->faker->name,
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('roles.destroy.api', ['id' => $role->id]))
            ->assertStatus(204);
    }
}
