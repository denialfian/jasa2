<?php

namespace Tests\Feature;

use App\Model\Menu;
use App\Services\MenuService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Permission;
use Tests\TestCase;

class MenuControllerTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function test_index()
    {
        factory(Menu::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('menus.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => ['id', 'name', 'childs', 'display_order', 'created_at', 'updated_at']
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(Menu::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('menus.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_store()
    {
        $playload = [
            'name' => $this->faker->name,
            'url' => $this->faker->url,
            'icon' => $this->faker->name,
            'display_order' => 1,
            'permission_id' =>  Permission::first()->id,
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('menus.store.api'), $playload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'id', 'name', 'url', 'icon', 'permission_id', 'created_at', 'updated_at'
                ],
            ]);
    }

    /** @test */
    public function test_store_with_parent()
    {
        $menu = factory(Menu::class)->create();

        $playload = [
            'name' => $this->faker->name,
            'url' => $this->faker->url,
            'icon' => $this->faker->name,
            'display_order' => 1,
            'permission_id' =>  Permission::first()->id,
            'parent_id' => $menu->id
        ];
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('menus.store.api'), $playload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'id', 'name', 'url', 'icon', 'permission_id', 'created_at', 'updated_at'
                ],
            ]);
    }

    /** @test */
    public function test_store_with_parent_not_found()
    {
        $playload = [
            'name' => $this->faker->name,
            'url' => $this->faker->url,
            'icon' => $this->faker->name,
            'display_order' => 1,
            'permission_id' =>  Permission::first()->id,
            'parent_id' => 'test'
        ];
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('menus.store.api'), $playload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'id', 'name', 'url', 'icon', 'permission_id', 'created_at', 'updated_at'
                ],
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('menus.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_show()
    {
        $menu = factory(Menu::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('menus.show.api', ['menu' => $menu->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'id', 'name', 'url', 'icon', 'permission_id', 'created_at', 'updated_at'
                ],
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('menus.show.api', ['menu' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update()
    {
        $menu = factory(Menu::class)->create();

        $playload = [
            'name' => $this->faker->name,
            'url' => $this->faker->url,
            'icon' => $this->faker->name,
            'display_order' => 1,
            'permission_id' =>  Permission::first()->id,
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('menus.update.api', ['menu' => $menu->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_with_parent()
    {
        $menu = factory(Menu::class)->create();
        $parent = factory(Menu::class)->create();

        $playload = [
            'name' => $this->faker->name,
            'url' => $this->faker->url,
            'icon' => $this->faker->name,
            'display_order' => 1,
            'permission_id' =>  Permission::first()->id,
            'parent_id' => $parent->id
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('menus.update.api', ['menu' => $menu->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_with_parent_not_found()
    {
        $menu = factory(Menu::class)->create();

        $playload = [
            'name' => $this->faker->name,
            'url' => $this->faker->url,
            'icon' => $this->faker->name,
            'display_order' => 1,
            'permission_id' =>  Permission::first()->id,
            'parent_id' => 'test'
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('menus.update.api', ['menu' => $menu->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $playload = [
            'name' => $this->faker->name,
            'url' => $this->faker->url,
            'icon' => $this->faker->name,
            'display_order' => 1,
            'permission_id' =>  Permission::first()->id,
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('menus.update.api', ['menu' => 'test']), $playload)
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name()
    {
        $menu = factory(Menu::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('menus.update.api', ['menu' => $menu->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_destroy()
    {
        $menu = factory(Menu::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('menus.destroy.api', ['menu' => $menu->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_has_childs()
    {
        $menu = factory(Menu::class)->create();
        factory(Menu::class)->create([
            'parent_id' => $menu->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('menus.destroy.api', ['menu' => $menu->id]))
            ->assertStatus(400);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('menus.destroy.api', ['menu' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
