<?php

namespace Tests\Feature;

use App\Services\UserService;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Spatie\Permission\Models\Permission;
use Tests\TestCase;

class PermissionControllerTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function test_index_permission()
    {
        factory(Permission::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('permissions.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => ['id', 'name', 'guard_name', 'created_at', 'updated_at']
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_store_permission()
    {
        $playload = [
            'name' => $this->faker->word
        ];
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('permissions.store.api'), $playload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'id', 'name', 'guard_name', 'created_at', 'updated_at'
                ],
            ]);
    }

    /** @test */
    public function test_store_permission_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('permissions.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_show_permission()
    {
        $permission = factory(Permission::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('permissions.show.api', ['id' => $permission->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'id', 'name', 'guard_name', 'created_at', 'updated_at'
                ],
            ]);
    }

    /** @test */
    public function test_show_not_found_permission()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('permissions.show.api', ['id' => 'test']))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_permission()
    {
        $permission = factory(Permission::class)->create();

        $playload = [
            'name' => $this->faker->word
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('permissions.update.api', ['id' => $permission->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_permission_required_name()
    {
        $permission = factory(Permission::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('permissions.update.api', ['id' => $permission->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_destroy_permission()
    {
        $permission = factory(Permission::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('permissions.destroy.api', ['id' => $permission->id]))
            ->assertStatus(204);
    }
}
