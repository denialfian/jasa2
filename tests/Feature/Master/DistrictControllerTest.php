<?php

namespace Tests\Feature\Master;

use App\Model\Master\MsCity;
use App\Model\Master\MsDistrict;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DistrictControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'name', 'ms_city_id', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'name' => $this->faker->name,
            'ms_city_id' => factory(MsCity::class)->create()->id
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(MsDistrict::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('districts.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(MsDistrict::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('districts.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(MsDistrict::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('districts.select2.api'))
            ->assertStatus(200);
    }


    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('districts.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_city_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('districts.store.api'), $this->getPlayload([
                'ms_city_id' => 0
            ]))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('districts.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_store_required_city_id()
    {
        $playload = [
            'name' => $this->faker->name
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('districts.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'ms_city_id',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $district = factory(MsDistrict::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('districts.store.api'), $this->getPlayload(['name' => $district->name]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }


    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $district = factory(MsDistrict::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('districts.show.api', ['district' => $district->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('districts.show.api', ['district' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    # test delete
    # 
    # ==============

    /** @test */
    public function test_destroy()
    {
        $district = factory(MsDistrict::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('districts.destroy.api', ['district' => $district->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('districts.destroy.api', ['district' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
