<?php

namespace Tests\Feature\Master;

use App\Model\Master\Vendor;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VendorControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'name', 'code', 'office_email', 'is_business_type', 'user_id', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'name' => $this->faker->name,
            'code' => $this->faker->name,
            'office_email' => $this->faker->email,
            'is_business_type' => 1,
            'user_id' => factory(User::class)->create()->id
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(Vendor::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('vendors.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(Vendor::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('vendors.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(Vendor::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('vendors.select2.api'))
            ->assertStatus(200);
    }

    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('vendors.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('vendors.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $vendor = factory(Vendor::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('vendors.store.api'), $this->getPlayload(['name' => $vendor->name]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $vendor = factory(Vendor::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('vendors.show.api', ['vendor' => $vendor->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('vendors.show.api', ['vendor' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $vendor = factory(Vendor::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('vendors.update.api', ['vendor' => $vendor->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('vendors.update.api', ['vendor' => 'test']), $this->getPlayload())
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    // update 
    /** @test */
    public function test_update_required_name_sukses()
    {
        $vendor = factory(Vendor::class)->create();

        $playload = $this->getPlayload([
            'id' => $vendor->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('vendors.update.api', ['vendor' => $vendor->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_fail()
    {
        $vendor = factory(Vendor::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json(
                'PUT',
                route('vendors.update.api', ['vendor' => $vendor->id]),
                $this->getPlayload([
                    'name' => $vendor->name
                ])
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name()
    {
        $vendor = factory(Vendor::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('vendors.update.api', ['vendor' => $vendor->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_update_uniq_name()
    {
        $vendor = factory(Vendor::class)->create();

        $playload = $this->getPlayload([
            'name' => $vendor->name
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('vendors.update.api', ['vendor' => $vendor->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }


    // delete

    /** @test */
    public function test_destroy()
    {
        $vendor = factory(Vendor::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('vendors.destroy.api', ['vendor' => $vendor->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('vendors.destroy.api', ['vendor' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
