<?php

namespace Tests\Feature\Master;

use App\Model\Master\MsCity;
use App\Model\Master\MsCountry;
use App\Model\Master\MsProvince;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProvinceControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'name', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'name' => $this->faker->name,
            'iso_code' => $this->faker->name,
            'ms_country_id' => factory(MsCountry::class)->create()->id
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(MsProvince::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('provinces.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(MsProvince::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('provinces.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(MsProvince::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('provinces.select2.api'))
            ->assertStatus(200);
    }


    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('provinces.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_country_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('provinces.store.api'), $this->getPlayload([
                'ms_country_id' => 0
            ]))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('provinces.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $province = factory(MsProvince::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('provinces.store.api'), $this->getPlayload(['name' => $province->name]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }


    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $province = factory(MsProvince::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('provinces.show.api', ['province' => $province->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('provinces.show.api', ['province' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }



    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $province = factory(MsProvince::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('provinces.update.api', ['province' => $province->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_country_not_found()
    {
        $province = factory(MsProvince::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('provinces.update.api', ['province' => $province->id]), $this->getPlayload([
                'ms_country_id' => 0
            ]))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('provinces.update.api', ['province' => 'test']), $this->getPlayload())
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_sukses()
    {
        $province = factory(MsProvince::class)->create();

        $playload = $this->getPlayload([
            'id' => $province->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('provinces.update.api', ['province' => $province->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_fail()
    {
        $province = factory(MsProvince::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json(
                'PUT',
                route('provinces.update.api', ['province' => $province->id]),
                $this->getPlayload([
                    'name' => $province->name
                ])
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name()
    {
        $province = factory(MsProvince::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('provinces.update.api', ['province' => $province->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_update_uniq_name()
    {
        $province = factory(MsProvince::class)->create();

        $playload = $this->getPlayload([
            'name' => $province->name
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('provinces.update.api', ['province' => $province->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    # test delete
    # 
    # ==============

    /** @test */
    public function test_destroy()
    {
        $province = factory(MsProvince::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('provinces.destroy.api', ['province' => $province->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('provinces.destroy.api', ['province' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_destroy_has_city()
    {
        $province = factory(MsProvince::class)->create();

        factory(MsCity::class)->create([
            'ms_province_id' => $province->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('provinces.destroy.api', ['province' => $province->id]))
            ->assertStatus(400);
    }
}
