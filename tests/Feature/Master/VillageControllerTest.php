<?php

namespace Tests\Feature\Master;

use App\Model\Master\MsDistrict;
use App\Model\Master\MsVillage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VillageControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'name', 'ms_district_id', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'name' => $this->faker->name,
            'ms_district_id' => factory(MsDistrict::class)->create()->id
        ];

        return array_merge($playload, $append);
    }


    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(MsVillage::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('villages.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(MsVillage::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('villages.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(MsVillage::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('villages.select2.api'))
            ->assertStatus(200);
    }

    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('villages.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_district_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('villages.store.api'), $this->getPlayload([
                'ms_district_id' => 0
            ]))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('villages.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_store_required_distrik_id()
    {
        $playload = [
            'name' => $this->faker->name
        ];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('villages.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'ms_district_id',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $village = factory(MsVillage::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('villages.store.api'), $this->getPlayload(['name' => $village->name]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $village = factory(MsVillage::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('villages.show.api', ['village' => $village->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('villages.show.api', ['village' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }


    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $village = factory(MsVillage::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('villages.update.api', ['village' => $village->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_distrik_not_found()
    {
        $village = factory(MsVillage::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('villages.update.api', ['village' => $village->id]), $this->getPlayload([
                'ms_district_id' => 0
            ]))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('villages.update.api', ['village' => 'test']), $this->getPlayload())
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_sukses()
    {
        $village = factory(MsVillage::class)->create();

        $playload = $this->getPlayload([
            'id' => $village->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('villages.update.api', ['village' => $village->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_fail()
    {
        $village = factory(MsVillage::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json(
                'PUT',
                route('villages.update.api', ['village' => $village->id]),
                $this->getPlayload([
                    'name' => $village->name
                ])
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name()
    {
        $village = factory(MsVillage::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('villages.update.api', ['village' => $village->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_update_uniq_name()
    {
        $village = factory(MsVillage::class)->create();

        $playload = $this->getPlayload([
            'name' => $village->name
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('villages.update.api', ['village' => $village->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    # test delete
    # 
    # ==============

    /** @test */
    public function test_destroy()
    {
        $village = factory(MsVillage::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('villages.destroy.api', ['village' => $village->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('villages.destroy.api', ['village' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
