<?php

namespace Tests\Feature\Master;

use App\Model\Master\BatchStatus;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BatchStatusControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'name', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'name' => $this->faker->name,
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(BatchStatus::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('batch_status.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(BatchStatus::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('batch_status.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(BatchStatus::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('batch_status.select2.api'))
            ->assertStatus(200);
    }

    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('batch_status.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_required_name()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('batch_status.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_name()
    {
        $status = factory(BatchStatus::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('batch_status.store.api'), $this->getPlayload(['name' => $status->name]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $status = factory(BatchStatus::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('batch_status.show.api', ['status' => $status->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('batch_status.show.api', ['status' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $status = factory(BatchStatus::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('batch_status.update.api', ['status' => $status->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('batch_status.update.api', ['status' => 'test']), $this->getPlayload())
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    // update 
    /** @test */
    public function test_update_required_name_sukses()
    {
        $status = factory(BatchStatus::class)->create();

        $playload = $this->getPlayload([
            'id' => $status->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('batch_status.update.api', ['status' => $status->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name_fail()
    {
        $status = factory(BatchStatus::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json(
                'PUT',
                route('batch_status.update.api', ['status' => $status->id]),
                $this->getPlayload([
                    'name' => $status->name
                ])
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_name()
    {
        $status = factory(BatchStatus::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('batch_status.update.api', ['status' => $status->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }

    /** @test */
    public function test_update_uniq_name()
    {
        $status = factory(BatchStatus::class)->create();

        $playload = $this->getPlayload([
            'name' => $status->name
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('batch_status.update.api', ['status' => $status->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'name',
                ]
            ]);
    }


    // delete

    /** @test */
    public function test_destroy()
    {
        $status = factory(BatchStatus::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('batch_status.destroy.api', ['status' => $status->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('batch_status.destroy.api', ['status' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
