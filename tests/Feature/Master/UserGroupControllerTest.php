<?php

namespace Tests\Feature\Master;

use App\Model\Master\UserGroup;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserGroupControllerTest extends TestCase
{
    use WithFaker;

    // response yg didapat
    public function resourceArray()
    {
        return ['id', 'business_type', 'property_name', 'created_at', 'updated_at'];
    }

    // inputan yg di kirim
    public function getPlayload(array $append = [])
    {
        $playload = [
            'business_type' => $this->faker->name,
            'property_name' => $this->faker->name,
            'user_id' => factory(User::class)->create()->id,
        ];

        return array_merge($playload, $append);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_index()
    {
        factory(UserGroup::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('user_group.index.api'))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'current_page',
                    'data' => [
                        '*' => $this->resourceArray()
                    ],
                    'first_page_url',
                    'from',
                    'last_page',
                    'last_page_url',
                    'next_page_url',
                    'path',
                    'per_page',
                    'prev_page_url',
                    'to',
                    'total',
                ],
            ]);
    }

    /** @test */
    public function test_datatables_list()
    {
        factory(UserGroup::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('user_group.datatables.api'))
            ->assertStatus(200);
    }

    /** @test */
    public function test_select2_list()
    {
        factory(UserGroup::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->get(route('user_group.select2.api'))
            ->assertStatus(200);
    }

    # test insert
    # 
    # ==============
    /** @test */
    public function test_store()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('user_group.store.api'), $this->getPlayload())
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_store_required()
    {
        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('user_group.store.api'), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'business_type',
                ]
            ]);
    }

    /** @test */
    public function test_store_uniq_property_name()
    {
        $user_group = factory(UserGroup::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('POST', route('user_group.store.api'), $this->getPlayload(['property_name' => $user_group->property_name]))
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'property_name',
                ]
            ]);
    }

    # test show list
    # 
    # ==============
    /** @test */
    public function test_show()
    {
        $user_group = factory(UserGroup::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('user_group.show.api', ['user_group' => $user_group->id]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => $this->resourceArray(),
            ]);
    }

    /** @test */
    public function test_show_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('GET', route('user_group.show.api', ['user_group' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    # test update
    # 
    # ==============
    /** @test */
    public function test_update()
    {
        $user_group = factory(UserGroup::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('user_group.update.api', ['user_group' => $user_group->id]), $this->getPlayload())
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('user_group.update.api', ['user_group' => 'test']), $this->getPlayload())
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_property_name_sukses()
    {
        $user_group = factory(UserGroup::class)->create();

        $playload = $this->getPlayload([
            'id' => $user_group->id
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('user_group.update.api', ['user_group' => $user_group->id]), $playload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_property_name_fail()
    {
        $user_group = factory(UserGroup::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json(
                'PUT',
                route('user_group.update.api', ['user_group' => $user_group->id]),
                $this->getPlayload([
                    'property_name' => $user_group->property_name
                ])
            )
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }

    /** @test */
    public function test_update_required_property_name()
    {
        $user_group = factory(UserGroup::class)->create();

        $playload = [];

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('user_group.update.api', ['user_group' => $user_group->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'property_name',
                ]
            ]);
    }

    /** @test */
    public function test_update_uniq_property_name()
    {
        $user_group = factory(UserGroup::class)->create();

        $playload = $this->getPlayload([
            'property_name' => $user_group->property_name
        ]);

        $this->withHeaders($this->getHeaderReq())
            ->json('PUT', route('user_group.update.api', ['user_group' => $user_group->id]), $playload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'property_name',
                ]
            ]);
    }

    # test delete
    # 
    # ==============

    /** @test */
    public function test_destroy()
    {
        $user_group = factory(UserGroup::class)->create();

        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('user_group.destroy.api', ['user_group' => $user_group->id]))
            ->assertStatus(204);
    }

    /** @test */
    public function test_destroy_not_found()
    {
        $this->withHeaders($this->getHeaderReq())
            ->json('DELETE', route('user_group.destroy.api', ['user_group' => 'test']))
            ->assertStatus(404)
            ->assertJsonStructure([
                'msg',
                'data',
            ]);
    }
}
