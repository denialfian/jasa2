<?php

namespace Tests\Feature;

use App\Services\UserService;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function can_register_api()
    {
        $payload = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => 'password',
            'password_confirmation' => 'password'
        ];

        $this->post(route('auth.register.api'), $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'token',
                    'user' => [
                        'id',
                        'name',
                        'email',
                        'api_token',
                        'updated_at',
                        'created_at',
                        'roles'
                    ]
                ]
            ]);
    }

    /** @test */
    public function can_login_api()
    {
        $payload = [
            'email' => $this->faker->email,
            'password' => 'password'
        ];

        factory(User::class)->create([
            'email' => $payload['email'],
            'password' => Hash::make('password')
        ]);

        $this->post(route('auth.login.api'), $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'msg',
                'data' => [
                    'token',
                    'user' => [
                        'id',
                        'name',
                        'email',
                        'api_token',
                        'updated_at',
                        'created_at'
                    ]
                ]
            ]);
    }

    /** @test */
    public function register_required_name_email_password_api()
    {
        $payload = [];

        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ])
            ->post(route('auth.register.api'), $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'name',
                    'email',
                    'password',
                ]
            ]);
    }

    /** @test */
    public function register_required_password_confirmation_api()
    {
        $payload = [
            'name' => 'test',
            'email' => 'testlogin@user.com',
            'password' => 'password',
        ];

        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ])
            ->post(route('auth.register.api'), $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'password',
                ]
            ]);
    }

    /** @test */
    public function login_required_email_password_api()
    {
        $payload = [];

        $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ])
            ->post(route('auth.login.api'), $payload)
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'email',
                    'password',
                ]
            ]);
    }

    /** @test */
    public function can_register_with_email_web()
    {
        $payload = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => 'password',
            'password_confirmation' => 'password',
        ];

        $response = $this->post(route('auth.register'), $payload);
        $response
            ->assertRedirect('/verify');
    }

    /** @test */
    public function can_register_with_phone_web()
    {
        $payload = [
            'name' => $this->faker->name,
            'email' => '0001',
            'password' => 'password',
            'password_confirmation' => 'password',
        ];

        $response = $this->post(route('auth.register'), $payload);
        $response
            ->assertRedirect('/verify');
    }

    /** @test */
    public function user_otp_verify_success()
    {
        $user = (new UserService)->create([
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'role_id' => Role::first()->id,
            'password' => 'password',
        ]);

        $payload = [
            'otp', $user->otp_code
        ];

        $response = $this->actingAs($user)->post(route('otp.verification'), $payload);
        $response
            ->assertRedirect('/success');
    }

    /** @test */
    public function user_otp_verify_failed()
    {
        $user = (new UserService)->create([
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'role_id' => Role::first()->id,
            'password' => 'password',
        ]);

        $payload = [
            'otp', '-'
        ];

        $response = $this->actingAs($user)->post(route('otp.verification'), $payload);
        $response
            ->assertRedirect('/error');
    }

    /** @test */
    public function can_login_web()
    {
        $user = factory(User::class)->create([
            'password' => Hash::make('password')
        ]);

        $payload = [
            'email' => $user->email,
            'password' => 'password',
        ];

        $response = $this->post(route('auth.login'), $payload);
        $response
            ->assertRedirect(route('home'));
    }

    /** @test */
    public function failed_login_web_not_verify()
    {
        $user = (new UserService)->create([
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'role_id' => Role::first()->id,
            'password' => 'password',
        ]);


        $payload = [
            'email' => $user->email,
            'password' => 'password',
        ];

        $response = $this->post(route('auth.login'), $payload);
        $response
            ->assertRedirect('/verify');
    }

    /** @test */
    public function failed_login_web()
    {
        $payload = [
            'email' => $this->faker->email,
            'password' => 'password',
        ];

        $response = $this->post(route('auth.login'), $payload);
        $response
            ->assertRedirect('/');
    }
}
