importScripts('https://www.gstatic.com/firebasejs/8.0.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.1/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "AIzaSyACwbJDRaXMUbGzuM2ONkGFWdpyQ1SPiwc",
    authDomain: "astech-fcm-417ce.firebaseapp.com",
    projectId: "astech-fcm-417ce",
    storageBucket: "astech-fcm-417ce.appspot.com",
    messagingSenderId: "1066340148903",
    appId: "1:1066340148903:web:79587d258985294c6a1d82"
});

console.log('calling fcm')

const messaging = firebase.messaging();

// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START on_background_message]
messaging.onBackgroundMessage(function (payload) {
    console.log('firebase payload ', payload);
    // Customize notification here
    const notificationTitle = payload.notification.title;
    const notificationOptions = {
        body: payload.notification.body
    };
    console.log('firebase content', notificationTitle, notificationOptions)
    self.registration.showNotification(notificationTitle,
        notificationOptions);
});
// [END on_background_message]