<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/image/delete', 'Admin\ImageUploadController@fileDestroy');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', 'Api\AuthController@login')->name('auth.login.api');
Route::post('register', 'Api\AuthController@register')->name('auth.register.api');

// reset password
Route::prefix('password')->group(function () {
    Route::post('/create', 'Api\PasswordResetController@create')
        ->name('password.reset.create.api');
    Route::post('/change', 'Api\PasswordResetController@change')
        ->name('password.reset.change.api');
    Route::get('/find/{token}', 'Api\PasswordResetController@find')
        ->name('password.reset.find.api');
});

Route::group(['middleware' => ['auth:api']], function () {

    // Email Verification Routes...
    Route::post('email/verify/{id}', 'Api\VerificationController@verify')
        ->name('verification.verify');
    Route::post('email/resend', 'Api\VerificationController@resend')
        ->name('verification.resend');

    // role, permission, user, menu
    require base_path('routes/api/setting.php');

    // product_attribute, product_additional
    require base_path('routes/api/product.php');

    // category, vendor
    require base_path('routes/api/master.php');

    // country, province, city, distrik, village, zipcode
    require base_path('routes/api/lokasi.php');

    // user modul
    require base_path('routes/api/user.php');

    // teknisi modul
    require base_path('routes/api/technician.php');

});
Route::post('/import-excel', 'Api\Master\ImportExcelController@importExcel')
->name('import.excel');
// address search
Route::prefix('address_search')->group(function () {
    Route::get('/find_country/{country_id?}', 'Api\AddressSearchController@findCountry')
        ->name('address_search.find_country.api');

    Route::get('/find_province/{province_id?}', 'Api\AddressSearchController@findProvince')
        ->name('address_search.find_province.api');

    Route::get('/find_city/{city_id?}', 'Api\AddressSearchController@findCity')
        ->name('address_search.find_city.api');

    Route::get('/find_district/{district_id?}', 'Api\AddressSearchController@findDistrict')
        ->name('address_search.find_district.api');

    Route::get('/find_village/{village_id?}', 'Api\AddressSearchController@findVillage')
        ->name('address_search.find_village.api');

    Route::get('/find_zipcode/{zipcode_id?}', 'Api\AddressSearchController@findZipcode')
        ->name('address_search.find_zipcode.api');
});

//product_search
Route::prefix('product_search')->group(function () {
    // search vendors by product id
    Route::get('/vendors/{product_id}', 'Api\Product\ProductSearchController@getVendorList')
        ->name('product_search.vendors.api');

    // search varian by product id and vendor id
    Route::get('/varians/{product_id}/{vendor_id}', 'Api\Product\ProductSearchController@getVendorVarianList')
        ->name('product_search.varians.api');

    // search
    Route::post('/varian', 'Api\Product\ProductSearchController@getVarian')
        ->name('product_search.varian.api');
});
