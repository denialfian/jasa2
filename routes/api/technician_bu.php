<?php

use Illuminate\Support\Facades\Route;

// job_title_categories
Route::prefix('job_title_categories')->group(function () {
    Route::get('/datatables', 'Api\Technician\JobTitleCategoryController@datatables')
        ->middleware('permission:job_title_category.show')
        ->name('job_title_categories.datatables.api');

    Route::get('/select2', 'Api\Technician\JobTitleCategoryController@select2')
        ->middleware('permission:job_title_category.show')
        ->name('job_title_categories.select2.api');

    Route::get('/', 'Api\Technician\JobTitleCategoryController@index')
        ->middleware('permission:job_title_category.show')
        ->name('job_title_categories.index.api');

    Route::post('/', 'Api\Technician\JobTitleCategoryController@store')
        ->middleware('permission:job_title_category.create')
        ->name('job_title_categories.store.api');

    Route::get('/{category}', 'Api\Technician\JobTitleCategoryController@show')
        ->middleware('permission:job_title_category.create')
        ->name('job_title_categories.show.api');

    Route::put('/{category}', 'Api\Technician\JobTitleCategoryController@update')
        ->middleware('permission:job_title_category.update')
        ->name('job_title_categories.update.api');

    Route::delete('/destroy_batch', 'Api\Technician\JobTitleCategoryController@destroyBatch')
        ->middleware('permission:job_title_category.delete')
        ->name('job_title_categories.destroy_batch.api');

    Route::delete('/{category}', 'Api\Technician\JobTitleCategoryController@destroy')
        ->middleware('permission:job_title_category.delete')
        ->name('job_title_categories.destroy.api');
});

// job_title_categories
Route::prefix('job_titles')->group(function () {
    Route::get('/datatables', 'Api\Technician\JobTitleController@datatables')
        ->middleware('permission:job_title.show')
        ->name('job_titles.datatables.api');

    Route::get('/select2', 'Api\Technician\JobTitleController@select2')
        ->middleware('permission:job_title.show')
        ->name('job_titles.select2.api');

    Route::get('/', 'Api\Technician\JobTitleController@index')
        ->middleware('permission:job_title.show')
        ->name('job_titles.index.api');

    Route::post('/', 'Api\Technician\JobTitleController@store')
        ->middleware('permission:job_title.create')
        ->name('job_titles.store.api');

    Route::get('/{job}', 'Api\Technician\JobTitleController@show')
        ->middleware('permission:job_title.create')
        ->name('job_titles.show.api');

    Route::put('/{job}', 'Api\Technician\JobTitleController@update')
        ->middleware('permission:job_title.update')
        ->name('job_titles.update.api');

    Route::delete('/destroy_batch', 'Api\Technician\JobTitleController@destroyBatch')
        ->middleware('permission:job_title.delete')
        ->name('job_titles.destroy_batch.api');

    Route::delete('/{job}', 'Api\Technician\JobTitleController@destroy')
        ->middleware('permission:job_title.delete')
        ->name('job_titles.destroy.api');
});

// job_experiences
Route::prefix('job_experiences')->group(function () {
    Route::get('/datatables', 'Api\Technician\JobExperienceController@datatables')
        ->middleware('permission:job_experience.show')
        ->name('job_experiences.datatables.api');

    Route::get('/select2', 'Api\Technician\JobExperienceController@select2')
        ->middleware('permission:job_experience.show')
        ->name('job_experiences.select2.api');

    Route::get('/', 'Api\Technician\JobExperienceController@index')
        ->middleware('permission:job_experience.show')
        ->name('job_experiences.index.api');

    Route::post('/', 'Api\Technician\JobExperienceController@store')
        ->middleware('permission:job_experience.create')
        ->name('job_experiences.store.api');

    Route::get('/{experience}', 'Api\Technician\JobExperienceController@show')
        ->middleware('permission:job_experience.create')
        ->name('job_experiences.show.api');

    Route::put('/{experience}', 'Api\Technician\JobExperienceController@update')
        ->middleware('permission:job_experience.update')
        ->name('job_experiences.update.api');

    Route::delete('/destroy_batch', 'Api\Technician\JobExperienceController@destroyBatch')
        ->middleware('permission:job_experience.delete')
        ->name('job_experiences.destroy_batch.api');

    Route::delete('/{experience}', 'Api\Technician\JobExperienceController@destroy')
        ->middleware('permission:job_experience.delete')
        ->name('job_experiences.destroy.api');
});
