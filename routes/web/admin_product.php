<?php

# Product Additional
Route::group(['prefix' => 'product-additional'], function () {
    // list data
    Route::get('/', 'Admin\MasterAdditionalController@showAddtitonal')
        ->name('admin.product-additional.index.web')
        ->middleware('permission:product_additional.show');
});

# Product Attribute
Route::group(['prefix' => 'product-attribute'], function () {
    // list data
    Route::get('/', 'Admin\MasterAttributeController@showListAttribute')
        ->middleware('permission:product_attribute.show')
        ->name('product.product_attribute.index.web');

    // create data
    Route::get('create', 'Admin\MasterAttributeController@createMasterAttribute')
        ->middleware('permission:product_attribute.create')
        ->name('product.product_attribute.create.web');

    // edit
    Route::get('{id}/edit', 'Admin\MasterAttributeController@updateMasterAttribute')
        ->middleware('permission:product_attribute.update')
        ->name('permission:product_attribute.update.web');
});

# Product Brand
Route::group(['prefix' => 'product-brand'], function () {
    // list data
    Route::get('/', 'Admin\MasterProductBrandController@showProductBrand')
        ->middleware('permission:product_brand.show')
        ->name('admin.product_brand.index.web');
});

//Product Category
Route::group(['prefix' => 'product-category'], function () {
    Route::get('/', 'Admin\MasterProductCategoryController@showProductCategory')->middleware('permission:product_category.show');
    Route::get('create', 'Admin\MasterProductCategoryController@createProductCategory')->middleware('permission:product_category.create');
    Route::get('update/{id}', 'Admin\MasterProductCategoryController@updateProductCategory')->middleware('permission:product_category.update');
    Route::get('search', 'Admin\MasterProductCategoryController@search')->middleware('permission:');
});

# Product Model
Route::group(['prefix' => 'product-model'], function () {
    // list data
    Route::get('/', 'Admin\MasterProductModelController@showProductModel')
        ->middleware('permission:product_model.show')
        ->name('admin.product-model.index.web');

    // create data
    Route::get('create', 'Admin\MasterProductModelController@createProductModel')
        ->middleware('permission:product_model.create')
        ->name('admin.product-model.create.web');

    // edit data
    Route::get('{id}/edit', 'Admin\MasterProductModelController@updateProductModel')
        ->middleware('permission:product_model.update')
        ->name('admin.product-model.edit.web');
});

# Product Part Category
Route::group(['prefix' => 'product-part-category'], function () {
    // list data
    Route::get('/', 'Admin\MasterPartCategoryController@showPartCategory')
        ->name('admin.product_part_category.index.web')
        ->middleware('permission:product_part_category.show');
});

# Product
Route::group(['prefix' => 'product'], function () {
    // list data
    Route::get('/', 'Admin\MasterProductController@showListProduct')
        ->middleware('permission:product.show')
        ->name('admin.product.index.web');

    // create data
    Route::get('create', 'Admin\MasterProductController@createProduct')
        ->middleware('permission:product.create')
        ->name('admin.product.create.web');

    // edit data
    Route::get('{id}/edit', 'Admin\MasterProductController@editProduct')
        ->middleware('permission:product.update')
        ->name('admin.product.edit.web');
});

# Product Status
Route::group(['prefix' => 'product-status'], function () {
    Route::get('show', 'Admin\MasterProductStatusController@index')->middleware('permission:product_status.show');
});
