<?php

# Country
Route::group(['prefix' => 'country'], function () {
    // list data
    Route::get('/', 'Admin\MasterCountries@showCountries')
        ->middleware('permission:country.show')
        ->name('admin.country.index.web');

    // create data
    Route::get('create', 'Admin\MasterCountries@createCountries')
        ->middleware('permission:country.create')
        ->name('admin.country.create.web');

    // edit data
    Route::get('{id}/edit', 'Admin\MasterCountries@updateCountries')
        ->middleware('permission:country.update')
        ->name('admin.country.edit.web');
});

# Province
Route::group(['prefix' => 'province'], function () {
    // list data
    Route::get('/', 'Admin\MasterProvinceController@showprovince')
        ->middleware('permission:province.show')
        ->name('admin.province.index.web');

    // create data
    Route::get('create', 'Admin\MasterProvinceController@createProvince')
        ->middleware('permission:province.create')
        ->name('admin.province.create.web');

    // edit data
    Route::get('{id}/edit', 'Admin\MasterProvinceController@updateProvince')
        ->middleware('permission:province.update')
        ->name('admin.province.edit.web');
});

//City
Route::group(['prefix' => 'city'], function () {
    // list data
    Route::get('/', 'Admin\MasterCityController@showCity')
        ->middleware('permission:city.show')
        ->name('admin.city.index.web');

    // create data  
    Route::get('create', 'Admin\MasterCityController@createCity')
        ->middleware('permission:city.create')
        ->name('admin.city.create.web');

    // edit data
    Route::get('{id}/edit', 'Admin\MasterCityController@updateCity')
        ->middleware('permission:city.update')
        ->name('admin.city.edit.web');
});

# District
Route::group(['prefix' => 'district'], function () {
    // list data
    Route::get('/', 'Admin\MasterDistricController@showDistric')
        ->name('admin.district.index.web')
        ->middleware('permission:district.show');

    // create data    
    Route::get('create', 'Admin\MasterDistricController@createDistric')
        ->name('admin.district.create.web')
        ->middleware('permission:district.create');

    // edit data
    Route::get('{id}/edit', 'Admin\MasterDistricController@updateDistric')
        ->name('admin.district.edit.web')
        ->middleware('permission:district.update');
});

# Village
Route::group(['prefix' => 'village'], function () {
    // list data
    Route::get('/', 'Admin\MasterVilageController@showVilage')
        ->name('admin.village.index.web')
        ->middleware('permission:village.show');

    // create data
    Route::get('create', 'Admin\MasterVilageController@createVilage')
        ->name('admin.village.create.web')
        ->middleware('permission:village.create');

    // edit data
    Route::get('{id}/edit', 'Admin\MasterVilageController@updateVilage')
        ->name('admin.village.edit.web')
        ->middleware('permission:village.update');
});

# Zipcode
Route::group(['prefix' => 'zipcode'], function () {
    // list data
    Route::get('/', 'Admin\MasterZipCodeController@showZipCode')
        ->name('admin.zipcode.index.web')
        ->middleware('permission:zipcode.show');

    // create data
    Route::get('create', 'Admin\MasterZipCodeController@createZipCode')
        ->name('admin.zipcode.create.web')
        ->middleware('permission:zipcode.create');

    // edit data
    Route::get('{id}/edit', 'Admin\MasterZipCodeController@updateZipCode')
        ->name('admin.zipcode.edit.web')
        ->middleware('permission:zipcode.update');
});
