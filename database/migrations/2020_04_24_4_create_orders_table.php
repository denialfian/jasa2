<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 50);
            $table->string('customer_email')->nullable();
            $table->string('customer_phone')->nullable();
            $table->string('address_type_name');
            $table->string('service_name');
            $table->string('symptom_name');
            $table->string('product_group_name');
            $table->string('product_name_detail')->nullable();
            $table->string('brand_name_detail')->nullable();
            $table->string('model_name_detail')->nullable();
            $table->string('serial_number_detail')->nullable();
            $table->string('remark_detail')->nullable();

            $table->tinyInteger('notifikasi_type')->default(0)->comment('0 tidak ada nofifikasi, 1 ada');
            $table->tinyInteger('is_canceled')->default(0);
            $table->tinyInteger('is_approve')->default(0);
            $table->tinyInteger('is_reschedule')->default(0);
            $table->tinyInteger('penalty_order_type')->nullable();           
            $table->tinyInteger('payment_type')->default(1)->comment('1 online, 0 offline');
            $table->tinyInteger('transfer_status')->nullable();

            $table->text('customer_json')->nullable();
            $table->text('repair_desc')->nullable();
            $table->text('note')->nullable();
            $table->text('note_complaint')->nullable();
            $table->text('address');
            $table->text('bill_to');

            $table->dateTime('customer_read_at')->nullable();
            $table->dateTime('teknisi_read_at')->nullable();
            $table->dateTime('admin_read_at')->nullable();
            $table->dateTime('canceled_at')->nullable();
            $table->dateTime('date_complete')->nullable();
            $table->dateTime('schedule', 0);
            $table->dateTime('reschedule', 0)->nullable();

            $table->double('penalty_order_value')->nullable(); 
            $table->double('wallet_customer')->nullable();
            $table->double('grand_total')->nullable();
            $table->double('ppn')->nullable();
            $table->double('discount')->nullable();
            $table->double('ppn_nominal')->nullable();
            $table->double('discount_nominal')->nullable();
            $table->double('total_commission')->nullable();


            $table->bigInteger('product_group_id')->unsigned()->index();
            $table->bigInteger('address_type_id')->unsigned()->index();
            $table->bigInteger('ms_service_id')->unsigned()->index();
            $table->bigInteger('ms_symptom_id')->unsigned()->index();
            $table->bigInteger('ms_requests_id')->unsigned()->index()->nullable();
            $table->bigInteger('ms_ticket_id')->unsigned()->index()->nullable();
            $table->bigInteger('ms_vouchers_id')->unsigned()->index()->nullable();
            $table->bigInteger('bank_transfer_id')->unsigned()->index()->nullable();
            $table->bigInteger('ms_payment_methods_id')->unsigned()->index()->nullable();
            $table->bigInteger('ms_warehouses_id')->unsigned()->index()->nullable();
            $table->bigInteger('orders_statuses_id')->unsigned()->index()->nullable();
            $table->bigInteger('rating_and_review_id')->unsigned()->index()->nullable();
            $table->bigInteger('after_commission')->nullable();
            $table->integer('estimation_hours')->nullable();
            $table->integer('deleted_at')->datetime()->nullable();
            $table->tinyInteger('is_less_balance')->nullable();
            $table->bigInteger('users_id')->unsigned()->index()->nullable();
            $table->bigInteger('ms_symptom_code')->unsigned()->index()->nullable();
            $table->bigInteger('ms_repair_code')->unsigned()->index()->nullable();

            $table->foreign('users_id')
                ->references('id')
                ->on('users');
            $table->text('symptom_detail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
