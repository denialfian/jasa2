<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsProvincesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_provinces', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('iso_code')->nullable();
            $table->text('meta')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->bigInteger('ms_country_id')->unsigned()->index();
            $table->foreign('ms_country_id')
                ->references('id')
                ->on('ms_countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_provinces');
    }
}
