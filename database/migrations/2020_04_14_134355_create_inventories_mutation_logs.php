<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoriesMutationLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories_mutation_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ms_batch_item_id')->unsigned()->index();
            // $table->foreign('ms_batch_item_id')
            //     ->references('id')
            //     ->on('batch_items')
            //     ->onDelete('cascade');
            $table->bigInteger('inventories_id_out')->unsigned()->index();
            // $table->foreign('inventories_id_out')
            //     ->references('id')
            //     ->on('inventories')
            //     ->onDelete('cascade');
            $table->integer('stock_out')->nullable();
            $table->integer('stock_available_out')->nullable();
            $table->integer('stock_available_in')->nullable();
            $table->bigInteger('inventories_id_in')->unsigned()->index();
            // $table->foreign('inventories_id_in')
            //     ->references('id')
            //     ->on('inventories')
            //     ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories_mutation_logs');
    }
}
