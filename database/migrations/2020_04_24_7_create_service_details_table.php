<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('price');
            $table->string('service_type_name')->nullable();
            $table->string('teknisi_email')->nullable();
            $table->string('symptom_name')->nullable();
            $table->string('teknisi_phone')->nullable();
            $table->text('teknisi_json')->nullable();
            $table->bigInteger('orders_id')->unsigned()->index();
            $table->foreign('orders_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');
            $table->bigInteger('ms_price_services_id')->unsigned()->index()->nullable();
            $table->bigInteger('ms_services_types_id')->unsigned()->index();
            $table->foreign('ms_services_types_id')
                ->references('id')
                ->on('ms_services_types');
            $table->bigInteger('ms_symptoms_id')->unsigned()->index();
            $table->foreign('ms_symptoms_id')
                ->references('id')
                ->on('ms_symptoms');
            $table->bigInteger('ms_service_statuses_id')->unsigned()->index()->nullable();
            $table->bigInteger('technicians_id')->unsigned()->index()->nullable();
            $table->dateTime('schedule', 0);
            $table->string('issue')->nullable();
            $table->integer('unit');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_details');
    }
}
