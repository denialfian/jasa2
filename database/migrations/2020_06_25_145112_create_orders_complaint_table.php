<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersComplaintTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_complaint', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('message');
            $table->text('attachment')->nullable();
            $table->dateTime('read_at')->nullable();
            $table->timestamps();
            $table->text('created_by');

            $table->bigInteger('ms_orders_id')->unsigned()->index();
            $table->foreign('ms_orders_id')
                ->references('id')
                ->on('orders');

            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_complaint');
    }
}
