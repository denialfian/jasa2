<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('stock')->nullable();
            $table->string('sku', 30)->nullable();
            $table->string('vendor_code', 30);
            $table->string('part_code', 30);
            $table->string('brand_code', 30);
            $table->string('product_code', 30);
            $table->timestamps();

            $table->bigInteger('ms_product_id')->unsigned()->index();
            $table->foreign('ms_product_id')
                ->references('id')
                ->on('ms_products')
                ->onDelete('cascade');

            $table->bigInteger('vendor_id')->unsigned()->index();
            $table->foreign('vendor_id')
                ->references('id')
                ->on('vendors')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_vendors');
    }
}
