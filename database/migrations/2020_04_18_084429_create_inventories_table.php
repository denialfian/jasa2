<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('batch_code');
            $table->double('cogs_value');
            $table->bigInteger('product_id');
            $table->bigInteger('ms_warehouse_id');
            $table->string('warehouse_name');
            $table->bigInteger('ms_batch_item_id');
            $table->bigInteger('ms_unit_type_id');
            $table->bigInteger('product_vendor_id');
            $table->integer('stock');
            $table->integer('stock_temp');
            $table->integer('is_need_approval');
            $table->integer('user_approve_id');
            $table->integer('item_name');
            $table->integer('stock_ret');
            $table->integer('stock_out');
            $table->integer('stock_now');
            $table->bigInteger('product_varian_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
