<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurriculumSquenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curriculum_squence', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sequence_name');
            $table->timestamps();

            $table->bigInteger('dev_program_id')->unsigned()->index();
            $table->foreign('dev_program_id')
                ->references('id')
                ->on('development_programs');

            $table->bigInteger('curriculum_id')->unsigned()->index();
            $table->foreign('curriculum_id')
                ->references('id')
                ->on('curriculum');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curriculum_squence');
    }
}
