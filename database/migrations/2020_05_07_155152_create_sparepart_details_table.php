<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSparepartDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sparepart_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('orders_id')->unsigned()->index();
            $table->foreign('orders_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade');
            $table->bigInteger('technicians_id')->unsigned()->index();
            $table->text('name_sparepart')->nullable();
            $table->double('price')->nullable();
            $table->integer('quantity')->nullable();
            // $table->longText('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sparepart_details');
    }
}
