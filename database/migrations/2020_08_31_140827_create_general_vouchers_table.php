<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_vouchers', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->integer('type');
            $table->double('value');
            $table->double('max_nominal');
            $table->double('min_payment');
            $table->string('valid_at');
            $table->string('valid_until');
            $table->integer('is_new_register');
            $table->text('desc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_vouchers');
    }
}
