<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsTicketsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_tickets_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ms_ticket_id')->unsigned()->index();
            $table->dateTime('read_at')->nullable();
            $table->foreign('ms_ticket_id')
                ->references('id')
                ->on('ms_tickets')
                ->onDelete('cascade');
                
            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->bigInteger('order_id')->unsigned()->index()->nullable();
            $table->foreign('order_id')
                ->references('id')
                ->on('orders');
            $table->longText('message')->nullable();
            $table->text('attachment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_tickets_details');
    }
}
