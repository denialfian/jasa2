<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_invites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('technicians_id')->unsigned()->index();
            $table->bigInteger('team_id')->unsigned()->index();
            $table->foreign('team_id')
                ->references('id')
                ->on('ms_tech_teams');
            $table->enum('type', ['invite', 'request']);
            $table->string('email', 50);
            $table->string('accept_token');
            $table->string('deny_token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_invites');
    }
}
