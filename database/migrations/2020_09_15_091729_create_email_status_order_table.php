<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailStatusOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_status_order', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->longText('content');
            $table->bigInteger('orders_statuses_id')->unsigned()->index();
            $table->foreign('orders_statuses_id')
                ->references('id')
                ->on('orders_statuses');
            $table->tinyInteger('type')->comment('1 = customer, 2 = technicians');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_status_order');
    }
}
