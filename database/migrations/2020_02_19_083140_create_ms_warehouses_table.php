<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsWarehousesTable extends Migration
{
    /**
     *
     *
     *
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_warehouses', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->text('address');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_warehouses');
    }
}
