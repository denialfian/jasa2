<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50)->comment('Nama Vendor');
            $table->string('code', 30)->comment('Kode Vendor');
            $table->string('office_email', 50)->comment('Alamat Surel');
            $table->enum('is_business_type', array(0,1))->default(0)->comment('Perusahaan Kena Pajak atau Bukan Kena Pajak');
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
