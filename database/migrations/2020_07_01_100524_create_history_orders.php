<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('orders_id')->unsigned()->index();
            $table->foreign('orders_id')
                ->references('id')
                ->on('orders');
            $table->bigInteger('orders_statuses_id')->unsigned()->index();
            $table->foreign('orders_statuses_id')
                ->references('id')
                ->on('orders_statuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_orders');
    }
}
