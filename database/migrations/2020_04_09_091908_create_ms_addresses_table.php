<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('address');
            $table->tinyInteger('is_main')->default(0);
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('location_customer')->nullable();
            $table->string('location_google')->nullable();
            $table->string('province')->nullable();
            $table->string('cities')->nullable();
            $table->string('phone1')->nullable();
            $table->string('phone2')->nullable();
            $table->string('is_teknisi')->nullable();
            $table->timestamps();
            $table->softDeletes();



            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->bigInteger('ms_city_id')->unsigned()->index()->nullable();
            $table->foreign('ms_city_id')
                ->references('id')
                ->on('ms_cities');

            $table->bigInteger('ms_district_id')->unsigned()->index()->nullable();
            $table->foreign('ms_district_id')
                ->references('id')
                ->on('ms_districts');

            $table->bigInteger('ms_village_id')->unsigned()->index()->nullable();
            $table->foreign('ms_village_id')
                ->references('id')
                ->on('ms_villages');

            $table->bigInteger('ms_zipcode_id')->unsigned()->index()->nullable();
            $table->foreign('ms_zipcode_id')
                ->references('id')
                ->on('ms_zipcodes');

            $table->bigInteger('ms_address_type_id')->unsigned()->index();
            $table->foreign('ms_address_type_id')
                ->references('id')
                ->on('address_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_addresses');
    }
}
