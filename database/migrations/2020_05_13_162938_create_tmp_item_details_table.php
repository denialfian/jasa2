<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmpItemDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_item_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('inventory_id')->nullable();
            $table->integer('orders_id')->nullable();
            $table->integer('quantity')->nullable();
            $table->double('price')->nullable();
            $table->text('name_product')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_details');
    }
}
