<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechnicianSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technician_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('order_id')->unsigned()->index()->nullable();
            $table->foreign('order_id')
                ->references('id')
                ->on('orders');
            $table->tinyInteger('is_deleted')->nullable();
            $table->bigInteger('technician_id')->unsigned()->index()->nullable();
            $table->foreign('technician_id')
                ->references('id')
                ->on('technicians')
                ->onDelete('cascade');
            $table->date('schedule_date')->nullable();
            $table->timestamp('start_work')->nullable();
            $table->timestamp('end_work')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technician_schedules');
    }
}
