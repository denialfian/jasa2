<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatchShipmentHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch_shipment_histories', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('batch_id')->unsigned()->index();
            $table->foreign('batch_id')
                ->references('id')
                ->on('batches');
            $table->bigInteger('batch_status_shipment_id')->unsigned()->index();
            $table->foreign('batch_status_shipment_id')
                ->references('id')
                ->on('batch_status_shipments');
            $table->dateTime('datetime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch_shipment_histories');
    }
}
