<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsAdditionalInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_additional_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->string('picture')->nullable();
            
            $table->timestamps();

            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->bigInteger('ms_religion_id')->unsigned()->index()->nullable();
            $table->foreign('ms_religion_id')
                ->references('id')
                ->on('religions');

            $table->bigInteger('ms_marital_id')->unsigned()->index()->nullable();
            $table->foreign('ms_marital_id')
                ->references('id')
                ->on('maritals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_additional_infos');
    }
}
