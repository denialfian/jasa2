<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMsRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('users_id')->unsigned()->index();
            $table->foreign('users_id')
                ->references('id')
                ->on('users');
            $table->bigInteger('ms_supports_id')->unsigned()->index();
            $table->foreign('ms_supports_id')
                ->references('id')
                ->on('ms_supports');
            $table->bigInteger('ms_tickets_id')->unsigned()->index();
            $table->foreign('ms_tickets_id')
                ->references('id')
                ->on('ms_tickets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_requests');
    }
}
