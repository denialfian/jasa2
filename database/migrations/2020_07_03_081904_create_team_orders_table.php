<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('team_id')->unsigned()->index();
            $table->foreign('team_id')
                ->references('id')
                ->on('ms_tech_teams');
            $table->bigInteger('orders_id')->unsigned()->index();
            $table->foreign('orders_id')
                ->references('id')
                ->on('orders');
            $table->bigInteger('service_details_id')->unsigned()->index();
            $table->foreign('service_details_id')
                    ->references('id')
                    ->on('service_details');
            $table->bigInteger('ms_tickets_id')->unsigned()->index();
            $table->foreign('ms_tickets_id')
                    ->references('id')
                    ->on('ms_tickets');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_orders');
    }
}
