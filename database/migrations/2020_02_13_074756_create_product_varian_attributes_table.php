<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVarianAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_varian_attributes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->bigInteger('product_varian_id')->unsigned()->index();
            $table->foreign('product_varian_id')
                ->references('id')
                ->on('product_varians')
                ->onDelete('cascade');

            $table->bigInteger('product_attribute_id')->unsigned()->index();
            $table->foreign('product_attribute_id')
                ->references('id')
                ->on('product_attributes');

            $table->bigInteger('product_attribute_term_id')->unsigned()->index();
            $table->foreign('product_attribute_term_id')
                ->references('id')
                ->on('product_attribute_terms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_varian_attributes');
    }
}
