<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_brands', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50)->comment("Nama brand ");
            $table->string('code', 30)->comment("Kode alias brand");
            $table->timestamps();
            $table->softDeletes();

            $table->bigInteger('product_part_category_id')->unsigned()->index();
            $table->foreign('product_part_category_id')
                ->references('id')
                ->on('product_part_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_brands');
    }
}
