<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopupWalletTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topup_wallet', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('nominal');
            $table->text('note');
            $table->timestamp('topup_date');
            $table->integer('status');
            $table->integer('user_id');
            $table->integer('status_transfer_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topup_wallet');
    }
}
