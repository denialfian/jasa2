<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Master\Vendor;
use App\User;
use Faker\Generator as Faker;

$factory->define(Vendor::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'office_email' => $faker->email,
        'code' => $faker->name,
        'is_business_type' => 1,
        // 'user_id' => factory(User::class)->create()->id
        'user_id' => 1
    ];
});
