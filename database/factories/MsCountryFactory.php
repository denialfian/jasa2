<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Master\MsCountry;
use Faker\Generator as Faker;

$factory->define(MsCountry::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'code' => $faker->name
    ];
});
