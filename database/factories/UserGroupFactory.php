<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Master\UserGroup;
use App\User;
use Faker\Generator as Faker;

$factory->define(UserGroup::class, function (Faker $faker) {
    return [
        'business_type' => $faker->name,
        'property_name' => $faker->name,
        'user_id' => factory(User::class)->create()->id
    ];
});
