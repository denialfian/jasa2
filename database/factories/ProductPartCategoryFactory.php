<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Product\ProductPartCategory;
use Faker\Generator as Faker;

$factory->define(ProductPartCategory::class, function (Faker $faker) {
    return [
        'part_category' => $faker->name,
        'part_code' => $faker->name,
    ];
});
