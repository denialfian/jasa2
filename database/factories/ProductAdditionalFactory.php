<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Product\ProductAdditional;
use Faker\Generator as Faker;

$factory->define(ProductAdditional::class, function (Faker $faker) {
    return [
        'additional_code' => $faker->name
    ];
});
