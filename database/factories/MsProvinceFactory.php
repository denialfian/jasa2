<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Master\MsCountry;
use App\Model\Master\MsProvince;
use Faker\Generator as Faker;

$factory->define(MsProvince::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'iso_code' => $faker->word,
        'ms_country_id' => factory(MsCountry::class)->create()->id
    ];
});
