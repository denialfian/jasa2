<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Product\ProductAttribute;
use App\Model\Product\ProductAttributeTerm;
use Faker\Generator as Faker;

$factory->define(ProductAttributeTerm::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'product_attribute_id' => factory(ProductAttribute::class)->create()->id
    ];
});
