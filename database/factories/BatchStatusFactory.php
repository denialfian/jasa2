<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Master\BatchStatus;
use Faker\Generator as Faker;

$factory->define(BatchStatus::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
