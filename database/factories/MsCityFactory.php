<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Master\MsCity;
use App\Model\Master\MsProvince;
use Faker\Generator as Faker;

$factory->define(MsCity::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'ms_province_id' => factory(MsProvince::class)->create()->id
    ];
});
