<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Master\MsCity;
use App\Model\Master\MsDistrict;
use Faker\Generator as Faker;

$factory->define(MsDistrict::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'ms_city_id' => factory(MsCity::class)->create()->id
    ];
});
