<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Product\ProductStatus;
use Faker\Generator as Faker;

$factory->define(ProductStatus::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
