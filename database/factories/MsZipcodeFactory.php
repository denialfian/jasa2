<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Master\MsDistrict;
use App\Model\Master\MsZipcode;
use Faker\Generator as Faker;

$factory->define(MsZipcode::class, function (Faker $faker) {
    return [
        'zip_no' => $faker->name,
        'ms_district_id' => factory(MsDistrict::class)->create()->id
    ];
});
