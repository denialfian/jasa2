<?php

use App\Helpers\CsvToArray;
use App\Model\Master\MsAddress;
use App\Model\Master\MsCity;
use App\Model\Master\MsCountry;
use App\Model\Master\MsDistrict;
use App\Model\Master\MsProvince;
use App\Model\Master\MsVillage;
use App\Model\Master\MsWarehouse;
use App\Model\Master\MsZipcode;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class AddressSeeder extends Seeder
{
    public $all = false;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {
            // insert countries
            MsCountry::insert($this->dataCountry());
            // provinsi
            MsProvince::insert($this->dataProvinsi());
            // city
            MsCity::insert($this->dataCity());
            // distrik
            MsDistrict::insert($this->dataDistrik());
            // vilage
            $this->village();
            // zip kode
            $this->dataZipcode();
            // warehouse
            $this->dataWarehouse();
            $this->userAddress();
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            dd($e);
        }
    }

    public function dataCountry()
    {
        $countries = collect($this->countries());

        return $countries->map(function ($name, $code) {
            return [
                'name' => $name,
                'code' => $code,
            ];
        })->toArray();
    }

    public function dataProvinsi()
    {
        $now = Carbon::now();

        $provinces = $this->provinces();

        $indo = MsCountry::where('name', 'Indonesia')->first();

        return $provinces->map(function ($item) use ($indo) {
            return [
                'id' => $item['id'],
                'name' => $item['name'],
                'meta' => json_encode(['lat' => $item['lat'], 'long' => $item['long']]),
                'ms_country_id' => $indo->id,
            ];
        })->toArray();
    }

    public function dataCity()
    {
        $cities = $this->cities();

        return $cities->map(function ($item) {
            return [
                'id' => $item['id'],
                'name' => $item['name'],
                'meta' => json_encode(['lat' => $item['lat'], 'long' => $item['long']]),
                'ms_province_id' => $item['province_id'],
            ];
        })->toArray();
    }

    public function dataDistrik()
    {
        $distrik = $this->distrik();

        if ($this->all) {
            return $distrik->map(function ($item) {
                return [
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'meta' => json_encode(['lat' => $item['lat'], 'long' => $item['long']]),
                    'ms_city_id' => $item['city_id'],
                ];
            })->toArray();
        } else {
            return $distrik->map(function ($item) {
                return [
                    'id' => $item['id'],
                    'name' => $item['name'],
                    'meta' => $item['meta'],
                    'ms_city_id' => $item['ms_city_id'],
                ];
            })->toArray();
        }
    }

    public function dataZipcode()
    {
        if ($this->all) {
            $csv = new CsvToArray();
            $resourceFiles = File::allFiles(base_path('resources/csv/zipcode/comma/split'));
            foreach ($resourceFiles as $file) {
                $header = ['id', 'urban', 'sub_district', 'city', 'province_code', 'postal_code'];
                $data = $csv->csv_to_array($file->getRealPath(), $header);

                $hasil = [];
                foreach ($data as $key => $item) {
                    $dis = Cache::get($item['sub_district'], function () use ($item) {
                        return MsDistrict::where('name', $item['sub_district'])->first();
                    });
                    if ($dis !== null) {
                        $hasil[] = [
                            'zip_no' => $item['postal_code'],
                            'ms_district_id' => $dis->id
                        ];
                    }
                }

                MsZipcode::insert($hasil);
            }
        } else {
            $ms_zipcodes = array(
                array('id' => '55625', 'zip_no' => '14240', 'created_at' => NULL, 'updated_at' => NULL, 'deleted_at' => NULL, 'ms_district_id' => '3175050'),
                array('id' => '55626', 'zip_no' => '14240', 'created_at' => NULL, 'updated_at' => NULL, 'deleted_at' => NULL, 'ms_district_id' => '3175050'),
                array('id' => '55675', 'zip_no' => '14250', 'created_at' => NULL, 'updated_at' => NULL, 'deleted_at' => NULL, 'ms_district_id' => '3175050')
            );
            MsZipcode::insert($ms_zipcodes);
        }
    }

    // =============================================
    public function provinces()
    {
        $csv = new CsvToArray();
        $file = base_path('resources/csv/provinces.csv');
        $header = ['id', 'name', 'lat', 'long'];
        $data = $csv->csv_to_array($file, $header);
        return collect($data);
    }

    public function cities()
    {
        $Csv = new CsvToArray();
        $file = base_path('resources/csv/cities.csv');
        $header = ['id', 'province_id', 'name', 'lat', 'long'];
        $data = $Csv->csv_to_array($file, $header);

        return collect($data);
    }

    public function distrik()
    {
        if ($this->all) {
            $csv = new CsvToArray();
            $file = base_path('resources/csv/districts.csv');
            $header = ['id', 'city_id', 'name', 'lat', 'long'];
            $data = $csv->csv_to_array($file, $header);
            return collect($data);
        } else {
            $ms_districts = array(
                array('id' => '3175010', 'name' => 'PENJARINGAN', 'meta' => '{"lat":"-6.128631","long":"106.8030892"}', 'created_at' => NULL, 'updated_at' => NULL, 'deleted_at' => NULL, 'ms_city_id' => '3175'),
                array('id' => '3175020', 'name' => 'PADEMANGAN', 'meta' => '{"lat":"-6.1325959","long":"106.8384873"}', 'created_at' => NULL, 'updated_at' => NULL, 'deleted_at' => NULL, 'ms_city_id' => '3175'),
                array('id' => '3175030', 'name' => 'TANJUNG PRIOK', 'meta' => '{"lat":"-6.1320555","long":"106.8714848"}', 'created_at' => NULL, 'updated_at' => NULL, 'deleted_at' => NULL, 'ms_city_id' => '3175'),
                array('id' => '3175040', 'name' => 'KOJA', 'meta' => '{"lat":"-6.1176635","long":"106.9063491"}', 'created_at' => NULL, 'updated_at' => NULL, 'deleted_at' => NULL, 'ms_city_id' => '3175'),
                array('id' => '3175050', 'name' => 'KELAPA GADING', 'meta' => '{"lat":"-6.1604549","long":"106.9054618"}', 'created_at' => NULL, 'updated_at' => NULL, 'deleted_at' => NULL, 'ms_city_id' => '3175'),
                array('id' => '3175060', 'name' => 'CILINCING', 'meta' => '{"lat":"-6.1214284","long":"106.947666"}', 'created_at' => NULL, 'updated_at' => NULL, 'deleted_at' => NULL, 'ms_city_id' => '3175')
            );
            return collect($ms_districts);
        }
    }

    public function village()
    {
        if ($this->all) {
            $csv = new CsvToArray();
            $resourceFiles = File::allFiles(base_path('resources/csv/villages'));
            foreach ($resourceFiles as $file) {
                $header = ['id', 'district_id', 'name', 'lat', 'long'];
                $data = $csv->csv_to_array($file->getRealPath(), $header);

                $data = collect($data);

                $villages = $data->map(function ($item) {
                    return [
                        'id' => $item['id'],
                        'name' => $item['name'],
                        'meta' => json_encode(['lat' => $item['lat'], 'long' => $item['long']]),
                        'ms_district_id' => $item['district_id'],
                    ];
                })->toArray();

                MsVillage::insert($villages);
            }
        } else {
            $ms_villages = array(
                array('id' => '3175050001', 'name' => 'KELAPA GADING BARAT', 'meta' => '{"lat":"-6.1561984","long":"106.8974964"}', 'created_at' => NULL, 'updated_at' => NULL, 'deleted_at' => NULL, 'ms_district_id' => '3175050'),
                array('id' => '3175050002', 'name' => 'KELAPA GADING TIMUR', 'meta' => '{"lat":"-6.166211","long":"106.9033981"}', 'created_at' => NULL, 'updated_at' => NULL, 'deleted_at' => NULL, 'ms_district_id' => '3175050'),
                array('id' => '3175050003', 'name' => 'PEGANGSAAN DUA', 'meta' => '{"lat":"-6.1658278","long":"106.9152021"}', 'created_at' => NULL, 'updated_at' => NULL, 'deleted_at' => NULL, 'ms_district_id' => '3175050')
            );

            MsVillage::insert($ms_villages);
        }
    }

    public function countries()
    {
        $arr = array(
            'AF' => 'Afghanistan',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'DS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua and Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia and Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CD' => 'Democratic Republic of the Congo',
            'CG' => 'Republic of Congo',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'HR' => 'Croatia (Hrvatska)',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'TP' => 'East Timor',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands (Malvinas)',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'FX' => 'France, Metropolitan',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GK' => 'Guernsey',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard and Mc Donald Islands',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'IM' => 'Isle of Man',
            'ID' => 'Indonesia',
            'IR' => 'Iran (Islamic Republic of)',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'CI' => 'Ivory Coast',
            'JE' => 'Jersey',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KP' => 'Korea, Democratic People\'s Republic of',
            'KR' => 'Korea, Republic of',
            'XK' => 'Kosovo',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Lao People\'s Democratic Republic',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libyan Arab Jamahiriya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macau',
            'MK' => 'North Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'TY' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia, Federated States of',
            'MD' => 'Moldova, Republic of',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'AN' => 'Netherlands Antilles',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestine',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            'RW' => 'Rwanda',
            'KN' => 'Saint Kitts and Nevis',
            'LC' => 'Saint Lucia',
            'VC' => 'Saint Vincent and the Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome and Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'SS' => 'South Sudan',
            'GS' => 'South Georgia South Sandwich Islands',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SH' => 'St. Helena',
            'PM' => 'St. Pierre and Miquelon',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard and Jan Mayen Islands',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania, United Republic of',
            'TH' => 'Thailand',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad and Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks and Caicos Islands',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UM' => 'United States minor outlying islands',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VA' => 'Vatican City State',
            'VE' => 'Venezuela',
            'VN' => 'Vietnam',
            'VG' => 'Virgin Islands (British)',
            'VI' => 'Virgin Islands (U.S.)',
            'WF' => 'Wallis and Futuna Islands',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe'
        );

        return $arr;
    }

    public function dataWarehouse()
    {
        $village = MsVillage::first();

        MsWarehouse::create([
            // 'ms_village_id'      => $village->id,
            // 'ms_district_id'     => $village->district->id,
            // 'ms_city_id'         => $village->district->city->id,
            // 'ms_zipcode_id'      => $village->district->zipcode->id,
            'name' => 'Gudang A',
            'address' => 'Jl. Pancuran No. 56',
        ]);
    }

    public function userAddress()
    {
        $village = MsVillage::first();
        $users = User::all();
        foreach ($users as $user) {
            $dataAddress = [
                'address'            => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error eaque modi corporis quas iste possimus doloribus cumque eius vero quos? Voluptas, velit dignissimos voluptatum voluptatibus fugiat dolor ad beatae amet!',
                'is_main'            => 1,
                'user_id'            => $user->id,
                'phone1'             => 1234,
                'phone2'             => 1234,
                'ms_address_type_id' => 1,
                'ms_village_id'      => $village->id,
                'ms_district_id'     => $village->district->id,
                'ms_city_id'         => $village->district->city->id,
                'ms_zipcode_id'      => $village->district->zipcode->id,
            ];

            MsAddress::create($dataAddress);
        }
    }
}
