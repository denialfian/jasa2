<?php

use App\Model\GeneralSetting;
use App\Model\Master\AddressType;
use App\Model\Master\Badge;
use App\Model\Master\BankTransfer;
use App\Model\Master\Batch;
use App\Model\Master\BatchItem;
use App\Model\Master\BatchShipmentHistory;
use App\Model\Master\BatchStatus;
use App\Model\Master\BatchStatusShipment;
use App\Model\Master\EmailOther;
use App\Model\Master\EmailStatusOrder;
use App\Model\Master\Ewallet;
use App\Model\Master\Marital;
use App\Model\Master\MsPriceService;
use App\Model\Master\MsProduct;
use App\Model\Master\MsProductCategory;
use App\Model\Master\MsProductModel;
use App\Model\Master\MsServiceStatus;
use App\Model\Master\MsServicesType;
use App\Model\Master\MsSymptom;
use App\Model\Master\OrdersStatus;
use App\Model\Master\ProductGroup;
use App\Model\Master\Religion;
use App\Model\Master\RepairCode;
use App\Model\Master\Services;
use App\Model\Master\SymptomCode;
use App\Model\Master\TechnicianSparepart;
use App\Model\Master\UnitTypes;
use App\Model\Master\Vendor;
use App\Model\Product\ProductAdditional;
use App\Model\Product\ProductAttribute;
use App\Model\Product\ProductAttributeTerm;
use App\Model\Product\ProductBrand;
use App\Model\Product\ProductCategoryCombine;
use App\Model\Product\ProductPartCategory;
use App\Model\Product\ProductStatus;
use App\Model\Product\ProductVarian;
use App\Model\Product\ProductVarianAttribute;
use App\Model\Product\ProductVendor;
use App\Model\Technician\JobTitle;
use App\Model\Technician\JobTitleCategory;
use App\Model\Technician\Technician;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        try {
            $this->dataMarital();
            $this->dataAddressType();
            $this->dataPartCategory();
            $this->dataReligion();
            $this->dataAttribute();
            $this->dataProductAdditional();
            $this->dataVendor();
            $this->dataCategory();
            $this->dataBrand();
            $this->dataModel();
            $this->dataBatchStatus();
            $this->dataBatchStatusShipment();
            $this->dataProduk();
            $this->dataUnitTypes();
            $this->dataServices();
            $this->dataSymptoms();
            $this->dataServicesTypes();
            $this->dataOrderStatuses();
            $this->dataaServiceStatuses();
            $this->dataJobTitleCategory();
            $this->dataBatch();
            $this->dataTeknisiPrice();
            SymptomCode::create([
                'name' => 'abc',
            ]);

            RepairCode::create([
                'name' => '12345'
            ]);

            $this->dataEmail();

            $this->randomData();

            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            dd($e);
        }
    }

    public function randomData()
    {
        $bank_transfer = array(
            array('id' => '1', 'bank_name' => NULL, 'name' => NULL, 'virtual_code' => NULL, 'desc' => NULL, 'icon' => NULL, 'created_at' => NULL, 'updated_at' => '2020-11-16 15:50:33', 'is_active' => '1', 'is_parent' => NULL, 'payment_name' => 'Bank Transfer'),
            array('id' => '2', 'bank_name' => NULL, 'name' => NULL, 'virtual_code' => NULL, 'desc' => NULL, 'icon' => NULL, 'created_at' => NULL, 'updated_at' => '2020-11-16 14:16:25', 'is_active' => '0', 'is_parent' => NULL, 'payment_name' => 'Midtrans'),
            array('id' => '3', 'bank_name' => 'BCA', 'name' => 'PT.ASTech Indonesiaa', 'virtual_code' => '098928922736', 'desc' => 'asddd', 'icon' => '5f681dfe06a1c.jpg', 'created_at' => '2020-09-21 10:29:02', 'updated_at' => '2020-09-21 10:39:10', 'is_active' => NULL, 'is_parent' => '1', 'payment_name' => NULL),
            array('id' => '4', 'bank_name' => 'BNI', 'name' => 'PT.ASTech SECONDARY', 'virtual_code' => '12321312123', 'desc' => 'asdasd', 'icon' => '5f681fce2b958.png', 'created_at' => '2020-09-21 10:36:46', 'updated_at' => '2020-09-21 10:39:20', 'is_active' => NULL, 'is_parent' => '1', 'payment_name' => NULL)
        );

        BankTransfer::insert($bank_transfer);

        $general_settings = array(
            array('id' => '1', 'name' => 'commission_type_rate', 'value' => '3', 'desc' => 'Commision Type', 'created_at' => NULL, 'updated_at' => '2020-11-16 16:00:34', 'type' => '1'),
            array('id' => '2', 'name' => 'pinalty_before_approve', 'value' => '25000', 'desc' => 'Commission Before Approve', 'created_at' => NULL, 'updated_at' => '2020-11-16 16:00:34', 'type' => '2'),
            array('id' => '3', 'name' => 'pinalty_after_approve_under_24_hours', 'value' => '50', 'desc' => 'Penalty Order Under 24 Hours', 'created_at' => '2020-08-11 16:00:19', 'updated_at' => '2020-11-16 16:00:35', 'type' => '1'),
            array('id' => '4', 'name' => 'penalty_above_24_hours', 'value' => '100', 'desc' => 'Penalty Above 24 hours', 'created_at' => '2020-08-11 16:00:19', 'updated_at' => '2020-11-16 16:00:35', 'type' => '1'),
            array('id' => '5', 'name' => 'timer_order_setting', 'value' => '22', 'desc' => 'Timer Order Setting', 'created_at' => NULL, 'updated_at' => '2020-11-16 16:00:35', 'type' => 'hours'),
            array('id' => '6', 'name' => 'commission_penalty_setting', 'value' => '5', 'desc' => 'Commission Penalty', 'created_at' => NULL, 'updated_at' => '2020-11-16 16:00:35', 'type' => '1'),
            array('id' => '7', 'name' => 'server_key_midtrans', 'value' => 'SB-Mid-server-7Q3bB3s0bd8oTo9iZVGghcqA', 'desc' => 'Hours of Auto Cancel If Less Balance Order', 'created_at' => NULL, 'updated_at' => '2020-09-29 13:35:28', 'type' => 'hours'),
            array('id' => '8', 'name' => 'client_keys', 'value' => 'SB-Mid-client-oBzvWZCgQ3_WlRA_', 'desc' => NULL, 'created_at' => NULL, 'updated_at' => '2020-09-29 13:35:28', 'type' => 'days'),
            array('id' => '9', 'name' => 'timer_expired_midtrans', 'value' => '20', 'desc' => NULL, 'created_at' => NULL, 'updated_at' => NULL, 'type' => 'hours'),
            array('id' => '10', 'name' => 'auto_cancel_and_jobs_completed', 'value' => '1', 'desc' => 'Hours of Auto Cancel and Jobs Completed ', 'created_at' => NULL, 'updated_at' => '2020-11-16 16:00:35', 'type' => 'hours'),
            array('id' => '11', 'name' => 'aggrement_title', 'value' => 'Modal Titlessss', 'desc' => 'Aggrement Title', 'created_at' => NULL, 'updated_at' => NULL, 'type' => NULL),
            array('id' => '12', 'name' => 'garansi_due_date', 'value' => '14', 'desc' => 'Due date claim warranty', 'created_at' => NULL, 'updated_at' => '2020-11-16 16:00:35', 'type' => 'days'),
            array('id' => '13', 'name' => 'agreement_text', 'value' => '<p><strong>Terms and Conditions</strong></p><p>Welcome to astech!</p><p>These terms and conditions outline the rules and regulations for the use of Astech\'s Website, located at www.astech.co.id.</p><p>By accessing this website we assume you accept these terms and conditions. Do not continue to use astech if you do not agree to take all of the terms and conditions stated on this page.</p><p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: "Client", "You" and "Your" refers to you, the person log on this website and compliant to the Company’s terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves. All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services, in accordance with and subject to, prevailing law of Netherlands. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p><p><strong>Cookies</strong></p><p>We employ the use of cookies. By accessing astech, you agreed to use cookies in agreement with the Astech\'s Privacy Policy.</p><p>Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.</p><p><strong>License</strong></p><p>Unless otherwise stated, Astech and/or its licensors own the intellectual property rights for all material on astech. All intellectual property rights are reserved. You may access this from astech for your own personal use subjected to restrictions set in these terms and conditions.</p><p>You must not:</p><ul><li>Republish material from astech</li><li>Sell, rent or sub-license material from astech</li><li>Reproduce, duplicate or copy material from astech</li><li>Redistribute content from astech</li></ul><p>This Agreement shall begin on the date hereof. Our Terms and Conditions were created with the help of the&nbsp;</p><p>Parts of this website offer an opportunity for users to post and exchange opinions and information in certain areas of the website. Astech does not filter, edit, publish or review Comments prior to their presence on the website. Comments do not reflect the views and opinions of Astech,its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions. To the extent permitted by applicable laws, Astech shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.</p><p>Astech reserves the right to monitor all Comments and to remove any Comments which can be considered inappropriate, offensive or causes breach of these Terms and Conditions.</p><p>You warrant and represent that:</p><ul><li>You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;</li><li>The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;</li><li>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material which is an invasion of privacy</li><li>The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</li></ul><p>You hereby grant Astech a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.</p><p><strong>Hyperlinking to our Content</strong></p><p>The following organizations may link to our Website without prior written approval:</p><ul><li>Government agencies;</li><li>Search engines;</li><li>News organizations;</li><li>Online directory distributors may link to our Website in the same manner as they hyperlink to the Websites of other listed businesses; and</li><li>System wide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.</li></ul><p>These organizations may link to our home page, to publications or to other Website information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products and/or services; and (c) fits within the context of the linking party’s site.</p><p>We may consider and approve other link requests from the following types of organizations:</p><ul><li>commonly-known consumer and/or business information sources;</li><li>dot.com community sites;</li><li>associations or other groups representing charities;</li><li>online directory distributors;</li><li>internet portals;</li><li>accounting, law and consulting firms; and</li><li>educational institutions and trade associations.</li></ul><p>We will approve link requests from these organizations if we decide that: (a) the link would not make us look unfavorably to ourselves or to our accredited businesses; (b) the organization does not have any negative records with us; (c) the benefit to us from the visibility of the hyperlink compensates the absence of Astech; and (d) the link is in the context of general resource information.</p><p>These organizations may link to our home page so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and (c) fits within the context of the linking party’s site.</p><p>If you are one of the organizations listed in paragraph 2 above and are interested in linking to our website, you must inform us by sending an e-mail to Astech. Please include your name, your organization name, contact information as well as the URL of your site, a list of any URLs from which you intend to link to our Website, and a list of the URLs on our site to which you would like to link. Wait 2-3 weeks for a response.</p><p>Approved organizations may hyperlink to our Website as follows:</p><ul><li>By use of our corporate name; or</li><li>By use of the uniform resource locator being linked to; or</li><li>By use of any other description of our Website being linked to that makes sense within the context and format of content on the linking party’s site.</li></ul><p>No use of Astech\'s logo or other artwork will be allowed for linking absent a trademark license agreement.</p><p><strong>iFrames</strong></p><p>Without prior approval and written permission, you may not create frames around our Webpages that alter in any way the visual presentation or appearance of our Website.</p><p><strong>Content Liability</strong></p><p>We shall not be hold responsible for any content that appears on your Website. You agree to protect and defend us against all claims that is rising on your Website. No link(s) should appear on any Website that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p><p><strong>Your Privacy</strong></p><p>Please read Privacy Policy</p><p><strong>Reservation of Rights</strong></p><p>We reserve the right to request that you remove all links or any particular link to our Website. You approve to immediately remove all links to our Website upon request. We also reserve the right to amen these terms and conditions and it’s linking policy at any time. By continuously linking to our Website, you agree to be bound to and follow these linking terms and conditions.</p><p><strong>Removal of links from our website</strong></p><p>If you find any link on our Website that is offensive for any reason, you are free to contact and inform us any moment. We will consider requests to remove links but we are not obligated to or so or to respond to you directly.</p><p>We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material on the website is kept up to date.</p><p><strong>Disclaimer</strong></p><p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website. Nothing in this disclaimer will:</p><ul><li>limit or exclude our or your liability for death or personal injury;</li><li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li><li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li><li>exclude any of our or your liabilities that may not be excluded under applicable law.</li></ul><p>The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.</p><p>As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>', 'desc' => 'Argumen Setting', 'created_at' => NULL, 'updated_at' => '2020-10-09 08:15:47', 'type' => NULL),
            array('id' => '14', 'name' => 'facebook', 'value' => 'https://facebook.com', 'desc' => 'Facebook', 'created_at' => NULL, 'updated_at' => '2020-10-15 08:39:48', 'type' => NULL),
            array('id' => '15', 'name' => 'youtube', 'value' => 'https://youtube.com', 'desc' => 'youtube', 'created_at' => NULL, 'updated_at' => '2020-10-15 08:39:48', 'type' => NULL),
            array('id' => '16', 'name' => 'twiter', 'value' => 'https://twitter.com', 'desc' => 'twiter', 'created_at' => NULL, 'updated_at' => '2020-10-15 08:39:48', 'type' => NULL),
            array('id' => '17', 'name' => 'instagram', 'value' => 'https://instagram.com', 'desc' => 'instagram', 'created_at' => NULL, 'updated_at' => '2020-10-15 08:39:48', 'type' => NULL),
            array('id' => '18', 'name' => 'email', 'value' => 'astech@email.com', 'desc' => 'email', 'created_at' => NULL, 'updated_at' => '2020-10-15 08:39:48', 'type' => NULL),
            array('id' => '19', 'name' => 'addreses', 'value' => '<p>PT ASTech</p><p><br>3rd Floor WISMA SSK BUILDING,</p><p><br>Daan Mogot Rd No.Km. 11, RT.5/RW.4,</p><p><br>Kedaung Kali Angke, Cengkareng, West Jakarta City,</p><p><br>Jakarta 11710</p>', 'desc' => 'addreses', 'created_at' => NULL, 'updated_at' => '2020-10-15 08:39:48', 'type' => NULL),
            array('id' => '20', 'name' => 'contact_us', 'value' => '(021) 123456789', 'desc' => 'Contact Us', 'created_at' => NULL, 'updated_at' => '2020-10-15 08:39:49', 'type' => NULL),
            array('id' => '21', 'name' => 'agreement_title', 'value' => 'Term & Condition', 'desc' => 'Agreement Title', 'created_at' => NULL, 'updated_at' => '2020-10-09 08:15:47', 'type' => NULL),
            array('id' => '22', 'name' => 'agreement_order_title', 'value' => 'Cancellation Warning', 'desc' => 'Title', 'created_at' => NULL, 'updated_at' => '2020-10-23 13:48:52', 'type' => NULL),
            array('id' => '23', 'name' => 'agreement_order_text', 'value' => '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p><p>&nbsp;</p><p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p><p>&nbsp;</p><p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</p>', 'desc' => 'Desc', 'created_at' => NULL, 'updated_at' => '2020-10-23 13:48:52', 'type' => NULL),
            array('id' => '25', 'name' => 'google_maps_key_setting', 'value' => 'AIzaSyCQmaBMz3O8uLV0qlIczUwGDHkhtvxRRJ8', 'desc' => 'Google Maps Key', 'created_at' => '2020-11-04 11:18:22', 'updated_at' => '2020-11-11 11:21:12', 'type' => NULL),
            array('id' => '26', 'name' => 'about_website_setting', 'value' => 'this si blablabla', 'desc' => 'About Website', 'created_at' => NULL, 'updated_at' => NULL, 'type' => NULL),
            array('id' => '27', 'name' => 'term_order_completed_title', 'value' => 'Warranty Warning', 'desc' => 'Title', 'created_at' => NULL, 'updated_at' => '2020-11-12 15:11:58', 'type' => NULL),
            array('id' => '28', 'name' => 'term_order_completed_desc', 'value' => '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</p><p>&nbsp;</p><p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p><p>&nbsp;</p><p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</p>', 'desc' => 'Description', 'created_at' => NULL, 'updated_at' => '2020-11-12 15:11:59', 'type' => NULL)
        );

        GeneralSetting::insert($general_settings);

        $email_other = array(
            array('id' => '1', 'name_module' => 'warranty_claim', 'description' => 'Notif Claim Garansi', 'subject' => 'Klaim Garansi Customer', 'content' => '<p>asdasd</p>', 'type' => '1', 'created_at' => '2020-09-25 08:53:47', 'updated_at' => '2020-09-16 15:08:12'),
            array('id' => '2', 'name_module' => 'warranty_claim', 'description' => 'Notif Claim Garansi', 'subject' => 'Klaim Garansi Notif Admin', 'content' => '<p>&nbsp;adasd</p>', 'type' => '3', 'created_at' => '2020-09-25 08:53:48', 'updated_at' => '2020-09-16 15:08:12'),
            array('id' => '3', 'name_module' => 'complaint', 'description' => 'Notif Complaint Order', 'subject' => 'Complaint Notif Teknisi', 'content' => '<p>&nbsp;complaint</p>', 'type' => '2', 'created_at' => '2020-09-25 08:54:05', 'updated_at' => NULL),
            array('id' => '4', 'name_module' => 'complaint_solved', 'description' => 'Notif Complaint Solved Order', 'subject' => 'Complaint Solved Notif Admin', 'content' => '<p>Complaint Notif Admin</p>', 'type' => '3', 'created_at' => '2020-10-19 09:11:12', 'updated_at' => NULL),
            array('id' => '5', 'name_module' => 'ticket', 'description' => 'Notif Ticket', 'subject' => 'Astech - New Ticket Notification from Customer', 'content' => '<p>Nama Customer : [{NAME_CUSTOMER}]</p><p>Email Customer : [{EMAIL_CUSTOMER}]</p><p>Ticket Code : #[{TICKET_NO}] - Status Ticket : [{STATUS_TICKET}]</p><p>[{SUBJECT_TICKET}],</p><p>[{DESCRIPTION_TICKET}]</p>', 'type' => '3', 'created_at' => '2020-09-25 14:09:08', 'updated_at' => '2020-09-25 14:09:08'),
            array('id' => '6', 'name_module' => 'review', 'description' => 'Notif Review After Jobs Completed', 'subject' => 'Your Request Service has been completed ! Review this service', 'content' => '<p>Hello <strong>[{NAME_CUSTOMER}]</strong></p><p>Kode Order : <strong>[{ORDER_CODE}]</strong></p><p>Request Job Service anda telah selesai.</p><p>Terima Kasih sudah menggunalan layanan jasa Astech dan mendukung teknisi yang sudah tergabung di layanan ini.</p><p>Berikan ulasan/review agar teknisi bisa lebih meningkatkan layanan service nya.</p>', 'type' => '1', 'created_at' => '2020-09-29 11:23:35', 'updated_at' => '2020-09-29 11:23:35'),
            array('id' => '7', 'name_module' => 'review', 'description' => 'Notif Review After Jobs Completed', 'subject' => 'Your Job Service has been reviewed', 'content' => '<p>Hello <strong>[{NAME_TECHNICIAN}],</strong></p><p>Kode Order : <strong>[{ORDER_CODE}]</strong></p><p>Jobs Service anda telah direview oleh Customer <strong>[{NAME_CUSTOMER}].</strong></p><p>“<strong>[{REVIEW_CUSTOMER}]</strong>” dengan rating “<strong>[{RATING_REVIEW}]</strong>”.</p><p>Cek link dibawah ini untuk melihat review/ulasan tersebut.</p>', 'type' => '2', 'created_at' => '2020-09-29 11:23:35', 'updated_at' => '2020-09-29 11:23:35'),
            array('id' => '8', 'name_module' => 'verify_registration', 'description' => NULL, 'subject' => 'Verify Registration', 'content' => '<p>Verified your Account.</p><p>Hi [{NAME_CUSTOMER}], please verify your account immediately !</p>', 'type' => '1', 'created_at' => '2020-10-19 10:48:32', 'updated_at' => '2020-10-19 10:48:32'),
            array('id' => '9', 'name_module' => 'forgot_password', 'description' => NULL, 'subject' => 'Forgot Password', 'content' => '<p>Forgot Password.</p><p>Hi [{NAME_CUSTOMER}], press the button below to reset the password</p>', 'type' => '1', 'created_at' => '2020-10-19 10:49:40', 'updated_at' => '2020-10-19 10:49:40'),
            array('id' => '10', 'name_module' => 'approved_registration', 'description' => NULL, 'subject' => 'Approved Registration', 'content' => '<p>Your accound Success to accept.</p><p>Conggratulation Your Accoun Has Been Active !</p>', 'type' => '1', 'created_at' => '2020-10-19 14:44:12', 'updated_at' => '2020-10-19 10:47:22'),
            array('id' => '11', 'name_module' => 'rejected_registration', 'description' => NULL, 'subject' => 'Rejected Registration', 'content' => '<p>Your Account has been rejected&nbsp;</p><p>Note : [{NOTE}]</p>', 'type' => '1', 'created_at' => '2020-10-19 14:44:20', 'updated_at' => '2020-10-19 14:00:06')
        );

        EmailOther::insert($email_other);

        Ewallet::create([
            'user_id' => 3,
            'nominal' => 10000000,
            'topup_code' => 121209102
        ]);

        Badge::create([
            'name' => 'VVIP',
            'color' => 'alternate'
        ]);

        // order dump
    }

    public function dataOrder()
    {
    }

    public function dataEmail()
    {
        $email_status_order = array(
            array('id' => '1', 'title' => 'Analyzing asdasd', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '2', 'type' => '1', 'created_at' => '2020-09-14 15:17:18', 'updated_at' => '2020-09-14 15:17:18'),
            array('id' => '2', 'title' => 'Wating Aprroval', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '3', 'type' => '1', 'created_at' => '2020-09-14 13:42:59', 'updated_at' => '2020-09-08 11:12:33'),
            array('id' => '3', 'title' => 'On Working', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '4', 'type' => '1', 'created_at' => '2020-09-14 13:42:59', 'updated_at' => '2020-09-08 11:14:14'),
            array('id' => '4', 'title' => 'Cancel', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '5', 'type' => '1', 'created_at' => '2020-09-14 13:43:00', 'updated_at' => '2020-09-08 11:14:35'),
            array('id' => '5', 'title' => 'Job Done', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '7', 'type' => '1', 'created_at' => '2020-09-14 13:43:01', 'updated_at' => '2020-09-08 11:15:25'),
            array('id' => '6', 'title' => 'Processing', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '9', 'type' => '1', 'created_at' => '2020-09-14 13:43:01', 'updated_at' => '2020-09-08 11:16:10'),
            array('id' => '7', 'title' => 'Jobs Completed', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '10', 'type' => '1', 'created_at' => '2020-09-14 13:43:02', 'updated_at' => '2020-09-08 11:16:33'),
            array('id' => '8', 'title' => 'Jobs Complaint', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '11', 'type' => '1', 'created_at' => '2020-09-14 13:43:02', 'updated_at' => '2020-09-08 11:17:00'),
            array('id' => '9', 'title' => 'Analyzing x', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '2', 'type' => '2', 'created_at' => '2020-09-14 15:17:18', 'updated_at' => '2020-09-14 15:17:18'),
            array('id' => '10', 'title' => 'Wating Aprroval', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '3', 'type' => '2', 'created_at' => '2020-09-14 13:43:10', 'updated_at' => '2020-09-08 11:12:33'),
            array('id' => '11', 'title' => 'On Working', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '4', 'type' => '2', 'created_at' => '2020-09-14 13:43:10', 'updated_at' => '2020-09-08 11:14:14'),
            array('id' => '12', 'title' => 'Cancel', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '5', 'type' => '2', 'created_at' => '2020-09-14 13:43:12', 'updated_at' => '2020-09-08 11:14:35'),
            array('id' => '13', 'title' => 'Job Done', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '7', 'type' => '2', 'created_at' => '2020-09-14 13:43:11', 'updated_at' => '2020-09-08 11:15:25'),
            array('id' => '14', 'title' => 'Processing', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '9', 'type' => '2', 'created_at' => '2020-09-14 13:43:12', 'updated_at' => '2020-09-08 11:16:10'),
            array('id' => '15', 'title' => 'Jobs Completed', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '10', 'type' => '2', 'created_at' => '2020-09-14 13:43:14', 'updated_at' => '2020-09-08 11:16:33'),
            array('id' => '16', 'title' => 'Jobs Complaint', 'content' => '<p>Hallo [{NAME_CUSTOMER}],</p><p>Order Status : &nbsp;[{STATUS_ORDER}]</p><p>Perubahan Status Order akan dikirim ke alamat email : [{EMAIL_CUSTOMER}]</p><p>Nama Teknisi : [{NAME_TECHNICIAN}]</p><p>Schedule : [{SCHEDULE}]</p><p>&nbsp;</p>', 'orders_statuses_id' => '11', 'type' => '2', 'created_at' => '2020-09-14 13:43:13', 'updated_at' => '2020-09-08 11:17:00')
        );

        EmailStatusOrder::insert($email_status_order);
    }

    public function dataaServiceStatuses()
    {
        $status = ['pending', 'awaiting confirmation', 'on progress', 'done', 'cancel'];
        foreach ($status as $key => $name) {
            MsServiceStatus::create([
                'name'        => $name,
                'description' => $name,
            ]);
        }
    }

    public function dataOrderStatuses()
    {
        $status = ['New Order', 'Pending Request / Waiting Confirmation', 'Approved Request / Waiting Payment', 'Success Payment', 'Cancel', 'Reschedule', 'Job Done', 'Pending Payment', 'Processing', 'Completed', 'Jobs Complaint'];
        foreach ($status as $key => $name) {
            OrdersStatus::create([
                'name'        => $name,
                'description' => $name,
            ]);
        }
    }

    public function dataJobTitleCategory()
    {
        $status = ['service', 'operator'];
        foreach ($status as $key => $name) {
            JobTitleCategory::create([
                'name' => $name,
            ]);
        }
    }

    public function dataServices()
    {
        Services::insert([
            [
                'name'       => 'Service Smartphone',
                'slug'       => 'service-smartphone',
                'is_visible' => 1,
                'images'     => null,
            ],
            [
                'name'       => 'Service Laptop',
                'slug'       => 'service-laptop',
                'is_visible' => 1,
                'images'     => null,
            ],
            [
                'name'       => 'Service Tab',
                'slug'       => 'service-tab',
                'is_visible' => 1,
                'images'     => null,
            ],
            [
                'name'       => 'Service TV',
                'slug'       => 'service-tv',
                'is_visible' => 1,
                'images'     => null,
            ],
            [
                'name'       => 'Service AC',
                'slug'       => 'service-ac',
                'is_visible' => 1,
                'images'     => null,
            ],

        ]);
    }

    public function dataSymptoms()
    {
        MsSymptom::insert([
            [
                'ms_services_id' => Services::where('slug', 'service-smartphone')->first()->id,
                'name'           => 'Smartphone Rusak',

            ],
            [
                'ms_services_id' => Services::where('slug', 'service-smartphone')->first()->id,
                'name'           => 'Baterai Smartphone Rusak',

            ],
            [
                'ms_services_id' => Services::where('slug', 'service-laptop')->first()->id,
                'name'           => 'Smartphone Tidak Mau Booting',

            ],
            [
                'ms_services_id' => Services::where('slug', 'service-laptop')->first()->id,
                'name'           => 'Laptop mati total',

            ],
            [
                'ms_services_id' => Services::where('slug', 'service-laptop')->first()->id,
                'name'           => 'Layar Laptop Pecah',

            ],
        ]);
    }

    public function dataServicesTypes()
    {
        MsServicesType::insert([
            [
                'ms_symptoms_id' => MsSymptom::where('name', 'Smartphone Rusak')->first()->id,
                'name'           => 'Smartphone Diperbaiki',

            ],
            [
                'ms_symptoms_id' => MsSymptom::where('name', 'Smartphone Rusak')->first()->id,
                'name'           => 'Ganti Baterai Smartphone',

            ],
            [
                'ms_symptoms_id' => MsSymptom::where('name', 'Smartphone Tidak Mau Booting')->first()->id,
                'name'           => 'Flash Ulang Smartphone',

            ],

        ]);
    }

    public function dataMarital()
    {
        $status = ['menikah', 'belum menikah'];
        foreach ($status as $key => $name) {
            Marital::create([
                'status' => $name,
            ]);
        }
    }

    public function dataAddressType()
    {
        $status = ['rumah', 'kantor', 'apartement'];
        foreach ($status as $key => $name) {
            AddressType::create([
                'name' => $name,
            ]);
        }
    }

    public function dataPartCategory()
    {
        $status = ['Sell', 'BNG'];
        foreach ($status as $key => $name) {
            ProductPartCategory::create([
                'part_category' => $name,
                'part_code'     => $key,
            ]);
        }
    }

    public function dataModel()
    {
        $status = ['klasik', 'high'];
        foreach ($status as $key => $name) {
            MsProductModel::create([
                'name'                   => $name,
                'model_code'             => $key,
                'product_brands_id'      => ProductBrand::first()->id,
                'ms_product_category_id' => MsProductCategory::first()->id,
            ]);
        }
    }

    public function dataBrand()
    {
        $status = ['BJN', 'old shcool'];
        foreach ($status as $key => $name) {
            ProductBrand::create([
                'name'                     => $name,
                'code'                     => $key,
                'product_part_category_id' => ProductPartCategory::first()->id,
            ]);
        }
    }

    public function dataCategory()
    {
        $status = ['elektronik', 'AC', 'rumah tangga'];

        foreach ($status as $key => $name) {
            MsProductCategory::create([
                'name'              => $name,
                'product_cate_code' => $name,
                'slug'              => $name,
            ]);
        }
    }

    public function dataVendor()
    {
        foreach (range(1, 5) as $key => $name) {
            factory(Vendor::class)->create();
        }
    }

    public function dataReligion()
    {
        $status = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Buddha', 'Konghucu'];
        foreach ($status as $key => $name) {
            Religion::create([
                'name' => $name,
            ]);
        }
    }

    public function dataAttribute()
    {
        $warna = ProductAttribute::create([
            'name' => 'warna',
        ]);
        $ukuran = ProductAttribute::create([
            'name' => 'ukuran',
        ]);
        ProductAttributeTerm::insert([
            [
                'name'                 => 'L',
                'product_attribute_id' => $ukuran->id,
            ],
            [
                'name'                 => 'M',
                'product_attribute_id' => $ukuran->id,
            ],
            [
                'name'                 => 'S',
                'product_attribute_id' => $ukuran->id,
            ],
        ]);

        ProductAttributeTerm::insert([
            [
                'name'                 => 'merah',
                'color_code'           => 'merah-1',
                'hex_rgb'              => '#000',
                'product_attribute_id' => $warna->id,
            ],
            [
                'name'                 => 'kuning',
                'color_code'           => 'kuning-1',
                'hex_rgb'              => '#000',
                'product_attribute_id' => $warna->id,
            ],
            [
                'name'                 => 'hijau',
                'color_code'           => 'hijau-1',
                'hex_rgb'              => '#000',
                'product_attribute_id' => $warna->id,
            ],
        ]);
    }

    public function dataProductAdditional()
    {
        $status = ['ADT-002931'];
        foreach ($status as $key => $name) {
            ProductAdditional::create([
                'additional_code' => $name,
            ]);
        }
    }

    public function dataBatchStatus()
    {
        $status = ['baik', 'buruk'];
        foreach ($status as $key => $name) {
            BatchStatus::create([
                'name' => $name,
            ]);
        }
    }

    public function dataBatchStatusShipment()
    {
        BatchStatusShipment::insert([
            [
                'name'        => 'Shipped',
            ],
            [
                'name'        => 'Received',
            ],
            [
                'name'        => 'Returned',
            ],
        ]);
    }

    public function dataUnitTypes()
    {
        $data = ['Kg', 'Cm', 'M', 'Carton', 'gram', 'pcs'];
        foreach ($data as $key => $name) {
            UnitTypes::create([
                'unit_name' => $name,
            ]);
        }
    }

    public function dataProduk()
    {
        $status = ['draft', 'published', 'hidden'];
        foreach ($status as $key => $name) {
            ProductStatus::create([
                'name' => $name,
            ]);
        }
        $dproduct = [
            'name'                     => "monitor",
            'product_model_id'         => 1,
            'product_additionals_id'   => 1,
            'slug'                     => 'monitor',
            'sku'                      => '1',
            'deskripsi'                => 'monitor',
            'product_attribute_header' => '{"variation":[{"attribute_id":"1","terms":["4"]},{"attribute_id":"2","terms":["1","2"]}],"terms":[4,1,2],"attributes":[1,2]}',
            'weight'                   => 1,
            'length'                   => 1,
            'width'                    => 1,
            'height'                   => 1,
            'publish_at'               => date('Y-m-d'),
            'product_status_id'        => 2,
        ];

        $product = MsProduct::create($dproduct);

        ProductCategoryCombine::create([
            'ms_product_id'       => $product->id,
            'product_category_id' => 1,
        ]);

        $product_vendor = [
            'sku'           => 'aaa',
            'ms_product_id' => $product->id,
            'vendor_id'     => 1,
            'vendor_code'   => 1,
            'stock'         => 1,
            'part_code'     => 1,
            'brand_code'    => 1,
            'product_code'  => 1,
        ];

        $productVendor = ProductVendor::create($product_vendor);

        $varians = [
            'sku'               => 1,
            'stock'             => 1,
            'length'            => 1,
            'width'             => 1,
            'weight'            => 1,
            'height'            => 1,
            'power'             => 1,
            'product_vendor_id' => $productVendor->id,
        ];

        $productVarian = productVarian::create($varians);

        $attributes = [
            'product_attribute_id'      => 1,
            'product_attribute_term_id' => 1,
            'product_varian_id'         => $productVarian->id,
        ];
        ProductVarianAttribute::create($attributes);

        $variation = [];
        $attr                   = ProductAttribute::first();
        $term                   = ProductAttributeTerm::first();
        $variation[$attr->id]   = $term->id;
        $variation[$attr->name] = $term->name;
        $productVarian->update([
            'variation' => json_encode($variation),
        ]);
    }

    public function dataBatch()
    {
        $dataBatch = [
            'batch_no' => 'B-16042020-00001',
            'shipping_cost' => 1000,
            'extra_cost' => 1000,
            'batch_date' => date('Y-m-d'),
        ];
        $batch = Batch::create($dataBatch);
        $hasil_item = [
            'item_name' => 'Monitor',
            'batch_id' => $batch->id,
            'product_id' => 1,
            'product_vendor_id' => 1,
            'product_varian_id' => 1,
            'quantity' => 1000,
            'value' => 100,
            'batch_status_id' => 1,
            'is_consigment' => 1,
        ];
        BatchItem::create($hasil_item);

        $datah = [
            'batch_id'  => $batch->id,
            'batch_status_shipment_id' => 1,
            'datetime' => Carbon::now()
        ];
        BatchShipmentHistory::create($datah);
        BatchItem::where('batch_id', $batch->id)->update(['is_accepted' => 1]);
        Batch::where('id', $batch->id)->update(['is_confirmed' => 1]);
    }

    public function dataTeknisiPrice()
    {
        $pg = ProductGroup::create([
            'name' => 'AC',
            'ms_services_type_id' => 1,
        ]);

        $teknisi = User::where('name', 'teknisi')->first();

        $JobTitleCategory = JobTitleCategory::create([
            'name' => 'jasa',
        ]);

        // data job title
        $jobTitle = JobTitle::create([
            'user_id'               => $teknisi->id,
            'description'           => 'operator',
            'job_title_category_id' => $JobTitleCategory->id,
        ]);

        $t = Technician::create([
            'user_id' => $teknisi->id,
            'job_title_id' => $jobTitle->id,
            'skill'        => 'bercocok tanam',
        ]);

        MsPriceService::create([
            'ms_services_types_id' => 1,
            'value'                => 10000,
            'technicians_id'       => $t->id,
            'product_group_id' => $pg->id,
        ]);

        TechnicianSparepart::create([
            'ms_services_id' => Services::first()->id,
            'technicians_id' => $t->id,
            'name'           => 'Lorem ipsum dolor sit amet',
            'price'          => 100000,
            'description'    => 'Lorem ipsum dolor sit amet',
        ]);
    }
}
