<?php

namespace App\Midtrans;
use App\Model\Master\GeneralSetting;

abstract class VtIntegrationTest extends \PHPUnit_Framework_TestCase
{
    public static function setUpBeforeClass()
    {
        $getServerKey = GeneralSetting::where('id', 7)->first();
        $getclientKey = GeneralSetting::where('id', 8)->first();
        Config::$serverKey = $getServerKey->value;
        Config::$clientKey = $getclientKey->value;
        // Config::$serverKey = getenv('SB-Mid-server-7Q3bB3s0bd8oTo9iZVGghcqA');
        // Config::$clientKey = getenv('SB-Mid-client-oBzvWZCgQ3_WlRA_');
        Config::$isProduction = false;
    }

    public function tearDown()
    {
        // One second interval to avoid throttle
        sleep(1);
    }
}
