<?php

namespace App\Imports;

use App\User;
use Carbon\Carbon;
use App\Model\Master\Batch;
use App\Model\Master\BatchItem;
use App\Model\Master\Inventory;
use App\Model\Master\MsProduct;
use App\Model\Master\MsWarehouse;
use Illuminate\Support\Collection;
use App\Model\Product\ProductVendor;
use App\Services\Master\BatchService;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;

class BatchInventoryImportBackup implements ToCollection, WithStartRow, WithCalculatedFormulas, WithValidation
{

    public function collection(Collection $rows)
    {

        // iterating each row and validating it:
        foreach ($rows->toArray() as $key => $row) {
            $batch = [
                'batch_date'    => Carbon::parse($row[0])->format('Y-m-d'),
                'batch_no'      => BatchService::generateBatchNo(),
                'shipping_cost' => $row[1],
                'extra_cost'    => $row[2],
                'is_confirmed'   => 1
            ];
            $insert_batch = Batch::create($batch);
            $product_vendor = ProductVendor::where('product_code', $row[3])->select('ms_product_id','id')->first();

            $batch_item = [
                'batch_id'          => $insert_batch['id'],
                'product_id'        => $product_vendor->ms_product_id,
                'product_vendor_id' => $product_vendor->id,
                'value'             => $row[4],
                'quantity'          => $row[5],
                'batch_status_id'   => $row[6],
                'is_accepted'       => 1,
                'is_confirmed'      => 1
            ];
            $insert_batch_item = BatchItem::create($batch_item);
            $warehouse_id = MsWarehouse::where('name',$row[9])->value('id');
            $inventory = [
                'product_id'       => $product_vendor->ms_product_id,
                // 'ms_batch_id'      => $insert_batch['id'],
                'ms_batch_item_id' => $insert_batch_item['id'],
                'cogs_value'       => $row[7],
                'ms_unit_type_id'  => $row[8],
                'ms_warehouse_id'  => $warehouse_id,
            ];
            $insert_inventory = Inventory::create($inventory);
        }
        
    }

    public function startRow(): int
    {
        return 2;
    }

    // this function returns all validation errors after import:
    public function getErrors()
    {
        return $this->errors;
    }

    public function rules(): array
    {
        return [
            '0' => 'required|max:255',
            '1' => 'required|numeric',
            '2' => 'required|numeric',
            '6' => 'required|numeric',
            '7' => 'required|numeric',
            '8' => 'required|max:255',
            '9' => 'required|numeric',
            '10' => 'required|max:255',
            '11' => 'required|max:255',
            '12' => 'required|max:255',
            '13' => 'required|max:255',
        ];
    }

    public function customValidationMessages()
    {
        return [
            '0.required'  => 'Batch Date must required',
            '1.required'  => 'Shipping Cost must required',
            '2.required'  => 'Batch Status must required',
            '6.required'  => 'Value must required',
            '7.required'  => 'Quantity  must required',
            '8.required'  => 'Status  must required',
            '9.required'  => 'Cogs Value  must required',
            '10.required' => 'Shipping must required',
            '11.required' => 'Unit Types must required',
            '12.required' => 'Warehouse must required',
            '13.required' => 'Warehouse must required',
        ];
    }


}
