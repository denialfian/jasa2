<?php

namespace App\Traits;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Services\UserService;
use App\Model\Master\WebSetting;
use App\User;

trait UserWebService
{
    public function store(UserStoreRequest $request, UserService $userService)
    {
        // validasi
        $request->validated();

        // create user
        $user = $userService->create($request->all());

        // redirect
        return $this->redirectTo($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserService $userService, UserUpdateRequest $request, $id)
    {
        // validasi
        $request->validated();

        // user
        $user = User::where('id', $id)->firstOrFail();

        // update user
        $userService
            ->update($request->all(), $user);

        // redirect
        return $this->redirectTo($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // get user
        $user = User::where(['id' => $id])->firstOrFail();

        // redirect
        return $this->redirectTo($user->delete());
    }

    private function redirectTo($condition)
    {
        return $this->getRedirection($condition, [
            'route' => route('manage.users')
        ]);
    }
}
