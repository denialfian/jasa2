<?php

namespace App\Traits;

use Carbon\Carbon;
use App\Model\Master\Order;
use Illuminate\Http\Request;
use App\Model\Master\Ewallet;
use App\Model\Master\Inventory;
use App\Model\Master\ItemDetail;
use App\Model\Master\ChatMessage;
use App\Model\Master\GaransiChat;
use App\Model\Master\HistoryOrder;
use App\Model\Master\OrdersStatus;
use App\Model\Master\ProductGroup;
use Illuminate\Support\Facades\DB;
use App\Model\Master\ServiceDetail;
use App\Services\NotifikasiService;
use App\Model\Master\EwalletHistory;
use App\Model\Master\GeneralSetting;
use App\Services\Master\ChatService;
use Illuminate\Support\Facades\Auth;
use App\Model\Master\SparepartDetail;
use App\Model\Master\TechnicianSaldo;
use App\Model\Master\GaransiChatDetail;
use Illuminate\Database\QueryException;
use App\Model\Master\TechnicianSchedule;
use App\Model\Master\TransactionHistory;
use App\Exceptions\SendErrorMsgException;
use App\Http\Controllers\AdminController;
use App\Services\Master\InventoryService;

trait OrderStatusHandle
{
    public $order;
    public $is_new_order = false;
    public $via = ['email', 'chat'];
    public $use_notification = true;
    public $chat_content = [
        'reply_content' => '',
        'list_chat_content' => '',
    ];

    // for order Log on schedule system ;
    public $is_by_system = null;
    public $is_send_notif = true;

    /**
     * set order data
     */
    public function setOrder(int $order_id)
    {
        $order = Order::with(
            'user.technician',
            'symptom',
            'service',
            'order_status',
            'sparepart_detail',
            'tmp_sparepart',
            'item_detail',
            'tmp_item_detail',
            'service_detail.symptom',
            'service_detail.services_type.product_group',
            'service_detail.technician',
            'service_detail.technician.user',
            'service_detail.price_service',
            'history_order.order_status',
            'orderpayment',
            'user.ewallet',
            'history_order.order_status'
        )
            ->where('id', $order_id)->firstOrFail();

        $this->order = $order;
        return $this;
    }

    public function setNotificationVia(array $via)
    {
        $this->via = $via;

        return $this;
    }

    /**
     * get order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * get new order
     */
    public function getNewOrder()
    {
        $this->is_new_order = true;
        $order = $this->getOrder();

        return $this->setOrder($order->id)->getOrder();
    }

    /**
     * get chat content
     */
    public function getContentChat()
    {
        return $this->chat_content;
    }

    /**
     * check if customer has own order
     */
    public function checkCustomerHasOrder()
    {
        $order = $this->getOrder();
        if ($order->users_id != Auth::id()) {
            throw new SendErrorMsgException('Not Found');
        }

        return $this;
    }

    /**
     * check if teknisi has own order
     */
    public function checkTeknisiHasOrder()
    {
        $order = $this->getOrder();
        if ($order->detail->teknisi_email != Auth::user()->email) {
            throw new SendErrorMsgException('Not Found');
        }

        return $this;
    }

    /**
     * check if change status by system
     */
    public function setChangeBySystem() {
        $this->is_by_system = 'system';
        return $this;
    }

    public function notSendNotif() {
        $this->is_send_notif = false;
        return $this;
    }


    /**
     * send chat auto reply
     * send email order
     * send notifikasi order
     */
    public function sendNotif(int $user_to = null, int $user_from = null, int $order_id = null, $use = true)
    {
        if($use == true) {
            $order = $this->is_new_order == true ? $this->getOrder() : $this->getNewOrder();

            if ($this->use_notification) {
                (new NotifikasiService())->kirimNotifikasiOrder($order);

                $order->update([
                    'notifikasi_type' => 1,
                    'customer_read_at' => null,
                    'teknisi_read_at' => null,
                    'admin_read_at' => null,
                ]);
                if ($user_to == null) {
                    $user_to = $order->detail->technician->user_id;
                }

                if ($user_from == null) {
                    $user_from = $order->users_id;
                }

                if ($order_id == null) {
                    $order_id = $order->id;
                }

                $vias = $this->via;
                foreach ($vias as $via) {
                    if ($via == 'email') {
                        //email status order
                        $this->sendEmailOrder($order);
                    }

                    if ($via == 'chat') {
                        // send chat
                        $notif = (new ChatService())->sendOrderHistory($user_to, $user_from, $order_id);
                        $this->order = $notif['order'];
                        $this->chat_content = $notif;
                    }
                }
            }
            return $order;
        } else {
            return null;
        }

    }

    /**
     * cancel Job by customer
     * setelah buat order atau setelah accept job by teknisi
     * baru bisa di cancel customer
     * kena potongan
     */
    public function changeToCancelByCustomer()
    {
        $order = $this->getOrder();

        if ($order->garansi != null) {
            throw new SendErrorMsgException('Klaim garansi tak bisa di cencel');
        }

        // total service
        $totalService = $order->service_detail->sum(function ($item) {
            return $item->price * $item->unit;
        });

        // total sparepart detail
        $totalSparepartsDetail = $order->sparepart_detail->sum(function ($item) {
            return $item->price * $item->quantity;
        });

        // total item detail
        $totalItemDetail = $order->item_detail->sum(function ($item) {
            return $item->price * $item->quantity;
        });

        // total all sparepart
        $totalSpareparts = $totalSparepartsDetail + $totalItemDetail;
        $settings = GeneralSetting::all();

        if ($order->is_less_balance == 1) {
            $nominal = $settings[1]->value;
            $getCommission =  $settings[5]->value / 100 * $nominal;
            $saldo = $totalService - $settings[1]->value + $order->user->ewallet->nominal;
            $balance = 0;
        } else {
            if ($settings[1]->type == 2) {
                $nominal = $settings[1]->value;
                $getCommission =  $settings[5]->value / 100 * $nominal;
                $balance = 0;
                $saldo = $totalSpareparts != "" ? ($totalService - $settings[1]->value + $totalSpareparts + $order->user->ewallet->nominal) : $totalService - $settings[1]->value + $order->user->ewallet->nominal;
            } else {
                $nominal = $settings[1]->value / 100 * $totalService;
                $getCommission =  $settings[5]->value / 100 * $nominal;
                $balance = 0;
                $saldo = $totalSpareparts != "" ? ($totalService - $settings[1]->value / 100 * $totalService + $totalSpareparts + $order->user->ewallet->nominal) : $totalService - $settings[1]->value / 100 * $totalService + $order->user->ewallet->nominal;
            }
        }

        if (count($order->sparepart_detail) > 0) {
            $store_parts = json_encode($order->sparepart_detail);
        } elseif (count($order->item_detail) > 0) {
            $store_parts = json_encode($order->item_detail);
        } else {
            $store_parts = null;
        }

        if ($order->is_canceled == 0) {
            $data_order = [
                'orders_statuses_id' => 5,
                'is_less_balance' => $balance,
                'penalty_order_type' => $settings[1]->type,
                'penalty_order_value' => $settings[1]->value,
                'commission_penalty' => $settings[5]->value,
                'type_commission_penalty' => $settings[5]->type,
                'transfer_status' => 0,
                'is_canceled' => 1,
                'canceled_at' => date('Y-m-d H:i:s')
            ];

            $data_wallet_histiry = [
                'ms_wallet_id' => $order->user->ewallet->id,
                'type_transaction_id' => 3, // cancel with customer
                'nominal' => $nominal,
                'saldo' => $saldo,
                'user_id' => $order->user->id
            ];

            $data_wallet = [
                'nominal' => $saldo,
            ];

            $data_teknisi_saldo = [
                'voucer_id'        => 0,
                'commission_value' => 0,
                'early_total' => $order->grand_total,
                'total' => $nominal - $getCommission,
                'transfer_status' => 0,
            ];

            // transaction History
            $dataHistoryTransaksi = [
                'type_history'      => 'Cancel Order By Customer',
                'order_code'        => $order->code,
                'order_id'          => $order->id,
                'service_type'      => $order->service->name,
                'symptom_name'      => $order->symptom->name,
                'product_group_name' => $order->product_group_name,
                'price'             => 0,
                'beginning_balance' => $order->user->ewallet->nominal + $order->service_detail[0]->price,
                'ending_balance'    => $order->user->ewallet->nominal + $order->service_detail[0]->price - $nominal,
                'number_of_pieces'  => $nominal,
                'status'            => 5,
                'technician_name'   => $order->service_detail[0]->technician->name,
                'parts'             => $store_parts,
                'commission_to_technician' => $nominal - $getCommission,
            ];
        } else {
            $data_order = [
                'orders_statuses_id' => 5,
            ];
        }

        // return [
        //     'order' => $order,
        //     'data_order' => $data_order,
        //     'data_wallet_histiry' => $data_wallet_histiry,
        //     'data_wallet' => $data_wallet,
        //     'data_teknisi_saldo' => $data_teknisi_saldo,
        // ];

        DB::beginTransaction();
        try {
            if ($order->is_canceled == 0) {
                TechnicianSchedule::where('order_id', $order->id)
                    ->update(['is_deleted' => 1]);

                EwalletHistory::where('id', $order->orderpayment->wallet_history_id)
                    ->where('type_transaction_id', 1)
                    ->update($data_wallet_histiry);

                Ewallet::where('user_id', $order->user->id)
                    ->update($data_wallet);

                TechnicianSaldo::where('order_id', $order->id)
                    ->update($data_teknisi_saldo);

                $transactionHistory = TransactionHistory::create($dataHistoryTransaksi);
            }
            // // balik part tidak terpakai
            // foreach ($order->item_detail as $item) {
            //     $inventory = Inventory::where('id', $item->inventory_id)->firstOrFail();
            //     $inventory->update([
            //         'stock_out' => $inventory->stock_out - $item->quantity
            //     ]);
            // }

            // update order
            $order->update($data_order);

            // get data order terbaru
            $order = $this->getNewOrder();

            TechnicianSchedule::where('order_id', $order->id)->update(['is_deleted' => 1]);
            $updateTechnicianSchedule = TechnicianSchedule::where('order_id', $order->id)->update(['is_deleted' => 1]);
            // balik part
            foreach ($order->item_detail as $item) {
                $inventory = Inventory::where('id', $item->inventory_id)->firstOrFail();
                $inventoryService = new InventoryService();
                // cancel order balik part quantity ke inventory
                $inventoryService->mutationLogsProcess(null,  $item->inventory_id, $item->quantity, $order->id, $order->service_detail[0]->technicians_id, 'cancel_order_true');
                $inventory->update([
                    'stock_now' => $inventory->stock_now + $item->quantity
                ]);
            }

            $adminController = new AdminController();
            $adminController->saveHistoryOrder($order->id, $order->order_status->id);

            // log order
            $this->orderLog(null, $this->is_by_system, $order->id, $order->order_status->name);
            if($this->is_send_notif) {
                // kirim notif
                $this->sendNotif($order->detail->technician->user_id, $order->users_id, $order->id);
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $order;
    }

    /**
     * reject Job by teknisi
     * saat customer minta request job
     * teknisi bisa reject job nya
     */
    public function changeToCancel($request = null)
    {
        $order = $this->getOrder();
        // return $order;
        $status = 5;

        DB::beginTransaction();
        try {
            $order->update([
                'orders_statuses_id' => $status
            ]);

            // transaction History
            $transactionHistory = TransactionHistory::create([
                'type_history'      => 'Technician Has Been Rejected This Order',
                'order_code'        => $order->code,
                'order_id'          => $order->id,
                'service_type'      => $order->service->name,
                'symptom_name'      => $order->symptom->name,
                'product_group_name' => $order->product_group_name,
                'price'             => 0,
                'beginning_balance' => $order->user->ewallet->nominal,
                'ending_balance'    => $order->grand_total + $order->user->ewallet->nominal,
                'number_of_pieces'  => 0,
                'status'            => $status,
                'technician_name'   => $order->service_detail[0]->technician->name,
                'parts'             => null
            ]);

            ServiceDetail::where('orders_id', $order->id)->update([
                'ms_service_statuses_id' => $status
            ]);

            // online
            if ($order->payment_type == 1) {
                EwalletHistory::where('id', $order->orderpayment->wallet_history_id)->where('type_transaction_id', 1)->update([
                    'ms_wallet_id' => $order->user->ewallet->id,
                    'type_transaction_id' => 3, // cancel with customer
                    'nominal' => $order->grand_total,
                    'saldo' => $order->grand_total + $order->user->ewallet->nominal,
                    'user_id' => $order->user->id
                ]);

                Ewallet::where('user_id', $order->user->id)->update([
                    'nominal' =>  $order->grand_total + $order->user->ewallet->nominal,
                ]);
            }

            // // balik part tidak terpakai
            // foreach ($order->item_detail as $item) {
            //     $inventory = Inventory::where('id', $item->inventory_id)->firstOrFail();
            //     $inventory->update([
            //         'stock_out' => $inventory->stock_out - $item->quantity
            //     ]);
            // }

            TechnicianSchedule::where('order_id', $order->id)->update(['is_deleted' => 1]);
            $updateTechnicianSchedule = TechnicianSchedule::where('order_id', $order->id)->update(['is_deleted' => 1]);

            // balik part
            foreach ($order->item_detail as $item) {
                $inventory = Inventory::where('id', $item->inventory_id)->firstOrFail();
                $inventoryService = new InventoryService();
                // cancel order balik part quantity ke inventory
                $inventoryService->mutationLogsProcess(null, $item->inventory_id, $item->quantity, $order->id, $order->service_detail[0]->technicians_id, 'cancel_order_true');
                $inventory->update([
                    'stock_now' => $inventory->stock_now + $item->quantity
                ]);
            }
            // get data order terbaru
            $order = $this->getNewOrder();

            //History Order
            $adminController = new AdminController();
            $adminController->saveHistoryOrder($order->id, $status);

            // log
            $this->orderLog(null, $this->is_by_system, $order->id, $order->order_status->name);

            if($this->is_send_notif) {
                // kirim notif
                $this->sendNotif($order->detail->technician->user_id, $order->users_id, $order->id);
            }

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $order;
    }

    /**
     * change order status to Waiting Approval
     * accept job by teknisi
     * saat customer minta request job
     * teknisi bisa terima job nya
     */
    public function changeToWaitingApproval($request)
    {
        $order = $this->getOrder();
        if (count($order->sparepart_detail) > 0) {
            $store_parts = json_encode($order->sparepart_detail);
        } elseif (count($order->item_detail) > 0) {
            $store_parts = json_encode($order->item_detail);
        } else {
            $store_parts = null;
        }
        // return $order;
        $komisionSetting = GeneralSetting::all();
        if ($komisionSetting[0]->type == 1) { // percent
            $result_deduction = $komisionSetting[0]->value / 100 * $order->grand_total;
        } else { // fix
            $result_deduction = $komisionSetting[0]->value;
        }
        // die();
        // $result_deduction = $komisionSetting[0]->price_service->commission_value;
        // // dd($komisionSetting);
        $status = 3;

        $estimation_hours = $request->estimation_hours;

        $totalPartTeknisi = $order->sparepart_detail->sum(function ($row) {
            return $row->price * $row->quantity;
        });
        $totalPartCompany = $order->item_detail->sum(function ($row) {
            return $row->price * $row->quantity;
        });
        $totalSparepart = $totalPartCompany + $totalPartTeknisi;
        $totalWalletUser = $order->user->ewallet == null ? 0 : $order->user->ewallet->nominal;

        // check jika wallet mencukupi
        if ($totalSparepart >= $totalWalletUser) {
            // jika tak mencukupi
            $less_balance = 1;
        } else {
            // jika menucupi
            $less_balance = 0;
        }

        $data_update_order = [
            'orders_statuses_id' => $status,
            'is_less_balance' => $less_balance,
            'is_approve' => 1,
            'estimation_hours' => $estimation_hours,
            'after_commission' => $order->grand_total > 0 ? ($order->grand_total - $result_deduction) : $result_deduction,
            'total_commission' => $result_deduction
        ];

        // jadwal teknisi
        $end_work = Carbon::parse($order->schedule)->addHour($estimation_hours);
        $data_schedule = [
            'order_id' => $order->id,
            'technician_id' => $order->detail->technicians_id,
            'schedule_date' => Carbon::parse($order->schedule)->format('Y-m-d'),
            'start_work' => $order->schedule,
            'end_work' => $end_work,
        ];

        $totalAkhirWallet = $less_balance == 0 ? ($totalWalletUser - $totalSparepart) : $totalWalletUser;
        $data_wallet = [
            'nominal' => $totalAkhirWallet
        ];

        // return $getSparepart = json_encode($order->sparepart_detail);

        DB::beginTransaction();
        try {

            // update order
            Order::where('id', $order->id)->update($data_update_order);

            // schedule teknisi
            TechnicianSchedule::create($data_schedule);

            // potong wallet
            if ($less_balance == 0) {
                Ewallet::where('user_id', $order->users_id)->update($data_wallet);
                if ($order->orderpayment != null) {
                    EwalletHistory::where('id', $order->orderpayment->wallet_history_id)->where('type_transaction_id', 1)->update([
                        'ms_wallet_id' => $order->user->ewallet->id,
                        'type_transaction_id' => 1,
                        'nominal' => $order->grand_total,
                        'saldo' => $totalAkhirWallet,
                        'user_id' => $order->users_id
                    ]);
                }
                // transaction History
                $transactionHistory = TransactionHistory::create([
                    'type_history'      => 'Technician Accepted Jobs',
                    'order_code'        => $order->code,
                    'order_id'          => $order->id,
                    'service_type'      => $order->service->name,
                    'symptom_name'      => $order->symptom->name,
                    'product_group_name' => $order->product_group_name,
                    'price'             => $totalSparepart,
                    'beginning_balance' => $totalWalletUser,
                    'ending_balance'    => $totalWalletUser - $totalSparepart,
                    'number_of_pieces'  => $totalSparepart,
                    'status'            => $status,
                    'technician_name'   => $order->service_detail[0]->technician->name,
                    'parts'             => $store_parts
                ]);
            }

            if ($less_balance == 1) {
                // Tmp
            }

            // get data order terbaru
            $order = $this->getNewOrder();

            //History Order
            $adminController = new AdminController();
            $adminController->saveHistoryOrder($order->id, $status);

            // create log
            $this->orderLog(null, $this->is_by_system, $order->id, $order->order_status->name);

            if($this->is_send_notif) {
                // kirim notif
                $this->sendNotif($order->detail->technician->user_id, $order->users_id, $order->id);
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $order;
    }

    /**
     * terima Job by customer
     * saat teknisi sudah menerima job dari customer
     * customer bisa menerima job
     */
    public function changeToProcessing($request = null)
    {
        $order = $this->getOrder();
        // return $order;
        $status = 9;
        $walltet = Ewallet::where('user_id', $order->users_id)->first();

        $totalPartTeknisi = $order->sparepart_detail->sum(function ($row) {
            return $row->price * $row->quantity;
        });

        $totalPartCompany = $order->item_detail->sum(function ($row) {
            return $row->price * $row->quantity;
        });

        $totalSparepart = $totalPartCompany + $totalPartTeknisi;
        $totalWalletUser = $walltet == null ? 0 : $walltet->nominal;
        $totalService = ServiceDetail::where('orders_id', $order->id)->sum('price');


        $data_order = [
            'orders_statuses_id' => 9,
            'is_less_balance' => $order->is_less_balance == 1 ? null : 0,
        ];

        $data_wallet = [
            'nominal' => $totalWalletUser - $totalSparepart,
        ];

        $data_wallet_history = [
            'ms_wallet_id' => $walltet->id,
            'type_transaction_id' => 1, // topup
            'transfer_status_id' => 0,
            'saldo' => $totalWalletUser - $totalSparepart,
            'nominal' => $totalSparepart + $totalService,
            'user_id' => $order->users_id,
        ];

        $data_order_history = [
            'orders_id'          => $order->id,
            'orders_statuses_id' => $status
        ];

        if (count($order->sparepart_detail) > 0) {
            $allParts = json_encode($order->sparepart_detail);
            $totalPart = 0;
            foreach ($order->sparepart_detail as $getTotalSparepartDetail) {
                $totalPart += $getTotalSparepartDetail->price * $getTotalSparepartDetail->quantity;
            }
        } elseif (count($order->item_detail) > 0) {
            $allParts = json_encode($order->item_detail);
            $totalPart = 0;
            foreach ($order->item_detail as $getItemDetail) {
                $totalPart += $getItemDetail->price * $getItemDetail->quantity;
            }
        } else {
            $allParts = null;
            $totalPart = 0;
        }

        DB::beginTransaction();
        try {

            // online
            if ($order->is_less_balance == 1) {
                $order->update($data_order);
                $walltet->update($data_wallet);
                EwalletHistory::create($data_wallet_history);
                $transactionHistory = TransactionHistory::create([
                    'type_history'      => 'Approved Order Less Balance By Customer',
                    'order_code'        => $order->code,
                    'order_id'          => $order->id,
                    'service_type'      => $order->service->name,
                    'symptom_name'      => $order->symptom->name,
                    'product_group_name' => $order->product_group_name,
                    'price'             => $totalPart,
                    'beginning_balance' => Auth::user()->ewallet->nominal + $totalPart,
                    'ending_balance'    => Auth::user()->ewallet->nominal,
                    'number_of_pieces'  => $totalPart,
                    'status'            => 9,
                    'technician_name'   => $order->service_detail[0]->technician->name,
                    'parts'             => $allParts,
                    'commission_to_technician' => $order->after_commission,
                ]);
            } else {
                $order->update([
                    'orders_statuses_id' => 9,
                ]);
            }

            // get data order terbaru
            $order = $this->getNewOrder();

            //History Order
            $adminController = new AdminController();
            $adminController->saveHistoryOrder($order->id, $status);

            if($this->is_send_notif) {
                // kirim notif
                $this->sendNotif($order->detail->technician->user_id, $order->users_id, $order->id);
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $order;
    }

    /**
     * change order status to complete
     * job selesai by customer
     */
    public function changeToComplate($request = null)
    {
        $status = 10;
        $order = $this->getOrder();
        // return $order;
        DB::beginTransaction();
        try {
            $order->update([
                'transfer_status' => 0,
                'orders_statuses_id' => $status,
                'date_complete' => date('Y-m-d H:i:s')
            ]);

            if (count($order->sparepart_detail) > 0) {
                $allParts = json_encode($order->sparepart_detail);
                $totalPart = 0;
                foreach ($order->sparepart_detail as $getTotalSparepartDetail) {
                    $totalPart += $getTotalSparepartDetail->price * $getTotalSparepartDetail->quantity;
                }
            } elseif (count($order->item_detail) > 0) {
                $allParts = json_encode($order->item_detail);
                $totalPart = 0;
                foreach ($order->item_detail as $getItemDetail) {
                    $totalPart += $getItemDetail->price * $getItemDetail->quantity;
                }
            } else {
                $allParts = null;
                $totalPart = 0;
            }

            $transactionHistory = TransactionHistory::create([
                'type_history'      => 'Technician Job Done',
                'order_code'        => $order->code,
                'order_id'          => $order->id,
                'service_type'      => $order->service->name,
                'symptom_name'      => $order->symptom->name,
                'product_group_name' => $order->product_group_name,
                'price'             => $totalPart,
                'beginning_balance' => $order->user->ewallet->nominal + $order->service_detail[0]->price + $totalPart,
                'ending_balance'    => $order->user->ewallet->nominal,
                'number_of_pieces'  => $totalPart,
                'status'            => $status,
                'technician_name'   => $order->service_detail[0]->technician->name,
                'parts'             => $allParts,
                'commission_to_technician' => $order->after_commission,
            ]);

            //Schedule teknisi hilang.
            TechnicianSchedule::where('order_id', $order->id)->update(['is_deleted' => 1]);

            // jika online bayar
            if ($order->payment_type == 1 && $order->garansi == null) {
                TechnicianSaldo::where('order_id', $order->id)->update([
                    'voucer_id'        => 0,
                    'commission_value' => 0,
                    'early_total' => $order->grand_total,
                    'total' => $order->after_commission,
                    'transfer_status' => 0,
                ]);
            }

            // jika action dari chat
            if (isset($request->chat_message_id) && $request->chat_message_id != null) {
                ChatMessage::where('id', $request->chat_message_id)->update([
                    'can_action' => 0
                ]);
            }

            if ($order->garansi != null) {
                GaransiChat::where('id', $order->garansi->id)->update([
                    'status' => 'done'
                ]);

                GaransiChatDetail::create([
                    'garansi_chat_id' => $order->garansi->id,
                    'message' => 'Claim Complete',
                    'user_id'  => $order->garansi->user_id
                ]);
            }

            //Schedule teknisi hilang.
            TechnicianSchedule::where('order_id', $order->id)->update(['is_deleted' => 1]);

            $order = $this->getOrder();

            // save history order
            $adminController = new AdminController();
            $adminController->saveHistoryOrder($order->id, $status);

            // log order
            $this->orderLog(null, $this->is_by_system, $order->id, $order->order_status->name);
            if($this->is_send_notif) {
                // kirim notif
                $this->sendNotif();
            }

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $order;
    }

    /**
     * change order status to on workung
     * on work by teknisi
     * teknisi sedang bekerja
     */
    public function changeToOnWorking($request = null)
    {
        $order = $this->getOrder();
        $status = 4;
        DB::beginTransaction();
        try {
            $order->update([
                'orders_statuses_id' => $status
            ]);

            // transaction History
            $transactionHistory = TransactionHistory::create([
                'type_history'      => 'Accepted To On working',
                'order_code'        => $order->code,
                'order_id'          => $order->id,
                'service_type'      => $order->service->name,
                'symptom_name'      => $order->symptom->name,
                'product_group_name' => $order->product_group_name,
                'price'             => 0,
                'beginning_balance' => $order->user->ewallet->nominal,
                'ending_balance'    => $order->user->ewallet->nominal,
                'number_of_pieces'  => 0,
                'status'            => $status,
                'technician_name'   => $order->service_detail[0]->technician->name,
                'parts'             => null
            ]);

            if ($request->chat_message_id != null) {
                ChatMessage::where('id', $request->chat_message_id)->update([
                    'can_action' => 0
                ]);
            }

            //Schedule teknisi hilang.
            TechnicianSchedule::where('order_id', $order->id)->update(['is_deleted' => 1]);

            $order = $this->getOrder();

            $adminController = new AdminController();
            $adminController->saveHistoryOrder( $order->id, $status);

            // log order
            $this->orderLog(null, $this->is_by_system, $order->id, $order->order_status->name);
            if($this->is_send_notif) {
                $this->sendNotif($order->users_id, $order->detail->technician->user_id, $order->id);
            }

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $order;
    }

    /**
     * change order status to done
     * job selesai by teknisi
     */
    public function changeToDone($request = null)
    {
        $order = $this->getOrder();
        // return $order;
        $status = 7;
        DB::beginTransaction();
        try {
            $order->update([
                'orders_statuses_id' => 7,
                'ms_symptom_code' => $request->ms_symptom_code_id == null ? $request->ms_symptom_code : $request->ms_symptom_code_id,
                'ms_repair_code' => $request->ms_repair_code_id == null ? $request->ms_repair_code : $request->ms_repair_code_id,
                'repair_desc' => $request->repair_desc,
            ]);

            // $getSparepartDetail  = SparepartDetail::where('orders_id', $order->id)->get();
            // $getItemDetail   = ItemDetail::where('orders_id', $order->id)->get();

            if (count($order->sparepart_detail) > 0) {
                $allParts = json_encode($order->sparepart_detail);
            } elseif (count($order->item_detail) > 0) {
                $allParts = json_encode($order->item_detail);
            } else {
                $allParts = null;
            }

            $transactionHistory = TransactionHistory::create([
                'type_history'      => 'Technician Job Done',
                'order_code'        => $order->code,
                'order_id'          => $order->id,
                'service_type'      => $order->service->name,
                'symptom_name'      => $order->symptom->name,
                'product_group_name' => $order->product_group_name,
                'price'             => 0,
                'beginning_balance' => $order->user->ewallet->nominal,
                'ending_balance'    => $order->user->ewallet->nominal,
                'number_of_pieces'  => 0,
                'status'            => $status,
                'technician_name'   => $order->service_detail[0]->technician->name,
                'parts'             => $allParts
            ]);

            if ($request->chat_message_id != null) {
                ChatMessage::where('id', $request->chat_message_id)->update([
                    'can_action' => 0
                ]);
            }

            $order = $this->getOrder();
            $adminController = new AdminController();
            $adminController->saveHistoryOrder( $order->id, $status);

            // log order
            $this->orderLog(null, $this->is_by_system, $order->id, $order->order_status->name);
            if($this->is_send_notif) {
                $this->sendNotif($order->users_id, $order->detail->technician->user_id, $order->id);
            }

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }

        return $order;
    }

    /**
     * only status order can akses
     */
    public function onlyStatus(array $status, $chat_message_id = null)
    {
        $order = $this->getOrder();

        if (!in_array($order->orders_statuses_id, $status)) {
            // jika action dari chat
            if ($chat_message_id != null) {
                ChatMessage::where('id', $chat_message_id)->update([
                    'can_action' => 0
                ]);
            }

            $statuses = OrdersStatus::where('id', $status)->get();
            $text = 'Status Must ';
            foreach ($statuses as $status) {
                $text .= $status->name . ' Or';
            }
            throw new SendErrorMsgException('Action Expired, ' . trim($text, ' Or'));
        }

        return $this;
    }

    /**
     * only done status
     */
    public function onlyJobDoneStatus($chat_message_id = null)
    {
        return $this->onlyStatus([7], $chat_message_id);
    }

    /**
     * only job proses
     */
    public function onlyJobProsesStatus($chat_message_id = null)
    {
        return $this->onlyStatus([9], $chat_message_id);
    }

    /**
     * only job working
     */
    public function onlyJobWorkingStatus($chat_message_id = null)
    {
        return $this->onlyStatus([4], $chat_message_id);
    }

    /**
     * only job pending
     */
    public function onlyJobPendingStatus($chat_message_id = null)
    {
        return $this->onlyStatus([2], $chat_message_id);
    }
}
