<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\QueryException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($request->ajax()) {
            $response = [
                'msg' => $exception->getMessage(),
                'data' => [
                    'line' => $exception->getLine(),
                    'file' => $exception->getFile(),
                    'info' => $exception->getCode(),
                    'details' => $exception,
                    'message' => $exception->getMessage(),
                ],
                'status_code' => 500,
                'success' => false
            ];

            if ($exception instanceof AuthenticationException) {
                return response()->json($response, 401);
            }
            // Kill reporting if this is an "access denied" (code 9) OAuthServerException.
            if ($exception instanceof \League\OAuth2\Server\Exception\OAuthServerException && $exception->getCode() == 9) {
                $response['status_code'] = 401;
                return response()->json($response, 401);
            }
            if ($exception instanceof QueryException) {
                if ($exception->getCode() == 23000) {
                    // $response['msg'] = 'data sudah dipakai dimodul lain';
                }
                return response()->json($response, 500);
            }
            if ($exception instanceof NotFoundHttpException) {
                $response['msg'] = 'route not found';
                $response['status_code'] = 404;
                return response()->json($response, 404);
            }
            if ($exception instanceof ModelNotFoundException) {
                $response['msg'] = 'data not found';
                $response['status_code'] = 404;
                return response()->json($response, 404);
            }
            if ($exception instanceof HasChildException) {
                $response['status_code'] = 400;
                return response()->json($response, 400);
            }
            return response()->json($response, 500);
        }



        return parent::render($request, $exception);
    }
}
