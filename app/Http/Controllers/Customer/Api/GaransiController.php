<?php

namespace App\Http\Controllers\Customer\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\GaransiChatRequest;
use App\Http\Resources\GaransichatCollection;
use App\Model\Master\GaransiChat;
use App\Model\Master\GaransiChatDetail;
use App\Services\Master\ChatService;
use App\Services\Master\GaransiChatService;
use App\Services\NotifikasiService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class GaransiController extends ApiController
{
    /**
     * list data
     */
    public function listAvailable(GaransiChatService $garansiChatService)
    {
        return $this->successResponse($garansiChatService->available(), 'ok');
    }

    /**
     * list data
     */
    public function listPending(GaransiChatService $garansiChatService)
    {
        return $this->successResponse($garansiChatService->pending(), 'ok');
    }

    /**
     * list data
     */
    public function listDone(GaransiChatService $garansiChatService)
    {
        return $this->successResponse($garansiChatService->done(), 'ok');
    }

    /**
     * list data
     */
    public function listReject(GaransiChatService $garansiChatService)
    {
        return $this->successResponse($garansiChatService->reject(), 'ok');
    }

    /**
     * datatables struktur
     */
    public function datatables()
    {
        $query = GaransiChat::with('order')->where('user_id', Auth::id());
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * list chat data
     */
    public function listChat($garansi_id)
    {
        $garansi = GaransiChatDetail::with('user', 'garansi')
            ->where('garansi_chat_id', $garansi_id)
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        $view = view('admin.customer.garansi._list_chat', [
            'details' => $garansi->sortBy('created_at'),
            'auth' => Auth::user()
        ])->render();

        return $this->successResponse($view, 'ok', 200);
    }

    /**
     * list chat data
     */
    public function listChatJson($garansi_id)
    {
        $garansi = GaransiChatDetail::with('user', 'garansi')
            ->where('garansi_chat_id', $garansi_id)
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return $this->successResponse(new GaransichatCollection($garansi), 'ok', 200);
    }

    /**
     * list detail data
     */
    public function show($garansi_id)
    {
        $garansi = GaransiChat::with('user', 'order')
            ->where('id', $garansi_id)
            ->first();

        return $this->successResponse($garansi, 'ok', 200);
    }

    /**
     * create data
     */
    public function store(GaransiChatRequest $request, GaransiChatService $garansiChatService)
    {
        return $this->successResponse($garansiChatService->createAsCustomer($request->all()), $this->successStoreMsg(), 200);
    }

    /**
     * reply data
     */
    public function reply(Request $request, GaransiChatService $garansiChatService)
    {
        if ($request->attachment != null) {
            request()->validate([
                'attachment' => 'mimes:jpeg,jpg,png',
            ]);
        }
        return $this->successResponse($garansiChatService->reply($request), $this->successStoreMsg(), 200);
    }

    /**
     * enable / disble customer reply
     */
    public function setCustomerReply($garansi_id, ChatService $chatService)
    {
        $garansi = GaransiChat::where('id', $garansi_id)->firstOrFail();

        $garansi->update([
            'status' => 'pending',
            'customer_can_reply' => $garansi->customer_can_reply == 1 ? 0 : 1
        ]);

        GaransiChatDetail::create([
            'garansi_chat_id' => $garansi->id,
            'message'      => 'your request has been accepted',
            'user_id'  => Auth::id(),
        ]);

        $chatService->notifGaransi($garansi);
        (new NotifikasiService())->kirimNotikasiGaransiStatus($garansi, 'accepted');

        return $this->successResponse($garansi, $this->successUpdateMsg(), 200);
    }

    /**
     * reject Garansi
     */
    public function rejectGaransi($garansi_id, ChatService $chatService)
    {
        $garansi = GaransiChat::where('id', $garansi_id)->firstOrFail();

        $garansi->update([
            'status' => 'reject',
            'customer_can_reply' => 0
        ]);

        GaransiChatDetail::create([
            'garansi_chat_id' => $garansi->id,
            'message' => 'your claim has been rejected',
            'user_id' => Auth::id(),
        ]);
        (new NotifikasiService())->kirimNotikasiGaransiStatus($garansi, 'rejected');

        return $this->successResponse($garansi, $this->successUpdateMsg(), 200);
    }
}
