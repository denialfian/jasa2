<?php

namespace App\Http\Controllers\Customer\Api;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Master\TicketRequest;
use App\Model\Master\MsTicket;
use App\Model\Master\MsTicketDetail;
use App\Services\Master\TicketService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class TicketController extends ApiController
{
    /**
     * show list
     */
    public function index(TicketService $service)
    {
        return $this->successResponse(
            $service->list()->where('users_id', Auth::id())->paginate(10)
        );
    }

    /**
     * datatables struktur
     */
    public function datatables()
    {
        $query = MsTicket::with('user', 'support', 'unread_ticket')->where('users_id', Auth::id());
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * create data
     */
    public function store(TicketRequest $request, TicketService $ticketService)
    {
        return $this->successResponse($ticketService->createAsCustomer($request->all()), $this->successStoreMsg(), 200);
    }

    /**
     * reply data / html
     */
    public function reply(Request $request, TicketService $ticketService)
    {
        if ($request->attachment != null) {
            request()->validate([
                'attachment' => 'mimes:jpeg,jpg,png',
            ]);
        }
        return $this->successResponse($ticketService->reply($request), $this->successStoreMsg(), 200);
    }

    /**
     * reply data / json
     */
    public function replyAsJson(Request $request, TicketService $ticketService)
    {
        if ($request->attachment != null) {
            request()->validate([
                'attachment' => 'mimes:jpeg,jpg,png'
            ]);
        }
        return $this->successResponse($ticketService->replyAsJson($request), $this->successStoreMsg(), 200);
    }

    /**
     * detail
     */
    public function show($ticket_id)
    {
        $resp = MsTicketDetail::where('ms_ticket_id', $ticket_id)->orderBy('id', 'Desc')->paginate(10);

        return $this->successResponse($resp, $this->successStoreMsg(), 200);
    }
}
