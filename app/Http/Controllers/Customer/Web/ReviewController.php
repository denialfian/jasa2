<?php

namespace App\Http\Controllers\Customer\Web;

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use App\Model\Master\Order;
use App\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends AdminController
{
    /**
     * list waiting review
     */
    public function waiting(OrderService $orderService)
    {
        $o = $orderService->dontHaveReview()->where('users_id', Auth::id());

        return $this
            ->viewAdmin('admin.customer.review.waiting', [
                'title'       => 'WAITING REVIEW',
                'auth'        => Auth::user(),
                'orders'      => $o->limit(10)->get(),
                'order_count' => $o->count(),
            ]);
    }

    /**
     * list done review
     */
    public function done(OrderService $orderService)
    {
        $o = $orderService->hasReview()->where('users_id', Auth::id());

        return $this
            ->viewAdmin('admin.customer.review.list', [
                'title'       => 'MY REVIEW',
                'auth'        => Auth::user(),
                'orders'      => $o->limit(10)->get(),
                'order_count' => $o->count(),
            ]);
    }

    /**
     * create review
     */
    public function createReview($order_id)
    {
        $order = Order::with(['rating_and_review.review', 'rating_and_review.rating', 'service_detail.technician.user.info'])
            ->where('users_id', Auth::id())
            ->where('id', $order_id)
            ->firstOrFail();

        return $this
            ->viewAdmin('admin.customer.review.create', [
                'title' => 'REVIEW ' . $order->code,
                'auth'  => Auth::user(),
                'order' => $order,
            ]);
    }
}
