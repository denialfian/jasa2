<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\MsSymptom;

class MasterSymptomController extends AdminController
{
    public function showListSymptom()
    {
        return $this
            ->setBreadcrumb('list Service Category', '/admin/symptom/show')
            ->viewAdmin('admin.master.symptom.index', [
                'title' => 'List Service Category',
            ]);
    }
}
