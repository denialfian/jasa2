<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\User;
use PDF;
use App\Model\Master\GeneralSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;

class AgreementController extends AdminController
{
   public function create()
   {
        $termCondition = GeneralSetting::whereIn('name', ['agreement_text', 'agreement_title'])->orderBy('id', 'desc')->get();
        return $this->viewAdmin('admin.master.agreement.create', [
            'title' => 'Master Agreement',
            'termCondition' => $termCondition
        ]);
   }

   public function agreementOrder()
   {
        $termConditionOrder = GeneralSetting::whereIn('name', ['agreement_order_text', 'agreement_order_title'])->get();
        // return $termConditionOrder;
        return $this->viewAdmin('admin.master.agreement-order.create', [
            'title' => 'Master Agreement',
            'termConditionOrder' => $termConditionOrder
        ]);
   }

   public function agreementOrderCompleted(Type $var = null)
   {
        $termConditionOrder = GeneralSetting::whereIn('name', ['term_order_completed_title', 'term_order_completed_desc'])->get();
        // return $termConditionOrder;
        return $this->viewAdmin('admin.master.agreement-order-completed.create', [
            'title' => 'Term Order Completed',
            'termConditionOrder' => $termConditionOrder
        ]);
   }

   public function privacyPolicy(Type $var = null)
   {
        $privacyPolice = GeneralSetting::whereIn('name', ['privacy_police_title', 'privacy_police_desc'])->get();
        // return $privacyPolice;
        return $this->viewAdmin('admin.master.privacy-police.create', [
            'title' => 'Privacy Police',
            'privacyPolice' => $privacyPolice
        ]);
   }
}
