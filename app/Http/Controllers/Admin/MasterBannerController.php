<?php

namespace App\Http\Controllers\Admin;

use App\Model\Master\Banner;
use App\Http\Controllers\AdminController;
use App\Model\Master\BatchShipmentHistory;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\SendErrorMsgException;

class MasterBannerController extends AdminController
{
    public function index()
    {
        $countData = Banner::get();
        // return count($countData);
        if (count($countData) == 0) {
            return $this
                ->setBreadcrumb('create Banner', '')
                ->viewAdmin('admin.master.banner.create', [
                    'title' => 'MASTER BANNER'
                ]);
        } else {
            return $this
                ->setBreadcrumb('List Banner', '')
                ->viewAdmin('admin.master.banner.index', [
                    'title' => 'SHOW BANNER',
                    'countData' => $countData
                ]);
        }
    }

    public function create()
    {
        return $this
            ->setBreadcrumb('create Banner', '')
            ->viewAdmin('admin.master.banner.create', [
                'title' => 'MASTER BANNER'
            ]);
    }

    public function upload(Request $request)
    {
        DB::beginTransaction();
        try {

            $name_file = null;
            if (!empty($request->hasfile('images'))) {
                if ($request->hasfile('images')) {
                    $file = $request->file('images');
                    foreach ($file as $image) {
                        $name_file = rand(999999, 000000) . date("Y-m-d-H:i:s") . '.' . $image->getClientOriginalExtension();
                        $extension = $image->getClientOriginalExtension();
                        $media_data = new Banner;
                        $media_data->images = $name_file;
                        $media_data->save();
                        $image->storeAs('public/banner/', $name_file);
                    }
                }
            } else {
                throw new SendErrorMsgException('Data cannot be empty');
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }
    }

    public function destroy($id)
    {
        $banner = Banner::findOrFail($id);
        $exists = Storage::disk('local')->has('/public/banner/' . $banner->images);
        if ($exists) {
            Storage::delete('/public/banner/' . $banner->images);
            $banner->delete();
        }
    }

    public function getListApi()
    {
        $banners = Banner::get();
        return $this->successResponse($banners, 'ok');
    }
}
