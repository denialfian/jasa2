<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class MasterGradeController extends AdminController
{
    public function index()
    {
        return $this
            ->setBreadcrumb('list Grades', '/admin/grades')
            ->viewAdmin('admin.master.grade.index', [
                'title' => 'List Grades',
            ]);
    }
}
