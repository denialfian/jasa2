<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\MsProductModel;

class MasterProductModelController extends AdminController
{
    public function showProductModel()
    {
        return $this
        ->setBreadcrumb('List Product Model', '/admin/product-model/show')
        ->viewAdmin('admin.master.product.product-model.index', [
            'title' => 'Product Model'
        ]);
    }

    public function createProductModel()
    {
        return $this
        ->setBreadcrumb('List Product Model', '/admin/product-model/show')
        ->setBreadcrumb('Create Product Model', '#')
        ->viewAdmin('admin.master.product.product-model.create', [
            'title' => 'Create Product Model'
        ]);
    }

    public function updateProductModel($id)
    {
        $editProductModel = MsProductModel::with('product_brand')->findOrFail($id);
        
        return $this
        ->setBreadcrumb('List Product Model', '/admin/product-model/show')
        ->setBreadcrumb('Update Product Model', '#')
        ->viewAdmin('admin.master.product.product-model.update', [
            'title' => 'Update Product Model',
            'editProductModel' => $editProductModel
        ]);
    }
}
