<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\ImageUpload;
use Yajra\DataTables\DataTables;
use File;
use Auth;

class ImageUploadController extends AdminController
{
    public function apiListImage()
    {
        $viewGalery = ImageUpload::get();
        return DataTables::of($viewGalery)
            ->toJson();
    }

    public function ShowDataImages(Request $request)
    {
        $userLogin = Auth::user()->id;
        // $viewGalery = ImageUpload::where('user_id', $userLogin)->paginate(12);
        $viewGalery = ImageUpload::paginate(12);
        $counter = ImageUpload::get();

        if ($request->ajax()) {
            return $this
                ->setBreadcrumb('List Images', '/admin/gallery-image/show-main')
                ->viewAdmin('admin.master.galeryImage.imageList', ['viewGalery' => $viewGalery, 'userLogin' => $userLogin])->render();
        }


        return $this
            ->setBreadcrumb('List Images', '/admin/gallery-image/show-main')
            ->viewAdmin('admin.master.galeryImage.create', [
                'title' => 'Gallery Image',
                'viewGalery' => $viewGalery,
                'counter' => $counter,
                'userLogin' => $userLogin
            ]);
    }

    public function fileStore(Request $request)
    {
        $userLogin = Auth::user()->id;
        $path = storage_path('app/public/images/Id_' . $userLogin);

        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }

        // dd('done');

        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move($path, $imageName);

        $imageUpload = new ImageUpload();
        $imageUpload->filename = $imageName;
        $imageUpload->user_id = $userLogin;
        $cek = $imageUpload->save();

        return response()->json(['success' => $imageName]);
    }

    public function fileDestroy(Request $request)
    {
        $gambar = ImageUpload::whereIn('id', $request->id)->first();
        File::delete('images/' . $gambar->filename);

        $deleteImage = ImageUpload::whereIn('id', $request->id)->delete();
        return response()->json(['success' => $deleteImage]);
    }

    // for galery image modal

    public function listImage(Request $request)
    {
        $userLogin = Auth::user()->id;
        $viewGalery = ImageUpload::where('user_id', $userLogin)->paginate(12);

        if ($request->ajax()) {
            return $this->viewAdmin('admin.modalGalery.dataImages', ['viewGalery' => $viewGalery, 'userLogin' => $userLogin])->render();
        }

        return $this->viewAdmin('admin.modalGalery.listImages', [
            'title' => 'Galery Image',
            'userLogin' => $userLogin,
            'viewGalery' => $viewGalery->appends(request()->except('page'))
        ]);
    }

    public function uploadImages(Request $request)
    {
        $showImage = ImageUpload::paginate(12);

        if ($request->ajax()) {
            return viewAdmin('admin.modalGalery.galeryImage', ['showImage' => $showImage])->render();
        }


        return $this->viewAdmin('admin.modalGalery.galeryImage', [
            'title' => 'CREATE PRODUCT',
            'showImage' => $showImage
        ]);
    }
}
