<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Model\Master\MsAdditionalInfo;
use App\Model\Master\Order;
use App\User;
use Auth;
use Illuminate\Http\Request;

class requestListStatusController extends AdminController
{
    public function approve()
    {
        return $this->viewAdmin('admin.customer.job_request.statusOrder.approveJobs');
    }

    public function pending()
    {
        return $this->viewAdmin('admin.customer.job_request.statusOrder.pending');
    }

    public function payConfrimCustomer()
    {
        return $this->viewAdmin('admin.customer.job_request.statusOrder.waitConfrimPayment');
    }

    public function payConfrimAdmin()
    {
        return $this->viewAdmin('admin.customer.job_request.statusOrder.waitConfrimAdmin');
    }

    public function payConfrim()
    {
        return $this->viewAdmin('admin.customer.job_request.statusOrder.payConfrim');
    }

    public function jobsDone()
    {
        return $this->viewAdmin('admin.customer.job_request.statusOrder.done');
    }

    public function jobsCancel()
    {
        return $this->viewAdmin('admin.customer.job_request.statusOrder.cancel');
    }
}
