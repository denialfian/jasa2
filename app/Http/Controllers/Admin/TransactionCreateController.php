<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\User;
use App\Model\Master\Order;
use App\Model\Technician\Technician;
use App\Model\Master\MsAdditionalInfo;
use App\Http\Controllers\AdminController;
use App\Model\Master\MsTicket;
use App\Model\Master\Ewallet;
use Illuminate\Http\Request;

class TransactionCreateController extends AdminController
{
    public function index($ticket_id = null, Request $request)
    {
        $ticket = null;
        if($ticket_id != null){
            $ticket = MsTicket::with('user.addresses')->where('id', $ticket_id)->first();
        }

        return $this
            ->viewAdmin('admin.master.transactions.create', [
                'title' => 'Transaction Create',
                'ticket' => $ticket
            ]);
    }
}
