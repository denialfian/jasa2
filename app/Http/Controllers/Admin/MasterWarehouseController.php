<?php

namespace App\Http\Controllers\Admin;

use App\Model\Master\MsCity;
use Illuminate\Http\Request;
use App\Model\Master\MsVillage;
use App\Model\Master\MsZipcode;
use App\Model\Master\MsDistrict;
use App\Model\Master\MsWarehouse;
use App\Http\Controllers\AdminController;

class MasterWarehouseController extends AdminController
{
    public function showWarehouse()
    {
        return $this
        ->setBreadcrumb('List Warehouses', '/admin/warehouse/show')
        ->viewAdmin('admin.master.warehouse.index', [
            'title' => 'MASTER WAREHOUSE'
        ]);
    }

    public function createWarehouse()
    {
        return $this
        ->setBreadcrumb('List Warehouses', '/admin/warehouse/show')
        ->setBreadcrumb('Create Warehouses', '#')
        ->viewAdmin('admin.master.warehouse.create');
    }

    public function updateWarehouse($id)
    {
        $editWarehouse = MsWarehouse::where('id', $id)->first();
        // return $editVilage;
        return $this
        ->setBreadcrumb('List Warehouses', '/admin/warehouse/show')
        ->setBreadcrumb('Update Warehouses', '#')
        ->viewAdmin('admin.master.warehouse.update', compact('editWarehouse'));
    }
}
