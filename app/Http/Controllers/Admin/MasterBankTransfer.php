<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\BankTransfer;
use App\Model\GeneralSetting;

class MasterBankTransfer extends AdminController
{
    public function midtransSettings()
    {
        $showKey = GeneralSetting::whereIn('id', [7,8,9])->get();
        // return $showKey;
        return $this->setData([
            'title' => 'Payment Getway Settings',
            'showKey' => $showKey
        ])
        ->setBreadcrumb('Payment Getway Settings', '/admin/bank/showPaymentType')
        ->viewAdmin('admin.master.bankTransfer.midtransDetailSettings');
    }

    public function showIndexListPayment()
    {
        $no = 1;
        $showListPayment = $getDataBank = BankTransfer::whereIn('id', [1,2])->get();
        return $this->setData([
            'title' => 'Payment List',
            'showListPayment' => $showListPayment,
            'no' => $no
        ])
        ->setBreadcrumb('Payment List', '/admin/bank/showPaymentType')
        ->viewAdmin('admin.master.bankTransfer.showPaymentType');
    }
    
    public function index()
    {
        return $this->setData([
            'title' => 'Master Bank Transfer'
        ])
        ->setBreadcrumb('List Bank', '/admin/bank/show')
        ->viewAdmin('admin.master.bankTransfer.index');
    }


    public function create()
    {
        return $this->setData([
            'title' => 'Create Bank Transfer'
        ])
        ->setBreadcrumb('List Bank', '/admin/bank/show')
        ->viewAdmin('admin.master.bankTransfer.create');
    }

    public function update($id)
    {
        $getDataBank = BankTransfer::where('id', $id)->first();
        // return $getDataBank;
        return $this->setData([
            'title' => 'Update Bank Transfer',
            'getDataBank' => $getDataBank
        ])
        ->setBreadcrumb('List Bank', '/admin/bank/show')
        ->viewAdmin('admin.master.bankTransfer.update');
    }
}
