<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\B2BMasterBrand;
use Yajra\DataTables\DataTables;

class B2BMasterBrandController extends AdminController
{
    public function index()
    {
        return $this
            ->viewAdmin('admin.business_to_business_master_brand.index', [
                'title' => 'BUSSINES TO BUSSINES LIST BRAND',
            ]);
    }

    public function datatables()
    {
        $brand = B2BMasterBrand::orderBy('id', 'desc');

        return DataTables::of($brand)
            ->addIndexColumn()
            ->toJson();
    }

    public function store(Request $request)
    {
        $store = B2BMasterBrand::create([
            'name' => $request->name,
        ]);

        return $this->successResponse($store,'success',201);
    }

    public function update(Request $request, $id)
    {
        $update = B2BMasterBrand::where('id', $id)->update([
            'name' => $request->name,
        ]);

        return $this->successResponse($update,'success',201);
    }

    public function destroy($id)
    {
       $delete = B2BMasterBrand::where('id', $id)->delete();
       return $this->successResponse($delete,'success',201);
    }
}
