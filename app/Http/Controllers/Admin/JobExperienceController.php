<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class JobExperienceController extends AdminController
{
    public function index()
    {
        return $this
            ->setBreadcrumb('Job Experience', '/product')
            ->viewAdmin('admin.teknisi.job_experience.index', [
                'title' => 'MASTER JOB EXPERIENCE',
            ]);
    }
}
