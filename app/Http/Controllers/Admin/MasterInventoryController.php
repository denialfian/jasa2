<?php

namespace App\Http\Controllers\Admin;

use App\Model\Master\Inventory;
use App\Http\Controllers\AdminController;
use App\Model\Master\MutationJoinHistories;
use App\Model\Master\InventoryMutationHistories;
use App\Model\Master\MsProduct;
use App\Model\Master\MsWarehouse;
use Illuminate\Http\Request;

class MasterInventoryController extends AdminController
{
    public function showListInventory(Request $request)
    {
        $optionSelectedWarehouse = '';
        if ($request->warehouse_id != null) {
            $warehouse = MsWarehouse::where('id', $request->warehouse_id)->first();
            if ($warehouse != null) {
                $optionSelectedWarehouse = '<option value="'.$warehouse->id.'" selected>'.$warehouse->name.'</option>';
            }
        }

        $optionSelectedProduct = '';
        if ($request->product_id != null) {
            $product = MsProduct::where('id', $request->product_id)->first();
            if ($product != null) {
                $optionSelectedProduct = '<option value="'.$product->id.'" selected>'.$product->name.'</option>';
            }
        }
        return $this
            ->setBreadcrumb('List Inventory', '/admin/inventory/show')
            ->viewAdmin('admin.master.inventory.index', [
                'title' => 'Master Inventory',
                'optionSelectedProduct' => $optionSelectedProduct,
                'optionSelectedWarehouse' => $optionSelectedWarehouse,
            ]);
    }

    public function createInventory()
    {
        return $this
            ->setBreadcrumb('List Inventory', '/admin/inventory/show')
            ->setBreadcrumb('Update Inventory', '#')
            ->viewAdmin('admin.master.inventory.create', [
                'title' => 'Create Master Inventory'
            ]);
    }

    public function updateInventory($id)
    {
        $inventory = Inventory::where('id', $id)
            ->with(['batch_item.batch', 'warehouse', 'unit_type'])
            ->firstOrFail();

        return $this
            ->setBreadcrumb('list Inventory', '/admin/inventory/show')
            ->setBreadcrumb('Update Inventory', '#')
            ->viewAdmin('admin.master.inventory.update', [
                'title' => 'Update Master Inventory',
                'inventory' => $inventory
            ]);
    }

    public function mutationInventory($id)
    {
        $inventory = Inventory::where('id', $id)
            ->with(['batch_item.batch', 'warehouse', 'unit_type'])
            ->firstOrFail();

        return $this
            ->setBreadcrumb('list Inventory', '/admin/inventory/show')
            ->setBreadcrumb('Mutation Inventory', '#')
            ->viewAdmin('admin.master.inventory.mutation', [
                'title' => 'Mutation Inventory',
                'inventory' => $inventory
            ]);
    }

    public function showMutationLogs()
    {
        // $inventory = InventoryMutationLog::with(['batch_item.batch','inventory_in.warehouse', 'inventory_out.warehouse'])->get();
        // return $inventory;
        return $this
            ->setBreadcrumb('List Inventory', '/admin/inventory/show')
            ->setBreadcrumb('Mutation Inventory', '#')
            ->viewAdmin('admin.master.inventory.show-mutation-logs', [
                'title' => 'Inventory Mutation Logs',
            ]);
    }

    public function showMutationHistories()
    {

        return $this
            ->setBreadcrumb('Mutation Inventory', '#')
            ->viewAdmin('admin.master.inventory.show-mutation-histories', [
                'title' => 'Inventory Mutation Histories',
            ]);
    }
}
