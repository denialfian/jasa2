<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\User;
use App\Model\GeneralSetting;
use App\Model\Technician\Technician;
use App\Model\Master\MsAdditionalInfo;
use App\Http\Controllers\AdminController;
use App\Model\Master\TeknisiInfo;

class TechnicianController extends AdminController
{
    public function index()
    {
        return $this
            ->viewAdmin('admin.teknisi.technician.index', [
                'title' => 'MASTER TECHNICIAN',
            ]);
    }

    public function create()
    {

        return $this
            ->viewAdmin('admin.teknisi.technician.create', [
                'title' => 'CREATE TECHNICIAN'
            ]);
    }

    public function edit($id)
    {
        $technician = Technician::with([
            'job_title.job_title_category',
            'user.info',
            'user.teknisidetail.devplant',
            'user.teknisidetail.curriculum'
        ])->where('id', $id)
            ->firstOrFail();

        $settings = GeneralSetting::all();
        return $this
            ->viewAdmin('admin.teknisi.technician.edit', [
                'title'      => 'EDIT TECHNICIAN',
                'technician' => $technician,
                'settings'   => $settings

            ]);
    }

    public function editInfo($user_id)
    {
        $teknisiInfo = TeknisiInfo::where('user_id', $user_id)->first();

        return $this
            ->viewAdmin('admin.teknisi.technician.edit_info', [
                'title'      => 'EDIT INFO TECHNICIAN',
                'teknisiInfo' => $teknisiInfo,
                'user_id' => $user_id,
            ]);
    }

    public function profile()
    {
        // return \App\Model\Master\MsPriceService::with('service_type')->where('technicians_id', 2)->get();
        $user = User::with(['technician.job_title.job_title_category', 'info.religion', 'info.marital'])->where('id', Auth::id())->firstOrFail();
        // return ($user);
        return $this
            ->viewAdmin('admin.teknisi.technician.profile', [
                'title' => 'TECHNICIAN',
                'user'  => $user,
            ]);
    }

    public function job_request_create($slug_service)
    {
        $properties = \App\Model\Master\AddressType::all();

        return $slug_service;
        // return ($properties);
        return $this
            ->viewAdmin('admin.customer.job_request.create', [
                'title' => 'TECHNICIAN',
                'properties'  => $properties

            ]);
    }

    public function job_reques_list_teknisi()
    {
        $technicians = Technician::all();
        $faker = \Faker\Factory::create();

        // return ($technicians);
        return $this
            ->viewAdmin('admin.customer.job_request.list_teknisi', [
                'title' => 'TECHNICIAN',
                'technicians'  => $technicians,
                'faker'  => $faker,
            ]);
    }

    public function showAllTechnician()
    {
        $no = 1;
        // $showTeknisi = User::Role('technician')->get();Auth::user()->hasRole('Technician')
        $showTeknisi = User::Role('technician')->orderBy('updated_at', 'desc')->get();
        $showUserTeknisi = User::with('address')->where('is_login_as_teknisi', 1)
            ->whereIn('status', [7, 8])
            ->whereNotNull('email_verified_at')
            ->whereNotNull('phone')
            ->orderBy('updated_at', 'desc')
            ->get();
        // return $showTeknisi;
        return $this
            ->viewAdmin('admin.setting.allTechnicians.showListTechnician', [
                'title' => 'SHOW ALL TECHNICIAN',
                'showTeknisi' => $showTeknisi,
                'no' => $no,
                'showUserTeknisi' => $showUserTeknisi
            ]);
    }
}
