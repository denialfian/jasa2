<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;

class MasterProductStatusController extends AdminController
{
    public function index()
    {
        return $this
            ->setBreadcrumb('List Product', '/product')
            ->viewAdmin('admin.master.product.status.index', [
                'title' => 'MASTER PRODUCT STATUS'
            ]);
    }
}
