<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\Services;

class MasterServicesController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master Service'
        ])
            ->setBreadcrumb('List Service', '/admin/services/show')
            ->viewAdmin('admin.master.services.index');
    }


    public function create()
    {
        return $this->setData([
            'title' => 'Create Services'
        ])
            ->setBreadcrumb('List services', '/admin/services/show')
            ->viewAdmin('admin.master.services.create');
    }

    public function update($id)
    {
        $getServices = Services::where('id', $id)->first();
        // return $getDataservices;
        return $this->setData([
            'title' => 'Update Services',
            'getServices' => $getServices
        ])
            ->setBreadcrumb('List services', '/admin/services/show')
            ->viewAdmin('admin.master.services.update');
    }

    public function showServiceForUser()
    {
        $getServices = Services::all();
        return $this->setData([
            'title' => 'List Category Services',
            'getServices' => $getServices
        ])
            ->setBreadcrumb('List Category Services', '/user/category-services')
            ->viewAdmin('users.list-category-services');
    }

    public function serviceStatusIndex()
    {
        return $this
            ->setBreadcrumb('Service Status', '/admin/service_status/show')
            ->viewAdmin('admin.master.service_status.index', [
                'title' => 'MASTER SERVICE STATUS'
            ]);
    }

    public function orderStatusIndex()
    {
        return $this
            ->setBreadcrumb('Order Status', '/admin/order_status/show')
            ->viewAdmin('admin.master.order_status.index', [
                'title' => 'MASTER ORDER STATUS'
            ]);
    }
}
