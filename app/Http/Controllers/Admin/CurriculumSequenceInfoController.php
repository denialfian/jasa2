<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\Educations;
use Auth;

class CurriculumSequenceInfoController extends AdminController
{
    public function index()
    {
       return $this
            ->setBreadcrumb('Education', '/admin/sequence/show')
            ->viewAdmin('admin.setting.educations.curriculumSequence.index',[
                'title' => 'List Curriculum',
            ]);
    }
}
