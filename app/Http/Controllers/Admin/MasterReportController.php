<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Model\Master\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Master\SparepartDetail;
use App\Http\Controllers\AdminController;


class MasterReportController extends AdminController
{
    public function order_service()
    {

        return $this
        ->viewAdmin('admin.master.report.order_service',
            [
                'title' => 'Report Order Services',
            ]
        );
    }

    public function chart_order_service() {
        $data['bu_total_order'] = Order::select(DB::raw('DATE(created_at) as date'), DB::raw('count(id) as bu_total_order'))
            ->groupBy('date')->orderBy('date','ASC')->get()->toArray();
        $data['bu_total_order'] = collect($data['bu_total_order'])->map(function ($item, $key) {
            return [$item['date'], $item['bu_total_order']];
        });
        $data['bu_sparepart_sold'] = SparepartDetail::select('id',DB::raw('DATE(created_at) as date'), DB::raw('count(id) as bu_sparepart_sold'))->groupBy('date')->orderBy('date','ASC')->get()->toArray();
        $data['bu_sparepart_sold'] = collect($data['bu_sparepart_sold'])->map(function ($item, $key) {
            return [$item['date'], $item['bu_sparepart_sold']];
        });
        $data['bu_completed_order'] = Order::select('id',DB::raw('DATE(created_at) as date'), DB::raw('count(id) as bu_completed_order'))
            ->where('orders_statuses_id', 10)->where('transfer_status', 1)->groupBy('date')->orderBy('date','ASC')->get()->toArray();
        $data['bu_completed_order'] = collect($data['bu_completed_order'])->map(function ($item, $key) {
            return [$item['date'], $item['bu_completed_order']];
        });
        $data['bu_pending_order'] = Order::select('id',DB::raw('DATE(created_at) as date'), DB::raw('count(id) as bu_pending_order'))
            ->where('orders_statuses_id', '!=', 10)->where('transfer_status', 0)->groupBy('date')->orderBy('date','ASC')->get()->toArray();
        $data['bu_pending_order'] = collect($data['bu_pending_order'])->map(function ($item, $key) {
            return [$item['date'], $item['bu_pending_order']];
        });
        $data['bu_cancel_order'] = Order::select('id',DB::raw('DATE(created_at) as date'), DB::raw('count(id) as bu_cancel_order'))
            ->where('orders_statuses_id', 5)->where('transfer_status', 0)->groupBy('date')->orderBy('date','ASC')->get()->toArray();
        $data['bu_cancel_order'] = collect($data['bu_cancel_order'])->map(function ($item, $key) {
            return [$item['date'], $item['bu_cancel_order']];
        });


        $data['bc_total_order'] = Order::select('id',DB::raw('DATE(created_at) as date'), DB::raw('sum(grand_total) as bc_total_order'))
            ->groupBy('date')->orderBy('date','ASC')->get()->toArray();
        $data['bc_total_order'] = collect($data['bc_total_order'])->map(function ($item, $key) {
            return [$item['date'], $item['bc_total_order']];
        });
        $data['bc_sparepart_sold'] = SparepartDetail::select('id',DB::raw('DATE(created_at) as date'), DB::raw('sum(price * quantity) as bc_sparepart_sold'))->groupBy('date')->orderBy('date','ASC')->get()->toArray();
        $data['bc_sparepart_sold'] = collect($data['bc_sparepart_sold'])->map(function ($item, $key) {
            return [$item['date'], $item['bc_sparepart_sold']];
        });
        $data['bc_completed_order'] = Order::select('id',DB::raw('DATE(created_at) as date'), DB::raw('sum(grand_total) as bc_completed_order'))->where('orders_statuses_id', 10)->where('transfer_status', 1)->groupBy('date')->orderBy('date','ASC')->get()->toArray();
        $data['bc_completed_order'] = collect($data['bc_completed_order'])->map(function ($item, $key) {
            return [$item['date'], $item['bc_completed_order']];
        });
        $data['bc_pending_orer'] = Order::select('id',DB::raw('DATE(created_at) as date'), DB::raw('sum(grand_total) as bc_pending_orer'))
            ->where('orders_statuses_id', '!=', 10)->where('transfer_status', 0)->groupBy('date')->orderBy('date','ASC')->get()->toArray();
        $data['bc_pending_orer'] = collect($data['bc_pending_orer'])->map(function ($item, $key) {
            return [$item['date'], $item['bc_pending_orer']];
        });
        $data['bc_cancel_order'] = Order::select('id',DB::raw('DATE(created_at) as date'), DB::raw('sum(grand_total) as bc_cancel_order'))
            ->where('orders_statuses_id', 5)->where('transfer_status', 0)->groupBy('date')->orderBy('date','ASC')->get()->toArray();
        $data['bc_cancel_order'] = collect($data['bc_cancel_order'])->map(function ($item, $key) {
            return [$item['date'], $item['bc_cancel_order']];
        });
        // dd($data);
        // BU mean By Unit, BC mean By Currency
        return $this
        ->viewAdmin('admin.master.report.chart_order_service',
            [
                'title' => 'Chart Order Services',
                'bu_total_order'     => json_encode($data['bu_total_order']),
                'bu_sparepart_sold'  => $data['bu_sparepart_sold'],
                'bu_completed_order' => $data['bu_completed_order'],
                'bu_pending_order'   => $data['bu_pending_order'],
                'bu_cancel_order'    => $data['bu_cancel_order'],
                'bc_total_order'     => $data['bc_total_order'],
                'bc_sparepart_sold'  => $data['bc_sparepart_sold'],
                'bc_completed_order' => $data['bc_completed_order'],
                'bc_pending_order'   => $data['bc_pending_orer'],
                'bc_cancel_order'    => $data['bc_cancel_order']
            ]
        );

    }

    public function customer()
    {
        return $this
        ->viewAdmin('admin.master.report.customer', ['title' => 'Report Customer']);
    }

    public function technician()
    {
        return $this
        ->viewAdmin('admin.master.report.technician', ['title' => 'Report Technician']);
    }
}
