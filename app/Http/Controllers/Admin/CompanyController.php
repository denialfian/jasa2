<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Model\BusinessToBusinessOutletTransaction;

class CompanyController extends AdminController
{
    public function index()
    {
        return $this
            ->viewAdmin('admin.company.index', [
                'title' => 'MASTER COMPANY',
            ]);
    }

    public function showOutlet($id)
    {
        $id = $id;
        $outlet = BusinessToBusinessOutletTransaction::where('business_to_business_transaction_id', $id)->first();
        return $this
            ->viewAdmin('admin.company.list_outlet', [
                'title' => 'MASTER OUTLET',
                'outlet' => $outlet,
                'id' => $id
            ]);
    }

    public function updateOutlet($id)
    {
        $outlet = BusinessToBusinessOutletTransaction::where('id', $id)->first();
        return $this
            ->viewAdmin('admin.company.list_outletUpdate', [
                'title' => 'MASTER OUTLET',
                'outlet' => $outlet
            ]);
    }
}
