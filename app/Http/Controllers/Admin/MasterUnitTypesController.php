<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class MasterUnitTypesController extends AdminController
{
    function showListUnitType(){
        return $this
        ->setBreadcrumb('List Unit Type', '/admin/unit-type/show')
        ->viewAdmin('admin.master.unitTypes.index', [
            'title' => 'List Unit Types'
        ]);
    }
}
