<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\Educations;
use Auth;

class EducationInfoController extends AdminController
{
    public function index()
    {
       return $this
            ->setBreadcrumb('Education', '/admin/educations/show')
            ->viewAdmin('admin.setting.educations.education.index',[
                'title' => 'List Educations',
            ]);
    }

    public function create()
    {
        return $this
            ->setBreadcrumb('Education', '/admin/educations/show')
            ->setBreadcrumb('Education Create', '#')
            ->viewAdmin('admin.setting.educations.education.create',[
                'title' => 'Create Educations'
            ]);
    }

    public function update($id)
    {
        $viewEducation = Educations::where('id', $id)->first();
        
        return $this
            ->setBreadcrumb('Education', '/admin/educations/show')
            ->setBreadcrumb('Education Update', '#')
            ->viewAdmin('admin.setting.educations.education.update',[
                'title' => 'Update Educations',
                'viewEducation' => $viewEducation
            ]);
    }
}
