<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class UsersStatusController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'User Status'
        ])->viewAdmin('admin.setting.userStatus.index');
    }
}
