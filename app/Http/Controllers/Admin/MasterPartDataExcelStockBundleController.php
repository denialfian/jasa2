<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\Master\PartDataExcelStockBundle;
use App\Model\Master\PartDataExcelStockBundleDetail;
use App\Http\Controllers\AdminController;

class MasterPartDataExcelStockBundleController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master Part Data Excel Stock Bundle'
        ])
            ->setBreadcrumb('List Part Data Excel Stock Bundle', '/admin/part-data-excel-stock-bundle/show')
            ->viewAdmin('admin.master.part-data-excel-stock-bundle.index');
    }


    public function create()
    {
        return $this->setData([
            'title' => 'Create Master Part Data Excel Stock Bundle'
        ])
            ->setBreadcrumb('List Part Data Excel Stock Bundle', '/admin/part-data-excel-stock-bundle/show')
            ->viewAdmin('admin.master.part-data-excel-stock-bundle.create');
    }

    public function update($id)
    {
        $getData = PartDataExcelStockBundle::with('asc')->where('id', $id)->firstOrFail();
        // return $getData;
        $detailData = PartDataExcelStockBundleDetail::with('part_data_stock_inventory.part_data_stock.asc')->where('part_data_stock_bundle_id', $id)->get();
        // return $detailData;
        return $this->setData([
            'title' => 'Update Master Part Data Excel Stock Bundle ',
            'getData' => $getData,
            'detailData' => $detailData
        ])
            ->setBreadcrumb('List Part Data Excel Stock Bundle', '/admin/part-data-excel-stock-bundle/show')
            ->viewAdmin('admin.master.part-data-excel-stock-bundle.update');
    }

    public function detail($id)
    {
        $getData = PartDataExcelStockBundle::where('id', $id)->first();
        $detailData = PartDataExcelStockBundleDetail::with(['part_data_stock_bundle','part_data_stock_inventory.part_data_stock.asc'])->where('part_data_stock_bundle_id',$id)->get();
        // return $getDataPart Data Excel Stock Bundle;
        return $this->setData([
            'title' => 'Detail Master Part Data Excel Stock Bundle Data Excel',
            'getData' => $getData,
            'detailData' => $detailData
        ])
            ->setBreadcrumb('Detail Part Data Excel Stock Bundle', '/admin/part-data-excel-stock-bundle/detail')
            ->viewAdmin('admin.master.part-data-excel-stock-bundle.detail');
    }

}
