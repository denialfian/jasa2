<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Model\Master\VoucherGeneral;
use Illuminate\Http\Request;

class MasterVoucherGeneralController extends AdminController
{
    public function index()
    {
        return $this
        ->setBreadcrumb('List General Vouchers', '#')
        ->viewAdmin('admin.master.vouchers.general.index', [
            'title' => 'MASTER GENERAL VOUCHERS'
        ]);
    }

    public function create()
    {
        return $this
            ->setBreadcrumb('Create General Vouchers', '#')
            ->viewAdmin('admin.master.vouchers.general.create', [
                'title' => 'MASTER GENERAL VOUCHERS CREATE'
            ]);
    }

    public function update($id)
    {
        $showData = VoucherGeneral::where('id', $id)->firstOrFail();
        return $this
            ->setBreadcrumb('Create General Vouchers', '#')
            ->viewAdmin('admin.master.vouchers.general.update', [
                'title' => 'MASTER GENERAL VOUCHERS UPDATE',
                'showData' => $showData
            ]);
    }

    public function newRegisterCreate(Request $request)
    {
        // dd('asd');
        $showData = VoucherGeneral::where('is_new_register', 1)->first();

        if($showData == null){
            return $this
                ->setBreadcrumb('Create Vouchers', '#')
                ->viewAdmin('admin.master.vouchers.newUser.index', [
                    'title' => 'VOUCHERS NEWS USERS',
                ]);
        }else{
            return $this
                ->setBreadcrumb('Update Vouchers', '#')
                ->viewAdmin('admin.master.vouchers.newUser.update', [
                    'title' => 'VOUCHERS NEWS USERS',
                    'showData' => $showData
                ]);
        }
    }
}
