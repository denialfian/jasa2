<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Master\MsCity;

class MasterCityController extends AdminController
{
    public function showCity()
    {
        return $this
        ->setBreadcrumb('List City', '/admin/city/show')
        ->viewAdmin('admin.master.location.city.index', [
            'title' => 'MASTER CITY',
        ]);
    }

    public function createCity()
    {
        return $this
        ->setBreadcrumb('List City', '/admin/city/show')
        ->setBreadcrumb('Create City', '/admin/city/create')
        ->viewAdmin('admin.master.location.city.create');
    }

    public function updateCity($id)
    {
        $editCity = MsCity::with(['province.country', 'districts'])->where('id', $id)->first();

        return $this
        ->setBreadcrumb('List City', '/admin/city/show')
        ->setBreadcrumb('Update City', '/admin/city/update')
        ->viewAdmin('admin.master.location.city.update', compact('editCity'));
    }
}
