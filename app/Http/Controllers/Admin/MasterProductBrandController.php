<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use App\Model\Product\ProductBrand;

class MasterProductBrandController extends AdminController
{
    public function showProductBrand()
    {
        return $this
        ->setBreadcrumb('List Product Brand', '/admin/product-brand/show')
        ->viewAdmin('admin.master.product.productBrand.index', [
            'title' => 'Master Product Brand'
        ]);
    }

    public function createProductBrand()
    {
        return $this
        ->setBreadcrumb('List Product Brand', '/admin/product-brand/show')
        ->setBreadcrumb('Create Product Brand', '#')
        ->viewAdmin('admin.master.product.productBrand.create');
    }

    public function updateProductBrand($id)
    {
        $updatebrand = ProductBrand::where('id', $id)->first();
        return $this
        ->setBreadcrumb('List Product Brand', '/admin/product-brand/show')
        ->setBreadcrumb('Update Product Brand', '#')
        ->viewAdmin('admin.master.product.productBrand.update', compact('updatebrand'));
    }
}
