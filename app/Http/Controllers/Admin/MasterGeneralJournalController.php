<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Master\GeneralJournal;
use App\Model\Master\GeneralSetting;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Api\Master\GeneralJournalController;

use function App\Helpers\thousanSparator;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Model\Master\GeneralJournalDetail;
use App\Services\Master\GeneralJournalService;

class MasterGeneralJournalController extends AdminController
{
    public function index()
    {
        return $this->setData([
            'title' => 'Master General Journal '
        ])
            ->setBreadcrumb('List General Journal', '/admin/general-journal/show')
            ->viewAdmin('admin.master.general-journal.index');
    }


    public function create(GeneralJournalService $service)
    {
        return $this->setData([
            'title' => 'Create Master General Journal',
            'code' => $service->generateCode()
        ])
            ->setBreadcrumb('List services', '/admin/general-journal/show')
            ->viewAdmin('admin.master.general-journal.create');
    }

    public function update($id)
    {
        $getData = GeneralJournal::where('id', $id)->first();
        $detailsData = GeneralJournalDetail::with('asc')->where('general_journal_id', $id)->orderBy('date','asc')->get();
        // return $detailsData;
        return $this->setData([
            'title' => 'Update Master General Journal',
            'getData' => $getData,
            'detailsData' => $detailsData
        ])
            ->setBreadcrumb('List General Journal', '/admin/general-journal/show')
            ->viewAdmin('admin.master.general-journal.update');
    }

    public function detail($id)
    {
        $getData = GeneralJournal::where('id', $id)->first();
        $detailsData = GeneralJournalDetail::where('general_journal_id', $id)->orderBy('date','ASC')->get();

        return $this->setData([
            'title' => 'Detail Master General Journal',
            'getData' => $getData,
            'detailsData' => $detailsData
        ])
            ->setBreadcrumb('Detail General Journal', '/admin/general-journal/detail')
            ->viewAdmin('admin.master.general-journal.detail');
    }

    public function logs()
    {
        return $this->setData([
            'title' => 'General Journal Logs (Deleted Only)'
        ])
            ->setBreadcrumb('List General Journal', '/admin/general-journal/logs')
            ->viewAdmin('admin.master.general-journal.logs');
    }

    public function getValuePart($details_data, $field, $i) {
        if($details_data != null && $details_data[$i-1]->{$field} !== "") {
            $val = $field == 'date' ? Carbon::parse($details_data[$i-1]->{$field})->format('d-m-Y') : $details_data[$i-1]->{$field};
        } else {
            $val = null;
        }
        return $val;
    }

    public function showImportDetailExcel($id)
    {
        $getData = GeneralJournal::where('id', $id)->first();

        return $this->setData([
            'title' => 'Import Master General Journal',
            'getData' => $getData,
        ])
            ->setBreadcrumb('List General Journal', '/admin/general-journal/show')
            ->viewAdmin('admin.master.general-journal.import-details');
    }

    public function exportData(Request $request) {

        $query = GeneralJournal::with('general_journal_detail');
        // return $q;
        if(!empty($request->date_from) && !empty($request->date_to)) {
            $query->whereHas('general_journal_detail', function ($q) use ($request) {
                $date_from = Carbon::parse($request->date_from)->format('Y-m-d');
                $date_to = Carbon::parse($request->date_to)->format('Y-m-d');
                $q->where('date', '>=', $date_from)
                ->where('date', '<=', $date_to);
            });
        }
        $query = $query->get();
        $data = [];

        foreach($query as $row) {
            $details_data = [];
            $count_detail = count($row->general_journal_detail);
            foreach(range(1,$count_detail) as $val) {
                $details_data['date_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'date', $val);
                $details_data['category_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'category', $val);
                $details_data['description_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'description', $val);
                $details_data['quantity_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'quantity', $val);
                $details_data['debit_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'debit', $val);
                $details_data['kredit_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'kredit', $val);
                $details_data['balance_'.$val.''] = $this->getValuePart($row->general_journal_detail, 'balance', $val);

            }
            $gj_data = [
                "code" => $row['code'],
                "title" => $row['title'],
                "image" => $row['image'],
                "created_at" => Carbon::parse($row['created_at'])->format('d-m-Y')
            ];

            $data [] = array_merge($gj_data, $details_data);
        }

        return (new FastExcel($data))->download('Export_General_Journal_Data_'.$request->date_from.'-'.$request->date_to.'.xlsx');
    }

    public function exportDetailExcel(Request $request, $id) {
        $data_excel = GeneralJournal::with(['general_journal_detail' => function($query) {
                        $query->orderBy('date', 'ASC');
                    }])->where('id', $id)->first();
        // return $data_excel;
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getSheetView()->setZoomScale(80);
        $sheet->setShowGridlines(false);
        $sheet->getStyle("C1:I1")->getFont()->setSize(22)->setBold(true)->setName('Arial');
        $sheet->mergeCells('C1:I1');
        $sheet->setCellValue('C1',$data_excel->title. ' - CODE('. $data_excel->code.')');
        $sheet->getStyle("C1")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getColumnDimension('A')->setWidth(2);
        $sheet->getColumnDimension('B')->setWidth(2);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(30);
        $sheet->getColumnDimension('E')->setWidth(50);
        $sheet->getColumnDimension('F')->setWidth(10);
        $sheet->getColumnDimension('G')->setWidth(20);
        $sheet->getColumnDimension('H')->setWidth(20);
        $sheet->getColumnDimension('I')->setWidth(20);
        $sheet->setCellValue('C3', 'Date')->getStyle('C3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('D3', 'Category')->getStyle('D3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('E3', 'Description')->getStyle('E3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('F3', 'Qty')->getStyle('F3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('G3', 'Debit')->getStyle('G3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('H3', 'Credit')->getStyle('H3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('I3', 'Balance')->getStyle('I3')->getFont()->setSize(12)->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle('C3:I3')
        ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('C3:I3')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $sheet->getStyle('C3:I3')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('2F75B5');
        if(count($data_excel->general_journal_detail) > 0) {
            $balance = 0;
            foreach($data_excel->general_journal_detail as $key => $val) {
                $debit = ($val['debit'] != '' ? +intval($val['debit']) : 0);
                $kredit = ($val['kredit'] != '' ? -intval($val['kredit']) : 0);
                $balance += ($debit + $kredit);
                $row = $key + 4;
                $sheet->setCellValue('C'.$row, Carbon::parse($val->date)->format('d-m-Y'))->getStyle('C'.$row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('DDEBF7');
                $sheet->setCellValue('D'.$row, $val->category)->getStyle('D'.$row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('DDEBF7');
                $sheet->setCellValue('E'.$row, $val->description)->getStyle('E'.$row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('DDEBF7');
                $sheet->setCellValue('F'.$row, $val->quantity)->getStyle('F'.$row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('DDEBF7');
                $sheet->setCellValue('G'.$row, 'Rp. '.($debit != 0 ? thousanSparator($debit) : '0'))->getStyle('G'.$row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('DDEBF7');
                $sheet->setCellValue('H'.$row, 'Rp. '.($kredit != 0 ? thousanSparator($kredit) : '0'))->getStyle('H'.$row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('C6E0B4');
                $sheet->setCellValue('I'.$row, 'Rp. '.($balance != 0 ? thousanSparator($balance) : '0'))->getStyle('I'.$row)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('DDEBF7');
            }
        }

        $row_after_load = 4 + count($data_excel->general_journal_detail);

        $sheet->setCellValue('C'.$row_after_load, 'Ending');
        $sheet->setCellValue('D'.$row_after_load, '');
        $sheet->setCellValue('E'.$row_after_load, '');
        $sheet->setCellValue('F'.$row_after_load, '');
        $sheet->setCellValue('G'.$row_after_load, 'Rp. '.($data_excel->total_debit != 0 ? thousanSparator($data_excel->total_debit) : ''));
        $sheet->setCellValue('H'.$row_after_load, 'Rp. '.($data_excel->total_kredit != 0 ? thousanSparator($data_excel->total_kredit) : ''));
        $sheet->setCellValue('I'.$row_after_load, 'Rp. '.($data_excel->total_balance != 0 ? thousanSparator($data_excel->total_balance) : ''))->getStyle('I'.$row_after_load)->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FFFF00');
        $spreadsheet->getActiveSheet()->getStyle('C4:I'.($row_after_load))
        ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $row_info = $row_after_load+2;
        $sheet->getStyle('D'.$row_info)->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('8EA9DB');
        $sheet->getStyle('D'.($row_info+1))->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FFFF00');
        $sheet->getStyle('D'.($row_info+2))->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('92D050');
        $sheet->getStyle('D'.($row_info+3))->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('C65911');
        $sheet->getStyle('D'.($row_info+4))->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('FF0000');
        $sheet->setCellValue('E'.$row_info, 'Nota fisik Belum dikirim');
        $sheet->setCellValue('E'.($row_info+1), 'Nota kirim Terbaru');
        $sheet->setCellValue('E'.($row_info+2), 'Nota Sudah dikirimkan');
        $sheet->setCellValue('E'.($row_info+3), 'Habis pakai');
        $sheet->setCellValue('E'.($row_info+4), 'revisi Item/Nota');

        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                ],
            ],
        ];
        $sheet->getStyle('D'.($row_after_load+2).':E'.($row_after_load+2+4))->applyFromArray($styleArray);

        $sheet->setCellValue('H'.($row_info+2), 'upload image')->getStyle('H'.($row_info+2))->getFont()->setSize(22)->setBold(true);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="General_Journal_Details.xls"');
        $writer = new Xlsx($spreadsheet);
        $writer->save("php://output");

    }

    public function exportReportHpp(Request $request) {

        $gj_controller = new GeneralJournalController();
        $data_excel = $gj_controller->getReportHpp($request);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->getSheetView()->setZoomScale(80);
        $sheet->setShowGridlines(false);
        $sheet->getStyle("C1:L1")->getFont()->setSize(22)->setBold(true)->setName('Arial');
        $sheet->mergeCells('C1:L1');
        $sheet->setCellValue('C1','Report Order '.Carbon::parse($request->date_from)->format('d-m-Y').' s/d '.Carbon::parse($request->date_to)->format('d-m-Y').'');
        $sheet->getStyle("C1")->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getColumnDimension('A')->setWidth(2);
        $sheet->getColumnDimension('B')->setWidth(2);
        $sheet->getColumnDimension('C')->setWidth(10);
        $sheet->getColumnDimension('D')->setWidth(15);
        $sheet->getColumnDimension('E')->setWidth(30);
        $sheet->getColumnDimension('F')->setWidth(30);
        $sheet->getColumnDimension('G')->setWidth(15);
        $sheet->getColumnDimension('H')->setWidth(20);
        $sheet->getColumnDimension('I')->setWidth(20);
        $sheet->getColumnDimension('J')->setWidth(15);
        $sheet->getColumnDimension('K')->setWidth(15);
        $sheet->getColumnDimension('L')->setWidth(15);
        $sheet->setCellValue('C3', 'ID')->getStyle('C3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('D3', 'Code Material')->getStyle('D3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('E3', 'Part Name')->getStyle('E3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('F3', 'Is Bundle?')->getStyle('F3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('G3', 'Hpp Price')->getStyle('G3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('H3', 'Selling Pcs / Main Price')->getStyle('H3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('I3', 'Sold to Customer')->getStyle('I3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('J3', 'Qty')->getStyle('J3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('K3', 'Order Code')->getStyle('K3')->getFont()->setSize(12)->setBold(true);
        $sheet->setCellValue('L3', 'Closed Date')->getStyle('L3')->getFont()->setSize(12)->setBold(true);
        $spreadsheet->getActiveSheet()->getStyle('C3:L3')
        ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('C3:L3')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $sheet->getStyle('C3:L3')->getFill()
        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        ->getStartColor()->setARGB('2F75B5');
        //#ebf7dd
        //#1590FF

        if(count($data_excel) > 0) {
            $total_hpp_price = 0;
            $total_spmp = 0;
            $total_spstc = 0;
            $total_qty = 0;
            $bundle_details = [];
            $labors = [];
            foreach($data_excel as $key_main => $val_main) {
                if(count($val_main->bundle_details) > 0) {
                    foreach($val_main->bundle_details as $bd) {
                        $bundle_details[] = (object)[
                            'main_id' => $val_main->id,
                            'order_code' => $val_main->service_order_no,
                            'id' => $bd->id,
                            'part_data_stock_bundle_id' => $bd->part_data_stock_bundle_id,
                            'part_data_stock_inventories_id' => $bd->part_data_stock_inventories_id,
                            'quantity' => $bd->quantity,
                            'part_data_stock_id' => $bd->part_data_stock_id,
                            'code_material' => $bd->code_material,
                            'part_description' => $bd->part_description,
                            'selling_price' => $bd->selling_price,
                            'hpp_average' => $bd->hpp_average,
                            'created_at' => $bd->created_at,
                        ];
                    }

                }
                if(count($val_main->labors) > 0) {
                    foreach($val_main->labors as $la) {
                        $labors[] = (object)[
                            'main_id' => $val_main->id,
                            'order_code' => $val_main->service_order_no,
                            'id' => $la->id,
                            'orders_data_excel_id' => $la->orders_data_excel_id,
                            'jasa_id' => $la->jasa_id,
                            'jasa_name' => $la->jasa_name,
                            'price' => $la->price,
                            'created_at' => Carbon::parse($la->created_at)->format('d-m-Y H:i:s'),
                        ];
                    }
                }

                $key_main = $key_main + 4;
                $is_bundle = ($val_main->is_bundle==1) ? 'Yes : '.$val_main->bundle_name.'' : 'No';
                //selling_pcs_main_price
                $spmp = 0;
                if($val_main->is_bundle == 0) {
                    $spmp = ($val_main->selling_price_pcs != null) ? $val_main->selling_price_pcs: 0;
                } else {
                    $spmp = ($val_main->bundle_main_price != null) ? $val_main->bundle_main_price: 0;
                }
                //selling_price_sold_to_customer
                $spstc = ($val_main->selling_price != null ) ? $val_main->selling_price  : 0;
                $color_details = (count($val_main->bundle_details) > 0 || count($val_main->labors) > 0)  ? 'EBF7DD' : 'DDEBF7';
                $sheet->setCellValue('C'.$key_main,$val_main->id)->getStyle('C'.$key_main)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB( $color_details);
                $sheet->setCellValue('D'.$key_main,$val_main->code_material)->getStyle('D'.$key_main)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB( $color_details);
                $sheet->setCellValue('E'.$key_main, $val_main->part_description)->getStyle('E'.$key_main)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB( $color_details);
                $sheet->setCellValue('F'.$key_main, $is_bundle)->getStyle('F'.$key_main)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB( $color_details);
                $sheet->setCellValue('G'.$key_main, 'Rp. '.($val_main->hpp_order != null ? thousanSparator($val_main->hpp_order ) : '0'))->getStyle('G'.$key_main)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB( $color_details);
                $sheet->setCellValue('H'.$key_main, 'Rp. '.thousanSparator($spmp))->getStyle('H'.$key_main)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB( $color_details);
                $sheet->setCellValue('I'.$key_main, 'Rp. '.thousanSparator($spstc))->getStyle('I'.$key_main)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB( $color_details);
                $sheet->setCellValue('J'.$key_main, $val_main->amount)->getStyle('J'.$key_main)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB( $color_details);
                $sheet->setCellValue('K'.$key_main, $val_main->service_order_no)->getStyle('K'.$key_main)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB( $color_details);
                $sheet->setCellValue('L'.$key_main, $val_main->closed_date)->getStyle('L'.$key_main)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB( $color_details);
                $total_hpp_price += $val_main->hpp_order;
                $total_spmp += $spmp;
                $total_spstc += $spstc;
                $total_qty += $val_main->amount;
            }
        }

        $row_after_load = 1 + $key_main;

        $sheet->setCellValue('C'.$row_after_load, 'Ending');
        $sheet->setCellValue('D'.$row_after_load, '');
        $sheet->setCellValue('E'.$row_after_load, '');
        $sheet->setCellValue('F'.$row_after_load, '');
        $sheet->setCellValue('G'.$row_after_load, 'Rp. '.thousanSparator($total_hpp_price).'');
        $sheet->setCellValue('H'.$row_after_load, 'Rp. '.thousanSparator($total_spmp).'');
        $sheet->setCellValue('I'.$row_after_load, 'Rp. '.thousanSparator($total_spstc).'');
        $sheet->setCellValue('J'.$row_after_load, $total_qty);
        $sheet->setCellValue('K'.$row_after_load, '');
        $sheet->setCellValue('L'.$row_after_load, '');
        $spreadsheet->getActiveSheet()->getStyle('C4:L'.($row_after_load))
        ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $bd = 0;
        if(count($bundle_details) > 0) {
            $bd = $row_after_load + 2;
            $sheet->getStyle('C'.$bd.':J'.$bd.'')->getFont()->setSize(22)->setBold(true)->setName('Arial');
            $sheet->mergeCells('C'.$bd.':J'.$bd.'');
            $sheet->setCellValue('C'.$bd,'Bundle Details');
            $sheet->getStyle('C'.$bd)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $bd = $bd + 1;
            $border_start_bd = $bd;
            $sheet->setCellValue('C'.$bd, 'ID')->getStyle('C'.$bd)->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('D'.$bd, 'Order Code')->getStyle('D'.$bd)->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('E'.$bd, 'Code Material')->getStyle('E'.$bd)->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('F'.$bd, 'Part Description')->getStyle('F'.$bd)->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('G'.$bd, 'Quantity')->getStyle('G'.$bd)->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('H'.$bd, 'Price Hpp Average')->getStyle('H'.$bd)->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('I'.$bd, 'Selling Price')->getStyle('I'.$bd)->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('J'.$bd, 'Created at')->getStyle('J'.$bd)->getFont()->setSize(12)->setBold(true);

            foreach($bundle_details as $key_bd => $val_bd) {
                $key_bd = $key_bd + $bd + 1;
                $sheet->setCellValue('C'.$key_bd,$val_bd->main_id)->getStyle('C'.$key_bd)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('EBF7DD');
                $sheet->setCellValue('D'.$key_bd, $val_bd->order_code)->getStyle('D'.$key_bd)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('EBF7DD');
                $sheet->setCellValue('E'.$key_bd,$val_bd->code_material)->getStyle('E'.$key_bd)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('EBF7DD');
                $sheet->setCellValue('F'.$key_bd, $val_bd->part_description)->getStyle('F'.$key_bd)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('EBF7DD');
                $sheet->setCellValue('G'.$key_bd, $val_bd->quantity)->getStyle('G'.$key_bd)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('EBF7DD');
                $sheet->setCellValue('H'.$key_bd, 'Rp. '.($val_bd->hpp_average != null ? thousanSparator($val_bd->hpp_average) : '0'))->getStyle('H'.$key_bd)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('EBF7DD');
                $sheet->setCellValue('I'.$key_bd, 'Rp. '.($val_bd->selling_price != null ? thousanSparator($val_bd->selling_price) : '0'))->getStyle('I'.$key_bd)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('EBF7DD');
                $sheet->setCellValue('J'.$key_bd, $val_bd->created_at)->getStyle('J'.$key_bd)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('EBF7DD');

            }
            $spreadsheet->getActiveSheet()->getStyle('C'.$border_start_bd.':J'.($key_bd))
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $bd = $key_bd;
        }


        if(count($labors) > 0) {
            $la = $bd + 2;
            $sheet->getStyle('C'.$la.':G'.$la.'')->getFont()->setSize(22)->setBold(true)->setName('Arial');
            $sheet->mergeCells('C'.$la.':G'.$la.'');
            $sheet->setCellValue('C'.$la,'Labors');
            $sheet->getStyle('C'.$la)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $la = $la + 1;
            $border_start_la = $la;
            $sheet->setCellValue('C'.$la, 'ID')->getStyle('C'.$la)->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('D'.$la, 'Order Code')->getStyle('D'.$la)->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('E'.$la, 'Jasa Name')->getStyle('E'.$la)->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('F'.$la, 'Price')->getStyle('F'.$la)->getFont()->setSize(12)->setBold(true);
            $sheet->setCellValue('G'.$la, 'Created at')->getStyle('G'.$la)->getFont()->setSize(12)->setBold(true);

            foreach($labors as $key_la => $val_la) {
                $key_la = $key_la + $la + 1;
                $sheet->setCellValue('C'.$key_la,$val_la->main_id)->getStyle('C'.$key_la)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('EBF7DD');
                $sheet->setCellValue('D'.$key_la, $val_la->order_code)->getStyle('D'.$key_la)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('EBF7DD');
                $sheet->setCellValue('E'.$key_la,$val_la->jasa_name)->getStyle('E'.$key_la)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('EBF7DD');
                $sheet->setCellValue('F'.$key_la, 'Rp. '.($val_la->price != null ? thousanSparator($val_la->price) : '0'))->getStyle('F'.$key_la)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('EBF7DD');
                $sheet->setCellValue('G'.$key_la, $val_la->created_at)->getStyle('G'.$key_la)->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()->setARGB('EBF7DD');

            }
            $spreadsheet->getActiveSheet()->getStyle('C'.$border_start_la.':G'.($key_la))
            ->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        }


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="Report_HPP_'.$request->date_from.'-'.$request->date_to.'.xls"');
        $writer = new Xlsx($spreadsheet);
        $writer->save("php://output");
    }

    public function showReportHpp()
    {
        return $this
            ->setBreadcrumb('Report HPP', '#')
            ->viewAdmin('admin.master.general-journal.report-hpp', [
                'title' => 'Report HPP',
            ]);
    }

    public function showReportHppItems()
    {
        $margin_rate_auto = intval(GeneralSetting::where('name','margin_rate_auto')->value('value'));
        return $this
            ->setBreadcrumb('Report HPP Items', '#')
            ->viewAdmin('admin.master.general-journal.report-hpp-items', [
                'title' => 'Report HPP Items',
                'margin_rate_auto' => $margin_rate_auto
            ]);
    }

    public function showReportHppItemDetails($pdes_id, $month, $year)
    {
        $data = GeneralJournalDetail::select([
            DB::raw('(CEIL(SUM(general_journal_details.kredit) / SUM(general_journal_details.quantity))) AS hpp_average_month'),
            'part_data_stock_id'])
            ->with(['hpp_item' => function ($query) use($month, $year) {
                $query->where([
                    ['type', '=', 1], //type 1 mean Month
                    ['month', '=', $month],
                    ['year', '=', $year],
                ]);
            }])
            ->join('ode_part_data_stock', 'ode_part_data_stock.id', '=', 'general_journal_details.part_data_stock_id')
            ->where('part_data_stock_id', $pdes_id)
            ->where('category', 'material')
            ->whereMonth('general_journal_details.created_at', $month)
            ->whereYear('general_journal_details.created_at', $year)
            ->groupBy('part_data_stock_id')->whereNotNull('part_data_stock_id')
            ->first();
        $details = GeneralJournalDetail::with('part_data_stock_inventory')->where('part_data_stock_inventories_id', $pdes_id)->whereNotNull('part_data_stock_inventories_id')->get();
        return $this
            ->setBreadcrumb('Report HPP Items', '#')
            ->viewAdmin('admin.master.general-journal.report-hpp-item-details', [
                'title' => 'Report HPP Items Details',
                'month' => $month,
                'year' => $year,
                'details' => $details,
                'hpp_avg_month' => $data->hpp_average_month,
                'hpp_avg_month_manual' => $data->hpp_item != null ? $data->hpp_item->hpp_average_month_manual : 0,
                'margin_rate_auto' => $data->margin_rate_auto
            ]);
    }

    public function showReportHppItemDetailsYearly($pdes_id)
    {
        $data = GeneralJournalDetail::select([
            DB::raw('(CEIL(SUM(general_journal_details.kredit) / SUM(general_journal_details.quantity))) AS hpp_average_all'),
            DB::raw('(MIN(YEAR(general_journal_details.created_at))) AS min_year'),
            DB::raw('(MAX(YEAR(general_journal_details.created_at))) AS max_year'),
            'part_data_stock_id'])
            ->with(['hpp_item' => function ($query) {
                $query->where([
                    ['type', '=', 2], //type 1 mean Year
                ]);
            }])
            ->join('ode_part_data_stock', 'ode_part_data_stock.id', '=', 'general_journal_details.part_data_stock_id')
            ->where('part_data_stock_id', $pdes_id)
            ->where('category', 'material')
            ->groupBy('part_data_stock_id')->whereNotNull('part_data_stock_id')
            ->first();

        $details = GeneralJournalDetail::with('part_data_stock')->where('part_data_stock_id', $pdes_id)->whereNotNull('part_data_stock_id')->get();
        return $this
            ->setBreadcrumb('Report HPP Items Year', '#')
            ->viewAdmin('admin.master.general-journal.report-hpp-item-details-year', [
                'title' => 'Report HPP Items Details Yearly',
                'pdes_id' => $pdes_id,
                'details' => $details,
                'hpp_avg_all' => $data->hpp_average_all,
                'hpp_avg_all_manual' => $data->hpp_item != null ? $data->hpp_item->hpp_average_all_manual : 0,
                'margin_rate_auto' => $data->margin_rate_auto,
                'min_year' => $data->min_year,
                'max_year' => $data->max_year
            ]);
    }



}
