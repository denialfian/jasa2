<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Requests\TeknisiLoginB2bRequest;
use App\Services\TeknisiB2bLoginService;

class TeknisiB2bLoginController extends ApiController
{
    public function login(TeknisiLoginB2bRequest $request, TeknisiB2bLoginService $userService)
    {
        // dd('asd');
        if ($userService->login($request->all())) {
            // get user login
            $user = $userService->getUser();

            // check user verify
            // if ($userService->isVerify() == false) {
            //     // send response
            //     return $this->errorResponse($user, 'your account has not been validated');
            // }

            // get access token
            $token = $user->createToken($userService->getTokenName())->accessToken;

            // send response
            return $this->successResponse([
                'token' => $token,
                'user' => $user,
            ], 'login sukses');
        }

        // send response
        return $this->errorResponse(
            [],
            'login gagal',
            400
        );
    }
}
