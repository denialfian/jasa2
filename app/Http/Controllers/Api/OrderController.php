<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Model\Master\Order;
use Illuminate\Http\Request;
use App\Model\Master\Ewallet;
use App\Services\OrderService;
use App\Model\Master\Inventory;
use App\Model\Master\ItemDetail;
use App\Model\Master\HistoryOrder;
use Illuminate\Support\Facades\DB;
use App\Model\Master\ServiceDetail;
use App\Model\Master\TmpItemDetail;
use App\Model\Master\EwalletHistory;
use App\Model\Master\MsPriceService;
use App\Model\Technician\Technician;
use App\Services\Master\ChatService;
use Illuminate\Support\Facades\Auth;
use App\Model\Master\SparepartDetail;
use App\Model\Master\TechnicianSaldo;
use App\Http\Controllers\ApiController;
use App\Model\Master\OrderMediaProblem;
use Illuminate\Database\QueryException;
use App\Http\Requests\OrderMediaRequest;
use App\Model\Master\TechnicianSchedule;
use App\Model\Master\TmpSparepartDetail;
use App\Model\Master\TransactionHistory;
use Yajra\DataTables\Facades\DataTables;
use App\Services\Master\InventoryService;
use App\Services\Master\GaransiChatService;
use App\Http\Requests\RevisitGaransiRequest;
use App\Http\Requests\UpdateSparepartOrderRequest;


class OrderController extends ApiController
{
    public function reviewListDatatables(OrderService $orderService)
    {
        $query = $orderService->hasReview();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatables($technician_id = null, OrderService $orderService, Request $request)
    {

        return $orderService->datatables($request);
    }

    public function datatablesSearch(OrderService $orderService, Request $request)
    {
        return $orderService->datatables($request);
    }

    public function updateBillDetail(Request $request, $order_id, OrderService $order_service)
    {

        $order_service->orderLog('updateBillDetail', $request, $order_id);
        $response = Order::where('id', $order_id)->update([
            'address' => $request->bill_address,
            'bill_to' => $request->bill_to,
        ]);
        return $this->successResponse($response, $this->successUpdateMsg());
    }

    public function assignNewTeknisi(Request $request, $service_detail_id, OrderService $order_service)
    {
        $unit = 1;
        $price_service = MsPriceService::with('technician.user')
            ->where('id', $request->teknisi_ms_price_services_id)
            ->where('technicians_id', $request->technician_id)
            ->firstOrFail();

        $data_update = [
            'teknisi_json' => json_encode($price_service->technician),
            'teknisi_email' => $price_service->technician->user->email,
            'teknisi_phone' => $price_service->technician->user->phone,
            'technicians_id' => $request->technician_id,
            'ms_price_services_id'   => $price_service->id,
            'price'   => $price_service->value,
        ];

        $service_detail = ServiceDetail::where('id', $service_detail_id)->firstOrFail();
        $total_service_detail = $price_service->value * $unit;

        $sparepart_details = SparepartDetail::where('orders_id', $service_detail->orders_id)->get();
        $total_sparepart_detail = $sparepart_details->sum(function ($row) {
            return ($row->price * $row->quantity);
        });
        $grand_total = $total_service_detail + $total_sparepart_detail;
        $result_deduction = ($unit * $price_service->commission_deduction);

        $end_work = Carbon::parse($request->schedule)->addHour($request->estimation_hours);
        $data_schedule = [
            'order_id' => $service_detail->orders_id,
            'technician_id' => $request->technician_id,
            'schedule_date' => $request->schedule,
            'start_work' => $request->schedule,
            'end_work' => $end_work,
        ];

        DB::beginTransaction();
        try {
            $service_detail->update($data_update);
            $order = Order::where('id', $service_detail->orders_id)->update([
                'schedule' => $request->schedule . ' ' . $request->hours,
                'grand_total' => $grand_total,
                'after_commission' => $total_sparepart_detail > 0 ? ($total_sparepart_detail + $result_deduction) : $result_deduction,
            ]);
            // inser schedule technician
            $technician_schedule = TechnicianSchedule::create($data_schedule);
            $order_service->orderLog('assignNewTeknisi', $request, $service_detail->orders_id);

            TechnicianSaldo::where('order_id', $service_detail->orders_id)->update([
                'technician_id' => $request->technician_id,
            ]);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
        return $this->successResponse($order, $this->successUpdateMsg());
    }

    public function garansiRevisit(RevisitGaransiRequest $request, $service_detail_id, GaransiChatService $garansiChatService)
    {
        return $this->successResponse($garansiChatService->revisit($request, $service_detail_id), $this->successUpdateMsg());
    }

    public function updateSparepartDetail(Request $request, $order_id, OrderService $order_service)
    {
        return $this->successResponse($order_service->updateSparepart($order_id, $request), $this->successUpdateMsg(), 200);
    }

    public function updateSparepartDetailBeforeApprove(UpdateSparepartOrderRequest $request, $order_id, InventoryService $inventoryService)
    {
        $teknisi = Technician::where('user_id', Auth::id())->firstOrFail();
        $order = Order::with(['orderpayment.orderhistory', 'user.masterwallet.history', 'service_detail'])
            ->where('id', $order_id)
            ->whereHas('service_detail', function ($q) use ($teknisi) {
                $q->where('technicians_id', $teknisi->id);
            })
            ->firstOrFail();

        $service_details = $order->service_detail;
        $total_service_detail = $service_details->sum(function ($row) {
            return ($row->price * $row->unit);
        });
        $total_service = 0;
        foreach ($order->service_detail as $key => $res) {
            $total_service += $res->price;
        }

        $sparepart_detail_total_update = 0;
        $sparepart_detail_total_insert = 0;

        DB::beginTransaction();
        try {
            if ($request->type_part == 'my-inventory') {
                $sparepart_detail_id = $request->sparepart_detail_id;
                $tmp_sparepart_id = $request->tmp_sparepart_id;
                $teknisi_sparepart_name = $request->teknisi_sparepart_name;
                $teknisi_sparepart_price = $request->teknisi_sparepart_price;
                $teknisi_sparepart_qty = $request->teknisi_sparepart_qty;
                // return $request->all();
                foreach ($sparepart_detail_id as $key => $value) {
                    // dd($value);
                    $check_part = !empty($teknisi_sparepart_name[$key]) && !empty($teknisi_sparepart_price[$key]) && !empty($teknisi_sparepart_qty[$key]);
                    if ($check_part) {
                        if ($value != 0) {
                            if ($order->orders_statuses_id != 4) {
                                SparepartDetail::where('id', $value)->update([
                                    'name_sparepart' => $teknisi_sparepart_name[$key],
                                    'quantity' => $teknisi_sparepart_qty[$key],
                                    'price' => $teknisi_sparepart_price[$key],
                                ]);
                            }

                            $sparepart_detail_total_update += $teknisi_sparepart_price[$key] * $teknisi_sparepart_qty[$key];
                        } else {
                            if ($order->orders_statuses_id != 4) {
                                SparepartDetail::create([
                                    'orders_id' => $order->id,
                                    'technicians_id' => $service_details[0]->technicians_id,
                                    'name_sparepart' => $teknisi_sparepart_name[$key],
                                    'quantity' => $teknisi_sparepart_qty[$key],
                                    'price' => $teknisi_sparepart_price[$key],
                                ]);
                                $sparepart_detail_total_insert += $teknisi_sparepart_price[$key] * $teknisi_sparepart_qty[$key];
                            }

                            if ($order->orders_statuses_id == 4 && $tmp_sparepart_id[$key] == 0) {
                                TmpSparepartDetail::create([
                                    'orders_id' => $order->id,
                                    'technicians_id' => $service_details[0]->technicians_id,
                                    'name_sparepart' => $teknisi_sparepart_name[$key],
                                    'quantity' => $teknisi_sparepart_qty[$key],
                                    'price' => $teknisi_sparepart_price[$key],
                                ]);
                            }
                        }
                    }
                }


                $sparepart_details = SparepartDetail::where('orders_id', $order_id)->get();

                $total_sparepart_detail = $sparepart_details->sum(function ($row) {
                    return ($row->price * $row->quantity);
                });
            } else {
                $item_detail_id = $request->item_detail_id;
                $inventory_id = $request->inventory_id;
                $inventory_qty = $request->inventory_qty;
                foreach ($item_detail_id as $key => $val) {
                    $check_inventory = !empty($inventory_id[$key]) && !empty($inventory_qty[$key]);
                    if ($check_inventory) {
                        $inventory = Inventory::where('id', $inventory_id[$key])->firstOrFail();
                        if ($inventory_qty[$key] > $inventory->stock_available) {
                            throw new \Exception($inventory->item_name . ' melebihi qty yg tersedia');
                        }

                        if ($val == 0) {
                            $inventoryService->mutationLogsProcess($inventory_id[$key], null, $inventory_qty[$key], $order_id, $teknisi->id);

                            $inventory->update([
                                // 'stock_out' => $inventory->stock_out + $inventory_qty[$key]
                                'stock_now' => $inventory->stock_now - $inventory_qty[$key]
                            ]);

                            if ($order->orders_statuses_id != 4) {
                                ItemDetail::create([
                                    'orders_id'      => $order_id,
                                    'inventory_id'   => $inventory->id,
                                    'name_product'   => $inventory->item_name,
                                    'quantity'       => $inventory_qty[$key],
                                    'price'          => $inventory->cogs_value,
                                ]);
                            }

                            if ($order->orders_statuses_id == 4) {
                                TmpItemDetail::create([
                                    'inventory_id'   => $inventory->id,
                                    'orders_id'      => $order_id,
                                    'name_product'   => $inventory->item_name,
                                    'quantity'       => $inventory_qty[$key],
                                    'price'          => $inventory->cogs_value,
                                ]);
                            }

                            $sparepart_detail_total_insert += $inventory->cogs_value * $inventory_qty[$key];
                        } else {
                            $itemD = ItemDetail::where('id', $val)->first();

                            // Proses replace log update dari before apporve
                            $inventoryService->deleteMutationHistories($order_id, $request->technician_id, $inventory_id[$key]);
                            $inventoryService->mutationLogsProcess($inventory_id[$key], null, $inventory_qty[$key], $order_id, $request->technician_id);

                            $inventory->update([
                                // 'stock_out' => $inventory->stock_out - ($itemD->quantity - $inventory_qty[$key])
                                'stock_now' => $inventory->stock_now + ($itemD->quantity - $inventory_qty[$key])
                            ]);

                            $itemD->update([
                                'inventory_id'   => $inventory->id,
                                'name_product'   => $inventory->item_name,
                                'quantity'       => $inventory_qty[$key],
                                'price'          => $inventory->cogs_value,
                            ]);

                            $sparepart_detail_total_update += $inventory->cogs_value * $inventory_qty[$key];
                        }
                    }
                }

                $sparepart_details = ItemDetail::where('orders_id', $order_id)->get();
                // return ['asd', $sparepart_details];
                $total_sparepart_detail = $sparepart_details->sum(function ($row) {
                    return ($row->price * $row->quantity);
                });
            }

            $grand_total = $total_service_detail + $total_sparepart_detail;
            // return ['grand total', $grand_total, 'total service detail', $total_service_detail, 'total sparepart detail', $total_sparepart_detail, 'sparepart_details', $sparepart_details];
            $result_deduction = ($service_details[0]->unit * $service_details[0]->price_services->commission_deduction);
            // $getWalletCustomer = $order->user->masterwallet->nominal;

            // if ($value != 0) {
            //     $hargaSparepart = $sparepart_detail_total_update; // return ['sparepart_detail_total_update =', $hargaSparepart];
            // }else{
            //     $hargaSparepart = $sparepart_detail_total_insert; // return ['sparepart_detail_total_insert =', $hargaSparepart];
            // }

            if ($order->orders_statuses_id == 4) {
                if ($order->payment_type == 1) {
                    if ($order->user->masterwallet == null) {
                        throw new \Exception('This user does not have a wallet balance');
                    }
                }
            } else {
                // return 'ke else';
                $order->update([
                    'grand_total' => $grand_total,
                    // 'after_commission' => $total_sparepart_detail > 0 ? ($total_sparepart_detail + $result_deduction) : $result_deduction,
                ]);
            }


            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        return $this->successResponse($order, $this->successUpdateMsg());
    }

    public function approvedChangeSpareparts(Request $request, $get_id_order)
    {
        DB::beginTransaction();
        try {
            $order = Order::with([
                'tmp_sparepart',
                'tmp_item_detail',
                'orderpayment.orderhistory',
                'user.masterwallet.history',
                'service_detail.price_services'
            ])
            ->where('id', $get_id_order)
            ->firstOrFail();

            if(count($order->tmp_sparepart) > 0){
                $store_parts = json_encode($order->tmp_sparepart);
            }elseif(count($order->tmp_item_detail) > 0){
                $store_parts = json_encode($order->tmp_item_detail);
            }else{
                $store_parts = null;
            }

            $getTmpSpareparts = TmpSparepartDetail::where('orders_id', $get_id_order)->get();
            $getTmpItemDetail = TmpItemDetail::where('orders_id', $get_id_order)->get();
            $sparepartDetail = SparepartDetail::where('orders_id', $get_id_order)->get();
            $itemDetail = ItemDetail::where('orders_id', $get_id_order)->get();
            $getId = TmpSparepartDetail::where('orders_id', $get_id_order)->get()->pluck('id');
            $getIdItems = TmpItemDetail::where('orders_id', $get_id_order)->get()->pluck('id');
            $result_deduction = ($order->service_detail[0]->unit * $order->service_detail[0]->price_services->commission_deduction);
            $getIdHistoryPayment = $order->orderpayment->wallet_history_id;
            $getTotalService = $order->orderpayment->orderhistory->nominal;
            $getWalletCustomer = $order->user->masterwallet->nominal;
            $service_details = $order->service_detail;

            if (count($getTmpSpareparts) > 0) {
                // return 'masukan ke table Sparepart detail';
                $created = [];
                $getTotal = 0;
                foreach ($getTmpSpareparts as $key => $value) {
                    $getTotal += $value->price * $value->quantity;
                    $created[] = SparepartDetail::insert([
                        'orders_id' => $value->orders_id,
                        'technicians_id' => $service_details[0]->technicians_id,
                        'name_sparepart' => $value->name_sparepart,
                        'price' =>  $value->price,
                        'quantity' => $value->quantity,
                    ]);
                }
                // return $order->grand_total + $getTotal;

                $order->update([
                    'grand_total' => $order->grand_total + $getTotal,
                    'after_commission' => $getTotal > 0 ? ($getTotal + $order->after_commission) : $order->after_commission,
                ]);
                EwalletHistory::where('id', $getIdHistoryPayment)->update([
                    'nominal' => $getTotal + $getTotalService,
                    'saldo' => $getWalletCustomer - $getTotal,
                ]);
                Ewallet::where('user_id', $order->user->id)->update([
                    'nominal' => $getWalletCustomer - $getTotal,
                ]);

                // transaction History
                $transactionHistory = TransactionHistory::create([
                    'type_history'      => 'Additional Spareparts',
                    'order_code'        => $order->code,
                    'order_id'          => $order->id,
                    'service_type'      => $order->service->name,
                    'symptom_name'      => $order->symptom->name,
                    'product_group_name' => $order->product_group_name,
                    'price'             => $getTotal,
                    'beginning_balance' => $order->user->ewallet->nominal + $getTotal,
                    'ending_balance'    => $order->user->ewallet->nominal,
                    'number_of_pieces'  => $getTotal,
                    'status'            => 4,
                    'technician_name'   => $request->teknisi_id,
                    'parts'             => $store_parts
                ]);

                foreach ($getTmpSpareparts as $key => $value) {
                    TmpSparepartDetail::where('id', $getId[$key])->delete();
                }
            }
            if (count($getTmpItemDetail) > 0) {
                // return 'masukan ke table item detail';
                $created = [];
                $getTotalItems = 0;
                foreach ($getTmpItemDetail as $key => $value) {
                    $getTotalItems += $value->price * $value->quantity;
                    $created[] = ItemDetail::create([
                        'orders_id'      => $value->orders_id,
                        'inventory_id'   => $value->inventory_id,
                        'name_product'   => $value->name_product,
                        'quantity'       => $value->quantity,
                        'price'          => $value->price,
                    ]);
                }
                $order->update([
                    'grand_total' => $order->grand_total + $getTotalItems,
                    'after_commission' => $getTotalItems > 0 ? ($getTotalItems + $order->after_commission) : $order->after_commission,
                ]);
                EwalletHistory::where('id', $getIdHistoryPayment)->update([
                    'nominal' => $getTotalItems + $getTotalService,
                    'saldo' => $getWalletCustomer - $getTotalItems,
                ]);
                Ewallet::where('user_id', $order->user->id)->update([
                    'nominal' => $getWalletCustomer - $getTotalItems,
                ]);

                // transaction History
                $transactionHistory = TransactionHistory::create([
                    'type_history'      => 'Additional Spareparts',
                    'order_code'        => $order->code,
                    'order_id'          => $order->id,
                    'service_type'      => $order->service->name,
                    'symptom_name'      => $order->symptom->name,
                    'product_group_name' => $order->product_group_name,
                    'price'             => $getTotalItems,
                    'beginning_balance' => $order->user->ewallet->nominal + $getTotalItems,
                    'ending_balance'    => $order->user->ewallet->nominal,
                    'number_of_pieces'  => $getTotalItems,
                    'status'            => 4,
                    'technician_name'   => $request->teknisi_id,
                    'parts'             => $store_parts
                ]);

                foreach ($getTmpItemDetail as $key => $value) {
                    TmpItemDetail::where('id', $getIdItems[$key])->delete();
                }
            }
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new SendErrorMsgException($e->getMessage());
        }
    }

    public function destroySparepartDetail(Request $request, $sparepart_detail_id, OrderService $order_service)
    {
        $part = SparepartDetail::where('id', $sparepart_detail_id)->firstOrFail();
        // $order = Order::where('id', $part->orders_id)->firstOrFail();
        $order = Order::with([
            'orderpayment.orderhistory',
            'user.masterwallet.history',
            'service_detail',
            'order_status'
        ])->where('id', $part->orders_id)
            ->firstOrFail();
        $service_details = $order->service_detail;
        $sparepart_details = $order->sparepart_detail;
        $total_service_detail = $service_details->sum(function ($row) {
            return ($row->price * $row->unit);
        });
        $total_sparepart_detail = $sparepart_details->sum(function ($row) {
            return ($row->price * $row->quantity);
        });
        $total_service = 0;
        foreach ($order->service_detail as $key => $res) {
            $total_service += $res->price;
        }
        $total_sparepart_detail = $total_sparepart_detail - ($part->price * $part->quantity);
        $grand_total = $total_service_detail + $total_sparepart_detail;
        $result_deduction = ($service_details[0]->unit * $service_details[0]->price_services->commission_deduction);

        DB::beginTransaction();
        try {
            if ($order->orders_statuses_id == 2 || $order->orders_statuses_id == 1) {
                // return 'hapus aja ';
                $order->update([
                    'grand_total' => $grand_total,
                    'after_commission' => $total_sparepart_detail > 0 ? ($total_sparepart_detail + $result_deduction) : $result_deduction,
                ]);
                $part->delete();
            } else {
                // return 'hapus + part balik lagi';
                $order_service->orderLog('destroySparepartDetail', $part, $part->orders_id);
                $order->update([
                    'grand_total' => $grand_total,
                    'after_commission' => $total_sparepart_detail > 0 ? ($total_sparepart_detail + $result_deduction) : $result_deduction,
                ]);

                if ($order->payment_type == '1') {
                    if ($order->user->masterwallet == null) {
                        throw new \Exception('This user does not have a wallet balance');
                    }
                    $getWalletCustomer = $order->user->masterwallet->nominal;
                    $getIdHistoryPayment = $order->orderpayment->wallet_history_id;
                    EwalletHistory::where('id', $getIdHistoryPayment)->update([
                        'nominal' => $order->orderpayment->orderhistory->nominal - $part->price * $part->quantity,
                        'saldo' => $getWalletCustomer + $part->price * $part->quantity,
                    ]);
                    Ewallet::where('user_id', $order->user->id)->update([
                        'nominal' => $getWalletCustomer + $part->price * $part->quantity,
                    ]);
                }

                $part->delete();
            }

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        return $this->successResponse([
            'grand_total' => $grand_total,
            'uniq' => time(),
        ], $this->successDeleteMsg(), 200);
    }

    public function destroySparepartDetailByTeknisi($sparepart_detail_id, OrderService $order_service)
    {
        $teknisi = Technician::where('user_id', Auth::id())->firstOrFail();
        $part = SparepartDetail::where('id', $sparepart_detail_id)->firstOrFail();

        $order = Order::with([
            'orderpayment.orderhistory',
            'user.masterwallet.history',
            'service_detail',
            'order_status'
        ])->where('id', $part->orders_id)
            ->whereHas('service_detail', function ($q) use ($teknisi) {
                $q->where('technicians_id', $teknisi->id);
            })
            ->firstOrFail();
        $service_details = $order->service_detail;
        $sparepart_details = $order->sparepart_detail;
        $total_service_detail = $service_details->sum(function ($row) {
            return ($row->price * $row->unit);
        });
        $total_sparepart_detail = $sparepart_details->sum(function ($row) {
            return ($row->price * $row->quantity);
        });
        $total_service = 0;
        foreach ($order->service_detail as $key => $res) {
            $total_service += $res->price;
        }
        $total_sparepart_detail = $total_sparepart_detail - ($part->price * $part->quantity);
        $grand_total = $total_service_detail + $total_sparepart_detail;
        $result_deduction = ($service_details[0]->unit * $service_details[0]->price_services->commission_deduction);

        DB::beginTransaction();
        try {
            if ($order->orders_statuses_id == 2 || $order->orders_statuses_id == 1) {
                // return 'hapus aja ';
                $order->update([
                    'grand_total' => $grand_total,
                    'after_commission' => $total_sparepart_detail > 0 ? ($total_sparepart_detail + $result_deduction) : $result_deduction,
                ]);
                $part->delete();
            } else {
                // return 'hapus + part balik lagi';
                $order_service->orderLog('destroySparepartDetail', $part, $part->orders_id);
                $order->update([
                    'grand_total' => $grand_total,
                    'after_commission' => $total_sparepart_detail > 0 ? ($total_sparepart_detail + $result_deduction) : $result_deduction,
                ]);

                if ($order->payment_type == '1') {
                    if ($order->user->masterwallet == null) {
                        throw new \Exception('This user does not have a wallet balance');
                    }
                    $getWalletCustomer = $order->user->masterwallet->nominal;
                    $getIdHistoryPayment = $order->orderpayment->wallet_history_id;
                    EwalletHistory::where('id', $getIdHistoryPayment)->update([
                        'nominal' => $order->orderpayment->orderhistory->nominal - $part->price * $part->quantity,
                        'saldo' => $getWalletCustomer + $part->price * $part->quantity,
                    ]);
                    Ewallet::where('user_id', $order->user->id)->update([
                        'nominal' => $getWalletCustomer + $part->price * $part->quantity,
                    ]);
                }

                $part->delete();
            }

            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        return $this->successResponse([
            'grand_total' => $grand_total,
            'uniq' => time(),
        ], $this->successDeleteMsg(), 200);
    }

    public function destroyTmpSparepartDetailByTeknisi($sparepart_detail_id, OrderService $order_service)
    {
        $part = TmpSparepartDetail::where('id', $sparepart_detail_id)->firstOrFail();

        DB::beginTransaction();
        try {
            $part->delete();
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        return $this->successResponse([
            'uniq' => time(),
        ], $this->successDeleteMsg(), 200);
    }

    public function destroyTmpItemDetailByTeknisi($sparepart_detail_id, OrderService $order_service)
    {
        $part = TmpItemDetail::where('id', $sparepart_detail_id)->firstOrFail();
        $inventory = Inventory::where('id', $part->inventory_id)->firstOrFail();

        DB::beginTransaction();
        try {
            $inventory->update([
                // 'stock_out' => $inventory->stock_out - $part->quantity
                'stock_now' => $inventory->stock_now + $part->quantity
            ]);
            $part->delete();
            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        return $this->successResponse([
            'uniq' => time(),
        ], $this->successDeleteMsg(), 200);
    }

    public function destroyItemDetail(Request $request, $item_detail_id, OrderService $order_service, InventoryService $inventoryService)
    {
        $part = ItemDetail::where('id', $item_detail_id)->firstOrFail();
        // $order = Order::where('id', $part->orders_id)->firstOrFail();
        $order = Order::with([
            'orderpayment.orderhistory',
            'user.masterwallet.history',
            'service_detail',
            'order_status'
        ])->where('id', $part->orders_id)
            ->firstOrFail();

        $inventory = Inventory::where('id', $part->inventory_id)->firstOrFail();
        $service_details = $order->service_detail;
        $item_details = $order->item_detail;
        $total_service_detail = $service_details->sum(function ($row) {
            return ($row->price * $row->unit);
        });
        $total_item_detail = $item_details->sum(function ($row) {
            return ($row->price * $row->quantity);
        });
        $total_service = 0;
        foreach ($order->service_detail as $key => $res) {
            $total_service += $res->price;
        }
        $total_sparepart_detail = $total_item_detail - ($part->price * $part->quantity);
        $grand_total = $total_service_detail + $total_sparepart_detail;
        $result_deduction = ($service_details[0]->unit * $service_details[0]->price_services->commission_deduction);
        DB::beginTransaction();
        try {
            if ($order->orders_statuses_id == 2 || $order->orders_statuses_id == 1) {
            } else {
                $order_service->orderLog('destroyItemDetail', $part, $part->orders_id);

                if ($order->payment_type == '1') {
                    if ($order->user->masterwallet == null) {
                        throw new \Exception('This user does not have a wallet balance');
                    }
                    $getWalletCustomer = $order->user->masterwallet->nominal;
                    $getIdHistoryPayment = $order->orderpayment->wallet_history_id;
                    EwalletHistory::where('id', $getIdHistoryPayment)->update([
                        'nominal' => $order->orderpayment->orderhistory->nominal - $part->price * $part->quantity,
                        'saldo' => $getWalletCustomer + $part->price * $part->quantity,
                    ]);
                    Ewallet::where('user_id', $order->user->id)->update([
                        'nominal' => $getWalletCustomer + $part->price * $part->quantity,
                    ]);
                }
            }

            $order->update([
                'grand_total' => $grand_total,
                'after_commission' => $total_sparepart_detail > 0 ? ($total_sparepart_detail + $result_deduction) : $result_deduction,
            ]);
            $inventoryService->mutationLogsProcess(null, $part->inventory_id, $part->quantity, $order->id, $order->service_detail[0]->technicians_id, 'cancel_order_true');
            $inventory->update([
                // 'stock_out' => $inventory->stock_out - $part->quantity
                'stock_now' => $inventory->stock_now + $part->quantity
            ]);


            $part->delete();


            DB::commit();
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        return $this->successResponse([
            'grand_total' => $grand_total,
            'uniq' => time(),
        ], $this->successDeleteMsg(), 200);
    }

    /**
     * Accept Job
     */
    public function acceptJob(Request $request, $order_id, OrderService $orderService)
    {
        return $this->successResponse($orderService->setOrder($order_id)->changeToWaitingApproval($request), $this->successUpdateMsg(), 200);
    }

    /**
     * reject Job
     */
    public function rejectJob(Request $request, $order_id, OrderService $orderService)
    {
        return $this->successResponse($orderService->setOrder($order_id)->changeToCancel($request), $this->successUpdateMsg(), 200);
    }

    /**
     * Accept Job
     */
    public function acceptJobByCustomer(Request $request, $order_id, OrderService $orderService)
    {
        return $this->successResponse($orderService->setOrder($order_id)->changeToProcessing($request), $this->successUpdateMsg(), 200);
    }

    /**
     * on working
     */
    public function toOnWorking($order_id, OrderService $orderService, Request $request)
    {
        return $this->successResponse($orderService->setOrder($order_id)->changeToOnWorking($request), $this->successUpdateMsg(), 200);
    }

    /**
     * job complate
     */
    public function toComplateJob($order_id, OrderService $orderService, Request $request)
    {
        return $this->successResponse($orderService->setOrder($order_id)->changeToComplate($request), $this->successUpdateMsg(), 200);
    }

    /**
     * job done
     */
    public function toDoneJob($order_id, Request $request, OrderService $orderService)
    {
        return $this->successResponse($orderService->setOrder($order_id)->changeToDone($request), $this->successUpdateMsg(), 200);
    }

    /**
     * Media Order Upload
     */
    public function uploadMedia($order_id, OrderMediaRequest $request, OrderService $orderService)
    {
        return $this->successResponse($orderService->addMediaProblem($order_id, $request), $this->successUpdateMsg(), 200);
    }
}
