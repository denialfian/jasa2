<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\UserProfileImageRequest;
use App\Http\Requests\UserAddressRequest;
use App\Http\Requests\UserChangePasswordRequest;
use App\Http\Requests\UserInfoRequest;
use App\Http\Requests\UserStoreRequest;
use App\Model\Technician\Technician;
use App\Http\Requests\UserUpdateRequest;
use App\Services\OtpService as ServicesOtpService;
use App\Services\UserService;
use App\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Hash;

class UserController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('info');
        return $this->successResponse($users->paginate(10));
    }

    /**
     * use datatables json structure
     *
     * @return \Illuminate\Http\Response
     */
    public function datatables()
    {
        $users = User::whereNull('is_b2b_users')->with('info', 'roles', 'badge')->orderBy('name', 'asc');

        return DataTables::of($users)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * use select2 json structure
     *
     * @return \Illuminate\Http\Response
     */
    public function select2(Request $request)
    {
        $response = User::where('name', 'like', '%' . $request->q . '%')
            ->where('badge_id', null)
            ->limit(20)
            ->orderBy('name', 'asc')
            ->get();

        return $this->successResponse($response);
    }

    public function select2Teknisi(Request $request)
    {
        $response = User::with(['ewallet', 'info', 'address', 'addresses.types'])
            ->where(function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->q . '%')->orWhere('email', 'like', '%' . $request->q . '%');
            })
            ->where('status', 1)
            ->whereHas('addresses')
            ->role('Customer')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    public function updateStatus(UserService $userService, $id)
    {
        $user = User::where('id', $id)->first();

        if ($user->status == 7) {
            $userService->sentNotificationApprove($user);
            User::where('id', $id)->update([
                'is_login_as_teknisi' => null,
                'status' => 1,
            ]);

            Technician::create([
                'user_id' => $user->id,
            ]);
            $role = Role::where('name', 'Technician')->firstOrFail();
            $user->assignRole($role);
            $user->roles;
        }
        return $this->successResponse($user, 'success Approve');
    }

    public function updateStatusReject(Request $request, UserService $userService, $id)
    {
        // return $this->successResponse($request->all(), 'success Rejected', 400);
        $user = User::where('id', $id)->first();
        $userService->sentNotificationReject($user, $request);
        $user = User::where('id', $id)->first();

        // if ($request->note_reject == null) {
        $userService->sentNotificationApprove($user);
        User::where('id', $id)->update([
            'status' => 8,
            'note_reject' => $request->note_reject
        ]);
        // }
        return $this->successResponse('success Rejected');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request, UserService $userService)
    {
        // create user
        $user = $userService->dontSendNotif()->createFromAdmin($request->all());

        // generate token
        $token = $user->createToken($userService->getTokenName())->accessToken;

        // response
        $response = [
            'user'  => $user,
            'token' => $token,
        ];

        return $this->successResponse($response, $this->successStoreMsg(), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = User::with('info')->where(['id' => $id])->firstOrFail();

        return $this->successResponse($users);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserService $userService, UserUpdateRequest $request, $id)
    {
        // validasi
        $request->validated();

        // user
        $user = User::where('id', $id)->firstOrFail();

        // update user
        $test = $userService
            ->update($request->all(), $user);

        // generate token
        $token = $user->createToken($userService->getTokenName())->accessToken;

        // set response
        $response = [
            'user'  => $user,
            'token' => $token,
        ];

        // send response
        return $this->successResponse($response, $this->successUpdateMsg());
    }

    public function changePassword(UserService $userService, UserChangePasswordRequest $request, $id)
    {
        // user
        $user = User::where('id', $id)->firstOrFail();

        $user->update([
            'password' => Hash::make($request->new_password)
        ]);

        // send response
        return $this->successResponse($user, $this->successUpdateMsg());
    }

    public function changStatus(Request $request, $id)
    {
        // user
        $user = User::where('id', $id)->firstOrFail();

        $user->update([
            'status' => $user->status == 1 ? 0 : 1
        ]);

        // send response
        return $this->successResponse($user, $this->successUpdateMsg());
    }


    /**
     * insert or update user info
     */
    public function info(UserService $userService, UserInfoRequest $request)
    {
        $response = $userService->createOrUpdateInfo($request);

        // send response
        return $this->successResponse($response, $this->successUpdateMsg());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, UserService $userService)
    {
        return $this->successResponse(
            $userService->deleteAll($id),
            'success delete',
            204
        );
    }

    public function otpSend(Request $request, ServicesOtpService $service)
    {

        $user = User::where('id', Auth::user()->id)->first();
        $otp = $service->create();
        $otp->setLabel($user->id);

        $token = $otp->now();

        // $msg = $service->sendSMS([
        //     'to' => $user->phone,
        //     'text' => $token,
        // ]);

        $user->update([
            'otp_verified_at' => null,
            'otp_code' => $token,
            'otp_url'  => $otp->getProvisioningUri(),
            'text'     => 'test OTP HardCode',
        ]);

        return $this->successResponse(
            $user,
            'success',
            201
        );
    }

    public function updatePhoneNumber(Request $request)
    {
        // dd($request->all());
        User::where('id', Auth::user()->id)->update([
            'otp_verified_at' => null,
            'otp_code'        => null,
        ]);

        User::where('id', Auth::user()->id)->update([
            'phone' => str_replace(' ', '', $request->phone), //$request->phone, //6281318737506,
        ]);
    }

    /**
     * insert or update user info
     */
    public function addressCreate(UserService $userService, UserAddressRequest $request)
    {

        $response = $userService->addressCreate($request);
        // send response
        return $this->successResponse($response, $this->successStoreMsg());
    }

    /**
     * update main Address
     */
    public function mainAddress(UserService $userService, $id)
    {
        $response = $userService->mainAddress($id);

        // send response
        return $this->successResponse($response, $this->successUpdateMsg());
    }

    /**
     * insert or update user info
     */
    public function updateAddress(UserAddressRequest $request, UserService $userService, $id)
    {
        $response = $userService->updateAddress($request);

        // send response
        return $this->successResponse($response, $this->successUpdateMsg());
    }

    /**
     * update profile image
     */
    public function updateProfileImage(UserProfileImageRequest $request, UserService $userService, $id)
    {
        $response = $userService->updateProfileImage($request);

        // send response
        return $this->successResponse($response, $this->successUpdateMsg());
    }
}
