<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\PermissionRequest;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Yajra\DataTables\DataTables;

class PermissionController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $response = Permission::paginate(10);

        return $this->successResponse($response);
    }

    public function select2(Request $request)
    {
        $response = Permission::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * use datatables json structure
     *
     * @return \Illuminate\Http\Response
     */
    public function datatables()
    {
        return DataTables::of(Permission::query())
            ->AddIndexColumn()
            ->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            Permission::create(['name' => $request->name]),
            'success',
            201
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = Permission::where(['id' => $id])->first();

        return $this->successResponse($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionRequest $request, $id)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            Permission::where(['id' => $id])
                ->first()
                ->update(['name' => $request->name])
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->successResponse(
            Permission::where(['id' => $id])->first()->delete(),
            'success',
            204
        );
    }
}
