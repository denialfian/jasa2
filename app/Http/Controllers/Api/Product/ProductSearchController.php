<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\ApiController;
use App\Services\Product\ProductSearchService as Service;
use Illuminate\Http\Request;

class ProductSearchController extends ApiController
{
    // get list vendor by product_id
    public function getVendorList(Service $service, $product_id)
    {
        return $this->successResponse(
            $service->getVendorList($product_id)
        );
    }

    // get list varian by product_id, vendor id
    public function getVendorVarianList(Service $service, $product_id, $vendor_id)
    {
        return $this->successResponse(
            $service->getVendorVarianList($product_id, $vendor_id)
        );
    }

    // get one varian
    public function getVarian(Service $service, Request $request)
    {
        return $this->successResponse(
            $service->getVarian($request)
        );
    }

    // get one varian
    public function findVarian(Service $service, $id)
    {
        return $this->successResponse(
            $service->findVarian($id)
        );
    }

    // get one varian
    public function findVendor(Service $service, $id)
    {
        return $this->successResponse(
            $service->findVendor($id)
        );
    }


    // get product by sku
    public function getProductBySKU(Service $service, Request $request)
    {
        return $this->successResponse(
            $service->getProductBySKU($request)
        );
    }
}
