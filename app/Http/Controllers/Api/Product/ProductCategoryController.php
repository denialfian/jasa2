<?php

namespace App\Http\Controllers\Api\Product;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Product\ProductCategoryRequest;
use App\Model\Master\MsProductCategory;
use App\Services\Master\ProductCategoryService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProductCategoryController extends ApiController
{
    /**
     * show list with paginate
     */

    public function index(ProductCategoryService $ProductCategoryService)
    {
        $response = $ProductCategoryService->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $response = MsProductCategory::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(ProductCategoryService $ProductCategoryService)
    {
        $query = $ProductCategoryService->list();

        return DataTables::of($query)
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(ProductCategoryRequest $request, ProductCategoryService $ProductCategoryService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $ProductCategoryService->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(MsProductCategory $productCategory)
    {
        $response = $productCategory;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(ProductCategoryRequest $request, MsProductCategory $productCategory, ProductCategoryService $ProductCategoryService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $ProductCategoryService->setProductCategory($productCategory)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(MsProductCategory $productCategory, ProductCategoryService $ProductCategoryService)
    {
        return $this->successResponse(
            $ProductCategoryService->setProductCategory($productCategory)->delete(),
            'success',
            204
        );
    }

    public function destroyBatch(Request $request, ProductCategoryService $ProductCategoryService)
    {
        return $this->successResponse(
            $ProductCategoryService->deleteById($request->id),
            'success',
            204
        );
    }

    public function updateMoveCategory(Request $request, ProductCategoryService $ProductCategoryService)
    {
        return $ProductCategoryService->updateMoveCategory($request);
    }
}
