<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Services\Master\UnitTypesService as Service;
use App\Http\Requests\Master\UnitTypesRequest;
use App\Model\Master\UnitTypes;

class UnitTypesController extends ApiController
{
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);
        return $this->successResponse($response);
    }

    public function select2(Request $request, Service $service)
    {
       return $this->successResponse($service->select2($request));
    }

    public function dataTables(Service $service)
    {
        return $service->datatables();
    }

    public function store(UnitTypesRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    public function update(UnitTypesRequest $request, UnitTypes $unitType ,Service $service)
    {
        return $this->successResponse(
            $service->setUnitType($unitType)->update($request->all()),
            'success'
        );
    }

    public function show(UnitTypes $unitType)
    {
        $response = $unitType;

        return $this->successResponse($response);
    }

    public function destroy(UnitTypes $unitType ,Service $service)
    {
        return $this->successResponse(
            $service->setUnitType($unitType)->delete(),
            'success',
            204
        );
    }
}
