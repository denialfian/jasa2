<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Model\Master\Curriculum;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\AdminController;
use App\Http\Requests\Master\CurriculumRequest;
use App\Services\Master\CurriculumService as Service;

class CurriculumController extends ApiController
{
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);
        return $this->successResponse($response);
    }

    public function select2(Request $request)
    {
        $a = new AdminController();
        $response = Curriculum::where('curriculum_name', 'like', '%' . $request->q . '%')

            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    public function datatables(Service $service)
    {
        $query = $service->list();

        return DataTables::of($query)
            ->toJson();
    }

    public function store(Service $service, CurriculumRequest $request)
    {
        $request->validated();
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    public function update(Service $service, CurriculumRequest $request, Curriculum $curiculum)
    {
        $request->validated();
        return $this->successResponse(
            $service->setCurriculum($curiculum)->update($request->all()),
            'Success'
        );
    }

    public function destroy(Service $service, Curriculum $curiculum)
    {
       return $this->successResponse(
           $service->setCurriculum($curiculum)->delete(),
           'success',
            204
       );
    }


}
