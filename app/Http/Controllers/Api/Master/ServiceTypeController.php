<?php

namespace App\Http\Controllers\Api\Master;

use App\Helpers\ImageUpload;
use Illuminate\Http\Request;
use App\Model\Master\MsServicesType;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\ServiceTypeRequest;
use App\Services\Master\ServiceTypeService as Service;

class ServiceTypeController extends ApiController
{
    public function index(Service $service, Request $request)
    {
        $q = $request->q;
        $ms_symptoms_id = $request->ms_symptoms_id;
        $paginate = $request->paginate;

        $response = $service
            ->list()
            ->when($q, function ($query, $q) {
                $query->where('name', 'like', '%' . $q . '%');
            })
            ->when($ms_symptoms_id, function ($query, $ms_symptoms_id) {
                $query->where('ms_symptoms_id', $ms_symptoms_id);
            })
            ->orderBy('name', 'asc');

        if ($paginate == 'no_paginate') {
            $response = $response->get();
        }else{
            $response = $response->paginate(10);
        }

        return $this->successResponse($response);
    }

    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    public function select2BySymptomId(Request $request, Service $service, $symptom_id = null)
    {
        return $this->successResponse($service->select2($request, $symptom_id));
    }

    public function store(Request $request, Service $service)
    {
        $user = auth()->user();
        $path = MsServicesType::getImagePathUpload();
        // dd($path);
        $filename = null;
        if ($request->hasFile('file')) {
            $image = (new ImageUpload)->upload($request->file('file'), $path);
            $filename = $image->getFilename();
        }
        $datas = [
            'ms_symptoms_id' => $request->ms_symptoms_id,
            'name'           => $request->name,
            'images'         => $filename
        ];
        return $this->successResponse(
            $service->create($datas),
            'success',
            201
        );
    }

    public function update(Request $request, MsServicesType $Service_type, Service $service)
    {
        $user = auth()->user();
        $path = MsServicesType::getImagePathUpload();
        // dd($path);
        $filename = null;
        if ($request->hasFile('file')) {
            $image = (new ImageUpload)->upload($request->file('file'), $path);
            $filename = $image->getFilename();
        }
        $datas = [
            'ms_symptoms_id' => $request->ms_symptoms_id,
            'name'           => $request->name,
            'images'         => $filename
        ];
        if ($Service_type->images != null) {
            $file_path = storage_path('app/public/services-type/' . $Service_type->images);
            if (file_exists($file_path)) {
                unlink($file_path);
            }
        }
        return $this->successResponse(
            $service->setServiceTypes($Service_type)->update($datas),
            'success'
        );
    }

    public function delete(MsServicesType $Service_type, Service $service)
    {
        if ($Service_type->images != null) {
            $file_path = storage_path('app/public/services-type/' . $Service_type->images);
            if (file_exists($file_path)) {
                unlink($file_path);
            }
        }
        return $this->successResponse(
            $service->setServiceTypes($Service_type)->delete($Service_type->id),
            'success',
            201
        );
    }
}
