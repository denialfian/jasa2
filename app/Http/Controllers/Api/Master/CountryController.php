<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use App\Imports\CountryImport;
use App\Model\Master\MsCountry;
use Yajra\DataTables\DataTables;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\ApiController;
use App\Services\Master\CountryService;
use App\Http\Requests\Master\CountryRequest;
use App\Traits\MasterImportTrait;

class CountryController extends ApiController
{
    /**
     * show list with paginate
     */
    use MasterImportTrait;

    public function index(CountryService $countryService)
    {
        $response = $countryService->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $response = MsCountry::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(CountryService $countryService)
    {
        $query = $countryService->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(CountryRequest $request, CountryService $countryService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $countryService->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(MsCountry $country)
    {
        $response = $country;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(CountryRequest $request, MsCountry $country, CountryService $countryService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $countryService->setCountry($country)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(MsCountry $country, CountryService $countryService)
    {
        return $this->successResponse(
            $countryService->setCountry($country)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, CountryService $countryService)
    {
        return $this->successResponse(
            $countryService->deleteById($request->id),
            'success',
            204
        );
    }

    public function importExcel(Request $request)
    {
        return $this->importExcelTraits('country', $request); // country is type of controller name
    }
}
