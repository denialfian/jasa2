<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Model\Master\JasaExcel;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Database\Eloquent\Collection;
use App\Services\Master\JasaExcelService as Service;

class JasaExcelController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service, Request $request)
    {
        $q = $request->q;
        $response = $service->list()
            ->when($q, function ($query, $q) {
                $query->where('name', 'like', '%' . $q . '%');
            })
            ->orderBy('name', 'asc')
            ->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function datatablesSearch(Service $service, Request $request)
    {
        return $service->datatables($request);
    }

    /**
     * insert data
     */
    public function store(Request $request, Service $service)
    {
        $check_exist = JasaExcel::where('name', 'like', '%' . $request->name . '%')->count();
        if($check_exist > 0) {
            return $this->errorResponse(
                $check_exist,
                'Nama Jas sudah pernah dibuat !',
                500
            );
        } else {
            return $this->successResponse(
                $service->create($request->all()),
                $this->successStoreMsg(),
                201
            );
        }
    }

    /**
     * show one data
     */
    public function show(JasaExcel $je)
    {
        $response = $je;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(Request $request, JasaExcel $je, Service $service)
    {

        return $this->successResponse(
            $service->setJasaExcel($je)->update($request->all()),
            $this->successUpdateMsg()
        );
    }


    /**
     * delete data
     */
    public function destroy(JasaExcel $je, Service $service)
    {
        return $this->successResponse(
            $service->setJasaExcel($je)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->deleteById($request->id),
            'success',
            204
        );
    }

}
