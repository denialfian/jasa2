<?php

namespace App\Http\Controllers\Api\Master;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\ApiController;
use App\Model\Master\PartDataExcelStock;
use App\Model\Master\PartDataExcelBundle;
use App\Model\Master\PartDataExcelStockBundle;
use App\Model\Master\PartDataExcelBundleDetail;
use App\Model\Master\PartDataExcelStockInventory;
use App\Model\Master\PartDataExcelStockBundleDetail;
use App\Services\Master\PartDataExcelStockBundleService as Service;

class PartDataExcelStockBundleController extends ApiController
{

    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function datatablesBundleDetails($pdesb_id, Service $service)
    {
        return $service->datatablesBundleDetails($pdesb_id);
    }

    public function datatablesSearch(Service $service, Request $request)
    {
        return $service->datatables($request);
    }

     /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service, $asc_id=null)
    {
        return $this->successResponse($service->select2($request, $asc_id));
    }

    /**
     * insert data
     */
    public function store(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(PartDataExcelStockBundle $pdesb)
    {
        $response = $pdesb; //part_data_excel_stock_bundle

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(Request $request, PartDataExcelStockBundle $pdesb, Service $service)
    {

        return $this->successResponse(
            $service->setData($pdesb)->updateX($request),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(PartDataExcelStockBundle $pdesb, Service $service)
    {

        return $this->successResponse(
            $service->setData($pdesb)->deleteX(),
            $this->successDeleteMsg(),
            204
        );
    }

    public function destroyDetails(Request $request, $pdesbd)
    {
        $data = PartDataExcelStockBundleDetail::where('id', $pdesbd)->firstOrFail();
        $data->delete();
        return $this->successResponse($data, $this->successDeleteMsg(), 204);
    }

}
