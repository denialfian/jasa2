<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\CityRequest;
use App\Model\Master\MsCity;
use App\Services\Master\CityService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Traits\MasterImportTrait;

class CityController extends ApiController
{
    /**
     * show list with paginate
     */
    use MasterImportTrait;

    public function index(CityService $cityService, Request $request)
    {
        $q = $request->q;

        $response = $cityService->list()
            ->when($q, function($query, $q){
                $query->where('name', 'like', '%' . $q . '%');
            })
            ->orderBy('name', 'asc')
            ->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $response = MsCity::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(CityService $cityService)
    {
        $query = $cityService->list()->with('province.country');

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(CityRequest $request, CityService $cityService)
    {
        return $this->successResponse(
            $cityService->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(MsCity $city)
    {
        $response = $city;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(CityRequest $request, MsCity $city, CityService $cityService)
    {

        // validasi
        $request->validated();

        return $this->successResponse(
            $cityService->setCity($city)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(MsCity $city, CityService $cityService)
    {
        return $this->successResponse(
            $cityService->setCity($city)->delete(),
            'success',
            204
        );
    }

    public function importExcel(Request $request)
    {
        return $this->importExcelTraits('city', $request); // country is type of controller name
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, CityService $cityService)
    {
        return $this->successResponse(
            $cityService->deleteById($request->id),
            'success',
            204
        );
    }
}
