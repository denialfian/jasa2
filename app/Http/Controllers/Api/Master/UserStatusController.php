<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Services\Master\UserStatusService as Service;
use App\Http\Requests\Master\UserStatusRequest;
use App\Model\Master\UserStatus;
use Yajra\DataTables\DataTables;

class UserStatusController extends ApiController
{
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);
        return $this->successResponse($response);
    }

    public function select2(Service $service, Request $request)
    {
        return $this->successResponse($service->select2($request));
    }

    public function datatables(Service $service)
    {
        $query = $service->list();

        return DataTables::of($query)
            ->toJson();
    }

    public function store(UserStatusRequest $request, Service $service)
    {
        // dd('asdas');
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    public function update(UserStatusRequest $request, Service $service, UserStatus $userStatus)
    {
        return $this->successResponse(
            $service->setUserStatus($userStatus)->update($request->all()),
            'success'
        );
    }

    public function destroy(Service $service, UserStatus $userStatus)
    {
        return $this->successResponse(
            $service->setUserStatus($userStatus)->delete(),
            'success',
            204
        );
    }
}
