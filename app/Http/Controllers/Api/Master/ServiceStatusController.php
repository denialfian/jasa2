<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\ServiceStatusRequest;
use App\Model\Master\MsServiceStatus;
use App\Services\Master\ServiceStatusService as Service;
use Illuminate\Http\Request;

class ServiceStatusController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(ServiceStatusRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(MsServiceStatus $status)
    {
        $response = $status;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(ServiceStatusRequest $request, MsServiceStatus $status, Service $service)
    {
        return $this->successResponse(
            $service->setStatus($status)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(MsServiceStatus $status, Service $service)
    {
        return $this->successResponse(
            $service->setStatus($status)->delete(),
            'success',
            204
        );
    }
}
