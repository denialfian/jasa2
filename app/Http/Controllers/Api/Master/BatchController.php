<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\BatchRequest;
use App\Model\Master\Batch;
use Illuminate\Http\Request;
use App\Services\Master\BatchService as Service;

class BatchController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with select2 structur
     */
    public function select2child(Request $request, Service $service)
    {
        return $this->successResponse($service->select2Child($request));
    }

    /**
     * show list with select2 structur
     */
    public function select2Accepted(Request $request, Service $service)
    {
        return $this->successResponse($service->select2Accepted($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * show list with datatables structur
     */
    public function datatablesShipmentHistories($batch_id, Service $service)
    {
        return $service->datatablesShipmentHistories($batch_id);
    }

    /**
     * show list with datatables structur
     */
    public function datatablesBatchItemEditOrder($batch_id, Service $service)
    {
        return $service->datatablesBatchItemEditOrder($batch_id);
    }

    /**
     * search list with datatables structur
     */
    public function searchDatatables(Service $service, Request $request)
    {
        return $service->searchDatatables($request);
    }

    /**
     * insert data
     */
    public function store(BatchRequest $request, Service $service)
    {
        $service->validasi($request);

        return $this->successResponse(
            $service->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(Batch $batch, Service $service)
    {
        $response = $service->show($batch->id);

        return $this->successResponse($response);
    }

    /**
     * show html template batch info
     */
    public function info_template(Batch $batch, Service $service)
    {
        $response = $service->infoTemplate($batch);

        return $this->successResponse($response);
    }

    /**
     * show one data
     */
    public function details($id_batch = null, Service $service)
    {
        return $service->setDatatables(true)->items($id_batch);
    }

    /**
     * update data
     */
    public function update(BatchRequest $request, Batch $batch, Service $service)
    {
        return $this->successResponse(
            $service->setBatch($batch)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * update data
     */
    public function updateItemOrder(Request $request, Service $service)
    {
        return $service->updateItemOrder($request);
    }

    /**
     * update data
     */
    public function updateShipmentHistory(Request $request, Service $service)
    {
        return $service->updateShipmentHistory($request);
    }

    /**
     * delete data
     */
    public function destroy(Batch $batch, Service $service)
    {
        return $this->successResponse(
            $service->setBatch($batch)->delete(),
            'success',
            204
        );
    }

    public function generateBatchNo()
    {
        return Service::generateBatchNo();
    }

    public function batchItemReason($batch_item_id)
    {
        $service = new Service;
        return $service->batchItemReason($batch_item_id);
    }
}
