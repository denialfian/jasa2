<?php

namespace App\Http\Controllers\Api\Master;

use App\Helpers\ImageUpload;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\AdditionalInfoRequest;
use App\Model\Master\MsAdditionalInfo;
use Auth;
use App\User;

class AdditionalInfoController extends ApiController
{
    public function store(AdditionalInfoRequest $request)
    {
        // validasi
        $request->validated();
        $segmenTeknisi = 'teknisi';
        $segmenCustomer = 'customer';
        $user = auth()->user();
        $path = MsAdditionalInfo::getImagePathUpload();
        // dd($path);
        $filename = null;

        if ($request->hasFile('picture')) {
            $image = (new ImageUpload)->upload($request->file('picture'), $path);
            $filename = $image->getFilename();
    }

        $info = MsAdditionalInfo::where('user_id', $user->id)->first();

        $dataInfo = [
            'ms_religion_id' => $request->ms_religion_id,
            'ms_marital_id' => $request->ms_marital_id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'date_of_birth' => $request->date_of_birth,
            'place_of_birth' => $request->place_of_birth,
            'gender' => $request->gender,
            'picture' => $filename,
            'user_id' => $user->id
        ];          

    

        if(Auth::user()->hasRole('Technician')){
            $url = '/teknisi/profile';
        }

        if( Auth::user()->hasRole('Customer')){
            $url = '/customer/profile/show';
        }

        if ($info == null) {
            $userInfo = MsAdditionalInfo::create($dataInfo);
        } else {
            // dd( $dataInfo);
            // if ($filename == null) {
            //     unset($dataInfo['picture']);
            // }
            $userInfo = $info->update($dataInfo);
        }

        // if ($userInfo) {
            $response = [
                'redirect'=> $url,
                'userInfo' => $userInfo
            ];

            return $this->successResponse(
                $response,
                'success',
                201
            );
            
        // } else {
        //     return $this->errorResponse($userInfo, 'error', 400);
        // }
    }

    // public function FunctionName(Type $var = null)
    // {
    //     # code...
    // }
}
