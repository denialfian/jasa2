<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\MaritalRequest;
use App\Model\Master\Marital;
use App\Services\Master\MaritalService as Service;
use Illuminate\Http\Request;

class MaritalController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service, Request $request)
    {
        $q = $request->q;
        $paginate = $request->paginate;
        
        $response = $service->list()
            ->when($q, function ($query, $q) {
                $query->where('status', 'like', '%' . $q . '%');
            })
            ->orderBy('status', 'asc');

            if ($paginate == 'no_paginate') {
                $response = $response->get();
            }else{
                $response = $response->paginate(10);
            }

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(MaritalRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(Marital $marital)
    {
        $response = $marital;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(MaritalRequest $request, Marital $marital, Service $service)
    {
        return $this->successResponse(
            $service->setMarital($marital)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(Marital $marital, Service $service)
    {
        return $this->successResponse(
            $service->setMarital($marital)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->deleteById($request->id),
            'success',
            204
        );
    }
}
