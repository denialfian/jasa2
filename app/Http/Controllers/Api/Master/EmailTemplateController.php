<?php

namespace App\Http\Controllers\Api\Master;


use Illuminate\Http\Request;
use App\Model\Master\EmailStatusOrder;
use App\Model\Master\EmailOther;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\EmailStatusOrderRequest;
use App\Http\Requests\Master\EmailOtherRequest;
use App\Services\Master\EmailOtherService as Service;

class EmailTemplateController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    public function indexEmailOrder(Service $service)
    {
        $response = $service->listEmailOrder()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function datatablesEmailOther(Service $service)
    {
        return $service->datatablesEmailOther();
    }

    /**
     * update data
     */
    public function update(EmailStatusOrderRequest $request)
    {
        $dataInfo = [];
        foreach(json_decode($request->id_email) as $key => $val) {
            $dataInfo = [
                'subject' => $request->{'subject'.$key} ,
                'content' => $request->{'content'.$key},
            ];
            EmailStatusOrder::where('id', $val)->update($dataInfo);
        }

        return $this->successResponse('Success Update !');
    }

    public function updateEmailOther(Request $request)
    {
        $dataInfo = [];
        foreach(json_decode($request->id_email) as $key => $val) {
            $dataInfo = [
                'subject' => $request->{'subject'.$key} ,
                'content' => $request->{'content'.$key},
            ];
            EmailOther::where('id', $val)->update($dataInfo);
        }

        return $this->successResponse('Success Update !');
    }


}
