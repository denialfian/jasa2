<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\DevPlantRequest;
use App\Model\Master\DevPlant;
use App\Services\Master\DevPlantService;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class DevelopmentPlantController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(DevPlantService $service)
    {
        
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(DevPlantService $service)
    {
        $query = $service->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function select2(DevPlantService $service, Request $request)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * insert data
     */
    public function store(DevPlantRequest $request, DevPlantService $service)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * update data
     */
    public function update(Request $request, DevPlant $devPlant, DevPlantService $service)
    {

        return $this->successResponse(
            $service->setDevPlant($devPlant)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(DevPlant $devPlant, DevPlantService $service)
    {
        return $this->successResponse(
            $service->setDevPlant($devPlant)->delete(),
            'success',
            204
        );
    }
}
