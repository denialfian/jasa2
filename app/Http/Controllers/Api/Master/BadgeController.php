<?php

namespace App\Http\Controllers\Api\Master;

use App\User;
use App\Model\Master\Badge;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Services\Master\BadgeService;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\BadgeRequest;

class BadgeController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(BadgeService $BadgeService)
    {
        $response = $BadgeService->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $response = Badge::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(BadgeService $BadgeService)
    {
        $query = $BadgeService->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(BadgeRequest $request, BadgeService $BadgeService)
    {

        return $this->successResponse(
            $BadgeService->create($request->all()),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(Badge $badge)
    {
        $response = $badge;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(BadgeRequest $request, Badge $badge, BadgeService $BadgeService)
    {
        return $this->successResponse(
            $BadgeService->setBadge($badge)->update($request->all()),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(Badge $badge, BadgeService $BadgeService)
    {
        User::where('badge_id', $badge->id)->update(['badge_id' => null]);
        return $this->successResponse(
            $BadgeService->setBadge($badge)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, BadgeService $BadgeService)
    {
        return $this->successResponse(
            $BadgeService->deleteById($request->id),
            'success',
            204
        );
    }
}
