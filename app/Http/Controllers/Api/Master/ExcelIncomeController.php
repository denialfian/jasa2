<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Model\Master\ExcelIncome;
use Illuminate\Support\Facades\DB;

use App\Model\Master\ExcelIncomeLog;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use App\Model\Master\ExcelIncomeDetail;
use Illuminate\Database\QueryException;
use App\Model\Master\ExcelIncomeDetailDescription;
use App\Services\Master\ExcelIncomeService as Service;

class ExcelIncomeController extends ApiController
{

    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function datatablesSearch(Service $service, Request $request)
    {
        return $service->datatables($request);
    }

    public function datatablesLogs($id, $type=null)
    {
        $query = ExcelIncomeLog::with('user');
        if($id != '0') {
            $query->where('excel_income_id', $id);
        }
        if(!empty($type)) {
            $query->where('type', $type);
        }
        $query->orderBy('created_at','DESC')->get();
        return DataTables::of($query)
        ->addIndexColumn()
        ->toJson();
    }

    /**
     * insert data
     */
    public function store(Request $request, Service $service)
    {

        return $this->successResponse(
            $service->create($request),
            $this->successStoreMsg(),
            201
        );
    }

    public function createDetail(Request $request, ExcelIncome $excel_income, Service $service)
    {

        return $this->successResponse(
            $service->createDetail($excel_income,$request),
            $this->successStoreMsg(),
            201
        );
    }

    public function updateDetail(Request $request, ExcelIncomeDetail $excel_income_detail, Service $service)
    {

        return $this->successResponse(
            $service->updateDetail($excel_income_detail,$request),
            $this->successStoreMsg(),
            201
        );
    }

    /**
     * show one data
     */
    public function show(ExcelIncome $excel_income)
    {
        $response = $excel_income;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(Request $request, ExcelIncome $excel_income, Service $service)
    {
        // return $request->all();
        return $this->successResponse(
            $service->setData($excel_income)->updateX($request),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(ExcelIncome $excel_income, Service $service)
    {
        return $this->successResponse(
            $service->setData($excel_income)->deleteX(),
            'success',
            204
        );
    }

    public function destroyDetailsData(Request $request, $excel_income_id)
    {
        $data = ExcelIncomeDetail::where('id', $excel_income_id)->firstOrFail();
        $data->delete();
        return $this->successResponse([
            'data' => $data,
            'uniq' => time(),
        ], $this->successDeleteMsg(), 200);
    }

    public function destroyDetailsDesc(Request $request, $desc_id)
    {
        $data = ExcelIncomeDetailDescription::where('id', $desc_id)->firstOrFail();
        $data->delete();
        return $this->successResponse([
            'data' => $data,
            'uniq' => time(),
        ], $this->successDeleteMsg(), 200);
    }




}
