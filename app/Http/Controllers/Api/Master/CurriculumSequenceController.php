<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\CurriculumSequenceRequest;
use App\Model\Master\CurriculumSequence;
use App\Services\Master\CurriculumSequenceService;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class CurriculumSequenceController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(CurriculumSequenceService $service)
    {
        
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(CurriculumSequenceService $service)
    {
        $query = $service->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function select2(CurriculumSequenceService $service, Request $request)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * insert data
     */
    public function store(CurriculumSequenceRequest $request, CurriculumSequenceService $service)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * update data
     */
    public function update(CurriculumSequenceRequest $request, CurriculumSequence $curSequence, CurriculumSequenceService $service)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $service->setCurSequence($curSequence)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(CurriculumSequence $curSequence, CurriculumSequenceService $service)
    {
        return $this->successResponse(
            $service->setCurSequence($curSequence)->delete(),
            'success',
            204
        );
    }
}
