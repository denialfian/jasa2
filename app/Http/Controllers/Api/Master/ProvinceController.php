<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\ProvinceRequest;
use App\Model\Master\MsProvince;
use App\Services\Master\ProvinceService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Traits\MasterImportTrait;

class ProvinceController extends ApiController
{
    /**
     * show list with paginate
     */
    use MasterImportTrait;

    public function index(ProvinceService $provinceService)
    {
        $response = $provinceService->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request)
    {
        $response = MsProvince::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    /**
     * show list with datatables structur
     */
    public function datatables(ProvinceService $provinceService)
    {
        $query = $provinceService->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * insert data
     */
    public function store(ProvinceRequest $request, ProvinceService $provinceService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $provinceService->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(MsProvince $province)
    {
        $response = $province;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(ProvinceRequest $request, MsProvince $province, ProvinceService $provinceService)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $provinceService->setProvince($province)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(MsProvince $province, ProvinceService $provinceService)
    {
        return $this->successResponse(
            $provinceService->setProvince($province)->delete(),
            'success',
            204
        );
    }

    public function deleteByChecked(ProvinceService $provinceService, Request $request)
    {
        return $this->successResponse(
            $provinceService->multipleDelete($request->id),
            'Success',
            204
        );
    }

    public function importExcel(Request $request)
	{
        return $this->importExcelTraits('province', $request); // country is type of controller name
    }


}
