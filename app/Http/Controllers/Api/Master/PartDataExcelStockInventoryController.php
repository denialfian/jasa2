<?php

namespace App\Http\Controllers\Api\Master;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Master\PartsDataExcel;
use App\Http\Controllers\ApiController;
use App\Model\Master\PartDataExcelStockInventory;
use Yajra\DataTables\Facades\DataTables;
use App\Model\Master\GeneralJournalDetail;
use App\Model\Master\PartDataExcelStockBundleDetail;
use App\Model\Master\PDESHistory;
use Illuminate\Database\Eloquent\Collection;
use App\Model\Master\PDSInventoryMutationHistories;
use App\Services\Master\PartDataExcelStockInventoryService as Service;

class PartDataExcelStockInventoryController extends ApiController
{
    /**
     * show list with paginate
     */
    // public function index(Service $service, Request $request)
    // {
    //     $q = $request->q;
    //     $response = $service->list()
    //         ->when($q, function ($query, $q) {
    //             $query->where('name', 'like', '%' . $q . '%');
    //         })
    //         ->orderBy('name', 'asc')
    //         ->paginate(10);
    //     return $this->successResponse($response);
    // }

    /**
     * show list with select2 structur
     */
    public function select2($search_by, $asc_id=null, $filter_stock=null, Request $request, Service $service)
    {
        return $this->successResponse($service->select2($search_by, $asc_id, $filter_stock, $request));
    }


    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    public function datatablesSearch(Service $service, Request $request)
    {
        return $service->datatables($request);
    }

    /**
     * insert data
     */
    public function store(Request $request, Service $service)
    {

        $check_exist = PartDataExcelStockInventory::with('part_data_stock.asc')->where('part_data_stock_id', $request->part_data_stock_id);
                        $check_exist->whereHas('part_data_stock', function ($q) use ($request) {
                            $q->where('asc_id', $request->asc_id);
                        });
        $check_exist = $check_exist->count();
        if($check_exist > 0) {
            return $this->errorResponse(
                $check_exist,
                'Tidak Bisa Create Part Data Stock Inventory tersebut dengan ASC name dan Code Material yang sama !',
                400
            );
        } else {
            return $this->successResponse(
                $service->create($request->all()),
                $this->successStoreMsg(),
                201
            );
        }
    }

    /**
     * show one data
     */
    public function show(PartDataExcelStockInventory $pdesi)
    {
        $response = $pdesi;
        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(Request $request, PartDataExcelStockInventory $pdesi, Service $service)
    {
        $datas = [
            'part_data_stock_id' => $request->part_data_stock_id,
            'selling_price' => $request->selling_price,
            // 'stock' => $pdesi->stock
        ];

        return $this->successResponse(
            $service->setPartDataExcelStockInventory($pdesi)->update($datas),
            $this->successUpdateMsg()
        );
    }

    /**
     * delete data
     */
    public function destroy(PartDataExcelStockInventory $pdesi, Service $service)
    {
        $find_data_1 = PartsDataExcel::where('part_data_stock_inventories_id', $pdesi->id)->count();
        $find_data_2 = GeneralJournalDetail::where('part_data_stock_inventories_id', $pdesi->id)->count();
        $find_data_3 = PartDataExcelStockBundleDetail::where('part_data_stock_inventories_id', $pdesi->id)->count();
        if($find_data_1 > 0) {
            return $this->errorResponse(
                $find_data_1,
                'Tidak bisa di delete, karena terdapat part stock Out di Orders data excel yang digunakan!',
                400
            );
        }
        if($find_data_2) {
            return $this->errorResponse(
                $find_data_2,
                'Tidak bisa di delete, karena terdapat Data General Journal Details yang digunakan!',
                400
            );
        }
        if($find_data_3) {
            return $this->errorResponse(
                $find_data_3,
                'Tidak bisa di delete, karena terdapat Data Bundle Details yang digunakan!',
                400
            );
        }
        return $this->successResponse(
            $service->setPartDataExcelStockInventory($pdesi)->delete(),
            'success',
            204
        );
    }

    /**
     * delete data
     */
    public function destroyBatch(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->deleteById($request->id),
            'success',
            204
        );
    }

    public function pdesInOutHistoriesDatatables($pdesi = null) {
        $query = PDESHistory::with(['part_data_stock_inventory','part_data_excel.order_data_excel','user']);
        if($pdesi != null) {
            $query->where('part_data_stock_inventories_id', $pdesi);
        }
        // $query->whereIn('type_mutation', [0,3,6]);
        // $query->where('is_deleted', 0);
        $query->orderBy('created_at', 'desc');
        $query->orderBy('id', 'desc');
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function leftoverStockDatatables(Request $request) {
        $data = GeneralJournalDetail::select([
                DB::raw('(MIN(YEAR(general_journal_details.date))) AS min_year'),
                DB::raw('(MAX(YEAR(general_journal_details.date))) AS max_year')
            ])->first();
        $min_month = 1;
        $max_month = 12;
        $min_year = $data->min_year;
        $max_year = $data->max_year;
        if(!empty($request->month)) {
            $min_month = $request->month;
            $max_month = $request->month;
        }
        if(!empty($request->year)) {
            $min_year = $request->year;
            $max_year = $request->year;
        }
        $collection = new Collection();
        $getIdInv = PartDataExcelStockInventory::pluck('id');
        foreach($getIdInv as $pdesi_id) { //part data excel id
            foreach (range($min_year, $max_year) as $year) {
                foreach (range($min_month,$max_month) as $month) {
                    $data = GeneralJournalDetail::select([
                        DB::raw('(CEIL(SUM(general_journal_details.kredit) / SUM(general_journal_details.quantity))) AS hpp_average_month'),
                        DB::raw('SUM(general_journal_details.quantity) AS stock_in'),
                        DB::raw('SUM(part_data_excel.quantity) AS stock_out'),
                        'general_journal_details.part_data_stock_inventories_id',
                        ])
                        ->with('part_data_stock_inventories.part_data_stock')
                        ->join('part_data_excel', 'part_data_excel.part_data_stock_inventories_id', '=', 'general_journal_details.part_data_stock_inventories_id')
                        ->where('general_journal_details.part_data_stock_inventories_id', $pdesi_id)
                        ->where('category', 'material')
                        ->whereMonth('general_journal_details.date', $month)
                        ->whereYear('general_journal_details.date', $year)
                        ->groupBy('general_journal_details.part_data_stock_inventories_id')
                        ->whereNotNull('general_journal_details.part_data_stock_inventories_id')
                        ->first();

                    if($data){
                        $collection->push([
                            'part_data_stock_inventories_id' => $data->part_data_stock_inventory->id,
                            'code_material' => $data->part_data_stock_inventory->code_material,
                            'asc_name' => $data->part_data_stock_inventory->asc_name,
                            'part_description' => $data->part_data_stock_inventory->part_description,
                            'stock_in' => $data->stock_in,
                            'stock_out' => $data->stock_out,
                            'leftover_stock' => ($data->stock_in - $data->stock_out),
                            'hpp_average_month' => $data->hpp_average_month,
                            'month' => date("F", mktime(0, 0, 0, $month, 1)),
                            'month_number' => $month,
                            'year' => $year
                        ]);
                    }
                }
            }
        }

        // Datatables::of($results)->make(true)
        return DataTables::of($collection)
            ->addIndexColumn()
            ->toJson();
    }

    public function getSellingPrice($pdesi_id, Service $service)
    {
        return $this->successResponse(
            $service->getSellingPrice($pdesi_id),
            'get hpp_average_all success',
            200
        );
    }

    public function setSellingPrice(Request $request, Service $service)
    {
        return $this->successResponse(
            $service->setSellingPrice($request->all()),
            'get hpp_average_all success',
            200
        );
    }


}
