<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\BatchStatusShipmentRequest;
use App\Model\Master\BatchStatusShipment;
use App\Services\Master\BatchStatusShipmentService as Service;
use Illuminate\Http\Request;

class BatchStatusShipmentController extends ApiController
{
    /**
     * show list with paginate
     */
    public function index(Service $service)
    {
        $response = $service->list()->paginate(10);

        return $this->successResponse($response);
    }

    /**
     * show list with select2 structur
     */
    public function select2(Request $request, Service $service)
    {
        return $this->successResponse($service->select2($request));
    }

    /**
     * show list with datatables structur
     */
    public function datatables(Service $service)
    {
        return $service->datatables();
    }

    /**
     * insert data
     */
    public function store(BatchStatusShipmentRequest $request, Service $service)
    {
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    /**
     * show one data
     */
    public function show(BatchStatus $status)
    {
        $response = $status;

        return $this->successResponse($response);
    }

    /**
     * update data
     */
    public function update(BatchStatusShipmentRequest $request, BatchStatusShipment $status, Service $service)
    {
        return $this->successResponse(
            $service->setStatus($status)->update($request->all()),
            'success'
        );
    }

    /**
     * delete data
     */
    public function destroy(BatchStatusShipment $status, Service $service)
    {
        return $this->successResponse(
            $service->setStatus($status)->delete(),
            'success',
            204
        );
    }

    // public function setAccepted(Request $request) {
    //     $reset = BatchStatusShipment::where('id', '>', 0)->update(['is_accepted' => 0]);
    //     return $find = BatchStatusShipment::where('id', $request->get('id'))->update(['is_accepted' => 1]);
    // }
}
