<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Model\Master\AddressType;
use Yajra\DataTables\DataTables;
use App\Http\Requests\Master\AddressTypeRequest;

use App\Services\Master\AddressTypeService;

class AddressTypeController extends ApiController
{
    public function index(AddressTypeService $addressTypeService, Request $request)
    {
        $q = $request->q;
        $paginate = $request->paginate;

        $response = AddressType::when($q, function ($query, $q) {
            $query->where('name', 'like', '%' . $q . '%');
        });

        if ($paginate == 'no_paginate') {
            $response = $response->get();
        }else{
            $response = $response->paginate(10);
        }

        return $this->successResponse($response);
    }

    public function datatables(AddressTypeService $addressTypeService)
    {
        $query = AddressType::query();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function select2(Request $request)
    {
        $response = AddressType::where('id', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();

        return $this->successResponse($response);
    }

    public function store(AddressTypeRequest $request, AddressTypeService $service)
    {
        // dd('asdas');
        return $this->successResponse(
            $service->create($request->all()),
            'success',
            201
        );
    }

    public function update(AddressTypeRequest $request, AddressType $addresses, AddressTypeService $service)
    {
        return $this->successResponse(
            $service->setAddressType($addresses)->update($request->all()),
            'success'
        );
    }

    public function destroy(AddressType $addresses, AddressTypeService $service)
    {

        return $this->successResponse(
            $service->setAddressType($addresses)->delete(),
            'success',
            204
        );
    }
}
