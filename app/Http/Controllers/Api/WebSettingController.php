<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\WebSettingRequest;
use App\Model\Master\WebSetting;
use App\Services\WebSettingService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Helpers\ImageUpload;

class WebSettingController extends ApiController
{
    public function index()
    {
        $response = WebSetting::paginate(10);

        return $this->successResponse($response);
    }

    public function store(WebSettingRequest $request)
    {
        // validasi
        $request->validated();
        $imgUpload = (new ImageUpload);
        $path = WebSetting::getImagePathUpload();
        $imageLogo = null;
        $imageIcon = null;

        if ($request->hasFile('image_logo')) {
            // $image = (new ImageUpload)->upload($request->file('image_logo'), $path);

            // $imageLogo = $image->getFilename();
            
            $imageLogo = $imgUpload->upload($request->file('image_logo'), $path)->getFilename();
        }

        if ($request->hasFile('image_icon')) {
            // $image = (new ImageUpload)->upload($request->file('image_icon'), $path);
            // $imageIcon = $image->getFilename();

            $imageIcon = $imgUpload->upload($request->file('image_icon'), $path)->getFilename();

        }
        
        // dd($imageLogo);
        $setting = WebSetting::first();

        $dataSetting = [
            'app_name'  => $request->app_name,
            'image_logo' => $imageLogo,
            'image_icon' => $imageIcon,
        ];

        if ($setting == null) {
            $settingInfo = WebSetting::create($dataSetting);
        } else {
            if ($filename == null) {
                unset($dataSetting['picture']);
            }
            $settingInfo = $setting->update($dataSetting);
        }

        if ($settingInfo) {
            return $this->successResponse($settingInfo, 'success', 201);
        } else {
            return $this->errorResponse($settingInfo, 'error', 400);
        }
    }

    public function update(WebSettingRequest $request, Menu $menu, WebSettingService $webSetting)
    {
        // validasi
        $request->validated();

        return $this->successResponse(
            $webSetting->setMenu($menu)->update($request->all()),
            'success',
            200
        );
    }
}
