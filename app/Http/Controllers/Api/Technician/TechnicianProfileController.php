<?php

namespace App\Http\Controllers\Api\Technician;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Master\TeknisiInfoRequest;
use App\Http\Requests\PriceServiceRequest;
use App\Http\Requests\Master\JobInfoRequest;
use App\Http\Requests\TeknisiInfoRekRequest;
use App\Model\Technician\Technician;
use App\Services\Technician\TechnicianProfileService as Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TechnicianProfileController extends ApiController
{
    public function jobInfo(JobInfoRequest $request, Service $service)
    {
        $response = $service->profileJobInfo($request->all());

        return $this->successResponse(
            $response,
            'success'
        );
    }

    public function rekInfo(TeknisiInfoRekRequest $request, Service $service)
    {
        $response = $service->rekInfo($request->all());

        return $this->successResponse(
            $response,
            'success'
        );
    }

    public function accountInfo()
    {
        $user    = Auth::user();
        $response = Technician::with(['user.info.religion', 'user.info.marital'])->where('user_id', $user->id)->firstOrFail();

        return $this->successResponse(
            $response,
            'success'
        );
    }

    public function jobExperienceList(Service $service)
    {
        $response = $service->jobExperienceList();

        return $response;
    }

    /**
     * insert price service
     */
    public function priceServiceStore(Service $service, PriceServiceRequest $request)
    {
        $response = $service->priceServiceStore($request->all());

        return $this->successResponse(
            $response,
            'success'
        );
    }

    /**
     * price service update
     */
    public function priceServiceUpdate(Service $service, PriceServiceRequest $request, $id)
    {
        $response = $service->priceServiceUpdate($id, $request->all());

        return $this->successResponse(
            $response,
            'success'
        );
    }

    /**
     * price service datatables
     */
    public function priceServiceList(Service $service)
    {
        $response = $service->priceServiceList();

        return $response;
    }

    /**
     * price service list
     */
    public function priceServiceListPaginate(Service $service)
    {
        $response = $service->priceServiceDataList();

        return $this->successResponse(
            $response,
            'success'
        );
    }

    /**
     * price service delete
     */
    public function priceServiceDestroy(Service $service, $id)
    {
        $response = $service->priceServiceDestroy($id);

        return $this->successResponse(
            $response,
            'success'
        );
    }

    public function jobExperience(Request $request, Service $service)
    {
        $response = $service->profileJobExperience($request->all());

        return $this->successResponse(
            $response,
            'success'
        );
    }

    public function jobExperienceDestroy($id, Service $service)
    {
        $response = $service->jobExperienceDestroy($id);

        return $this->successResponse(
            $response,
            'success'
        );
    }
}
