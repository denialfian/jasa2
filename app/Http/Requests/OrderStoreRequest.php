<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class OrderStoreRequest extends FormRequest
{
    use HandleFailedValidationApi;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'address_id' => ['required'],
            'schedule' => ['required'],
            'ms_service_id' => ['required'],
            'symptom_id' => ['required'],
            'service_type_id' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'address_id.required' => 'Address is required',
            'ms_service_id.required' => 'Service is required',
            'symptom_id.required' => 'Symptom is required',
        ];
    }
}
