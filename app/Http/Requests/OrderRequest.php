<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    use HandleFailedValidationApi;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ms_outlet_id' => ['required'],
            'ms_transaction_b2b_detail' => ['required'],
            'service_type' => ['required'],
            'remark' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'ms_outlet_id.required' => 'Outlet is required',
            'ms_transaction_b2b_detail.required' => 'Unit is required',
            'service_type.required' => 'Service Type is required',
            'remark.required' => 'Remark is required',
        ];
    }
}
