<?php

namespace App\Http\Requests\Product;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class ProductAttributeUpdateBatchRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // insert
        return [
            'name' => ['required', 'array'],
            'id' => ['required', 'array'],
            'parent_id' => ['integer'],
        ];
    }
}
