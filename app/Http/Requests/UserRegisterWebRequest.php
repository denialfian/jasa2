<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class UserRegisterWebRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (is_numeric($this->email)) {
            $rule = ['required', 'numeric', 'unique:users'];
        } else {
            $rule = ['required', 'string', 'email', 'max:255', 'unique:users'];
        }
        
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => $rule,
            'phone' => ['string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'g-recaptcha-response' => 'required|captcha'
        ];
    }
}
