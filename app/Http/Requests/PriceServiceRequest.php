<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class PriceServiceRequest extends FormRequest
{
    use HandleFailedValidationApi;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ms_services_types_id' => ['required'],
            'product_group_id' => ['required'],
            'value' => ['required'],
        ];
    }
}
