<?php

namespace App\Http\Requests\Technician;

use App\Model\Technician\Technician;
use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class TechnicianRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'job_title_category_id' => ['required'],
            'user_id'               => ['required', 'unique:' . (new Technician)->getTable() . ',user_id'],
        ];
    }
}
