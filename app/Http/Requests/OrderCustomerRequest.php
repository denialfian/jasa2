<?php

namespace App\Http\Requests;

use App\Model\Technician\Technician;
use App\Traits\HandleFailedValidationApi;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class OrderCustomerRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ms_service_id' => ['required', 'numeric'],
            'symptom_id' => ['required', 'numeric'],
            'product_group_id' => ['required', 'numeric'],
            'address_id' => ['required', 'numeric'],
            'schedule' => ['required', 'date', 'after_or_equal:' . date('Y-m-d H:i:s')],
            'symtom_detail' => ['max:191'],
            'product_name' => ['max:191'],
            'brand_name' => ['max:191'],
            'model_name' => ['max:191'],
            'serial_number' => ['max:191'],
            'remark' => ['max:191'],
            'teknisi_id' => ['required', 'numeric', function ($attribute, $value, $fail) {
                $teknisi = Technician::with('user')->where('id', $value)->first();
                if ($teknisi == null) {
                    $fail($attribute . ' technician not found');
                }

                if ($teknisi != null) {
                    if ($teknisi->user->hasRole('Technician') == false) {
                        $fail($attribute . ' user not technician');
                    }
                }
            }],
            'service_type_id' => ['required', 'numeric'],
            // 'files' => ['mimes:jpeg,png,jpg', 'max:5048', 'array'],
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'phone' => str_replace(' ', '', $this->phone),
        ]);
    }
}
