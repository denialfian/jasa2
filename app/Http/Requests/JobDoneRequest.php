<?php

namespace App\Http\Requests;
use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class JobDoneRequest extends FormRequest
{
    use HandleFailedValidationApi;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ms_symptom_code' => ['required', 'string'],
            'ms_repair_code' => ['required', 'string'],
            'repair_desc' => ['required', 'string'],
        ];
    }

    public function messages()
    {
        return [
            'ms_symptom_code.required' => 'Symptom Code is required!',
            'ms_repair_code.required' => 'Repair Code is required!',
            'repair_desc.required' => 'Description is required!'
        ];
    }
}
