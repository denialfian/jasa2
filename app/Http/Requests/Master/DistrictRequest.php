<?php

namespace App\Http\Requests\Master;

use App\Model\Master\MsDistrict;
use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class DistrictRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($this->id)) {
            return $this->updateRule();
        }

        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'name' => ['required', 'string', 'max:100', 'unique:' . (new MsDistrict())->getTable() . ',name'],
            'ms_city_id' => ['required']
        ];
    }

    public function updateRule()
    {
        return [
            'name' => ['required', 'string', 'max:100', 'unique:' . (new MsDistrict())->getTable() . ',name,' . $this->id],
            'ms_city_id' => ['required']
        ];
    }
}
