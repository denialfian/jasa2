<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class ServicesRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // update
        // if (isset($this->id)) {
        //     return [
        //         'bank_name' => ['required', 'string' . $this->id],
        //         'no_rek' => ['required', 'string' . $this->id],
        //         'desc' => ['required', 'string' . $this->id],
        //         'icon' => ['required', 'string' . $this->id],
        //     ];
        // }
        // insert
        return [
            'name' => ['required', 'string'],
            'slug' => ['required', 'string'],
        ];
    }
}
