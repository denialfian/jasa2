<?php

namespace App\Http\Requests\Master;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;

class UserGroupRequest extends FormRequest
{
    use HandleFailedValidationApi;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($this->id)) {
            return $this->updateRule();
        }

        return $this->insertRule();
    }

    public function insertRule()
    {
        return [
            'property_name' => ['required', 'string', 'max:50', 'unique:user_groups,property_name'],
            'business_type' => ['required', 'string', 'max:50'],
            'user_id' => ['required'],
        ];
    }

    public function updateRule()
    {
        return [
            'property_name' => ['required', 'string', 'max:50', 'unique:user_groups,property_name,' . $this->id],
            'business_type' => ['required', 'string', 'max:50'],
            'user_id' => ['required'],
        ];
    }
}
