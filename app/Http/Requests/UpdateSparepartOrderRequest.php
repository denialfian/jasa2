<?php

namespace App\Http\Requests;

use App\Traits\HandleFailedValidationApi;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSparepartOrderRequest extends FormRequest
{
    use HandleFailedValidationApi;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type_part' => [Rule::in(['my-inventory', 'my-company'])],
            'sparepart_detail_id' => ['array'],
            'tmp_sparepart_id' => ['array'],
            'teknisi_sparepart_name' => ['array'],
            'teknisi_sparepart_price' => ['array'],
            'teknisi_sparepart_qty' => ['array'],
        ];
    }
}
