<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserB2B extends Model
{
    use Notifiable, HasApiTokens;
    
    protected $table = 'users_b_2_b';
    protected $fillable = [
        'name',
        'username',
        'password',
        'email',
        'ms_company_id',
        'attachment',
        'phone',
        'status',
        'api_token',
        'address'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getImagePathUpload()
    {
        return 'public/userb_2_b';
    }
}
