<?php

namespace App\Notifications\Ticket;

use App\Model\Master\MsTicket;
use App\User;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TicketNewNotification extends Notification
{
    use Queueable;

    public $ticket;
    public $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(MsTicket $ticket, User $user = null)
    {
        $this->ticket = $ticket;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', url('/'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $ticket = $this->ticket;
        return [
            'name' => 'New Ticket ' . $ticket->ticket_no,
            'description' => $ticket->subject,
            'icon' => 'fa fa-envelope',
            'color' => 'border-danger bg-danger',
            'url' => route('tickets.detail', ['id' => $ticket->id]),
        ];
    }

    public function toDatabase($notifiable)
    {
        $ticket = $this->ticket;
        return [
            'name' => 'New Ticket ' . $ticket->ticket_no,
            'description' => $ticket->subject,
            'icon' => 'fa fa-envelope',
            'color' => 'border-danger bg-danger',
            'url' => route('tickets.detail', ['id' => $ticket->id]),
        ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel('tickets.' . $this->user->id);
    }

    public function broadcastAs()
    {
        return 'tickets.created';
    }

    public function toBroadcast($notifiable)
    {
        $ticket = $this->ticket;

        return (new BroadcastMessage([
            'name' => '(TICKET) New Ticket ' . $ticket->ticket_no,
            'description' => $ticket->subject,
            'icon' => 'fa fa-envelope',
            'color' => 'border-danger bg-danger',
            'url' => route('tickets.detail', ['id' => $ticket->id]),
        ]))->onConnection('sync');
    }
}
