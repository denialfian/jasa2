<?php

namespace App\Model\Technician;

use Illuminate\Database\Eloquent\Model;
use App\User;

class JobTitle extends Model
{
    protected $fillable = [
        'description',
        'user_id',
        'job_title_category_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function job_title_category()
    {
        return $this->belongsTo(JobTitleCategory::class, 'job_title_category_id', 'id');
    }
}
