<?php

namespace App\Model\Technician;

use App\User;
use Illuminate\Database\Eloquent\Model;

class JobExperience extends Model
{
    protected $fillable = [
        'position',
        'company_name',
        'period_start',
        'period_end',
        'type',
        'user_id',
        'job_title_id',
    ];

    protected $appends = ['job_type'];

    // accessor
    public function getJobTypeAttribute()
    {
        return $this->type == 0 ? 'Pengalaman Internal' : 'Pengalaman External';
    }

    // mutator
    public function setPeriodStartAttribute($value)
    {
        $this->attributes['period_start'] = date("Y-m-d", strtotime($value));
    }
    public function setPeriodEndAttribute($value)
    {
        $this->attributes['period_end'] = date("Y-m-d", strtotime($value));
    }

    // relasi
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function job_title()
    {
        return $this->belongsTo(JobTitle::class, 'job_title_id', 'id');
    }
}
