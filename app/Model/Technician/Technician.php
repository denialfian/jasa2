<?php

namespace App\Model\Technician;

use DB;
use App\User;
use Carbon\Carbon;
use App\Model\Master\Order;
use App\Model\Master\Rating;
use App\Model\Master\Review;
use App\Model\Master\ServiceDetail;
use App\Model\Master\MsPriceService;
use App\Model\Master\TeamTechnician;
use App\Model\Master\SparepartDetail;
use App\Model\Master\TechnicianSaldo;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use App\Model\Master\TechnicianSparepart;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Master\InventoryMutationHistories;

class Technician extends Model
{
    // use SoftDeletes;
    protected $fillable = [
        'id',
        'user_id',
        'job_title_id',
        'status',
        'skill',
        'rekening_bank_name',
        'rekening_number',
        'rekening_name',
    ];

    protected $appends = [
        'total_prices',
        'name',
        'email',
        'created_service',
        // 'total_services',
        // 'total_transactions',
        // 'service_type_id',
        // 'avg_rating',
        // 'total_review',
        // 'total_pending_orders',
        // 'total_success_orders',
        // 'total_cancel_orders',
        // 'total_orders_gross_revenue',
        // 'total_orders_net_revenue',
        // 'total_orders_cut_commission',
        // 'total_orders_gross_revenue_pending',
        // 'total_spareparts_sold',
        // 'total_spareparts_sold_revenue',
        // 'last_order'
    ];

    /**
     * ====
     * all accessor
     * ===
     */

    /**
     * get jumlah pending order
     */

    //new

    public function getTotalTransactionsAttribute()
    {
        $query = Order::whereHas('service_detail', function ($q) {
            $q->where('technicians_id', $this->id);
        });
        if (Session::get('date_range')) {
            $query = $query->dateRange(Session::get('date_range'));
        }
        $query = $query->count();
        if ($query) {
            return $query;
        }
        return null;
    }

    public function getTotalServicesAttribute()
    {
        $query = MsPriceService::where('technicians_id', $this->id)->orderBy('created_at', 'DESC')->count();
        return $query;
    }

    public function getTotalOrdersGrossRevenueAttribute()
    {
        return $this->getOrderRevenue(1); //succcess
    }

    public function getTotalOrdersGrossRevenuePendingAttribute()
    {
        return $this->getOrderRevenue(0); //pending
    }

    public function getTotalOrdersCutCommissionAttribute()
    {
        return $this->total_orders_gross_revenue - $this->total_orders_net_revenue;
    }

    public function getTotalOrdersNetRevenueAttribute()
    {
        $query = Order::whereHas('service_detail', function ($q) {
            $q->where('technicians_id', $this->id);
        })->where('transfer_status', 1);
        if (Session::get('date_range')) {
            $query = $query->dateRange(Session::get('date_range'));
        }
        if ($query) {
            return $query = $query->sum('after_commission');
        }
        return null;
    }


    public function getOrderRevenue($transfer_status)
    { // param = transfer_status;
        $query = Order::whereHas('service_detail', function ($q) {
            $q->where('technicians_id', $this->id);
        })->where('transfer_status', $transfer_status);
        if (Session::get('date_range')) {
            $query = $query->dateRange(Session::get('date_range'));
        }
        if ($query) {
            return $query = $query->sum('grand_total');
        }
        return null;
    }

    public function getTotalSparepartsSoldAttribute()
    {
        return $query = $this->getSpareparts()->sum('quantity');
    }

    public function getTotalSparepartsSoldRevenueAttribute()
    {
        $queries = $this->getSpareparts()->select('price', 'quantity')->get();
        if ($queries) {
            $total = 0;
            foreach ($queries as $key => $query) {
                $total += $query->price * $query->quantity;
            }
            return $total;
        }
        return null;
    }

    public function getSpareparts()
    {
        $total = SparepartDetail::where('technicians_id', $this->id);
        if (Session::get('date_range')) {
            $total = $total->whereHas('order', function ($q) {
                $q->dateRange(Session::get('date_range'));
            });
        }
        if ($total) {
            return $total;
        }
        return null;
    }

    public function getLastOrderAttribute()
    {
        $query = Order::whereHas('service_detail', function ($q) {
            $q->where('technicians_id', $this->id);
        });
        if (Session::get('date_range')) {
            $query = $query->dateRange(Session::get('date_range'));
        }
        $query = $query->orderBy('created_at', 'DESC')->limit(1)->first(['code', 'created_at']);
        if ($query) {
            return '#' . $query->code . ' - ' . Carbon::parse($query->created_at)->format('Y-m-d');
        }
        return null;
    }

    public function getTotalCancelOrdersAttribute()
    {
        $count = Order::where('orders_statuses_id', 5)->whereHas('service_detail', function ($q) {
            $q->where('technicians_id', $this->id);
        });
        if (Session::get('date_range')) {
            $count = $count->dateRange(Session::get('date_range'));
        }
        $count = $count->count();
        if ($count) {
            return $count;
        }
        return null;
    }

    public function getTotalPendingOrdersAttribute()
    {
        $count = Order::where('transfer_status', 0)->whereHas('service_detail', function ($q) {
            $q->where('technicians_id', $this->id);
        });
        if (Session::get('date_range')) {
            $count = $count->dateRange(Session::get('date_range'));
        }
        $count = $count->count();
        if ($count) {
            return $count;
        }
        return null;
    }

    /**
     * get jumlah sukses order
     */
    public function getTotalSuccessOrdersAttribute()
    {
        $count = Order::where('transfer_status', 1)->whereHas('service_detail', function ($q) {
            $q->where('technicians_id', $this->id);
        });
        if (Session::get('date_range')) {
            $count = $count->dateRange(Session::get('date_range'));
        }
        $count = $count->count();
        if ($count) {
            return $count;
        }
        return null;
    }

    /**
     * get total price
     */
    public function getTotalPricesAttribute()
    {
        if (Session::get('service_type_id') && Session::get('product_group_id')) {
            $value = MsPriceService::where('technicians_id', $this->id)
                ->where('ms_services_types_id', Session::get('service_type_id'))
                ->where('product_group_id', Session::get('product_group_id'))
                ->value('value');
            return $value;
        }
        return null;
    }

    /**
     * get serpice tive
     */

    /**
     * get vreated service
     */

    /**
     * get user name
     */
    public function getNameAttribute()
    {
        return $this->user == null ? '' : ucfirst($this->user->full_name);
    }

    /**
     * get user email
     */
    public function getEmailAttribute()
    {
        return $this->user == null ? '' : ucfirst($this->user->email);
    }


    // public function getServiceTypeIdAttribute()
    // {
    //     if (Session::get('service_type_id')) {
    //         $val = $this->price_services()->where('ms_services_types_id', Session::get('service_type_id'))->orderBy('ms_services_types_id', 'asc')->pluck('ms_services_types_id')->toArray();
    //         return implode('', $val);
    //     }
    //     return null;
    // }


    public function getCreatedServiceAttribute()
    {
        return Carbon::parse(MsPriceService::where('technicians_id', $this->id)->value('created_at'))->format('Y-m-d h:i:s');
    }

    /**
     * get avg rating
     */
    public function getAvgRatingAttribute()
    {
        $jumlah_rate = Rating::where('technician_id', $this->id)->count();
        if ($jumlah_rate == 0) {
            return 0;
        }
        $sum_star = Rating::where('technician_id', $this->id)->sum('value');

        return $sum_star / $jumlah_rate;
    }

    /**
     * get avg rating
     */
    public function getAvgRating()
    {
        return $this->getAvgRatingAttribute();
    }

    /**
     * get total review
     */
    public function getTotalReviewAttribute()
    {
        return Review::where('technician_id', $this->id)->count();
    }

    /**
     * ====
     * all relasi
     * ===
     */

    /**
     * has one user
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * has one job_title
     */
    public function job_title()
    {
        return $this->belongsTo(JobTitle::class, 'job_title_id', 'id');
    }

    public function getteknisi()
    {
        return $this->hasOne(TechnicianSaldo::class, 'technician_id');
    }

    /**
     * has many price_services
     */
    public function price_services()
    {
        return $this->hasMany(MsPriceService::class, 'technicians_id', 'id');
    }

    /**
     * has one price_services
     */
    public function price_service()
    {
        return $this->hasOne(MsPriceService::class, 'technicians_id', 'id');
    }

    /**
     * has many service_detail
     */
    public function service_detail()
    {
        return $this->hasMany(ServiceDetail::class, 'technicians_id', 'id');
    }

    /**
     * has many service_detail
     */
    public function service_details()
    {
        return $this->hasMany(ServiceDetail::class, 'technicians_id', 'id');
    }

    /**
     * has many team_technician
     */
    public function team_technician()
    {
        return $this->hasMany(TeamTechnician::class, 'technicians_id', 'id');
    }

    /**
     * has many reviews total
     */
    public function reviews()
    {
        return $this->hasMany(Review::class, 'technician_id', 'id');
    }

    /**
     * has many ratings total
     */
    public function sum_total_rating()
    {
        return $this->hasOne(Rating::class, 'technician_id', 'id')
            ->select('technician_id', DB::raw('sum(value) as sum_rating'))
            ->groupBy('technician_id');
    }

    public function count_total_review()
    {
        return $this->hasOne(Review::class, 'technician_id', 'id')
            ->select('technician_id', DB::raw('COUNT(*) as count_review'))
            ->groupBy('technician_id');
    }

    /**
     * has many technician curriculum
     */
    public function technician_curriculum()
    {
        return $this->hasMany(TechnicianCurriculum::class, 'technician_id', 'id');
    }

    /**
     * ====
     * all model helper
     * ===
     */

    /**
     * generate rating star html
     */
    public function getRatingStarUi($light = '', $dark = '')
    {
        if (empty($this->jumlah_rating)) {
            $jumlah_rating = Rating::where('technician_id', $this->id)->count();
        } else {
            $jumlah_rating = $this->jumlah_rating;
        }

        if (empty($this->total_rating)) {
            $total_rating = Rating::where('technician_id', $this->id)->sum('value');
        } else {
            $total_rating = $this->total_rating;
        }

        if ($light == '') {
            $light = '<span class="star" style="color: gold"><i class="fa fa-star"></i></span>';
        }
        if ($dark == '') {
            $dark = '<span class="star" style="color: black"><i class="fa fa-star-o"></i></span>';
        }

        $half =  '<span class="star" style="color: gold"><i class="fa fa-star-half-o"></i></span>';

        $totalRating = 5;
        if (empty($jumlah_rating)) {
            $starRating = 0;
        } else {
            $starRating = number_format($total_rating / $jumlah_rating, 1);
        }

        $star = '';
        for ($i = 1; $i <= $totalRating; $i++) {
            if ($starRating < $i) {
                if (is_float($starRating) && (round($starRating) == $i)) {
                    $star .= $half . ' ';
                } else {
                    $star .= $dark . ' ';
                }
            } else {
                $star .= $light . ' ';
            }
        }

        return $star;
    }

    /**
     * sum rating star
     */
    public function getSumRateValue()
    {
        return Rating::where('technician_id', $this->id)->count('value');
    }

    /**
     * rating break down
     */
    public function getRatingBreakDown()
    {
        return \DB::table('ratings')
            ->select('value', \DB::raw('count(*) as total'))
            ->where('technician_id', $this->id)
            ->groupBy('value')
            ->orderBy('value', 'DESC')
            ->get();
    }

    /**
     * teknisi done order
     */
    public function doneOrder()
    {
        return $this->hasMany(ServiceDetail::class, 'technicians_id')->whereHas('order', function ($query) {
            $query->where('transfer_status', 1);
        })->with(['order', 'order.order_status'])->groupBy('orders_id');
    }

    public function allOrder()
    {
        return $this->hasMany(ServiceDetail::class, 'technicians_id')->with('order')->groupBy('orders_id');
    }
    
    public function otherOrder()
    {
        return $this->hasMany(ServiceDetail::class, 'technicians_id')->doesntHave('order');
    }



    /**
     * teknisi pending order
     */
    public function pendingOrder()
    {
        // return $this->hasMany(ServiceDetail::class, 'technicians_id')->whereHas('order', function ($query) {
        //     $query->where('transfer_status', 1);
        // })->with(['order'])->groupBy('id');

        return $this->hasMany(ServiceDetail::class, 'technicians_id')->whereHas('order', function ($query) {
            $query->where('transfer_status', 0);
        })->with(['order', 'order.order_status'])->groupBy('orders_id');
    }

    /**
     * teknisi pending order
     */
    public function getFormatRatingValue()
    {
        if (empty($this->total_rating)) {
            $total_rating = Rating::where('technician_id', $this->id)->sum('value');
            if($total_rating > 0){
                return number_format($total_rating / $this->total_review, 1);
            }else{
                return 0;
            }            
        }
        return number_format($this->total_rating / $this->total_review, 1);
    }

    public function technician_schedule()
    {
        return $this->hasMany(TechnicianSchedule::class, 'technician_id', 'id');
    }

    public function order_completes()
    {
        return $this->hasMany(ServiceDetail::class, 'technicians_id')->whereHas('order', function ($query) {
            $query->where('orders_statuses_id', 10);
        })->with(['order'])->groupBy('orders_id');
    }

    public function order_revenue()
    {
        return $this->hasMany(ServiceDetail::class, 'technicians_id')->whereHas('order', function ($query) {
            $query->where('transfer_status', 1);
        })->with(['order'])->groupBy('orders_id');
    }

    public function order_revenue_pending()
    {
        return $this->hasMany(ServiceDetail::class, 'technicians_id')->whereHas('order', function ($query) {
            $query->where('transfer_status', 0);
        })->with(['order'])->groupBy('orders_id');
    }

    public function sparepart_details()
    {
        return $this->hasMany(SparepartDetail::class, 'technicians_id');
    }

    public function inventory_mutation_histories()
    {
        return $this->hasMany(InventoryMutationHistories::class, 'order_id');
    }
}
