<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GeneralSettingB2b extends Model
{
    protected $table = 'general_settings_b2b';
    protected $fillable = [
        'name', 'value', 'desc', 'type'
    ];

}
