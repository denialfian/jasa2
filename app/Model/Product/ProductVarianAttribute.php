<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductVarianAttribute extends Model
{
    protected $fillable = [
        'product_varian_id',
        'product_attribute_id',
        'product_attribute_term_id',
    ];

    public function product_varian()
    {
        return $this->belongsTo(ProductVarian::class, 'product_varian_id', 'id');
    }

    public function attribute()
    {
        return $this->belongsTo(ProductAttribute::class, 'product_attribute_id', 'id');
    }

    public function term()
    {
        return $this->belongsTo(ProductAttributeTerm::class, 'product_attribute_term_id', 'id');
    }

}
