<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductAttributeTerm extends Model
{
    protected $fillable = [
        'name',
        'color_code',
        'hex_rgb',
        'product_attribute_id'
    ];

    public function term()
    {
        return $this->has(ProductVarianAttribute::class, 'product_attribute_term_id', 'id');
    }
}
