<?php

namespace App\Model\Product;

use App\Model\Master\MsProduct;
use App\Model\Master\MsProductCategory;
use Illuminate\Database\Eloquent\Model;

class ProductCategoryCombine extends Model
{

    protected $table = 'product_category_combine';

    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(MsProduct::class, 'ms_product_id', 'id');
    }

    public function product_category()
    {
        return $this->belongsTo(MsProductCategory::class, 'product_category_id', 'id');
    }

}
