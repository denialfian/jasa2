<?php

namespace App\Model\Product;

use App\Model\Master\MsProduct;
use Illuminate\Database\Eloquent\Model;

class ProductAdditional extends Model
{
    protected $fillable = [
        'additional_code',
    ];

    public function products()
    {
        return $this->hasMany(MsProduct::class, 'product_additionals_id', 'id');
    }
}
