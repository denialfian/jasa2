<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductStatus extends Model
{
    protected $fillable = [
        'name',
    ];
}
