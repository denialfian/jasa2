<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Services\Master\GeneralJournalService;

class PartDataExcelStockBundleDetail extends Model
{
    protected $table = 'ode_part_data_stock_bundle_details';

    protected $guarded = [];

    protected $appends = [
        'part_data_stock_id',
        'code_material',
        'part_description',
        'selling_price',
        'hpp_average',
    ];

    public function part_data_stock_bundle()
    {
        return $this->belongsTo(PartDataExcelStockBundle::class, 'part_data_stock_bundle_id');
    }

    public function part_data_stock_inventory()
    {
        return $this->belongsTo(PartDataExcelStockInventory::class, 'part_data_stock_inventories_id');
    }

    public function pdes_history()
    {
        return $this->hasMany(PDESHistory::class, 'part_data_stock_bundle_id', 'id');
    }

    public function getPartDataStockIdAttribute()
    {
        return $this->part_data_stock_inventory()->with('part_data_stock')->where('id', $this->part_data_stock_inventories_id)->value('part_data_stock_id');
    }

    public function getCodeMaterialAttribute()
    {
        return PartDataExcelStock::where('id', $this->part_data_stock_id)->value('code_material');
    }

    public function getPartDescriptionAttribute()
    {
        return PartDataExcelStock::where('id', $this->part_data_stock_id)->value('part_description');
    }

    public function getSellingPriceAttribute()
    {
        return $this->part_data_stock_inventory()->where('id', $this->part_data_stock_inventories_id)->value('selling_price');
    }

    public function getHppAverageAttribute() {
        $gj_cont = new GeneralJournalService();
        $val = $this->part_data_stock_inventory()->where('id', $this->part_data_stock_inventories_id)->first(['id']);
        return $val->hpp_average;
    }




}
