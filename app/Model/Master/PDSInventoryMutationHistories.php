<?php

namespace App\Model\Master;

use App\User;

use Illuminate\Database\Eloquent\Model;
use function App\Helpers\thousanSparator;

class PDSInventoryMutationHistories extends Model
{
    protected $table = 'pds_mutation_histories';

    protected $guarded = [];

    public function part_data_stock()
    {
        return $this->belongsTo(PartDataExcelStock::class, 'part_data_stock_id');
    }

    public function general_journal_detail()
    {
        return $this->belongsTo(GeneralJournalDetail::class, 'general_journal_detail');
    }

    public function part_data_excel()
    {
        return $this->belongsTo(PartsDataExcel::class, 'part_data_excel_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function mutation_join_in()
    {
        return $this->hasMany(PDSMutationJoinHistories::class, 'history_in');
    }

    public function mutation_join_out()
    {
        return $this->hasMany(PDSMutationJoinHistories::class, 'history_out');
    }



    public function mutation_join() {
        return $this->belongsTo(PDSMutationJoinHistories::class, 'mutation_join_id');
    }

}
