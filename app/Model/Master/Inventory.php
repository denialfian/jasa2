<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

use function App\Helpers\thousanSparator;

class Inventory extends Model
{
    protected $table = 'inventories';

    protected $guarded = [];

    protected $appends = ['cogs_value_format', 'stock_available'];

    public function product()
    {
        return $this->belongsTo(MsProduct::class, 'product_id');
    }

    public function mutation_log_in()
    {
        return $this->hasMany(InventoryMutationLog::class, 'inventories_id_in');
    }

    public function mutation_log_out()
    {
        return $this->hasMany(InventoryMutationLog::class, 'inventories_id_out');
    }

    public function batch()
    {
        return $this->belongsTo(Batch::class, 'ms_batch_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(MsWarehouse::class, 'ms_warehouse_id');
    }

    public function batch_item()
    {
        return $this->belongsTo(BatchItem::class, 'ms_batch_item_id');
    }

    public function unit_type()
    {
        return $this->belongsTo(UnitTypes::class, 'ms_unit_type_id');
    }

    // =========================
    public function getCogsValueFormatAttribute()
    {
        return thousanSparator($this->cogs_value);
    }

    public function getStockAvailableAttribute()
    {
        return $this->stock_now;
    }
}
