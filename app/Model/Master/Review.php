<?php

namespace App\Model\Master;

use App\Model\Technician\Technician;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
        'technician_id',
        'service_detail_id',
        'id_tech_team',
        'order_id',
        'user_id',
        'description',
        'published_at',
    ];

    // relasi
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function service_detail()
    {
        return $this->belongsTo(ServiceDetail::class, 'order_id');
    }

    public function technician()
    {
        return $this->belongsTo(Technician::class, 'technician_id');
    }
}
