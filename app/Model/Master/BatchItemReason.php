<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class BatchItemReason extends Model
{
    protected $guarded = [];

    public function batch_item()
    {
        return $this->hasMany(BatchItem::class);
    }

}
