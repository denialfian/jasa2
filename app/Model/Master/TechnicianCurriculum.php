<?php

namespace App\Model\Master;

use App\Model\Master\Curriculum;
use App\Model\Technician\Technician;
use Illuminate\Database\Eloquent\Model;

class TechnicianCurriculum extends Model
{
    protected $guarded = [];

    protected $table = 'technician_curriculum';

    public function curriculum()
    {
        return $this->belongsTo(Curriculum::class, 'curriculum_id', 'id');
    }

    public function technician()
    {
        return $this->belongsTo(Technician::class, 'technician_id', 'id');
    }

}
