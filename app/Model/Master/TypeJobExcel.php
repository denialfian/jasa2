<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TypeJobExcel extends Model
{
    protected $table = 'ode_type_job';

    protected $guarded = [];

}
