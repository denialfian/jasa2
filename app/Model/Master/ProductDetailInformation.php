<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class ProductDetailInformation extends Model
{
    protected $guarded = [];
    protected $table = 'product_detail_information';

}
