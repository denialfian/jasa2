<?php

namespace App\Model\Master;

use App\User;

use Illuminate\Database\Eloquent\Model;
use function App\Helpers\thousanSparator;

class PDESHistory extends Model
{
    protected $table = 'pdes_histories';

    protected $guarded = [];

    public function part_data_stock_inventory()
    {
        return $this->belongsTo(PartDataExcelStockInventory::class, 'part_data_stock_inventories_id');
    }

    public function part_data_stock_bundle_id()
    {
        return $this->belongsTo(PartDataExcelStockBundle::class, 'part_data_stock_bundle_id');
    }

    public function part_data_excel()
    {
        return $this->belongsTo(PartsDataExcel::class, 'part_data_excel_id');
    }

    public function general_journal_detail()
    {
        return $this->belongsTo(GeneralJournalDetail::class, 'general_journal_details_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
