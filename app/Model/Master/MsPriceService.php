<?php

namespace App\Model\Master;

use App\User;
use App\Model\Master\GeneralSetting;
use App\Model\Technician\Technician;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MsPriceService extends Model
{
    // use SoftDeletes;

    protected $table = 'ms_price_services';

    protected $appends = ['commission_deduction','commission_now'];

    protected $guarded = [];

    public function service_type()
    {
        return $this->belongsTo(MsServicesType::class, 'ms_services_types_id');
    }

    public function technician()
    {
        return $this->belongsTo(Technician::class, 'technicians_id');
    }

    public function product_group()
    {
        return $this->belongsTo(ProductGroup::class, 'product_group_id');
    }

    public function service_detail()
    {
        return $this->hasMany(ServiceDetail::class, 'ms_price_services_id');
    }

    public function getCommissionDeductionAttribute()
    {
        return ($this->value - $this->commission_value);
    }
    public function getCommissionNowAttribute()
    {
        $gs = GeneralSetting::where('id', 1)->first(['value','type']);
        $type_desc = '';
        if($gs) {
            if($gs->type == '1') {
                $type_desc = 'percent';
            } else {
                $type_desc = 'fixed rate';
            }
            return $data = [
                'value' => $gs->value,
                'type' => $gs->type,
                'type_desc' => $type_desc
            ];
        }

        return null;
    }
}
