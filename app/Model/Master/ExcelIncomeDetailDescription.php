<?php

namespace App\Model\Master;

use App\User;
use App\Model\Master\GeneralJournal;
use Illuminate\Database\Eloquent\Model;

class ExcelIncomeDetailDescription extends Model
{
    protected $table = 'excel_income_detail_description';

    protected $guarded = [];


    public function details_desc()
    {
        return $this->belongsTo(ExcelIncomeDetail::class, 'excel_income_detail_id');
    }

}
