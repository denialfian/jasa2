<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PartsDataExcelBundle extends Model
{
    protected $table = 'part_data_excel_bundle';
    protected $guarded = [];
    protected $appends = ['bundle_details'];

    public function order_data_excel()
    {
        return $this->belongsTo(OrdersDataExcel::class, 'orders_data_excel_id');
    }

    public function part_data_stock_bundle()
    {
        return $this->belongsTo(PartDataExcelStockBundle::class, 'part_data_stock_bundle_id');
    }

    public function getBundleDetailsAttribute()
    {
        return json_decode($this->bundle_details_obj);
    }

}
