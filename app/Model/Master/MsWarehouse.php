<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class MsWarehouse extends Model
{
    protected $table = 'ms_warehouses';

    // protected $fillable = [
    //     'name', 'meta', 'ms_district_id'
    // ];

    protected $guarded = [];

    // protected $casts = [
    //     'meta' => 'array',
    // ];

    public function inventory() {
        return $this->hasMany(Inventory::class);
    }
}
