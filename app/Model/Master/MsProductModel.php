<?php

namespace App\Model\Master;

use App\Model\Product\ProductBrand;
use App\Model\Master\MsProductCategory;
use Illuminate\Database\Eloquent\Model;

class MsProductModel extends Model
{
    protected $table = 'product_models';

    protected $guarded = [];

    public function product_category()
    {
        return $this->belongsTo(MsProductCategory::class, 'ms_product_category_id');
    }

    public function product_brand()
    {
        return $this->belongsTo(ProductBrand::class, 'product_brands_id', 'id');
    }

    public function products()
    {
        return $this->hasMany(MsProduct::class, 'product_model_id', 'id');
    }
}
