<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Chat extends Model
{
    protected $table = 'chats';
    protected $guarded = [];

    public function members_group()
    {
        return $this->hasMany(ChatMember::class, 'chat_id');
    }

    public function members()
    {
        return $this->hasMany(ChatMember::class, 'chat_id');
    }

    public function messages()
    {
        return $this->hasMany(ChatMessage::class, 'chat_id');
    }

    public function last_unread_message()
    {
        return $this->hasOne(ChatMessage::class, 'chat_id')->whereNull('read_at')->orderBy('created_at', 'DESC');
    }

    public function last_message()
    {
        return $this->hasOne(ChatMessage::class, 'chat_id')->orderBy('created_at', 'DESC');
    }

    public function unread_message()
    {
        return $this->hasMany(ChatMessage::class, 'chat_id')->whereNull('read_at')->where('user_id', '!=', Auth::id());
    }

    public function unreads()
    {
        return $this->hasMany(ChatMessage::class, 'chat_id')->whereNull('read_at');
    }
}
