<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class TeknisiInfo extends Model
{
    protected $fillable = [
        'attachment',
        'no_identity',
        'ms_curriculum_id',
        'user_id'
    ];

    protected $appends = ['link_image'];

    public static function getImagePathUpload()
    {
        return 'public/attachment';
    }

    public function getLinkImageAttribute()
    {
        return ($this->attachment == null) ? '' : asset('/storage/attachment/' . $this->attachment);
    }

    public function curriculum()
    {
        return $this->belongsTo(Curriculum::class, 'ms_curriculum_id', 'id');
    }

    public function devplant()
    {
        return $this->belongsTo(DevelopmentProgram::class, 'ms_curriculum_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
