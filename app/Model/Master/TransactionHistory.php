<?php

namespace App\Model\Master;
use App\User;
use App\Model\Technician\Technician;
use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{
    protected $table = "transaction_history";
    protected $guarded = [];

    protected $appends = [
        'spareparts',
    ];

    public function getSparepartsAttribute()
    {
        return json_decode($this->parts);
    }

    public function order_status()
    {
        return $this->belongsTo(OrdersStatus::class, 'status');
    }

    public function technician()
    {
        return $this->belongsTo(Technician::class, 'technician_name');
    }
}


