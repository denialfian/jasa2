<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class GaleryImages extends Model
{
    protected $table = 'galery_image';
    protected $guarded = [];
}
