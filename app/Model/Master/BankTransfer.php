<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class BankTransfer extends Model
{
    protected $table = 'bank_transfer';
    protected $fillable = [
        'bank_name',
        'name',
        'virtual_code',
        'desc',
        'icon',
        'is_active',
        'is_parent'
    ];

    public static function getImagePathUpload()
    {
        return 'public/bankIcon';
    }
}
