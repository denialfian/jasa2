<?php

namespace App\Model\Master;

use App\Model\Master\MsPriceService;
use Illuminate\Database\Eloquent\Model;

class ItemDetail extends Model
{
    protected $table = 'item_details';

    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(MsProduct::class, 'ms_products_id');
    }

    public function inventory()
    {
        return $this->belongsTo(Inventory::class, 'inventory_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'orders_id');
    }
}
