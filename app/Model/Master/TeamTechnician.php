<?php

namespace App\Model\Master;

use App\Model\Technician\Technician;
use Illuminate\Database\Eloquent\Model;

class TeamTechnician extends Model
{
    protected $table = 'team_technicians';

    protected $guarded = [];

    public function tech_team()
    {
        return $this->belongsTo(MsTechTeam::class, 'team_id');
    }

    public function technician()
    {
        return $this->belongsTo(Technician::class, 'technicians_id');
    }

}
