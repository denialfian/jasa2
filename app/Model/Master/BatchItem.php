<?php

namespace App\Model\Master;

use App\Model\Product\ProductAttribute;
use App\Model\Product\ProductVarian;
use App\Model\Product\ProductVendor;
use Illuminate\Database\Eloquent\Model;

use function App\Helpers\onlyNumber;
use function App\Helpers\thousanSparator;

class BatchItem extends Model
{
    protected $table = 'batch_items';
    protected $guarded = [];

    protected $appends = ['product_name', 'quantity_format', 'value_format'];

    # Defining A Mutator

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = onlyNumber($value);
    }

    public function setQuantityAttribute($value)
    {
        $this->attributes['quantity'] = onlyNumber($value);
    }


    public function batch()
    {
        return $this->belongsTo(Batch::class);
    }

    public function product()
    {
        return $this->belongsTo(MsProduct::class);
    }

    public function inventory()
    {
        return $this->hasOne(Inventory::class, 'ms_batch_item_id');
    }

    public function status()
    {
        return $this->belongsTo(BatchStatus::class, 'batch_status_id', 'id');
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    public function productVendor()
    {
        return $this->belongsTo(ProductVendor::class);
    }

    public function varian()
    {
        return $this->belongsTo(ProductVarian::class, 'product_varian_id', 'id');
    }

    public function batch_item_reason()
    {
        return $this->belongsTo(BatchItemReason::class, 'batch_status_id', 'id');
    }

    public function getAvailableAttribute()
    {
        $productVendor = ProductVendor::where('ms_product_id', $this->product_id)
            ->where('vendor_id', $this->productVendor->vendor_id)
            ->first();

        $response = ProductVarian::where('product_vendor_id', $productVendor->id)
            ->with('productVarianAttributes.term', 'productVarianAttributes.attribute')
            ->get();

        $combination = [];
        $att = [];
        foreach ($response as $varian) {
            foreach ($varian->productVarianAttributes as $pAttribute) {
                $combination[$pAttribute->attribute->id][$pAttribute->term->id] = $pAttribute;
            }
        }

        foreach ($combination as $attribute_id => $com) {
            $att[] = [
                'attribute' => ProductAttribute::find($attribute_id),
                'terms' => $com,
            ];
        }

        return $att;
    }

    public function getProductNameAttribute()
    {
        if ($this->product == null) {
            return '';
        }
        $name = $this->product->name;
        if ($this->product_varian_id != null) {

            $variation = json_decode($this->varian->variation);
            foreach ($variation as $attribute => $term) {
                if (!is_numeric($attribute)) {
                    $name .= ' ' . $attribute . ' : ' . $term . ' ';
                }
            }
        }
        if ($this->product_vendor_id != null) {
            $name .= ' ' . $this->productVendor->vendor->name;
        }

        return ucfirst($name);
    }

    // =========================
    public function getQuantityFormatAttribute()
    {
        return thousanSparator($this->quantity);
    }
    public function getValueFormatAttribute()
    {
        return thousanSparator($this->value);
    }
}
