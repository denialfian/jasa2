<?php

namespace App\Model\Master;

use App\User;
use App\Model\Master\OrdersDataExcel;
use Illuminate\Database\Eloquent\Model;

class HppItem extends Model
{
    protected $table = 'hpp_items';

    protected $guarded = [];

    public function hpp_items()
    {
        return $this->hasOne(PartDataExcelStock::class, 'pdes_id');
    }

}
