<?php

namespace App\Model\Master;

use App\Model\Technician\Technician;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = [
        'technician_id',
        'service_detail_id',
        'id_tech_team',
        'order_id',
        'user_id',
        'value',
        'published_at',
    ];

    // relasi
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'orders_id');
    }

    public function service_detail()
    {
        return $this->belongsTo(ServiceDetail::class, 'order_id');
    }

    public function technician()
    {
        return $this->belongsTo(Technician::class, 'technician_id');
    }

    public function getRatingStarUi($light = '', $dark = ''){
        if ($light == '') {
            $light = '<span class="star"><i class="fa fa-star"></i></span>';
        }
        if ($dark == '') {
            $dark = '<span class="star"><i class="fa fa-star-o"></i></span>';
        }
        $rating = round($this->value);
        
        $star = '';
        if ($rating > 0) {
            $range = range(1, $rating);
            foreach ($range as $key => $value) {
                $star .= $light . ' ';
            }   
            
            if ($rating < 5) {
                $range = range(1, 5 - $rating);
                foreach ($range as $key => $value) {
                    $star .= $dark. ' ';
                } 
            }  
        }else{
            $range = range(1, 5);
            foreach ($range as $key => $value) {
                $star .= $dark. ' ';
            } 
        }

        return $star;
    }
}
