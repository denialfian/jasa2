<?php

namespace App\Model\Master;

use App\User;
use App\Model\Master\OrdersDataExcel;
use Illuminate\Database\Eloquent\Model;

class OrdersDataExcelLog extends Model
{
    protected $table = 'orders_data_excel_logs';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
