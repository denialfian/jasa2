<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class EngineerCodeExcel extends Model
{
    protected $table = 'ode_engineer_code';

    protected $guarded = [];

}
