<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class DevPlant extends Model
{
    protected $table = 'development_plan';
    protected $fillable = [
        'user_id',
        'curr_sequence_id',
        'status',
        'date_time'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function curriculum()
    {
        return $this->belongsTo(CurriculumSequence::class, 'curr_sequence_id');
    }
}
