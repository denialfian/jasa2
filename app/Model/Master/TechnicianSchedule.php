<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class TechnicianSchedule extends Model
{
    protected $guarded = [];
    protected $table = 'technician_schedules';

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function technician()
    {
        return $this->belongsTo(Technician::class, 'technician_id', 'id');
    }

}
