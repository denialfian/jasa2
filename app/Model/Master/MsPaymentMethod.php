<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class MsPaymentMethod extends Model
{
    protected $table = 'ms_payment_methods';
    protected $guarded = [];

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
