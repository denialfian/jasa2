<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

use function App\Helpers\thousanSparator;

class InventoryMutationHistories extends Model
{
    protected $table = 'inventory_mutation_histories';

    protected $guarded = [];

    public function inventory()
    {
        return $this->belongsTo(Inventory::class, 'inventory_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function technician()
    {
        return $this->belongsTo(Technician::class, 'technician_id');
    }

    public function mutation_join_in()
    {
        return $this->hasMany(MutationJoinHistories::class, 'history_in');
    }

    public function mutation_join_out()
    {
        return $this->hasMany(MutationJoinHistories::class, 'history_out');
    }

    public function mutation_join() {
        return $this->belongsTo(MutationJoinHistories::class, 'mutation_join_id');
    }

}
