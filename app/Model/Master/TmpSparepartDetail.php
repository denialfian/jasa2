<?php

namespace App\Model\Master;

use App\Model\Master\MsPriceService;
use Illuminate\Database\Eloquent\Model;

class TmpSparepartDetail extends Model
{
    protected $table = 'tmp_sparepart_details';

    protected $guarded = [];
}
