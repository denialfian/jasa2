<?php

namespace App\Model\Master;
use App\User;
use stdClass;
use Carbon\Carbon;
use App\Model\Master\GeneralSetting;
use Illuminate\Database\Eloquent\Model;

class EwalletHistory extends Model
{
    protected $table = "e_wallet_history";
    protected $guarded = [];

    protected $appends = [
        'countdown_autocancel_topup'
    ];

    public function getCountdownAutocancelTopupAttribute()
    {
        $settings = GeneralSetting::all();
        $created_at = EwalletHistory::where('id', $this->id)->where('status', '=', 'unpaid')->value('created_at');
        $val = Carbon::parse($created_at);
        return $val = $val->addHours(intval($settings[9]->value))->format('Y-m-d H:i');
    }

    public function bank()
    {
        return $this->belongsTo(BankTransfer::class, 'payment_metod_id');
    }

    public function voucer()
    {
        return $this->belongsTo(VoucherGeneral::class, 'voucher_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function getImagePathUpload()
    {
        return 'public/buktiTransfer';
    }

    public const PAYMENT_CHANNELS = [
        'credit_card',
        'mandiri_clickpay',
        'cimb_clicks',
        'bca_klikbca',
        'bca_klikpay',
        'bri_epay',
        'telkomsel_cash',
        'echannel',
        'permata_va',
        'other_va',
        'bca_va',
        'bni_va',
        'indomaret',
        'danamon_online',
        'akulaku'
    ];

    public const EXPIRY_DURATION = 'days';
    public const EXPIRY_UNIT = '7';

    public const CHALLENGE = 'challenge';
    public const SUCCESS = 'success';
    public const SETTLEMENT = 'settlement';
    public const PENDING = 'pending';
    public const DENY = 'deny';
    public const EXPIRE = 'expire';
    public const CANCEL = 'cancel';


    public const PAYMENTCODE = 'pay';
    public const PAID = 'paid';
    public const UNPAID = 'unpaid';
    public const CONFIRMED = 'confrim';

    public function isPaid()
    {
        return $this->payment_status == self::PAID;
    }

}


