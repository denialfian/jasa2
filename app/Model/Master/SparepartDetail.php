<?php

namespace App\Model\Master;

use App\Model\Master\MsPriceService;
use Illuminate\Database\Eloquent\Model;

class SparepartDetail extends Model
{
    protected $table = 'sparepart_details';

    protected $guarded = [];

    protected $appends = [
        'image_sparepart'
    ];

    public function getImageSparepartAttribute()
    {
        if(isset($this->technician_sparepart->images)) {
            if ($this->technician_sparepart->images == null) {
                return asset('/storage/user/profile/avatar/default_avatar.jpg');
            }
            return asset('/storage/services/' . $this->technician_sparepart->images);
        }
        return '-';

    }

    public function technician_sparepart()
    {
        return $this->belongsTo(TechnicianSparepart::class, 'technicians_sparepart_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'orders_id');
    }

    public function priceThousandSeparator(){
        return number_format($this->price);
    }

}
