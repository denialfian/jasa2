<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class JasaExcel extends Model
{
    protected $table = 'ode_jasa';

    protected $guarded = [];

    public function jasa_data_excel()
    {
        return $this->hasMany(JasaDataExcel::class, 'jasa_id', 'id');
    }

}
