<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Educations extends Model
{
    protected $table = 'educations';
    protected $fillable = [
        'institution',
        'place',
        'education_start_date',
        'education_end_date',
        'type',
        'major',
        'user_id'
    ];
}
