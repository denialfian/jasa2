<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Model\Master\OrdersDataExcelLog;

class GeneralJournal extends Model
{
    protected $table = 'general_journal';

    protected $guarded = [];

    protected $appends = [
        'total_kredit',
        'total_debit',
        'total_balance'
    ];



    public static function getImagePathUpload()
    {
        return 'public/general-journal';
    }

    public function general_journal_detail()
    {
        return $this->hasMany(GeneralJournalDetail::class, 'general_journal_id', 'id');
    }

    public function general_journal_log()
    {
        return $this->hasMany(GeneralJournalLog::class, 'general_journal_id', 'id');
    }

    public function getTotalBalanceAttribute() {
        $collect = $this->general_journal_detail;
        $balance = 0;
        if(count($collect) > 0) {
            foreach($collect as $row) {
                $debit = ($row['debit'] != '' ? +intval($row['debit']) : 0);
                $kredit = ($row['kredit'] != '' ? -intval($row['kredit']) : 0);
                $balance += $debit + $kredit;
            }
        }
        return $balance;
    }

    public function getTotalDebitAttribute() {
        $collect = $this->general_journal_detail;
        $value = 0;
        if(count($collect) > 0) {
            foreach($collect as $row) {
                $debit = ($row['debit'] != '' ? +intval($row['debit']) : 0);
                $value += $debit;
            }
        }
        return $value;

    }
    public function getTotalKreditAttribute() {
        $collect = $this->general_journal_detail;
        $value = 0;
        if(count($collect) > 0) {
            foreach($collect as $row) {
                $kredit = ($row['kredit'] != '' ? -intval($row['kredit']) : 0);
                $value += $kredit;
            }
        }
        return $value;

    }


}
