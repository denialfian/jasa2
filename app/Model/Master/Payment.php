<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = "payment";
    protected $guarded = [];

    public const PAYMENT_CHANNELS = [
        'credit_card',
        'mandiri_clickpay',
        'cimb_clicks',
        'bca_klikbca',
        'bca_klikpay',
        'bri_epay',
        'telkomsel_cash',
        'echannel',
        'permata_va',
        'other_va',
        'bca_va',
        'bni_va',
        'indomaret',
        'danamon_online',
        'akulaku'
    ];

    public const EXPIRY_DURATION = 'minute';
    public const EXPIRY_UNIT = '2';

    public const CHALLENGE = 'challenge';
    public const SUCCESS = 'success';
    public const SETTLEMENT = 'settlement';
    public const PENDING = 'pending';
    public const DENY = 'deny';
    public const EXPIRE = 'expire';
    public const CANCEL = 'cancel';


    public const PAYMENTCODE = 'pay';
    public const PAID = 'paid';
    public const UNPAID = 'unpaid';
    public const CONFIRMED = 'confrim';

    public function isPaid()
    {
        return $this->payment_status == self::PAID;
    }

}




