<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class SymptomCode extends Model
{
    protected $guarded = [];
    protected $table = 'ms_symptom_codes';
}
