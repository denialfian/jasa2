<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class DevelopmentProgram extends Model
{
    protected $table = 'development_programs';
    protected $fillable = [
        'program_name',
        'desc',
    ];

    public function teknisiinfo() {
        return $this->hasMany(TeknisiInfo::class, 'ms_curriculum_id', 'id');
    }
}
