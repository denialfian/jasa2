<?php

namespace App\Model\Master;

use App\User;
use App\Model\Master\GeneralJournal;
use Illuminate\Database\Eloquent\Model;
use App\Model\Master\ExcelIncomeDetailDescription;

class ExcelIncomeDetail extends Model
{
    protected $table = 'excel_income_detail';

    protected $guarded = [];

    protected $dates = ['date_transaction', 'date_transfer'];

    public function excel_income()
    {
        return $this->belongsTo(ExcelIncome::class, 'excel_income_id');
    }

    public function details_desc()
    {
        return $this->hasMany(ExcelIncomeDetailDescription::class, 'excel_income_detail_id', 'id');
    }



}
