<?php

namespace App\Model\Master;

use App\User;
use Illuminate\Database\Eloquent\Model;

class StatusExcel extends Model
{
    protected $table = 'ode_status';

    protected $guarded = [];

}
