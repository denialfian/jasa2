<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'ms_banner';
    protected $guarded = [];

    protected $appends = ['link_image'];

    public function getLinkImageAttribute()
    {
        return asset('/storage/banner/' . $this->images);
    }
}
