<?php

namespace App\Model\Master;
use App\User;
use Auth;
use Illuminate\Database\Eloquent\Model;

class EwalletPayment extends Model
{
    protected $table = "e_wallet_payment";
    protected $guarded = [];

    public function orderhistory()
    {
        return $this->belongsTo(EwalletHistory::class, 'wallet_history_id');
    }

    public function getproduct()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function order() {
        return $this->belongsTo(Order::class, 'order_id');
    }
}


