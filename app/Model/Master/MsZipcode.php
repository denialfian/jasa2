<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class MsZipcode extends Model
{
    protected $table = 'ms_zipcodes';

    protected $fillable = [
        'zip_no', 'ms_district_id'
    ];

    public function district()
    {
        return $this->belongsTo(MsDistrict::class, 'ms_district_id');
    }

    public function address() {
        return $this->hasMany(MsAddress::class);
    }

}
