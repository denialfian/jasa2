<?php

namespace App\Model\Master;

use Illuminate\Database\Eloquent\Model;

class BatchShipmentHistory extends Model
{
    protected $guarded = [];
    protected $table = 'batch_shipment_histories';

    public function batch()
    {
        return $this->belongsTo(BatchStatus::class, 'batch_id', 'id');
    }

    public function batch_status_shipment()
    {
        return $this->belongsTo(BatchStatusShipment::class, 'batch_status_shipment_id', 'id');
    }

}
