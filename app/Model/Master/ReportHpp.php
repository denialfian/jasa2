<?php

namespace App\Model\Master;

use App\User;

use Illuminate\Database\Eloquent\Model;
use function App\Helpers\thousanSparator;

class ReportHpp extends Model
{
    protected $table = 'report_hpp';

    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

    public function part_data_stock_inventory()
    {
        return $this->belongsTo(PartDataExcelStockInventory::class, 'part_data_stock_inventories_id');
    }

    public function part_data_excel()
    {
        return $this->belongsTo(PartsDataExcel::class, 'part_data_excel_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
