<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ImageInvoice extends Model
{
    protected $table = 'image_invoice';
    protected $fillable = [
        'type', 'image_invoice', 'ms_b2b_detail'
    ];
}
