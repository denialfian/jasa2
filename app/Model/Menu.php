<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;
class Menu extends Model
{
    protected $fillable = [
        'name', 'url', 'icon', 'parent_id', 'permission_id', 'display_order', 'module', 'permission_item'
    ];

    protected $guarded = [];

    public function childs()
    {
        return $this->hasMany(Menu::class, 'parent_id', 'id');
    }

    public function permission(){
        return $this->belongsTo(Permission::class);
    }

}
