<?php

namespace App\Mail\Register;

use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Model\Master\EmailOther;
use Illuminate\Support\Facades\URL;
use App\Model\Master\GeneralSetting;
use App\Model\Master\WebSetting;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $url;
    public $facebook;
    public $youtube;
    public $instagram;
    public $twiter;
    public $get_email;
    public $get_number;
    public $get_address;
    public $email_content;
    public $replace_string;
    public $get_img_astech;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, string $url)
    {
        $this->user = $user;
        $this->url = $url;
        $this->youtube = GeneralSetting::where('name', 'youtube')->first();
        $this->instagram = GeneralSetting::where('name', 'instagram')->first();
        $this->twiter = GeneralSetting::where('name', 'twiter')->first();
        $this->facebook = GeneralSetting::where('name', 'facebook')->first();
        $this->get_email =  GeneralSetting::where('name', 'email')->first();
        $this->get_number =  GeneralSetting::where('name', 'contact_us')->first();
        $this->get_address =  GeneralSetting::where('name', 'addreses')->first();
        $this->get_img_astech =  WebSetting::first();
        $this->email_content = EmailOther::where('name_module', 'verify_registration')->first();
        $vars = array(
            "[{NAME_CUSTOMER}]" => $this->user->name,
        );
        $this->replace_string = strtr($this->email_content->content,$vars);

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.register.verify_email');
    }
}
