<?php

namespace App\Listeners;

use App\Events\TeknisiRejectedEvent;
use App\Notifications\FailedApprovalNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;


class SendFailedApproval
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TeknisiRejectedEvent  $event
     * @return void
     */
    public function handle(TeknisiRejectedEvent $event)
    {
        $user = $event->user;
        $request = $event->request;
        // dd($request->all());
        $user->notify(new FailedApprovalNotification($user, $request));
    }
}
