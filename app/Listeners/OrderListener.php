<?php

namespace App\Listeners;

use App\User;
use App\Mail\OrderEmail;
use App\Events\OrderEvent;
use App\Notifications\OrderNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderListener  $event
     * @return void
     */
    public function handle(OrderEvent $event)
    {
        // type 1 = customer, 2 = teknisi, 3 = admin.

        $modul = $event->modul;
        $is_order = false;
        if ($modul instanceof \Illuminate\Database\Eloquent\Model) {
            $is_order = true;
        }
        if($event->email_content->type == 1) {
            \Mail::to(($is_order == true ? $event->modul->user->email : (isset($event->modul['customer_email']) ? $event->modul['customer_email'] : null)))
            ->send(new OrderEmail($event->modul, $event->email_content));
        }
        if($event->email_content->type == 2) {
            \Mail::to(($is_order == true ? $event->modul->service_detail[0]->technician->user->email : (isset($event->modul['technician_email']) ? $event->modul['technician_email'] : null)))
            ->send(new OrderEmail($event->modul, $event->email_content));
        }

        if($event->email_content->type == 3) {
            $emails_name = User::getAdminRole()->pluck('email');
            foreach($emails_name as $key => $email) {
                \Mail::to($email)
                ->send(new OrderEmail($event->modul, $event->email_content));
            }

        }



    }


}
