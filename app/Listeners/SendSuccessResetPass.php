<?php

namespace App\Listeners;

use App\Events\ResetPasswordEvent;
use App\Notifications\PasswordResetRequestNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\User;
use Illuminate\Http\Request;


class SendSuccessResetPass
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Handle the event.
     *
     * @param  ResetPasswordEvent  $event
     * @return void
     */
    public function handle(ResetPasswordEvent $event)
    {
        $user = $event->user;
        $request = $event->request;
        $resetPass = $event->resetPass;
        // dd($request->all());
        $user->notify(new PasswordResetRequestNotification($user, $request, $resetPass));
    }
}
