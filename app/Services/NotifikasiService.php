<?php

namespace App\Services;

use App\Model\Master\GeneralSetting;
use App\Model\Master\Order;
use App\Notifications\GaransiNotification;
use App\Notifications\Order\OrderNotification;
use App\Notifications\OrderComplaintNotification;
use App\User;
use Illuminate\Support\Facades\Auth;

class NotifikasiService
{
    public function kirimNotifikasiOrder($order)
    {
        $auth = Auth::id();
        $userAdmin = User::getAdminRole();
        foreach ($userAdmin as $user) {
            if ($auth  != $user->id) {
                $url = url('/admin/order/service_detail/' . $order->id);
                $title = ($order->garansi == null ? '' : '(GARANSI) ') . '(ORDER) ' . $order->order_status->name . ' ' . $order->code;
                $opsi = [
                    'name' => $title,
                    'description' => $order->service->name,
                    'icon' => 'fa fa-exchange',
                    'color' => 'border-primary bg-primary',
                    'url' => $url,
                    'type' => 'order',
                    'order_id' => $order->id,
                ];
                $user->notify(new OrderNotification($order, $opsi));
                // $dataPush = [
                //     'title' => $order->service->name,
                //     'body' => $title,
                // ];
                // $this->firebasePushNotifikasi($user, $dataPush);
            }
        }

        if ($auth != $order->user->id || $order->order_status->id == 5) {
            $userCustomer = $order->user;
            $urlCustomer = url('/customer/service_detail/' . $order->id);
            $opsiCustomer = [
                'name' => ($order->garansi == null ? '' : '(GARANSI) ') . '(ORDER) ' . $order->order_status->name . ' ' . $order->code,
                'description' => $order->service->name,
                'icon' => 'fa fa-exchange',
                'color' => 'border-primary bg-primary',
                'url' => $urlCustomer,
                'type' => 'order',
                'order_id' => $order->id,
            ];
            $userCustomer->notify(new OrderNotification($order, $opsiCustomer));
            $dataPush = [
                'title' => $order->service->name,
                'body' => ($order->garansi == null ? '' : '(GARANSI) ') . '(ORDER) ' . $order->order_status->name . ' ' . $order->code,
            ];
            $this->firebasePushNotifikasi($userCustomer, $dataPush);
        }

        if ($auth  != $order->detail->technician->user->id || $order->order_status->id == 5) {
            $userTeknisi = $order->detail->technician->user;
            $urlTeknisi = url('/teknisi/request-job-accept/' . $order->id);
            $opsiTeknisi = [
                'name' => ($order->garansi == null ? '' : '(GARANSI) ') . '(ORDER) ' . $order->order_status->name . ' ' . $order->code,
                'description' => $order->service->name,
                'icon' => 'fa fa-exchange',
                'color' => 'border-primary bg-primary',
                'url' => $urlTeknisi,
                'type' => 'order',
                'order_id' => $order->id,
            ];
            $userTeknisi->notify(new OrderNotification($order, $opsiTeknisi));
            $dataPush = [
                'title' => $order->service->name,
                'body' => ($order->garansi == null ? '' : '(GARANSI) ') . '(ORDER) ' . $order->order_status->name . ' ' . $order->code,
            ];
            $this->firebasePushNotifikasi($userTeknisi, $dataPush);
        }
    }

    public function kirimNotikasiGaransi($garansi, $order)
    {
        $auth = Auth::id();
        $userAdmin = User::getAdminRole();
        foreach ($userAdmin as $user) {
            if ($auth  != $user->id) {
                $url = url("admin/garansi/" . $garansi->id . "/detail");
                $opsi = [
                    'name' => '(GARANSI) ' . $garansi->garansi_no,
                    'description' => $order->code,
                    'icon' => 'fa fa-exchange',
                    'color' => 'border-primary bg-primary',
                    'url' => $url,
                    'type' => 'garansi',
                    'garansi_id' => $garansi->id,
                ];
                $user->notify(new GaransiNotification($order, $opsi));
                $dataPush = [
                    'title' => '(GARANSI) ' . $garansi->garansi_no,
                    'body' => $order->code,
                ];
                $this->firebasePushNotifikasi($user, $dataPush);
            }
        }

        if ($auth  != $order->user->id) {
            $userCustomer = $order->user;
            $urlCustomer = url("customer/garansi/" . $garansi->id . "/detail");
            $opsiCustomer = [
                'name' => '(GARANSI) ' . $garansi->garansi_no,
                'description' => $order->code,
                'icon' => 'fa fa-exchange',
                'color' => 'border-primary bg-primary',
                'url' => $urlCustomer,
                'type' => 'garansi',
                'garansi_id' => $garansi->id,
                'order_id' => $order->id,
            ];
            $userCustomer->notify(new OrderNotification($order, $opsiCustomer));
            $dataPush = [
                'title' => '(GARANSI) ' . $garansi->garansi_no,
                'body' => $order->code,
            ];
            $this->firebasePushNotifikasi($userCustomer, $dataPush);
        }

        if ($auth  != $order->detail->technician->user->id) {
            $userTeknisi = $order->detail->technician->user;
            $urlTeknisi = url('/teknisi/request-job-accept/' . $order->id);
            $opsiTeknisi = [
                'name' => '(GARANSI) ' . $order->code,
                'description' => $order->code,
                'icon' => 'fa fa-exchange',
                'color' => 'border-primary bg-primary',
                'url' => $urlTeknisi,
                'type' => 'garansi',
                'garansi_id' => null,
                'order_id' => $order->id,
            ];
            $userTeknisi->notify(new OrderNotification($order, $opsiTeknisi));
            $dataPush = [
                'title' => '(GARANSI) ' . $order->code,
                'body' => $order->code,
            ];
            $this->firebasePushNotifikasi($userTeknisi, $dataPush);
        }
    }

    public function kirimNotikasiGaransiStatus($garansi, $type)
    {
        $auth = Auth::id();
        $order = $garansi->order;

        if ($auth  != $order->user->id) {
            $userCustomer = $order->user;
            $urlCustomer = url("customer/garansi/" . $garansi->id . "/detail");
            $opsiCustomer = [
                'name' => '(GARANSI) ' . $type . ' ' . $garansi->garansi_no,
                'description' => $order->code,
                'icon' => 'fa fa-exchange',
                'color' => 'border-primary bg-primary',
                'url' => $urlCustomer,
                'type' => 'garansi'
            ];
            $userCustomer->notify(new OrderNotification($order, $opsiCustomer));
            $dataPush = [
                'title' => '(GARANSI) ' . $order->code,
                'body' => 'your request has been accepted',
            ];
            $this->firebasePushNotifikasi($userCustomer, $dataPush);
        }

        if ($auth  != $order->detail->technician->user->id) {
            $userTeknisi = $order->detail->technician->user;
            $urlTeknisi = url('/teknisi/request-job-accept/' . $order->id);
            $opsiTeknisi = [
                'name' => '(GARANSI) ' . $type . ' ' . $order->code,
                'description' => $order->code,
                'icon' => 'fa fa-exchange',
                'color' => 'border-primary bg-primary',
                'url' => $urlTeknisi,
                'type' => 'garansi'
            ];
            $userTeknisi->notify(new OrderNotification($order, $opsiTeknisi));
            $dataPush = [
                'title' => '(GARANSI) ' . $order->code,
                'body' => 'claim warranty request has been accepted',
            ];
            $this->firebasePushNotifikasi($userTeknisi, $dataPush);
        }
    }

    public function kirimNotifikasiComplain($order)
    {
        $auth = Auth::id();
        $userAdmin = User::getAdminRole();
        foreach ($userAdmin as $user) {
            if ($auth  != $user->id) {
                $url = url('/admin/transacsion_list/complaint/detail/' . $order->id);
                $opsi = [
                    'name' => '(COMPLAINT) #' . $order->order_status->name . ' ' . $order->code,
                    'description' => $order->service->name,
                    'icon' => 'fa fa-exchange',
                    'color' => 'border-primary bg-primary',
                    'url' => $url,
                    'type' => 'complaint'
                ];
                $user->notify(new OrderComplaintNotification($order, $opsi));
            }
        }

        if ($auth  != $order->user->id) {
            $userCustomer = $order->user;
            $urlCustomer = url('/customer/transacsion_list/complaint/detail/' . $order->id);
            $opsiCustomer = [
                'name' => '(COMPLAINT) #' . $order->order_status->name . ' ' . $order->code,
                'description' => $order->service->name,
                'icon' => 'fa fa-exchange',
                'color' => 'border-primary bg-primary',
                'url' => $urlCustomer,
                'type' => 'complaint'
            ];
            $userCustomer->notify(new OrderComplaintNotification($order, $opsiCustomer));
        }

        if ($auth  != $order->detail->technician->user->id) {
            $userTeknisi = $order->detail->technician->user;
            $urlTeknisi = url('/teknisi/complaint-jobs/detail/' . $order->id);
            $opsiTeknisi = [
                'name' => '(COMPLAINT) #' . $order->order_status->name . ' ' . $order->code,
                'description' => $order->service->name,
                'icon' => 'fa fa-exchange',
                'color' => 'border-primary bg-primary',
                'url' => $urlTeknisi,
                'type' => 'complaint'
            ];
            $userTeknisi->notify(new OrderComplaintNotification($order, $opsiTeknisi));
        }
    }

    public function firebasePushNotifikasi($user, $data_notification)
    {
        $serverKey = 'AAAAt5mtkzg:APA91bFhH80Wp_hDORBCCeV-V0ahUUfUW_9QYoHa1dnOoovj_q7oyjjkFRmgnNAT7X3q0JSystsFO4eHzNlZ2AFAPZe4vE9-9kOo8ytr0dZ8a0YBKUmTluociemOc_CLOabAeH1r8srB';
        $firebaseServerKey = GeneralSetting::where('name', 'firebase_serverKey')->first();
        $user = User::findOrFail($user->id);
        if (empty($user->device_token)) {
            return;
        }

        $click_action = null;
        if (isset($data_notification['type'])) {
            if ($data_notification['type'] == 'chat' || $data_notification['type'] == 'chat_map') {
                $click_action = 'FLUTTER_NOTIFICATION_CLICK';
            }
        }

        $data = [
            "to" => $user->device_token,
            "notification" =>
            [
                "title" => isset($data_notification['title']) ?  $data_notification['title'] : 'title',
                "body" =>  isset($data_notification['body']) ?  $data_notification['body'] : 'body',
                'click_action' => $click_action,
            ],
            'data' => $data_notification,
        ];

        $dataString = json_encode($data);

        // if ($firebaseServerKey != null) {
        //     $serverKey = $firebaseServerKey->value;
        // }


        $headers = [
            'Authorization: key=' . $serverKey,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        curl_exec($ch);
    }
}
