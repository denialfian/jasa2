<?php

namespace App\Services\Technician;

use App\Model\Technician\JobExperience;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class JobExperienceService
{
    public $experience;

    /**
     * get list query
     * 
     * @return App\Model\Technician\JobExperience;
     */
    public function list()
    {
        return JobExperience::with([
            'user',
            'job_title.job_title_category'
        ]);
    }

    /**
     * datatables data
     * 
     * @return \App\Model\Technician\JobExperience
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     * 
     * @return \App\Model\Technician\JobExperience
     */
    public function select2(Request $request)
    {
        return JobExperience::where('company_name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * find data
     *
     * @param int $id
     * 
     * @return \App\Model\Technician\JobExperience
     */
    public function find(int $id)
    {
        return JobExperience::where('id', $id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Technician\JobExperience
     * 
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return JobExperience::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     * 
     * @return bool
     * 
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getExperience()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     * 
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return $this->getExperience()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return JobExperience::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'position' => $data['position'],
            'company_name' => $data['company_name'],
            'period_start' => $data['period_start'],
            'period_end' => $data['period_end'],
            'type' => $data['type'],
            'user_id' => $data['user_id'],
            'job_title_id' => $data['job_title_id'],
        ];
    }

    public function setExperience(JobExperience $experience)
    {
        $this->experience = $experience;
        return $this;
    }

    public function getExperience()
    {
        return $this->experience;
    }
}
