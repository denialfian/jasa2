<?php

namespace App\Services\Master;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Model\Master\RepairCode;
use App\Exceptions\HasChildException;
use Illuminate\Database\QueryException;

class RepairCodeService
{
    public $repairCode;

    /**
     * get list query
     *
     * @return \App\Model\Master\;
     */
    public function list()
    {
        return RepairCode::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\RepairCode
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\RepairCode
     */
    public function select2(Request $request)
    {
        return RepairCode::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * find data
     *
     * @param int $id
     *
     * @return \App\Model\Master\RepairCode
     */
    public function find(int $id)
    {
        return RepairCode::where('id', $id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\RepairCode
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return RepairCode::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getRepairCode()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return $this->getRepairCode()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function deleteById(array $ids)
    {
        try {
            return RepairCode::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * generate data
     *
     * @return array
     */
    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
        ];
    }

    public function setRepairCode(RepairCode $repairCode)
    {
        $this->repairCode = $repairCode;
        return $this;
    }

    public function getRepairCode()
    {
        return $this->repairCode;
    }
}
