<?php

namespace App\Services\Master;

use Auth;
use Illuminate\Http\Request;
use App\Model\Master\BatchItem;
use App\Model\Master\Inventory;
use App\Model\Master\MsProduct;
use App\Model\Master\ItemDetail;
use Yajra\DataTables\DataTables;
use App\Model\Master\MsWarehouse;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use App\Http\Controllers\AdminController;
use App\Model\Master\InventoryMutationLog;
use App\Model\Master\MutationJoinHistories;
use App\Model\Master\InventoryMutationHistories;


class InventoryService extends AdminController
{
    public $inventory;
    /**
     * get list query
     *
     * @return App\Model\Master\Inventory;
     */
    public function list()
    {
        return Inventory::with([
            'unit_type',
            'warehouse',
            'batch_item.batch',
            'batch_item.product',
            'batch_item.productVendor.vendor',
            'batch_item.varian.productVarianAttributes',
            'batch_item.status',
        ]);
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\Inventory
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function searchDatatables(Request $request)
    {
        $query = $this->list();

        if (!empty($request->warehouse_id)) {
            $query->whereIn('ms_warehouse_id', explode(',', $request->warehouse_id));
        }

        if (!empty($request->product_id)) {
            $query->whereIn('product_id', $this->comaToArray($request->product_id));
        }

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\Inventory
     */
    public function select2(Request $request)
    {
        return Inventory::where('shipping', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    public function approvalModalTemplate($id)
    {
        $query = $this->list();

        $inventory = $query->where('id', $id)->firstOrFail();

        return view('admin.master.inventory._approvalModalTemplate', [
            'inventory' => $inventory
        ])->render();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\Inventory
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return Inventory::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function createMany(array $data)
    {
        $data_inventory = [];
        $cogs_value  = $data['cogs_value'];
        $batch_item_id = [];
        $warehouse_name = MsWarehouse::where('id',$data['ms_warehouse_id'])->value('name');
        foreach ($cogs_value as $key => $val) {
            $ms_batch_item_id = $data['ms_batch_item_id'][$key];
            $qty_inventory = $data['qty_inventory'][$key];
            $batchItem = BatchItem::with(['batch'])->where('id', $ms_batch_item_id)->first();
            if ($qty_inventory <= $batchItem->quantity) {
                $batch_item_id[] = $ms_batch_item_id;
                $data_inventory[] = [
                    'batch_code'        => $batchItem->batch->batch_no,
                    'item_name'         => $batchItem->product_name,
                    'ms_warehouse_id'   => $data['ms_warehouse_id'],
                    'warehouse_name'    => $warehouse_name,
                    'cogs_value'        => $val,
                    'stock'             => $qty_inventory,
                    'stock_out'         => 0,
                    'stock_ret'         => 0,
                    'stock_now'         => $qty_inventory,
                    'ms_batch_item_id'  => $ms_batch_item_id,
                    'product_id'        => $batchItem->product_id,
                    'product_vendor_id' => $batchItem->product_vendor_id,
                    'product_varian_id' => $batchItem->product_varian_id,
                    'ms_unit_type_id'   => $data['ms_unit_type_id'][$key],
                ];
            }
        }
        DB::beginTransaction();
        try {
            if (count($data_inventory) > 0) {
                // update batch
                BatchItem::whereIn('id', $batch_item_id)->update([
                    'is_inventory' => 1,
                ]);

                $inventory = Inventory::insert($data_inventory);
            }
            DB::commit();
        } catch (QueryException $e) {
            // rollback db
            DB::rollback();
            throw new \Exception($e->getMessage());
        }

        return $inventory;
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        $data_inventory = [
            'cogs_value'       => $data['cogs_value'],
            'stock_temp'       => $data['qty_inventory'],
            'ms_warehouse_id'  => $data['ms_warehouse_id'],
            'ms_batch_item_id' => $data['ms_batch_item_id'],
            'ms_unit_type_id'  => $data['ms_unit_type_id'],
            'is_need_approval' => 1,
            'user_approve_id'  => null,
        ];

        try {
            return $this->getInventory()->update($data_inventory);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function approve($id)
    {
        $inventory = Inventory::where('id', $id)->firstOrFail();
        $this->setInventory($inventory);

        $data_inventory = [
            'stock_temp'       => null,
            'stock'            => $inventory->stock_temp,
            'is_need_approval' => 0,
            'user_approve_id'  => Auth::id(),
        ];

        try {
            return $this->getInventory()->update($data_inventory);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        $inventory = $this->getInventory();

        DB::beginTransaction();
        try {
            BatchItem::where('id', $inventory->ms_batch_item_id)->update([
                'is_inventory' => 0,
            ]);
            ItemDetail::where('inventory_id', $inventory->id)->update([
                'inventory_id' => null,
            ]);
            $inventory->delete();
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new \Exception($e->getMessage());
        }

        return true;
    }



    // public function deleteById(array $ids)
    // {
    //     try {
    //         return Marital::whereIn('id', $ids)->delete();
    //     } catch (QueryException $e) {
    //         throw new \Exception($e->getMessage());
    //     }
    // }

    public function generateData(array $data)
    {
        return [
            'cogs_value'       => $data['cogs_value'],
            'ms_warehouse_id'  => $data['ms_warehouse_id'],
            'warehouse_name'   => $data['warehouse_name'],
            'ms_batch_item_id' => $data['ms_batch_item_id'],
            'ms_unit_type_id'  => $data['ms_unit_type_id'],
        ];
    }

    public function setInventory(Inventory $inventory)
    {
        $this->inventory = $inventory;
        return $this;
    }

    public function getInventory()
    {
        return $this->inventory;
    }

    public function createMutation(array $data)
    {
        $inventory = Inventory::where('id', $data['id'])->where('ms_batch_item_id', $data['ms_batch_item_id'])->first();
        $by_warehouse = Inventory::where('ms_batch_item_id', $data['ms_batch_item_id'])->where('ms_warehouse_id', $data['ms_warehouse_id']);
        $warehouse_name = MsWarehouse::where('id', $data['ms_warehouse_id'])->value('name');

        $data_mutation = [
            'item_name'         => MsProduct::getNameProduct($inventory['product_id'], $inventory['product_varian_id'], $inventory['product_vendor_id']),
            'product_id'        => $inventory['product_id'],
            'product_varian_id' => $inventory['product_varian_id'],
            'product_vendor_id' => $inventory['product_vendor_id'],
            'ms_batch_item_id'  => $data['ms_batch_item_id'],
            'ms_warehouse_id'   => $data['ms_warehouse_id'],
            'warehouse_name'    => $warehouse_name,
            'batch_code'        => $data['this_batch_code'],
            'cogs_value'        => $inventory['cogs_value'],
            'stock'             => $data['stock'],
            'stock_now'         => $data['stock'],
            'ms_unit_type_id'   => $inventory['ms_unit_type_id'],
        ];
        DB::beginTransaction();
        try {
            if ($by_warehouse->count() > 0) {
                $by_warehouse = $by_warehouse->first();
                $updatestock = Inventory::where('ms_batch_item_id', $data['ms_batch_item_id'])->where('ms_warehouse_id', $data['ms_warehouse_id']);

                $this->mutationLogsProcess($data['id'], $updatestock->value('id'), $data['stock']);
                $update = $updatestock->update(['stock_now' => ($by_warehouse['stock_now'] + $data['stock'])]);
                // return $this->mutationLogs($inventory['id'], $updatestock->first(), $eliminate_stock, $data['stock']);
            } else {
                $create_mutation = Inventory::create($data_mutation);
                $this->mutationLogsProcess($data['id'], $create_mutation->id, $data['stock']);
                // return $this->mutationLogs($inventory['id'], $create_mutation, $eliminate_stock, $data['stock']);
            }
            $eliminate_stock = $inventory['stock_now'] - $data['stock'];
            $update_stock = Inventory::where('id', $data['id'])->update(['stock_now' => $eliminate_stock]);
            DB::commit();
            return $update_stock;

        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());

        }
    }

    // public function mutationLogs($in, $data, int $eliminate_stock, $data_stock)
    // {
    //     $data = [
    //         'ms_batch_item_id' => $data['ms_batch_item_id'],
    //         'inventories_id_in' => $in,
    //         'inventories_id_out' => $data['id'],
    //         'stock_out' => $data_stock,
    //         'stock_available_out' => $eliminate_stock,
    //         'stock_available_in' => $data['stock'],
    //     ];
    //     return InventoryMutationLog::create($data);
    // }


    public function mutationLogsProcess($inventory_id_out=null, $inventory_id_in=null, $amount, $order_id=null, $technician_id=null, $is_cancel_order=null) {

        $q  = null;
        $ids = null;
        $history_id = [];
        // typemutation => 1=between_inventory; 0 = order ;
        $type_mutation = ($order_id === null ? 1 : 0);
        // mutation antar inventory detected ;
        if($inventory_id_in !== null) {
            $ids = array($inventory_id_out,$inventory_id_in);
        }
        // mutation order detected ;
        if($order_id !== null) {
            $ids = array($inventory_id_out);
        }

        // mutation cancel order detected ;
        if($is_cancel_order !== null) {
            $ids = array($inventory_id_in);
        }

        // proses create history mutation = ada yang berkurang /& ada yang bertambah ;
        foreach($ids as $key => $id) {
            // inventory_out=minus; inventory_in=plus ;
            // $type_amount = ($key == 0 ? 'minus' : 'plus');
            // jika cancel order type_amount 1 ;
            $type_amount = ($key == 0 ? 0 : 1);
            if($is_cancel_order !== null) {
                $type_amount = 1;
            }
            $q = Inventory::where('id',$id)->first();
            $data = [
                'id'              => $q->id,
                'warehouse_name'  => $q->warehouse_name,
                'item_name'       => $q->item_name,
                'stock_available' => $q->stock_now,
                'amount'          => $amount,
                'type_mutation'   => $type_mutation,
                'order_id'        => $order_id,
                'technician_id'   => $technician_id,
            ];
            //simpan Inventory Mutation Histories ;
            $created_mutation_histories = $this->createMutationHistories($data,$type_amount);
            // [0]=out [1]=in ;
            $history_id[] = $created_mutation_histories->id;
        }
        $mutation_join_histories = [
            // jika is_cancel_order tidak null maka history out kosong alias null ;
            'history_out'   => ($is_cancel_order !== null ? null : $history_id[0]),
            // jika history_in tidak null maka mutasi antar inventory warehouse detected ;
            // jika is_cancel_order tidak null maka history_id[0] type menjadi history in ;
            'history_in'    => ($is_cancel_order !== null ? $history_id[0] : ((array_key_exists(1,$history_id)) ? $history_id[1]: null)),
            'type_mutation' => $type_mutation
        ];
        $mutation_join = MutationJoinHistories::create($mutation_join_histories);
        // update mutation_join_id ;
        foreach($history_id as $key=> $history) {
            InventoryMutationHistories::where('id', $history)->update(['mutation_join_id' => $mutation_join->id]);
        }

    }

    // $data inventory ;
    // $type_amount == 0=minus; 1=plus ;
    public function createMutationHistories(array $data, $type_amount) {
        $stock_before = $data['stock_available'];
        // check apakah data histories mutation dengan type_amount = plus belum pernah terecord, maka stock before dipastikan 0 ;
        if($type_amount == 1 && $data['type_mutation'] == 1) {
            $new_inventory_mutation = InventoryMutationHistories::where('inventory_id', $data['id'])->where('type_amount', 1)->where('type_mutation', 1)->count();
            if($new_inventory_mutation == 0) {
                $stock_before = 0;
            }
        }
        // pastikan stock before 0 jika hasil mutasi inventory baru ter create ;
        $stock_now = ($stock_before == 0 ? 0 : $data['stock_available']);
        $stock_after = ($type_amount === 1 ? ($stock_now + $data['amount']) : ($stock_now - $data['amount']));
        $data = [
            'inventory_id'   => $data['id'],
            'warehouse_name' => $data['warehouse_name'],
            'item_name'      => $data['item_name'],
            'amount'         => $data['amount'],
            'type_amount'    => $type_amount,
            'type_mutation'  => $data['type_mutation'], // 1=between_inventory; 2=order ;
            // check apakah data histories mutation dengan type_amount = plus belum pernah terecord, maka stock before dipastikan 0 ;
            'stock_before'   => $stock_before,
            'stock_after'    => $stock_after,
            'order_id'       => $data['order_id'],
            'technician_id'  => $data['technician_id'],
        ];
        return InventoryMutationHistories::create($data);
    }

    public function deleteMutationHistories($order_id, $technician_id, $inventory_id) {
        $data = [];
        $deleteMutationHistories = InventoryMutationHistories::where('inventory_id', $inventory_id)
        ->where('order_id', $order_id)
        ->where('technician_id', $technician_id);

        $data['deleteMutationJoinHistories'] = MutationJoinHistories::where('id', $deleteMutationHistories->value('mutation_join_id'))->delete();
        $data['deleteMutationHistories'] = $deleteMutationHistories->delete();
        return $data;
    }

}
