<?php

namespace App\Services\Master;

use App\Helpers\ImageUpload;
use Illuminate\Http\Request;
use App\Model\Master\MsSymptom;
use Yajra\DataTables\DataTables;
use App\Model\Master\MsServicesType;
use Illuminate\Database\QueryException;

class SymptomService
{
    /**
     * dipakai saat delete data
     * akan mengecek ke semua relasi
     * pada model ini
     * untuk mencari jika model ini sudah di pakai di model lain
     * note: lumayan berat karna harus cek ke semua table
     */
    public $use_relation_check = true;

    public $symptom;
    /**
     * get list query
     *
     * @return App\Model\Master\MsSymptom;
     */
    public function list()
    {
        return MsSymptom::query();
    }

    public function select2(Request $request, $ms_services_id)
    {
        if ($ms_services_id != null) {
            return MsSymptom::where('name', 'like', '%' . $request->q . '%')
                ->where('ms_services_id', $ms_services_id)
                ->whereHas('service_types')
                ->limit(20)
                ->get();
        }
        return MsSymptom::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }


    public function datatables()
    {
        $query = $this->list()->with(['services']);

        return DataTables::of($query)->addIndexColumn()->toJson();
    }

    public function create($data)
    {
        try {

            return MsSymptom::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function update(array $data)
    {
        try {

            return $this->getSymptoms()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }


    public function delete($id)
    {
        try {
            return $this->relasiCheck()->delete();
            // $name = MsSymptom::where('id', $id)->value('name');
            // $rand = rand(999999, 000000);
            // $update_name = MsSymptom::where('id', $id)->update(['name' => 'deleted-' . $name . '-' . $rand]);
            // return $this->relasiCheck()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function setSymptoms(MsSymptom $symptom)
    {
        $this->symptom = $symptom;
        return $this;
    }

    public function generateData(array $data)
    {
        return [
            'ms_services_id' => $data['ms_services_id'],
            'name' => $data['name'],
            'images' => $data['images']
        ];
    }

    public function getSymptoms()
    {
        return $this->symptom;
    }

    /**
     * check relasi saat delete data
     */
    public function relasiCheck()
    {
        $row = $this->getSymptoms();

        if ($this->use_relation_check) {
            // cek ke produk
            $servicesType = MsServicesType::where('ms_symptoms_id', $row->id)->first();
            if ($servicesType != null) {
                throw new \Exception('already used in service type module, service type name (' . $servicesType->name . ')');
            }
        }

        return $row;
    }
}
