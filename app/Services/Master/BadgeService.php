<?php

namespace App\Services\Master;

use App\User;
use App\Model\Master\Badge;
use Illuminate\Database\QueryException;

class BadgeService
{
    public $Badge;

    /**
     * dipakai saat delete data
     * akan mengecek ke semua relasi
     * pada model ini
     * untuk mencari jika model ini sudah di pakai di model lain
     * note: lumayan berat karna harus cek ke semua table
     */
    public $use_relation_check = true;

    /**
     * get list query
     *
     * @return App\Model\Badge
     */
    public function list()
    {
        return Badge::query();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Badge
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {

            $insert = Badge::create($this->generateData($data));
            foreach($data['user_emails'] as $key=> $val) {
                User::where('id', $val)->update(['badge_id' => $insert->id]);
            }
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            User::where('badge_id', $data['id'])->update(['badge_id' => null]);
            foreach($data['user_emails'] as $key=> $val) {
                User::where('id', $val)->update(['badge_id' => $data['id']]);
            }
            return $this->getBadge()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {

        return $this->relasiCheck()->delete();
    }

    /**
     * delete data
     *
     * @param array ids
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function deleteById(array $ids)
    {
        try {
            return Badge::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
            'color' => $data['color'],

        ];
    }

    /**
     * check relasi saat delete data
     */
    public function relasiCheck()
    {
        $row = $this->getBadge();

        if ($this->use_relation_check) {
            // cek ke produk
            $badge = User::where('badge_id', $row->id)->first();
            if ($badge != null) {
                throw new \Exception('already used this badge');
            }
        }

        return $row;
    }

    public function setBadge(Badge $Badge)
    {
        $this->Badge = $Badge;
        return $this;
    }

    public function getBadge()
    {
        return $this->Badge;
    }
}
