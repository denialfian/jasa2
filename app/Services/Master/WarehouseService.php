<?php

namespace App\Services\Master;

use App\Model\Master\MsCity;
use App\Model\Master\MsVillage;
use App\Model\Master\MsZipcode;
use App\Model\Master\MsDistrict;
use App\Model\Master\MsWarehouse;
use App\Exceptions\HasChildException;
use Illuminate\Database\QueryException;

class WarehouseService
{
    public $warehouse;

    /**
     * get list query
     *
     * @return App\Model\Master\MsWarehouse;
     */
    public function list()
    {
        return MsWarehouse::get();
    }
    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\MsWarehouse
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        $this->checkNameWarehouse($data['name']);

        $data = [
            'name' => $data['name'],
            'address' => $data['address'],
        ];

        try {
            return MsWarehouse::create($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function checkNameWarehouse($name) {
        $find = MsWarehouse::where('name', $name)->count();
        if ($find) {
            throw new \Exception('Nama Warehouse sudah pernah dipakai !, Silahkan ganti nama lain');
        }
        return true;
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {

        $data = [
            'name' => $data['name'],
            'address' => $data['address'],
        ];

        $warehouse = $this->getWarehouse();

        try {
            return $warehouse->update($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        $warehouse = $this->getWarehouse();

        if ($warehouse->inventory->count() > 0) {
            throw new HasChildException('tidak bisa delete sudah terpakai di Inventory pada Warehouse ini');
        }

        try {
            return $warehouse->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function setWarehouse(MsWarehouse $warehouse)
    {
        $this->warehouse = $warehouse;
        return $this;
    }

    public function getWarehouse()
    {
        return $this->warehouse;
    }
}
