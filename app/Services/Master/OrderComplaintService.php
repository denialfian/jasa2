<?php

namespace App\Services\Master;

use App\Model\Master\MsOrderComplaint;
use App\Model\Master\MsTicketDetail;
use Auth;
use Yajra\DataTables\DataTables;

class OrderComplaintService
{
    public $ticketOrderComplaint;

    /**
     * get list query
     *
     * @return App\Model\Master\MsOrderComplaint;
     */
    // function list() {
    //     return MsOrderComplaint::with('user');
    // }

    /**
     * datatables data
     *
     * @return \App\Model\Master\MsOrderComplaint
     */

    public function reply($request)
    {
        $file_name = null;
        if ($request->attachment != null) {
            $file      = $request->file('attachment');
            $file_name = rand() . $file->getClientOriginalName();
            $file->storeAs('public/orderComplaint/', $file_name);
        }

        if(Auth::user()->hasRole('admin')){
            $reply_complaint = MsOrderComplaint::create([
                'message' => $request->get('message'),
                'ms_orders_id' => $request->get('ms_orders_id'),
                'user_id'      => Auth::id(),
                'attachment'   => $file_name,
                'created_by'   => 'Admin',
            ]);
            return view('admin.customer.complaint._replay', [
                'reply_complaint' => $reply_complaint,
            ])->render();
        }

        if(Auth::user()->hasRole('Customer')){
            $reply_complaint = MsOrderComplaint::create([
                'message' => $request->get('message'),
                'ms_orders_id' => $request->get('ms_orders_id'),
                'user_id'      => Auth::id(),
                'attachment'   => $file_name,
                'created_by'   => 'Customer',
            ]);
            return view('admin.customer.complaint._replay', [
                'reply_complaint' => $reply_complaint,
            ])->render();
        }

        if(Auth::user()->hasRole('Technician')){
            $reply_complaint = MsOrderComplaint::create([
                'message' => $request->get('message'),
                'ms_orders_id' => $request->get('ms_orders_id'),
                'user_id'      => Auth::id(),
                'attachment'   => $file_name,
                'created_by'   => 'Technician',
            ]);
            return view('admin.customer.complaint._replay', [
                'reply_complaint' => $reply_complaint,
            ])->render();
        }
    }

    public function create(array $data)
    {
        return Ticket::create([
            'status'    => 1,
            'ticket_no' => Ticket::genetateTicketNo(),
            'subject'   => $data['subject'],
            'desc'      => $data['desc'],
            'users_id'  => $data['user_id'],
        ]);
    }

    public function createAsCustomer(array $data)
    {
        $file_name = null;
        if ($data['attachment'] != null) {
            $file      = $data->file('attachment');
            $file_name = rand() . $file->getClientOriginalName();
            $file->storeAs('public/orderComplaint/', $file_name);
        }

        $orderComplaint = MsOrderComplaint::create([
            'message'   => $data['message'],
            'ms_orders_id'  => $data['ms_orders_id'],
            'user_id'      => Auth::id(),
            'attachment'   => $file_name,
        ]);

        return $orderComplaint;
    }

    public function setTicket(Ticket $ticketOrderComplaint)
    {
        $this->ticket = $ticketOrderComplaint;
        return $this;
    }

    public function getTicket()
    {
        return $this->ticket;
    }

    // public function update(array $data)
    // {
    //     $ticketOrderComplaint = $this->getTicket();

    //     return $ticketOrderComplaint->update([
    //         'subject'  => $data['subject'],
    //         'desc'     => $data['desc'],
    //         'users_id' => $data['user_id'],
    //     ]);
    // }

    // public function updateStatus(array $data)
    // {
    //     $ticketOrderComplaint = $this->getTicket();

    //     $ticketOrderComplaint->update([
    //         'status' => $data['status'],
    //     ]);

    //     return $this->getStatusHtml($data['status']);
    // }

    // public function delete($id = null)
    // {
    //     if ($id == null) {
    //         $ticketOrderComplaint = $this->getTicket();
    //     } else {
    //         $ticketOrderComplaint = Ticket::where('id', $id)->firstOrFail();
    //     }

    //     return $ticketOrderComplaint->delete();
    // }

    // public function getStatusHtml($status)
    // {
    //     switch ($status) {
    //         case '1':
    //             return '<span class="btn btn-sm btn-primary">open</span>';
    //             break;

    //         case '2':
    //             return '<span class="btn btn-sm btn-info">on progress</span>';
    //             break;

    //         case '3':
    //             return '<span class="btn btn-sm btn-dark">on resolving</span>';
    //             break;

    //         case '4':
    //             return '<span class="btn btn-sm btn-success">done</span> ';
    //             break;

    //         case '5':
    //             return '<span class="btn btn-sm btn-danger">closed</span> ';
    //             break;

    //         default:
    //             return $status;
    //             break;
    //     }
    // }

    
}
