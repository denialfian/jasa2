<?php

namespace App\Services\Master;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Model\Master\OrdersStatus;
use Illuminate\Support\Facades\DB;
use App\Exceptions\HasChildException;
use Illuminate\Database\QueryException;

class OrderStatusService
{
    public $status;

    /**
     * get list query
     *
     * @return \App\Model\Master\OrdersStatus;
     */
    public function list()
    {
        return OrdersStatus::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\OrdersStatus
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\OrdersStatus
     */
    public function select2(Request $request)
    {
        $id = [2,3,4,5,7,9,10,11];
        return OrdersStatus::whereIn('id', $id)->orderByRaw("FIELD(id, 2, 3, 4, 9, 7, 10, 11, 5)")->where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * find data
     *
     * @param int $id
     *
     * @return \App\Model\Master\OrdersStatus
     */
    public function find(int $id)
    {
        return OrdersStatus::where('id', $id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\OrdersStatus
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return OrdersStatus::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getStatus()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {

        $status = $this->getStatus();

        if ($status->order->count() > 0) {
            throw new HasChildException('tidak bisa delete sudah terpakai di Order pada Status ini');
        }
        try {
            return $this->getStatus()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function deleteById(array $ids)
    {
        try {
            return OrdersStatus::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * generate data
     *
     * @return array
     */
    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
        ];
    }

    public function setStatus(OrdersStatus $status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }
}
