<?php

namespace App\Services\Master;

use App\Model\Master\EmailStatusOrder;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class EmailStatusOrderService
{
    public $email;
    /**
     * get list query
     *
     * @return App\Model\Master\EmailStatusOrder;
     */
    public function list()
    {
        return EmailStatusOrder::with('order_status')->groupBy('orders_statuses_id');
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\EmailStatusOrder
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\EmailStatusOrder
     */

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\EmailStatusOrder
     *
     * @throws \Exception
     */
    // public function create(array $data)
    // {
    //     try {
    //         return EmailStatusOrder::create($this->generateData($data));
    //     } catch (QueryException $e) {
    //         throw new \Exception($e->getMessage());
    //     }
    // }




    public function update(array $data, $id)
    {

        $email_status_order = $this->getEmailStatusOrder();

        try {
            return $email_status_order->where('orders_statuses_id',$id)->update([$data]);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
   

  
}
