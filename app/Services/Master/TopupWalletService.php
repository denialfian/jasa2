<?php

namespace App\Services\Master;

use Illuminate\Http\Request;
use App\Model\Master\TopupWallet;
use App\Model\Master\Ewallet;
use Yajra\DataTables\DataTables;
use App\Exceptions\HasChildException;
use App\Model\Master\EwalletHistory;
use Illuminate\Database\QueryException;
use Auth;
use DB;
use Carbon\Carbon;

class TopupWalletService
{
    public $topupWallet;

    /**
     * get list query
     *
     * @return App\Model\Master\TopupWallet;
     */
    public function list()
    {
        return Ewallet::query();
    }

    /**
     * datatables data
     *
      * @return \App\Model\Master\TopupWallet
     */
    public function datatables()
    {
        $query = EwalletHistory::with(['bank', 'voucer'])->where('user_id', Auth::user()->id)->where('type_transaction_id', 0)->orderBy('id', 'desc');
        return DataTables::of($query)
                ->addIndexColumn()
                ->toJson();
    }

    public function datatablesAdminListWallet()
    {
        
            
        $query = Ewallet::with('user');
        return DataTables::of($query)
                ->addIndexColumn()
                ->toJson();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\TopupWallet
     *
     * @throws \Exception
     */

    public function adminCreate(array $data)
    {
        try{
            return TopupWallet::create($this->generateDataAdmin($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try{
            return $this->getTopupWallet()->update($this->generateData($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */

    public function delete()
    {
        try {
            return  $this->getTopupWallet()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

  

    public function generateDataAdmin(array $data)
    {
        return [
            'nominal' => $data['nominal'],
            'note' => $data['note'],
            'status' => 0,
            'user_id' => $data['user_id'],
        ];
    }

    public function setTopupWallet(TopupWallet $topupWallet)
    {
        $this->topupWallet = $topupWallet;
        return $this;
    }

    public function getTopupWallet()
    {
        return $this->topupWallet;
    }
}
