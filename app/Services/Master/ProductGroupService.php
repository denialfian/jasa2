<?php

namespace App\Services\Master;

use App\Model\Master\ProductGroup;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProductGroupService
{
    public $product;

    /**
     * get list query
     */
    public function list()
    {
        return ProductGroup::with('service_type');
    }

    /**
     * datatables data
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     */
    public function select2(Request $request, $type_id = null)
    {
        if ($type_id != null) {
            return ProductGroup::where('name', 'like', '%' . $request->q . '%')
                ->where('ms_services_type_id', $type_id)
                ->limit(20)
                ->get();
        }

        return ProductGroup::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * find data
     */
    public function find(int $id)
    {
        return ProductGroup::where('id', $id)->first();
    }

    /**
     * create data
     */
    public function create(array $data)
    {
        try {
            return ProductGroup::create($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     */
    public function update(array $data)
    {
        try {
            return $this->getProduct()->update($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     */
    public function delete()
    {
        return $this->getProduct()->delete();
    }

    /**
     * delete multiple data
     */
    public function deleteById(array $ids)
    {
        try {
            return ProductGroup::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
            'ms_services_type_id' => $data['ms_services_type_id'],
            'images' =>  $data['images'],
        ];
    }

    public function setProduct(ProductGroup $product)
    {
        $this->product = $product;
        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }
}
