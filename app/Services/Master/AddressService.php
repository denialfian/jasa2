<?php

namespace App\Services\Master;

use App\Model\Master\MsAddress;
use App\Model\Master\MsProvince;
use Illuminate\Database\QueryException;
use Auth;
use App\Model\Master\MsAdditionalInfo;
use App\Helpers\ImageUpload;

class AddressService
{
    public $address;

    /**
     * get list query
     *
     * @return App\Model\Master\MsAddress;
     */
    public function list()
    {
        $userlogin = Auth::user()->id;
        return MsAddress::where('user_id', $userlogin)->with('user', 'city', 'district', 'village', 'zipcode', 'types');
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\MsAddress
     *
     * @throws \Exception
     */
    public function create(array $data)
    {

        try {
            return MsAddress::create($this->generatePlayload($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        $address = $this->getAddress();

        $data = $this->generatePlayload($data, [
            'is_main' => $address->is_main
        ]);

        try {
            return $address->update($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * set main address
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function setMain()
    {
        $address = $this->getAddress();

        try {
            if ($address->is_main == 0) {
                // set semua main jadi 0
                MsAddress::where('user_id', $address->user_id)->update([
                    'is_main' => 0
                ]);
                // set main
                return $address->update([
                    'is_main' => 1
                ]);
            }

            return true;
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return $this->getAddress()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generatePlayload(array $data, array $replace = [])
    {
        $user =  auth()->user();
        $isMain = MsAddress::where('user_id', $user->id)
            ->first();

        if(Auth::user()->hasRole('Technician')){
            $data = [
                'is_main' => $isMain == null ? 1 : 0,
                'is_teknisi' => 1,
                'address' => $data['address'],
                'user_id' => $data['user_id'],
                'ms_city_id' => $data['ms_city_id'],
                // 'ms_district_id' => $data['ms_district_id'],
                // 'ms_village_id' => $data['ms_village_id'],
                // 'ms_zipcode_id' => $data['ms_zipcode_id'],
                'ms_address_type_id' => $data['ms_address_type_id'],
                'phone1' => $data['phone1'] ?? null,
                'phone2' => $data['phone2'] ?? null,
                'latitude' => $data['lat'] ?? null,
                'longitude' => $data['long'] ?? null,
                'location_google' => $data['search_map'] ?? null,
                'cities' => $data['cities'] ?? null,
                'province' => $data['province'] ?? null,

            ];
        }

        if(Auth::user()->hasRole('Customer') && Auth::user()->hasRole('admin')){
            $data = [
                'is_main' => $isMain == null ? 1 : 0,
                'address' => $data['address'],
                'user_id' => $data['user_id'],
                'ms_city_id' => $data['ms_city_id'],
                // 'ms_district_id' => $data['ms_district_id'],
                // 'ms_village_id' => $data['ms_village_id'],
                // 'ms_zipcode_id' => $data['ms_zipcode_id'],
                'ms_address_type_id' => $data['ms_address_type_id'],
                'phone1' => $data['phone1'] ?? null,
                'phone2' => $data['phone2'] ?? null,
                'latitude' => $data['lat'] ?? null,
                'longitude' => $data['long'] ?? null,
                'location_google' => $data['search_map'] ?? null,
                'cities' => $data['cities'] ?? null,
                'province' => $data['province'] ?? null,
            ];
        }

        return array_merge($data, $replace);
    }

    public function setAddress(MsAddress $address)
    {
        $this->address = $address;
        return $this;
    }

    public function getAddress()
    {
        return $this->address;
    }
}
