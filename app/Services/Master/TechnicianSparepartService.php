<?php

namespace App\Services\Master;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Model\Technician\Technician;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use App\Model\Master\TechnicianSparepart;

class TechnicianSparepartService
{
    public $Services;
    /**
     * get list query
     *
     * @return App\Model\Master\TechnicianSparepart;
     */
    public function list()
    {
        $technician_id = Technician::where('user_id', Auth::user()->id)->value('id');
        return TechnicianSparepart::with(['service','technician'])
                    ->whereHas('service', function ($q) {
                        $q->whereNull('deleted_at');
                    })->whereHas('technician', function ($q) {
                        $q->whereNull('deleted_at');
                    })->where('technicians_id', $technician_id);
    }
    /**
     * datatables data
     *
     * @return \App\Model\Master\TechnicianSparepart
     */
    public function datatables()
    {
        $query = $this->list();
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\TechnicianSparepart
     */
    public function select2($user_id, Request $request)
    {
        if($user_id != null) {
            $user_id = $user_id;
        } else {
            $user_id = Auth::user()->id;
        }

        $technician_id = Technician::where('user_id', $user_id)->value('id');
        return TechnicianSparepart::where('name', 'like', '%' . $request->q . '%')
            ->where('technicians_id', $technician_id)
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\TechnicianSparepart
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return TechnicianSparepart::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getServices()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete($id)
    {
        try {
            $name = TechnicianSparepart::where('id', $id)->value('name');
            $rand = rand(999999,000000);
            $update_name = TechnicianSparepart::where('id', $id)->update(['name' => 'deleted-'.$name.'-'.$rand]);
            return  $this->getServices()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            $name = TechnicianSparepart::where('id', $id)->value('name');
            $rand = rand(999999,000000);
            $update_name = TechnicianSparepart::where('id', $id)->update(['name' => 'deleted-'.$name.'-'.$rand]);
            return TechnicianSparepart::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {

        $generate = [
            'ms_services_id' => $data['ms_services_id'],
            'technicians_id' => $data['technicians_id'],
            'name'           => $data['name'],
            'price'          => $data['price'],
            'description'    => $data['description'],
        ];
        if (isset($data['images'])) {
            $generate['images'] = $data['images'];
        }
        return $generate;
    }

    public function setServices(TechnicianSparepart $Services)
    {
        $this->Services = $Services;
        return $this;
    }

    public function getServices()
    {
        return $this->Services;
    }
}
