<?php

namespace App\Services\Master;

use App\Model\Master\CurriculumSequence;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class CurriculumSequenceService
{
    public $curSequence;
    /**
     * get list query
     *
     * @return App\Model\Master\CurriculumSequence;
     */
    public function list()
    {
        return CurriculumSequence::with(['developmentprog', 'curriculum']);
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\CurriculumSequence
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\CurriculumSequence
     */
    public function select2(Request $request)
    {
        return CurriculumSequence::where('sequence_name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\CurriculumSequence
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return CurriculumSequence::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getCurSequence()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getCurSequence()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'sequence_name' => $data['sequence_name'],
            'dev_program_id' => $data['dev_program_id'],
            'curriculum_id' => $data['curriculum_id'],
        ];
    }

    public function setCurSequence(CurriculumSequence $curSequence)
    {
        $this->curSequence = $curSequence;
        return $this;
    }

    public function getCurSequence()
    {
        return $this->curSequence;
    }
}
