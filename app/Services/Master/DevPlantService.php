<?php

namespace App\Services\Master;

use App\Model\Master\DevPlant;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class DevPlantService
{
    public $devPlant;
    /**
     * get list query
     *
     * @return App\Model\Master\DevPlant;
     */
    public function list()
    {
        return DevPlant::with(['user', 'curriculum']);
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\DevPlant
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\DevPlant
     */
    public function select2(Request $request)
    {
        return DevPlant::where('status', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\DevPlant
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return DevPlant::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getDevPlant()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getDevPlant()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'user_id' => $data['user_id'],
            'curr_sequence_id' => $data['curr_sequence_id'],
            'status' => $data['status'],
            'date_time' => $data['date_time'],
        ];
    }

    public function setDevPlant(DevPlant $devPlant)
    {
        $this->devPlant = $devPlant;
        return $this;
    }

    public function getDevPlant()
    {
        return $this->devPlant;
    }
}
