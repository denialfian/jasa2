<?php

namespace App\Services\Master;

use Illuminate\Http\Request;
use App\Model\Master\Services;
use Yajra\DataTables\DataTables;
use Illuminate\Database\QueryException;
use App\Model\Master\ASCExcel;

class ASCExcelService
{
    public $asc_excel;

    /**
     * get list query
     *
     * @return App\Model\Master\ASCExcel;
     */
    public function list()
    {
        return ASCExcel::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Product\ASCExcel
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Product\ASCExcel
     */
    public function select2(Request $request)
    {
        return ASCExcel::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\ASCExcel
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return ASCExcel::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getASCExcel()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getASCExcel()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return ASCExcel::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
            'address' => $data['address'],
        ];
    }

    public function setASCExcel(ASCExcel $asc_excel)
    {
        $this->asc_excel = $asc_excel;
        return $this;
    }

    public function getASCExcel()
    {
        return $this->asc_excel;
    }
}
