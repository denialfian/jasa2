<?php

namespace App\Services\Master;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Database\QueryException;
use App\Model\Master\JasaExcel;

class JasaExcelService
{
    public $jasa_excel;

    /**
     * get list query
     *
     * @return App\Model\Master\JasaExcel;
     */
    public function list()
    {
        return JasaExcel::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Product\JasaExcel
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Product\JasaExcel
     */
    public function select2(Request $request)
    {
        return JasaExcel::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\JasaExcel
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return JasaExcel::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getJasaExcel()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getJasaExcel()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return JasaExcel::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
            'price' => $data['price'],
            'desc' => $data['desc'],
        ];
    }

    public function setJasaExcel(JasaExcel $jasa_excel)
    {
        $this->jasa_excel = $jasa_excel;
        return $this;
    }

    public function getJasaExcel()
    {
        return $this->jasa_excel;
    }
}
