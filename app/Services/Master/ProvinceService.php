<?php

namespace App\Services\Master;

use App\Exceptions\HasChildException;
use App\Model\Master\MsCountry;
use App\Model\Master\MsProvince;
use Illuminate\Database\QueryException;

class ProvinceService
{
    public $province;

    /**
     * get list query
     *
     * @return App\Model\Master\MsProvince;
     */
    public function list()
    {
        return MsProvince::with('country');
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\MsProvince
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        $this->checkCountry($data);

        $data = [
            'name' => $data['name'],
            'iso_code' => isset($data['iso_code']) ?? null,
            'ms_country_id' => $data['ms_country_id']
        ];

        try {
            return MsProvince::create($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        $this->checkCountry($data);

        $data = [
            'name' => $data['name'],
            'iso_code' => isset($data['iso_code']) ?? null,
            'ms_country_id' => $data['ms_country_id']
        ];

        $province = $this->getProvince();

        try {
            return $province->update($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        $province = $this->getProvince();

        if ($province->cities->count() > 0) {
            throw new HasChildException('tidak bisa delete terdapat kota pada provinsi ini');
        }

        try {
            return $province->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\MsProvince
     *
     * @throws \Exception
     */
    public function multipleDelete(array $ids)
    {
        try {
            return MsProvince::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception('Terdapat Beberapa Kota Pada Province Ini');
        }
    }

    public function checkCountry(array $data)
    {
        $country = MsCountry::where('id', $data['ms_country_id'])->firstOrFail();

        if ($country == null) {
            throw new \Exception('county yg dipilih tidak ditemukan');
        }

        return $country;
    }

    public function setProvince(MsProvince $province)
    {
        $this->province = $province;
        return $this;
    }

    public function getProvince()
    {
        return $this->province;
    }
}
