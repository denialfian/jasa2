<?php

namespace App\Services\Master;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Model\Master\BatchStatus;
use App\Exceptions\HasChildException;
use Illuminate\Database\QueryException;

class BatchStatusService
{
    public $status;

    /**
     * get list query
     *
     * @return \App\Model\Master\BatchStatus;
     */
    public function list()
    {
        return BatchStatus::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\BatchStatus
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\BatchStatus
     */
    public function select2(Request $request)
    {
        return BatchStatus::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * find data
     *
     * @param int $id
     *
     * @return \App\Model\Master\BatchStatus
     */
    public function find(int $id)
    {
        return BatchStatus::where('id', $id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\BatchStatus
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return BatchStatus::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getStatus()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {

        $status = $this->getStatus();

        if ($status->batch_item->count() > 0) {
            throw new HasChildException('tidak bisa delete sudah terpakai di Shipping Batch Item pada Status ini');
        }
        try {
            return $this->getStatus()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function deleteById(array $ids)
    {
        try {
            return BatchStatus::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * generate data
     *
     * @return array
     */
    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
        ];
    }

    public function setStatus(BatchStatus $status)
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }
}
