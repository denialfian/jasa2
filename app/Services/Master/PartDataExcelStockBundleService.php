<?php

namespace App\Services\Master;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use App\Model\Master\PartDataExcelStock;
use App\Model\Master\PartDataExcelStockBundle;
use App\Services\Master\PartDataExcelStockService;
use App\Model\Master\PartDataExcelStockBundleDetail;

class PartDataExcelStockBundleService
{
    public $pdesb;

    /**
     * get list query
     *
     * @return App\Model\Master\PartDataExcelStockBundle;
     */
    public function list()
    {
        return PartDataExcelStockBundle::with(['part_data_stock_bundle_detail.part_data_stock_inventory.part_data_stock','user']);
    }

    public function datatables($request = null)
    {
        $query = $this->list();
        $query = $query->orderBy('id', 'desc')->get();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function datatablesBundleDetails($pdesb_id)
    {
        $query = PartDataExcelStockBundleDetail::where('part_data_stock_bundle_id', $pdesb_id);
        $query = $query->orderBy('id', 'desc')->get();
        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    public function select2(Request $request, $asc_id=null)
    {
        $q =  PartDataExcelStockBundle::where('name', 'like', '%' . $request->q . '%');
        if ($asc_id != null) {
            $q->where('asc_id', $asc_id);
        }
        return $q = $q->limit(20)->get();
    }


    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\PartDataExcelStockBundle
     *
     * @throws \Exception
     */
    public function create($request)
    {
        DB::beginTransaction();
        try {
            $insert_main = PartDataExcelStockBundle::create($this->generateData($request));
            if($request->part_data_stock_inventories_id != null) {
                $stock_inventory_service = new PartDataExcelStockInventoryService();
                foreach($request->part_data_stock_inventories_id as $key => $id) {
                    $data_details = [
                        'part_data_stock_bundle_id' => $insert_main->id,
                        'part_data_stock_inventories_id' => $request->part_data_stock_inventories_id[$key],
                        'quantity' => $request->qty[$key],

                    ];
                    $insert_detail = PartDataExcelStockBundleDetail::create($data_details);
                }
            }
            DB::commit();
            return [$insert_main];
        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }


    public function generateData($row)
    {
        $data = [
            'name' =>  $row['name'],
            'main_price' => $row['main_price'],
            'asc_id' => $row['asc_id'],
            'users_id' => Auth::user()->id
        ];
        return $data;
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function updateX($data)
    {
        DB::beginTransaction();

        try {
            $update = $this->getData()->update($this->generateData($data));
            $this->updateDetails($data);
            DB::commit();
            return $update;

        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function deleteX()
    {
        $pdesb = $this->getData();
        DB::beginTransaction();

        try {
            $delete = $pdesb->delete();
            DB::commit();


        } catch (QueryException $e) {
            DB::rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\PartDataExcelStockBundle
     *
     * @throws \Exception
     */

    public function setData(PartDataExcelStockBundle $pdesb)
    {
        $this->pdesb = $pdesb;
        return $this;
    }

    public function getData()
    {
        return $this->pdesb;
    }

    public function deleteById(array $ids)
    {
        try {
            return PartDataExcelStockBundle::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function updateDetails($request)
    {
        $details_update = [];
        $details_insert = [];
        // loop request

        if(isset($request['bundle_details_id'])) {
            $stock_inventory_service = new PartDataExcelStockInventoryService();
            foreach ($request['bundle_details_id'] as $key => $value) {
                    if ($value != 0) {

                        $details_update[] = [
                            'id' => $value,
                            'part_data_stock_inventories_id' => $request->part_data_stock_inventories_id[$key],
                            'quantity' => $request->qty[$key],
                        ];

                    } else { // jika id 0 == insert
                        $details_insert[] = [
                            'part_data_stock_bundle_id' => $request->id,
                            'part_data_stock_inventories_id' => $request->part_data_stock_inventories_id[$key],
                            'quantity' => $request->qty[$key],
                        ];
                    }
                // }
            }

            DB::beginTransaction();
            try {
                // update
                if (count($details_update) > 0) {
                    // return $details_update;
                    foreach ($details_update as $data) {
                        PartDataExcelStockBundleDetail::where('id', $data['id'])->update([
                            'id' => $data['id'],
                            'part_data_stock_inventories_id' => $data['part_data_stock_inventories_id'],
                            'quantity' => $data['quantity'],
                        ]);
                    }
                }
                // insert
                if (count($details_insert) > 0) {
                    foreach($details_insert as $data) {
                        PartDataExcelStockBundleDetail::create([
                            'part_data_stock_bundle_id' =>  $data['part_data_stock_bundle_id'],
                            'part_data_stock_inventories_id' => $data['part_data_stock_inventories_id'],
                            'quantity' => $data['quantity'],
                        ]);
                    }
                }
                DB::commit();

            } catch (QueryException $e) {
                DB::rollback();
                throw new \Exception($e->getMessage());
            }
        }
    }

}
