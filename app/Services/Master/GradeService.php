<?php

namespace App\Services\Master;

use App\Model\Master\Grade;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class GradeService
{
    public $grade;

    /**
     * get list query
     */
    public function list()
    {
        return Grade::query();
    }

    /**
     * datatables data
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     */
    public function select2(Request $request)
    {
        return Grade::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * find data
     */
    public function find(int $id)
    {
        return Grade::where('id', $id)->first();
    }

    /**
     * create data
     */
    public function create(array $data)
    {
        try {
            return Grade::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     */
    public function update(array $data)
    {
        try {
            return $this->getGrade()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     */
    public function delete()
    {
        return $this->getGrade()->delete();
    }

    /**
     * delete multiple data
     */
    public function deleteById(array $ids)
    {
        try {
            return Grade::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
            'min_value' => $data['min_value'],
            'max_value' => $data['max_value'],
        ];
    }

    public function setGrade(Grade $grade)
    {
        $this->grade = $grade;
        return $this;
    }

    public function getGrade()
    {
        return $this->grade;
    }
}
