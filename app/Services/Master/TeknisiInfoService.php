<?php

namespace App\Services\Master;

use App\Model\Master\TeknisiInfo;
use App\Model\Master\MsAdditionalInfo;
use Illuminate\Database\QueryException;
use Auth;
use App\User;
use DB;
use App\Helpers\ImageUpload;

class TeknisiInfoService
{
    public $teknisiInfo;

    /**
     * get list query
     * 
     * @return \App\Model\Master\TeknisiInfo
     */
    public function list()
    {
        $userlogin = Auth::user()->id;
        return TeknisiInfo::where('user_id', $userlogin)->get();
    }

    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Master\TeknisiInfo
     * 
     * @throws \Exception
     */

    public function create(array $data)
    {
        
        try{
            return TeknisiInfo::create($this->generateData($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function save(array $data)
    {
        DB::beginTransaction();
        try {
            // insert data
            $this->generateDataInfo($data);
            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new \Exception($e->getMessage());
        }
    }

    public function generateDataInfo(array $data)
    {
            $user = auth()->user();
            $path = MsAdditionalInfo::getImagePathUpload();
            $filename = null;   

            if ($data['picture']) {
                $image = (new ImageUpload)->upload($data['picture'], $path);
                $filename = $image->getFilename();
            }

           // data profile
           MsAdditionalInfo::create([
                'ms_religion_id' =>$data{'ms_religion_id'},
                'ms_marital_id' =>$data{'ms_marital_id'},
                'first_name' =>$data{'first_name'},
                'last_name' =>$data{'last_name'},
                'date_of_birth' =>$data{'date_of_birth'},
                'place_of_birth' =>$data{'place_of_birth'},
                'gender' =>$data{'gender'},
                'picture' => $filename,
                'user_id' => $user->id
            ]);



            // data teknisi
             TeknisiInfo::create($this->generateData($data));

    }

    // public function updateStatus()
    // {
    //     $userLogin = Auth::user()->id;
    //     $user = User::where('id', $userLogin)->first();
        
    //     if($user->status == 1){
    //         User::where('id', $userLogin)->update([
    //             'status' => 6,
    //         ]);
    //     }

    //     return $this->successResponse($user, 'success Approve');
    // }

    public function update(array $data)
    {
        try{
            return $this->getTeknisiInfo()->update($this->generateData($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function delete()
    {
        try{
            return $this->getTeknisiInfo()->delete();
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }


    public function generateData(array $data)
    {
        $user =  Auth::user()->id;

        $path = TeknisiInfo::getImagePathUpload();
    
        // dd($path);
        $filename = null;

        if ($data['attachment']) {
            $image = (new ImageUpload)->upload($data['attachment'], $path);
            $filename = $image->getFilename();
        }

        $data = [
            'attachment' => $filename,
            'no_identity' => $data['no_identity'],
            'ms_curriculum_id' => $data['ms_curriculum_id'],
            'user_id' => $user,
        ];

        return array_merge($data);
    }

    public function setTeknisiInfo(TeknisiInfo $teknisiInfo)
    {
        $this->teknisiInfo = $teknisiInfo;
        return $this;
    }

    public function getTeknisiInfo()
    {
        return $this->teknisiInfo;
    }
}