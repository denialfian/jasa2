<?php

namespace App\Services\Master;

use App\Model\Product\ProductBrand;
use App\Exceptions\HasChildException;
use App\Model\Master\MsProductCategory;
use Illuminate\Database\QueryException;

class ProductCategoryService
{
    public $productCategory;

    /**
     * get list query
     *
     * @return App\Model\Master\MsProductCategory;
     */
    public function list()
    {
        return MsProductCategory::with(['brand'])->orderBy('id','desc');
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\MsProductCategory
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        $data = [
            'name' => $data['name'],
            'slug' => $data['slug'],
            'product_cate_code' => $data['product_cate_code'],
        ];
        try {
            MsProductCategory::create($data);

        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {

        $data = [
            'name' => $data['name'],
            'slug' => $data['slug'],
            'product_cate_code' => $data['product_cate_code'],
        ];

        $productCategory = $this->getProductCategory();

        try {
            return $productCategory->update($data);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    // public function delete()
    // {
    //     $productCategory = $this->getProductCategory();

    //     if ($productCategory->whereHas('models')->count() > 0) {
    //         throw new \Exception('tidak bisa delete terdapat models pada product category ini');
    //     }

    //     try {
    //         return $productCategory->delete();
    //     } catch (QueryException $e) {
    //         throw new \Exception($e->getMessage());
    //     }
    // }

    public function delete()
	{
		$productCategory = $this->getProductCategory();
		// Find all items with the parent_id of this one and reset the parent_id to zero
		$items = MsProductCategory::where('parent_id', $productCategory->id)->get()->each(function($item)
		{
			$item->delete();
		});
        try {
            return $productCategory->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
	}

    public function deleteById(array $ids)
    {
        $prod_category = $this->getProductCategory();
        try {
            return MsProductCategory::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function checkParent(array $data)
    {
        $brand = MsProductCategory::find($data['parent_id'])->first();

        if ($brand == null) {
            throw new \Exception('parent yg dipilih tidak ditemukan');
        }

        return $brand;
    }

    public function setProductCategory(MsProductCategory $productCategory)
    {
        $this->productCategory = $productCategory;
        return $this;
    }

    public function getProductCategory()
    {
        return $this->productCategory;
    }

    public function updateMoveCategory($request) {

        $data = json_decode($request->data);
        $readbleArray = $this->parseJsonArray($data);

        foreach($readbleArray as $key => $row){
            $update = MsProductCategory::where('id',$row['id'])->update(['parent_id' => $row['parentID'], 'order' => $key]);
        }
    }

    public function parseJsonArray($jsonArray, $parentID = 0) {
        $return = array();
        foreach ($jsonArray as $subArray) {
          $returnSubSubArray = array();
          if (isset($subArray->children)) {
               $returnSubSubArray = $this->parseJsonArray($subArray->children, $subArray->id);
          }
          $return[] = array('id' => $subArray->id, 'parentID' => $parentID);
          $return = array_merge($return, $returnSubSubArray);
        }
        return $return;
    }

    public function buildMenu($product_category, $parentid = 0)
	{
	  $result = null;
	  foreach ($product_category as $item)
	    if ($item->parent_id == $parentid) {
	      $result .= "<li class='dd-item nested-list-item' data-order='{$item->order}' data-id='{$item->id}'>
	      <div class='dd-handle nested-list-handle'>
	        <span class='fa fa-arrows-alt'></span>
	      </div>
	      <div class='nested-list-content'>{$item->name}
	        <div class='pull-right'>
	          <a href='#editModal' class='update_toggle' rel='{$item->id}'  data-toggle='modal' data-backdrop='false'>Edit</a> |
	          <a href='#' class='delete_toggle' rel='{$item->id}'>Delete</a>
	        </div>
	      </div>".$this->buildMenu($product_category, $item->id) . "</li>";
	    }
	  return $result ?  "\n<ol class=\"dd-list\">\n$result</ol>\n" : null;
	}

	// Getter for the HTML menu builder
	public function getHTML($items)
	{
		return $this->buildMenu($items);
    }



}
