<?php

namespace App\Services\Master;

use App\Model\Master\Religion;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ReligionService
{
    public $religion;

    /**
     * get list query
     *
     * @return App\Model\Master\Religion;
     */
    public function list()
    {
        return Religion::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Product\Religion
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Product\Religion
     */
    public function select2(Request $request)
    {
        return Religion::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\Religion
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return Religion::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getReligion()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getReligion()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return Religion::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
        ];
    }

    public function setReligion(Religion $religion)
    {
        $this->religion = $religion;
        return $this;
    }

    public function getReligion()
    {
        return $this->religion;
    }
}
