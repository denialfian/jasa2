<?php

namespace App\Services\Master;

use App\Model\Master\TeknisiInfo;
use App\Model\Master\MsAdditionalInfo;
use Illuminate\Database\QueryException;
use App\Model\Master\VoucherGeneral;
use Auth;
use App\User;
use DB;
use App\Helpers\ImageUpload;

class VoucherGeneralService
{
    public $generalVoucher;

    /**
     * get list query
     * 
     * @return \App\Model\Master\VoucherGeneral
     */
    public function list()
    {
        return VoucherGeneral::orderBy('id', 'Desc')->get();
    }

    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Master\VoucherGeneral
     * 
     * @throws \Exception
     */

    public function create(array $data)
    {
        
        try{
            return VoucherGeneral::create($this->generateData($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function update(array $data)
    {
        try{
            return $this->getGeneralVoucher()->update($this->generateDataUpdate($data));
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function delete()
    {
        try{
            return $this->getGeneralVoucher()->delete();
        }catch(QueryException $e){
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        $length = 6;
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $data = [
            'code' => 'VCR'. date('ymd') . $randomString,
            'voucher_type' => $data['voucher_type'],
            'type' => $data['type'],
            'value' => $data['value'],
            'max_nominal' => $data['max_nominal'],
            'min_payment' => $data['min_payment'],
            'valid_at' => $data['valid_at'],
            'valid_until' => $data['valid_until'],
            'desc' => $data['desc'],
        ];

        return array_merge($data);
    }

    public function generateDataUpdate(array $data)
    {

        $data = [
            'voucher_type' => $data['voucher_type'],
            'type' => $data['type'],
            'value' => $data['value'],
            'max_nominal' => $data['max_nominal'],
            'min_payment' => $data['min_payment'],
            'valid_at' => $data['valid_at'],
            'valid_until' => $data['valid_until'],
            'desc' => $data['desc'],
        ];

        return array_merge($data);
    }

    public function setGeneralVoucher(VoucherGeneral $generalVoucher)
    {
        $this->generalVoucher = $generalVoucher;
        return $this;
    }

    public function getGeneralVoucher()
    {
        return $this->generalVoucher;
    }
}