<?php

namespace App\Services\Master;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Model\Master\SymptomCode;
use App\Exceptions\HasChildException;
use Illuminate\Database\QueryException;

class SymptomCodeService
{
    public $symptomCode;

    /**
     * get list query
     *
     * @return \App\Model\Master\Msorder-status;
     */
    public function list()
    {
        return SymptomCode::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Master\ServiceStatus
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Master\SymptomCode
     */
    public function select2(Request $request)
    {
        return SymptomCode::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * find data
     *
     * @param int $id
     *
     * @return \App\Model\Master\SymptomCode
     */
    public function find(int $id)
    {
        return SymptomCode::where('id', $id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\SymptomCode
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return SymptomCode::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getSymptomCode()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return $this->getSymptomCode()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function deleteById(array $ids)
    {
        try {
            return SymptomCode::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * generate data
     *
     * @return array
     */
    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
        ];
    }

    public function setSymptomCode(SymptomCode $symptomCode)
    {
        $this->symptomCode = $symptomCode;
        return $this;
    }

    public function getSymptomCode()
    {
        return $this->symptomCode;
    }
}
