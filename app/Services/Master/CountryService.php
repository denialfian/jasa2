<?php

namespace App\Services\Master;

use App\Exceptions\HasChildException;
use App\Model\Master\MsCountry;
use Illuminate\Database\QueryException;

class CountryService
{
    public $country;

    /**
     * get list query
     *
     * @return App\Model\Master\MsCountry;
     */
    public function list()
    {
        return MsCountry::with('provinces');
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Master\MsCountry
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return MsCountry::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getCountry()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        $country = $this->getCountry();

        $count = $country->provinces->count();

        if ($count > 0) {
            throw new HasChildException('tidak bisa delete terdapat (' . $count . ') provinsi pada negara ini');
        }

        try {
            return $country->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return MsCountry::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
            'code' => $data['code'],
        ];
    }

    public function setCountry(MsCountry $country)
    {
        $this->country = $country;
        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }
}
