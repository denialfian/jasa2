<?php

namespace App\Services\Master;

use App\Model\Master\UserStatus;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UserStatusService
{
    public $userStatus;
    /**
     * get list query
     * 
     * @return App\Model\Master\UserStatus;
     */
    public function list()
    {
        return UserStatus::query();
    }

    /**
     * datatables data
     * 
     * @return \App\Model\Master\UserStatus
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     * 
     * @return \App\Model\Master\UserStatus
     */
    public function select2(Request $request)
    {
        return UserStatus::where('name', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Master\UserStatus
     * 
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return UserStatus::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     * 
     * @return bool
     * 
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getUserStatus()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     * 
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function delete()
    {
        try {
            return  $this->getUserStatus()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function deleteById(array $ids)
    {
        try {
            return UserStatus::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'name' => $data['name'],
        ];
    }

    public function setUserStatus(UserStatus $userStatus)
    {
        $this->userStatus = $userStatus;
        return $this;
    }

    public function getUserStatus()
    {
        return $this->userStatus;
    }
}
