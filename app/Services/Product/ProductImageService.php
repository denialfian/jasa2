<?php

namespace App\Services\Product;

use App\Model\Master\MsProduct;
use App\Model\Product\ProductImage;
use Illuminate\Database\QueryException;

class ProductImageService
{
    public $product;

    /**
     * create data
     *
     * @param array $data
     * 
     * @return \App\Model\Product\ProductImage
     * 
     * @throws \Exception
     */
    public function create(array $product_images)
    {
        try {
            return ProductImage::insert($this->generateData($product_images));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     * 
     * @return bool|null
     * 
     * @throws \Exception
     */
    public function deleteAndCreate(array $product_images)
    {
        $product = $this->getProduct();
        if ($product == null) {
            throw new \Exception('product tidak ditemukan');
        }
        try {
            ProductImage::where('product_id', $product->id)->delete();
            return ProductImage::insert($this->generateData($product_images));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $product_images)
    {
        $product = $this->getProduct();
        $data = [];
        foreach ($product_images as $value) {
            $data[] = [
                'product_id' => $product->id,
                'galery_image_id' => $value,
            ];
        }
        return $data;
    }

    public function setProduct(MsProduct $product)
    {
        $this->product = $product;
        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }
}
