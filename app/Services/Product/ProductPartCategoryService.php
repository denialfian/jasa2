<?php

namespace App\Services\Product;

use App\Model\Product\ProductBrand;
use App\Model\Product\ProductPartCategory;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ProductPartCategoryService
{
    public $category;

    /**
     * dipakai saat delete data
     * akan mengecek ke semua relasi 
     * pada model ini
     * untuk mencari jika model ini sudah di pakai di model lain
     * note: lumayan berat karna harus cek ke semua table
     */
    public $use_relation_check = true;

    /**
     * get list query
     *
     * @return App\Model\Product\ProductPartCategory;
     */
    public function list()
    {
        return ProductPartCategory::query();
    }

    /**
     * datatables data
     *
     * @return \App\Model\Product\ProductPartCategory
     */
    public function datatables()
    {
        $query = $this->list();

        return DataTables::of($query)
            ->addIndexColumn()
            ->toJson();
    }

    /**
     * select2 data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Model\Product\ProductPartCategory
     */
    public function select2(Request $request)
    {
        return ProductPartCategory::where('part_category', 'like', '%' . $request->q . '%')
            ->limit(20)
            ->get();
    }

    /**
     * find data
     *
     * @param int $id
     *
     * @return \App\Model\Product\ProductPartCategory
     */
    public function find(int $id)
    {
        return ProductPartCategory::where('id', $id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\Model\Product\ProductPartCategory
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        try {
            return ProductPartCategory::create($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * update data
     *
     * @param array $data
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function update(array $data)
    {
        try {
            return $this->getCategory()->update($this->generateData($data));
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * delete data
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        return $this->relasiCheck()->delete();
    }

    public function deleteById(array $ids)
    {
        try {
            return ProductPartCategory::whereIn('id', $ids)->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function generateData(array $data)
    {
        return [
            'part_category' => $data['part_category'],
            'part_code' => $data['part_code'],
        ];
    }

    public function relasiCheck()
    {
        $row = $this->getCategory();

        if ($this->use_relation_check) {
            // cek ke produk brand
            $brand = ProductBrand::where('product_part_category_id', $row->id)->first();
            if ($brand != null) {
                throw new \Exception('already used in the brand product module, brand name (' . $brand->name . ')');
            }
        }

        return $row;
    }

    public function setCategory(ProductPartCategory $category)
    {
        $this->category = $category;
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }
}
