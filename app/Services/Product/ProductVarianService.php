<?php

namespace App\Services\Product;

use stdClass;
use App\Model\Master\MsProductModel as Product;
use App\Model\Product\ProductVarian;
use App\Model\Product\ProductVarianAttribute;
use Illuminate\Database\QueryException;

class ProductVarianService
{
    public $product;
    public $varian;

    /**
     * create data
     *
     * @param array $data
     * @param array $product_vendor_id
     *
     * @return \App\Model\Product\ProductVarian
     *
     * @throws \Exception
     */
    public function create(array $data, $product_vendor_id, $key)
    {

        $data_success = new StdClass();
        $product_varian = [];
        $product_varian_attribute = [];
        try {

                $dataVarian = [
                    'stock' => $data['stock'][$key],
                    'product_vendor_id' => $product_vendor_id,
                ];
                $varian = ProductVarian::create($dataVarian);

                $product_varian[] = $varian;
                if(isset($data['product_attribute_id'])) {
                    foreach($data['product_attribute_id'] as $key => $val) {
                        $dataAttribute = $this->createAttribute($varian['id'], $data, $key);
                        $dataVarianAttribute = ProductVarianAttribute::insert($dataAttribute);
                        $product_varian_attribute[] = $dataAttribute;
                    }
                }

            $data_success->varian = $product_varian;
            $data_success->attribute = $product_varian_attribute;
            return $data_success;
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function createAttribute($varian_id, array $data, $uniq)
    {
        $attributes = $data['product_attribute_id'][$uniq];
        $terms = $data['product_attribute_term_id'][$uniq];
        $dataVarianAttribute = [
            'product_varian_id' => $varian_id,
            'product_attribute_id' => $attributes,
            'product_attribute_term_id' => $terms,
        ];

        return $dataVarianAttribute;

    }

    public function update(array $data)
    {
        $stocks = $data['stock'];
        $varians = $data['varian_id'];

        try {
            foreach ($stocks as $key => $value) {
                $varian_id = $varians[$key];
                $dataVarian = [
                    'stock' => $stocks[$key],
                ];
                $varian = ProductVarian::where('id', $varian_id)->first();
                // update varian
                $varian->update($dataVarian);
                // delete varian attribute
                ProductVarianAttribute::where('product_varian_id', $varian_id)->delete();
                // insert ulang attribute
                $dataAttribute = $this->createAttribute($varian, $data, $key);
                ProductVarianAttribute::insert($dataAttribute);
            }
            return $varian;
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function delete()
    {
        try {
            return $this->getVarian()->delete();
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function setProduct(Product $product)
    {
        $this->product = $product;
        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setVarian(ProductVarian $varian)
    {
        $this->varian = $varian;
        return $this;
    }

    public function getVarian()
    {
        return $this->varian;
    }
}
