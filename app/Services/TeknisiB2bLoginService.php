<?php

namespace App\Services;

use Ap;
use App\Helpers\ImageUpload;
use DB;
use App\User;
use App\Model\RequestOrders;
use App\Model\RequestOrderDetail;
use App\Model\RequestOrderTmp;
use App\Model\RequestOrderImage;
use App\Services\User\UserB2bUpdateService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Services\User\TeknisiLoginB2b;
use Intervention\Image\ImageManagerStatic as Image;
use App\Mail\SuccessRegisterEmail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use App\Services\User\UserStoreB2bService;
use App\Model\BusinessToBusinessOutletDetailTransaction;

class TeknisiB2bLoginService
{
    public $token_name = 'jasaWebb3b';
    public $user;
    public $notif    = true;
    public $redirect = '/';
    public $status;

    public function login(array $credentials)   
    {
        $loginService = new TeknisiLoginB2b();
        $login = $loginService->handle($credentials);
        if ($login) {
            // $this->setUser($loginService->getUser())->setBearerToken();
            $this->setUser($loginService->getUser());
        }

        return $login;
    }

    public function dontSendNotif()
    {
        $this->notif = false;
        return $this;
    }

    public function getTokenName(): string
    {
        return $this->token_name;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }
}