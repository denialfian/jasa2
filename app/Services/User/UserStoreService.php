<?php

namespace App\Services\User;

use App\Model\Master\MsAdditionalInfo;
use App\Model\Master\MsAddress;
use App\Model\Master\MsVillage;
use App\Model\Technician\Technician;
use App\User;
use DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class UserStoreService
{
    public $role;

    /**
     * handle create data
     *
     * @param array $data
     *
     * @return \App\User
     *
     * @throws \Exception
     */
    public function handle(array $data)
    {
        // if (isset($data['role_id'])) {
        //     $role = Role::where('id', $data['role_id'])->firstOrFail();
        // } else {
        //     $role = Role::firstOrFail();
        // }

        // return $this->setRole($role)->create($data);
        $user = $this->create($data);
        return User::with('roles', 'info')->where('id', $user->id)->first();
    }

    /**
     * create data
     *
     * @param array $data
     *
     * @return \App\User
     *
     * @throws \Exception
     */
    public function create(array $data)
    {
        $phone = isset($data['phone']) ? $data['phone'] : null;
        $customer = Role::where('name', 'Customer')->first();

        // login as customer
        $userData = [
            'name'      => $data['name'],
            'email'     => $data['email'],
            'phone'     => $phone,
            'password'  => Hash::make($data['password']),
            'api_token' => Str::random(80),
            'status'    => isset($data['status']) ? $data['status'] : '1',
        ];

        // login as teknisi
        if (isset($data['role_id'])) {
            if ($data['role_id'] == 2) {
                $userData = [
                    'name'                => $data['name'],
                    'email'               => $data['email'],
                    'phone'               => $phone,
                    'password'            => Hash::make($data['password']),
                    'api_token'           => Str::random(80),
                    'status'              => isset($data['status']) ? $data['status'] : '6',
                    'is_login_as_teknisi' => '1',
                ];
            }
        }

        try {
            return User::create($userData)->assignRole($customer->id);
        } catch (QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function createFromAdmin(array $data)
    {
        $phone = str_replace(' ', '', $data['phone']);

        $userData = [
            'name'      => $data['name'],
            'email'     => $data['email'],
            'phone'     => $phone,
            'password'  => Hash::make($data['password']),
            'api_token' => Str::random(80),
            'status'    => 1,
        ];

        $roles       = Role::whereIn('id', $data['role_id'])->get();
        $teknisiRole = $roles->where('name', 'Technician')->first();
        $adminRole   = $roles->where('name', 'admin')->first();

        $village = MsVillage::where('id', $data['ms_village_id'])->first();

        DB::beginTransaction();
        try {
            $user = User::create($userData);
            // email verify
            $user->markEmailAsVerified();

            // otp verify
            if ($phone != '') {
                $user->update([
                    'otp_verified_at' => now(),
                ]);
            }

            // assign role
            if ($adminRole != null) {
                $user->assignRole($adminRole->id);
            } else {
                $user->assignRole(
                    Role::whereIn('id', $data['role_id'])->where('name', '!=', 'admin')->get()
                );
            }

            // buat teknisi
            if ($teknisiRole != null && $adminRole == null) {
                Technician::create([
                    'user_id' => $user->id,
                ]);
            }

            // create user info
            MsAdditionalInfo::create([
                'first_name' => $user->name,
                'user_id'    => $user->id,
            ]);

            // create address
            MsAddress::create([
                'phone1'             => '6285718441777',
                'address'            => $data['address'],
                'is_main'            => 1,
                'user_id'            => $user->id,
                'ms_address_type_id' => $data['ms_address_type_id'],
                'ms_village_id'      => $village == null ? null : $village->id,
                'ms_district_id'     => $village == null ? null : $village->district->id,
                'ms_city_id'         => $village == null ? null : $village->district->city->id,
                'ms_zipcode_id'      => $village == null ? null : $village->district->zipcode->id,
                'latitude'           => $data['lat'],
                'longitude'          => $data['long'],
                'cities'             => $data['cities'],
                'province'           => $data['prv'],
                'location_google'    => $data['searchMap'],
            ]);

            // commit db
            DB::commit();
        } catch (\Exception $e) {
            // rollback db
            DB::rollback();

            throw new \Exception($e->getMessage());
        }

        return $user;
    }

    // asd

    public function setRole(Role $role)
    {
        $this->role = $role;
        return $this;
    }

    public function getRole(): Role
    {
        return $this->role;
    }
}
