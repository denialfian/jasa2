@extends('admin.home')
<style>
    .box {
    position: relative;
    display: inline-block;
    width: 210px;
    height: 120px;
    background-color: #fff;
    border-radius: 5px;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
    border-radius: 5px;
    -webkit-transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
    transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
    }

    .box::after {
    content: "";
    border-radius: 5px;
    position: absolute;
    z-index: -1;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    box-shadow: 0 5px 15px rgba(0, 0, 0, 0.3);
    opacity: 0;
    -webkit-transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
    transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
    }

    .box:hover {
    -webkit-transform: scale(1.05, 1.05);
    transform: scale(1.05, 1.05);
    }

    .box:hover::after {
        opacity: 1;
    }

</style>
@section('content')
<div class="col-md-12 col-xl-12">
    <div class="row">
        @foreach($getServices as $service)
        <div class="col-lg-6 col-xl-3" style="margin-bottom: 20px">
            <div class="card">
                <div class="card-body text-center">
                    <a href="{{ url('customer/request_job/'.$service->slug) }}">
                        {{ $service->name }}
                    </a>
                    <div class="widget-subheading text-center">
                        <div class="font-icon-wrapper">
                            <a href="{{ url('customer/request_job/'.$service->slug) }}">
                                <i aria-hidden="true" class="fa fa-arrow-circle-right" title="go">
                                </i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>

@endsection

@section('script')
@endsection
