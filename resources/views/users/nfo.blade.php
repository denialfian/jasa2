@extends('admin.home')
@section('content')
    @if(Auth::user()->status == 7 )
        <div class="col-md-12">
            <div class="card">
                <div class="card-header-tab card-header">
                    <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                        Technician Register
                    </div>
                </div>
                <div class="card-body">
                    <h3 align="center">Your account is being verified by admin</h3>
                    <p align="center">please attention to the login notification to your email account</p>
                </div>
            </div>
        </div>
    @else
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                @if ( is_null(Auth::user()->otp_verified_at) )
                    <div class="col-md-12">
                        <h2 align="center" style="color: #b2bec3">Your number has not been verified in our system</h2>
                    </div>
                    @if ( !is_null(Auth::user()->otp_code) )
                        @if ( !is_null(Auth::user()->otp_verified_at) )
                            @if ( !is_null( Auth::user()->otp_url ) )
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                {{ Auth::user()->phone }}   
                                            </div>
                                            <div class="col-md-6">
                                            <button type="button" class="btn btn-danger  btn-sm btn-white" data-toggle="modal" data-target="#exampleModal">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Update Phone Number
                                            </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>      
                            @endif
                        @else

                        
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row" align="center">
                                        <form action="{{ route('otp.verification') }}" method="post" style="display: contents">
                                            @csrf
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="credit-card" name="otp" placeholder="X - X - X - X " data-inputmask="'mask': '9  9  9  9'" style="width: 300px">
                                            </div>
                                            <div class="col-md-2" style="float: left">
                                                <button type="submit"  class="btn btn-success btn-sm btn-white" >Varification</button>
                                            </div>
                                            &nbsp;&nbsp;&nbsp;
                                        </form>
                            
                                        <form action="{{ route('otp.resend.verification') }}" method="post" style="display: contents">
                                            @csrf
                                            <div class="col-md-2">
                                                <button type="submit"  class="btn btn-danger btn-sm btn-white" >Resend</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endif        
                    @else
                        <div class="col-md-12" align="center">
                            <h5 align="center" style="color:#636e72">We will send the verification code to <strong>{{  Auth::user()->phone }}</strong> by pressing the button below</h6>
                        </div><hr>
                        <div class="col-md-12" align="center">
                            <button class="sendOTP btn btn-success btn-sm btn-white" align="center"><i class='fa fa-check'></i>&nbsp; Send Code OTP</button>
                        </div>
                    @endif
                @else
                <form id="form-teknisi" method="POST" enctype="multipart/form-data">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6"><br>
                                <div align="center">
                                    <div class="circle-2">
                                        <img  src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" id="profile-image1" />
                                    </div>
                                    <!-- <img alt="User Pic" src="https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg" id="profile-image1" class="circle"> -->
                                    <div class="p-image"><br><br>
                                        <input id="profile-image-upload" class="form-group" name="picture" type="file" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3 style="color:#00b1b1;" align="center"><strong>Create Profile</strong></h3>
                                <hr>
                                <table>
                                    <tr>
                                        <td><h5 style="font-family:'Times New Roman', Times, serif">First Name</h5></td>
                                        <td style="color:white">&nbsp;&nbsp;____</td>
                                        <td><div class="col-sm-12"><input name="first_name" id="examplename" placeholder="First Name" type="text" class="form-control"></div></td>
                                    </tr>
                                    <tr>
                                        <td><h5 style="font-family:'Times New Roman', Times, serif">Last Name</h5></td>
                                        <td style="color:white">&nbsp;&nbsp;____</td>
                                        <td><div class="col-sm-12"><input name="last_name" id="examplename" placeholder="Last Name" type="text" class="form-control"></div></td>
                                    </tr>
                                    <tr>
                                        <td><h5 style="font-family:'Times New Roman', Times, serif">Date Of Birth</h5> </td>
                                        <td style="color:white">&nbsp;&nbsp;____</td>
                                        <td>
                                            <div class="col-sm-12">
                                                <input name="date_of_birth" id="datetimepicker" placeholder="" type="text" class="form-control">
                                                <!-- <input class="form-control" name="" id="" type="text"> -->
                                            </div>
                                            <!-- <div class="col-sm-12"><input name="date_of_birth" id="examplename" placeholder="Date Of Birth" type="date" class="form-control"></div> -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><h5 style="font-family:'Times New Roman', Times, serif">Place Of Birth</h5></td>
                                        <td style="color:white">&nbsp;&nbsp;____</td>
                                        <td><div class="col-sm-12"><input name="place_of_birth" id="examplename" placeholder="Place Of Birth" type="text" class="form-control"></div></td>
                                    </tr>
                                    <tr>
                                        <td><h5 style="font-family:'Times New Roman', Times, serif">Marital</h5></td>
                                        <td style="color:white">&nbsp;&nbsp;____</td>
                                        <td><div class="col-md-12">
                                            <select class="js-example-basic-single" id="marital" name="ms_marital_id" style="width:100%">
                                                <option value="marital">Marital</option>
                                            </select>
                                        </div></td>
                                    </tr>
                                    <tr>
                                        <td><h5 style="font-family:'Times New Roman', Times, serif">Religion</h5></td>
                                        <td style="color:white">&nbsp;&nbsp;____</td>
                                        <td><div class="col-md-12">
                                            <select class="js-example-basic-single" id="religion" name="ms_religion_id" style="width:100%">
                                                <option value="religion">Religion</option>
                                            </select>
                                        </div></td>
                                    </tr>
                                    <tr>
                                        <td><h5 style="font-family:'Times New Roman', Times, serif">Gender</h5></td>
                                        <td style="color:white">&nbsp;&nbsp;____</td>
                                        <td><div class="col-md-12">
                                            <select class="js-example-basic-single" id="gender" name="gender" style="width:100%">
                                                <option value="0">Male</option>
                                                <option value="1">Woman</option>
                                                <option value="2">another one</option>
                                            </select>
                                        </div></td>
                                    </tr>
                                </table>
                                <hr>
                                <!-- <button class="btn_login" type="submit" id="save">Save</button> -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
@section