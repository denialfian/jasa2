<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{{ $title_header_global }}</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="{{ $metta_header_global }}">
    <meta name="msapplication-tap-highlight" content="no">

	<link href="https://demo.dashboardpack.com/architectui-html-pro/main.d810cf0ae7f39f28f336.css" rel="stylesheet"></head>
<style>
    <style>
    div.hr-or {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    div.hr-or:before {
        content: 'OR';
        background-color: #fff;
    }

    div.hr {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    .hr-title {
        background-color: #fff;
    }
</style>
</style>
<body>
    <div class="app-container app-theme-white body-tabs-shadow">
        <div class="app-container">
            <div class="h-100">
                <div class="h-100 no-gutters row">
                    <div class="d-none d-lg-block col-lg-4">
                        <div class="slider-light">
                            <div class="slick-slider">
                                <div>
                                    <div class="position-relative h-100 d-flex justify-content-center align-items-center bg-plum-plate" tabindex="-1">
                                        <div class="slide-img-bg" style="background-image: url('{{ asset('image2.jpg')}}');"></div>
                                        <div class="slider-content">
                                            {{-- <h3>Perfect Balance</h3>
                                            <p>ArchitectUI is like a dream. Some think it's too good to be true! Extensive
                                                collection of unified React Boostrap Components and Elements.
                                            </p> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="h-100 d-flex bg-white justify-content-center align-items-center col-md-12 col-lg-8">
                        <div class="mx-auto app-login-box col-sm-12 col-md-10 col-lg-9">
                            <div >
                                <img src="{{ url('/get/logo/from-storage/'.$get_logo_astech) }}" alt="" style="width:150px;height:auto">
                            </div><br>
                            <h4 class="mb-0">
                                <span class="d-block">Welcome back,</span>
                                <span>Please sign in to your account.</span>
                            </h4>
                            <h6 class="mt-3">No account? <a href="{{ url('/user/register') }}" class="text-primary">Sign up now</a></h6>
								<div class="divider row"></div>
                            <div>
								@if(session()->has('error'))
									<div class="alert alert-danger" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<strong>{{ session()->get('error') }} </strong>
									</div>
                                @endif
                                @if(session()->has('success'))
									<div class="alert alert-primary" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<strong>{{ session()->get('success') }} </strong>
									</div>
								@endif
								<form class="" id="signupForm" action="{{ url('loginUsers') }}" method="post">
									@csrf
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="exampleEmail" class="">Email</label>
												<input name="email" id="exampleEmail" placeholder="Email here..." type="email" class="form-control">
												
											</div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="examplePassword" class="">Password</label>
                                                <input name="password" id="examplePassword" placeholder="Password here..." type="password" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative form-check">
                                        {{-- <input name="check" id="exampleCheck" type="checkbox" class="form-check-input">
                                        <label for="exampleCheck" class="form-check-label">Keep me logged in</label> --}}
                                        <div class="ml-auto" style="float: right">
                                            <a href="{{url('/forgot-password-form')}}" class="btn-lg btn btn-link">Recover Password</a>
                                            <button class="btn btn-primary btn-lg">Login to Dashboard</button>
                                        </div>
                                    </div><br><br>
                                    
                                    @if($social_media_login_google->value == 1 || $social_media_login_fb->value == 1)
                                    <div class='hr'>
                                        <span class='hr-title'><strong>Or Sign With</strong></span>
                                    </div>
                                    @endif                                        
                                    
                                    <div class="form-row">
                                        @if($social_media_login_google->value == 1 && $social_media_login_fb->value == 0)
                                        <div class="col-md-12">
                                            <div class="position-relative form-group">
                                                <a href="{{ url('/auth/google') }}" class="btn btn-light btn-lg btn-block" style="color:white; background-color: #dd4b39"><img src="{{ asset('google-plus.png') }}" alt="" width="20px" style="float: left;"> <strong>Google</strong></a>
											</div>
                                        </div>
                                        @endif
                                        @if($social_media_login_google->value == 0 && $social_media_login_fb->value == 1)
                                        <div class="col-md-12">
                                            <div class="position-relative form-group">
                                                <a href="{{ url('/auth/facebook') }}" class="btn btn-light btn-lg btn-block" style="color:white; background-color: #4596f3"><img src="{{ asset('facebook_.png') }}" alt="" width="20px" style="float: left"> <strong>Facebook</strong></a>
                                            </div>
                                        </div>  
                                        @endif 
                                        @if($social_media_login_google->value == 1 && $social_media_login_fb->value == 1)
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <a href="{{ url('/auth/google') }}" class="btn btn-light btn-lg btn-block" style="color:white; background-color: #dd4b39"><img src="{{ asset('google-plus.png') }}" alt="" width="20px" style="float: left;"> <strong>Google</strong></a>
											</div>
                                        </div> 
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <a href="{{ url('/auth/facebook') }}" class="btn btn-light btn-lg btn-block" style="color:white; background-color: #4596f3"><img src="{{ asset('facebook_.png') }}" alt="" width="20px" style="float: left"> <strong>Facebook</strong></a>
                                            </div>
                                        </div>  
                                        @endif                                                                             
                                    </div>
                                    
                                    {{-- <div class="d-flex align-items-center">
                                        <div class="ml-auto">
                                            <a href="{{url('/forgot-password-form')}}" class="btn-lg btn btn-link">Recover Password</a>
                                            <button class="btn btn-primary btn-lg">Login to Dashboard</button>
                                        </div>
                                    </div> --}}
                                    <div class="form-group row">
                                        <div class="col-md-12 offset-md-6">
                                            
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://demo.dashboardpack.com/architectui-html-pro/assets/scripts/main.d810cf0ae7f39f28f336.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    @if (Session::has('success'))
    <script>
        // swal({
        //     type: 'success',
        //     title: '{{ Session::get("success") }}',
        //     icon: "success",
        // });
    </script>
    @endif

    @if (Session::has('error'))
    <script>
        // swal({
        //     type: 'error',
        //     title: '{{ Session::get("error") }}',
        //     icon: "error",
        // });
    </script>
    @endif
</body>

</html>
