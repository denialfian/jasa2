@extends('admin.home')
@section('content')
<div class="col-md-12 card">
    <div class="card-header-tab card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            {{ $title }}
        </div>
        <div class="btn-actions-pane-right text-capitalize">
            <a href="{{ url('/admin/educations/create') }}" class="mb-2 mr-2 btn btn-primary btn-sm" style="float:right">Add Education</a>
        </div>
    </div>
    <div class="card-body">
        <table id="table-education" class="table table-hover table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Institution</th>
                    <th>Place</th>
                    <th>Education Start Date</th>
                    <th>Education End Date</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
@section('script')
    <script>
        globalCRUD.datatables({
            url: '/education/datatables',
            selector: '#table-education',
            columnsField: ['id', 'institution', 'place', 'education_start_date', 'education_end_date'],
            actionLink: {
                update: function(row) {
                    return "/admin/educations/update/" + row.id;
                },
                delete: function(row) {
                    return "/education/" + row.id;
                }
            }
        })
    </script>
@endsection

{{-- @section('script')
<script>
var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        select: true,
        dom: 'Bflrtip',
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/education/datatables'),
            "type": "get",
            "data": function(d) {
                return $.extend({}, d, {
                    "extra_search": $('#extra').val()
                });
            }
        },

        columns: [{
                data: "id",
                name: "id"
            },
            {
                data: "institution",
                name: "institution",
            },
            {
                data: "place",
                name: "place",
            },
            {
                data: "education_start_date",
                name: "education_start_date",
            },
            {
                data: "education_end_date",
                name: "education_end_date",
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    return "<a href='' data-id='" + full.id + "'  class='delete btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>" +
                        "&nbsp<a href=''  data-id='" + full.id + "' class='update btn btn-primary btn-sm'><i class='fa fa-pencil'></i></a>"
                }
            },
        ],
    });
</script>
@endsection --}}