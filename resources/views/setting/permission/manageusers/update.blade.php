@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <form action="  " method="post" id="form-user-update">
                <input type="hidden" value="{{ $updateUsers->id }}" name="id">
                <label>Name</label>
                <div class="form-group">
                    <input name="name" placeholder="Name" type="text" class="form-control" value="{{ $updateUsers->name }}" />
                </div>

                <label>E-Mail</label>
                <div class="form-group">
                    <input name="email" placeholder="email" type="email" class="form-control" value="{{ $updateUsers->email }}" readonly/>
                </div>

                <label>Phone</label>
                <div class="form-group">
                    <input name="phone" placeholder="phone" type="text" class="form-control" value="{{ $updateUsers->phone }}"/>
                </div>

                <label>Status</label>
                <input name="status" class="tgl tgl-skewed" id="cb3" type="checkbox"  value="{{ $updateUsers->status }}" {{ $updateUsers->status == 1 ? 'checked' : '' }}/>
                <label class="tgl-btn" data-tg-off="UNACTIVE" data-tg-on="ACTIVE" for="cb3"></label>

                <label>Role</label>
                <div class="form-group">
                    <select class="form-control" name="role_id[]" id="role" style="width:100%;" multiple>
                        @foreach ($showRole as $role)
                            <option value="{{ $role->id }}" {{ ($updateUsers->roles->where('id', $role->id)->count() == 1) ? "selected" : null }}>{{ $role->name }}</option>
                        @endforeach                              
                    </select>
                </div>
                
                <div class="position-relative">
                    <button class="btn btn-success btn-sm" id="submit"><i class="fa fa-plus"></i> Save</button>
                </div>

            </form>        
        </div>
    </div>
</div>
<style>

.tgl {
  display: none;
}
.tgl, .tgl:after, .tgl:before, .tgl *, .tgl *:after, .tgl *:before, .tgl + .tgl-btn {
  box-sizing: border-box;
}
.tgl::-moz-selection, .tgl:after::-moz-selection, .tgl:before::-moz-selection, .tgl *::-moz-selection, .tgl *:after::-moz-selection, .tgl *:before::-moz-selection, .tgl + .tgl-btn::-moz-selection {
  background: none;
}
.tgl::selection, .tgl:after::selection, .tgl:before::selection, .tgl *::selection, .tgl *:after::selection, .tgl *:before::selection, .tgl + .tgl-btn::selection {
  background: none;
}
.tgl + .tgl-btn {
  outline: 0;
  display: block;
  width: 7em;
  height: 2em;
  position: relative;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
          user-select: none;
}
.tgl + .tgl-btn:after, .tgl + .tgl-btn:before {
  position: relative;
  display: block;
  content: "";
  width: 50%;
  height: 100%;
}
.tgl + .tgl-btn:after {
  left: 0;
}
.tgl + .tgl-btn:before {
  display: none;
}
.tgl:checked + .tgl-btn:after {
  left: 50%;
}


.tgl-skewed + .tgl-btn {
  overflow: hidden;
  -webkit-transform: skew(-10deg);
          transform: skew(-10deg);
  -webkit-backface-visibility: hidden;
          backface-visibility: hidden;
  -webkit-transition: all .2s ease;
  transition: all .2s ease;
  font-family: sans-serif;
  background: #888;
}
.tgl-skewed + .tgl-btn:after, .tgl-skewed + .tgl-btn:before {
  -webkit-transform: skew(10deg);
          transform: skew(10deg);
  display: inline-block;
  -webkit-transition: all .2s ease;
  transition: all .2s ease;
  width: 100%;
  text-align: center;
  position: absolute;
  line-height: 2em;
  font-weight: bold;
  color: #fff;
  text-shadow: 0 1px 0 rgba(0, 0, 0, 0.4);
}
.tgl-skewed + .tgl-btn:after {
  left: 100%;
  content: attr(data-tg-on);
}
.tgl-skewed + .tgl-btn:before {
  left: 0;
  content: attr(data-tg-off);
}
.tgl-skewed + .tgl-btn:active {
  background: #888;
}
.tgl-skewed + .tgl-btn:active:before {
  left: -10%;
}
.tgl-skewed:checked + .tgl-btn {
  background: #1976d2;
}
.tgl-skewed:checked + .tgl-btn:before {
  left: -100%;
}
.tgl-skewed:checked + .tgl-btn:after {
  left: 0;
}
.tgl-skewed:checked + .tgl-btn:active:after {
  left: 10%;
}
</style>
@endsection
@section('script')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script>
        globalCRUD
            .select2("#role", '/role/select2')
            .select2("#status", '/user_status/select2')

        $('#form-user-update').submit(function(e) {

            globalCRUD.handleSubmit($(this))
                .updateTo(function(formData) {
                    return '/user/' + formData.id;
                })
                .redirectTo(function(resp) {
                    return '/admin/user/show';
                })

            e.preventDefault();
        });
    </script>
@endsection
