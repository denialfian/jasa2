@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('/admin/role/create') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">Add Role</a>
            </div>
        </div>
        <div class="card-body">
            <table id="example" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Guard Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


@endsection
@section('script')
<script>
    globalCRUD.datatables({
        url: '/role/datatables',
        selector: '#example',
        columnsField: ['DT_RowIndex', 'name', 'guard_name'],
        actionLink: {
            update: function(row) {
                return "/admin/role/update/" + row.id;
            },
            delete: function(row) {
                return "/role/" + row.id;
            }
        }
    })
</script>
@endsection
