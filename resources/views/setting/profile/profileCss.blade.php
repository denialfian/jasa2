<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
<style>
    .inputs {
        position: absolute;
        left: -9999px;
    }

    #profile-image123 {
        cursor: pointer;
        width: 60%;
        border-radius: 80%;
        border: 2px solid #03b1ce;
    }

    .btn_login {
        background-color: #26C6DA;
        border: none;
        padding: 10px;
        width: 200px;
        border-radius: 3px;
        box-shadow: 1px 5px 20px -5px rgba(0, 0, 0, 0.4);
        color: #fff;
        margin-top: 10px;
        cursor: pointer;
    }

    .circle {
        border-radius: 100% !important;
        overflow: hidden;
        width: 10%;
        height: 70%;
        border: 8px solid rgba(255, 255, 255, 0.7);
        position: absolute;
        top: 10%;
        margin-left: -30%;
    }

    .p-image {
        position: absolute;
        top: 85%;
        right: 30%;
        color: #666666;
        transition: all .3s cubic-bezier(.175, .885, .32, 1.275);
    }

    .circle-2 img {
        width:300px;
        height:300px;
        border-radius: 50%;
        cursor:pointer;
        filter: url(filters.svg#grayscale); /* Firefox 3.5+ */
            filter: gray; /* IE6-9 */
            -webkit-filter: grayscale(1); /* Google Chrome & Safari 6+ */
    }
</style>