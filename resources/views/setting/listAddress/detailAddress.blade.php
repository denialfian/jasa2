@extends('admin.home')
@section('content')
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header-tab card-header">
                        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                            {{ $title }}
                        </div>
                        <div class="btn-actions-pane-right text-capitalize">
                            <a href="{{ url('/admin/address/show') }}" class="mb-2 mr-2 btn btn-primary" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
                        </div>
                    </div>
                    <div class="table-responsive"><br>
                        <div class="container">
                            <div class="card-body">
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-2 col-form-label">Phone Number 1</label>
                                    <div class="col-sm-10">
                                        <input name="phone1" value="{{ $detailAddress->phone1 }}" id="examplename" placeholder="+628xxx" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-2 col-form-label">Phone Number 2</label>
                                    <div class="col-sm-10">
                                        <input name="phone2" value="{{ $detailAddress->phone2 }}" id="examplename" placeholder="+628xxx" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-2 col-form-label">Type</label>
                                    <div class="col-sm-10">
                                        <input name="type" id="examplename" placeholder="Rumah/Apartement Dll" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-2 col-form-label">Country Name</label>
                                    <div class="col-sm-10">
                                        <input name="ms_city_id" value="{{ $detailAddress->city->province->country->name }}" id="examplename" type="text" class="form-control" disabled>
                                    </div>
                                </div>

                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-2 col-form-label">Province Name</label>
                                    <div class="col-sm-10">
                                        <input name="ms_city_id" value="{{ $detailAddress->city->province->name }}" id="examplename" type="text" class="form-control" disabled>
                                    </div>
                                </div>

                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-2 col-form-label">City Name</label>
                                    <div class="col-sm-10">
                                        <input name="ms_city_id" value="{{ $detailAddress->city->name }}" id="examplename" type="text" class="form-control" disabled>
                                    </div>
                                </div>

                                    <div class="position-relative row form-group" >
                                        <label for="exampleEmail" class="col-sm-2 col-form-label" >Distric Name</label>
                                        <div class="col-sm-10">
                                            <input name="ms_district_id" value="{{ $detailAddress->district->name }}" id="examplename" type="text" class="form-control" disabled>
                                        </div>
                                    </div>

                                    <div class="position-relative row form-group" >
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Village Name</label>
                                        <div class="col-sm-10">
                                            <input name="ms_village_id" value="{{ $detailAddress->village->name }}" id="examplename" type="text" class="form-control" disabled>
                                            {{-- <select class="js-example-basic-single" id="vilage" name="ms_village_id" style="width:100%">
                                                <option value="{{ $detailAddress->village->id }}" selected>{{ $detailAddress->village->name }}</option>
                                            </select> --}}
                                        </div>
                                    </div>

                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Zip-Code</label>
                                        <div class="col-sm-10">
                                            <input name="ms_zipcode_id" value="{{ $detailAddress->zipcode->zip_no }}" placeholder="06XXX" id="zipcode-text" type="text" class="form-control" disabled>
                                        </div>
                                    </div>

                                    {{-- <input name="ms_zipcode_id" id="zipcode-id" placeholder="06XXX" type="hidden" class="form-control" disabled> --}}

                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Address</label>
                                        <div class="col-sm-10">
                                            <textarea name="address" class="form-control" id="exampleFormControlTextarea1" rows="3" disabled>{{ $detailAddress->address }}</textarea>
                                        </div>
                                    </div><br><br>
                            </div>
                        </div>
                    </div>
                    <div class="d-block text-center card-footer">

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.template._mainScript')
    @include('admin.script.profile._addressScript')
@endsection
