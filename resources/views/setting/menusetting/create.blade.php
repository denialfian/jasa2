@extends('admin.home')
@section('content')
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-header-tab card-header">
                        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                            {{ $title }}
                        </div>
                        <div class="btn-actions-pane-right text-capitalize">
                            <a href="{{ url('/show-menu') }}" class="mb-2 mr-2 btn btn-primary" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
                        </div>
                    </div>
                    <div class="table-responsive"><br>
                        <div class="container">
                            <div class="card-body">
                                {{-- <form action="" method="post"> --}}
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input name="name" id="examplename" placeholder="Dashboard" type="text" class="form-control">
                                        </div>
                                    </div>

                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Url / Destination</label>
                                        <div class="col-sm-10">
                                            <input name="url" id="examplename" placeholder="http://" type="text" class="form-control">
                                        </div>
                                    </div>

                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Display Order</label>
                                        <div class="col-sm-10">
                                            <input name="display_order" id="examplename" placeholder="1" type="text" class="form-control">
                                        </div>
                                    </div>

                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Icon</label>
                                        <div class="col-sm-10">
                                            <input name="icon" id="examplename" placeholder="fa fa-icon" type="text" class="form-control">
                                        </div>
                                    </div>

                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Permission</label>
                                        <div class="col-sm-10">
                                            <select class="js-example-basic-single" name="permission_id " style="width:100%">
                                                <option value="" disabled-selected>Permission Name</option>
                                                    @foreach ($showPermission as $item)
                                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                    @endforeach
                                              </select>
                                        </div>
                                    </div>

                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-2 col-form-label">Parent ID</label>
                                        <div class="col-sm-10">
                                            <select class="js-example-basic-single" name="parent_id" style="width:100%">
                                                <option value="" disabled-selected>Parent ID</option>
                                                
                                              </select>
                                        </div>
                                    </div>


                                    <div class="position-relative row form-check">
                                        <div class="col-sm-10 offset-sm-2">
                                            <button class="btn btn-success" id="save"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                                        </div>
                                    </div>
                                {{-- </form> --}}
                            </div>
                        </div>
                    </div>
                    <div class="d-block text-center card-footer">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.template._mainScript')
    @include('admin.script.menu._menuSettingScript')
@endsection
