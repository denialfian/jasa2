<tr>
    <td class="accordion-body collapse" id="{{ $category->id }}-cores">
        <table class="table">
            <tbody>
                @foreach($categories as $category)
                <tr>
                    <td class="accordion-toggle" data-toggle="collapse" data-target="#{{ $category->id }}-cores">-</td>
                    <td>{{ $category->name }}</td>
                    @if($category->childs->count() > 0)
                        @include('test2', [
                            'categories' => $category->childs,
                            'id' => $category->id
                        ])
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
    </td>
</tr>