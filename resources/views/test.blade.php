<!DOCTYPE html>
<html>

<head>
    <title>
        Async file upload with jQuery
    </title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
    </script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>

<body>
    <table class="table">
        <thead>
            <tr>
                <th>Account ID</th>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            @foreach($categories as $category)
            <tr class="accordion-toggle" data-toggle="collapse" data-target="#{{ $category->id }}-cores">
                <td>12345</td>
                <td>{{ $category->name }}</td>
            </tr>
                @if($category->childs->count() > 0)
                    @include('test2', [
                        'categories' => $category->childs,
                        'id' => $category->id
                    ])
                @endif
            @endforeach
        </tbody>
    </table>

</body>

</html>