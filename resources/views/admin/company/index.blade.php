@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <!-- <button class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Add New Grade</button> -->
            </div>
        </div>
        <div class="card-body">
            <table id="table-grade" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>PIC</th>
                        <th>Npwp</th>
                        <th>Nomor Rekening</th>
                        <th>Nama Bank</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Add New</button>
        </div>
    </div>
</div>
<form id="form-grade">
    <div class="modal fade" id="modal-grade" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Company</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleEmail">Nama Company</label>
                                <input name="name" placeholder="name" id="name" type="text" class="form-control" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleEmail">NPWP</label>
                                <input name="npwp" placeholder="npwp" id="npwp" type="number" class="form-control" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleEmail">Email</label>
                                <input name="email" placeholder="email" id="email" type="text" class="form-control" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleEmail">Nomor Rekening</label>
                                <input name="nomor_rekening" placeholder="nomor Rekening" id="nomor Rekening" type="number" class="form-control" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleEmail">Nama Bank</label>
                                <input name="nama_bank" placeholder="Nama Bank" id="Nama Bank" type="text" class="form-control" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleEmail">Nama Pic</label>
                                <input name="nama_pic" placeholder="Nama Pic" id="Nama Pic" type="text" class="form-control" >
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleEmail">Nomor Telepon</label>
                                <input name="phone" placeholder="Nomor Telepon" id="phone" type="text" class="form-control" >
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleEmail">Alamat</label>
                                <textarea name="alamat" id="" cols="30" rows="3" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
@include('admin.script.master._imageUploadScript')
<script>
    $('#phone').mask('628 0000 0000 00');
    globalCRUD.datatables({
        orderBy: [1, 'asc'],
        url: '/company/datatables',
        selector: '#table-grade',
        columnsField: ['DT_RowIndex', 'name', 'email', 'nama_pic', 'npwp', 'nomor_rekening', 'nama_bank'],
        modalSelector: "#modal-grade",
        modalButtonSelector: ".add-grade",
        modalFormSelector: "#form-grade",
        actionLink: {
            store: function() {
                return "/company";
            },
            update: function(row) {
                return "/company/" + row.id;
            },
            delete: function(row) {
                return "/company/" + row.id;
            },
            detail: function(row) {
                return "/admin/company/list_outlet-by-company/"+ row.id;
            }
        }
    })
</script>
@endsection
