{{-- preview image --}}
<script>
    $(".imgAdd").click(function(){
        $(this).closest(".row").find('.imgAdd').before('<div class="col-sm-2 imgUp"><div class="imagePreview"></div><label class="btn btn-primary">Upload<input type="file" class="uploadFile img" value="Upload Photo" style="width:0px;height:0px;overflow:hidden;"></label><i class="fa fa-times del"></i></div>');
    });
    $(document).on("click", "i.del" , function() {
        $(this).parent().remove();
    });
    $(function() {
        $(document).on("change",".uploadFile", function()
        {
            
            var uploadFile = $(this);
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
    
            if (/^image/.test( files[0].type)){ // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file
    
                reader.onloadend = function(){ // set image data as background of div
                    //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                    uploadFile.closest(".imgUp").find('.imagePreview').css("background-image", "url("+this.result+")");
                    $('.imagePreview1').hide();
                }
            }
        
        });
    });
</script>

<script>
    // var webSettingObj = {
    //     //insert
    //     insert: function(data){
    //         $.ajax({
    //             url : Helper.apiUrl('/web_setting'),
    //             data : data,
    //             type : 'post',
    //             contentType: false,
    //             processData: false,
    //             success: function(resp) {
    //                 webSettingObj.successHandle(resp);
    //             },
    //             error: function(xhr, status, error) {
    //                 Helper.errorMsgRequest(xhr, status, error);
    //             },
    //         });
    //     },

    //     // hadle ketika response sukses
    //     successHandle: function(resp) {
    //         // send notif
    //         Helper.successNotif(resp.msg);
    //     },
    // }

    // submit data
    $("#save").click(function() {
        // alert('asd');
        var fd = new FormData();
        // var id = $('input[name="id"]').val();
        var files = $('.upload_logo')[0].files[0];
        if(files){
            fd.append('logo', files);
        }
        // fd.append('id', $('input[name="id"]').val());
        fd.append('title_setting', $('input[name="title_setting"]').val());
        fd.append('metta_setting', $('input[name="metta_setting"]').val());
        fd.append('image_url', $('input[name="image_url"]').val());
        $.ajax({
            url:Helper.apiUrl('/admin/websetting/create'),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                var getUrlImage = response.logo_src;
                $('#getLogo').val(getUrlImage)
                window.location.href = Helper.redirectUrl('/admin/web-setting');
                console.log(getUrlImage)
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                    })
                }
            },
        });
    });
</script>
