@extends('admin.home')
@section('content')
<style>
    .btn_login { 
        background-color: #26C6DA;
        border: none;
        padding: 10px;
        width: 200px;
        border-radius:3px;
        box-shadow: 1px 5px 20px -5px rgba(0,0,0,0.4);
        color: #fff;
        margin-top: 10px;
        cursor: pointer;
    }
</style>


    <div class="col-md-12 card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Web Setting Management
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                {{-- <a href="{{ url('/create-vendors') }}" class="mb-2 mr-2 btn btn-primary btn-sm" style="float:right">Add Vendor</a> --}}
            </div>
        </div>
    
        <div class="card-body">
            <div class="card-title">
                <h3 style="color:#00b1b1;" align="center"><strong>   
            </div><br>
            {{-- <form id="form-web-setting"> --}}
                <input type="hidden" name="id" id="id">

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-3 col-form-label">Title Setting</label>
                    <div class="col-sm-9">
                        <input name="title_setting" id="examplename" placeholder="Title This Application" type="text" class="form-control" value="{{ !empty($showData->title_setting) ? $showData->title_setting : '' }}">
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-3 col-form-label">Meta Setting</label>
                    <div class="col-sm-9">
                        <input name="metta_setting" id="examplename" placeholder="Metta This Application" type="text" class="form-control" value="{{ !empty($showData->metta_setting) ? $showData->metta_setting : '' }}">
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-3 col-form-label">Logo</label>
                    <div class="col-md-9">
                        <input name="image_url" id="getLogo" placeholder="URL Image" type="text" class="form-control" value="{{ !empty($showData->image_url) ? $showData->image_url : '' }}">
                    </div>
                    
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-3 imgUp">
                        @if (!empty($showData->logo))
                            <div class="imagePreview" style="background-image: url({{  url('/admin/get/logo/from-storage/'.$showData->logo) }});">
                                {{-- <img src="{{  url('/admin/get/logo/from-storage/'.$showData->logo) }}" alt="Logo"> --}}
                            </div>
                        @else
                            <div class="imagePreview"></div>
                        @endif
                        <label class="btn btn-primary">
                            Upload
                            <input type="file" class="uploadFile img upload_logo" name="logo" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;">
                        </label>
                    </div>
                </div>
        
                <button type="submit" class="btn btn-primary waves-effect" id="save">Save</button>
            {{-- </form> --}}
        </div>
    </div>


<style>
    .imagePreview {
        width: 100%;
        height: 80px;
        background-position: center center;
        background:url(http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg);
        background-color:#fff;
        background-size: cover;
        background-repeat:no-repeat;
        display: inline-block;
        box-shadow:0px -3px 6px 2px rgba(0,0,0,0.2);
    }
    .btn-primary{
        display:block;
        border-radius:0px;
        box-shadow:0px 4px 6px 2px rgba(0,0,0,0.2);
        margin-top:-5px;
    }
    .imgUp{
        margin-bottom:15px;
    }
    .del{
        position:absolute;
        top:0px;
        right:15px;
        width:30px;
        height:30px;
        text-align:center;
        line-height:30px;
        background-color:rgba(255,255,255,0.6);
        cursor:pointer;
    }
    .imgAdd{
        width:30px;
        height:30px;
        border-radius:50%;
        background-color:#4bd7ef;
        color:#fff;
        box-shadow:0px 0px 2px 1px rgba(0,0,0,0.2);
        text-align:center;
        line-height:30px;
        margin-top:0px;
        cursor:pointer;
        font-size:15px;
    }
</style>

@endsection
@section('script')
@include('admin.setting.webSetting.js')
@endsection