@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-marital" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Marital</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
            <button class="btn btn-primary btn-sm add-marital"><i class="fa fa-plus"></i> ADD MARITAL</button>
        </div>
    </div>
</div>
@include('admin.setting.maritals.modelMaritals')

@endsection

@section('script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script>
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })

    globalCRUD.datatables({
        url: '/marital/datatables',
        selector: '#table-marital',
        columnsField: ['DT_RowIndex', 'status'],
        modalSelector: "#modal-marital",
        modalButtonSelector: ".add-marital",
        modalFormSelector: "#form-marital",
        actionLink: {
            store: function() {
                return "/marital";
            },
            update: function(row) {
                return "/marital/" + row.id;
            },
            delete: function(row) {
                return "/marital/" + row.id;
            },
        }
    })
</script>
@endsection
