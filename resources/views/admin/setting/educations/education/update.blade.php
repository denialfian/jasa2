@extends('admin.home')
@section('content')
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<div class="col-md-12 card">
    <div class="card-header-tab card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            {{ $title }}
        </div>
        <div class="btn-actions-pane-right text-capitalize">
            <a href="{{ url('/admin/educations/show') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
        </div>
    </div>
    <div class="card-body">
        <form action="" method="post" id="form-update-education">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <input type="hidden" name="id" value="{{ $viewEducation->id }}" id="examplename" class="form-control">
                        <div class="col-md-6">
                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label align-right">Institution</label>
                                <div class="col-sm-10">
                                    <input name="institution" value=" {{ $viewEducation->institution }} " id="institution" placeholder="Institution" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label">Start Date</label>
                                <div class="col-sm-10">
                                    {{-- <input id="datepicker1" class="form-control" value=" {{ $viewEducation->education_start_date }} " type="date" name="education_start_date" width="100%" /> --}}
                                    <input type="date" class="form-control" name="education_start_date" width="100%" value="{{ $viewEducation->education_start_date }}" />
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label">Type</label>
                                <div class="col-sm-10">
                                    <input name="type" value=" {{ $viewEducation->type }} " id="type" placeholder="Type" type="text" class="form-control">
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label">Place</label>
                                <div class="col-sm-10">
                                    <input name="place" value=" {{ $viewEducation->place }} " id="place" placeholder="Place" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label">End Date</label>
                                <div class="col-sm-10">
                                    {{-- <input id="datepicker2" class="form-control" value=" {{ $viewEducation->education_end_date }} " type="date" name="education_end_date" width="100%" /> --}}
                                    <input type="date" class="form-control" name="education_end_date" width="100%" value="{{ $viewEducation->education_end_date }}" />
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="exampleEmail" class="col-sm-2 col-form-label">Major</label>
                                <div class="col-sm-10">
                                    <input name="major" value=" {{ $viewEducation->major }} " id="major" placeholder="Major" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="position-relative row form-check">
                                <div class="col-sm-10 offset-sm-2">
                                    <button class="btn btn-success" id="save"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')
    <script>
        $('#form-update-education').submit(function(e) {

            globalCRUD.handleSubmit($(this))
                .updateTo(function(formData) {
                    return '/education/' + formData.id;
                })
                .backTo(function(resp) {
                    return "/admin/educations/show";
                })

            e.preventDefault();
        });
    </script>
@endsection