<form id="form-address-type">
    <div class="modal fade" id="createType" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            {{-- <form method="post" action="" enctype="multipart/form-data"> --}}
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create Address Type</h5>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id" value="">
                        <div class="position-relative form-group">
                            <label for="name" class="">Name</label>
                            <input name="name" id="name" placeholder="Rumah/Apartemen Dll.." type="text" class="form-control form-control-sm">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="save">Save</button>
                    </div>
                </div>
            {{-- </form> --}}
        </div>
    </div>
</form>