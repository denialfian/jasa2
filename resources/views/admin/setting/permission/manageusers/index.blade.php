@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="main-card mb-3 card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                List Users
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <!-- <a href="{{ url('/admin/permission') }}" class="btn btn-sm btn-danger">Manage Permission</a>
                <a href="{{ url('/admin/role/show') }}" class="btn btn-sm btn-danger">Manage Role</a> -->

            </div>
        </div>
        <div class="card-body">
            <table id="form-user-table" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Role</th>
                        <th>Active</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <a href="{{ url('/admin/user/create') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Users</a>
        </div>
    </div>
</div>
<form id="form-change_password">
    <div class="modal fade" data-backdrop="false" id="modal-change_password" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Change Password
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="user_id" value="">
                    <label>New Password</label>
                    <div class="form-group">
                        <input name="new_password" placeholder="new password" type="password" class="form-control" required />
                    </div>
                    <label>New Password Confim</label>
                    <div class="form-group">
                        <input id="password-input" name="new_password_confirmation" placeholder="new password confirmation" type="password" class="form-control" required />
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SAVE
                    </button>
                    <button class="btn waves-effect" data-dismiss="modal" type="button">
                        CLOSE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@section('script')
<script>
    var dt = globalCRUD.datatables({
        url: '/user/datatables',
        selector: '#form-user-table',
        columnsField: [
            {
                data: 'user.full_name',
                name: 'user.full_name',
                orderable: false,
                searchable: false,
                render: function(data, type, row) {
                    url = Helper.url('/admin/user/detail/' + row.id);
                    return '<a href="' + url + '">' + row.name + '</a>';
                }
            },
            'email',
            'phone',
            {
                data: 'id',
                name: 'id',
                orderable: false,
                render: function(data, type, row) {
                    text = '';
                    _.each(row.roles, function(i) {
                        text += i.name + ' '
                    })

                    return text;
                }
            },
            {
                data: 'id',
                name: 'id',
                orderable: false,
                render: function(data, type, row) {
                    if (row.name == 'admin') {
                        return '-';
                    }
                    if (row.status != 1) {
                        return "<button type='button' class='btn btn-sm btn-user-status btn-drak' data-id='" + row.id + "'><i style='font-size: 24px;' class='fa fa-square'></i></button>";
                    } else {
                        return "<button type='button' class='btn btn-sm btn-user-status btn-drak' data-id='" + row.id + "'><i class='fa fa-check-square' style='font-size: 24px;'></i></button>";
                    }
                }
            }
        ],
        actionLink: {
            only_icon: false,
            update: function(row) {
                return '/admin/user/update/' + row.id;
            },
            delete: function(row) {
                return '/user/' + row.id;
            },
            render: function(data, type, full, actionData) {
                btn_hapus = '<button class="btn btn-danger btn-sm btn-hapus-user" data-id="' + full.id + '"><i class="fa fa-trash"></i></button>';
                btn_cp = '<button class="btn btn-success btn-sm btn-change_password" data-id="' + full.id + '"><i class="fa fa-pencil"></i> Change Password</button>';
                if (full.name == 'admin') {
                    return btn_cp + " <a class='btn btn-info btn-sm' href='/admin/user/detail/" + full.id + "'><i class='fa fa-arrow-right'></i> Detail</a>";
                } else {
                    return btn_cp + ' ' + actionData.edit_btn + " " + btn_hapus;
                }
            }
        },
    })
</script>

<script>
    $(document).on('click', '.btn-change_password', function() {
        $('[name="user_id"]').val($(this).attr('data-id'))
        $('#modal-change_password').modal('show');
    })

    // ganti status
    $(document)
        .on('click', '.btn-user-status', function() {
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.put('/user/change_status/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('ok');
                        // reload table
                        dt.table.reloadTable();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })

        // ganti hapus
    $(document)
        .on('click', '.btn-hapus-user', function() {
            id = $(this).attr('data-id');
            bootbox.prompt("Warning!, Semua data yang berhunbungan dengan User ini akan di hapus termasuk data transaksi dan lain-lain, ketik 'DELETE' dibawah ini untuk konfirmasi", function(result){ 
                if (result === 'DELETE') {
                    Axios.delete('/user/' + id)
                        .then(function(response) {
                            // send notif
                            Helper.successNotif('user has been deleted');
                            // reload table
                            dt.table.reloadTable();
                        })
                        .catch(function(error) {
                            console.log(error)
                            Helper.handleErrorResponse(error)
                        });
                }
            });
        })

    $('#form-change_password').submit(function(e) {
        data = {
            id: $('[name="user_id"]').val(),
            new_password: $('[name="new_password"]').val(),
            new_password_confirmation: $('[name="new_password_confirmation"]').val(),
        };

        Helper.loadingStart();
        // post data
        Axios.put(Helper.apiUrl('/user/change_password/' + data.id), data)
            .then(function(response) {
                Helper.successNotif('Success Change Password');
                $('#modal-change_password').modal('hide');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>
@endsection