@extends('admin.home')
@section('content')
<input type="hidden" id="hidden-user-id" value="{{ $user->id }}">
<div class="col-md-4">
    <div class="card">
        <div class="card-header-tab card-header text-white header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                PROFILE
            </div>
        </div>
        <div class="card-body">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic text-center">
                    <div class="avatar-upload">
                        <div class="avatar-edit">
                            <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                            <label for="imageUpload"></label>
                        </div>
                        <div class="avatar-preview">
                            <div id="imagePreview" style="background-image: url({{ $user->avatar }});"></div>
                        </div>
                    </div>
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        {{ $user->full_name }}<br/>
                        @if($user->badge_id != null)
                            <div class="mb-2 mr-2 badge badge-pill badge-{{ $user->badge->color }}">{{ $user->badge->name }}</div>
                        @endif
                    </div>
                    <div class="profile-usertitle-job">
                        @foreach ($user->roles as $role)
                          <label class="label-danger">{{ $role->name }} </label>
                        @endforeach
                    </div>
                    <div class="profile-userbuttons">
                        @if($user->hasRole('Technician'))
                            @if($user->technician != null)
                                <a href="{{ url('/admin/technician/edit/'. $user->technician->id) }}" class="btn btn-success btn-sm" ><i class="fa fa-user"></i> View Technician Info</a>
                            @endif
                        @endif
                        <button class="btn btn-warning btn-white btn-sm btn-change_password" ><i class="fa fa-lock"></i> Change Password</button>
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->

                <!-- END SIDEBAR BUTTONS -->
            </div>
        </div>
    </div>
</div>
<div class="col-md-8">
    <div class="card">
        <div class="card-header-tab card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                DETAIL USER
            </div>
        </div>
        <div class="card-body">
            <table class="table table-user-information">
                <tbody>
                    <tr>
                        <th>
                            Email
                        </th>
                        <td>
                            {{ $user->email }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Phone
                        </th>
                        <td>
                            {{ $user->phone }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card" style="margin-top: 10px;">
        <div class="card-body header-border">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-user-information">
                        <tbody>
                            <tr>
                                <th>
                                    First Name
                                </th>
                                <td>
                                    {{ $user->info == null ? '' : $user->info->first_name }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Last Name
                                </th>
                                <td>
                                    {{ $user->info == null ? '' : $user->info->last_name }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Date Of Birth
                                </th>
                                <td>
                                    {{ $user->info == null ? '' : $user->info->place_of_birth }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Date Of Birth
                                </th>
                                <td>
                                    @if($user->info != null)
                                        @if($user->info->date_of_birth != null)
                                            {{ $user->info->date_of_birth->format('d F Y') }}
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Religion
                                </th>
                                <td>
                                    @if ($user->info != null)
                                        @if ($user->info->religion != null)
                                            {{ $user->info->religion->name }}
                                        @endif
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Marital
                                </th>
                                <td>
                                    @if ($user->info != null)
                                        @if ($user->info->marital != null)
                                            {{ $user->info->marital->status }}
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="profile-userbuttons">
                        <a href="{{ url('/admin/user/info/'. $user->id) }}" class="btn btn-success btn-sm" ><i class="fa fa-user"></i> Edit Info</a>
                    </div>
                </div>
                <div class="col-md-6">
                    @if ($user->hasRole('Technician'))
                    <form id="teknisi-rek-form">
                        @if(!empty($user->technician))
                            <input type="hidden" name="id" value="{{ $user->technician->id }}" />
                        @endif
                        <label>Bank Name</label>
                        <div class="form-group">
                            @if(!empty($user->technician))
                                <input
                                class="form-control"
                                id="rekening_bank_name"
                                name="rekening_bank_name"
                                placeholder="bank name"
                                type="text"
                                value="{{ $user->technician->rekening_bank_name }}" required/>
                            @else
                                <input
                                    class="form-control"
                                    id="rekening_bank_name"
                                    name="rekening_bank_name"
                                    placeholder="bank name"
                                    type="text"
                                    value="" required/>
                            @endif

                        </div>

                        <label>Rekening Number</label>
                        <div class="form-group">

                            @if(!empty($user->technician))
                                <input
                                    class="form-control"
                                    id="rekening_number"
                                    name="rekening_number"
                                    placeholder="Rekening number"
                                    type="text"
                                    value="{{ $user->technician->rekening_number }}" required/>
                            @else
                                <input
                                    class="form-control"
                                    id="rekening_number"
                                    name="rekening_number"
                                    placeholder="Rekening number"
                                    type="text"
                                    value="" required/>
                            @endif

                        </div>

                        <label>Bank Account Under the Name</label>
                        <div class="form-group">
                            @if(!empty($user->technician))
                                <input
                                    class="form-control"
                                    id="rekening_name"
                                    name="rekening_name"
                                    placeholder="Account name"
                                    type="text"
                                    value="{{ $user->technician->rekening_name }}" required/>
                            @else
                                <input
                                    class="form-control"
                                    id="rekening_name"
                                    name="rekening_name"
                                    placeholder="Account name"
                                    type="text"
                                    value="" required/>
                            @endif

                        </div>

                        <button class="btn btn-white bg-drak btn-sm waves-effect" type="submit"><i class="fa fa-pencil"></i> Save Rekening Info</button>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header-tab card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                ADDRESS USER
            </div>
        </div>
        <div class="card-body">
        	<table id="address-table" class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Type</th>
                        <th>City</th>
                        <th>Full Address</th>
                        <th>Main</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>

            <a href="{{ url('/admin/user/address/' . $user->id . '/create') }}" class="mb-2 mr-2 btn btn-primary">
                <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add New Address
            </a>
        </div>
    </div>
</div>

<form id="form-change_password">
    <div class="modal fade" data-backdrop="false" id="modal-change_password" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Change Password
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="{{ $user->id }}">
                    <label>New Password</label>
                    <div class="form-group">
                        <input name="new_password" placeholder="new password" type="password" class="form-control" required/>
                    </div>
                    <label>New Password Confim</label>
                    <div class="form-group">
                        <input name="new_password_confirmation" placeholder="new password confirmation" type="password" class="form-control" required/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SAVE
                    </button>
                    <button class="btn waves-effect" data-dismiss="modal" type="button">
                        CLOSE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection


@section('script')
<script>
    var dt = globalCRUD.datatables({
        url: '/user_address/datatables_user/' + $('#hidden-user-id').val(),
        selector: '#address-table',
        columnsField: [
            'DT_RowIndex',
            'types.name',
            {
                data:'id',
                name:'id',
                orderable: false,
                searchable: false,
                render: function(data, type, row){
                    return row.city == null ? '-' : row.city.name;
                }
            },
            {
                data:'id',
                name:'id',
                orderable: false,
                searchable: false,
                render: function(data, type, row){
                    return row.address;
                }
            },
            {
                data:'id',
                name:'id',
                orderable: false,
                searchable: false,
                render: function(data, type, row){
                    if (row.is_main == 0) {
                        return "<button type='button' class='btn btn-main-address btn-drak' data-id='"+row.id+"'><i style='font-size: 24px;' class='fa fa-square'></i></button>";
                    }else{
                        return "<button type='button' class='btn btn-main-address btn-drak' data-id='"+row.id+"'><i class='fa fa-check-square' style='font-size: 24px;'></i></button>";
                    }
                }
            }
        ],
        actionLink: {
            only_icon: false,
            update: function(row) {
                return '/admin/user/address/'+row.id+'/edit/';
            },
            delete: function(row) {
                return '/user_address/' + row.id;
            }
        }
    })

    // set main address
    $(document)
        .on('click', '.btn-main-address', function(){
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.put('/user/main_address/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif(response.data.msg);
                        // reload table
                        dt.table.reloadTable();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })

   	function readURL(input) {
	    var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
	}

	$("#imageUpload").change(function() {
		input = this;

		if (input.files && input.files[0]) {
			Helper.loadingStart();
			user_id = $("#hidden-user-id").val();
			var formData = new FormData();
			formData.append("image", input.files[0]);
			formData.append("user_id", user_id);

			console.log(formData);
	     	Axios
				.post(Helper.apiUrl('/user/profile/image/' + user_id), formData, {
				    headers: {
				      'Content-Type': 'multipart/form-data'
				    }
				})
				.then(function(response) {
					Helper.successNotif(response.data.msg);
	                readURL(input);
	            })
	            .catch(function(error) {
	                Helper.handleErrorResponse(error)
	            });
	    }
	});

    $('.btn-change_password').click(function(){
        $('#modal-change_password').modal('show');
    })

    $('#form-change_password').submit(function(e){
        data = {
            id: $('[name="id"]').val(),
            new_password: $('[name="new_password"]').val(),
            new_password_confirmation: $('[name="new_password_confirmation"]').val(),
        };

        Helper.loadingStart();
         // post data
        Axios.put(Helper.apiUrl('/user/change_password/' + data.id), data)
            .then(function(response) {
                Helper.successNotif('Success Change Password');
                $('#modal-change_password').modal('hide');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })

    $('#teknisi-rek-form').submit(function(e){
        data = Helper.serializeForm($(this));
        Helper.loadingStart();
        Axios.put('/technician/' + data.id + '/rek_info/', data)
            .then(function(response) {
                // send notif
                Helper.successNotif(response.data.msg).redirectTo();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
        e.preventDefault()
    })
</script>
@endsection



<style>
/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #555;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  margin: 20px 0px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}

.profile-usermenu {
  margin-top: 30px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}

.avatar-upload {
  position: relative;
  max-width: 205px;
  margin: 0px auto;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 1;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
  cursor: pointer;
  font-weight: normal;
  transition: all 0.2s ease-in-out;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
.avatar-upload .avatar-edit input + label:after {
  content: "\f040";
  font-family: 'FontAwesome';
  color: #757575;
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  text-align: center;
  margin: auto;
}
.avatar-upload .avatar-preview {
  width: 192px;
  height: 192px;
  position: relative;
  border-radius: 100%;
  border: 6px solid #F8F8F8;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
}
.avatar-upload .avatar-preview > div {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}

</style>
