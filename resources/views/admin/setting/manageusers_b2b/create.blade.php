@extends('admin.home')
@section('content')
<form id="form-user-b2b-create" style="display: contents;">
<div class="col-md-12">
    
    <div class="card">
        <div class="card-header-tab card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                User Create
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <label class="label-control">Name</label>
                    <div class="form-group">
                        <input name="name" placeholder="Name" type="text" class="form-control" required/>
                    </div> 
                </div>
                {{-- <div class="col-md-4"> --}}
                    {{-- <label class="label-control">Username</label>
                    <div class="form-group">
                        <input name="username" placeholder="Username" type="text" class="form-control" required/>
                    </div>
                    
                </div> --}}
                <div class="col-md-6">
                    <label class="label-control">Company Name</label>
                    <div class="form-group">
                        <select class="form-control" id="company" name="ms_company_id" style="width:100%" required>
                            
                            @foreach ($company as $comp)
                                <option value="{{ $comp->id }}">{{$comp->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="label-control">Password</label>
                    <div class="form-group">
                        <input name="password" placeholder="password" type="password" class="form-control" required/>
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="label-control">Password Confirmation</label>
                    <div class="form-group">
                        <input name="password_confirmation" placeholder="password confirmation" type="password" class="form-control" required/>
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="label-control">Email</label>
                    <div class="form-group">
                        <input name="email" placeholder="email" type="email" class="form-control" />
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="label-control">Phone Number 1</label>
                    <div class="form-group">
                        <input name="phone1" placeholder="628" type="text" class="form-control phone" required/>
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="label-control">Phone Number 2</label>
                    <div class="form-group">
                        <input name="phone2" placeholder="628" type="text" class="form-control phone" />
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="label-control">Phone Number 3</label>
                    <div class="form-group">
                        <input name="phone3" placeholder="628" type="text" class="form-control phone" />
                    </div>
                </div>
            </div>
            <label class="label-control">Address</label>
            <div class="form-group">
                <textarea name="address" class="form-control" id="" cols="30" rows="4"></textarea>
            </div>

            {{-- <label class="label-control">Attachment</label>
            <div class="form-group">
                <input type="file" name="attachment" class="form-cntrol">
            </div> --}}

            <div class="form-group text-right">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9"></div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-success btn-block">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form> 
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
@include('admin.template._mapsScript')
    <script>
        $('.phone').mask('628 0000 0000 00');

        globalCRUD
            .select2("#company", '/user-b-2-b/select2')

            $('#form-user-b2b-create').submit(function(e) {
                globalCRUD.handleSubmit($(this))
                    .storeTo('/user-b-2-b')
                    .backTo(function(resp) {
                        console.log(resp)
                        return "/admin/user-business-to-business/show";
                    })
                e.preventDefault();
            });
    </script>
@endsection
