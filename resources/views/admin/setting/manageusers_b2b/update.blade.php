@extends('admin.home')
@section('content')
<form method="POST" id="form-user-b2b-update" style="display: contents;">
    <input type="hidden" value="{{ $updateUsers->id }}" name="id">
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                User Create
            </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <label class="label-control">Name</label>
              <div class="form-group">
                  <input name="name" placeholder="Name" type="text" class="form-control" value="{{ $updateUsers->name }}" required/>
              </div>
            </div>
            {{-- <div class="col-md-6">
              <label class="label-control">Username</label>
              <div class="form-group">
                  <input name="username" placeholder="Username" value="{{ $updateUsers->username }}" type="text" class="form-control" required/>
              </div>
            </div> --}}
            <div class="col-md-6">
              <label class="label-control">Company Name</label>
              <div class="form-group">
                  <select class="form-control" id="company" name="ms_company_id" style="width:100%" required>
                      @foreach ($company as $comp)
                          <option value="{{$comp->id}}" {{$comp->id == $updateUsers->ms_company_id ? 'selected' : ''}}>{{$comp->name}}</option>
                      @endforeach
                      
                  </select>
              </div>
            </div>
              <div class="col-md-6">
                <label class="label-control">Email</label>
                <div class="form-group">
                    <input name="email" placeholder="email" type="email" value="{{ $updateUsers->email }}" class="form-control" />
                </div>
              </div>
              <div class="col-md-6">
                <label class="label-control">Phone Number 1</label>
                <div class="form-group">
                    <input name="phone1" value="{{ $updateUsers->phone1 }}" placeholder="628" type="text" class="form-control phone" required/>
                </div>
            </div>
            <div class="col-md-4">
                <label class="label-control">Phone Number 2</label>
                <div class="form-group">
                    <input name="phone2" value="{{ $updateUsers->phone2 }}" placeholder="628" type="text" class="form-control phone" />
                </div>
            </div>
            <div class="col-md-4">
                <label class="label-control">Phone Number 3</label>
                <div class="form-group">
                    <input name="phone3" value="{{ $updateUsers->phone3 }}" placeholder="628" type="text" class="form-control phone" />
                </div>
            </div>
            <div class="col-md-4">
              <label class="label-control">Status</label>
              <div class="form-group">
                  <input name="status" class="tgl tgl-skewed" id="cb3" type="checkbox"  value="{{ $updateUsers->status }}" {{ $updateUsers->status == 1 ? 'checked' : '' }}/>
                  <label class="tgl-btn" data-tg-off="UNACTIVE" data-tg-on="ACTIVE" for="cb3"></label>
              </div>
            </div>
          </div>
            <label class="label-control">Address</label>
            <div class="form-group">
                <textarea name="address" class="form-control" id="" cols="30" rows="4">{{ $updateUsers->address }}</textarea>
            </div>

            {{-- <label class="label-control">Attachment</label>
            <div class="form-group">
                <input type="file" name="attachment" class="form-cntrol">
            </div> --}}

            <div class="form-group text-right">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-9"></div>
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-success btn-block">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>

    .tgl {
      display: none;
    }
    .tgl, .tgl:after, .tgl:before, .tgl *, .tgl *:after, .tgl *:before, .tgl + .tgl-btn {
      box-sizing: border-box;
    }
    .tgl::-moz-selection, .tgl:after::-moz-selection, .tgl:before::-moz-selection, .tgl *::-moz-selection, .tgl *:after::-moz-selection, .tgl *:before::-moz-selection, .tgl + .tgl-btn::-moz-selection {
      background: none;
    }
    .tgl::selection, .tgl:after::selection, .tgl:before::selection, .tgl *::selection, .tgl *:after::selection, .tgl *:before::selection, .tgl + .tgl-btn::selection {
      background: none;
    }
    .tgl + .tgl-btn {
      outline: 0;
      display: block;
      width: 7em;
      height: 2em;
      position: relative;
      cursor: pointer;
      -webkit-user-select: none;
         -moz-user-select: none;
          -ms-user-select: none;
              user-select: none;
    }
    .tgl + .tgl-btn:after, .tgl + .tgl-btn:before {
      position: relative;
      display: block;
      content: "";
      width: 50%;
      height: 100%;
    }
    .tgl + .tgl-btn:after {
      left: 0;
    }
    .tgl + .tgl-btn:before {
      display: none;
    }
    .tgl:checked + .tgl-btn:after {
      left: 50%;
    }
    
    
    .tgl-skewed + .tgl-btn {
      overflow: hidden;
      -webkit-transform: skew(-10deg);
              transform: skew(-10deg);
      -webkit-backface-visibility: hidden;
              backface-visibility: hidden;
      -webkit-transition: all .2s ease;
      transition: all .2s ease;
      font-family: sans-serif;
      background: #888;
    }
    .tgl-skewed + .tgl-btn:after, .tgl-skewed + .tgl-btn:before {
      -webkit-transform: skew(10deg);
              transform: skew(10deg);
      display: inline-block;
      -webkit-transition: all .2s ease;
      transition: all .2s ease;
      width: 100%;
      text-align: center;
      position: absolute;
      line-height: 2em;
      font-weight: bold;
      color: #fff;
      text-shadow: 0 1px 0 rgba(0, 0, 0, 0.4);
    }
    .tgl-skewed + .tgl-btn:after {
      left: 100%;
      content: attr(data-tg-on);
    }
    .tgl-skewed + .tgl-btn:before {
      left: 0;
      content: attr(data-tg-off);
    }
    .tgl-skewed + .tgl-btn:active {
      background: #888;
    }
    .tgl-skewed + .tgl-btn:active:before {
      left: -10%;
    }
    .tgl-skewed:checked + .tgl-btn {
      background: #1976d2;
    }
    .tgl-skewed:checked + .tgl-btn:before {
      left: -100%;
    }
    .tgl-skewed:checked + .tgl-btn:after {
      left: 0;
    }
    .tgl-skewed:checked + .tgl-btn:active:after {
      left: 10%;
    }
    </style>
</form> 
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
@include('admin.template._mapsScript')
    <script>
        $('.phone').mask('628 0000 0000 00');

        globalCRUD
            .select2("#company", '/user-b-2-b/select2')

            $('#form-user-b2b-update').submit(function(e) {

                globalCRUD.handleSubmit($(this))
                    .updateTo(function(formData) {
                        return '/user-b-2-b/' + formData.id;
                    })
                    .redirectTo(function(resp) {
                        return '/admin/user-business-to-business/show';
                    })

                e.preventDefault();
            });
    </script>
@endsection
