@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Update Address
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('/admin/address/show') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">
                    <i class="" aria-hidden="true"></i>&nbsp;&nbsp;Back
                </a>
            </div>
        </div>
        <div class="card-body">
            <form action="" method="post" id="form-address-update">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Phone Number 1</label>
                    <div class="col-sm-10">
                        <input name="phone1" value=" {{ $updateAddress->phone1 }} " id="examplename" placeholder="+628xxx" type="text" class="form-control">
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Phone Number 2</label>
                    <div class="col-sm-10">
                        <input name="phone2" value=" {{ $updateAddress->phone2 }} " id="examplename" placeholder="+628xxx" type="text" class="form-control">
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Type</label>
                    <div class="col-sm-10">
                        <select class="js-example-basic-single" id="address_type" name="ms_address_type_id" style="width:100%">
                            <option value="{{ $updateAddress->types->id }}" selected>{{ $updateAddress->types->name }}</option>
                        </select>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Country Name</label>
                    <div class="col-sm-10">
                        <select class="js-example-basic-single" id="countries" name="ms_countries_id" style="width:100%">
                            <option value="{{ $updateAddress->city->province->country->id }}" selected>{{ $updateAddress->city->province->country->name }}</option>
                        </select>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Province Name</label>
                    <div class="col-sm-10">
                        <select class="js-example-basic-single" id="province" name="ms_province_id" style="width:100%">
                            <option value="{{ $updateAddress->city->province->id }}" selected>{{ $updateAddress->city->province->name }}</option>
                        </select>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">City Name</label>
                    <div class="col-sm-10">
                        <select class="js-example-basic-single" id="city" name="ms_city_id" style="width:100%">
                            <option value="{{ $updateAddress->city->id }}" selected>{{ $updateAddress->city->name }}</option>
                        </select>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Distric Name</label>
                    <div class="col-sm-10">
                        <select class="js-example-basic-single" id="distric" name="ms_district_id" style="width:100%">
                            <option value="{{ $updateAddress->district->id }}" selected>{{ $updateAddress->district->name }}</option>
                        </select>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Village Name</label>
                    <div class="col-sm-10">
                        <select class="js-example-basic-single" id="vilage" name="ms_village_id" style="width:100%">
                            <option value="{{ $updateAddress->village->id }}" selected>{{ $updateAddress->village->name }}</option>
                        </select>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Zip-Code</label>
                    <div class="col-sm-10">
                        <input name="ms_zipcode_id" value="{{ $updateAddress->zipcode->zip_no }}" placeholder="06XXX" id="zipcode-text" type="text" class="form-control" readonly>
                    </div>
                </div>

                <input name="ms_zipcode_id" value="{{ $updateAddress->ms_zipcode_id }}" id="zipcode-id" placeholder="06XXX" type="hidden" class="form-control" readonly>
                <input name="id" value="{{ $updateAddress->id }}" type="hidden" class="form-control" readonly>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10">
                        <textarea name="address" class="form-control" id="exampleFormControlTextarea1" rows="3">{{ $updateAddress->address }}</textarea>
                    </div>
                </div>

                <div class="position-relative row form-check">
                    <div class="col-sm-10 offset-sm-2">
                        <button class="btn btn-success" autocomplete="off" id="update"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
@include('admin.script.profile._addressScript')
<script>
    $('#form-address-update').submit(function(e) {
        globalCRUD.handleSubmit($(this))
            .updateTo(function(formData) {
                return '/user_address/' + formData.id;
            })
            .redirectTo(function(resp) {
                return '/admin/user/show';
            })
    })
</script>
@endsection