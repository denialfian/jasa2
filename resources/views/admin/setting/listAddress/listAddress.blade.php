@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                List Address
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                @if(Request::segment(1) == 'customer' && Auth::user()->hasRole('Customer'))
                    <a href="{{ url('/customer/address/create') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">
                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add New Address
                    </a>
                @endif

                @if(Request::segment(1) == 'teknisi' && Auth::user()->hasRole('Technician'))
                    <a href="{{ url('/admin/address/create') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">
                        <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add New Address
                    </a>
                @endif

            </div>
        </div>
        <div class="card-body">
            <table id="address-table" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th style="width:70%">Full Address</th>
                        <th>Primary</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>

    var dt = globalCRUD.datatables({
        url: '/customer/profile/address_list/datatables',
        selector: '#address-table',
        columnsField: [
          'DT_RowIndex',
          {
              data:'id',
              name:'id',
              render: function(data, type, row){
                  return row.address;
                  // return row.village.name + ' ' + row.district.name + ' ' + row.city.name;
              }
          },
          {
              data:'id',
              name:'id',
              render: function(data, type, row){
                  if (row.is_main == 0) {
                    return "<button type='button' class='btn btn-main-address btn-drak' data-id='"+row.id+"'><i style='font-size: 24px;' class='fa fa-square'></i></button>";
                  }else{
                    return "<button type='button' class='btn btn-drak' data-id='"+row.id+"'><i class='fa fa-check-square' style='font-size: 24px;'></i></button>";
                  }
              }
          }
        ],
        scrollX: true,
        actionLink: {
            only_icon: false,
            update: function(row) {
                return '/customer/profile/address/'+row.id+'/edit';
            },
            delete: function(row) {
                return '/customer/profile/address/'+row.id;
            }
        }
    })

    $(document)
        .on('click', '.btn-main-address', function(){
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.put('/customer/profile/address/main/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif(response.data.msg);
                        // reload table
                        dt.table.reloadTable();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })
</script>
@endsection
