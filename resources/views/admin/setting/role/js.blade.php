<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
<script>
    
    $(document).ready(function() {
        $(".js-select2").select2({
            closeOnSelect : false,
            placeholder : "Placeholder",
            allowHtml: true,
            allowClear: true,
            tags: true
        });
    });
 
    $(document).ready(function() {
    $('#btn-add').click(function(){
        $('#select-from option:selected').each( function() {
            $('#select-to').append("<option value='"+$(this).val()+"' selected>"+$(this).text()+"</option>");
            $(this).remove();
        });
    });
    $('#btn-remove').click(function(){
        $('#select-to option:selected').each( function() {
            $('#select-from').append("<option value='"+$(this).val()+"' >"+$(this).text()+"</option>");
            $(this).remove();
        });
    });
    
    $('#multi').submit( function () {
        var selectalla = $('#select-to').val();
        if(!selectalla) {
            $('#select-to option').attr('selected','selected');
        }
        var selectallb = $('#select-from').val();
        if(!selectallb) {
            $('#select-from option').attr('selected','selected');
        }

    });

});
</script>