@extends('admin.home')
@section('content')
@include('admin.setting.profile.profileCss')
<style>
                                                    /* input{
   text-align:center;
} */
                                                </style>
<div class="col-md-4">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                PROFILE
            </div>
        </div>
        <div class="card-body">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic text-center">
                    @if(Auth::user()->info->attachment == null)
                        <img alt="" class="img-responsive" src="https://image.freepik.com/free-vector/man-avatar-profile-round-icon_24640-14046.jpg"></img>
                    @else
                        <img alt="" id="profile-image1" class="img-responsive" src="{{ asset('admin/storage/image/profile/' . $user->info->picture) }}"></img>
                    @endif
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        {{ Auth::user()->name }}
                    </div>
                    <div class="profile-usertitle-job">
                        {{ Auth::user()->email }}
                    </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <div class="profile-userbuttons">
                    <a href="{{ url('/customer/update/'. Auth::user()->id) }}" class="btn btn-success btn-sm" >Edit Info</a>
                    <!-- <button class="btn btn-success btn-sm" type="button">
                        EDIT INFO
                    </button> -->
                </div>
                <!-- END SIDEBAR BUTTONS -->
            </div>
        </div>
    </div><br>
</div><br><br><br>
<div class="col-md-8">
    <div class="card">
        <div class="card-header-tab card-header text-white bg-primary">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                @if(Request::segment(1) == 'customer' && Auth::user()->hasRole('Customer') )
                    <p>Customer</p>
                @endif
                @if(Request::segment(1) == 'teknisi' && Auth::user()->hasRole('Technician') )
                    <p>Mechanic</p>
                @endif
            </div>
        </div>
        <div class="card-body">
            <table class="table table-user-information">
                <tbody>
                	<tr>
                        <th>
                            Name
                            <input type="hidden" name="url" value="{{ Request::segment(1) }}">
                        </th>
                        <td>
                            {{ Auth::user()->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Phone
                        </th>
                        <td>
                            @if ( !is_null(Auth::user()->otp_code) )
                                    @if ( !is_null(Auth::user()->otp_verified_at) )
                                        @if ( !is_null( Auth::user()->otp_url ) )
                                            <!-- nah disini kondisi final karena semuanya udah ada otp code , otp verify , otp url -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            +{{ substr(Auth::user()->phone, 0, 2) }} {{ substr(Auth::user()->phone, 2, 4) }} {{ substr(Auth::user()->phone, 6, 4) }} {{ substr(Auth::user()->phone, 10, 3) }} 
                                                        </div>
                                                        <div class="col-md-6">
                                                        <button type="button" class="btn btn-danger  btn-sm btn-white" data-toggle="modal" data-target="#exampleModal">
                                                            <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Update Phone Number
                                                        </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>      
                                        @endif
                                    @else
                                        <!-- verify -->
                                        <div class="row">
                                            <form action="{{ route('otp.verification') }}" method="post">
                                            @csrf
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control" id="credit-card" name="otp" placeholder="X - X - X - X " data-inputmask="'mask': '9  9  9  9'">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <button type="submit"  class="btn btn-success btn-sm btn-white" >Varification</button>
                                                        </div>&nbsp;&nbsp;&nbsp;
                                            </form>
                                            
                                            <form action="{{ route('otp.resend.verification') }}" method="post">
                                                @csrf
                                                        <div class="col-md-2">
                                                            <button type="submit"  class="btn btn-danger btn-sm btn-white" >Resend</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    @endif        
                                @else
                                    <!-- alert notif harus confirmasi otp code -->
                                    <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h6>{{  Auth::user()->phone }}</h6>
                                                        <!-- <input name="phone" id="" placeholder="immediately verify your number" type="number" class="form-control"> -->
                                                    </div>
                                                    <div class="col-md-6" style="float: right">
                                                           <button class="sendOTP btn btn-success btn-sm btn-white"><i class='fa fa-check'></i>&nbsp; Send OTP Code</button>
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Email
                        </th>
                        <td>
                            {{ Auth::user()->email }} 
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Date Of Birth
                        </th>
                        <td>
                            {{ Auth::user()->info == null ? '' : Auth::user()->info->date_of_birth }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Religion
                        </th>
                        <td>
                            {{ Auth::user()->info == null ? '' : Auth::user()->info->religion->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Gender
                        </th>
                        <td>
                            @if (Auth::user()->info != null)
                                @if(Auth::user()->info->gender == 0)
                                    <p>Male</p>
                                @endif
                                @if(Auth::user()->info->gender == 1)
                                    <p>Woman</p>
                                @endif
                            @endif                            
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Marital
                        </th>
                        <td>
                            {{ Auth::user()->info == null ? '' : Auth::user()->info->marital->status }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<hr>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                List Address
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('/admin/address/create') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">
                    <i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;Add New Address
                </a>
            </div>
        </div>
        <div class="card-body">
            <table id="form-list-address-table" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr align="center">
                        <th>No</th>
                        <th>Phone</th>
                        <th>City</th>
                        <th>Distric</th>
                        <th>Village</th>
                        <th>Zipcode</th>
                        <th>Adress Type</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<!-- ====================modal Show========================================= -->
<div class="modal fade" id="exampleModal" tabindex="-1"  data-backdrop="false" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Phone Number</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-3 col-form-label">Phone Number</label>
                    <div class="col-sm-9">
                        <input type="text" name="phone" placeholder="+628" value="{{ Auth::user()->phone }}" class="form-control phone"></input>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm btn-white" data-dismiss="modal">Close</button>
                <button class="update btn btn-danger btn-sm btn-white" data-id="{{ Auth::user()->id }}">
                     Update
                </button>
            </div>
        </div>
    </div>
</div>
<!-- ====================modal End========================================= -->

@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
	<script>
		$('.phone').mask('628 0000 0000 00');
	</script>
<script>
    $(document).on('click', '.sendOTP', function(e) {
        id = $(this).attr('data-id');
        redirect = $('input[name  = "url"]').val();
        phone  = $('input[name="phone"]').val();

        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                type: 'post',
                data:{
                    phone: $('input[name="phone"]').val(),
                },

                url:Helper.apiUrl('/user/otp/send' ),

                success: function(res) {
                    Helper.successNotif('Otp Has Been Send');
                    Helper.loadingStop();
                    console.log('url TO', redirect)
                    if(redirect == 'teknisi'){
                        window.location = Helper.url('/teknisi/profile'); 
                    }
                    if(redirect == 'customer'){
                        window.location = Helper.url('/customer/profile/show'); 
                    }
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })

    $(":input").inputmask();

    // update OTP
    $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/user/update/number/'),
                type: 'post',
                data : {
                    phone : $('input[name="phone"]').val(),
                },

                success: function(res) {
                    Helper.successNotif('data berhasil di Update');
                    Helper.loadingStop();
                    Helper.redirectTo('/admin/profile/show');
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })


    globalCRUD.datatables({
        url: '/user_address/datatables',
        selector: '#form-list-address-table',
        columnsField: ['DT_RowIndex', 'phone1', 'city.name', 'district.name', 'village.name', 'zipcode.zip_no', 'types.name'],
        scrollX: true,
        actionLink: {
            update: function(row) {
                return '/admin/address/update/' + row.id;
            },
            delete: function(row) {
                return '/user_address/' + row.id;
            }
        }
    })
</script>
@endsection
<style>
/* Profile container */
.profile {
  margin: 20px 0;
}

/* Profile sidebar */
.profile-sidebar {
  padding: 20px 0 10px 0;
  background: #fff;
}

.profile-userpic img {
  float: none;
  margin: 0 auto;
  width: 50%;
  -webkit-border-radius: 50% !important;
  -moz-border-radius: 50% !important;
  border-radius: 50% !important;
}

.profile-usertitle {
  text-align: center;
  margin-top: 20px;
}

.profile-usertitle-name {
  color: #5a7391;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 7px;
}

.profile-usertitle-job {
  text-transform: uppercase;
  color: #5b9bd1;
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 15px;
}

.profile-userbuttons {
  text-align: center;
  margin-top: 10px;
}

.profile-userbuttons .btn {
  text-transform: uppercase;
  font-size: 11px;
  font-weight: 600;
  padding: 6px 15px;
  margin-right: 5px;
}

.profile-userbuttons .btn:last-child {
  margin-right: 0px;
}
    
.profile-usermenu {
  margin-top: 30px;
}

/* Profile Content */
.profile-content {
  padding: 20px;
  background: #fff;
  min-height: 460px;
}
</style>


