@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="alert alert-danger" role="alert">
        <h5 class="alert-heading">your application does not meet the requirements</h5><hr>
            <p>{{ Auth::user()->note_reject }}</p>
      </div>
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Update Data Technician
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                @if (Request::segment(1) == 'admin')
                    <a href="{{ url('/admin/home') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">
                        <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back
                    </a>
                @elseif(Request::segment(1) == 'customer')
                    <a href="{{ url('/customer/dashboard') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">
                        <i class="fa fa-arrow-left"></i>&nbsp;&nbsp;Back
                    </a>
                @endif
            </div>
        </div>
        <div class="card-body">
            {{-- <form action="" method="post" id="update_offer_teknisi"> --}}
                <div class="row">
                    <div class="col-md-12">
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Document Card</label>
                            <div class="col-sm-10">
                                <input name="attachment" id="image_icon" placeholder="Icons" type="file" class="form-control">
                                <small style="color: red">* KTP, SIM, Passport</small><br>    
                                <strong><span id="error-attachment" style="color:red"></span></strong>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Document Number</label>
                            <div class="col-sm-10">
                                <input name="no_identity" id="no_identity" id="examplename" placeholder="0012XXX" type="number" class="form-control" >
                                <small style="color: red">* KTP, SIM, Passport</small><br>    
                                <strong><span id="error-no_identity" style="color:red"></span></strong>
                            </div>
                        </div>

                        
                        <input type="hidden" name="user_id" id="user_id" value="{{ Auth::user()->id }}">
                        <button class="btn btn-primary btn-sm" id="saved" style="float:right">Update</button>
                    </div>
                </div>
            {{-- </form> --}}
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $('.alert-regis').hide();

        

        //untuk save all data
        $("#saved").click(function() {
            var fd = new FormData();
            id = $('#user_id').val();
            attachmen = $('#image_icon')[0].files[0];
            if(attachmen){
                fd.append('attachment', attachmen);
            }
            fd.append('no_identity', $('input[name="no_identity"]').val());
            
            console.log(fd)
            $.ajax({
                url:Helper.apiUrl('/technician-offer/update/' + id),
                data: fd,
                type: 'POST',
                contentType: false,
                processData: false,
                success: function(response) {
                    Helper.successNotif('Update Data Has Been Success');
                    window.location.href = Helper.redirectUrl('/customer/dashboard');
                },
                error: function(xhr, status, error) {
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])
                        })
                    }
                },
            });
        });
    </script>
    <script>
        $("#curriculum").select2({
             ajax: {
                 type: "GET",
                 url: Helper.apiUrl('/development_program/select2'),
                 // url: 'http://localhost:8001/api/address_search/find_district?city_id=' + $(this).val(),
                 data: function(params) {
                     return {
                         q: params.term
                     };
                 },
                 processResults: function(data) {
                     var res = $.map(data.data, function(item) {
                         return {
                             text: item.program_name,
                             id: item.id
                         }
                     });
 
                     return {
                         results: res
                     };
                 }
             }
         });
    </script>
@endsection