<div class="modal fade" id="show_po_model" data-backdrop="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Berita Acara</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body po_area">
                <table class="table table-hover table-bordered table_po" style="width:100%">
                    <thead>
                        <th>Image</th>
                        <th>Action</th>
                    </thead>
                    <tbody id="append_po_area">
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready( function () {
        $('.table_po').DataTable();
    });

    var $modal_po = $('#show_po_model').modal({
        show: false
    });

    var poObj = {
        // isi field input
        isiDataFormModalPo: function(type, id) {
            $.ajax({
                url: Helper.apiUrl('/request-order-b-2-b/show-po/'+type+'/'+id),
                type: 'get',
                success: function(resp) {
                    console.log(resp.data)
                    $("#append_po_area").html('');
                    var content_po_area = '';
                    _.each(resp.data, function(po) {
                        content_po_area += `<tr>
                                                <td><a href="/storage/media-btb/${po.image_po}" target="__blank">${po.image_po}</a></td>
                                                <td><button type="submit" class="btn btn-danger btn-sm" id="btn_delete_po" data-id="${po.id}"><i class="fa fa-trash"></i></button></td>
                                            </tr> `;
                    });
                    $("#append_po_area").append(content_po_area);
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error);
                },
            })

        },
        // hadle ketika response sukses
        successHandle: function(resp) {
            // send notif
            Helper.successNotif(resp.msg);
            $modal_po.modal('hide');
        },
    }


    $(document)
        .on('click', '#show-po', function() {
            id = $('.btb-outlet-item-id').val();
            type = $(this).attr('data-type');
            console.log(poObj.isiDataFormModalPo(type, id));
            $modal_po.modal('show');
        })

    $(document)
        .on('click', '#btn_delete_po', function() {
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.post('/request-order-b-2-b/delete-po/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('Data Berhasil Dihapus');
                        // reload table
                        location.reload()
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })
</script>