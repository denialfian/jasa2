@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <input type="hidden" id="btb-outlet-id" value="{{ $outlet->id }}">
            <input type="hidden" id="btb-outlet-jumlah_unit" value="{{ $outlet->jumlah_unit }}">
            <p>Company</p>
            <p><strong>{{ $outlet->business_to_business_transaction->company->name }} ({{ $outlet->name }})</strong></p>
            <hr />
            <p>Jumlah Unit</p>
            <p><strong>{{ $outlet->jumlah_unit }}</strong></p>
            <hr />
            <p>Alamat</p>
            <p><strong>{{ $outlet->alamat }}</strong></p>

            <button class="btn btn-primary btn-sm add-btb-outlet-item"><i class="fa fa-plus"></i> ADD OUTLET DETAIL</button>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                OUTLET DETAIL
            </div>
        </div>
        <div class="card-body table-responsive">
            <table id="table-outlet-detail" class="display table table-hover table-bordered" style="width:100%; white-space: nowrap;">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ACTION</th>
                        <th>UNIT KE</th>
                        <th>MEREK</th>
                        <th>REMARK</th>
                        <th>NO QUOTATION</th>
                        <th>NO PO</th>
                        <th>NO PO</th>
                        <th>NOMINAL QUOTATION</th>
                        <th>STATUS QUOTATION</th>
                        <th>TANGGAL PEMBAYARAN</th>
                        <th>TANGGAL QUOTATION</th>
                        <th>TANGGAL PO</th>
                        <th>TANGGAL INVOICE</th>
                        <th>FILE BERITA ACARA</th>
                        <th>FILE QUOTATION</th>
                        <th>FILE PO</th>
                        <th>FILE INVOICE</th>
                        <th>USER CREATED</th>
                        <th>USER UPDATE</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<form id="form-btb-outlet-item">
    <div class="modal fade" id="modal-btb-outlet-item" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title modal-title-btb-outlet-item"></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="btb-outlet-item-id">
                    <input type="hidden" name="business_to_business_outlet_transaction_id" id="business_to_business_outlet_transaction_id" value="{{ $outlet->id }}">

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Unit Ke</label>
                                <select name="unit_ke" class="form-control btb-outlet-item-unit_ke" required>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Merek</label>
                                <input name="merek" placeholder="merek" type="text" class="btb-outlet-item-merek form-control" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Remark</label>
                                <input name="remark" placeholder="remark" type="text" class="btb-outlet-item-remark form-control">
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>No Quotation</label>
                                <input name="no_quotation" placeholder="No Quotation" type="text" class="btb-outlet-item-no_quotation form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>No Po</label>
                                <input name="no_po" placeholder="No PO" type="text" class="btb-outlet-item-no_po form-control">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>No Invoice</label>
                                <input name="no_invoice" placeholder="No Invoice" type="text" class="btb-outlet-item-no_invoice form-control">
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nominal Quotation</label>
                                <input name="nominal_quot" placeholder="Nominal Quot" type="number" class="btb-outlet-item-nominal_quot form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status Quotation</label>
                                <input name="status_quotation" placeholder="Status Quotation" type="text" class="btb-outlet-item-status_quotation form-control">
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal Pembayaran</label>
                                <input name="tanggal_pembayaran" placeholder="Tanggal Pembayaran" type="text" class="datepick btb-outlet-item-tanggal_pembayaran form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal Quotation</label>
                                <input name="tanggal_quot" placeholder="Tanggal Quotation" type="text" class="datepick btb-outlet-item-tanggal_quot form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal PO</label>
                                <input name="tanggal_po" placeholder="Tanggal PO" type="text" class="datepick btb-outlet-item-tanggal_po form-control">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tanggal Invoice</label>
                                <input name="tanggal_Invoice" placeholder="Tanggal Invoice" type="text" class="datepick btb-outlet-item-tanggal_Invoice form-control">
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Upload Berita acara</label>
                                <input name="file_berita_acara" type="file" class="btb-outlet-item-file_berita_acara form-control">
                                <span class="file_berita_acara-file_area">
                                    <br>
                                    <a href="" target="__blank" class="btn btn-xs btn-info file_berita_acara-link">
                                        <i class='fa fa-download'></i><span class="file_berita_acara-file_name"></span></a>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Upload File Quotation</label>
                                <input name="file_quot" type="file" class="btb-outlet-item-file_quot form-control">
                                <span class="file_quot-file_area">
                                    <br>
                                    <a href="" target="__blank" class="btn btn-xs btn-info file_quot-link">
                                        <i class='fa fa-download'></i><span class="file_quot-file_name"></span></a>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Upload PO</label>
                                <input name="file_po" type="file" class="btb-outlet-item-file_po form-control">
                                <span class="file_po-file_area">
                                    <br>
                                    <a href="" target="__blank" class="btn btn-xs btn-info file_po-link">
                                        <i class='fa fa-download'></i><span class="file_po-file_name"></span></a>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Upload Invoice</label>
                                <input name="file_invoice" type="file" class="btb-outlet-item-file_invoice form-control">
                                <span class="file_invoice-file_area">
                                    <br>
                                    <a href="" target="__blank" class="btn btn-xs btn-info file_invoice-link">
                                        <i class='fa fa-download'></i><span class="file_invoice-file_name"></span></a>
                                </span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect"><i class="fa fa-plus"></i> Save</button>
                    <button type="button" class="btn btn-sm waves-effect btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
</form>
<style>
    .fixed-sidebar .app-main .app-main__outer {
        width: 100%;
    }
</style>
@endsection

@section('script')
<script>
    Helper.datePick('.datepick');

    var tableOrder = $('#table-outlet-detail').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        scrollX: true,
        ordering: 'true',
        order: [2, 'asc'],
        responsive: false,
        language: {
            buttons: {
                colvis: '<i class="fa fa-list-ul"></i>'
            },
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        buttons: [{
                extend: 'colvis'
            },
            {
                text: '<i class="fa fa-refresh"></i>',
                action: function(e, dt, node, config) {
                    dt.ajax.reload();
                }
            }
        ],
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/business_to_business/' + $('#btb-outlet-id').val() + '/outlet_detail'),
            type: 'get',
        },
        columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                sortable: false,
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    btn_edit_item = '<button type="button" data-id="' + full.id + '" class="btn btn-success btn-xs edit-btb-outlet-item"><i class="fa fa-pencil"></i></button>';
                    btn_delete_item = '<button type="button" data-id="' + full.id + '" class="btn btn-danger btn-xs delete-btb-outlet-item"><i class="fa fa-trash"></i></button>';

                    return btn_edit_item + " " + btn_delete_item;
                }
            },
            {
                data: "unit_ke",
                name: "unit_ke",
                render: function(data, type, full) {
                    return full.unit_ke;
                }
            },
            {
                data: "merek",
                name: "merek",
                render: function(data, type, full) {
                    return full.merek;
                }
            },
            {
                data: "remark",
                name: "remark",
                render: function(data, type, full) {
                    return full.remark;
                }
            },
            {
                data: "no_quotation",
                name: "no_quotation",
                render: function(data, type, full) {
                    return full.no_quotation;
                }
            },
            {
                data: "no_po",
                name: "no_po",
                render: function(data, type, full) {
                    return full.no_po;
                }
            },
            {
                data: "no_invoice",
                name: "no_invoice",
                render: function(data, type, full) {
                    return full.no_invoice;
                }
            },
            {
                data: "nominal_quot",
                name: "nominal_quot",
                render: function(data, type, full) {
                    return full.nominal_quot;
                }
            },
            {
                data: "status_quotation",
                name: "status_quotation",
                render: function(data, type, full) {
                    return full.status_quotation;
                }
            },
            {
                data: "tanggal_pembayaran",
                name: "tanggal_pembayaran",
                render: function(data, type, full) {
                    if (full.tanggal_pembayaran === null) {
                        return '-';
                    }
                    return moment(full.tanggal_pembayaran).format("DD MMMM YYYY");
                }
            },
            {
                data: "tanggal_quot",
                name: "tanggal_quot",
                render: function(data, type, full) {
                    if (full.tanggal_quot === null) {
                        return '-';
                    }
                    return moment(full.tanggal_quot).format("DD MMMM YYYY");
                }
            },
            {
                data: "tanggal_po",
                name: "tanggal_po",
                render: function(data, type, full) {
                    if (full.tanggal_po === null) {
                        return '-';
                    }
                    return moment(full.tanggal_po).format("DD MMMM YYYY");
                }
            },
            {
                data: "tanggal_Invoice",
                name: "tanggal_Invoice",
                render: function(data, type, full) {
                    if (full.tanggal_Invoice === null) {
                        return '-';
                    }
                    return moment(full.tanggal_Invoice).format("DD MMMM YYYY");
                }
            },
            {
                data: "file_berita_acara",
                name: "file_berita_acara",
                render: function(data, type, full) {
                    link = "<a target='__blank' href='" + Helper.url('/admin/business_to_business/' + full.uniq_key + '/download?type=file_berita_acara') + "' class='btn btn-xs btn-info'><i class='fa fa-download'></i></a>";

                    return full.file_berita_acara === null ? '-' : link;
                }
            },
            {
                data: "file_quot",
                name: "file_quot",
                render: function(data, type, full) {
                    link = "<a target='__blank' href='" + Helper.url('/admin/business_to_business/' + full.uniq_key + '/download?type=file_quot') + "' class='btn btn-xs btn-info'><i class='fa fa-download'></i></a>";

                    return full.file_quot === null ? '-' : link;
                }
            },
            {
                data: "file_po",
                name: "file_po",
                render: function(data, type, full) {
                    link = "<a target='__blank' href='" + Helper.url('/admin/business_to_business/' + full.uniq_key + '/download?type=file_po') + "' class='btn btn-xs btn-info'><i class='fa fa-download'></i></a>";

                    return full.file_po === null ? '-' : link;
                }
            },
            {
                data: "file_invoice",
                name: "file_invoice",
                render: function(data, type, full) {
                    link = "<a target='__blank' href='" + Helper.url('/admin/business_to_business/' + full.uniq_key + '/download?type=file_invoice') + "' class='btn btn-xs btn-info'><i class='fa fa-download'></i></a>";

                    return full.file_invoice === null ? '-' : link;
                }
            },
            {
                data: "user_created",
                name: "user_created",
                render: function(data, type, full) {
                    if (full.user_created !== null) {
                        return full.user_create.name;
                    }
                    return '-';
                }
            },
            {
                data: "user_updated",
                name: "user_updated",
                render: function(data, type, full) {
                    if (full.user_updated !== null) {
                        return full.user_update.name;
                    }
                    return '-';
                }
            },
        ]
    });

    $('#form-btb-outlet-item').submit(function(e) {
        e.preventDefault();
        Helper.loadingStart();

        data = Helper.serializeForm($(this));
        formData = generateFormDataDetail(data);

        id = $('.btb-outlet-item-id').val();
        if (id === '') {
            Axios.post('/business_to_business/outlet_detail', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    //send notif
                    Helper.successNotif(response.data.msg);
                    // refresh
                    tableOrder.ajax.reload();
                    // hide modal
                    $('#modal-btb-outlet-item').modal('hide')
                    // empty
                    kosongkanFielDetail();
                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    Helper.loadingStop();
                });
        } else {
            Axios.post('/business_to_business/' + id + '/outlet_detail', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    //send notif
                    Helper.successNotif(response.data.msg);
                    // refresh
                    tableOrder.ajax.reload();
                    // hide modal
                    $('#modal-btb-outlet-item').modal('hide')
                    // empty
                    kosongkanFielDetail();
                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    Helper.loadingStop();
                });
        }
    })

    $('.add-btb-outlet-item').click(function() {
        $('#modal-btb-outlet-item').modal('show');
        kosongkanFielDetail();
        $('.modal-title-btb-outlet-item').text('ADD OUTLET ITEM')
    })

    $(document).on('click', '.edit-btb-outlet-item', function() {
        var row = tableOrder.row($(this).parents('tr')).data();
        console.log(row)
        isiFielDetail(row);
        $('#modal-btb-outlet-item').modal('show');
        $('.modal-title-btb-outlet-item').text('EDIT OUTLET ITEM')
    })

    $(document).on('click', '.delete-btb-outlet-item', function() {
        var id = $(this).attr('data-id');

        Helper.confirmDelete(function() {
            Helper.loadingStart();
            Axios.delete('/business_to_business/' + id + '/outlet_detail')
                .then(function(response) {
                    //send notif
                    Helper.successNotif(response.data.msg);
                    // refresh
                    tableOrder.ajax.reload();
                    Helper.loadingStop();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                    Helper.loadingStop();
                });
        })
    })

    const kosongkanFielDetail = function() {
        var jumlah_unit = $('#btb-outlet-jumlah_unit').val();

        $('.btb-outlet-item-file_berita_acara').val('');
        $('.btb-outlet-item-file_quot').val('');
        $('.btb-outlet-item-file_po').val('');
        $('.btb-outlet-item-file_invoice').val('');
        $('.file_berita_acara-file_area').hide();
        $('.file_quot-file_area').hide();
        $('.file_po-file_area').hide();
        $('.file_invoice-file_area').hide();

        $('.btb-outlet-item-merek').val('');
        $('.btb-outlet-item-remark').val('');
        $('.btb-outlet-item-no_quotation').val('');
        $('.btb-outlet-item-no_po').val('');
        $('.btb-outlet-item-no_invoice').val('');
        $('.btb-outlet-item-nominal_quot').val('');
        $('.btb-outlet-item-tanggal_pembayaran').val('');
        $('.btb-outlet-item-tanggal_quot').val('');
        $('.btb-outlet-item-tanggal_po').val('');
        $('.btb-outlet-item-tanggal_Invoice').val('');
        $('.btb-outlet-item-unit_ke').val('');
        $('.btb-outlet-item-id').val('');
        $('.btb-outlet-item-status_quotation').val('');
        $('.btb-outlet-item-unit_ke').empty().append('<option value="">Select Unit Ke</option>');
        var i;
        var $selectUnitKe = $('.btb-outlet-item-unit_ke');
        for (i = 1; i <= jumlah_unit; i++) {
            $selectUnitKe.append('<option value="' + i + '">' + i + '</option>')
        }
    }

    const isiFielDetail = function(row) {
        detail = row;

        $('.btb-outlet-item-file_berita_acara').val('');
        $('.btb-outlet-item-file_quot').val('');
        $('.btb-outlet-item-file_po').val('');
        $('.btb-outlet-item-file_invoice').val('');

        $('.file_berita_acara-file_area').hide();
        $('.file_quot-file_area').hide();
        $('.file_po-file_area').hide();
        $('.file_invoice-file_area').hide();

        if (detail.file_berita_acara != null) {
            link = Helper.url('/admin/business_to_business/' + detail.uniq_key + '/download?type=file_berita_acara&exptime=' + new Date().getTime());
            $('.file_berita_acara-file_area').show();
            $('.file_berita_acara-file_name').text(detail.file_berita_acara);
            $('.file_berita_acara-link').attr('href', link);
        }

        if (detail.file_quot != null) {
            link = Helper.url('/admin/business_to_business/' + detail.uniq_key + '/download?type=file_quot&exptime=' + new Date().getTime());
            $('.file_quot-file_area').show();
            $('.file_quot-file_name').text(detail.file_quot);
            $('.file_quot-link').attr('href', link);
        }

        if (detail.file_po != null) {
            link = Helper.url('/admin/business_to_business/' + detail.uniq_key + '/download?type=file_po&exptime=' + new Date().getTime());
            $('.file_po-file_area').show();
            $('.file_po-file_name').text(detail.file_po);
            $('.file_po-link').attr('href', link);
        }

        if (detail.file_invoice != null) {
            link = Helper.url('/admin/business_to_business/' + detail.uniq_key + '/download?type=file_invoice&exptime=' + new Date().getTime());
            $('.file_invoice-file_area').show();
            $('.file_invoice-file_name').text(detail.file_invoice);
            $('.file_invoice-link').attr('href', link);
        }



        $('#business_to_business_outlet_transaction_id').val(row.business_to_business_outlet_transaction_id);
        $('.btb-outlet-item-merek').val(row.merek);
        $('.btb-outlet-item-remark').val(row.remark);
        $('.btb-outlet-item-no_quotation').val(row.no_quotation);
        $('.btb-outlet-item-no_po').val(row.no_po);
        $('.btb-outlet-item-no_invoice').val(row.no_invoice);
        $('.btb-outlet-item-nominal_quot').val(row.nominal_quot);
        $('.btb-outlet-item-tanggal_pembayaran').val(row.tanggal_pembayaran);
        $('.btb-outlet-item-tanggal_quot').val(row.tanggal_quot);
        $('.btb-outlet-item-tanggal_po').val(row.tanggal_po);
        $('.btb-outlet-item-tanggal_Invoice').val(row.tanggal_Invoice);
        $('.btb-outlet-item-status_quotation').val(row.status_quotation);
        $('.btb-outlet-item-id').val(row.id);

        $('.btb-outlet-item-unit_ke').empty().append('<option value="">Select Unit Ke</option>');
        var i;
        var jumlah_unit = $('#btb-outlet-jumlah_unit').val();
        var $selectUnitKe = $('.btb-outlet-item-unit_ke');
        for (i = 1; i <= jumlah_unit; i++) {
            $selectUnitKe.append('<option value="' + i + '">' + i + '</option>')
        }
        $('.btb-outlet-item-unit_ke').val(row.unit_ke).change();
    }

    const generateFormDataDetail = function(input) {
        var formData = new FormData();

        _.each(input, function(val, key) {
            formData.append(key, val);
        })

        file_berita_acara = $('.btb-outlet-item-file_berita_acara').prop('files')[0];
        if (file_berita_acara) {
            formData.append("file_berita_acara", file_berita_acara);
        }

        file_quot = $('.btb-outlet-item-file_quot').prop('files')[0];
        if (file_quot) {
            formData.append("file_quot", file_quot);
        }

        file_po = $('.btb-outlet-item-file_po').prop('files')[0];
        if (file_po) {
            formData.append("file_po", file_po);
        }

        file_invoice = $('.btb-outlet-item-file_invoice').prop('files')[0];
        if (file_invoice) {
            formData.append("file_invoice", file_invoice);
        }

        return formData;
    }
</script>
@endsection