@if (Auth::id() == $reply_ticket->user_id)
<div class="outgoing_msg">
    <div class="sent_msg">
        @if ($reply_ticket->attachment != null)
        <img alt="" class="img-responsive" src="{{ asset('/storage/attachment/' . $reply_ticket->attachment) }}" />
        @endif
        <p>{{ $reply_ticket->message }}</p>
        @if ($reply_ticket->order != null)
        <div class="card widget-content">
            <div class="widget-content-wrapper">
                <div class="widget-content-left">
                    <div class="widget-heading">{{ $reply_ticket->order->code }}</div>
                    <div class="widget-subheading">{{ $reply_ticket->order->symptom->name }}</div>
                    <div class="widget-subheading">{{ $reply_ticket->order->schedule->format('d F Y H:i:s') }}</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-success"><a href="{{ $reply_ticket->order->url_detail }}"><span><i class="fa fa-arrow-right"></i></span></a></div>
                </div>
            </div>
        </div>
        @endif
        <span class="time_date">{{ $reply_ticket->created_at->format('H:i | M j') }}</span>
    </div>
</div>
@endif

@if (Auth::id() != $reply_ticket->user_id)
<div class="incoming_msg">
    <div class="incoming_msg_img">
    </div>
    <div class="received_msg">
        <div class="received_withd_msg">
            @if ($reply_ticket->attachment != null)
            <img alt="" class="img-responsive" src="{{ asset('/storage/attachment/' . $reply_ticket->attachment) }}" />
            @endif
            <p>{{ $reply_ticket->message }}</p>
            @if ($reply_ticket->order != null)
            <div class="card widget-content">
                <div class="widget-content-wrapper">
                    <div class="widget-content-left">
                        <div class="widget-heading">{{ $reply_ticket->order->code }}</div>
                        <div class="widget-subheading">{{ $reply_ticket->order->symptom->name }}</div>
                        <div class="widget-subheading">{{ $reply_ticket->order->schedule->format('d F Y H:i:s') }}</div>
                    </div>
                    <div class="widget-content-right">
                        <div class="widget-numbers text-success"><a href="{{ $reply_ticket->order->url_detail }}"><span><i class="fa fa-arrow-right"></i></span></a></div>
                    </div>
                </div>
            </div>
            @endif
            <span class="time_date">{{ $reply_ticket->created_at->format('H:i | M j') }}</span>
        </div>
    </div>
</div>
@endif