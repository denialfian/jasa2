@extends('admin.home')
@section('content')
<div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-header header-bg">
            TICKET
        </div>
        <div class="card-body">
            <table id="table-tiket" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>TICKET NO</th>
                        <th>SUBJECT</th>
                        <th>STATUS</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="d-block card-footer">
            <button class="btn-wide btn btn-sm btn-primary add-tiket"><i aria-hidden="true" class="fa fa-plus" title="add"></i> CREATE NEW</button>
        </div>
    </div>
</div>

<form id="form-tiket">
    <div class="modal fade" data-backdrop="false" id="modal-tiket" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        CREATE NEW TICKET
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="position-relative form-group">
                        <label class="" for="">Subject</label>
                        <input name="subject" class="form-control" />
                    </div>
                    <div class="position-relative form-group">
                        <label class="" for="">Message</label>
                        <textarea name="desc" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SUBMIT
                    </button>
                    <button class="btn waves-effect" data-dismiss="modal" type="button">
                        CLOSE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script>
    // add job experience
    $(document)
        .on('click', '.add-tiket', function() {
            $('#modal-tiket').modal('show');
        })

    var table = $('#table-tiket')
        .DataTable({
            processing: true,
            serverSide: true,
            select: true,
            dom: 'Bflrtip',
            ordering: 'true',
            order: [1, 'desc'],
            responsive: true,
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/customer/tickets/datatables'),
                "type": "get"
            },
            columns: [{
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                    sortable: false,
                    width: "10%"
                },
                {
                    data: "ticket_no",
                    name: "ticket_no",
                    render: function(data, type, full) {
                        link = "<a href='" + Helper.url('/customer/tickets/' + full.id + '/detail') + "'># " + full.ticket_no + "</a>";
                        if (full.unread_ticket != null) {
                            link += '</br><span class="ml-auto badge badge-warning">NEW</span>';
                        }
                        return link;
                    }
                },
                {
                    data: "subject",
                    name: "subject",
                    sortable: false,
                },
                {
                    data: "status",
                    name: "status",
                    sortable: false,
                    render: function(data, type, full) {
                        switch (full.status) {
                            case '1':
                                return '<span class="btn btn-sm btn-primary">open</span>';
                                break;

                            case '2':
                                return '<span class="btn btn-sm btn-info">on progress</span>';
                                break;

                            case '3':
                                return '<span class="btn btn-sm btn-dark">on resolving</span>';
                                break;

                            case '4':
                                return '<span class="btn btn-sm btn-success">done</span> ';
                                break;

                            case '5':
                                return '<span class="btn btn-sm btn-danger">closed</span> ';
                                break;

                            default:
                                return full.status;
                                break;
                        }
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        return "<a href='" + Helper.url('/customer/tickets/' + full.id + '/detail') + "' class='btn btn-search btn-sm btn-danger'><i class='fa fa-dropbox'></i> DETAIL</a>";
                    }
                }
            ]
        });

    $('#form-tiket').submit(function(e) {
        data = Helper.serializeForm($(this));

        Helper.loadingStart();
        // post data
        Axios.post(Helper.apiUrl('/customer/tickets'), data)
            .then(function(response) {
                Helper.successNotif('Success, Successfully!');
                window.location.href = Helper.redirectUrl('/customer/tickets/' + response.data.data.id + '/detail');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>
@endsection