@extends('admin.home')
@section('content')
<div class="col-md-12">
    @if (!empty($getTitle->value) && !empty($getDesc->value))
    <div class="alert alert-warning alert-regis" role="alert">
        <center>
            <i class="fa fa-bell-o"></i><strong> Attention please read before processing the order</strong>
            &nbsp;&nbsp;&nbsp;
            <a href="" type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#exampleModalLong" style="border-color: rgb(159 21 21)!important;border: 1px solid; text-color:black">Here</a>
        </center>
    </div>
    @endif

    <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">{{ $getTitle->value ?? '' }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! $getDesc->value ?? '' !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <form id="form-request-job" enctype="multipart/form-data">

                <input type='hidden' name="issubmit" value="1">
                @if ($getWalletUser !== null)
                <input type='hidden' name="wallet_user" id="wallet_user" value={{ $getWalletUser->nominal }}>
                @else
                <input type='hidden' name="wallet_user" id="wallet_user" value="0">
                @endif
                <!-- Tabs -->
                <div id="wizard" class="swMain">

                    <ul>
                        <li>
                            <a href="#step-1">
                                <label class="stepNumber">1</label>
                                <span class="stepDesc">
                                    Services Information<br />
                                    <small>Fill your service information</small>
                                </span>
                            </a></li>
                        <li>
                            <a href="#step-2">
                                <label class="stepNumber">2</label>
                                <span class="stepDesc">
                                    Product Information<br />
                                    <small>Fill your Product information</small>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-3">
                                <label class="stepNumber">3</label>
                                <span class="stepDesc">
                                    Select Technicians<br />
                                    <small>Select your technicians</small>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="#step-4">
                                <label class="stepNumber">4</label>
                                <span class="stepDesc">
                                    Details Information<br />
                                    <small>Fill your details information</small>
                                </span>
                            </a>
                        </li>
                    </ul>

                    <div id="step-1" style="overflow-y: hidden; overflow-x: hidden;">
                        <label>SERVICE <span style="color:red">*</span> </label>
                        <div class="form-group">
                            <select id="select-services" name="ms_service_id" class="form-control" style="width:100%;" required>
                                <option value="" selected="selected">--Select--</option>
                            </select>
                        </div>

                        <label>SERVICE CATEGORY <span style="color:red">*</span></label>
                        <div class="form-group">
                            <select id="select-service-category" name="symptom_id" class="form-control" style="width:100%;" required>
                                <option value="" selected="selected">--Select--</option>
                            </select>
                        </div>

                        <label>SERVICE TYPE <span style="color:red">*</span></label>
                        <div class="form-group">
                            <select id="select-service-type" name="service_type_id" class="form-control" style="width:100%;" required>
                                <option value="" selected="selected">--Select--</option>
                            </select>
                        </div>

                        <label>PRODUCT GROUP <span style="color:red">*</span></label>
                        <div class="form-group">
                            <select id="select-product_group" name="product_group_id" class="form-control" style="width:100%;" required>
                                <option value="" selected="selected">--Select--</option>
                            </select>
                        </div>
                        <hr />
                        <label>ADDITIONAL INFO <span>(Optional)</span></label>
                        <div class="form-group">
                            <textarea class="form-control" name="note" id="note"></textarea>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="position-relative form-group">
                                    <label class="label-header" for="">
                                        Alamat
                                    </label>
                                    <div>
                                        <div class="list-group">
                                            @if ($main_address != null)
                                            <input type="hidden" name="address_id" value="{{ $main_address->id }}" />
                                            <label class="list-group-item">
                                                <span class="type-address-text">{{ $main_address->types->name }}</span>
                                                <p class="detail-address-text">{{ $main_address->address }}</p>
                                            </label>
                                            @endif
                                        </div>
                                        <a href="#" class="ganti-alamat"><i class="fa fa-pencil"></i> Change Primary Address</a>
                                    </div>
                                    <hr />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="step-2" style="overflow-y: hidden; overflow-x: hidden;">
                        <h2 class="StepTitle">Step 2: Product Detail Information <span>(Optional)</span></h2>

                        <label>PRODUCT NAME <span>(Optional)</span></label>
                        <div class="form-group">
                            <input type="text" id="product_name" name="product_name" class="form-control" style="width:100%;">
                        </div>

                        <label>BRAND NAME <span>(Optional)</span></label>
                        <div class="form-group">
                            <input type="text" id="brand_name" name="brand_name" class="form-control" style="width:100%;">
                        </div>

                        <label>MODEL NAME <span>(Optional)</span></label>
                        <div class="form-group">
                            <input type="text" id="model_name" name="model_name" class="form-control" style="width:100%;">
                        </div>

                        <label>SERIAL NUMBER <span>(Optional)</span></label>
                        <div class="form-group">
                            <input type="text" id="serial_number" name="serial_number" class="form-control" style="width:100%;">
                        </div>
                        <hr />
                        <label>REMARK <span>(Optional)</span></label>
                        <div class="form-group">
                            <textarea class="form-control" name="remark" id="remark"></textarea>
                        </div>

                    </div>
                    <div id="step-3">
                        <h2 class="StepTitle">Step 3: Select Technicians</h2>
                        <input id="prices" name="prices" type="hidden" />
                        <div id="accordion" class="accordion-wrapper mb-3">
                            <div class="card">
                                <section id="list-technicians">
                                </section>
                                <input type="hidden" id="teknisi-dipilih" name="teknisi_id">
                            </div>
                        </div>
                    </div>
                    <div id="step-4" style="overflow-y: scroll;>
                        <h2 class=" StepTitle">Step 4: Details Information</h2>
                        <div class="col-md-4">
                            <div class="position-relative form-group" style="display:none;">
                                <label class="label-header" for="">
                                    Technician Schedules
                                </label>
                                <p id="technician_schedules"></p>
                            </div>
                            <hr />
                        </div>


                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="position-relative form-group">
                                        <label>Available Schedule <span style="color: red;">*</span></label>
                                        <input class="form-control datetimepicker" name="schedule" required="" type="text" id="schedule" readonly />
                                    </div>
                                </div>
                                <div class="col-md-12 row" id="display_hours_available" style="margin-left:20px;"></div>
                            </div>
                            <hr />
                        </div>
                        <br />
                        <div class="col-md-12">
                            <label>Symptom Detail <span style="color: red;">*</span></label>
                            <div class="position-relative form-group">
                                <textarea class="form-control" name="symptom_detail" id="symptom_detail"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label class="" for="">
                                <b>Upload file information media about the description of symptoms <span class="text-danger">(max 4 files image/video) (Optional)</span></b>.
                            </label>
                            <br />
                            <label class="text-danger" for="">
                                <b>Video Upload max 10 MB.</b>
                            </label>

                            <!-- Example 2 -->
                            <input type="file" name="files[]" id="filer_input2" multiple="multiple">
                            <!-- end of Example 2 -->
                        </div>
                    </div>
                </div>
                <!-- End SmartWizard Content -->
            </form>
        </div>

    </div>
</div>

<div class="modal fade" data-backdrop="false" id="modal-alamat" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Select Address
                </h4>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="list-group">
                    @foreach($addresses as $addr)
                    <input {{-- data-type_address="{{ $addr->types->name }}" --}} data-detail_address="{{ $addr->address }}" data-id="{{ $addr->id }}" class="radio-alamat" type="radio" name="address_id" value="{{ $addr->id }}" id="Radio-{{ $addr->id }}" {{ $addr->is_main == 1 ? 'checked' : '' }} />
                    <label class="list-group-item" for="Radio-{{ $addr->id }}">
                        {{-- {{ $addr->types->name }} --}}
                        <p>{{ $addr->address }}</p>
                    </label>
                    @endforeach
                </div>
                <hr />
                <a href="/customer/profile/address/create"><i class="fa fa-plus"></i> Add New Address</a>
            </div>
        </div>
    </div>
</div>
{{-- modal jika saldo <= harga jasa --}}
<!-- Modal -->
<div class="modal fade" id="modalNotice" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="modalNoticeLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalNoticeLabel">Notice</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Your wallet balance is not sufficient to continue this job request, please refill first via the following link <a href="{{ url('/customer/topup/show-list') }}" style="text-decoration: underline; color: blue">Top up</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<!-- CSS Script -->
<style>
    .easy-autocomplete {
        width: 100%;
    }
</style>
<style>
    .modal-dialog {
        overflow-y: initial !important
    }

    .modal-body {
        height: 250px;
        overflow-y: auto;
    }
</style>
@include('admin.customer.job_request.ltScript') {{-- List Technician Script CSS --}}
@include('admin.customer.job_request.swScript') {{-- Step Wizard Script CSS --}}

<!-- End Style from list technicians-->


<link rel="stylesheet" href="{{ asset('adminAssets/css/easy-autocomplete.min.css') }}">
<link rel="stylesheet" href="{{ asset('adminAssets/css/easy-autocomplete.themes.min.css') }}">


<link rel="stylesheet" href="{{ asset('resources/views/customer/css/jquery.filer.css') }}">

<link rel="stylesheet" href="{{ asset('adminAssets/css/jquery.filer.css') }}">
<link rel="stylesheet" href="{{ asset('adminAssets/css/themes/jquery.filer-dragdropbox-theme.css') }}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('adminAssets/js/jquery.easy-autocomplete.min.js') }}"></script>
<!-- Begin Smart Wizard -->
<link href="{{ asset('adminAssets/css/demo_style.css" rel="stylesheet') }}" type="text/css">
<link href="{{ asset('adminAssets/css/smart_wizard.css" rel="stylesheet') }}" type="text/css">
{{-- <script type="text/javascript" src="{{ asset('adminAssets/js/jquery-2.0.0.min.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('adminAssets/js/jquery.smartWizard.js') }}"></script>
<script type="text/javascript">
    $(document).on('click', '.teknisi-card', function() {
        $('.hide-button').hide();
        id_teknisi = $(this).attr('data-id');
        $('.btn-next-step-' + id_teknisi).show();

        // $('.selected-technician').prop('checked', false)
        // $('.teknisi-card-check-'+ id_teknisi).prop('checked', true)
        // console.log(`$('.teknisi-card-check-'+ id_teknisi).prop('checked', true)`)
    })

    function check(input) {
        var checkboxes = $(".selected-technician");
        for (var i = 0; i < checkboxes.length; i++) {
            //uncheck all
            if (checkboxes[i].checked == true) {
                checkboxes[i].checked = false;
            }
        }
        //set checked of clicked object
        if (input.checked == true) {
            input.checked = false;
        } else {
            input.checked = true;
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {

        // Smart Wizard
        $('#wizard').smartWizard({
            transitionEffect: 'slideleft',
            onLeaveStep: leaveAStepCallback,
            onFinish: onFinishCallback
        });

        function leaveAStepCallback(obj) {
            var step_num = obj.attr('rel');
            return validateSteps(step_num);
        }

        function onFinishCallback() {
            if (validateAllSteps()) {
                var schedule = $('#schedule').val() + ' ' + $("input[name='hours']:checked").val();
                var form = new FormData($('#form-request-job')[0]);
                form.append('schedule', schedule);
                form.append('total_prices', $("#prices").val());
                Helper.loadingStart();
                Axios.post(Helper.apiUrl('/customer/request-job/save'), form, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then(function(response) {
                        console.log(response)
                        Helper.successNotif('Success, Successfully submit request job !');
                        window.location.href = Helper.redirectUrl('/customer/request_list');
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });

                e.preventDefault();
            }
        }

    });

    function validateAllSteps() {
        var isStepValid = true;

        if (validateStep1() == false) {
            isStepValid = false;
            $('#wizard').smartWizard('setError', {
                stepnum: 1,
                iserror: true
            });
        } else {
            $('#wizard').smartWizard('setError', {
                stepnum: 1,
                iserror: false
            });
        }

        //    if(validateStep2() == false){
        //      isStepValid = false;
        //      $('#wizard').smartWizard('setError',{stepnum:2,iserror:true});
        //    }else{
        //      $('#wizard').smartWizard('setError',{stepnum:2,iserror:false});
        //    }

        if (validateStep3() == false) {
            isStepValid = false;
            $('#wizard').smartWizard('setError', {
                stepnum: 3,
                iserror: true
            });
        } else {
            $('#wizard').smartWizard('setError', {
                stepnum: 3,
                iserror: false
            });
        }

        if (validateStep4() == false) {
            isStepValid = false;
            $('#wizard').smartWizard('setError', {
                stepnum: 4,
                iserror: true
            });
        } else {
            $('#wizard').smartWizard('setError', {
                stepnum: 4,
                iserror: false
            });
        }

        if (!isStepValid) {
            $('#wizard').smartWizard('showMessage', 'Please correct the errors in the steps and continue');
        }

        return isStepValid;
    }


    function validateSteps(step) {
        $('.hide-button').hide();
        var isStepValid = true;
        // validate step 1
        if (step == 1) {
            if (validateStep1() == false) {
                isStepValid = false;
                $('#wizard').smartWizard('showMessage', 'Please correct the errors in step' + step + ' and click next.');
                $('#wizard').smartWizard('setError', {
                    stepnum: step,
                    iserror: true
                });
            } else {
                $('#wizard').smartWizard('hideMessage');
                $('#wizard').smartWizard('setError', {
                    stepnum: step,
                    iserror: false
                });
            }
        }

        //   if(step == 2){
        //     if(validateStep2() == false ){
        //       isStepValid = false;
        //       $('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
        //       $('#wizard').smartWizard('setError',{stepnum:step,iserror:true});
        //     }else{
        //       $('#wizard').smartWizard('hideMessage');
        //       $('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
        //     }
        //   }

        // validate step3
        if (step == 3) {

            if (validateStep3() == false) {
                isStepValid = false;
                $('#wizard').smartWizard('showMessage', 'Please correct the errors in step' + step + ' and click next.');
                $('#wizard').smartWizard('setError', {
                    stepnum: step,
                    iserror: true
                });
            } else {
                $('#wizard').smartWizard('hideMessage');
                $('#wizard').smartWizard('setError', {
                    stepnum: step,
                    iserror: false
                });
            }
        }

        // validate step3
        if (step == 3) {
            if (validateStep3() == false) {
                isStepValid = false;
                $('#wizard').smartWizard('showMessage', 'Please correct the errors in step' + step + ' and click next.');
                $('#wizard').smartWizard('setError', {
                    stepnum: step,
                    iserror: true
                });
            } else {
                $('#wizard').smartWizard('hideMessage');
                $('#wizard').smartWizard('setError', {
                    stepnum: step,
                    iserror: false
                });
            }
        }

        return isStepValid;
    }

    function validateStep1() {
        var isValid = true;
        var ss = $('#select-services').val();
        var ssc = $('#select-service-category').val();
        var sst = $('#select-service-type').val();
        var spg = $('#select-product_group').val();
        var note = $('#note').val();
        if (ss == '') {
            isValid = false;
            Helper.warningNotif('Please select your services!');
            return false;
        } else if (!ssc) {
            isValid = false;
            Helper.warningNotif('Please select your services category!');
            return false;
        } else if (!sst) {
            isValid = false;
            Helper.warningNotif('Please select your services type!');
            return false;
        } else if (!spg) {
            isValid = false;
            Helper.warningNotif('Please select your product group!');
            return false;
        }
        // else if(note == ""){
        //     isValid = false;
        //     Helper.warningNotif('Please fill your additional info!');
        //     return false;
        // }
        return isValid;
    }

    function validateStep2() {
        var isValid = true;
        var pn = $('#product_name').val();
        var bn = $('#brand_name').val();
        var mn = $('#model_name').val();
        var sn = $('#serial_number').val();
        var r = $("#remark").val();
        var spg = $("#select-product_group").val();
        if (pn == '') {
            isValid = false;
            Helper.warningNotif('Please fill or select your product name!');
            return false;
        } else if (bn == '') {
            isValid = false;
            Helper.warningNotif('Please fill or select your brand name!');
            return false;
        } else if (mn == '') {
            isValid = false;
            Helper.warningNotif('Please fill or select your model name!');
            return false;
        } else if (sn == '') {
            isValid = false;
            Helper.warningNotif('Please fill or select your serial number!');
            return false;
        } else if (r == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your remark!');
            return false;
        } else if (!spg) {
            isValid = false;
            Helper.warningNotif('Please select your product group!');
            return false;
        }
        return isValid;
    }

    function validateStep3() {



        var isValid = true;
        var id_techician = $('.selected-technician:checked').map(function() {
            return $(this).attr('teknisi-id')
        }).get();
        var total_prices = $('.selected-technician:checked').map(function() {
            return $(this).attr('total-prices')
        }).get()
        $('#teknisi-dipilih').val(id_techician);
        // $('#technician_id').val(id_techician);
        $('#prices').val(total_prices);
        if (!$('.selected-technician').is(':checked')) {
            isValid = false;
            Helper.warningNotif('Please select your technicians!');
            return false;
        } else if (parseInt($("#wallet_user").val()) <= parseInt($('#prices').val())) {
            $(this).prop('checked', false);
            $('#modalNotice').modal('show');
            return false;
        }
        if (parseInt($("#wallet_user").val()) <= parseInt(total_prices)) {
            isValid = false;
            $(this).prop('checked', false);
            $('#modalNotice').modal('show');
            return false;
        }
        return isValid;
    }

    function validateStep4() {
        var isValid = true;
        //validate email  email
        now = new Date();
        var sd = $('#symptom_detail').val();
        var hours = $('.hours:checked').map(function() {
            return $(this).val();
        }).get();
        var now = moment(now).format('YYYY-MM-DD');
        if (sd == '') {
            isValid = false;
            Helper.warningNotif('Please fill your simptom detail!');
            return false;
        } else if ($('#schedule').val() == '') {
            isValid = false;
            Helper.warningNotif('Please select your schedule date!');
            return false;
        } else if ($('#schedule').val() < now) {
            isValid = false;
            Helper.warningNotif("Schedule must be in the future !");
            return false;
        } else if (!$('.hours').is(':checked')) {
            isValid = false;
            Helper.warningNotif("Please select your schedule hours!");
            return false;
        }
        return isValid;
    }

    // // Email Validation
    // function isValidEmailAddress(emailAddress) {
    //   var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    //   return pattern.test(emailAddress);
    // }
</script>
<!-- End Smart Wizard -->

<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" rel="stylesheet" />
<script type="text/javascript">
    var TransactionApp = {
        initialize: function() {
            //Datetimepicker plugin
            this.serviceChangeEvent();
            // agar user profile bisa diclik
            $(".show-userinfo-menu").click(function() {
                $('.widget-content-wrapper').toggleClass('show')
                $('.dropdown-menu-right').toggleClass('show')
            })

            $('.ganti-alamat').click(function(e) {
                $('#modal-alamat').modal('show')
                e.preventDefault();
            });
            $('.radio-alamat').change(function() {
                id = $(this).attr('data-id');
                type_address = $(this).attr('data-type_address');
                detail_address = $(this).attr('data-detail_address');

                $('[name="address_id"]').val(id);
                $('.type-address-text').text(type_address);
                $('.detail-address-text').text(detail_address);

                $('#modal-alamat').modal('hide')
            })

            $(".show-userinfo-menu").click(function() {
                $('.widget-content-wrapper').toggleClass('show')
                $('.dropdown-menu-right').toggleClass('show')
            })

            $('.btn-sort').click(function() {
                $('.btn-sort-area').toggleClass('show');
            })

            $(document).on('click', '.selected-technician', function() {

                $('#schedule').val('');
                $('#display_hours_available').html('');
                var id_techician = $('.selected-technician:checked').map(function() {
                    return $(this).attr('teknisi-id')
                }).get();
                var total_prices = $('.selected-technician:checked').map(function() {
                    return $(this).attr('total-prices')
                }).get()
                $('#teknisi-dipilih').val(id_techician);
                // $('#technician_id').val(id_techician);
                $('#prices').val(total_prices);

                // if(parseInt($("#wallet_user").val()) <= parseInt(total_prices)) {
                //     $(this).prop('checked', false);
                //     $('#modalNotice').modal('show');
                // }

                Helper.dateScheduleJob('#schedule', id_techician);

            });

            $(document).on('click', '.hours', function() {
                if ($.inArray($(this).val(), $('#hours_disable').val().split(',')) !== -1) {
                    $(this).prop("checked", false);
                    Helper.warningNotif('Hours ' + $(this).val() + ' is disabled!');
                }
            })

        },

        serviceChangeEvent: function() {
            globalCRUD.select2("#select-services", '/services/select2');

            $('#select-service-category').prop('disabled', true);
            $('#select-service-type').prop('disabled', true);
            $('#select-product_group').prop('disabled', true);

            // change service
            $(document).on('change', '#select-services', function() {
                TransactionApp.handleChangeServiceSelect($(this).val());
            });

            // change symtom
            $(document).on('change', '#select-service-category', function() {
                TransactionApp.handleChangeServiceCategorySelect($(this).val());
            });

            // change product_group
            $(document).on('change', '#select-service-type', function() {
                TransactionApp.handleChangeServiceTypeSelect($(this).val());
            });

            $(document).on('change', '#select-product_group', function() {
                TransactionApp.autoCompleteProductDetail($(this).val(), 'product_group', 'product_name');
            });
            $(document).on('change', '#product_name', function() {
                TransactionApp.autoCompleteProductDetail($(this).val(), 'product_name', 'brand_name');
            });
            $(document).on('change', '#brand_name', function() {
                TransactionApp.autoCompleteProductDetail($(this).val(), 'brand_name', 'model_name');
            });
            $(document).on('change', '#model_name', function() {
                TransactionApp.autoCompleteProductDetail($(this).val(), 'model_name', 'serial_number');
            });
        },

        autoCompleteProductDetail: function(val, type, el) {
            var type = type;
            var el = el;
            var options = {
                url: function(phrase) {
                    return Helper.apiUrl('/pdi/name');
                },
                adjustWidth: false,

                getValue: function(element) {
                    return element[el];
                },

                template: {
                    type: "custom",
                    method: function(value, item) {
                        return value + ' - <i>Serial Number (' + item.serial_number + ')</i>';
                    }
                },


                ajaxSettings: {
                    dataType: "json",
                    method: "POST",
                    data: {
                        dataType: "json",
                        val: val,
                        type: type,
                        el: el
                    }
                },

                list: {
                    onSelectItemEvent: function() {

                        if (el === 'product_name') {
                            let brand_name = $("#product_name").getSelectedItemData().brand_name;
                            let model_name = $("#product_name").getSelectedItemData().model_name;
                            let serial_number = $("#product_name").getSelectedItemData().serial_number;
                            let remark = $("#product_name").getSelectedItemData().remark;
                            $("#brand_name").val(brand_name).trigger("change");
                            $("#model_name").val(model_name).trigger("change");
                            $("#serial_number").val(serial_number).trigger("change");
                            $("#remark").val(remark).trigger("change");
                        } else if (el === 'brand_name') {
                            let model_name = $("#brand_name").getSelectedItemData().model_name;
                            let serial_number = $("#brand_name").getSelectedItemData().serial_number;
                            let remark = $("#brand_name").getSelectedItemData().remark;
                            $("#model_name").val(model_name).trigger("change");
                            $("#serial_number").val(serial_number).trigger("change");
                            $("#remark").val(remark).trigger("change");
                        } else if (el === 'model_name') {
                            let serial_number = $("#model_name").getSelectedItemData().serial_number;
                            let remark = $("#model_name").getSelectedItemData().remark;
                            $("#serial_number").val(serial_number).trigger("change");
                            $("#remark").val(remark).trigger("change");
                        } else if (el === 'serial_number') {
                            let remark = $("#serial_number").getSelectedItemData().remark;
                            $("#remark").val(remark).trigger("change");
                        }

                    },
                    match: {
                        enabled: true
                    }
                },

                preparePostData: function(data) {
                    data.phrase = $("#" + el + "").val();
                    return data;
                },

                requestDelay: 400
            };

            $("#" + el + "").easyAutocomplete(options);
        },

        handleChangeServiceSelect: function(service_id) {
            globalCRUD.select2("#select-service-category", '/admin/symptom/select2/' + service_id);
            $('#select-service-category').empty();
            $('#select-service-type').empty();
            $('#select-product_group').empty();
            $('#select-service-category').prop('disabled', false);
            $('#select-service-type').prop('disabled', true);
            $('#select-product_group').prop('disabled', true);
            // change symtom
            $(document).on('change', '#select-service-category', function() {
                TransactionApp.handleChangeServiceCategorySelect($(this).val());
            });

            // change product_group
            $(document).on('change', '#select-service-type', function() {
                TransactionApp.handleChangeServiceTypeSelect($(this).val());
            });

            $(document).on('change', '#select-product_group', function() {
                Axios.get('/customer/request-job/list-technician?service_type_id=' + $('#select-service-type').val() + '&product_group_id=' + $('#select-product_group').val() + '').then(function(response) {
                    $('#list-technicians').html(response.data.html);
                }).catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
            });
        },

        handleChangeServiceCategorySelect: function(symptom_id) {
            $('#select-service-type').empty();
            $('#select-product_group').empty();

            $('#select-service-type').prop('disabled', false);
            $('#select-product_group').prop('disabled', true);
            globalCRUD.select2("#select-service-type", '/service_type/select2_symptom/' + symptom_id);

        },

        handleChangeServiceTypeSelect: function(type_id) {
            $('#select-product_group').empty();
            $('#select-product_group').prop('disabled', false);
            globalCRUD.select2("#select-product_group", '/product_groups/select2/' + type_id);

        },
    }

    TransactionApp.initialize();
</script>
<script type="text/javascript" src="{{ asset('adminAssets/js/filer/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminAssets/js/filer/jquery.filer.min.js') }}"></script>

<script type="text/javascript">
    $(function() {
        $('body').on('click', '.pagination a', function(e) {
            e.preventDefault();

            $('#load a').css('color', '#dfecf6');
            $('#load').append('<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />');
            var url = $(this).attr('href');
            if ($('#teknisi-dipilih').val() != '') {
                url = $(this).attr('href') + '&teknisi-dipilih=' + $('#teknisi-dipilih').val() + '';
            }

            getTechnicians(url, $('#sort').val());
        });

        $(document).on('change', '#sort', function(e) {
            e.preventDefault();
            Helper.loadingStart();
            var url = $('#url').val();
            url = removeParamUrl('sort_by', url);
            url = url + '&sort_by=' + $(this).val() + '';
            getTechnicians(url, $(this).val());
        });

        function getTechnicians(url, sort = null) {
            Helper.loadingStart();
            console.log(url);

            var sort = sort;
            $.ajax({
                url: url,
                method: 'get'
            }).done(function(res) {
                $('#list-technicians').html(res.html);

                $('#sort').val(sort);
                $('#myCheckbox' + $('#teknisi-dipilih').val() + '').prop('checked', true);

                $('.hide-button').hide();
                Helper.loadingStop();
            }).fail(function() {
                Helper.warningNotif('Technicians could not be loaded.');
            });
        }

        function removeParamUrl(key, sourceURL) {
            var rtn = sourceURL.split("?")[0],
                param,
                params_arr = [],
                queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
            if (queryString !== "") {
                params_arr = queryString.split("&");
                for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                    param = params_arr[i].split("=")[0];
                    if (param === key) {
                        params_arr.splice(i, 1);
                    }
                }
                rtn = rtn + "?" + params_arr.join("&");
            }
            return rtn;
        }

    });
</script>


@endsection
