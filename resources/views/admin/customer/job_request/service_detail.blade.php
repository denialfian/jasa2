@extends('admin.home')
@section('content')

<style>
    .rounded-profile {
    border-radius: 100% !important;
    overflow: hidden;
    width: 100px;
    height: 100px;
    border: 8px solid rgba(255, 255, 255, 0.7);
}
</style>
<div class="col-md-12">
    {{-- @if ($order->orders_statuses_id == 3 || $order->orders_statuses_id == 4 )
        @if($grand_total >= $order->user->masterwallet->nominal)
            <div class="alert alert-warning" role="alert">
                LESS BALANCE ORDER
                @if($order->orders_statuses_id == 9 && $order_is_approve == 1)
                    <button class="btn_cancel_is_approve btn btn-sm btn-danger" data-order ="{{ $order->id }}" data-toggle="tooltip" data-html="true" title="<b>Cancel Jobs</b>"><i class="fa fa-close"></i></button>
                @else
                    <button type="button" class="mb-2 mr-2 btn btn-sm btn-danger btn_cancel_jobs" data-order ="{{ $order->id }}" data-toggle="tooltip" data-html="true" title="<b>Cancel Jobs</b>" style="float:right">
                        <i class="fa fa-close"></i> CANCEL SERVICES
                    </button>
                @endif
                <button type="button" class="btn_less_balance mb-2 mr-2 btn btn-sm btn-primary" data-nominal="0" data-id="{{ $order->id }}" data-selisih="{{ $diff }}" data-toggle="modal" data-target="#lessBalanceModal" style="float:right"><i class="fa fa-check"></i> ACCEPTED SERVICES</button>
            </div>
        @endif
    @endif --}}

    @if ($order->orders_statuses_id == 3 && $order->garansi == null)
        <div class="alert alert-warning alert-dismissible show">
            @if($grand_total >= $order->user->masterwallet->nominal )
                <strong>NEW EXTRA PARTS</strong>&nbsp;&nbsp;
                <button type="button" class="btn_less_balance badge badge-warning" data-nominal="0" data-id="{{ $order->id }}" data-selisih="{{ $diff }}" data-toggle="modal" data-target="#lessBalanceModal">Accepted Service</button>
            @else
                <strong>WAITING APPROVAL </strong>&nbsp;&nbsp;
                <button type="button" class="badge badge-primary btn_accepted_job" data-order-accept ="{{ $order->id }}" style="border: none">Accept Jobs</button>
            @endif

            <input type="hidden" name="id" value="{{ $order->id }}">
            <button type="button" class="badge badge-danger btn_cancel_jobs" data-order ="{{ $order->id }}" style="border: none">Cancel Jobs</button>
        </div>
    @endif

    @if ($order->orders_statuses_id == 9)
        @if($order->is_approve == 1 && $order->garansi == null)
            <div class="alert alert-warning alert-dismissible show">
                <strong>PROCESSING </strong>&nbsp;&nbsp;
                <button type="button" class="badge badge-danger btn_cancel_is_approve" data-order ="{{ $order->id }}" data-toggle="tooltip" data-html="true" title="<b>Cancel Jobs</b>" style="border: none">Cancel Jobs</button>
            </div>
        @endif
    @endif
</div>

<div class="col-md-12">
    @if ($order->orders_statuses_id == 7 )
        <div class="alert alert-warning alert-dismissible show">
            <strong>Warning !</strong> Pleace Approve Where Order Code : {{ $order->code }}.
            <a href="" type="button" data-completed_jobs ="{{ $order->id }}" class="badge badge-primary badge-sm append_id" data-toggle="modal" data-target="#exampleModalLong">Completed Jobs</a>
            {{-- <button type="button" class="badge badge-primary approve" data-id="{{ $order->id }}" style="border: none">Approve</button> --}}
            <input type="hidden" name="id" value="{{ $order->id }}">
            <button type="button" class="badge badge-danger btn_complaint" data-toggle="modal" data-target="#exampleModal" style="border: none">Complaint</button>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document" style="overflow-y: initial !important">
                <div class="modal-content">
                    <input type="hidden" id="completed_jobs" name="completed_jobs">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">{{ $getTitle->value ?? '' }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="height: 250px;overflow-y: auto;">
                        {!! $getDesc->value ?? '' !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-sm approve" data-id="{{ $order->id }}" style="border: none">Complete Jobs</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>

<div class="col-md-12">
    <div class="card">
        <div class="card-header header-bg">
            Order #{{ $order->code }}
        </div>
    </div>
</div>

<div class="col-md-4" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Order Detail

            <div class="btn-actions-pane-right text-capitalize">

            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Order No:</label>
                            <p>{{ $order->code }}</p>
                        </div>
                        <div class="col-md-6">
                            <label>Product Detail :</label>
                            <p><a href="" data-toggle="modal" data-target="#productDetail" style="text-decoration: underline">Product Detail</a></p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="productDetail" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Product Detail</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <label>Product Name</label>
                        <div class="form-group">
                            <input type="text" class="form-control" value="{{ $order->product_name_detail }}" readonly>
                        </div>

                        <label>Brand Name</label>
                        <div class="form-group">
                            <input type="text" class="form-control"  value="{{ $order->brand_name_detail }}" readonly>
                        </div>

                        <label>Model Name</label>
                        <div class="form-group">
                            <input type="text" class="form-control"  value="{{ $order->model_name_detail }}" readonly>
                        </div>

                        <label>Serial Number</label>
                        <div class="form-group">
                            <input type="text" class="form-control" value="{{ $order->serial_number_detail }}" readonly>
                        </div>

                        <label>Remark</label>
                        <div class="form-group">
                            <textarea name="" class="form-control" id="" readonly>{{ $order->remark_detail }}</textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
            </div>

            <label>Schedule:</label>
            <p id="schedule_display">
                <div class="badge badge-pill badge-primary">{{ $order->schedule->format('d F Y G:i') }}</div>
            </p>

            <label>Order Status:</label>
            <p><span class="mb-2 mr-2 badge badge-pill badge-dark">{{ $order->order_status->name }}</span></p>

            <label>Order Note:</label>
            <p>{{ $order->note }}</p>

            <label>Symptom detail:</label>
            <div class="alert alert-info" role="alert">
                {{ $order->symptom_detail !== null ? $order->symptom_detail : "-" }}
            </div>
            <label for="ms_symptom_code">Symptom Code</label>
            <p>{{ $order->symptom_code == null ? '-' : $order->symptom_code->name }}</p>
            <label for="ms_symptom_code">Repair Code</label>
            <p>{{ $order->repair_code == null ? '-' : $order->repair_code->name }}</p>
            <label for="repair_desc">Repair Description</label>
            <p>{{ $order->repair_desc == null ? '-' : $order->repair_desc }}</p>

            <label>Payment Type:</label>
            <br />
            {!! $order->payment_type == 0 ? '<div class="badge badge-danger">Offline</div>' : '<div class="badge badge-info">Online</div>' !!}

            @if($order->date_complete != null && $order->garansi == null)
            <hr>
            <label>Due Date Claim: </label>
            @if($order->date_complete->addDays($garansi_setting->value)->gt(now()))
            <p>{{ $order->date_complete->addDays($garansi_setting->value)->format('d F Y') }} / {{ $order->date_complete->diff(date('Y-m-d', strtotime($order->date_complete. ' + '.$garansi_setting->value.' days')))->format('%d days, %h hours and %i minutes') }} Left</p>
            <br>
            <button class="btn btn-success btn-sm btn-claim-garansi"><i class="fa fa-arrow-right"></i> Klaim Garansi</button>
            @else
            <p>Expired</p>
            @endif
            @endif

            @if($order->garansi != null)
            <hr>
            <a href="{{ url('customer/garansi/'.$order->garansi->id.'/detail') }}" class="btn btn-success btn-sm"><i class="fa fa-envelope"></i> Detail Claim</a>
            @endif

            <div class="text-right" style="margin-top: 10px;">
                @if($order->orders_statuses_id == 9)
                <label>Delivery Note : &nbsp;&nbsp;</label>
                <a href="{{ url('/admin/order/workmanship_report_pdf/'. $order_id ) }}" class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-sm btn-outline-warning"><i class="fa fa-file-pdf"></i></a>
                <small>.pdf</small>
                @endif
                <br />
                @if ($order->orders_statuses_id == 1 || $order->orders_statuses_id == 2 || $order->orders_statuses_id == 5 || $order->orders_statuses_id == 6)
                <label>Invoice: &nbsp; - </label>
                @else
                <label>Invoice: &nbsp;&nbsp;</label>
                <a href="{{ url('/admin/invoice/'. $order_id ) }}" class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-sm btn-outline-danger" target="_blank"><i class="fa fa-file-pdf"></i></a>
                <small>.pdf</small>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="col-md-4" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Billing Detail
            <!-- <div class="btn-actions-pane-right text-capitalize">
                <button type="button" id="edit_billing" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit Billing</button>
            </div> -->
        </div>
        <div class="card-body">
            <div class="mb-3 profile-responsive card">
                <div class="dropdown-menu-header">
                    <div class="dropdown-menu-header-inner">
                        <div class="menu-header-image opacity-2"></div>
                        <div class="menu-header-content">
                            <div class="avatar-icon-wrapper mr-3 avatar-icon-xl btn-hover-shine">
                                <div class="avatar-icon rounded">
                                    <img class="rounded-circle" src="{{ $order->user->avatar }}" alt="Avatar 5">
                                </div>
                            </div>
                            <div style="color: black;">
                                <h5 class="menu-header-title">{{ $order->customer->full_name }}</h5>
                                <h6 class="menu-header-subtitle">{{ $order->customer_email }}</h6>
                                <h6 class="menu-header-subtitle">{{ $order->customer_phone }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <label>Address:</label>
                        <p>{{ $order->address_type_name }}</p>
                        <p>{{ $order->address }}</p>
                        <p><a href="" data-toggle="modal" data-target="#modal_maps" data-lat='{{ $order->user->address->latitude ?? '' }}' data-lng='{{ $order->user->address->longitude ?? '' }}' style="text-decoration: underline"><i class="fa fa-map-marker"></i> View Location</a></p>
                    </li>
                    <li class="list-group-item">
                        <label>Bill Address:</label>
                        <p>{{ $order->bill_to }}</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- Billing Amount -->
<div class="col-md-4" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Billing Amount
        </div>
        <div class="card-body bill-amount-area">
            <ul class="list-group">
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">Services {{ $order->service_name }}</div>
                            </div>
                        </div>
                    </div>
                </li>
                @foreach($order->service_detail as $key => $detail)
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                @if ($detail->symptom_name !== null && $detail->service_type_name !== null && $order->product_group_name !== null)
                                    <div class="widget-heading">{{ $detail->symptom_name }}</div>
                                    <div class="widget-subheading">{{ $detail->service_type_name }} {{ $order->product_group_name }}</div>
                                @else
                                    <div class="widget-heading">{{ $detail->symptom->name }}</div>
                                    <div class="widget-subheading">{{ $detail->services_type->name }} {{ $order->product_group->name }}</div>
                                    <div class="widget-subheading">Qty : {{ $detail->unit }} x</div>
                                    <input type="hidden" value="{{ $detail->unit }}" class="unit_services">
                                @endif
                            </div>
                            <div class="widget-content-right">
                                <div class="text-primary"><span> <strong>Rp. {{ number_format($detail->price) }}</strong></span></div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">Sparepart</div>
                            </div>
                        </div>
                    </div>
                </li>
                @foreach($sparepart_details as $sparepart_detail)
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">{{ $sparepart_detail->name_sparepart }}</div>
                                <div class="widget-subheading">{{ $sparepart_detail->priceThousandSeparator() }} x {{ $sparepart_detail->quantity }}</div>
                            </div>
                            <div class="widget-content-right">
                                <div class="text-primary"><span><strong>Rp. {{ \App\Helpers\thousanSparator($sparepart_detail->quantity * $sparepart_detail->price) }}</strong></span></div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
                @foreach($item_details as $item)
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">{{ $item->name_product }}</div>
                                <div class="widget-subheading">{{ $item->price }} x {{ $item->quantity }}</div>
                            </div>
                            <div class="widget-content-right">
                                <div class="text-primary"><span> <strong>Rp. {{ \App\Helpers\thousanSparator($item->quantity * $item->price) }}</strong></span></div>
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach

                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">Grand Total</div>
                            </div>
                            <div class="widget-content-right">
                                <div class="text-primary"><span id="grand_total_order"> <span>Rp. {{ \App\Helpers\thousanSparator($order->grand_total) }}</span></span></div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

</div>

<div class="col-md-8" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Detail sparepart
        </div>
    </div>

    {{-- @if(!in_array($order->orders_statuses_id, [7, 10]))
    @include('admin.order._sparepart')
    @else --}}
    @include('admin.customer.job_request.sparepart_view')
    {{-- @endif --}}

    <!-- media -->
    @include('admin.customer.job_request.order_media')
</div>

<?php $technician = $order->service_detail->first()->technician; ?>
<?php $teknisi = $order->service_detail->first()->teknisi; ?>
<div class="col-md-4" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Technician
            <div class="btn-actions-pane-right text-capitalize">
                {{-- @if($technician != null)
                <a target="__blank" href="{{ url('admin/user/detail/' .  $order->service_detail[0]->teknisi->user->id) }}" class="btn btn-sm btn-danger">
                    <i class="fa fa-eye"></i>
                </a>
                @endif --}}
                {{-- @if(!in_array($order->orders_statuses_id, [4, 7, 10]))
                <button type="button" class="btn btn-sm btn-success assign-mechanic"><i class="fa fa-plus"></i> Assign New Mechanic</button>
                @endif --}}
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="profile-sidebar">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic text-center">
                    <div class="avatar-upload">
                        <div class="avatar-preview">
                            <div id="imagePreview" style="background-image: url( {{ $technician->user->avatar }} )"></div>
                        </div>
                    </div>
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                        <h3 class="name">{{ $technician->user->name }}</h3>
                        <p class="name">{{ $technician->user->email }}</p>
                    </div>
                    <div class="profile-usertitle-job">
                        <hr />
                        <span class="point">{{ $technician == null ? '' : '(' . $technician->getFormatRatingValue() . ')' }}</span>
                        {!! $technician == null ? '' : $technician->getRatingStarUi() !!}
                        @if($technician != null)
                        @if($technician->total_review > 0)
                        <br />
                        <span class="amount">
                            ({{ $technician->total_review }}) Reviews
                        </span>
                        @endif
                        @else
                        <button class="btn btn-sm btn-danger">
                            <i class="fa fa-close"></i> Deleted <i class="fa fa-close"></i>
                        </button>
                        @endif
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <div class="col-md-12" style="margin-top: 10px;">
    <div class="card-shadow-alternate border mb-3 card card-body border-alternate">
        <h5>Technician</h5>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    @if($order->service_detail[0]->technician != null)
                        <div class="col-md-4">
                            <img class="rounded-profile" src="{{ $order->service_detail[0]->technician->user->avatar }}" alt="">
                        </div>
                        <div class="col-md-8" style="padding:15px; margin-left: -25px;">
                            <div class="col-xs-12" style="margin-bottom: 15px;">
                                <h4>{{ $order->service_detail[0]->technician->user->full_name }}</h4>
                                <h6><i class="fa fa-envelope"></i> {{ $order->service_detail[0]->technician->user->email}}</h6>
                                <div class="review-details" style="margin-left:-40px; margin-top:10px;">
                                    <ul class="review">
                                        <li class="point">
                                        {{ $order->service_detail->first()->technician->getFormatRatingValue() }}
                                        </li>
                                        {!! $order->service_detail->first()->technician->getRatingStarUi() !!}
                                    </ul>

                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-md-12"><h5>Still Pending. You haven't looked for a technician!</h5></div>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="text-right" style="margin-bottom:47px;">
                    <h4>{{ $order->order_status->name }}</h4>
                </div>
                @if($order->service_detail[0]->technician != null)
                    <div class="text-right">
                        <button type="button" href="#" class="mb-2 mr-2 btn btn-sm btn-alternate">Contact Customer Support</button>
                        <button type="button" href="#" class="mb-2 mr-2 btn btn-sm btn-alternate delete-selected">Contact Mechanic</button>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div> --}}

<div class="col-md-12" style="margin-top: 10px;">
    <div class="card-shadow-alternate border mb-3 card card-body border-alternate">
        <h5>History Order</h5>
        <div class="price scroll-area-sm" style="margin-top:25px; margin-bottom: 30px; height:200px">
            <div class="scrollbar-container ps--active-y ps">
                <div class="vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                    @foreach($order->history_order->reverse() as $key => $history_order)
                    <?php
                        $color = '';
                        $text = '';
                        if($history_order->orders_statuses_id == 1) {
                            $color = 'primary';
                            $text = 'New order has created - ';
                        }
                        elseif($history_order->orders_statuses_id == 2) {
                            $color = 'warning';
                            $text = 'Job has pending, need waiting confirmation at - ';
                        }
                        elseif ($history_order->orders_statuses_id == 3) {
                            $color = 'success';
                            $text = 'Job has approved by '.$order->service_detail[0]->technician->user->name.' at - ';
                        }
                        elseif ($history_order->orders_statuses_id == 4) {
                            $color = 'success';
                            $text = 'Payment successfully paid at - ';
                        }
                        elseif ($history_order->orders_statuses_id == 5) {
                            $color = 'danger';
                            $text = 'Order has canceled at - ';
                        }
                        elseif ($history_order->orders_statuses_id == 6) {
                            $color = 'warning';
                            $text = 'Order has rescheduled at - ';
                        }
                        elseif ($history_order->orders_statuses_id == 7) {
                            $color = 'success';
                            $text = 'Job has done ! at - ';
                        }
                        elseif ($history_order->orders_statuses_id == 9) {
                            $color = 'warning';
                            $text = 'Job Has Been Processing ! at - ';
                        }
                        elseif ($history_order->orders_statuses_id == 10) {
                            $color = 'primary';
                            $text = 'Job Completed ! at - ';
                        }
                    ?>
                    <div class="vertical-timeline-item vertical-timeline-element">
                        <div><span class="vertical-timeline-element-icon bounce-in"><i class="badge badge-dot badge-dot-xl badge-{{ $color }}"> </i></span>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h2 class="timeline-title">{{ $history_order->order_status->name }}</h2>
                                <p>{{ $text }}<b class="text-{{ $color }}">{{ $history_order->created_at }}</b></p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            {{-- <div class="col-md-6">
                <div class="text-right" style="margin-bottom:47px;">
                    <h4>{{ $order->order_status->name }}</h4>
                </div>
                @if($order->service_detail[0]->technician != null)
                    <div class="text-right">
                        <button type="button" href="#" class="mb-2 mr-2 btn btn-sm btn-alternate">Contact Customer Support</button>
                            <button type="button" href="#" class="mb-2 mr-2 btn btn-sm btn-alternate delete-selected">Contact Mechanic</button>
                    </div>
                @endif
            </div> --}}
        </div>
    </div>
</div>






<!-- @if(count($sparepart_details) > 0 || count($item_details) > 0)
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Additional sparepart from
        </div>
    </div>
</div>
@endif -->

<!-- @if(count($sparepart_details) > 0)
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header">
                My Inventory
            </div>
            <div class="card-body">
                <div class="row">
                    @foreach($sparepart_details as $sparepart_detail)
                    <div class="col-md-4">
                        <img class="img-responsive" width="100px" height="100px" src="{{ $sparepart_detail->image_sparepart }}" alt="Ash" />
                    </div>
                    <div class="col-md-8">
                        <table class="table table-bordered table-sm table-no-margin">
                            <tr>
                                <td>Name</td>
                                <td>{{ $sparepart_detail->name_sparepart }}</td>
                            </tr>
                            <tr>
                                <td>Price</td>
                                <td>Rp.{{ $sparepart_detail->priceThousandSeparator() }}</td>
                            </tr>
                            <tr>
                                <td>Qty</td>
                                <td>{{ $sparepart_detail->quantity }}</td>
                            </tr>
                        </table>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="card-footer pull-right">
                <h6><b>TOTAL : </b>Rp. {{ $sparepart_details->sum(function($detail){ return $detail->price * $detail->quantity;}) }}</h6>
            </div>
        </div>
    </div>
@endif

@if(count($item_details) > 0)
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header">
                Company Inventory
            </div>
            <div class="card-body">
                <div class="row">
                    @foreach($item_details as $item)
                    <div class="col-md-2">
                        <img class="img-responsive" width="100px" height="100px" src="{{ $item->inventory->batch_item->product->main_image }}" alt="Ash" />
                    </div>
                    <div class="col-md-10">
                        <table class="table table-bordered table-sm table-no-margin nowrap">
                            <tr>
                                <td>Name</td>
                                <td>{{ $item->inventory->batch_item->product_name }}</td>
                            </tr>
                            <tr>
                                <td>Price</td>
                                <td>Rp.{{ number_format($item->price) }}</td>
                            </tr>
                            <tr>
                                <td>Qty</td>
                                <td>{{ $item->quantity }}</td>
                            </tr>
                        </table>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="card-footer pull-right">
                <h6><b>TOTAL : </b>Rp. {{ $item_details->sum(function($detail){ return $detail->price * $detail->quantity;}) }}</h6>
            </div>
        </div>
    </div>
@endif -->

<input type="hidden" name="commission_value" value="0">
<input type="hidden" name="early_total" value="{{ $order->grand_total }}">
<input type="hidden" name="total" value="{{ $order->after_commission }}">

@endsection
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Complaint Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger msg_error" role="alert" id="alert_condition">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>You must be Agree to our term and condition </strong>
                </div>
                <div>
                    <label for="" class="">Status Order</label>
                    <input type="text" name="orders_statuses_id" value="Complaint" class="form-control" readonly>
                </div>
                <br>
                <div>
                    <label for="" class="">Note <span class="required" style="color:red">*</span></label>
                    <textarea class="form-control" name="note_complaint" id="note_complaint" rows="3" required></textarea>
                    <span id="error-note_complaint"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" data-id="{{ $order->id }}" class="complaint btn btn-primary">Complaint</button>
            </div>
        </div>
    </div>
</div>

<!-- modal less balance -->
{{-- <form id="forms-topup"> --}}
    <div class="modal fade bd-example-modal-lg" id="lessBalanceModal" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Topup Form</h5>
                    <input type="hidden" name="id" id="data_id">
                    <input type="hidden" name="selisih" id="data_selisih">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <div class="modal-body">
                        <table class="table table-hover table-bordered" style="width:100%">
                            <thead>
                                <th>Sparepart Name</th>
                                <th>Sparepart qty</th>
                                <th>Total</th>
                            </thead>
                            @if(count($sparepart_details) > 0)
                                @foreach($sparepart_details as $sparepart_detail)
                                        <tbody>
                                            <tr>
                                                <td>{{ $sparepart_detail->name_sparepart }}</td>
                                                <td>{{ $sparepart_detail->quantity }}</td>
                                                <td>Rp. {{ $sparepart_detail->priceThousandSeparator() }}</td>
                                            </tr>
                                        </tbody>
                                @endforeach
                            @endif

                            @if(count($item_details) > 0)
                                @foreach($item_details as $item)
                                    <tbody>
                                        <tr>
                                            <td>{{ $item->inventory->batch_item->product_name }}</td>
                                            <td>{{ $item->quantity }}</td>
                                            <td>Rp. {{ $item->price }}</td>
                                        </tr>
                                    </tbody>
                                @endforeach
                            @endif
                        </table><hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-2"><strong>Note : </strong></div>
                                    <div class="col-md-10">Your wallet is not balanced with the grand total because there are some additional parts, please top up the form below</div>
                                </div>
                            </div>
                        </div><hr>
                        <div id="inputs">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="10000" /> Rp.10,000</div>
                                        <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="50000" /> Rp.50,000</div>
                                        <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="200000" /> Rp.200,000</div>
                                        <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="500000" /> Rp.500,000</div>
                                    </div>
                                </div>
                            </div><br>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Nominal Top up</label>
                            <input type="text" class="form-control number_only" name="nominal" placeholder="10000" value="{{ $difference }}" id="amount"/>
                            <input type="hidden" class="form-control" id="exampleFormControlInput1" name="note" placeholder="10000">
                            <small><strong>Note : </strong> You must top up by Rp. {{ number_format($difference) }}</small>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary waves-effect" id="topup_lessbalance" type="submit">
                        Top up
                    </button>
                </div>
            </div>
        </div>
    </div>
{{-- </form> --}}

<form id="form-garansi">
    <div class="modal fade" data-backdrop="false" id="modal-garansi" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Claim Garansi
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id-order" name="order_id" value="{{ $order->id }}">
                    <div class="position-relative form-group">
                        <label class="" for="">Subject</label>
                        <input name="subject" class="form-control" />
                    </div>
                    <div class="position-relative form-group">
                        <label class="" for="">Message</label>
                        <textarea name="desc" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SUBMIT
                    </button>
                    <button class="btn waves-effect" data-dismiss="modal" type="button">
                        CLOSE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
<style>
    /* Profile container */
    .profile {
      margin: 20px 0;
    }

    /* Profile sidebar */
    .profile-sidebar {
      background: #fff;
    }

    .profile-userpic img {
      float: none;
      margin: 0 auto;
      width: 50%;
      -webkit-border-radius: 50% !important;
      -moz-border-radius: 50% !important;
      border-radius: 50% !important;
    }

    .profile-usertitle {
      text-align: center;
      margin-top: 20px;
    }

    .profile-usertitle-name {
      color: #5a7391;
      font-size: 16px;
      font-weight: 600;
      margin-bottom: 7px;
    }

    .profile-usertitle-job {
      text-transform: uppercase;
      color: #555;
      font-size: 12px;
      font-weight: 600;
      margin-bottom: 15px;
    }

    .profile-userbuttons {
      text-align: center;
      margin-top: 10px;
    }

    .profile-userbuttons .btn {
      text-transform: uppercase;
      font-size: 11px;
      font-weight: 600;
      padding: 6px 15px;
      margin-right: 5px;
    }

    .profile-userbuttons .btn:last-child {
      margin-right: 0px;
    }

    .profile-usermenu {
      margin-top: 30px;
    }

    /* Profile Content */
    .profile-content {
      padding: 20px;
      background: #fff;
      min-height: 460px;
    }

    .avatar-upload {
      position: relative;
      max-width: 205px;
      margin: 0px auto;
    }
    .avatar-upload .avatar-edit {
      position: absolute;
      right: 12px;
      z-index: 1;
      top: 10px;
    }
    .avatar-upload .avatar-edit input {
      display: none;
    }
    .avatar-upload .avatar-edit input + label {
      display: inline-block;
      width: 34px;
      height: 34px;
      margin-bottom: 0;
      border-radius: 100%;
      background: #FFFFFF;
      border: 1px solid transparent;
      box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
      cursor: pointer;
      font-weight: normal;
      transition: all 0.2s ease-in-out;
    }
    .avatar-upload .avatar-edit input + label:hover {
      background: #f1f1f1;
      border-color: #d6d6d6;
    }
    .avatar-upload .avatar-edit input + label:after {
      content: "\f040";
      font-family: 'FontAwesome';
      color: #757575;
      position: absolute;
      top: 10px;
      left: 0;
      right: 0;
      text-align: center;
      margin: auto;
    }
    .avatar-upload .avatar-preview {
      width: 192px;
      height: 192px;
      position: relative;
      border-radius: 100%;
      border: 6px solid #F8F8F8;
      box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview > div {
      width: 100%;
      height: 100%;
      border-radius: 100%;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
    }

</style>
@section('script')
@include('admin.order.service_detail_style')
<link rel="stylesheet" href="{{ asset('adminAssets/css/frontend.css') }}" />
<script>
    $(document).on('click', '.btn_complaint', function(){
        $('.msg_error').hide();
    });

    $(document).on('click', '.btn_less_balance', function() {
        $('.hamburger--elastic').addClass('is-active');
        $('.fixed-sidebar').addClass('closed-sidebar');
        data_id = $(this).attr('data-id');
        data_selisih = $(this).attr('data-selisih');
        $('#data_id').val(data_id);
        $('#data_selisih').val(data_selisih);
        $('#lessBalanceModal').modal('show');
        Helper.onlyNumberInput('.number_only')
    })

    $('.radiogroup').change(function(e){
        var selectedValue = $(this).val();
        $('#amount').val(selectedValue)
    });

    // cancel order sebelum di accept customer penalty (75K)
    $(document).on('click', '.btn_cancel_jobs', function(e) {
        id = $(this).attr('data-order');
            $.ajax({
                url:Helper.apiUrl('/customer/durasi/' + id ),
                type: 'post',

                success: function(res) {
                    Helper.confirm(function(){
                        Helper.loadingStart();
                        $.ajax({
                            url:Helper.apiUrl('/customer/cancel-jobs/' + id ),
                            type: 'post',

                            success: function(res) {

                                Helper.successNotif('Cancel This Order Success !');
                                Helper.loadingStop();
                                Helper.redirectTo('/customer/dashboard');
                            },
                            error: function(res) {
                                Helper.errorNotif('Something Went Wrong');
                                console.log(res);
                                Helper.loadingStop();
                            }
                        })
                    },
                    {
                        title: "Are You Sure",
                        message: "<strong>Are you sure </strong></br> If this order is canceled you will be charged a deduction fee for your balance of <strong> Rp."+ res.data.setting1 +"</strong> ",
                    })
                },
                error: function(res) {
                    Helper.errorNotif('Something Went Wrong');
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        // {
        //     title: "Are You Sure",
        //     message: "<strong>Are you sure </strong></br> If this order is canceled you will be charged a deduction fee for your balance of <strong> IDR 70,000.00 </strong>, from the total service fee",
        // })

        e.preventDefault()
    })

    $(document).on('click', '#topup_lessbalance', function(){
        id = $('#getIds').val();
        Helper.loadingStart();
        $.ajax({
            data:{
                nominal : $('#amount').val(),
            },
            url: Helper.apiUrl('/customer/topup-wallet/less-balance-saved'),
            type: 'post',
            success: function(resp) {
                console.log(resp)
                Helper.successNotif('Topup Wallet Has been Success !');
                Helper.loadingStop();
                window.location.href = Helper.redirectUrl('/customer/topup/checkout/' + resp.data.id);
            },
            error: function(res, xhr, status, error) {
                Helper.errorNotif('Something Went Wrong');
                console.log(res);
                Helper.loadingStop();
            },
        })

    });

    // agar user profile bisa diclik
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })

    $(document).on('click', '.approve', function(e) {
        id = $(this).attr('data-id');
        $('#exampleModal').modal('hide');
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/customer/approve/' + id ),
                type: 'post',
                data : {
                    commission_value : $('input[name="commission_value"]').val(),
                    early_total : $('input[name="early_total"]').val(),
                    total : $('input[name="total"]').val(),
                },

                success: function(res) {
                    // Helper.successNotif('Success');
                    // Helper.loadingStop();
                    // Helper.redirectTo('/customer/request_list');
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })

        e.preventDefault()
    })

    $(document).on('click', '.complaint', function(e) {
        id = $(this).attr('data-id');
        // $('#exampleModal').modal('hide');
        // Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/customer/complaint/' + id ),
                type: 'post',
                data : {
                    note_complaint : $('textarea[name="note_complaint"]').val(),
                },

                success: function(res) {
                    Helper.successNotif('Success');
                    Helper.redirectTo('/customer/request_list');
                },
                error: function(xhr, status, error) {
                    if(xhr.status == 422){
                        error = xhr.responseJSON.data;
                        _.each(error, function(pesan, field){
                            $('#error-'+ field).text(pesan[0])

                        })
                        Helper.handleErrorResponse(error)
                    }
                            Helper.loadingStop();

                }
            })
        // })

        e.preventDefault()
    })

    $(document).on('click', '.btn_accepted_job', function(e) {
        id = $(this).attr('data-order-accept');
        Helper.confirm(function() {
            Helper.loadingStart();
            $.ajax({
                url: Helper.apiUrl('/customer/accept-order/' + id),
                type: 'post',
                success: function(res) {
                    Helper.successNotif('This Order Has Been Success !');
                    Helper.loadingStop();
                    Helper.redirectTo('/customer/dashboard');
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        }, {
            title: "Are You Sure",
            message: "Are You Sure, you will continue this offer?",
        })

        e.preventDefault()
    })

    // cancel order sesudah di accept customer penalty (50%)
    $(document).on('click', '.btn_cancel_is_approve', function(e) {
        id = $(this).attr('data-order');
        $.ajax({
            url: Helper.apiUrl('/customer/durasi/' + id),
            type: 'post',

            success: function(res) {
                // alert(res.data.jam)

                Helper.confirm(function() {
                    Helper.loadingStart();
                    $.ajax({
                        url: Helper.apiUrl('/customer/cancel-jobs-is-approve/' + id),
                        type: 'post',

                        success: function(res) {
                            Helper.successNotif('Cancel This Order Success !');
                            Helper.loadingStart();
                            Helper.loadingStop();
                            Helper.redirectTo('/customer/dashboard');
                        },
                        error: function(res) {
                            Helper.errorNotif('This service cannot be canceled because the mechanic will come to your place soon');
                            console.log(res);
                            Helper.loadingStop();
                        }
                    })
                }, {
                    title: "Are You Sure",
                    message: "<strong>Are you sure </strong></br> If this order is canceled you will be charged a deduction fee for your balance of <strong>" + (res.data.getSettingType == 1 ? res.data.getSettingValue + "%" : "Rp." + res.data.getSettingValue) + "</strong> ",
                    // from the total service fee, This offer can be canceled for up to <strong>" + res.data.jam + "</strong> hours
                })
            },
            error: function(res) {
                Helper.errorNotif('This service cannot be canceled because the mechanic will come to your place soon');
                console.log(res);
                Helper.loadingStop();
            }
        })


        e.preventDefault()
    })
</script>

<script>
    $(document).on('click', '.btn-claim-garansi', function() {
        $('#modal-garansi').modal('show');
    })

    $('#form-garansi').submit(function(e) {
        data = Helper.serializeForm($(this));

        Helper.loadingStart();
        // post data
        Axios.post(Helper.apiUrl('/customer/garansi'), data)
            .then(function(response) {
                Helper.successNotif('Success, Successfully!');
                window.location.href = Helper.redirectUrl('/customer/garansi/' + response.data.data.id + '/detail');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>
@include('admin.template._locationServiceDetail')
@endsection
