<br>
<div class="col-md-12">
    <div class="card">
        <div class="header-border card-header d-flex">
            <div class="mr-auto p-1 card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="p-2">Sort By : </div>
            <div class="p-3 ">
                <select class="form-control" name="sort" id="sort" style="width:100%">
                    <option value="">Nearest</option>
                    <option value="by_rating">Rating</option>
                    <option value="by_review">Review</option>
                    <option value="by_low_price">Lowest Price</option>
                    <option value="by_high_price">Highest Price</option>
                    <option value="by_newest">Newest</option>
                    <option value="by_name">Name</option>
                </select>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 5px">
    <input id="order_code" name="order_code" type="hidden" value="{{ $order_code }}">
    <input id="url" name="url" type="hidden" value="{{ url()->full() }}">
    <br>
    <section class="technicians" style="margin:10px;">
        @include('admin.customer.job_request.load_ajax')
    </section>
</div>

