<!-- Step Wizard Script -->
<style>

    .swMain {

      margin:0px;
      padding:0px;
      border: 0 solid #CCC;
      overflow:visible;
      float:left;
      width:100%;

    }
    .swMain .stepContainer {
      display:block;
      position: relative;
      margin: 0;
      padding:0;
      border: 0 solid #CCC;
      clear:both;
      height:300px;
      width:100%:
    }

    .swMain .stepContainer div.content {
      display:block;
      position: absolute;
      float:left;
      margin: 0;
      padding:10px;
      border: 1px solid #ccc;
      font: normal 12px Verdana, Arial, Helvetica, sans-serif;
      color:#5A5655;
      background-color:#F8F8F8;
      height:700px;
      text-align:left;
      /* overflow:scroll; */
      z-index:88;
      -webkit-border-radius: 5px;
      -moz-border-radius  : 5px;
      /* width:968px; */
      clear:both;
      width:100%;
      border-radius: 0px;
    }

    .swMain div.actionBar {
      display:block;
      position: relative;
      clear:both;
      margin:             3px 0 0 0;
      border:             1px solid #CCC;
      padding:            0;
      color:              #5A5655;
      background-color:   #F8F8F8;
      height:40px;
      text-align:left;
      overflow:auto;
      z-index:88;

      -webkit-border-radius: 5px;
      -moz-border-radius  : 5px;
      left:0;
    }

    .swMain .stepContainer .StepTitle {
      display:block;
      position: relative;
      margin:0;
      /* border:1px solid #E0E0E0; */
      padding:5px;
      font: bold 16px Verdana, Arial, Helvetica, sans-serif;
      color:#ffffff;
      background-color:#444054;
      clear:both;
      text-align:left;
      z-index:88;
      /* -webkit-border-radius: 5px;
      -moz-border-radius  : 5px; */
    }
    .swMain ul.anchor {
      position: relative;
      display:block;
      float:left;
      list-style: none;
      padding: 0;
      margin: 10px 0;
      clear: both;
      border: 0 solid #CCCCCC;
      background: transparent; /*#EEEEEE */
      width:100%;
    }
    .swMain ul.anchor li{
      position: relative;
      display:block;
      margin: 0;
      padding: 0 3px;
      border: 0 solid #E0E0E0;
      float: left;

    }
    /* Anchor Element Style */
    .swMain ul.anchor li a {
      display:block;
      position:relative;
      float:left;
      margin: 5px 0 0 0;
      padding:3px;
      height:80px;
      width:250px;
      text-decoration: none;
      outline-style:none;
      -moz-border-radius  : 5px;
      -webkit-border-radius: 5px;
      z-index:99;
    }
    .swMain ul.anchor li a .stepNumber{
      position:relative;
      float:left;
      width:30px;
      text-align: center;
      padding: 5px;
      font: bold 15px Verdana, Arial, Helvetica, sans-serif;
      border-radius: 0px;
    }
    .swMain ul.anchor li a .stepDesc{
      position:relative;
      display:block;
      float:left;
      text-align: left;
      padding:5px;

      font: bold 15px Verdana, Arial, Helvetica, sans-serif;
    }
    .swMain ul.anchor li a .stepDesc small{
      font: normal 12px Verdana, Arial, Helvetica, sans-serif;
    }
    .swMain ul.anchor li a.selected{
      color:#FFF;
      background: #2196F3;  /* EA8511 */
      border: 1px solid #2196F3;
      border-radius: 0px;
      cursor:text;
      /* -moz-box-shadow: 5px 5px 8px #888;
      -webkit-box-shadow: 5px 5px 8px #888;
      box-shadow: 5px 5px 8px #888; */
    }


    .swMain ul.anchor li a.done {
      position:relative;
      color:#FFF;
      background: #009688;
      border: 1px solid #009688;
      z-index:99;
    }

    .swMain ul.anchor li a.disabled {
      border-radius: 0px;
      color:#CCCCCC;
      background: #F8F8F8;
      border: 1px solid #CCC;
      cursor:text;
    }


    .swMain ul.anchor li a.error {
      color:#ffffff !important;
      background: #f44336  !important;
      border: 1px solid #f44336 !important;
    }


    .swMain .buttonNext {
      display:block;
      float:right;
      margin:5px 3px 0 3px;
      /* padding:5px; */
      text-decoration: none;
      text-align: center;
      font: bold 13px Verdana, Arial, Helvetica, sans-serif;
      width:100px;
      color:#FFF;
      outline-style:none;
      background-color:   #5A5655;
      /* border: 1px solid #5A5655; */
      /* -moz-border-radius  : 5px;
      -webkit-border-radius: 5px; */
    }
    .swMain .buttonDisabled {
      color:#F8F8F8  !important;
      background-color: #CCCCCC !important;
      border: 1px solid #CCCCCC  !important;
      cursor:text;
    }
    .swMain .buttonPrevious {
      display:block;
      float:right;
      margin:5px 3px 0 3px;
      padding:5px;
      text-decoration: none;
      text-align: center;
      font: bold 13px Verdana, Arial, Helvetica, sans-serif;
      width:100px;
      color:#FFF;
      outline-style:none;
      background-color:   #5A5655;
      border: 1px solid #5A5655;
      -moz-border-radius  : 5px;
      -webkit-border-radius: 5px;
    }
    .swMain .buttonFinish {
      display:block;
      float:right;
      margin:5px 10px 0 3px;
      padding:5px;
      text-decoration: none;
      text-align: center;
      font: bold 13px Verdana, Arial, Helvetica, sans-serif;
      width:100px;
      color:#FFF;
      outline-style:none;
      background-color:   #5A5655;
      border: 1px solid #5A5655;
      -moz-border-radius  : 5px;
      -webkit-border-radius: 5px;
    }

    /* Form Styles */

    .txtBox {
      border:1px solid #CCCCCC;
      color:#5A5655;
      font:13px Verdana,Arial,Helvetica,sans-serif;
      padding:2px;
      width:430px;
    }
    .txtBox:focus {
      border:1px solid #3f6ad8;
    }

    .swMain .loader {
      position:relative;
      display:none;
      float:left;
      margin: 2px 0 0 2px;
      padding:8px 10px 8px 40px;
      border: 1px solid #FFD700;
      font: bold 13px Verdana, Arial, Helvetica, sans-serif;
      color:#5A5655;
      background: #FFF url(../images/loader.gif) no-repeat 5px;
      -moz-border-radius  : 5px;
      -webkit-border-radius: 5px;
      z-index:998;
    }
    .swMain .msgBox {
      position:relative;
      display:none;
      float:left;
      margin: 4px 0 0 5px;
      padding:5px;
      border: 1px solid #FFD700;
      background-color: #FFFFDD;
      font: normal 12px Verdana, Arial, Helvetica, sans-serif;
      color:#5A5655;
      -moz-border-radius  : 5px;
      -webkit-border-radius: 5px;
      z-index:999;
      min-width:200px;
    }
    .swMain .msgBox .content {
      font: normal 12px Verdana,Arial,Helvetica,sans-serif;
      padding: 0;
      float:left;
    }
    .swMain .msgBox .close {
      border: 1px solid #CCC;
      border-radius: 3px;
      color: #CCC;
      display: block;
      float: right;
      margin: 0 0 0 5px;
      outline-style: none;
      padding: 0 2px 0 2px;
      position: relative;
      text-align: center;
      text-decoration: none;
    }
    .swMain .msgBox .close:hover{
      color: #3f6ad8;
      border: 1px solid #3f6ad8;
    }
    </style>
