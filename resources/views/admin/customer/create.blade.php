@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body" id="request_job">
            <form id="form-job">
                <input name="ms_service_id" type="hidden" value="{{ $service->id }}"/>
                <div class="position-relative form-group">
                    <label class="label-header" for="">
                        Keluhan
                    </label>
                    <div>
                        @foreach ($symptoms as $symptom)
                        <div class="custom-radio custom-control">
                            <input class="custom-control-input keluhan-radio" id="keluhan-{{ $symptom->id }}" name="symptom_id" type="radio" value="{{ $symptom->id }}"/>
                            <label class="custom-control-label" for="keluhan-{{ $symptom->id }}">
                                {{ $symptom->name }}
                            </label>
                        </div>
                        @endforeach
                    </div>
                </div>
                <hr />
                <label class="label-header" for="">
                    Layanan
                </label>
                <div class="form-row layanan-input-area">
                </div>
                <hr />
                <div class="form-row">
                    <div class="col-md-12">
                        <div class="position-relative form-group">
                            <label class="label-header" for="">
                                Alamat
                            </label>
                            <div>
                                <div class="list-group">
                                    @if ($main_address != null)
                                        <input type="hidden" name="address_id" value="{{ $main_address->id }}" />
                                        <label class="list-group-item">
                                                <span class="type-address-text">{{ $main_address->types->name }}</span>
                                                <p class="detail-address-text">{{ $main_address->address }}</p>
                                        </label>
                                    @endif
                                </div>
                                <a href="#" class="ganti-alamat"><i class="fa fa-pencil"></i> Change Primary Address</a>
                            </div>
                            <hr />
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="position-relative form-group">
                            <label class="label-header" for="">
                                Schedule
                            </label>
                            <input class="form-control datetimepicker" name="schedule" required="" type="text" id="schedule" readonly />
                        </div>
                        <hr />
                    </div>

                    <div class="col-md-12">
                        <div class="position-relative form-group">
                            <label class="label-header" for="">
                                Note
                            </label>
                            <textarea name="note" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger" type="button" id="submit-form">
                    Order Now
                </button>
            </form>
        </div>
        {{-- <div class="row " id="list_technicians" style="display:none;margin:10px;"></div> --}}
    </div>
</div>

<div class="modal fade" data-backdrop="false" id="modal-alamat" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Select Address
                </h4>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="list-group">
                    @foreach($addresses as $addr)
                    <input
                        data-type_address="{{ $addr->types->name }}"
                        data-detail_address="{{ $addr->address }}"
                        data-id="{{ $addr->id }}"
                        class="radio-alamat"
                        type="radio"
                        name="address_id"
                        value="{{ $addr->id }}"
                        id="Radio-{{ $addr->id }}"
                        {{ $addr->is_main == 1 ? 'checked' : '' }}/>
                    <label class="list-group-item" for="Radio-{{ $addr->id }}">
                        {{ $addr->types->name }}
                        <p>{{ $addr->address }}</p>
                    </label>
                    @endforeach
                </div>
                <hr/>
                <a href="/customer/profile/address/create"><i class="fa fa-plus"></i> Add New Address</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js">
</script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" rel="stylesheet" />
<script>
// agar user profile bisa diclik
$(".show-userinfo-menu").click(function() {
    $('.widget-content-wrapper').toggleClass('show')
    $('.dropdown-menu-right').toggleClass('show')
})

    //Datetimepicker plugin
Helper.date('#schedule',1);

$('.ganti-alamat').click(function(e){
    $('#modal-alamat').modal('show')
    e.preventDefault();
});
$('.radio-alamat').change(function(){
    id = $(this).attr('data-id');
    type_address = $(this).attr('data-type_address');
    detail_address = $(this).attr('data-detail_address');

    $('[name="address_id"]').val(id);
    $('.type-address-text').text(type_address);
    $('.detail-address-text').text(detail_address);

    $('#modal-alamat').modal('hide')
})

$('.keluhan-radio').change(function(e){
    var symptom_id = $(this).val();

    $('.layanan-input-area').html('');

    // get layanan
    Axios.get('/symptom/' + symptom_id)
        .then(function(response) {
            symptom = response.data.data;

            template = '';
            _.each(symptom.service_types, function(row){
                template += templateLayanan(row);
            })

            $('.layanan-input-area').html(template);
            $('.unit-input').hide();
            Helper.noNegative();
        })
        .catch(function(error) {
            Helper.handleErrorResponse(error)
        });
})

$(document).on('change', '.layanan-checkbox', function(e){
    id = $(this).attr('data-id');

    if (this.checked) {
        $('.unit-input-no-' + id).show();
    }else{
        $('.unit-input-no-' + id).hide();
    }
})

$('#submit-form').click(function(e){
    var data_form_job = Helper.serializeForm($('#form-job'));
    data.service_type_id = [];
    $('input[name="service_type_id[]"]').each(function() {
        if($(this).is(':checked')){
            data.service_type_id.push($(this).val());
        }else{
            data.service_type_id.push(null);
        }
    });

    // if ($('input[name="address_id"]:checked').length == 0) {
    //     Helper.errorNotif('Belum pilih alamat');
    //     return false;
    // }

    if (data.service_type_id.length == 0) {
        Helper.errorNotif('Belum isi Layanan');
        return false;
    }else{
        Helper.loadingStart();
        data = btoa(JSON.stringify(data));
        window.location.href = Helper.redirectUrl('/customer/request-job/list-technician?data='+data+'');
    }
});

function templateLayanan(data){
    t = '';
    t +=' <div class="col-md-12">';
        t +=' <div class="position-relative form-group">';
            t +=' <div>';
                t +=' <div class="custom-checkbox custom-control">';
                    t +=' <input type="checkbox" name="service_type_id[]" value="'+data.id+'" id="layanan-'+data.id+'" class="custom-control-input layanan-checkbox" data-id="'+data.id+'">';
                    t +=' <label class="custom-control-label" for="layanan-'+data.id+'">'+data.name+'</label>';
                t +=' </div>';
            t +=' </div>';
        t +=' </div>';
    t +=' </div>';
    t +=' <div class="col-md-4 unit-input unit-input-no-'+data.id+'">';
        t +=' <div class="position-relative form-group">';
            t +=' <input type="number" name="unit[]" class="form-control form-control-sm" placeholder="Unit">';
        t +=' </div>';
    t +=' </div>';

    return t;
}
</script>


<style>
    .list-group-item {
    user-select: none;
    }

    .list-group input[type="checkbox"] {
    display: none;
    }

    .list-group input[type="checkbox"] + .list-group-item {
    cursor: pointer;
    }

    .list-group input[type="checkbox"] + .list-group-item:before {
    content: "\2713";
    color: transparent;
    font-weight: bold;
    margin-right: 1em;
    }

    .list-group input[type="checkbox"]:checked + .list-group-item {
    background-color: #0275D8;
    color: #FFF;
    }

    .list-group input[type="checkbox"]:checked + .list-group-item:before {
    color: inherit;
    }

    .list-group input[type="radio"] {
    display: none;
    }

    .list-group input[type="radio"] + .list-group-item {
    cursor: pointer;
    }

    .list-group input[type="radio"] + .list-group-item:before {
    color: transparent;
    font-weight: bold;
    margin-right: 1em;
    }

    .list-group input[type="radio"]:checked + .list-group-item {
    background-color: #0275D8;
    color: #FFF;
    }

    .list-group input[type="radio"]:checked + .list-group-item:before {
    color: inherit;
    }
</style>


@endsection
