@extends('admin.home')
@section('content')
@include('admin.customer.complaint.styleChat')
<style>
    .scroll-area-sm {
        height: 500px;
        margin-top: 10px;
        margin-bottom: 10px;
        padding: 10px;
    }

    .incoming_msg_img {
        display: inline-block;
        width: 6%;
    }

    .received_msg {
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 50%;
    }

    .received_withd_msg p {
        background: #ebebeb none repeat scroll 0 0;
        border-radius: 3px;
        color: #646464;
        font-size: 14px;
        margin: 0;
        padding: 5px 10px 5px 12px;
        width: 175%;
    }

    .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 8px 0 0;
    }

    .received_withd_msg {
        width: 57%;
    }

    .mesgs {
        float: left;
        padding: 30px 15px 0 25px;
        width: 100%;
    }

    .sent_msg p {
        background: #05728f none repeat scroll 0 0;
        border-radius: 3px;
        font-size: 14px;
        margin: 0;
        color: #fff;
        padding: 5px 10px 5px 12px;
        width: 100%;
    }

    .outgoing_msg {
        overflow: hidden;
        margin: 26px 0 26px;
    }

    .sent_msg {
        float: right;
        width: 46%;
    }

    .input_msg_write input {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        color: #4c4c4c;
        font-size: 15px;
        min-height: 48px;
        width: 100%;
    }

    .messaging {
        padding: 0 0 50px 0;
    }
</style>
<div class="col-md-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-body">
            <div class="tiket-status-option">
                <h4>Detail Complaint Form</h4>
                <hr />
                <div class="widget-subheading">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">Order Code : <strong>#{{ $query->code }}</strong></div>
                                <div class="col-md-4">Complaint Date : {{  date('d-M-y H:i:s', strtotime($query->updated_at)) }}</div>
                                <div class="col-md-4">Email : {{ $query->user->email }}</div>
                            </div><br><br>
                        </div>
                        <div class="col-md-12">
                            <div class="messaging">
                                <div class="inbox_msg">
                                    <div class="mesgs">
                                        <div class="msg_history scroll-area-sm">
                                            <?php $before = null; ?>
                                           
{{-- <span>{{ $query }}</span> --}}
                                           {{-- @if(Request::segment(1) == 'teknisi')
                                                <div class="incoming_msg">
                                                    <div class="incoming_msg_img"> 
                                                        <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> 
                                                        <div class="avatar-upload">
                                                            <div class="avatar-preview">
                                                                @if (!empty($query->user->info->picture))
                                                                    <div id="imagePreview" style="background-image: url( {{ asset('/storage/user/profile/avatar/' . $query->user->info->picture) }} )"></div>
                                                                @else
                                                                    <div id="imagePreview" style="background-image: url( {{ asset('storage/user/profile/avatar/default_avatar.jpg') }} )"></div>
                                                                @endif
                                                                <div id="imagePreview" style="background-image: url( {{ asset('/storage/user/profile/avatar/' . $query->user->info->picture) }} )"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="received_msg">
                                                        <div class="received_withd_msg">
                                                            <p>{{ $query->note_complaint }}</p>
                                                            <span class="time_date"> {{  date('d-M-y H:i:s', strtotime($query->created_at)) }}</span>
                                                        </div>
                                                    </div>
                                                </div><br>
                                            @endif

                                            @if(Request::segment(1) == 'customer')
                                                <div class="outgoing_msg">
                                                    <div class="sent_msg">
                                                    <p>{{ $query->note_complaint }}</p>
                                                    <span class="time_date"> {{  date('d-M-y H:i:s', strtotime($query->created_at)) }}</span> </div>
                                                </div><br>
                                            @endif --}}

                                            @if(Request::segment(1) == 'admin')
                                                <div class="incoming_msg">
                                                    <div class="incoming_msg_img"> 
                                                        {{-- <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">  --}}
                                                        <div class="avatar-upload">
                                                            <div class="avatar-preview">
                                                                @if (!empty($query->user->info->picture))
                                                                    <div id="imagePreview" style="background-image: url( {{ asset('/storage/user/profile/avatar/' . $query->user->info->picture) }} )"></div>
                                                                @else
                                                                    <div id="imagePreview" style="background-image: url( {{ asset('storage/user/profile/avatar/default_avatar.jpg') }} )"></div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="received_msg">
                                                        <div class="received_withd_msg">
                                                            <p>{{ $query->note_complaint }}</p>
                                                            <span class="time_date"> {{  date('d-M-y H:i:s', strtotime($query->created_at)) }}</span>
                                                        </div>
                                                    </div>
                                                </div><br>
                                            @endif
                                           
                                           

                                            @foreach ($showComplaint as $detail)
                                                @if(Auth::user()->id == $detail->user_id)
                                                    <?php $before = $detail->user_id; ?>
                                                    <div class="outgoing_msg">
                                                        <div class="sent_msg">
                                                        <p >{{ $detail->message }}</p>
                                                        <!--<p >KIRI</p>-->
                                                        @if ($detail->attachment != null)
                                                            @if(Request::segment(1) == 'customer')
                                                                <img alt="" class="img-responsive" src="{{ asset('customer/storage/attachment/complaint/' . $detail->attachment) }}" />
                                                            @endif
                                                            @if(Request::segment(1) == 'admin')
                                                                <img alt="" class="img-responsive" src="{{ asset('admin/storage/attachment/complaint/' . $detail->attachment) }}" />
                                                            @endif
                                                            @if(Request::segment(1) == 'teknisi')
                                                                <img alt="" class="img-responsive" src="{{ asset('teknisi/storage/attachment/complaint/' . $detail->attachment) }}" />
                                                            @endif
                                                        @endif
                                                        <span class="time_date"> {{  date('d-M-y H:i:s', strtotime($detail->created_at)) }}</span> </div>
                                                    </div>
                                                @endif
                                                
                                                @if (Auth::user()->id != $detail->user_id)
                                                {{-- <span>{{ $detail }}</span> --}}
                                                    <div class="incoming_msg">
                                                        <div class="incoming_msg_img"> 
                                                            @if ($before != $detail->user_id)
                                                                <?php $before = $detail->user_id; ?>
                                                                {{-- <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">  --}}
                                                                {{-- <img alt="" src="{{ asset('/storage/user/profile/avatar/' . $detail->user->info->picture) }}" /> --}}
                                                                <div class="profile-userpic text-center">
                                                                    <div class="avatar-upload">
                                                                        <div class="avatar-preview">
                                                                            @if (!empty($detail->user->info->picture))
                                                                            <div id="imagePreview" style="background-image: url( {{ asset('/storage/user/profile/avatar/' . $detail->user->info->picture) }} )"></div>
                                                                            @else
                                                                                <div id="imagePreview" style="background-image: url( {{ asset('storage/user/profile/avatar/default_avatar.jpg') }} )"></div>
                                                                            @endif
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="received_msg">
                                                            <strong>{{ $detail->user->name }}</strong>
                                                            {{-- <strong style="float: right;">{{ $detail->created_by }}</strong> --}}
                                                            <div class="received_withd_msg">
                                                                <p>{{ $detail->message }}</p>
                                                                <!--<p>KANAN</p>-->
                                                                @if ($detail->attachment != null)
                                                                    <img alt="" class="img-responsive" src="{{ asset('/storage/attachment/complaint/' . $detail->attachment) }}" />
                                                                @endif
                                                                <span class="time_date"> {{  date('d-M-y H:i:s', strtotime($detail->created_at)) }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-body">
            @if($query->orders_statuses_id == 11)
                <form id="reply-complaint">
                    <input type="hidden" name="ms_orders_id" id="ms_orders_id" value="{{ $query->id }}">
                    <div class="position-relative form-group">
                        <textarea id="message" class="form-control" cols="5" name="message" placeholder="Message" rows="5" type="text"></textarea>
                    </div>
                    <div class="position-relative form-group">
                        <label for="">attachment</label>
                        <br>
                        <input type="file" name="attachment" id="attachment" />
                    </div>
                    <button class="btn btn-success" type="submit">
                        REPLY
                    </button>
                    @if(Request::segment(1) == 'customer')
                        <button class="btn btn-primary approve-jobs" data-id="{{ $query->id }}" type="submit">
                            SOLVED
                        </button>
                    @endif
                    @if(Request::segment(1) == 'admin')
                        <button class="btn btn-primary admin_solved_jobs" data-id="{{ $query->id }}" type="submit">
                            SOLVED
                        </button>
                    @endif
                </form>
            @endif
        </div>
        <input type="hidden" name="commission_value" value="15000">
        <input type="hidden" name="early_total" value="{{ $query->grand_total }}">
        <input type="hidden" name="total" value="{{ $query->after_commission }}">
        <div class="card-body">
            <div class="widget-content p-0">
                <div class="widget-content-wrapper">
                    <div class="widget-content-left">
                        @if($query->orders_statuses_id == 10)
                            @if(Request::segment(1) == 'teknisi')
                                <a href="{{ url('/teknisi/complaint-jobs') }}" class="btn btn-primary ">Back</a>
                            @endif

                            @if(Request::segment(1) == 'customer')
                                <a href="{{ url('/customer/transacsion_list/complaint') }}" class="btn btn-primary ">Back</a>
                            @endif

                            @if(Request::segment(1) == 'admin')
                                <a href="{{ url('/admin/transacsion_list/complaint') }}" class="btn btn-primary ">Back</a>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .profile-userpic img {
        float: none;
        margin: 0 auto;
        width: 50%;
        -webkit-border-radius: 50% !important;
        -moz-border-radius: 50% !important;
        border-radius: 50% !important;
        }

        .avatar-upload {
  position: relative;
  max-width: 205px;
  margin: 0px auto;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 1;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
  cursor: pointer;
  font-weight: normal;
  transition: all 0.2s ease-in-out;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
.avatar-upload .avatar-edit input + label:after {
  content: "\f040";
  font-family: 'FontAwesome';
  color: #757575;
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  text-align: center;
  margin: auto;
}
.avatar-upload .avatar-preview {
  width: 70px;
  height: 70px;
  position: relative;
  border-radius: 100%;
  border: 6px solid #F8F8F8;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
}
.avatar-upload .avatar-preview > div {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}
</style>

@endsection

@section('script')

    {{-- reply comment --}}
    <script>
        $(document).on('click', '.approve-jobs', function(e) {
            id = $(this).attr('data-id');
            Helper.confirm(function(){
                Helper.loadingStart();
                $.ajax({
                    url:Helper.apiUrl('/customer/complaint/close/' + id ),
                    type: 'post',
                    data : {
                        commission_value : $('input[name="commission_value"]').val(),
                        early_total : $('input[name="early_total"]').val(),
                        total : $('input[name="total"]').val(),
                    },

                    success: function(res) {
                        Helper.successNotif('Success');
                        Helper.redirectTo('/customer/request_list');
                    },
                    error: function(res) {
                        alert("Something went wrong");
                        console.log(res);
                        Helper.loadingStop();
                    }
                })
            })
            
            e.preventDefault()
        })

        $(document).on('click', '.admin_solved_jobs', function(e) {
            id = $(this).attr('data-id');
            Helper.confirm(function(){
                Helper.loadingStart();
                $.ajax({
                    url:Helper.apiUrl('/admin/complaint/close/' + id ),
                    type: 'post',
                    data : {
                        commission_value : $('input[name="commission_value"]').val(),
                        early_total : $('input[name="early_total"]').val(),
                        total : $('input[name="total"]').val(),
                    },

                    success: function(res) {
                        Helper.successNotif('Success');
                        Helper.redirectTo('/admin/transaction-complaint/show');
                    },
                    error: function(res) {
                        alert("Something went wrong");
                        console.log(res);
                        Helper.loadingStop();
                    }
                })
            })
            
            e.preventDefault()
        })

        $(".msg_history").animate({ scrollTop: $(".msg_history").height() }, 1000);

        $('#reply-complaint').submit(function(e) {
            data = Helper.serializeForm($(this));

            var formData = new FormData();
            var attachment = document.querySelector('#attachment');
            if (attachment.files[0]) {
                formData.append("attachment", attachment.files[0]);
            }
            formData.append("message", $('#message').val());
            formData.append("ms_orders_id", data.ms_orders_id);

            Helper.loadingStart();
            // post data
            Axios.post(Helper.apiUrl('/customer/request-ticket/reply/83'), formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function(response) {
                    console.log(response.data)
                    // Helper.successNotif('Success, ok');
                    $('#message').val('');
                    $('.msg_history').append(response.data.data);
                    window.scrollTo(x-coord, y-coord);
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });

            e.preventDefault();
        })
    </script>

@endsection