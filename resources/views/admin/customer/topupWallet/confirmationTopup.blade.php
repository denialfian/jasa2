@extends('admin.home')
@section('content')
<style>
    div.hr-or {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    div.hr-or:before {
        content: 'OR';
        background-color: #fff;
    }

    div.hr {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    .hr-title {
        background-color: #fff;
    }
</style>

<div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-header">
            Order Received
            <div class="btn-actions-pane-right">
                <a href="{{ url('/customer/topup/show-list') }}" class="btn btn-primary btn-sm add-bank-transfer">Back</a>
            </div>
        </div>
        <div class="card-body">
            <div class="row">

                <div class="col-md-12">
                    <p>thank you, your Top up Has been Received</p>
                    <table class="table table-striped">
                        <thead>
                            <th>Bank</th>
                            <th>Rekening</th>
                            <th>A.N</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td><img src="{{ asset('/customer/storage/bankIcon/' . $wallet->bank->icon) }}" alt="asd" width="50"></td>
                                <td>{{ $wallet->bank->virtual_code }}</td>
                                <td>{{ $wallet->bank->name }}</td>
                            </tr>
                        </tbody>
                    </table>
                        <div class="container">
                            <input type="hidden" id="id" class="form-control" name="id" value="{{ $wallet->id }}">
    
                            <div class="form-group">
                                <label for="" class="">Email</label>
                                <input type="text" id="" class="form-control" value="{{ $wallet->user->email }}" readonly>
                            </div>
    
                            <div class="form-group">
                                <label for="" class="">A.N Rekening</label>
                                <input type="text" id="" class="form-control" name="a_n_rekening" >
                                <strong><span id="error-a_n_bank" style="color:red"></span></strong>
                            </div>
                            
    
                            <div class="form-group">
                                <label for="" class="">Tanggal Transfer</label>
                                <input name="transfer_date" id="datetimepicker" placeholder="" type="text" class="form-control">
                                <strong><span id="error-transfer_date" style="color:red"></span></strong>
                            </div>
    
                            <div class="form-group">
                                <label for="" class="">Nominal</label>
                                <input type="text" id="" class="form-control" name="nominal" value="Rp.{{ number_format($wallet->nominal) }}" >
                                <strong><span id="error-nominal" style="color:red"></span></strong>
                            </div>
                            
    
                            <div class="form-group">
                                <label for="" class="">Bukti Transfer</label>
                                <input type="file" id="profile-image-upload" class="@error('attachment') is-invalid @enderror form-control" name="attachment" >
                                <strong><span id="error-attachment" style="color:red"></span></strong>
                                
                                
                            </div>
    
                            <div class="form-group">
                                <label for="" class="">Catatan</label>
                                <textarea class="form-control"  rows="3" name="note" ></textarea><br>
                                <strong><span id="error-note" style="color:red"></span></strong>
                            </div>

                            <button type="submit" class="btn btn-primary btn-lg btn-block" id="save">Confirmation</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<link rel="stylesheet" type="text/css" href="{{ asset('/datePicker/jquery.datetimepicker.css') }}" >
<script src="{{ asset('/datePicker/jquery.datetimepicker.full.min.js') }}"></script>
<script>
    $(document).ready(function(){
        jQuery('#datetimepicker').datetimepicker({
            format:'Y-m-d',
            mask:true
        });
    });
</script>
<script>
    $("#save").click(function() {
        var fd = new FormData();
        var id = $('input[name="id"]').val();
        var files = $('#profile-image-upload')[0].files[0];
        if(files){
            fd.append('attachment', files);
        }
        // fd.append('id', $('input[name="id"]').val());
        fd.append('a_n_rekening', $('input[name="a_n_rekening"]').val());
        fd.append('transfer_date', $('input[name="transfer_date"]').val());
        $.ajax({
            url:Helper.apiUrl('/customer/topup-wallet/confirmationTopup/'+ id ),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Data Has Been Saved',
                    });
                    window.location = Helper.url('/customer/topup/show-list');
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])

                    })
                }
            },
        });
    });
</script>

@endsection
