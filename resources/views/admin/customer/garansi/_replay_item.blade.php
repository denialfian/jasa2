@if($auth->id != $detail->user->id)
<div class="message">
    <img class="avatar-md" src="{{ $detail->user->avatar }}" data-toggle="tooltip" data-placement="top" title="{{ $detail->user->name }}" alt="avatar">
    <div class="text-main">
        <div class="text-group">
            <div class="text">
                <p>{{ $detail->message }}</p>
            </div>
        </div>
        <span>{{ $detail->created_at->format('H:i') }}</span>
    </div>
</div>
@endif
@if($auth->id == $detail->user->id)
<div class="message me">
    <div class="text-main">
        <div class="text-group me">
            <div class="text me">
                @if($detail->attachment != null)
                <img src="{{ asset('/storage/garansi/attachment/' . $detail->attachment) }}" alt="" style="width: 100%; height: auto;">
                @endif
                <p>{{ $detail->message }}</p>
            </div>
        </div>
        <span>{{ $detail->created_at->format('H:i') }}</span>
    </div>
</div>
@endif