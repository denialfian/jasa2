@foreach($details as $detail)

@if($auth->id != $detail->user->id)
<div class="message">
    <img class="avatar-md" src="{{ $detail->user->avatar }}" data-toggle="tooltip" data-placement="top" title="{{ $detail->user->name }}" alt="avatar">
    <div class="text-main">
        <div class="text-group">
            <div class="text">
                @if($detail->attachment != null)
                <img src="{{ asset('/storage/garansi/attachment/' . $detail->attachment) }}" alt="" style="width: 100%; height: auto;">
                @endif
                @if($detail->order_json != null)

                    @if($detail->message == 'Revisit Order')
                        <label>{{ $detail->message }}</label>
                        <hr />
                        <label>Schedule:</label>
                        <p>{{ $detail->order->schedule }}</p>
                        <hr />
                        <label>Technician:</label>
                        <p>{{ $detail->order->detail->teknisi->user->name }}</p>
                        <p>{{ $detail->order->detail->teknisi->user->email }}</p>
                        <p>{{ $detail->order->detail->teknisi->user->phone }}</p>
                        <hr />
                    @else
                        <label>Subject:</label>
                        <p>{{ $detail->garansi->subject }}</p>
                        <p>{{ $detail->message }}</p>
                        <hr />

                        <label for="ms_symptom_code">Symptom Code</label>
                        <p>{{ $detail->order->symptom_code == null ? '' : $detail->order->symptom_code->name }}</p>
                        <label for="ms_symptom_code">Repair Code</label>
                        <p>{{ $detail->order->repair_code == null ? '' : $detail->order->repair_code->name }}</p>
                        <label for="repair_desc">Repair Description</label>
                        <p>{{ $detail->order->repair_desc }}</p>
                        <hr />

                        @if($detail->garansi->status != 'done' && $auth->hasRole('admin'))
                            @if($detail->garansi->customer_can_reply == 0)
                            <button data-id="{{ $detail->garansi->id }}" class="btn-hover-shine customer-reply btn btn-primary">
                                <i class="fa fa-square-o"></i>
                                Accept Claim
                            </button>
                            @else
                                <b><i class="fa fa-check-square-o"></i> Claim has been accepted</b>
                            @endif
                        @endif
                    @endif

                @else
                <p>{{ $detail->message }}</p>
                @endif
            </div>
        </div>
        <span>{{ $detail->created_at->format('H:i') }}</span>
    </div>
</div>
@endif
@if($auth->id == $detail->user->id)
<div class="message me">
    <div class="text-main">
        <div class="text-group me">
            <div class="text me">
                @if($detail->attachment != null)
                <img src="{{ asset('/storage/garansi/attachment/' . $detail->attachment) }}" alt="" style="width: 100%; height: auto;">
                @endif

                @if($detail->order_json != null)

                    @if($detail->message == 'Revisit Order')
                        <label>{{ $detail->message }}</label>
                        <hr />
                        <label>Schedule:</label>
                        <p>{{ $detail->order->schedule }}</p>
                        <hr />
                        <label>Technician:</label>
                        <p>{{ $detail->order->detail->teknisi->user->name }}</p>
                        <p>{{ $detail->order->detail->teknisi->user->email }}</p>
                        <p>{{ $detail->order->detail->teknisi->user->phone }}</p>
                        <hr />
                    @else
                        <label>Subject:</label>
                        <p>{{ $detail->garansi->subject }}</p>
                        <p>{{ $detail->message }}</p>
                        <hr />

                        <label for="ms_symptom_code">Symptom Code</label>
                        <p>{{ $detail->order->symptom_code == null ? '' : $detail->order->symptom_code->name }}</p>
                        <label for="ms_symptom_code">Repair Code</label>
                        <p>{{ $detail->order->repair_code == null ? '' : $detail->order->repair_code->name }}</p>
                        <label for="repair_desc">Repair Description</label>
                        <p>{{ $detail->order->repair_desc }}</p>
                        <hr />

                        @if($detail->garansi->status != 'done' && $auth->hasRole('admin'))
                            @if($detail->garansi->customer_can_reply == 0)
                            <button data-id="{{ $detail->garansi->id }}" class="btn-hover-shine customer-reply btn btn-primary">
                                <i class="fa fa-square-o"></i>
                                Accept Claim
                            </button>
                            @else
                                <b><i class="fa fa-check-square-o"></i> Claim has been accepted</b>
                            @endif
                        @endif
                    @endif

                @else
                <p>{{ $detail->message }}</p>
                @endif
            </div>
        </div>
        <span>{{ $detail->created_at->format('H:i') }}</span>
    </div>
</div>
@endif

@endforeach