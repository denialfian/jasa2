@extends('admin.home')
@section('content')
<style>
    div.hr-or {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    div.hr-or:before {
        content: 'OR';
        background-color: #fff;
    }

    div.hr {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    .hr-title {
        background-color: #fff;
    }
</style>

<div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-header">
            {{ $title }}
            <div class="btn-actions-pane-right">

            </div>
        </div>
        <div class="card-body">
            <br><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="">First Name</label>
                                            <input type="text" id="firstName" class="form-control" name="" value="{{ !empty($getDataUser->info) ? $getDataUser->info->first_name : Auth::user()->name  }}" readonly >
                                        </div>
                                        <div class="col-md-6">
                                            <label class="">Last Name</label>
                                            <input type="text" id="lastname" class="form-control" name="" value="{{ !empty($getDataUser->info) ?  $getDataUser->info->last_name : '-'}}" readonly >
                                        </div>
                                    </div><br>

                                    <label class="">Email</label>
                                    <input type="email" id="email" class="form-control" name="email" value="{{ $getDataUser->email }}" readonly >
                                    <br>

                                    {{-- <label for="address" class="">Address</label>
                                    <textarea class="form-control" id="address" rows="3" name="address" readonly>asdad{{ $getDataUser->info->address }}</textarea> --}}
                                    <br>

                                    {{-- <div class="row">

                                        <div class="col-md-6">
                                            <label class="">City</label>
                                            <input type="text" id="city" class="form-control" name="city" readonly value="{{ $getDataUser->address->city->name != null ? $getDataUser->address->city->name : 'Not Found' }}">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="">Village</label>
                                            @if (@empty($getDataUser->address->village->name))
                                                <input type="text" id="village" class="form-control" name="village" readonly value="Not Found">
                                            @else
                                                <input type="text" id="village" class="form-control" name="village" readonly value="{{ $getDataUser->address->village->name }}">
                                            @endif
                                        </div>
                                    </div><br> --}}

                                    {{-- <div class="row">
                                        <div class="col-md-6">
                                            <label class="">Distric</label>
                                            <input type="text" id="distric" class="form-control" name="distric" readonly value="{{ $getDataUser->address->district->name != null ? $getDataUser->address->district->name : 'Not Found' }}">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="">Zipp Code</label>
                                            @if (@empty($getDataUser->address->zipcode->zip_no))
                                                <input type="text" id="zip_no" class="form-control" name="zip_no" readonly value="Not Found">
                                            @else
                                                <input type="text" id="zip_no" class="form-control" name="zip_no" readonly value="{{ $getDataUser->address->zipcode->zip_no }}">
                                            @endif

                                        </div>
                                    </div><br> --}}

                                    <div class='hr'>
                                        <span class='hr-title'><strong>Payment Type</strong></span>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <label  class="">Choose Payment Method</label>
                                            <select name="payment_type_id" class="form-control js-example-basic-single" id="payment_type">
                                                @if ($wallet->payment_type_id == null)
                                                    @foreach ($getPaymentType as $payment_type)
                                                        <option value="{{ $payment_type->id }}">{{ $payment_type->payment_name }}</option>
                                                    @endforeach
                                                @endif
                                                @if ($wallet->payment_type_id == 1)
                                                    <option value="1">Bank Transfer</option>
                                                @elseif($wallet->payment_type_id == 2)
                                                    <option value="2">Payment Gatway (MIDTRANS)</option>
                                                @endif

                                            </select>
                                            
                                        </div><br><br><br>
                                        @if ($wallet->payment_type_id == 1)
                                        <br>
                                            <div class="col-md-12" id="bank_transfer">
                                                <label class="">Choose Bank</label>
                                                <select name="payment_metod_id" class="form-control js-example-basic-single" id="exampleFormControlSelect1">
                                                    @foreach ($getBankName as $nameBank)
                                                        <option value="{{ $nameBank->id }}" {{ $wallet->payment_metod_id == $nameBank->id ? 'selected' : '' }}>{{ $nameBank->bank_name }} - {{ $nameBank->name }} - ({{ $nameBank->virtual_code }})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        @endif
                                        <div class="col-md-12" id="bank_transfer" hidden><br>
                                            <label class="">Choose Bank</label>
                                            <select name="payment_metod_id" class="form-control js-example-basic-single" id="exampleFormControlSelect1">
                                                @foreach ($getBankName as $nameBank)
                                                    <option value="{{ $nameBank->id }}">{{ $nameBank->bank_name }} - {{ $nameBank->name }} - ({{ $nameBank->virtual_code }})</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5" id="charts">
                            <h4 class="d-flex justify-content-between align-items-center mb-3">
                                <span class="text-muted">Your cart</span>
                                <span class="badge badge-secondary badge-pill"></span>
                            </h4>
                            <ul class="list-group mb-3 z-depth-1">
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <h6 class="my-0">Top up E-wallet</h6>
                                        <!-- <small class="text-muted">qty : x2</small> -->
                                    </div>
                                    <span class="text-muted">Rp. {{ number_format($wallet->nominal) }}</span>
                                </li>
                                
                                @if ($wallet->payment_type_id == 1)
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Additional Cost</h6>
                                            <!-- <small class="text-muted">qty : x2</small> -->
                                        </div>
                                        <span class="text-muted">Rp. {{ number_format($wallet->kode_uniq_bank) }}</span>
                                    </li>
                                @endif
                                
                                <div id="kode_uniq" style="display:none">
                                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                                        <div>
                                            <h6 class="my-0">Additional Cost</h6>
                                            <!-- <small class="text-muted">qty : x2</small> -->
                                        </div>
                                        <span class="text-muted">Rp. {{ number_format($wallet->kode_uniq_bank) }}</span>
                                    </li>
                                </div>

                                <div id="total_awal">
                                    <li class="list-group-item d-flex justify-content-between bg-light">
                                        <strong>Sub Total</strong>
                                        @if($wallet->payment_type_id == 1)
                                            @php
                                                $total = $wallet->nominal + $wallet->kode_uniq_bank
                                            @endphp
                                            <strong>Rp.{{ number_format($total) }}</strong>
                                        @else
                                            <strong>Rp.{{ number_format($wallet->nominal) }}</strong>
                                        @endif
                                    </li>
                                </div>
                                
                                
                                <div id="total_kode_uniq" style="display:none">
                                    <li class="list-group-item d-flex justify-content-between bg-light">
                                        <strong>Sub Total</strong>
                                        @php
                                            $total = $wallet->nominal + $wallet->kode_uniq_bank;
                                        @endphp
                                        <strong>Rp.{{ number_format($total) }}</strong>
                                    </li>
                                </div>
                                <div id="append_template_diskon"></div>
                            </ul>
                            <input type="hidden" name="id" id="id" value="{{ $wallet->id }}">

                            <div class="card p-2 bg-light">
                                <div class="input-group">
                                    @if (!empty($wallet->voucer))
                                        @if (!empty($wallet->voucer->id))
                                            @if (!empty($wallet->payment_token))
                                                <input type="text" class="form-control reedem_voucers" value="{{ $wallet->voucer->code }}" name="reedem_voucer" id="" placeholder="Reedem Your Voucher" disabled>
                                            @else
                                                <input type="text" class="form-control reedem_voucers" value="" name="reedem_voucer" id="" placeholder="Reedem Your Voucher">
                                            @endif
                                        @else
                                            @if (empty($wallet->payment_token))
                                                <input type="text" class="form-control reedem_voucers" value="" name="reedem_voucer" id="" placeholder="Reedem Your Voucher">
                                            @else
                                                <input type="text" class="form-control reedem_voucers" value="" name="reedem_voucer" id="" placeholder="Reedem Your Voucher" readonly>
                                            @endif
                                        @endif
                                    @else
                                        @if (empty($wallet->payment_token))
                                            <input type="text" class="form-control reedem_voucers" value="" name="reedem_voucer" id="" placeholder="Reedem Your Voucher">
                                        @else
                                            <input type="text" class="form-control reedem_voucers" value="" name="reedem_voucer" id="" placeholder="Reedem Your Voucher" readonly>
                                        @endif
                                    @endif
                                    <div class="input-group-append">
                                        @if (empty($wallet->payment_token))
                                            <button type="submit" id="btn_clime_voucher" class="btn btn-secondary">Reedem</button>
                                        @else
                                            <button type="submit" id="btn_clime_voucher" class="btn btn-secondary" disabled>Reedem</button>
                                        @endif
                                    </div>
                                </div>
                            </div> <br>
                            <div id="append_message">
                                <div class="alert alert-danger" id="alert-error" role="alert"></div>
                            </div>
                            <div id="success_response">
                                <div class="alert alert-success" role="alert">
                                    Claim Voucer Success
                                </div>
                            </div>

                        </div>

                        {{-- <div class="col-md-7" id="btn_midtrans" hidden>
                            @if(!$wallet->isPaid())
                                <button type="submit"  id="pay-button" class="btn btn-primary btn-lg btn-block btnMidtrans">Proccessed To Payment</button>
                            @endif
                        </div> --}}
                        <div class="col-md-7" id="btn_bank_transfer" hidden>
                            <button type="submit"  id="save" class="btn btn-primary btn-lg btn-block">Proccessed To Payment (bank)</button>
                        </div>
                        @if ($wallet->payment_type_id == null)
                            <div class="col-md-7" id="btn_midtrans">
                                {{-- @if(!$wallet->isPaid()) --}}
                                    <button type="submit"  id="pay-button" class="btn btn-primary btn-lg btn-block btnMidtrans">Proccessed To Payment (midtrans)</button>
                                {{-- @endif --}}
                            </div>
                        @endif
                        @if ($wallet->payment_type_id == 1)
                            <div class="col-md-7">
                                <button type="submit"  id="saved" class="btn btn-primary btn-lg btn-block">Proccessed To Payment (bank)</button>
                            </div>
                        @elseif($wallet->payment_type_id == 2)
                            <div class="col-md-7">
                                <a href="#" type="submit" data-link="{{ $wallet->payment_token }}"  id="continueToPayment" class="btn btn-primary btn-lg btn-block">Continue Payment </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <button id="pay-button">Pay!</button> --}}
    

@endsection

@section('script')
<script 
    type="text/javascript"
    src="https://app.sandbox.midtrans.com/snap/snap.js"
    data-client-key="SB-Mid-client-oBzvWZCgQ3_WlRA_"></script>

<script type="text/javascript">
    var payButton = document.getElementById('continueToPayment');
    payButton.addEventListener('click', function () {
        getLink = $(this).attr('data-link')
        snap.pay(getLink);
    });
</script>

<script>
    $('#append_message').hide();
    $('#success_response').hide();
    $(document)
        .on('click', '#btn_clime_voucher', function() {
            id = $('#id').val();
            // alert(id)
            $.ajax({
                url: Helper.apiUrl('/customer/reedem/voucer/' + id),
                type: 'post',
                data:{
                    reedem_voucer : $('.reedem_voucers').val()
                },
                success: function(resp) {
                    $('#append_template_diskon').html('');
                    var getDataVOucer = resp.data[0];
                    var getDataTopup = resp.data[1];
                    var totalVoucer = 0;
                    var totalPotongan = 0;
                    if(getDataVOucer.type == 1){
                        totalPotongan = getDataVOucer.max_nominal;
                        totalVoucer = parseInt(getDataTopup.nominal) - parseInt(getDataVOucer.max_nominal)
                    }else{
                        totalPotongan = getDataVOucer.value;
                        totalVoucer = parseInt(getDataTopup.nominal) - parseInt(getDataVOucer.value)
                    }
                    if(getDataVOucer.voucher_type == 1){
                        title = "Cashback"
                        marker = "+"
                        totalPayment = parseInt(getDataTopup.nominal)
                    }else{
                        title = "Discount"
                        marker = "-"
                        totalPayment = totalVoucer
                    }
                    var template_diskon = '';
                    var alert_success = '';
                    template_diskon +=  `<li class="list-group-item d-flex justify-content-between bg-light">
                                            <strong>${title}</strong>
                                                <strong>${marker} Rp. ${Helper.thousandsSeparators(totalPotongan)}</strong>
                                        </li>
                                        <li class="list-group-item d-flex justify-content-between bg-light">
                                            <strong>Total Pay</strong>
                                                <strong>Rp. ${Helper.thousandsSeparators(totalPayment)}</strong>
                                        </li>`;
                    // alert_success +=    `<div class="alert alert-success" role="alert">
                    //                         Clime Voucer Success
                    //                     </div>`;
                    $('#append_template_diskon').append(template_diskon);
                    $('#success_response').show();
                    $('#append_message').hide();
                },
                error: function(xhr, status, error) {
                    Helper.errorsAlert(xhr, status, error, function(msg){
                        $('#append_message').show();
                        $('#success_response').hide();
                        $('#append_template_diskon').html('');
                        $('#alert-error').text(msg)
                    })
                },
            })
        })
</script>

<script>
     $("#pay-button").click(function() {
        var fd = new FormData();
        var id = $('input[name="id"]').val();
        // Helper.loadingStart();
        // alert(id);
        $.ajax({
            url:Helper.apiUrl('/customer/topup-wallet/checkout/midtrans/' + id),
            type: 'POST',
            data:{
                reedem_voucer : $('.reedem_voucers').val()
            },
            success: function(response) {
                console.log(response)
                snap.pay(response.data.payment_token, {
                    onSuccess: function(result){
                        /* You may add your own implementation here */
                        console.log(result);
                    },
                    onPending: function(result){
                        /* You may add your own implementation here */
                        console.log(result);
                    },
                    onError: function(result){
                        /* You may add your own implementation here */
                        console.log(result);
                    },
                    onClose: function(){
                        /* You may add your own implementation here */
                    }
                });
                
            },
            error: function(xhr, status, error) {
                alert('something wrong');
                // Helper.loadingStop();
            },
        });
    });

    $("#save").click(function() {
        var fd = new FormData();
        var id = $('input[name="id"]').val();
        fd.append('payment_type_id', $('select[name="payment_type_id"]').val());
        fd.append('payment_metod_id', $('select[name="payment_metod_id"]').val());

        $.ajax({
            url:Helper.apiUrl('/customer/topup-wallet/checkout/' + id),
            data: fd,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Data Has Been Saved',
                    });
                    window.location.href = Helper.redirectUrl('/customer/topup/checkout/confirmation/' + id);
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                    })
                }
            },
        });
    });
    
    $("#saved").click(function() {
        var fd = new FormData();
        var id = $('input[name="id"]').val();
        fd.append('payment_type_id', $('select[name="payment_type_id"]').val());
        fd.append('payment_metod_id', $('select[name="payment_metod_id"]').val());

        $.ajax({
            url:Helper.apiUrl('/customer/topup-wallet/checkout/' + id),
            data: fd,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Data Has Been Saved',
                    });
                    window.location.href = Helper.redirectUrl('/customer/topup/checkout/confirmation/' + id);
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                    })
                }
            },
        });
    });
</script>

<script>
    $('#payment_type').change(function(){
        if( $(this).val() == 1){
            $('#bank_transfer').prop('hidden', false);
            $('#btn_bank_transfer').prop('hidden', false);
            $('#btn_disabled').prop('hidden', true);
            $('#btn_midtrans').prop('hidden', true);
            $('#total_kode_uniq').show();
            $('#kode_uniq').show();
            $('#total_awal').hide()
        }else if($(this).val() == 2){
            $('#total_kode_uniq').hide()
            $('#kode_uniq').hide();
            $('#total_awal').show();
            $('#btn_midtrans').prop('hidden', false);
            $('#bank_transfer').prop('hidden', true);
            $('#btn_bank_transfer').prop('hidden', true);
            $('#btn_disabled').prop('hidden', true);
            $('#bank_transfer').prop('hidden', true);
            // $('#btn_bank_transfer').prop('hidden', true);
        }
    });
</script>

@endsection
