@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="mb-12 card">
        <div class="card-header">

            <div class="btn-actions-pane-right">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    NEW TOP UP
                </button>
            </div>
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5>History Topup</h5>
                    <table id="table-list-orders" class="display table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Nominal</th>
                                <th>Payment</th>
                                <th>Cashback/Discount</th>
                                <th>Date</th>
                                <th>status</th>
                                <th>Expired At</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div><br><br><br>
            </div>
        </div>
    </div><hr>
</div>



{{-- modal --}}
<form id="form-topup">
    <div class="modal fade" id="exampleModal" data-backdrop="false" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="inputs">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="10000" /> Rp.10,000</div>
                                    <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="50000" /> Rp.50,000</div>
                                    <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="200000" /> Rp.200,000</div>
                                    <div class="col-md-3"><input type="radio" name="radiogroup" id="radiogroup" class="radiogroup" value="500000" /> Rp.500,000</div>
                                </div>

                            </div>
                        </div><br>
                      </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Nominal Top up</label>
                        <input type="number" class="form-control" name="nominal" placeholder="10000" id="amount" />
                        <input type="hidden" class="form-control" id="topup" name="note" placeholder="10000">
                        <small><strong>Note : </strong> Minimal Topup Rp 10.000.00</small>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary waves-effect" type="submit">
                        Top up
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection


@section('script')
@include('admin.master.batch.batch_js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>
    Helper.onlyNumberInput('#topup');
    $('.radiogroup').change(function(e){
        var selectedValue = $(this).val();
        $('#amount').val(selectedValue)
    });

    $(document).ready(function() {
        $('#service-detail').DataTable();
    } );

    var table = $('#table-list-orders').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        select: true,
        dom: 'Bflrtip',
        ordering:'true',
        order: [1, 'desc'],
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search..."
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/customer/topup-wallet/datatables'),
            type: "get",

        },
        columns: [
            {
                data: "nominal",
                name: "nominal",
                render: $.fn.dataTable.render.number( ',', '.,', 0, 'Rp. ', '.00' )
            },
            {
                data: "payment_metod_type",
                name: "payment_metod_type",
                // render: $.fn.dataTable.render.number( ',', '.,', 0, 'Rp. ', '.00' )
                render: function(data, type, full){
                    if(full.payment_metod_type != null){
                        return full.payment_metod_type
                    }else{
                        return '-'
                    }
                }
            },
            {
                data: "number_of_pieces",
                name: "number_of_pieces",
                // render: $.fn.dataTable.render.number( ',', '.,', 0, 'Rp. ', '.00' )
                render: function(data, type, full){
                    if(full.voucer != null){
                        if(full.voucher_id != null){
                            if(full.voucher_type == 1){
                                return "+ Rp," + full.number_of_pieces
                            }else if(full.voucher_type == 2){
                                return "- Rp," + full.number_of_pieces
                            }
                        }else{
                            return '-'
                        }
                    }else{
                        return '-';
                    }
                }
            },
            {
                data: "created_at",
                name: "created_at",
                render: function(data, type, full){
                    return moment(full.created_at).format("DD MMMM YYYY hh:mm");
                    
                }
            },
            {
                data: "status",
                name: "status",
                orderable: false,
                searchable: false,
                render: function(data, type, full) {
                    if (full.status == 'unpaid') {
                        return "<span class='badge badge-warning btn-sm ' style='color:white'><i class='fa fa-spinner'></i> Pending</span>";
                    }else if(full.status == 'confrim'){
                        return "<span class='badge badge-success btn-sm ' style='color:white'><i class='fa fa-check'></i> Success</span> ";
                    }else if(full.status == 'cancel'){
                        return "<span class='badge badge-danger btn-sm ' style='color:white'><i class='fa fa-close'></i> Cancel</span> ";
                    }else if(full.status == 'processing'){
                        return "<span class='badge badge-primary btn-sm ' style='color:white'><i class='fa fa-spinner'></i> Processing</span> ";
                    }
                    // if (full.transfer_status_id === 0) {
                    //     if(full.attachment === null){
                    //         return "<span class='badge badge-danger btn-sm ' style='color:white'><i class='fa fa-band'></i> Pending</span>";
                    //     }else{
                    //         return "<span class='badge badge-warning btn-sm ' style='color:white'><i class='fa fa-spinner'></i> Processing</span>";
                    //     }
                    // }else if (full.transfer_status_id === 1) {
                    //     return "<span class='badge badge-success btn-sm ' style='color:white'><i class='fa fa-check'></i> Success</span> ";
                    // }
                }
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full){
                    if (full.countdown_autocancel_topup != null) {
                        $(document).ready(function() {
                            $('#clock' + full.id + '').countdown(full.countdown_autocancel_topup, function(event) {
                                $(this).html(event.strftime('%D Days (%H Hour : %M Min)'));
                            });
                        });
                    }
                    if (!$.inArray(full.status, ['unpaid'])) {
                        if(full.countdown_autocancel_topup != null){
                            return "<div class='badge badge-danger ml-2' data-toggle='tooltip' data-placement='top' title='this countdown for auto cancel Topup'><span id='clock" + full.id + "'></span></div>";
                        }else{
                            return '-'
                        }
                    }else{
                        if(!$.inArray(full.status, ['confrim']) || !$.inArray(full.status, ['processing'])){
                            return '-'
                        }else{
                            return "<div class='badge badge-danger ml-2'><i class='fa fa-close'></i> Expired</div>";
                        }
                    }
                    // return link;
                }
            },
            {
                data: "id",
                name: "id",
                orderable: false,
                searchable: false,
                render: function(data, type, full) {
                    // if (full.transfer_status_id === null) {
                    //     if(full.type_transaction_id === 1){
                    //         return "<span class='badge badge-success btn-sm' data-toggle='tooltip' data-html='true' title='Success Top up'><i class='fa fa-check'></i></span>";
                    //     }else{
                    //         return "<a href='' class='badge badge-primary btn-sm' data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>&nbsp;<a href='/customer/topup/checkout/"+ full.id + "' class='badge badge-success btn-sm' data-toggle='tooltip' data-html='true' title='Checkout'><i class='fa fa-shopping-cart'></i></a>"
                    //     }
                    // } else
                    checkout = "<a href='/customer/topup/checkout/"+ full.id + "' class='badge badge-success btn-sm' data-toggle='tooltip' data-html='true' title='Checkout'><i class='fa fa-shopping-cart'></i></a>";
                    detail = "<a href='/customer/topup/details/"+ full.id +"' class='badge badge-primary btn-sm' data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>";
                    confrim = "<a href='/customer/topup/checkout/confirmation/"+ full.id +"' class='badge badge-warning btn-sm' data-toggle='tooltip' data-html='true' title='Confirmation Payment'><i class='fa fa-check'></i></a>";
                    if(full.payment_type_id == 1){
                        if(full.status == 'confrim' || full.status == 'processing' || full.status == 'cancel'){
                            return detail
                        }else{
                            return confrim;
                        }
                        return confrim;
                    }else{
                        if (full.status == 'unpaid') {
                            return checkout;
                        }else if(full.status == 'confrim'){
                            return detail;
                        }else if(full.status == 'cancel'){
                            return detail;
                        }else if(full.status == 'processing'){
                            return detail;
                        }else{
                            return detail;
                        }
                    }
                    
                    // 
                    
                    //     if(full.attachment !== null){
                    //         return "<a href='' class='badge badge-primary btn-sm' data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>";
                    //     }else if(full.payment_metod_id !== null){
                    //         return "<a href='/customer/topup/checkout/confirmation/"+ full.id + "' class='badge badge-warning btn-sm' data-toggle='tooltip' data-html='true' title='Confirmation Top up'><i class='fa fa-check'></i></a>"
                    //     }else{
                    //         return "<a href='/customer/topup/checkout/"+ full.id + "' class='badge badge-success btn-sm' data-toggle='tooltip' data-html='true' title='Checkout'><i class='fa fa-shopping-cart'></i></a>"
                    //     }
                    // }else if (full.transfer_status_id === 1) {
                    //     return "<span class='badge badge-success btn-sm' data-toggle='tooltip' data-html='true' title='Success Top up'><i class='fa fa-check'></i></span>";
                    // }
                }
            }
        ]
    });

    $('#form-topup').submit(function(e){
        data = Helper.serializeForm($(this));

        Helper.loadingStart();
         // post data
        Axios.post(Helper.apiUrl('/customer/topup-wallet/save'), data)
            .then(function(response) {
                // Helper.successNotif('Success, Top up Success, Pleace Confirmation your Payment');
                window.location.href = Helper.redirectUrl('/customer/topup/checkout/' + response.data.data.id);
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

      e.preventDefault();
    })
</script>

@endsection
