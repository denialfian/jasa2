@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Detail History Transactions
            </div>
        </div>
        <input type="hidden" id="id" name="id" value="{{ Request::segment(3) }}">
        <div class="card-body">
            <table id="detail-transaction-history" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>History Type</th>
                        <th>Service</th>
                        <th>Status</th>
                        <th>Technician Name</th>
                        <th>Begining Of Balance</th>
                        <th>Balance Deduction</th>
                        <th>Ending of Balance</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection

@section('script')
<style>
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>
<script>
    var id = $('#id').val();
    globalCRUD
        .select2(".select-category", '/product_category/select2')

    function formatx(d) {
        template = '';
        if(d.spareparts != null){
            template += '<table class="table table-striped">';
                template += '<tr>';
                    template += '<td>No</td>';
                    template += '<td>Part</td>';
                    template += '<td>Price</td>';
                    template += '<td>Qty</td>';
                    template += '<td>Sub Total</td>';
                template += '</tr>';
                // parts = JSON.parse(d.parts.toString());
                // console.log(parts)
                grand_total = 0;
                _.each(d.spareparts, function(i, key) {
                    total = parseInt(i.price) * parseInt(i.quantity)
                    grand_total += total
                    template += '<tr>';
                        template += '<td>' + (key + 1) + '</td>';
                        if(i.name_product){
                            template += '<td>' + i.name_product + '</td>';
                        }else{
                            template += '<td>' + i.name_sparepart + '</td>';
                        }
                        template += '<td> Rp. '  + Helper.thousandsSeparators(i.price) + '</td>';
                        template += '<td>' + i.quantity + '</td>';
                        template += '<td> Rp. ' + Helper.thousandsSeparators(total) + '</td>';
                    template += '</tr>';
                })
                template += '<tr>';
                    template += '<td colspan="4" align="right">Total</td>';
                    template += '<td colspan="2">  Rp. ' + Helper.thousandsSeparators(grand_total) + '</td>';
                template += '</tr>';
            template += '</table>';
        }else{
            template += '<table class="table table-striped">';
                template += '<tr>';
                    template += '<td>No</td>';
                    template += '<td>Part</td>';
                    template += '<td>Qty</td>';
                    template += '<td>Price</td>';
                template += '</tr>';
                template += '<tr>';
                    template += '<td colspan="4" align="center">No Extra Parts</td>';
                template += '</tr>';
            template += '</table>';
        }

        return template;
    }

    $(document).ready(function() {
        var table = $('#detail-transaction-history').DataTable({
            processing: true,
            serverSide: true,
            select: false,
            dom: 'Bflrtip',
            responsive: true,
            order: [2, 'asc'],
            language: {
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/customer/transaction-history/detail/datatables/' + id),
                "type": "get",
                "data": function(d) {
                    return $.extend({}, d, {
                        "extra_search": $('#extra').val()
                    });
                }
            },
            columns: [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {
                    data: "type_history",
                    name: "type_history",
                },
                {
                    data: "service_type",
                    name: "service_type",
                    render: function(data, type, full) {
                        return full.service_type + '</br> <small>' + full.symptom_name + ' ' + full.product_group_name + '</small>'  
                    }
                },
                {
                    data: "status",
                    name: "status",
                    render: function(data, type, full) {
                        if (full.status == 2) {
                            return '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                        } else if (full.status == 3) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.status == 4) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.status == 5) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.status == 6) {
                            return '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                        } else if (full.status == 7) {
                            return '<span class="badge badge-focus">' + full.order_status.name + '</span>';
                        } else if (full.status == 8) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.status == 9) {
                            return '<span class="badge badge-primary">' + full.order_status.name + '</span>';
                        } else if (full.status == 10) {
                            return '<span class="badge badge-success">' + full.order_status.name + '</span>';
                        } else if (full.status == 11) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        }
                    }
                },
                {
                    data: "technician_name",
                    name: "technician_name",
                    render: function(data, type, full) {
                        if(full.technician_name == 1){
                            return full.technician.name
                        }else{
                            return full.technician_name
                        }
                    }
                },
                {
                    data: "beginning_balance",
                    name: "beginning_balance",
                    render: function(data, type, full) {
                        return  'Rp.' + Helper.thousandsSeparators(full.beginning_balance)
                    }
                },
                {
                    data: "number_of_pieces",
                    name: "number_of_pieces",
                    render: function(data, type, full) {
                        if(full.number_of_pieces > 1){
                            return  '-' + ' ' + 'Rp.' +  Helper.thousandsSeparators(full.number_of_pieces)
                        }else{
                            return 'Rp.' + Helper.thousandsSeparators(full.number_of_pieces)
                        }
                    }
                },
                {
                    data: "ending_balance",
                    name: "ending_balance",
                    render: function(data, type, full) {
                        return  'Rp.' + Helper.thousandsSeparators(full.ending_balance)
                    }
                },
                
            ],
        });

        $('#detail-transaction-history tbody').on('click', 'td.details-control', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(formatx(row.data())).show();
                tr.addClass('shown');
            }
        });

    });
</script>
@endsection