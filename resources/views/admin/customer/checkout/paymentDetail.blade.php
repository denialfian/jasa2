@extends('admin.home')
@section('content')
<style>
    div.hr-or {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    div.hr-or:before {
        content: 'OR';
        background-color: #fff;
    }

    div.hr {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    .hr-title {
        background-color: #fff;
    }
</style>

<div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-header">
            {{ $title }}
            <div class="btn-actions-pane-right">
                <a href="{{ url('/customer/request_list') }}" class="btn btn-primary btn-sm add-bank-transfer">Back</a>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="container">
                        <label for="" class="">Order Code</label>
                        <input type="text" id="" class="form-control" value="{{ $detailOrder->code }}" readonly>
                        <input type="hidden" id="" class="form-control" name="id" value="1" readonly>

                        <label for="" class="">Email</label>
                        <input type="text" id="" class="form-control" value="{{ $detailOrder->user->email }}" readonly>

                        <label for="" class="">Bank</label>
                        <input type="text" id="" class="form-control" value="{{ $detailOrder->bank->bank_name }} - {{ $detailOrder->bank->name }} - {{ $detailOrder->bank->virtual_code }}" readonly>

                        <label for="" class="">A.N Rekening</label>
                        <input type="text" id="" value="{{ $user->a_n_bank }}" class="form-control" name="a_n_bank" readonly>

                        <label for="" class="">Tanggal Transfer</label>
                        <input name="transfer_date" placeholder="" value="{{ date('d-M-y', strtotime($user->transfer_date)) }}" type="text" class="form-control" readonly>

                        <input type="hidden" class="form-control" name="ms_order_id" value="{{ $detailOrder->id }}">
                        <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">

                        <label for="" class="">Nominal</label>
                        <input type="text" id="" class="form-control" name="nominal" value="{{ number_format($detailOrder->grand_total) }}" readonly>

                        <label for="" class="">Bukti Transfer</label>
                        <input type="file" id="profile-image-upload" class="form-control" name="attachment" readonly>

                        <label for="" class="">Catatan</label>
                        <textarea class="form-control" id="address" rows="3" name="note" readonly>{{ $user->note }}</textarea><br>


                        <button type="submit" class="btn btn-primary btn-lg btn-block" id="save" disabled>Waiting Verification Admin</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
<link rel="stylesheet" type="text/css" href="{{ asset('/datePicker/jquery.datetimepicker.css') }}" >
<script src="{{ asset('/datePicker/jquery.datetimepicker.full.min.js') }}"></script>
<script>
    $(document).ready(function(){
        jQuery('#datetimepicker').datetimepicker({
            format:'Y-m-d',
            mask:true
        });
    });
</script>
@endsection
