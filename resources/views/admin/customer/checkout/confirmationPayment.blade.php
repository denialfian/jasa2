@extends('admin.home')
@section('content')
<style>
    div.hr-or {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    div.hr-or:before {
        content: 'OR';
        background-color: #fff;
    }

    div.hr {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #eee;
        text-align: center;
        height: 0px;
        line-height: 0px;
    }

    .hr-title {
        background-color: #fff;
    }
</style>

<div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-header">
            {{ $title }}
            <div class="btn-actions-pane-right">
                <a href="{{ url('/customer/request_list') }}" class="btn btn-primary btn-sm add-bank-transfer">Back</a>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    
                        <div class="container">
                        <div class="form-group">
                            <label for="" class="">Order Code</label>
                            <input type="text" id="" class="form-control" value="{{ $detailOrder->code }}" readonly>
                        </div>
                        <input type="hidden" id="" class="form-control" name="id" value="1" readonly style="margin-bottom: 200px;">

                        <div class="form-group">
                            <label for="" class="">Email</label>
                            <input type="text" id="" class="form-control" value="{{ $detailOrder->user->email }}" readonly>
                        </div>

                        <div class="form-group">
                            <label for="" class="">Bank</label>
                            <input type="text" id="" class="form-control" value="{{ $detailOrder->bank->bank_name }} - SDR.Testing - {{ $detailOrder->bank->virtual_code }}" readonly>
                        </div>

                        <div class="form-group">
                            <label for="" class="">A.N Rekening</label>
                            <input type="text" id="" class="form-control" name="a_n_bank" required>
                            <strong><span id="error-a_n_bank" style="color:red"></span></strong>
                        </div>
                        

                        <div class="form-group">
                            <label for="" class="">Tanggal Transfer</label>
                            <input name="transfer_date" id="datetimepicker" placeholder="" type="text" class="form-control">
                            <strong><span id="error-transfer_date" style="color:red"></span></strong>
                        </div>
                        

                        <input type="hidden" class="form-control" name="ms_order_id" value="{{ $detailOrder->id }}">
                        <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}">

                        <div class="form-group">
                            <label for="" class="">Nominal</label>
                            <input type="number" id="" class="form-control" name="nominal" value="{{ $detailOrder->grand_total }}" >
                            <strong><span id="error-nominal" style="color:red"></span></strong>
                        </div>
                        
                        

                        <div class="form-group">
                            <label for="" class="">Bukti Transfer</label>
                            <input type="file" id="profile-image-upload" class="@error('attachment') is-invalid @enderror form-control" name="attachment" >
                            <strong><span id="error-attachment" style="color:red"></span></strong>
                            
                            
                        </div>

                        <div class="form-group">
                            <label for="" class="">Catatan</label>
                            <textarea class="form-control"  rows="3" name="note" ></textarea><br>
                            <strong><span id="error-note" style="color:red"></span></strong>
                        </div>
                        


                        <button type="submit" class="btn btn-primary btn-lg btn-block" id="save">Confirmation</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('script')
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('/datePicker/jquery.datetimepicker.css') }}" >
<script src="{{ asset('/datePicker/jquery.datetimepicker.full.min.js') }}"></script> --}}
{{-- <script>
    $(document).ready(function(){
        jQuery('#datetimepicker').datetimepicker({
            format:'Y-m-d',
            mask:true
        });
    });
</script> --}}

<script>
   $("#save").click(function() {
        var fd = new FormData();
        var files = $('#profile-image-upload')[0].files[0];
        if(files){
            fd.append('attachment', files);
        }
        // fd.append('id', $('input[name="id"]').val());
        fd.append('a_n_bank', $('input[name="a_n_bank"]').val());
        fd.append('transfer_date', $('input[name="transfer_date"]').val());
        fd.append('nominal', $('input[name="nominal"]').val());
        fd.append('note', $('textarea[name="note"]').val());
        fd.append('ms_order_id', $('input[name="ms_order_id"]').val());
        fd.append('user_id', $('input[name="user_id"]').val());

        $.ajax({
            url:Helper.apiUrl('/confrim'),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Data Has Been Saved',
                    });
                    // window.location = Helper.url('/customer/request_list');
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])

                    })
                }
            },
        });
    });
</script>

@endsection
