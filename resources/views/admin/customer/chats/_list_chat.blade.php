@foreach($chats as $chat)
<a data-room="{{ $chat->id }}" data-user="{{ $chat->members_group->first()->user->id }}" data-name="{{ $chat->members_group->first()->user->full_name }}" href="#" class="chat-room filterDiscussions all unread single {{ $chat->id == $chat_id ? 'active' : '' }}" data-toggle="list" role="tab">
    <img class="avatar-md" src="{{ $chat->members_group->first()->user->avatar }}" data-toggle="tooltip" data-placement="top" title="{{ $chat->members_group->first()->user->full_name }}" alt="avatar" />
    <div class="data">
        <h5>{{ $chat->members_group->first()->user->full_name }}</h5>
        @if($chat->unread_message_count > 0)
        <div class="new bg-red chat-list-count-{{ $chat->id }}">
            <span>{{ $chat->unread_message_count }}+</span>
        </div>
        @endif
        @if($chat->unreads_count > 0)
        <div class="new bg-red chat-list-count-{{ $chat->id }}">
            <span>{{ $chat->unreads_count }}+</span>
        </div>
        @endif
        <span>{{ $chat->last_message->created_at->format('H:i') }}</span>
        <p class="chat-list-msg-{{ $chat->id }}">{{ $chat->last_message->is_html == 1 ? '-' : Str::limit($chat->last_message->message, 20) }}</p>
        @if($chat->is_has_order == 1)
        <span class="badge" style="background-color: #444054; color: #fff;">#{{ $chat->room_no }}</span>
        @endif 
    </div>
</a>
@endforeach