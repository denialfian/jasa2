<p>{{ $message }}</p>
<hr />
<ul class="list-group list-group-flush">
    @foreach($orders as $order)
    <li class="list-group-item">
        <div class="widget-content p-0">
            <div class="widget-content-wrapper">
                <div class="widget-content-left">
                    <div class="widget-heading"><a href="{{ url('/customer/service_detail/' . $order->id) }}"><i class="fa fa-arrow-right"></i> {{ $order->code }}</a></div>
                </div>
                <div class="widget-content-right">
                    <div style="margin-left: 20px;" class="badge badge-danger">{{ $order->order_status->name }}</div>
                </div>
            </div>
        </div>
    </li>
    @endforeach
</ul>