<div class="action-chat-area">
@if($order->orders_statuses_id == 7)
    <label for="ms_symptom_code">Order Code</label>
    <p><a target="__blank" href="{{ $url_order }}"><i class="fa fa-arrow-right"></i> {{ $order->code }}</a></p>
    <hr/>

    <label for="ms_symptom_code">Symptom Code</label>
    <p>{{ $order->symptom_code->name ?? '-' }}</p>
    <label for="ms_symptom_code">Repair Code</label>
    <p>{{ $order->repair_code->name ?? '-' }}</p>
    <label for="repair_desc">Repair Description</label>
    <p>{{ $chat->order->repair_desc }}</p>
    <hr />
    @if($order->users_id == $auth->id)
        @if($chat->can_action == 1)
            <button class="btn btn-primary btn-hover-shine chat-btn-job-complate" data-chat-message="{{ $chat->id }}" data-id="{{ $order->id }}">Complete Job</button>
        @endif
    @endif

@endif

@if($order->orders_statuses_id == 9)
    <label for="ms_symptom_code">Order Code</label>
    <p><a target="__blank" href="{{ $url_order }}"><i class="fa fa-arrow-right"></i> {{ $order->code }}</a></p>
    <hr/>

    @if(isset($order->detail->teknisi_email))
        @if($order->detail->teknisi_email == $auth->email)
            @if($chat->can_action == 1)
            <button class="btn btn-primary btn-hover-shine chat-btn-job-working" data-chat-message="{{ $chat->id }}" data-id="{{ $order->id }}">On Working Job</button>
            @endif
        @endif
    @endif

@endif

@if($order->orders_statuses_id == 4)
    <label for="ms_symptom_code">Order Code</label>
    <p><a target="__blank" href="{{ $url_order }}"><i class="fa fa-arrow-right"></i> {{ $order->code }}</a></p>
    <hr/>

    @if(isset($order->detail->teknisi_email))
        @if($order->detail->teknisi_email == $auth->email)
            @if($chat->can_action == 1)
                <button class="btn btn-primary btn-hover-shine chat-btn-job-done" data-chat-message="{{ $chat->id }}" data-id="{{ $order->id }}">Done Job</button>
            @endif
        @endif
    @endif
    
@endif

@if($order->orders_statuses_id == 10)
    <label for="ms_symptom_code">Order Code</label>
    <p><a target="__blank" href="{{ $url_order }}"><i class="fa fa-arrow-right"></i> {{ $order->code }}</a></p>
    <hr/>    
@endif

@if($order->orders_statuses_id == 2)
    <label>Order Code</label>
    <p><a target="__blank" href="{{ $url_order }}"><i class="fa fa-arrow-right"></i> {{ $order->code }}</a></p>
    <hr/>    
@endif

@if($order->orders_statuses_id == 3)
    <label>Order Code</label>
    <p><a target="__blank" href="{{ $url_order }}"><i class="fa fa-arrow-right"></i> {{ $order->code }}</a></p>
    <hr/>    
@endif

@if($order->orders_statuses_id == 5)
    <label>Order Code</label>
    <p><a target="__blank" href="{{ $url_order }}"><i class="fa fa-arrow-right"></i> {{ $order->code }}</a></p>
    <hr/>    
@endif
</div>