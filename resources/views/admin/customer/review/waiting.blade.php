@extends('admin.home')
@section('content')
<div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
    <div class="card">
        <div class="card-header">
            {{ $title }}
            <div class="btn-actions-pane-right">
                <a href="{{ url('/customer/reviews/waiting') }}" class="btn btn-focus">
                    Waiting For Review ({{ $order_count }})
                </a>
                <a href="{{ route('customer.reviews.done') }}" class="btn btn-default">
                    My Review
                </a>
            </div>
        </div>
    </div>
</div>

@foreach ($orders as $order)
<div class="col-md-6 col-xl-6" style="margin-bottom: 20px">
    <div class="card" style="box-shadow: none;">
        <div class="card-header" style="color: #fff; background: #434055;">
            {{ $order->code }}
        </div>
        <div class="card-body">
            <p>{{ $order->schedule->format('d F Y H:i:s') }}</p>
            <div class="user-profile">
                <img class="avatar" src="{{ $order->service_detail->first()->technician->user->avatar }}" alt="Ash" />
                <div class="username">{{ $order->service_detail->first()->technician->user->name }}</div>
                <div class="bio">
                    <p>{{ $order->service_detail->first()->technician->user->email }}</p>
                    {!! $order->service_detail->first()->technician->getRatingStarUi() !!}
                </div>
            </div>
            <div class="form-area-review">
                <input 
                    id="rating-star-{{ $order->id }}" 
                    name="star" 
                    type="hidden" 
                    value="1"/>
                <div class="main">
                    <div class="star-border" data-order="{{ $order->id }}">
                        <div class="star star-order-{{ $order->id }}" style="background: gold">
                        </div>
                    </div>
                    @foreach (range(1, 4) as $star)
                        <div class="star-border" data-order="{{ $order->id }}">
                            <div class="star star-order-{{ $order->id }}">
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="position-relative form-group">
                    <input type="hidden" name="technician_id" class="technician_id-{{ $order->id }}" value="{{ $order->service_detail->first()->technician->id }}" />
                    <input type="hidden" name="order_id" value="{{ $order->id }}" />
                    <textarea class="form-control note-{{ $order->id }}" name="note" placeholder="Message" required></textarea>
                </div>
                <button class="btn btn-dark btn-sm btn-save-review" type="button" data-order="{{ $order->id }}">
                    <i class="fa fa-pencil"></i> Save
                </button>
            </div>
        </div>
    </div>
</div>
@endforeach

<style>
    .form-area-review{
        box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
        padding: 10px;
    }
    .user-profile {
        box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
        margin: auto;
        height: 8em;
        background: #fff;
        border-radius: .3em;
        margin-bottom: 20px;
    }

    .user-profile .username {
        margin: auto;
        margin-top: -4.40em;
        margin-left: 5.80em;
        color: #658585;
        font-size: 1.53em;
        font-family: "Coda", sans-serif;
        font-weight: bold;
    }

    .user-profile .bio {
        margin: auto;
        display: inline-block;
        margin-left: 10.43em;
        margin-bottom: 10px;
        color: #e76043;
        font-size: .87em;
        font-family: "varela round", sans-serif;
    }

    .user-profile>.description {
        margin: auto;
        margin-top: 1.35em;
        margin-right: 4.43em;
        width: 14em;
        color: #c0c5c5;
        font-size: .87em;
        font-family: "varela round", sans-serif;
    }
     .user-profile>img.avatar {
        padding: .7em;
        margin-left: .3em;
        margin-top: .3em;
        height: 6.23em;
        width: 6.23em;
        border-radius: 18em;
    }
</style>
<style>
    .star{
        width:30px;
        height:30px;
        -webkit-clip-path: polygon(50% 0%, 61% 35%, 98% 35%, 68% 57%, 79% 91%, 50% 70%, 21% 91%, 32% 57%, 2% 35%, 39% 35%);
      clip-path: polygon(50% 0%, 61% 35%, 98% 35%, 68% 57%, 79% 91%, 50% 70%, 21% 91%, 32% 57%, 2% 35%, 39% 35%);
        background:white;
    }
    .star-border{
        filter: drop-shadow(0px 0px 2px black);
        display:inline-block;
    }
    .star-border:hover{
        cursor:pointer;
    }
    .rounded-circle{
        width: 60px;
        height: auto;
    }
    .card .widget-content {
        border: 1px solid #c1c3c5;
    }
    .widget-content-wrapper{
        margin: 10px;
    }
    .rating-block{
      background-color:#FAFAFA;
      border:1px solid #EFEFEF;
      padding:15px 15px 20px 15px;
      border-radius:3px;
    }
</style>
@endsection

@section('script')
<script>
    $( document ).ready(function() {
        var set = 0;
        $(".star-border").click(function(){
            order_id = $(this).attr('data-order');
            $(this).find(".star-order-" + order_id).css("background","gold");
            $(this).prevAll().find(".star-order-" + order_id).css("background","gold");
            $(this).nextAll().find(".star-order-" + order_id).css("background","white");
            set = $(this).prevAll().length + 1;
            $('#rating-star-' + order_id).val(set);
            console.log(set);
        }); 
    });

    $('.btn-save-review').click(function(e){
        order_id = $(this).attr('data-order');
        var data = {
            note: $('.note-' + order_id).val(),
            technician_id: $('.technician_id-' + order_id).val(),
            star: $('#rating-star-' + order_id).val(),
            order_id: order_id
        }

        Helper.loadingStart();
        Axios.post('/customer/reviews', data)
            .then(function(response) {
                // send notif
                Helper.successNotif(response.data.msg);
                // redirect 
                Helper.redirectTo('/customer/reviews/done')
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)                    
            });
    })
</script>
@endsection
