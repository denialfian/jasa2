@extends('admin.home')
@section('content')
<form id="push-form" style="display: contents;">
    <div class="col-md-12">
        <div class="card">
            <div class="header-bg card-header-tab card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    {{ $title }}
                </div>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label>User</label>
                    <select name="device_token_push" id="device_token_push" class="form-control" required>
                        <option value="" selected>Choise</option>
                        @foreach($users as $user)
                        <option value="{{ $user->device_token }}">{{ $user->email }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Title</label>
                    <input name="title_push" placeholder="title_push" id="title_push" type="text" class="form-control" required>
                </div>
                <div class="form-group">
                    <label>Body</label>
                    <input name="body_push" placeholder="body_push" id="body_push" type="text" class="form-control" required>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Add New</button>
            </div>
        </div>
    </div>
</form>


@endsection

@section('script')
<script>
    $('#push-form').submit(function(e) {
        data = {
            title: $('#title_push').val(),
            body: $('#body_push').val(),
            device_token: $('#device_token_push').val(),
        }
        Helper.loadingStart();
        // post data
        Axios.post(Helper.apiUrl('/firebase_send_push'), data)
            .then(function(response) {
                Helper.successNotif('Success');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
        e.preventDefault();
    })
</script>
@endsection