@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <div class="vertical-time-icons vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                @foreach($notifications as $notification)
                <?php $datan = (json_decode($notification->data)) ?>
                <div class="vertical-timeline-item vertical-timeline-element">
                    <div>
                        <div class="vertical-timeline-element-icon bounce-in">
                            <div class="timeline-icon {{ $datan->color }}">
                                <i class="{{ $datan->icon }} text-white"></i>
                            </div>
                        </div>
                        <div class="vertical-timeline-element-content bounce-in">
                            <h4 class="timeline-title">
                                <a href="{{ route('noty.read', ['id' => $notification->id]) }}">{{ $datan->name }}</a>
                            </h4>
                            <p>{{ $datan->description }}
                                <a href="javascript:void(0);">{{ \Carbon\Carbon::parse($notification->created_at)->diffForHumans() }}</a>
                            </p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12" style="margin-top:20px;">
    <div class="text-center">
        {{ $notifications->links() }}
    </div>
</div>
@endsection

@section('script')
@endsection