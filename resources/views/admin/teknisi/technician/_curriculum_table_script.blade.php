<script>
    var Curriculum = {
		staticTable: function(selector){
			return $(selector).DataTable({
	            dom: 'Bflrtip',
	            responsive: true,
	            language: {
	                search: '',
	                searchPlaceholder: "Search..."
	            },
	            oLanguage: {
	                sLengthMenu: "_MENU_",
	            },
	            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
	                "<'row'<'col-sm-12'tr>>" +
	                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
	            columns: [
	            	{
	                    data: "curriculum_name",
	                    name: "curriculum_name",
	                },

	                {
	                	render: function(data, type, full) {
		                    return "<button type='button' class='delete-curriculum btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>";
		                }
	                }
	            ],
	        });
		},
		table: function(config){
			return $(config.selector).DataTable({
	            processing: true,
                serverSide: true,
                select: true,
                dom: 'Bflrtip',
                responsive: true,
                language: {
                    search: '',
                    searchPlaceholder: "Search..."
                },
                oLanguage: {
                    sLengthMenu: "_MENU_",
                },
                dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                ajax: {
                    url: Helper.apiUrl(config.url),
                    "type": "get"
                },
	            columns: [
	            	{
	                    data: "curriculum.curriculum_name",
	                    name: "curriculum.curriculum_name",
	                },

	                {
                        data: "id",
	                    name: "id",
	                	render: function(data, type, full) {
		                    return "<button data-id='"+full.id+"' type='button' class='delete-curriculum btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>";
		                }
	                }

	            ],
	        });
		}
	};
</script>
