<form id="form-curriculum">
    <div class="modal fade" data-backdrop="false" id="modal-curriculum" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content" >
                <div class="modal-header">
                    <h4 class="modal-title">
                        Add Curriculum
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body" style="max-height: calc(100% - 120px);overflow-y: scroll;">
                    <input id="id" name="id" type="hidden" class="curriculum-id" />
                    <label>Name</label>
                    <div class="form-group">
                        <select class="form-control curriculum-select" id="curriculum-select" name="curriculum-select" style="width:100%">
                        </select>
                    </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        Save
                    </button>
                    <button class="btn waves-effect" data-dismiss="modal" type="button">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
