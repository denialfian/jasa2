<script>
$('#value').on('keyup', function() {
        commission($(this).val());
    });
    function commission(thisObj) {
        Helper.unMask('#value');
        var price = 0;
        if (thisObj != '') {
            price = thisObj;
        }
        $('#save_price_service').prop('disabled', false);
        var commission = parseInt($('#commission').val()); // commission fixed rate
        var after_commission = 0;
        if($('#commission_type').val() == 1){
            deduction = parseInt(price) * (parseInt($('#commission').val()) / 100); // commission percent rate
            after_commission = parseInt(price) - deduction;
        } else {
            after_commission = parseInt(price) - commission;
            if(parseInt(price) < commission) {
                Helper.warningNotif('Nominal must not be less than the commission !')
                $('#save_price_service').prop('disabled', true);
            }
        }

        $('#commission_display').show();
        $('#after_commission_display').show();
        $('#commission_value').val(after_commission);
        $('#after_commission').val('Rp. '+Helper.toCurrency(after_commission));
    }
</script>
