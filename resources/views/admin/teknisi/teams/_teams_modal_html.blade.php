<form id="form-teams">
    <div class="modal fade" data-backdrop="false" id="modal-teams" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        ADD YOUR TEAM
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <input name="id" type="hidden" class="team-id" />
                        <div>
                            <label>Name Team</label>
                            <div class="form-group">
                                <input class="form-control name" id="name" name="name" class="name" placeholder="Name Team" type="text" required />
                            </div>
                        </div>
                        <div>
                            <label>Description</label>
                            <div class="form-group">
                                <textarea id="description" class="form-control description" name="description" placeholder="Description Team" required></textarea>
                            </div>
                        </div>
                        <label>
                            Select Technician Invitation
                        </label>
                        {{-- <div class="form-group">
                            <select class="form-control member-technician-select" data-placeholder="Email" name="member-technician-select[]" multiple="multiple" style="width:100%" id="member-technician-select" required>
                            </select>
                        </div> --}}
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" id="technicians_email" name="technicians_email" placeholder="" autocomplete="off" class="form-control" />
                            </div>
                            {{-- <br />
                            <span id="name_email"></span>
                            <input type="hidden" name="technicians_id" id ="technicians_id"> --}}
                        </div>

                        {{-- <label>
                            Invitation with Email
                        </label>
                        <div class="form-group">
                            <select class="form-control invitation-email-select" name="invitation-email-select[]" multiple="multiple" required style="width:100%" id="invitation-email-select" required>
                                ....
                            </select>
                        </div> --}}
                    </input>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        Save
                    </button>
                    <button class="btn waves-effect" data-dismiss="modal" type="button">
                        Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>


