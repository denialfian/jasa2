<script>
    var TeamsTechnician = {
		table: function(url = '/teknisi/teams/datatables'){
			return $('#table-teams-technicians').DataTable({
	            processing: true,
                serverSide: true,
                select: true,
                dom: 'Bflrtip',
                responsive: true,
                language: {
                    search: '',
                    searchPlaceholder: "Search..."
                },
                oLanguage: {
                    sLengthMenu: "_MENU_",
                },
                dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                ajax: {
                    url: Helper.apiUrl(url),
                    "type": "get"
                },
	            columns: [
	            	{
	                    data: "name",
	                    name: "name",
	                    orderable: false,
	                },
	            	{
	                    data: "description",
	                    name: "description",
	                    orderable: false,
	                },
	            	{
                        data: "team_technician",
	                    name: "team_technician",
	                    orderable: false,
                        className: 'align-bottom',
                        render: function(data, type, full) {
                            var html = '<ul>';
                            if(data.length > 0) {
                                _.each(data, function(attr) {
                                    if(full.technicians_id !== attr.technicians_id) {
                                        html += '<li><span class="badge badge-success ml-2">'+attr.technician.name+'</span></li>';
                                    }
                                });
                            }
                            html += '</ul>';
                            if(data.length == 0) {
                                return '<center>-</center>';
                            } else {
                                return html;
                            }

                        }
	                },
                    {
                        data: "team_invite",
	                    name: "team_invite",
                        orderable: false,
                        className: 'align-bottom',
                        render: function(data, type, full) {
                            var html = '<ul>';
                            if(data.length > 0) {
                                _.each(data, function(attr) {
                                    if(full.technicians_id !== attr.technicians_id) {
                                        html += '<li><span class="badge badge-secondary ml-2">'+attr.technician.name+'</span></li>';
                                    }
                                });
                            }
                            html += '</ul>';
                            if(data.length == 0) {
                                return '<center>-</center>';
                            } else {
                                return html;
                            }
                        }
                    },
                    {
	                    data: "technician.name",
	                    name: "technician.name",
                        className: 'align-text-top',
	                    orderable: false,
                        render: function(data, type, full) {
                            return '<i class="fa fa-check-o" style="color:orange"></i><span class="badge badge-primary ml-2">'+data+'</span>';
                        }
	                },

	                {
                        data: "id",
	                    name: "id",
	                	render: function(data, type, full) {

		                    return "<button title='Edit Team' data-id='"+data.id+"' type='button' class='edit-team btn btn-warning btn-white btn-sm'><i class='fa fa-pencil-square-o'></i></button> <button data-id='"+data.id+"' title='Delete Team' type='button' class='delete-team btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
		                }
	                }
	            ],
	        });
		}
	};

    //his member
    var TeamMember = {
		table: function(url = '/teknisi/teams/member/datatables'){
			return $('#table-teams-member').DataTable({
	            processing: true,
                serverSide: true,
                select: true,
                dom: 'Bflrtip',
                responsive: true,
                language: {
                    search: '',
                    searchPlaceholder: "Search..."
                },
                oLanguage: {
                    sLengthMenu: "_MENU_",
                },
                dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                ajax: {
                    url: Helper.apiUrl(url),
                    "type": "get"
                },
	            columns: [
	            	{
	                    data: "name",
	                    name: "name",
	                    orderable: false,
	                },
	            	{
	                    data: "description",
	                    name: "description",
	                    orderable: false,
	                },
	            	{
	                    data: "team_technician",
	                    name: "team_technician",
	                    orderable: false,
                        className: 'align-bottom',
                        render: function(data, type, full) {
                            var html = '<ul>';
                            _.each(data, function(attr) {
                                if(full.technician.id !== attr.technicians_id) {
                                    html += '<li><span class="badge badge-secondary ml-2">'+attr.technician.name+'</span></li>';
                                }
                            });
                            html += '</ul>';
                            return html;
                        }
	                },
                    {
	                    data: "technician.name",
	                    name: "technician.name",
                        className: 'align-text-top',
	                    orderable: false,
                        render: function(data, type, full) {
                            return '<i class="fa fa-crown" style="color:orange"></i><span class="badge badge-primary ml-2">'+data+'</span>';
                        }
	                },

	                {
	                	render: function(data, type, full) {
		                    return "<button title='Leave Team' data-id='"+full.id+"' type='button' class='leave-team btn btn-danger btn-white btn-sm'><i class='fa fa-sign-in-alt'></i></button>";
		                }
	                }
	            ],
	        });
		}
	};

    //team Invitation
    var TeamInvitation = {
		table: function(url = '/teknisi/teams/invitation/datatables'){
			return $('#table-teams-invitation').DataTable({
	            processing: true,
                serverSide: true,
                select: true,
                dom: 'Bflrtip',
                responsive: true,
                language: {
                    search: '',
                    searchPlaceholder: "Search..."
                },
                oLanguage: {
                    sLengthMenu: "_MENU_",
                },
                dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                ajax: {
                    url: Helper.apiUrl(url),
                    "type": "get"
                },
	            columns: [
	            	{
	                    data: "tech_team.name",
	                    name: "tech_team.name",
	                    orderable: false,
	                },
	            	{
	                    data: "tech_team.description",
	                    name: "tech_team.description",
	                    orderable: false,
	                },

                    {
	                    data: "tech_team.technician.name",
	                    name: "tech_team.technician.name",
                        className: 'align-text-top',
	                    orderable: false,
                        render: function(data, type, full) {
                            return '<i class="fa fa-check" style="color:orange"></i><span class="badge badge-primary ml-2">'+data+'</span>';
                        }
	                },

	                {
	                	render: function(data, type, full) {
		                    return "<button title='Approve Invitation ' data-id='"+full.id+"' type='button' class='approve-team btn btn-success btn-white btn-sm'><i class='fa fa-check-square'></i></button>";
		                }
	                }
	            ],
	        });
		}
	};


</script>
