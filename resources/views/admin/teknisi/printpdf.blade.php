<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Delivery Note : </title>

<style type="text/css">
	body {
	-webkit-print-color-adjust: exact !important;
	}


    * {
        font-family: Verdana, Arial, sans-serif;
    }
    /* table{
        font-size: x-small;
    } */
	table tr td
	table tr th {
		font-size: 9pt;
        padding-left: 5px;
		/* font-weight: bold; */
	}

	p {
		font-size: 9pt;
		margin: 0px;
	}

    .gray {
        background-color: lightgray
    }

    .column {
        float: left;
        width: 50%;
    }

    /* Clear floats after the columns */
    .row:after {
        content: "";
        display: table;
        clear: both;
    }

	table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }
</style>
<style type="text/css">
	html { font-family:Calibri, Arial, Helvetica, sans-serif; font-size:11pt; background-color:white }
	a.comment-indicator:hover + div.comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em }
	a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em }
	div.comment { display:none }

	.gridlines td { border:1px dotted black }
	.gridlines th { border:1px dotted black }
	.b { text-align:center }
	.e { text-align:center }
	.f { text-align:right }
	.inlineStr { text-align:left }
	.n { text-align:right }
	.s { text-align:left }
	td.style0 { vertical-align:middle; border-bottom:none #000000; border-top:none #000000; border-left:none #000000; border-right:none #000000; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style0 { vertical-align:middle; border-bottom:none #000000; border-top:none #000000; border-left:none #000000; border-right:none #000000; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	td.style1 { vertical-align:bottom; text-align:center; border-bottom:2px solid #000000 !important; border-top:2px solid #000000 !important; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; font-weight:bold; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:#D9DCE1 }
	th.style1 { vertical-align:bottom; text-align:center; border-bottom:2px solid #000000 !important; border-top:2px solid #000000 !important; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; font-weight:bold; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:#D9DCE1 }
	td.style2 { vertical-align:bottom; text-align:center; border-bottom:2px solid #000000 !important; border-top:2px solid #000000 !important; border-left:2px solid #000000 !important; border-right:2px solid #000000 !important; font-weight:bold; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:#D9DCE1 }
	th.style2 { vertical-align:bottom; text-align:center; border-bottom:2px solid #000000 !important; border-top:2px solid #000000 !important; border-left:2px solid #000000 !important; border-right:2px solid #000000 !important; font-weight:bold; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:#D9DCE1 }
	td.style3 { vertical-align:middle; border-bottom:1px solid #000000 !important; border-top:none #000000; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style3 { vertical-align:middle; border-bottom:1px solid #000000 !important; border-top:none #000000; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	td.style4 { vertical-align:middle; border-bottom:1px solid #000000 !important; border-top:none #000000; border-left:1px solid #000000 !important; border-right:2px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style4 { vertical-align:middle; border-bottom:1px solid #000000 !important; border-top:none #000000; border-left:1px solid #000000 !important; border-right:2px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	td.style5 { vertical-align:middle; border-bottom:1px solid #000000 !important; border-top:1px solid #000000 !important; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style5 { vertical-align:middle; border-bottom:1px solid #000000 !important; border-top:1px solid #000000 !important; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	td.style6 { vertical-align:middle; border-bottom:1px solid #000000 !important; border-top:1px solid #000000 !important; border-left:1px solid #000000 !important; border-right:2px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style6 { vertical-align:middle; border-bottom:1px solid #000000 !important; border-top:1px solid #000000 !important; border-left:1px solid #000000 !important; border-right:2px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	td.style7 { vertical-align:bottom; border-bottom:none #000000; border-top:1px solid #000000 !important; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style7 { vertical-align:bottom; border-bottom:none #000000; border-top:1px solid #000000 !important; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	td.style8 { vertical-align:bottom; border-bottom:none #000000; border-top:1px solid #000000 !important; border-left:1px solid #000000 !important; border-right:2px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style8 { vertical-align:bottom; border-bottom:none #000000; border-top:1px solid #000000 !important; border-left:1px solid #000000 !important; border-right:2px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	td.style9 { vertical-align:top; text-align:left; padding-left:0px; border-bottom:1px solid #000000 !important; border-top:none #000000; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style9 { vertical-align:top; text-align:left; padding-left:0px; border-bottom:1px solid #000000 !important; border-top:none #000000; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	td.style10 { vertical-align:top; text-align:left; padding-left:0px; border-bottom:1px solid #000000 !important; border-top:1px solid #000000 !important; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style10 { vertical-align:top; text-align:left; padding-left:0px; border-bottom:1px solid #000000 !important; border-top:1px solid #000000 !important; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	td.style11 { vertical-align:top; text-align:left; padding-left:0px; border-bottom:none #000000; border-top:1px solid #000000 !important; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style11 { vertical-align:top; text-align:left; padding-left:0px; border-bottom:none #000000; border-top:1px solid #000000 !important; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	td.style12 { vertical-align:middle; text-align:left; padding-left:0px; border-bottom:none #000000; border-top:1px solid #000000 !important; border-left:1px solid #000000 !important; border-right:2px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style12 { vertical-align:middle; text-align:left; padding-left:0px; border-bottom:none #000000; border-top:1px solid #000000 !important; border-left:1px solid #000000 !important; border-right:2px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	td.style13 { vertical-align:middle; text-align:left; padding-left:0px; border-bottom:2px solid #000000 !important; border-top:1px solid #000000 !important; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style13 { vertical-align:middle; text-align:left; padding-left:0px; border-bottom:2px solid #000000 !important; border-top:1px solid #000000 !important; border-left:2px solid #000000 !important; border-right:1px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	td.style14 { vertical-align:middle; text-align:left; padding-left:0px; border-bottom:2px solid #000000 !important; border-top:1px solid #000000 !important; border-left:1px solid #000000 !important; border-right:2px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style14 { vertical-align:middle; text-align:left; padding-left:0px; border-bottom:2px solid #000000 !important; border-top:1px solid #000000 !important; border-left:1px solid #000000 !important; border-right:2px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	td.style15 { vertical-align:middle; border-bottom:1px solid #000000 !important; border-top:1px solid #000000 !important; border-left:1px solid #000000 !important; border-right:2px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	th.style15 { vertical-align:middle; border-bottom:1px solid #000000 !important; border-top:1px solid #000000 !important; border-left:1px solid #000000 !important; border-right:2px solid #000000 !important; color:#000000; font-family:'Calibri'; font-size:11pt; background-color:white }
	table.sheet0 col.col0 { width:125.38888745pt }
	table.sheet0 col.col1 { width:361.93332918pt }
	table.sheet0 tr { height:15pt }
	table.sheet0 tr.row0 { height:15pt }
	table.sheet0 tr.row4 { height:15pt }
	table.sheet0 tr.row5 { height:15pt }
	table.sheet0 tr.row28 { height:15pt }
	table.sheet0 tr.row29 { height:15pt }
	table.sheet0 tr.row32 { height:15pt }

  </style>
</head>
<body>
  <table width="100%">
    <tr>
        <td valign="top"><img src="{{ url('adminAssets/image/logo-astech.png') }}" alt="" width="380"/></td>
        <td width="100%" align="left">
            <p style="font-size:11pt;">Wisma SSK, Lt.3 ASTech</p>
			<p>Jl. Daan Mogot KM.11 Cengkareng, Jakarta Barat</p>
			<p>Telp : (021) 5515263 / 0822 1166 1450 (Whatsapp Only)</p>

        </td>
    </tr>
  </table>
  <br/>
  <hr>
  <br/>
  <br/>

<div style="margin: 0px 50px 0px 50px;">
	<table border="0" cellpadding="0" cellspacing="0" id="sheet0" class="sheet0 gridlines">
        <tbody>
          <tr class="row0">
            <td class="column0 style1 s style2 s" colspan="2" > DATA CLIENT </td>
          </tr>
          <tr class="row1">
            <td class="column0 style3 s"> NAME OF COMPANY / PIC </td>
            <td class="column1 style4 null"> {{ $order->name_customer }} </td>
          </tr>
          <tr class="row2">
            <td class="column0 style5 s"> ADDRESS </td>
            <td class="column1 style6 null"> {{ $order->address }} </td>
          </tr>
          <tr class="row3">
            <td class="column0 style5 s"> PHONE </td>
            <td class="column1 style6 null"> {{ $order->phone_number }} </td>
          </tr>
          <tr class="row4">
            <td class="column0 style7 null"></td>
            <td class="column1 style8 null"></td>
          </tr>
          <tr class="row5">
            <td class="column0 style1 s style2 s" colspan="2">UNIT INFORMATION</td>
          </tr>
          <tr class="row6">
            <td class="column0 style9 s style10" rowspan="5">PRODUCT</td>
            <td class="column1 style4 null">&nbsp;</td>
          </tr>
          <tr class="row7">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row8">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row9">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row10">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row11">
            <td class="column0 style10 s style10" rowspan="5">MODEL</td>
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row12">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row13">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row14">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row15">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row16">
            <td class="column0 style10 s style11" rowspan="13">UNIT INFORMATION</td>
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row17">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row18">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row19">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row20">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row21">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row22">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row23">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row24">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row25">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row26">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row27">
            <td class="column1 style6 null">&nbsp;</td>
          </tr>
          <tr class="row28">
            <td class="column1 style12 null">&nbsp;</td>
          </tr>
          <tr class="row29">
            <td class="column0 style1 s style2 s" colspan="2">TERMS &amp; CONDITION</td>
          </tr>
          <tr class="row30">
            <td class="column0 style3 s">VALIDITY</td>
            <td class="column1 style4 s">15 Days</td>
          </tr>
          <tr class="row31">
            <td class="column0 style5 s">STATUS WARANTY</td>
            <td class="column1 style15 s">15 HARI KERJA SETELAH PENGERJAAN DENGAN SPAREPART YANG SAMA</td>
          </tr>
          <tr class="row32">
            <td class="column0 style13 s">STATUS REPORT</td>
            <td class="column1 style14 s">MERUPAKAN BUKTI SAH DAN DI SETUJUI KEDUA BELAH PIHAK</td>
          </tr>
        </tbody>
    </table>
    <br/>
    <br/>
    <div class="column">
        <p style="font-size:11pt; font-weight:bold;">Hormat Kami</p>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <p><b>_____________________________</b></p>
        <p style="font-size:11pt; margin-top:5px; font-weight:bold;">Tanggal : </p>
    </div>
    <div class="column" style="float:right;text-align:right;">
        <p style="font-size:11pt; font-weight:bold;text-align:center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Penerima</p>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <p><b>_____________________________</b></p>
    </div>
</div>


</body>
</html>
