@extends('admin.home')
@section('content')
<style>
    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 100%;
}
</style>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
               <button class="btn btn-primary btn-sm add-services">Back</button>
            </div>
        </div>
        <input type="hidden" name="id" value="{{ $getData->id }}">
        <input type="hidden" name="technicians_id" id="technicians_id" value="{{ $technicians_id }}">
        <div class="card-body">
            <div class="position-relative row form-group" >
                <label for="exampleEmail" class="col-sm-3 col-form-label" >Service Name</label>
                <div class="col-sm-9">
                    <select class="js-example-basic-single" id="ms_services_id" name="ms_services_id" style="width:100%">
                        <option value="{{ $getData->ms_services_id }}" >{{ $getData->service->name }}</option>
                    </select>
                </div>
            </div>

            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Name Sparepart</label>
                <div class="col-sm-9">
                    <input name="name" value="{{ $getData->name }}" placeholder="Services Name" type="text" class="form-control">
                </div>
            </div>

            <div class="position-relative row form-group" >
                <label for="exampleEmail" class="col-sm-3 col-form-label">Price</label>
                <div class="col-sm-9">
                    <input name="price" id="price" value="{{ $getData->price }}" placeholder="Price" type="text" class="form-control">
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Description</label>
                <div class="col-sm-9">
                    <textarea name="description" id="description" class="form-control">{{ $getData->description }}</textarea>
                </div>
            </div>

            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-3 col-form-label">Images</label>
                <div class="col-sm-6">
                    <input name="images" placeholder="Image" type="file" class="form-control" id="imgInp">
                </div>
                <div class="col-sm-3">
                    @if($getData->images == "" )
                        <img id="img-upload" src="http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png" alt="your image" style="width:100px;height:100px"/>
                    @else
                        <img id='img-upload' src="{{asset('/storage/services/' . $getData->images)}}">
                    @endif

                </div>
            </div>
            <div class="position-relative row form-check">
                <div class="col-sm-10 offset-sm-2">
                    <button class="btn btn-success" id="save" style="float:right"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                </div>
            </div><br>
        </div>
    </div>
</div>
@endsection

@section('script')

<script>
    Helper.currency('#price');
    globalCRUD
        .select2("#ms_services_id", '/services/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })
    $("#save").click(function() {
        Helper.unMask('#price')
        id = $('input[name="id"]').val();
        var fd = new FormData();
        var files = $('#imgInp')[0].files[0];
        if(files){
            fd.append('images', files);
        }
        fd.append('ms_services_id', $('#ms_services_id').val());
        fd.append('name', $('input[name="name"]').val());
        fd.append('price', $('input[name="price"]').val());
        fd.append('description', $('#description').val());
        fd.append('technicians_id', $('#technicians_id').val());

        $.ajax({
            url:Helper.apiUrl('/technician_sparepart/'+id+''),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Technician Sparepart Has Been Saved',
                    });
                    window.location.href = Helper.redirectUrl('/teknisi/technician_sparepart/show');
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });

                    })
                }

            },
        });
    });
</script>


<script>
    $(document).ready( function() {
    	$(document).on('change', '.btn-file :file', function() {
		var input = $(this),
			label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		input.trigger('fileselect', [label]);
		});

		$('.btn-file :file').on('fileselect', function(event, label) {

		    var input = $(this).parents('.input-group').find(':text'),
		        log = label;

		    if( input.length ) {
		        input.val(log);
		    } else {
		        if( log ) alert(log);
		    }

		});
		function readURL(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();

		        reader.onload = function (e) {
		            $('#img-upload').attr('src', e.target.result);
		        }

		        reader.readAsDataURL(input.files[0]);
		    }
		}

		$("#imgInp").change(function(){
		    readURL(this);
		});
	});
</script>

@endsection
