@extends('admin.home')
@section('content')


<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                DETAIL TRANSACTION ORDER LIST
            </div>
            <div class="btn-actions-pane-right text-capitalize"></div>
        </div>
        <input type="hidden" value="{{$pendingTransfer->id}}" name="user_id" id="user_id">
        <div class="card-body">
            <div class="tab-pane show active" id="tab-1" role="tabpanel">
                <div class="row">
                </div>
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Pending Transfer</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Success Transfer</a>
                        <a class="nav-item nav-link" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab" aria-controls="nav-all" aria-selected="false">All Transfer</a>
                        <a class="nav-item nav-link" id="nav-other-tab" data-toggle="tab" href="#nav-other" role="tab" aria-controls="nav-other" aria-selected="false">other orders</a>
                    </div>
                </nav>
                <br>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="container">
                            <table class="display table table-hover table-bordered" id="sort_table">
                                    <tr>
                                        <th></th>
                                        <th>Order Code</th>
                                        <th>Date</th>
                                        <th>Price Services</th>
                                        <th>Cancellation Fee</th>
                                        <th>Total Spareparts</th>
                                        <th>Total Deductions</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                <tbody>
                                    @php
                                        $sumAllTotal = 0;
                                    @endphp
                                    @foreach ($pendingTransfer->pendingOrder as $key => $total)
                                    
                                    @php
                                        $sparepartDetail = 0; 
                                        $data_id = $total->order->id;
                                        if(!empty($total->price_services)){
                                            $total_harga = $total->price_services->commission_deduction * $total->unit;
                                            $komisi = $total->price_services->commission_value * $total->unit;
                                        }else{
                                            $total_harga =  $total->unit;
                                            $komisi =  $total->unit;
                                        }
                                        
                                        
                                        if(!empty($total->order->sparepart_detail)){
                                            foreach($total->order->sparepart_detail as $totalSparepart){
                                                $sparepartDetail += $totalSparepart->price * $totalSparepart->quantity;
                                            }
                                        }

                                        if(!empty($total->order->item_detail)){
                                            foreach($total->order->item_detail as $totalSparepart){
                                                $sparepartDetail += $totalSparepart->price * $totalSparepart->quantity;
                                            }
                                        }

                                        if (!empty($total->order->saldo)){
                                            $sumAllTotal += $total->order->saldo->total;
                                        }
                                    @endphp
                                    @php
                                        $color = '';
                                        $deduction = 0;
                                        if ($total->order->orders_statuses_id == 5) {
                                            $color = 'danger';
                                            if($total->order->penalty_order_type == 1){
                                                $getTotalPenalti = $total->order->penalty_order_value / 100 * $total->price;
                                                $deduction = $total->order->commission_penalty / 100 * $getTotalPenalti;
                                                if(!empty($total->order->penalty_order_value)){
                                                    $getCanceledFee = $total->order->penalty_order_value . '%';
                                                }else{
                                                    $getCanceledFee = 0;
                                                }
                                            }else{
                                                if($total->order->type_commission_penalty == 1){
                                                    $deduction = $total->order->commission_penalty / 100 * $total->order->penalty_order_value;
                                                }else{
                                                    $deduction = $total->order->commission_penalty;
                                                }
                                                $getCanceledFee = 'Rp.' . number_format($total->order->penalty_order_value);
                                            }
                                        }elseif ($total->order->orders_statuses_id == 10) {
                                            $color = 'success';
                                            $deduction = $total->order->total_commission;
                                            $getCanceledFee = '-';
                                        }
                                    @endphp
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="update_checkbox[]" class="check-order" data-id="{{ $total->order->id }}" id="customCheck1-{{ $total->id }}">
                                                <span class="checkmark" for="customCheck1-{{ $total->id }}"></span>
                                            </td>

                                            <td>
                                                <a href="{{ url('/admin/order/service_detail/'.$total->order->id) }}" style="text-decoration: underline"># {{ $total->order->code }}</a> <br> 
                                                @if(!empty($total->price_services))
                                                    <small>{{ $total->price_services->service_type->name }}</small>
                                                @endif
                                            </td>

                                            <td>
                                                <small align:center>
                                                    <span class="badge badge-{{ $color }}">{{ $total->order->order_status->name }} At </span>
                                                </small> 
                                                <br> {{ date('Y-M-d H:i:s', strtotime($total->order->updated_at)) }}
                                            </td>

                                            <td>
                                                Rp. {{ number_format($total->price) }}<br>
                                                <small align="right"> Qty{{ $total->unit }}</small>
                                            </td>

                                            <td>{{ $getCanceledFee }}</td>
                                            
                                            @if(empty($total->order->sparepart_detail))
                                                <td>Rp. 0.00</td>
                                            @else
                                                @if ($total->order->orders_statuses_id == 10)
                                                    <td>Rp. {{ number_format($sparepartDetail) }}</td>
                                                @else
                                                    <td>Rp. 0</td>
                                                @endif
                                            @endif

                                            <td>Rp. {{ number_format($deduction) }}</td>

                                            @if (!empty($total->order->saldo))
                                                @if(!empty($total->order->sparepart_detail))
                                                    <td>Rp. {{ number_format($total->order->saldo->total) }} </td>
                                                @endif
                                            @else
                                                <td>Rp. 0 </td>
                                            @endif

                                            <td>
                                                <a href="" type="submit" id="successTransfer" data-id="{{ $pendingTransfer->id }}" data-update-id="{{ $total->order->id }}" class="badge badge-primary" title="Success Transfer"><i class="fa fa-check"></i></a> 
                                                <input type="hidden" name="ids" id="ids" value="{{ $total->order->id }}">
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="7" align="right">Bank account</td>
                                        <td colspan="2">{{ $pendingTransfer->rekening_number }} - ({{ ($pendingTransfer->rekening_name) }})</td>
                                    </tr>
                                    <tr>
                                        <td colspan="7" align="right">Total</td>
                                        <td colspan="2">Rp. {{ number_format($sumAllTotal) }}</td>
                                    </tr>
                                </tbody>  
                            </table>
                            @if ($pendingTransfer->pendingOrder->count() !== 0)
                                <button class="btn btn-success btn-sm updatestatus btn_succes_transfer" data-id="" style="float:right">Success Transfer</button>
                            @endif
                        </div>
                    </div>

                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="container">
                            <table class="display table table-hover table-bordered">
                                    <tr>
                                        <th>Order Code</th>
                                        <th>Date</th>
                                        <th>Price Services</th>
                                        <th>Cancellation Fee</th>
                                        <th>Total Spareparts</th>
                                        <th>Total Deductions</th>
                                        <th>Total</th>
                                    </tr>
                                <tbody>
                                    @php
                                        $sumAllTotal = 0;
                                    @endphp
                                    @foreach ($succcessTransfer->doneOrder as $key => $pending)
                                    
                                    @php
                                        $sparepartDetail = 0; 
                                        $data_id = $pending->order->id;
                                        if(!empty($pending->price_services)){
                                            $total_harga = $pending->price_services->commission_deduction * $pending->unit;
                                            $komisi = $pending->price_services->commission_value * $pending->unit;
                                            }else{
                                                $total_harga =  $pending->unit;
                                                $komisi =  $pending->unit;
                                            }
                                        
                                        if(!empty($pending->order->sparepart_detail)){
                                            foreach($pending->order->sparepart_detail as $totalSparepart){
                                                $sparepartDetail += $totalSparepart->price * $totalSparepart->quantity;
                                            }
                                        }

                                        if(!empty($pending->order->item_detail)){
                                            foreach($pending->order->item_detail as $totalSparepart){
                                                $sparepartDetail += $totalSparepart->price * $totalSparepart->quantity;
                                            }
                                        }

                                        if (!empty($pending->order->saldo)){
                                            $sumAllTotal += $pending->order->saldo->total + $sparepartDetail;
                                        }
                                    @endphp
                                    @php
                                        $color = '';
                                        $deduction = 0;
                                        if ($pending->order->orders_statuses_id == 5) {
                                            $color = 'danger';
                                            if($pending->order->penalty_order_type == 1){
                                                $getTotalPenalti = $pending->order->penalty_order_value / 100 * $pending->price;
                                                $deduction = $pending->order->commission_penalty / 100 * $getTotalPenalti;
                                                if(!empty($pending->order->penalty_order_value)){
                                                    $getCanceledFee = $pending->order->penalty_order_value . '%';
                                                }else{
                                                    $getCanceledFee = 0;
                                                }
                                            }else{
                                                if($pending->order->type_commission_penalty == 1){
                                                    $deduction = $pending->order->commission_penalty / 100 * $pending->order->penalty_order_value;
                                                }else{
                                                    $deduction = $pending->order->commission_penalty;
                                                }
                                                $getCanceledFee = 'Rp.' . number_format($pending->order->penalty_order_value);
                                            }
                                        }elseif ($pending->order->orders_statuses_id == 10) {
                                            $color = 'success';
                                            $deduction = $pending->order->total_commission;
                                            $getCanceledFee = '-';
                                        }
                                    @endphp
                                        <tr>
                                            
                                            <td>
                                                <a href="{{ url('/admin/order/service_detail/'.$pending->order->id) }}" style="text-decoration: underline"># {{ $pending->order->code }}</a> <br> 
                                                <small>{{ !empty($pending->price_services) ? $pending->price_services->service_type->name : '' }}</small>
                                            </td>
                                            <td><small align:center><span class="badge badge-{{ $color }}">{{ $pending->order->order_status->name }} At  </span></small> <br> {{ date('Y-M-d H:i:s', strtotime($pending->order->updated_at)) }}</td>
                                            <td>Rp. {{ number_format($pending->price) }}<br><small align="right"> Qty{{ $pending->unit }}</small></td>
                                            <td>{{ $getCanceledFee }}</td>
                                            @if(empty($pending->order->sparepart_detail))
                                                <td>Rp. 0.00</td>
                                            @else
                                                @if ($pending->order->orders_statuses_id == 10)
                                                    <td>Rp. {{ number_format($sparepartDetail) }}</td>
                                                @else
                                                    <td>Rp. 0</td>
                                                @endif
                                            @endif
                                            <td>Rp. {{ number_format($deduction) }}</td>

                                            @if (!empty($pending->order->saldo))
                                                @if(!empty($pending->order->sparepart_detail))
                                                    <td>Rp. {{ number_format($pending->order->saldo->total + $sparepartDetail) }} </td>
                                                @else
                                                    <td>Rp. {{ number_format($pending->order->saldo->total) }} </td>
                                                @endif
                                            @else
                                                <td>Rp. {{$pending->order->after_commission}}</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="6" align="right">Bank account</td>
                                        <td colspan="2">{{ $succcessTransfer->rekening_number }} - ({{ ($succcessTransfer->rekening_name) }})</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" align="right">Total</td>
                                        <td colspan="2">Rp. {{ number_format($sumAllTotal) }}</td>
                                    </tr>
                                </tbody>  
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="nav-all" role="tabpanel" aria-labelledby="nav-all-tab">
                        <div class="container">
                                <table class="display table table-hover table-bordered" id="getAllPendingTransfer">
                                        <tr>
                                            <th>Order Code</th>
                                            <th>Date</th>
                                            <th>Price Services</th>
                                            <th>Cancellation Fee</th>
                                            <th>Total Spareparts</th>
                                            <th>Total Deductions</th>
                                            <th>Total</th>
                                        </tr>
                                    <tbody>
                                        @php
                                            $sumAllTotal = 0;
                                        @endphp
                                        @foreach ($getAllOrders->allOrder as $key => $getAllOrder)
                                        
                                        @php
                                            $sparepartDetail = 0; 
                                            if(!empty($getAllOrder->order)){
                                                $data_id = $getAllOrder->order->id;
                                            }
                                            if(!empty($getAllOrder->price_services)){
                                                $total_harga = $getAllOrder->price_services->commission_deduction * $getAllOrder->unit;
                                                $komisi = $getAllOrder->price_services->commission_value * $getAllOrder->unit;
                                            }else{
                                                $total_harga =  $getAllOrder->unit;
                                                $komisi =  $getAllOrder->unit;
                                            }
                                            
                                            if(!empty($getAllOrder->order->sparepart_detail)){
                                                foreach($getAllOrder->order->sparepart_detail as $totalSparepart){
                                                    $sparepartDetail += $totalSparepart->price * $totalSparepart->quantity;
                                                }
                                            }
    
                                            if(!empty($getAllOrder->order->item_detail)){
                                                foreach($getAllOrder->order->item_detail as $totalSparepart){
                                                    $sparepartDetail += $totalSparepart->price * $totalSparepart->quantity;
                                                }
                                            }
    
                                            if (!empty($getAllOrder->order->saldo)){
                                                $sumAllTotal += $getAllOrder->order->saldo->total + $sparepartDetail;
                                            }
                                        @endphp
                                        @php
                                            $color = '';
                                            $deduction = 0;
                                            if(!empty($getAllOrder->order)){
                                                $cek_id_status = $getAllOrder->order->orders_statuses_id;
                                            }
                                            if ($cek_id_status == 5) {
                                                $color = 'danger';
                                                if($getAllOrder->order->penalty_order_type == 1){
                                                    $getTotalPenalti = $getAllOrder->order->penalty_order_value / 100 * $getAllOrder->price;
                                                    $deduction = $getAllOrder->order->commission_penalty / 100 * $getTotalPenalti;
                                                    if(!empty($getAllOrder->order->penalty_order_value)){
                                                        $getCanceledFee = $getAllOrder->order->penalty_order_value . '%';
                                                    }else{
                                                        $getCanceledFee = 0;
                                                    }
                                                }else{
                                                    if($getAllOrder->order->type_commission_penalty == 1){
                                                        $deduction = $getAllOrder->order->commission_penalty / 100 * $getAllOrder->order->penalty_order_value;
                                                    }else{
                                                        $deduction = $getAllOrder->order->commission_penalty;
                                                    }
                                                    $getCanceledFee = 'Rp.' . number_format($getAllOrder->order->penalty_order_value);
                                                }
                                            }elseif ($cek_id_status == 10) {
                                                $color = 'success';
                                                if(!empty($getAllOrder->order)){
                                                    $deduction = $getAllOrder->order->total_commission;
                                                }
                                                $getCanceledFee = '-';
                                            }
                                        @endphp
                                            @if(!empty($getAllOrder->order))
                                                <tr>
                                                    <td>
                                                        <a href="{{ url('/admin/order/service_detail/'.$getAllOrder->order->id) }}" style="text-decoration: underline"># {{ $getAllOrder->order->code }}</a>
                                                        <br> <small>{{ !empty($getAllOrder->price_services) ?  $getAllOrder->price_services->service_type->name : '-' }}</small></td>
                                                    <td><small align:center><span class="badge badge-{{ $color }}">{{ $getAllOrder->order->order_status->name }} At  </span></small> <br> {{ date('Y-M-d H:i:s', strtotime($getAllOrder->order->updated_at)) }}</td>
                                                    <td>Rp. {{ number_format($getAllOrder->price) }}<br><small align="right"> Qty{{ $getAllOrder->unit }}</small></td>
                                                    <td>{{ $getCanceledFee }}</td>
                                                    @if(empty($getAllOrder->order->sparepart_detail))
                                                        <td>Rp. 0.00</td>
                                                    @else
                                                        @if ($getAllOrder->order->orders_statuses_id == 10)
                                                            <td>Rp. {{ number_format($sparepartDetail) }}</td>
                                                        @else
                                                            <td>Rp. 0</td>
                                                        @endif
                                                    @endif
                                                    <td>Rp. {{ number_format($deduction) }}</td>
        
                                                    @if (!empty($getAllOrder->order->saldo))
                                                        @if(!empty($getAllOrder->order->sparepart_detail))
                                                            <td>Rp. {{ number_format($getAllOrder->order->saldo->total + $sparepartDetail) }} </td>
                                                        @else
                                                            <td>Rp. {{ number_format($getAllOrder->order->saldo->total) }} </td>
                                                        @endif
                                                    @else
                                                        <td>Rp. {{$getAllOrder->order->after_commission}}</td>
                                                    @endif
                                                </tr>
                                            @endif
                                        @endforeach
                                        <tr>
                                            <td colspan="6" align="right">Bank account</td>
                                            <td colspan="2">{{ $succcessTransfer->rekening_number }} - ({{ ($succcessTransfer->rekening_name) }})</td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" align="right">Total</td>
                                            <td colspan="2">Rp. {{ number_format($sumAllTotal) }}</td>
                                        </tr>
                                    </tbody>  
                                </table>
                            <br>
                        </div>
                    </div>
                    
                    <div class="tab-pane fade " id="nav-other" role="tabpanel" aria-labelledby="nav-other-tab">
                        <div class="container">
                            
                            <table id="table-list-orders" class="display table table-hover table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Transfer Date</th>
                                        <th>Status</th>
                                        <th>Total Transfer</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($getOtherOrders as $key => $val) 
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="update_checkbox[]" class="check-order" data-id="{{ $val->order_id }}" id="customCheck1-{{ $val->id }}">
                                                <span class="checkmark" for="customCheck1-{{ $val->order_id }}"></span>
                                            </td>
                                            <td>{{ date('Y-M-d H:i:s', strtotime($val->created_at)) }}</td>
                                            <td>
                                                @if($val->transfer_status == 1)
                                                     <span class="badge badge-success">Transfer Success</span> 
                                                @else
                                                    <span class="badge badge-warning">Pending</span>
                                                @endif
                                            </td>
                                            <td>Rp. {{ number_format($val->total) }}</td>
                                            <td>
                                                <a href="" type="submit" id="balance_trnsfer" data-id="{{ $val->technician_id }}" data-update-ids="{{ $val->order_id }}" class="badge badge-primary" title="Success Transfer"><i class="fa fa-check"></i></a> 
                                                <input type="hidden" name="ids" id="ids" value="{{ $val->id  }}">
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @if ($pendingTransfer->pendingOrder->count() !== 0)
                                <button class="btn btn-success btn-sm updatestatus_balance btn_succes_transfer_balance" data-id="" style="float:right">Success Transfers</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
{{-- change status transfer --}}
<script>
    $(document).ready(function(){
        $('.btn_succes_transfer').hide();
        $('.btn_succes_transfer_balance').hide();
    });
    $(document).on('change', '.check-order', function(e) {
        var id_order = $('.check-order:checked').map(function(){
            return $(this).attr('data-id');
        }).get();
        if (id_order.length > 0) {
            $('.btn_succes_transfer').show();
            $('.btn_succes_transfer_balance').show();
        }else{
            $('.btn_succes_transfer').hide();
            $('.btn_succes_transfer_balance').hide();
        }
        console.log(id_order);
    })

    $(document).on('click', '.updatestatus', function(e) {
        id = $(this).attr('data-id');
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/admin/update/status-transfer'),
                data:{
                    ids : $('.check-order:checked').map(function(){
                                return $(this).attr('data-id');
                            }).get()
                },
                type: 'post',

                success: function(res) {
                    Helper.loadingStop();
                    Helper.redirectTo('/admin/transaction-success/show');
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })
    
    $(document).on('click', '.updatestatus_balance', function(e) {
        id = $(this).attr('data-id');
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/admin/update/status-transfer'),
                data:{
                    ids : $('.check-order:checked').map(function(){
                                return $(this).attr('data-id');
                            }).get()
                },
                type: 'post',

                success: function(res) {
                    Helper.loadingStop();
                    Helper.redirectTo('/admin/transaction-success/show');
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })

    $(document).on('click', '#successTransfer', function(e) {
        idss = $(this).attr('data-update-id');
        id = $(this).attr('data-id');
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/admin/update/transfer/' + idss),
                type: 'post',
                data:{
                    ids : $('input[name="ids"]').val()
                },
                type: 'post',

                success: function(res) {
                    Helper.loadingStop();
                    Helper.redirectTo('/admin/transaction-success/detail/'+id);
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })
    
    $(document).on('click', '#balance_trnsfer', function(e) {
        idss = $(this).attr('data-update-ids');
        id = $(this).attr('data-id');
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/admin/update/transfer/balance/' + idss),
                type: 'post',
                data:{
                    ids : $('input[name="ids"]').val()
                },
                type: 'post',

                success: function(res) {
                    Helper.loadingStop();
                    Helper.redirectTo('/admin/transaction-success/detail/'+id);
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })
</script>

<script>
    $(document).ready( function () {
        $('#getAllPendingTransfer').DataTable();
    } );
</script>



{{-- show all transaction --}}
<script>
    var id = $('#user_id').val()
    
    function clickButton(){
        $('#myButton').trigger('click', function(e) {
            var type = [];
            $.each($("#type option:selected"), function(){            
                type.push($(this).val());
            });
            if(type != ''){
                init(type);
            }else{
                init($(this).val());
            }
        });
    }
</script>

@endsection
