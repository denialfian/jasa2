<div class="card">
    <div class="card-header header-border">
        Billing Amount
    </div>
    <div class="card-body bill-amount-area">
        <ul class="list-group">
            <li class="list-group-item">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left" style="width: 50%;">
                            <div class="widget-heading text-primary">Services {{ $order->service_name }}</div>
                        </div>
                    </div>
                </div>
            </li>
            @foreach($order->service_detail as $key => $detail)
            <li class="list-group-item">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left" style="width: 50%;">
                            <div class="widget-heading">{{ $detail->symptom_name }}</div>
                            <div class="widget-subheading">{{ $detail->service_type_name }} {{ $order->product_group_name }}</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers"><span> Rp. {{ number_format($detail->price) }}</span></div>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
            <li class="list-group-item">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left" style="width: 50%;">
                            <div class="widget-heading text-primary">Sparepart</div>
                        </div>
                    </div>
                </div>
            </li>
            @foreach($sparepart_details as $sparepart_detail)
            <li class="list-group-item">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left" style="width: 50%;">
                            <div class="widget-heading">{{ $sparepart_detail->name_sparepart }}</div>
                            <div class="widget-subheading">{{ $sparepart_detail->priceThousandSeparator() }} x {{ $sparepart_detail->quantity }}</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers"><span> Rp. {{ \App\Helpers\thousanSparator($sparepart_detail->quantity * $sparepart_detail->price) }}</span></div>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach
            @foreach($item_details as $item)
            <li class="list-group-item">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left" style="width: 50%;">
                            <div class="widget-heading">{{ $item->name_product }}</div>
                            <div class="widget-subheading">{{ $item->price }} x {{ $item->quantity }}</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers"><span> Rp. {{ \App\Helpers\thousanSparator($item->quantity * $item->price) }}</span></div>
                        </div>
                    </div>
                </div>
            </li>
            @endforeach

            <li class="list-group-item">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left" style="width: 50%;">
                            <div class="widget-heading text-primary">Grand Total</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers"><span id="grand_total_order"> Rp. {{ \App\Helpers\thousanSparator($order->grand_total) }}</span></div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>