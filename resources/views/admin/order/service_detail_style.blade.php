<style>
    .custom-control{
        margin: 10px;
    }
    .custom-control-label-2{
        margin: auto;
    }
    .modal-header{
        background-color: #3f9688;
    }
    .modal-content{
        border: none;
    }
    .gallery {
        display: flex;
        padding: 2px;
        transition: 0.3s;
    }

    .gallery:hover .gallery__image {
        filter: grayscale(1);
    }

    .gallery__column {
        display: flex;
        flex-direction: column;
        width: 25%;
    }

    .gallery__link {
        margin: 2px;
        overflow: hidden;
    }

    .gallery__link:hover .gallery__image {
        filter: grayscale(0);
    }

    .gallery__link:hover .gallery__caption {
        opacity: 1;
    }

    .gallery__thumb {
        position: relative;
    }

    .gallery__image {
        display: block;
        width: 100%;
        transition: 0.3s;
    }

    .gallery__image:hover {
        transform: scale(1.1);
    }

    .gallery__caption {
        position: absolute;
        bottom: 0;
        left: 0;
        padding: 25px 15px 15px;
        width: 100%;
        font-family: "Raleway", sans-serif;
        font-size: 16px;
        color: white;
        opacity: 0;
        background: linear-gradient(0deg, rgba(0, 0, 0, 0.5) 0%, rgba(255, 255, 255, 0) 100%);
        transition: 0.3s;
    }
</style>
<style>
    .widget-content .widget-numbers {
        font-size: 1em;
    }

    .select2-result-repository {
        padding-top: 4px;
        padding-bottom: 3px;
    }

    .select2-result-repository__avatar {
        float: left;
        width: 60px;
        height: auto;
        margin-right: 10px;
    }

    .select2-result-repository__avatar img {
        width: 100%;
        height: auto;
        border-radius: 50%;
    }

    .select2-result-repository__meta {
        /* margin-left: 70px;0 */
        margin-left: 0px;
    }

    .select2-result-repository__title {
        color: black;
        font-weight: bold;
        word-wrap: break-word;
        line-height: 1.1;
        margin-bottom: 4px;
    }

    .select2-result-repository__price,
    .select2-result-repository__stargazers {
        margin-right: 1em;
    }

    .select2-result-repository__price,
    .select2-result-repository__stargazers,
    .select2-result-repository__watchers {
        display: inline-block;
        color: rgb(68, 68, 68);
        font-size: 13px;
    }

    .select2-result-repository__description {
        font-size: 13px;
        color: #777;
        margin-top: 4px;
    }

    .select2-results__option--highlighted .select2-result-repository__title {
        color: white;
    }

    .select2-results__option--highlighted .select2-result-repository__price,
    .select2-results__option--highlighted .select2-result-repository__stargazers,
    .select2-results__option--highlighted .select2-result-repository__description,
    .select2-results__option--highlighted .select2-result-repository__watchers {
        color: #c6dcef;
    }
</style>

<style>
    .our-team {
        /* border-top: 2px solid #444054; */
        box-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);
        padding: 30px 0 40px;
        margin-bottom: 30px;
        background-color: #fff;
        text-align: center;
        overflow: hidden;
        position: relative;
    }

    .our-team .picture {
        display: inline-block;
        height: 130px;
        width: 130px;
        margin-bottom: 50px;
        z-index: 1;
        position: relative;
    }

    .our-team .picture::before {
        content: "";
        width: 100%;
        height: 0;
        border-radius: 50%;
        background-color: #444054;
        position: absolute;
        bottom: 135%;
        right: 0;
        left: 0;
        opacity: 0.9;
        transform: scale(3);
        transition: all 0.3s linear 0s;
    }

    .our-team:hover .picture::before {
        height: 100%;
    }

    .our-team .picture::after {
        content: "";
        width: 100%;
        height: 100%;
        border-radius: 50%;
        background-color: #444054;
        position: absolute;
        top: 0;
        left: 0;
        z-index: -1;
    }

    .our-team .picture img {
        width: 100%;
        height: auto;
        border-radius: 50%;
        transform: scale(1);
        transition: all 0.9s ease 0s;
    }

    .our-team:hover .picture img {
        box-shadow: 0 0 0 14px #f7f5ec;
        transform: scale(0.7);
    }

    .our-team .title {
        display: block;
        font-size: 15px;
        color: #4e5052;
        text-transform: capitalize;
    }

    .our-team .social {
        width: 100%;
        padding: 0;
        margin: 0;
        background-color: #444054;
        position: absolute;
        bottom: -100px;
        left: 0;
        transition: all 0.5s ease 0s;
    }

    .our-team:hover .social {
        bottom: 0;
    }

    .our-team .social li {
        display: inline-block;
    }

    .our-team .social li a {
        display: block;
        padding: 10px;
        font-size: 17px;
        color: white;
        transition: all 0.3s ease 0s;
        text-decoration: none;
    }

    .our-team .social li a:hover {
        color: #444054;
        background-color: #f7f5ec;
    }
</style>