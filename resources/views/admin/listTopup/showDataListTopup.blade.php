@extends('admin.home')
@section('content')

<div class="col-md-12" style="margin-top: 20px">

    <div class="card">
        <div class="card-header">
                WALLET TOP UP
            <div class="btn-actions-pane-right">
                {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    TOP UP
                </button> --}}
            </div>
        </div>
        
        <div class="card-body">
            
            
            <table id="table-list-orders" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Amount</th>
                        <th>Payment</th>
                        <th>Cashback / Discount</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($detailTopupData as $item)
                        <tr>
                            <!-- amount -->
                            <td>Rp.{{ number_format($item->nominal) }}</td> 
                            <!-- payment -->
                            @if($item->payment_metod_id == 3)
                                <td>{{ $item->payment_metod_type }} <br> {{$item->bank->bank_name}} {{ $item->bank->name }}</td>
                            @else
                                <td>{{ $item->payment_metod_type }}</td>
                            @endif
                            <!-- kupon -->
                            @if($item->voucer != null)
                                @if($item->voucher_id != null)
                                    @if($item->voucher_type == 1)
                                        <td>Rp {{ $item->number_of_pieces }} </td>
                                    @elseif($item->voucher_type == 2)
                                        <td>Rp {{$item->number_of_pieces}} </td>
                                    @endif
                                @else
                                    <td> - </td>
                                @endif
                            @else
                                <td> - </td>
                            @endif
                            <!-- tanggal -->
                            <td>{{ date('d-M-Y H:i', strtotime($item->created_at)) }}</td>
                            <!-- status -->
                            @if($item->status == 'unpaid')
                                <td>
                                    <span class="badge badge-warning btn-sm " style="color:white"><i class="fa fa-spinner"></i> Pending</span>
                                </td>
                            @elseif($item->status == 'processing')
                                <td>
                                    <span class="badge badge-primary btn-sm " style="color:white"><i class="fa fa-spinner"></i> Processing</span>
                                </td>
                            @elseif($item->status == 'cancel')
                                <td>
                                    <span class="badge badge-danger btn-sm " style="color:white"><i class="fa fa-close"></i> Cancel</span>
                                </td>
                            @elseif($item->status == 'paid')
                                <td>
                                    <span class="badge badge-success btn-sm " style="color:white"><i class="fa fa-check"></i> Success</span>
                                </td>
                            @elseif($item->status == 'confrim')
                                <td>
                                    <span class="badge badge-success btn-sm " style="color:white"><i class="fa fa-check"></i> Success</span>
                                </td>
                            @else
                                <td>-</td>
                            @endif
                            <!-- action -->
                            @if ($item->bank != null)
                                @if($item->transfer_status_id == 0)
                                    <td>
                                        <a href={{ '/admin/topup/detail/'.$item->id }} type='button' class='badge badge-primary btn-sm'  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>&nbsp;
                                        <!-- <button type="submit" class="badge badge-success btn-sm" data-id="{{ $item->id }}" id="approve_topup"><i class='fa fa-check'></i></button></td> -->
                                @endif
                                @if($item->transfer_status_id == '1')
                                    <td>
                                        <a href={{ '/admin/topup/detail/'.$item->id }} type='button' class='badge badge-primary btn-sm'  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>&nbsp;
                                    </td>
                                @endif
                            @else
                                <td>
                                    <a href={{ '/admin/topup/detail/'.$item->id }} type='button' class='badge badge-primary btn-sm'  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>&nbsp;
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody> 
            </table>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Nominal Top up</label>
                        <input type="number" class="form-control" name="nominal" placeholder="10000" id="" />
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Transfer Date</label>
                        <input type="number" class="form-control" name="nominal" placeholder="2019-Mei-01" id="" />
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Attachment</label>
                        <input type="number" class="form-control" name="nominal" placeholder="10000" id="" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary waves-effect" type="submit">
                        Approve
                    </button>
                </div>
            </div>
    </div>
</div>

@endsection


@section('script')
@include('admin.master.batch.batch_js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>
    $(document).on('click', '#approve_topup', function(e) {
        id = $(this).attr('data-id');
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/admin/approved-topup/' + id ),
                type: 'post',

                success: function(res) {
                    Helper.successNotif('This Top up Has Approvd');
                    Helper.loadingStop();
                    Helper.redirectTo('/admin/topup/show-list');
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        },
        {
            title: "Are You Sure",
            message: "<strong>Are you sure </strong></br> have you checked the customer top up data, if sure, press the confirmation button, if you haven't pressed the cancel button and double check",
        })
        
        e.preventDefault()
    })
    $("#get_user").select2({
        ajax: {
            type: "GET",
            url: Helper.apiUrl('/user/select2'),
            data: function(params) {
                return {
                    q: params.term
                };
            },
            processResults: function(data) {
                var res = $.map(data.data, function(item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                });

                return {
                    results: res
                };
            }
        }
    });

    $('.radiogroup').change(function(e){
        var selectedValue = $(this).val();
        $('#amount').val(selectedValue)
    });

    var table = $('#table-list-orders').DataTable({
        processing: true,
        ordering:'true',
        order: [0, 'asc'],
        order: [3, 'desc'],
    });

    $('#form-topup').submit(function(e){
        data = Helper.serializeForm($(this));

        Helper.loadingStart();
         // post data
        Axios.post(Helper.apiUrl('/admin/topup-wallet/save'), data)
            .then(function(response) {
                Helper.successNotif('Success, Top up Success');
                window.location.href = Helper.redirectUrl('/admin/topup/show-list');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

      e.preventDefault();
    })
</script>

@endsection
