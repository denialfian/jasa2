<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.1.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.23.0/firebase-messaging.js"></script>
<script>
    console.log('calling fcm 1')
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyBZ5-RsWEZV_u5N0OqpPInZ9JIOYkpplT0",
        authDomain: "astech-client-mobile.firebaseapp.com",
        databaseURL: "https://astech-client-mobile.firebaseio.com",
        projectId: "astech-client-mobile",
        storageBucket: "astech-client-mobile.appspot.com",
        messagingSenderId: "788557304632",
        appId: "1:788557304632:web:aedbe0fd39c9081f9b67cd",
        measurementId: "G-VXMLM64W3G"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    const messaging = firebase.messaging();

    messaging
        .requestPermission()
        .then(function() {
            return messaging.getToken()
        })
        .then(function(token) {
            $.ajax({
                url: Helper.apiUrl('/chats/firebase_save_token'),
                type: 'POST',
                data: {
                    fcm_token: token
                },
                success: function(response) {
                    console.log(response)
                },
                error: function(err) {
                    console.log(" Can't do because: " + err);
                },
            });
        })
        .catch(function(err) {
            console.log("Unable to get permission to notify.", err);
        });

    messaging.onMessage(function(payload) {
        console.log('onMessage asasas', payload)
        console.log('type', payload.data.type)

        animateCSS('.link-dropdown-noty', 'bounce').then((message) => {
            $('.link-dropdown-noty').removeClass('animate__animated animate__bounce');
        });

        if (payload.data.type == 'chat') {
            console.log('chat incoming')
            $('.count-notif').text(payload.data.count_unread);

            if ($('#content').length) {
                console.log('chat incoming append')
                $('#chat-content[data-chat="' + payload.data.id + '"]').append(payload.data.reply);
                $("#content").animate({
                    scrollTop: $('#content')[0].scrollHeight + 100
                }, 1000);
            }
        }
    });
</script>