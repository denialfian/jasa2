
	<link rel="stylesheet" type="text/css" href="{{ asset('adminAssets/css/util.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('adminAssets/css/login.css') }}">
	<style>
		.btn1 {
			background-color: #26C6DA;
			border: none;
			padding: 10px;
			width: 180px;
			border-radius:8px;
			box-shadow: 1px 5px 20px -5px rgba(0,0,0,0.4);
			color: #fff;
			cursor: pointer;
		}
		.btn2 {
			background-color: #d63031;
			border: none;
			padding: 10px;
			width: 180px;
			border-radius:8px;
			box-shadow: 1px 5px 20px -5px rgba(0,0,0,0.4);
			color: #fff;
			cursor: pointer;
		}
	</style>
	@if ($getDataUser->otp_verified_at == null)
		<div class="wrap-input100 validate-input m-b-36" data-validate = "email is required" style="margin-left: 11%;">
			<form action="{{ route('otp.verification') }}" method="post">
			@csrf
			<input class="input100" id="phone" autocomplete="off" placeholder="X X X X" data-inputmask="'alias': 'phonebe'" name="otp" style="text-align:center;font-size:30px">
			<span class="focus-input100"></span>
		</div>
		<div class="container-login100-form-btn" style="margin-left: 11%;">
			<button type="submit" class="btn1" id="" >Varification</button>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</form>

			<form action="{{ route('otp.resend.verification') }}" method="post">
				@csrf
				<button type="submit" class="btn2" id="" >Resend</button>
			</form>

		</div>

	@else
		<div class="container-login100-form-btn">
			<h3>Hallo <strong>{{ Auth::user()->name }}</strong>,  Your account has been verified. <a href="{{ url('/home') }}">Click here</a>, to continue</h3>
				{{-- <a href="http://"><h3>Hallo <strong>{{ Auth::user()->name }}</strong>, Account Anda sudah Ter verifikasi </h3></a> --}}
		</div>
	@endif


	@include('admin.template._mainScript')
	@include('admin.script.login._otpVerifScript')

	<div id="dropDownSelect1"></div>
	<script src="{{ asset('adminAssets/js/login.js') }}"></script>
