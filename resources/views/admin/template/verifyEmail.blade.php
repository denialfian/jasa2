

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Login - ASTech</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="ArchitectUI HTML Bootstrap 4 Dashboard Template">
    <meta name="msapplication-tap-highlight" content="no">

	<link href="https://demo.dashboardpack.com/architectui-html-pro/main.d810cf0ae7f39f28f336.css" rel="stylesheet"></head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow">
        <div class="app-container">
            <div class="h-100">
                <div class="h-100 no-gutters row">
                    <div class="d-none d-lg-block col-lg-4">
                        <div class="slider-light">
                            <div class="slick-slider">
                                <div>
                                    <div class="position-relative h-100 d-flex justify-content-center align-items-center bg-plum-plate" tabindex="-1">
                                        <div class="slide-img-bg" style="background-image: url('{{ asset('image2.jpg')}}');"></div>
                                        <div class="slider-content">
                                            {{-- <h3>Perfect Balance</h3>
                                            <p>ArchitectUI is like a dream. Some think it's too good to be true! Extensive
                                                collection of unified React Boostrap Components and Elements.
                                            </p> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="h-100 d-flex bg-white justify-content-center align-items-center col-md-12 col-lg-8">
                        <div class="mx-auto app-login-box col-sm-12 col-md-10 col-lg-9">
                            <div class="app-logo" style="background-image: url('{{ asset('astech.png')}}'); width:150px;height:30px"></div>
								<div class="divider row"></div>
                            <div>
                                @if($link == null)
                                    <form action="{{ url('/email/resend') }}" method="post">
                                        @csrf
                                        <h4 align="center">
                                            <strong>Congratulations, your registration has been successful</strong>
                                        </h4>
                                        <h6 align="center">
                                            <strong>Next, please pay attention to the notification that comes into your email account that was registered when registering, make sure the email that is registered is an active email.</strong>
                                        </h6>
                                        <br>
                                        <small align="center" style="color:red">
                                            <strong>NOTE : </strong> <span>If you don't receive an email, please press the resend email button and make sure the email you registered is an active email</span>
                                        </small><br><br>
                                        <!-- <div class="divider row"></div> -->
                                        <div class="d-flex align-items-center">
                                            <div class="ml-auto">
                                                <button class="btn btn-primary btn-lg" type="submit">Resend Email</button>
                                            </div>
                                        </div>
                                    </form>
                                @else
                                    <form action="{{ $link }}" method="post">
                                        @csrf
                                        <h4 align="center">
                                            <strong>Congratulations, your registration has been successful</strong>
                                        </h4>
                                        <h6 align="center">
                                            <strong>Next, please pay attention to the notification that comes into your email account that was registered when registering, make sure the email that is registered is an active email.</strong>
                                        </h6>
                                        <br>
                                        <small align="center" style="color:red">
                                            <strong>NOTE : </strong> <span>If you don't receive an email, please press the resend email button and make sure the email you registered is an active email</span>
                                        </small><br><br>
                                        <!-- <div class="divider row"></div> -->
                                        <div class="d-flex align-items-center">
                                            <div class="ml-auto">
                                                <button class="btn btn-primary btn-lg" type="submit">Verification</button>
                                            </div>
                                        </div>
                                    </form>
                                @endif
								
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://demo.dashboardpack.com/architectui-html-pro/assets/scripts/main.d810cf0ae7f39f28f336.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</body>

</html>

<!-- 

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Astech | Verifycation Email</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="{{ asset('loginAssets/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('loginAssets/css/main.css') }}">
</head>

<body>
    <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(images/bg-01.jpg);">
					<span class="login100-form-title-1">Verification Form</span>
				</div>
				<div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
                    @if ($getDataUser->email != null && $getDataUser->email_verified_at == null)
                        @include('admin.template.emailverify')
                    @endif
                </div>
			</div>
		</div>
	</div>
    {{-- <div class="limiter">
        <div class="container-login100">
            <div class="login100-form-title" style="background-image: url(images/bg-01.jpg);">
                <span class="login100-form-title-1">Sign In</span>
            </div>
            <div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
                @if ($getDataUser->phone == null && $getDataUser->email_verified_at == null)
                    @include('admin.template.emailverify')
                @endif
                @if ($getDataUser->email == null && $getDataUser->otp_verified_at == null)
                    <div class="flex-sb-m w-full p-b-48">
                        <div>
                            @include('admin.template.otpverify')
                        </div>
                    </div>
                @endif
                @if ($getDataUser->email_verified_at != null || $getDataUser->otp_verified_at != null)
                    <a href="{{ url('/home') }}">back home</a>
                @endif
                
			</div>
		</div>
	</div> --}}
	{{-- @include('admin.template._mainScript') --}}
	@include('admin.script.login._VerifyEmailScript')

    <div id="dropDownSelect1"></div>
    <script src="{{ asset('loginAssets/js/main.js') }}"></script>

</body>

</html> -->