<style>
  @import url(http://fonts.googleapis.com/css?family=Calibri:400,300,700);

  body {
      background-color: blue;
      font-family: 'Calibri', sans-serif !important
  }

  .mt-100 {
      margin-top: 50px
  }

  .mb-100 {
      margin-bottom: 50px
  }

  .card {
      border-radius: 1px !important
  }

  .card-header {
      background-color: #fff
  }

  .card-header:first-child {
      border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0
  }

  .btn-sm,
  .btn-group-sm>.btn {
      padding: .25rem .5rem;
      font-size: .765625rem;
      line-height: 1.5;
      border-radius: .2rem
  }
</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<div class="container-fluid mt-100 mb-100">
    <div id="ui-view">
        <div>
            <div class="card">
                <div class="card-header"> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                <img alt="I'm an image" border="0" class="left autowidth" src="http://astechindo.com/an-component/media/upload-gambar-pendukung/AstLogo.png" style="text-decoration: none; -ms-interpolation-mode: bicubic; border: 0; height: auto; width: 100%; max-width: 150px; display: block;" title="I'm an image" width="150"/>
                                </div>
                                <div class="col-md-6" align="right">
                                    <h4>Invoice<strong>&nbsp; #BBB-245432</strong></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                

                    <div class="row mb-4">
                        <div class="col-sm-4">
                            <h6 class="mb-3">From:</h6>
                            <div><strong>PT ASTECH</strong></div>
                            <div>Email: ASTech@email.com</div>
                            <div>Phone: (021) 54367231</div>
                            <div>3rd Floor WISMA SSK BUILDING, <br> Daan Mogot Rd No.Km. 11, RT.5/RW.4,<br> Kedaung Kali Angke, Cengkareng, <br> West Jakarta City, Jakarta 11710</div>
                        </div>

                        <div class="col-sm-4" >
                            <h6 class="mb-3">To:</h6>
                            <div><strong>Name Of Customer</strong></div>
                            <div>Email: Email@Customer.com</div>
                            <div>Phone: +628</div>
                            <div>Address Of Customer</div>
                        </div>

                        <div class="col-sm-4">
                        <div class="mb-2 ml-auto"> <span class="text-muted">Payment Details:</span>
                            <div class="d-flex flex-wrap wmin-md-400">
                                <ul class="list list-unstyled mb-0 text-left">
                                    <li>
                                        <h5 class="my-2">total bill:</h5>
                                    </li>
                                    <li>Payment Method :</li>
                                    <li>Virtual Code :</li>
                                </ul>
                                <ul class="list list-unstyled text-right mb-0 ml-auto">
                                    <li>
                                        <h5 class="font-weight-semibold my-2">Rp.4.947.800</h5>
                                    </li>
                                    <li><span class="font-weight-semibold">Bank Transfer</span></li>
                                    <li><span class="font-weight-semibold">1267273627323233</span></li>
                                </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="table-responsive-sm">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th>Item</th>
                                    <th>Description</th>
                                    <th class="center">UNIT</th>
                                    <th class="right">COST</th>
                                    <th class="right">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="center">1</td>
                                    <td class="left">AC (Samsung)</td>
                                    <td class="left">Samsung AR10HVSDBWKN Fast Cooling Deluxe Inverter Air Conditioner [1 PK]</td>
                                    <td class="center">1</td>
                                    <td class="right">Rp4.498.000</td>
                                    <td class="right">Rp4.498.000</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-sm-5 mb-1">
                            <table>
                                <tr>
                                    <td>Note :</td>
                                </tr>
                                <tr>
                                    <td>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Itaque ipsum architecto ipsa fuga. Consequuntur voluptates debitis facilis aut iure nesciunt, veniam, sequi voluptas dicta iste saepe corporis sit exercitationem itaque.</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-lg-4 col-sm-5 ml-auto">
                            <table class="table table-clear">
                                <tbody>
                                    <tr>
                                        <td class="left"><strong>Subtotal</strong></td>
                                        <td class="right">Rp4.498.000</td>
                                    </tr>
                                    <tr>
                                        <td class="left"><strong>Discount (%)</strong></td>
                                        <td class="right">0%</td>
                                    </tr>
                                    <tr>
                                        <td class="left"><strong>PPN (10%)</strong></td>
                                        <td class="right">Rp.449.800</td>
                                    </tr>
                                    <tr>
                                        <td class="left"><strong>Total</strong></td>
                                        <td class="right"><strong>Rp.4.947.800</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- <div class="pull-right"> <a class="btn btn-sm btn-success" href="#" data-abc="true"><i class="fa fa-paper-plane mr-1"></i> Proceed to payment</a>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>