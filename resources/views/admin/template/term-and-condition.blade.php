<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="Content-Language" content="en">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
    
        <title>{{ $title_header_global }}</title>
    
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
        <meta name="description" content="{{ $metta_header_global }}">
        <meta name="msapplication-tap-highlight" content="no">
        {{-- ======================CSS====================== --}}
        <link href="{{ asset('adminAssets/css/main2.css') }}" rel="stylesheet">
        <link href="{{  asset('adminAssets/css/nestable.css') }}" rel="stylesheet" />
        <link href="{{  asset('adminAssets/css/dropzone.css') }}" rel="stylesheet" />
    
    
        {{-- <link href="{{  asset('adminAssets/css/font-awesome.min.css') }}" rel="stylesheet" /> --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        {{-- ==================Javascript================== --}}
        <script type="text/javascript" src="{{ asset('adminAssets/js/dropzone.js') }}"></script>
    
        <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    </head>
<body>
    <style>
        .ui-theme-settings .btn-open-opt {
            border-radius: 50px;
            position: absolute;
            left: -114px;
            bottom: 80px;
            padding: 0;
            height: 54px;
            line-height: 54px;
            width: 54px;
            text-align: center;
            display: block;
            box-shadow: 0 0.46875rem 2.1875rem rgba(4,9,20,0.03), 0 0.9375rem 1.40625rem rgba(4,9,20,0.03), 0 0.25rem 0.53125rem rgba(4,9,20,0.05), 0 0.125rem 0.1875rem rgba(4,9,20,0.03);
            margin-top: -27px;
        }

        .alert-danger .btn-danger {
            float: right;
        }

        .alert-danger span {
            line-height: 34px;
        }

        .alert-danger > div:after {
            clear: both;
            content: '';
            display: table;
        }
    </style>
    <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <div class="logo-src">
                    <center>
                        
                        {{-- <img src="{{  url('/admin/get/logo/from-storage/' . $get_logo_astech) }}" alt="" style="width: 100px; height: auto"> --}}
                    </center>
                </div>
            </div>
        </div>
        <div class="app-header header-shadow">
            <div class="app-header__logo">
                <div class="logo-src">
                    <a href="{{ url('/') }}">
                        <img src="{{ url('/get/logo/from-storage/'.$get_logo_astech) }}" alt="" style="width:120px;height:auto">
                    </a>
                </div>
            </div>
            <div class="app-header__content">
                <div class="app-header-right">
                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a href="{{ url('/') }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn link-dropdown-avatar">
                                            Back
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="app-main">
            <div class="container">
                <br><br><br>
                <h4 align=center>{{ $getTermTitle }}</h4><br>
                <h5>{!! $getTermDesc !!}</h5>
            </div>
        </div>
    </div>
</body>

</html>