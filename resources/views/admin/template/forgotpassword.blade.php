

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{{ $title_header_global }}</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
        <meta name="description" content="{{ $metta_header_global }}">
    <meta name="msapplication-tap-highlight" content="no">

	<link href="https://demo.dashboardpack.com/architectui-html-pro/main.d810cf0ae7f39f28f336.css" rel="stylesheet"></head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow">
        <div class="app-container">
            <div class="h-100">
                <div class="h-100 no-gutters row">
                    <div class="d-none d-lg-block col-lg-4">
                        <div class="slider-light">
                            <div class="slick-slider">
                                <div>
                                    <div class="position-relative h-100 d-flex justify-content-center align-items-center bg-plum-plate" tabindex="-1">
                                        <div class="slide-img-bg" style="background-image: url('{{ asset('image2.jpg')}}');"></div>
                                        <div class="slider-content">
                                            {{-- <h3>Perfect Balance</h3>
                                            <p>ArchitectUI is like a dream. Some think it's too good to be true! Extensive
                                                collection of unified React Boostrap Components and Elements.
                                            </p> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="h-100 d-flex bg-white justify-content-center align-items-center col-md-12 col-lg-8">
                        <div class="mx-auto app-login-box col-sm-12 col-md-10 col-lg-9">
                            <div>
                                <img src="{{ url('/get/logo/from-storage/'.$get_logo_astech) }}" alt="" style="width:150px;height:auto">
                            </div><br>
                            {{-- <div class="app-logo" style="background-image: url('{{  url('/get/logo/from-storage/'. $get_logo_astech) }}'); width:150px;height:auto"></div> --}}
                            <h4 class="mb-0">
                                <span class="d-block">Forgot Password Form,</span>
                                <!-- <span>Please enter your Email.</span> -->
                            </h4>
                            <h6 class="mt-3">No account? <a href="{{ url('/user/register') }}" class="text-primary">Sign up now</a></h6>
								<div class="divider row"></div>
                            <div>
								@if(session()->has('error'))
									<div class="alert alert-danger" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<strong>{{ session()->get('error') }} </strong>
									</div>
								@endif
								@if(session()->has('success'))
									<div class="alert alert-primary" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<strong>{{ session()->get('success') }} </strong>
										<form class="" id="" action="{{ url('/forgot-password-send') }}" method="post">
									</div>
								@endif 
                            </div>
									<!-- <div class="position-relative form-check">
										<input name="check" id="exampleCheck" type="checkbox" class="form-check-input">
									<label for="exampleCheck" class="form-check-label">Keep me logged in</label>
									</div> -->

									<form class="" id="signupForm" action="{{ url('/forgot-password-send') }}" method="post">
                                    @csrf
                                    <input type="hidden" value="{{ request()->token }}" name="key">
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <div class="position-relative form-group">
                                                <label for="exampleEmail" class="">Email</label>
												<input name="email" id="exampleEmail" placeholder="Email here..." type="email" class="form-control" value="{{ old('email') }}">
											</div>
                                        </div>
                                    </div>
                                    
                                    <div class="divider row"></div>
                                    <div class="d-flex align-items-center">
                                        <div class="ml-auto">
                                            <a href="{{ url('/') }}" class="btn-lg btn btn-link">Back To Login</a>
                                            <button class="btn btn-primary btn-lg">Forgot Password</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	<script type="text/javascript" src="https://demo.dashboardpack.com/architectui-html-pro/assets/scripts/main.d810cf0ae7f39f28f336.js"></script>
</body>

</html>
