<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V15</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('loginAssets/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('loginAssets/css/main.css') }}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-form-title" style="background-image: url(images/bg-01.jpg);">
					<span class="login100-form-title-1">Sign In</span>
				</div>

				<form class="login100-form validate-form" action=" {{ url('/register') }} " method="post">
					@csrf
					<div class="wrap-input100 validate-input m-b-18" data-validate="Username is required">
						<span class="label-input100">Name</span>
						<input class="input100" type="text" name="name" value="{{ old('name') }}">
						<span class="focus-input100"></span>
					</div>
					<div class="flex-sb-m w-full p-b-10">
						@error('name')
							<span class="txt1 p-b-9" style="color:red">{{ $message }}</span>
						@enderror
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate="Username is required">
						<span class="label-input100">Email / Phone</span>
						<input class="input100" type="text" name="email" value="{{ old('email') }}">
						<span class="focus-input100"></span>
					</div>
					<div class="flex-sb-m w-full p-b-10">
						@error('email')
							<span class="txt1 p-b-9" style="color:red">{{ $message }}</span>
						@enderror
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate="Username is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password"  id="password" name="password">
						<span class="focus-input100"></span>
					</div>
					<div class="flex-sb-m w-full p-b-10">
						@error('password')
							<span class="txt1 p-b-9" style="color:red">{{ $message }}</span>
						@enderror
					</div>

					<div class="wrap-input100 validate-input m-b-18" data-validate="Username is required">
						<span class="label-input100">Password Confirm</span>
						<input class="input100" type="password" id="password-confirm" name="password_confirmation" >
						<span class="focus-input100"></span>
					</div>
					<div class="flex-sb-m w-full p-b-10">
						@error('password_confirmation')
							<span class="txt1 p-b-9" style="color:red">{{ $message }}</span>
						@enderror
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn"><i class="fa fa-sign-in" aria-hidden="true" style="color:white"></i> &nbsp;&nbsp;Sign in</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script src="{{ asset('loginAssets/js/main.js') }}"></script>

</body>
</html>