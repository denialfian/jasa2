@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <!-- <button class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Add New Grade</button> -->
            </div>
        </div>
        <div class="card-body">
            <table id="table-grade" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Detail</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Add New</button>
        </div>
    </div>
</div>
<form id="form-grade">
    <div class="modal fade" id="modal-grade" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Type</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleEmail">Tipe Produk</label>
                                <input name="name" placeholder="Tipe Produk" id="name" type="text" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleEmail">Detail</label>
                                <textarea name="desc" id="" cols="30" rows="3" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection

@section('script')
<script>
    globalCRUD.datatables({
        // orderBy: [1, 'asc'],
        url: '/business-to-business-master-type/datatables',
        selector: '#table-grade',
        columnsField: ['DT_RowIndex', 'name', 'desc'],
        modalSelector: "#modal-grade",
        modalButtonSelector: ".add-grade",
        modalFormSelector: "#form-grade",
        actionLink: {
            store: function() {
                return "/business-to-business-master-type/store";
            },
            update: function(row) {
                return "/business-to-business-master-type/update/" + row.id;
            },
            delete: function(row) {
                return "/business-to-business-master-type/destroy/" + row.id;
            },
        }
    })
</script>
@endsection
