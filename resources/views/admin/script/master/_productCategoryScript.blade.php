<script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
<script>
    $('#save').click(function(e){
        $.ajax({
            type:'post',
            data:{
                name : $('input[name="name"]').val(),
                parent_id : $('select[name="parent_id"]').val(),
                slug : $('input[name="slug"]').val(),
                product_cate_code : $('input[name="product_cate_code"]').val(),
            },

            url:Helper.apiUrl('/product_category'),
            success:function(html){
                console.log(html)
                swal.fire({
                    type: 'success',
                    title: 'Product Category Has Been Saved',
                    icon: "success",
                });
                window.location.href = Helper.redirectUrl('/show-product-category');
            },
            error:function(xhr, status, error){
                swal.fire({
                    type: 'error',
                    title: 'brand name yg dipilih tidak ditemukan',
                    icon: "error",
                });
            },
        });
        e.preventDefault();
    })

    // update
    $('#update').click(function(e){
        id = $('input[name="id"]').val()
        $.ajax({
            type:'put',
            data:{
                id : $('input[name="id"]').val(),
                name : $('input[name="name"]').val(),
                parent_id : $('select[name="parent_id"]').val(),
                slug : $('input[name="slug"]').val(),
                product_cate_code : $('input[name="product_cate_code"]').val()
            },

            url:Helper.apiUrl('/product_category/' + id),
            success:function(html){
                swal.fire({
                    type: 'success',
                    title: 'Product Category Has Been Updated',
                    icon: "success",
                });
                console.log(html)
                window.location.href = Helper.redirectUrl('/show-product-category');
            },
            error:function(xhr, status, error){
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+field).text(pesan[0])
                    })
                }
            },
        });
        e.preventDefault();
    })

    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        select: {
            style: 'multi',
        },
        columnDefs: [{
            orderable: false,
            defaultContent: '',
            className: 'select-checkbox',
            targets: 0
        }],
        dom: 'Bflrtip',
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: {
            url:Helper.apiUrl('/product_category/datatables'),
            "type": "get",
            "data": function ( d ) {
                console.log(d);
                return $.extend( {}, d, {
                    "extra_search": $('#extra').val()
                });
            }
        },
        order: [[1, 'asc']],
        columns: [
            {
                data : "id",
                name : "id",
                render : function(data, type, full) {
                    return '';

                }
            },
            {
                data : "name",
                name: "name",
            },
            {
                data : "slug",
                name: "slug",
            },
            {
                data : "product_cate_code",
                name: "product_cate_code",
            },
            {
                data : "id",
                name : "id",
                render : function(data, type, full) {
                    return "<a href='#' data-id='"+full.id+"'  class='delete btn btn-danger btn-xs'><i class='fa fa-trash'></i></a>" +
                            "&nbsp <a href=''  data-id='"+full.id+"' class='update btn btn-primary btn-xs'><i class='fa fa-pencil'></i></a>"

                }
            },
        ],
    });

    // Helper.numberRow(table)

    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        deleteProductCategory(id)
        e.preventDefault()
    })

    $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        viewProductCategory(id)
        e.preventDefault()
    })

    $('.delete-selected-category').click(function() {
        var ids = $.map(table.rows('.selected').data(), function(item) {
            return item.id;
        });
        deleteSelectedCountry(ids);
        console.log(ids)
    });

    function deleteSelectedCountry(ids) {
        $.ajax({
            url: Helper.apiUrl('/product_category/destroy_batch/'),
            type: 'delete',
            data: {
                id: ids
            },
            success: function(res) {
                table.ajax.reload();
            },
            error: function(res) {
                alert("Something went wrong");
                console.log(res);
            }
        })
    }

    function viewProductCategory(id) {
        window.location.href = Helper.redirectUrl('/update-product-category/' + id )
    }

    function deleteProductCategory(id) {
        console.log('id', id)
        if(confirm("Are you sure to delete this Product Category ? Your action can not be reversed")) {
            $.ajax({
                url:Helper.apiUrl('/product_category/' + id ),
                type: 'delete',
                dataType: 'JSON',
                contentType: 'application/json',

                success: function(res) {
                    swal.fire({
                    type: 'success',
                    title: 'Product Category Has Been Deleted',
                    icon: "success",
                });
                    table.ajax.reload();

                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            })
        }
    }

    $(document).ready(function() {
        $("#parent").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/product_category/select2'),
                // url: 'http://localhost:8001/api/product-category/select2',
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    });


</script>
