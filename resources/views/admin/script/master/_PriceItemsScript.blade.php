<script>
    // select 2 where location id
    $(document).ready(function() {
        document.getElementById("product").disabled = true;
        document.getElementById("value").disabled = true;
        $("#product").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/product/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
        $("#districHide").hide('')
        $("#vilageHide").hide('')
        

        $("#city").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/cities/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });

        $("#city").change(function() {
            $("#districHide").show('')
            $("#distric").select2({
                ajax: {
                    type: "GET",
                    url:Helper.apiUrl('/address_search/find_district?city_id=' + $(this).val()),
                    // url: 'http://localhost:8001/api/address_search/find_village?district_id=' + $(this).val(),
                    data: function(params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function(data) {
                        _distric = data.data
                        var res = $.map(data.data, function(item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        });

                        return {
                            results: res
                        };
                    }
                }
            });
        })

        $("#distric").change(function() {
            $("#vilageHide").show('')
            $("#vilage").select2({
                ajax: {
                    type: "GET",
                    url:Helper.apiUrl('/address_search/find_village?district_id=' + $(this).val()),
                    // url: 'http://localhost:8001/api/address_search/find_village?district_id=' + $(this).val(),
                    data: function(params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function(data) {
                        var res = $.map(data.data, function(item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        });

                        return {
                            results: res
                        };
                    }
                }
            }); 
        })

        $("#vilage").change(function() {
            document.getElementById("product").disabled = false;
            document.getElementById("value").disabled = false;
        })
    });
</script>