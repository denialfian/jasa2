{{-- CKEDITOR --}}
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('summary-ckeditor');

    var uniqueCounter = 0;
    var maxCounter = 100;
    var counterVArian = 0;
    $(document).ready(function() {
        var table = $('#example').DataTable({
            processing: true,
            serverSide: true,
            select: true,
            dom: 'Bflrtip',
            responsive: true,
            language: {
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            ajax: {
                url: Helper.apiUrl('/product/datatables'),
                "type": "get",
                "data": function(d) {
                    return $.extend({}, d, {
                        "extra_search": $('#extra').val()
                    });
                }
            },

            columns: [{
                    data: "id",
                    name: "id",
                },
                {
                    data: "name",
                    name: "name",
                },
                {
                    data: "model.name",
                    name: "model_name",
                },
                {
                    data: "additional.additional_code",
                    name: "additional_code",
                },

                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        return "<a href='#' data-id='" + full.id + "'  class='delete btn btn-sm btn-danger btn-xs'><i class='fa fa-trash'></i></a>" +
                            " <a href='" + Helper.redirectUrl('/edit-product/' + full.id) + "'  data-id='" + full.id + "' class='update btn btn-sm btn-warning btn-white btn-xs'><i class='fa fa-pencil'></i></a>" +
                            " <a href=''  data-id='" + full.id + "' class='view btn btn-sm btn-primary btn-xs'><i class='fa fa-eye'></i></a>"

                    }
                },
            ],
        });
    });

    $(document).ready(function() {
        $('#vendorTable').DataTable({
            "order": [
                [3, "desc"]
            ]
        });
    });

    //view Product
    // button update
    $(document).on('click', '.view', function(e) {
        id = $(this).attr('data-id');
        viewProd(id)
        e.preventDefault()
    })

    function viewProd(id) {
        window.location.href = Helper.redirectUrl('/detail-product/' + id)
    }

    // append form vendors
    $(document).ready(function() {
        $('#product-info-area').append(appendVendors(0))
        loadSelect(0)
        // add button product info
        $("#btn-add-product-info").click(function(e) {
            key = uniqueCounter;
            new_counter = 0;
            $('#product-info-area').append(appendVendors(new_counter))
            loadSelect(key)
            e.preventDefault();
        });
    });

    // apend
    $(document).on('click', "#arrt-add", function(e) {
        countVarianAdd = $(this).attr('data-attr')
        varianAdd = $(this).attr('data-test')
        appendAttributeTerms(countVarianAdd, varianAdd)
        console.log('countVarianAdd', countVarianAdd)
        e.preventDefault();
    });


    // append variant
    $(document).on('change', ".check-product-variant", function(e) {
        uniqueKey = $(this).attr('data-attr')
        // jumlahVariant = $('.attribute-select-0').length
        // jumlahCardVarian = $('.attribute-select-0').length
        countVarian = 0;
        if (this.checked) {
            appendAttribute(uniqueKey)
            // if(jumlahVariant == 0){
            //     appendAttribute(uniqueKey)
            // }else{
            //     appendAttribute(uniqueKey)
            //     for (i = uniqueKey; i < jumlahVariant ; i++) {
            //         appendAttributeTerms(uniqueKey, i)
            //         // console.log('varianAdd', i)
            //     }
            // }

        } else {
            $('#product-attr-varian-area-' + uniqueKey).html('')
        }

        e.preventDefault();
    });

    $(document).on('click', ".btn-add-product-varian", function(e) {
        // console.log('1-', uniqueKey)
        uniqueKey = $(this).parent().parent().parent().parent().parent().parent().attr('data-attr')
        uniqueKeyAttribute = $(this).parent().parent().attr('data-attr')
        jumlahVariant = $('.attribute-select-0').length
        countVarian = 0;
        counterVArian = 1;
        console.log('uniqey', $(this).parent().parent().attr('class'))

        if (jumlahVariant == 0) {
            appendAttribute(uniqueKey)
        } else {
            appendAttribute(uniqueKey, jumlahVariant);
        }

        // appendAttribute(uniqueKey)
        e.preventDefault();
    });

    $(document).ready(function() {
        $("#model_id").select2({
            ajax: {
                type: "GET",
                url: Helper.apiUrl('/product_model/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
        $("#product_additionals_id").select2({
            ajax: {
                type: "GET",
                url: Helper.apiUrl('/product_additional/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.additional_code,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    });

    function appendAttribute(x, jumlahVariant = 0) {
        var templateAttribute = '<div class="form-groups-childs child_hide-area" data-attr="' + counterVArian + '" id="child_hide-' + counterVArian + '"><br>' +
            '<div class="input-group-append" style="float:right">' +
            '<button class="btn-add-product-varian btn btn-outline-secondary" type="button">' +
            '<i class="fa fa-plus"></i>' +
            '</button>' +
            '</div><br><br>' +
            '<div class="card card-' + countVarian + '" style="background-color:white;" data-attr="' + countVarian + '">' +
            '<input type="text" name="vendor_have_varian[]" value="vendor_' + x + '_term_' + counterVArian + '[]">' +
            '<div class="card-body">' +
            '<div class="position-relative row form-group" data-attr="remove1">' +
            '<label for="exampleEmail" class="col-sm-2 col-form-label">Attribute</label>' +
            '<div class="input-group col-md-4">' +
            '<select class="product_attribute_id  attribute-select-' + x + '" id="product_attribute_id-' + counterVArian + '" data-verian="' + x + '" name="vendor_' + x + '_attr_' + countVarian + '[]" style="width:100%">' +
            '<option selected-disabled>"vendor_' + x + '_attr_' + countVarian + '[]"</option>' +
            '<option value="WY">Wyoming</option>' +
            '</select>' +
            '</div>' +
            // '<label for="exampleEmail" class="col-sm-2 col-form-label" id="attr-terms-'+ counterVArian +'"></label>'+
            '<div class="input-group col-md-4">' +
            '<select class="js-example-basic-single" id="product_attribute_term_id-' + counterVArian + '" name="vendor_' + x + '_term_' + countVarian + '[]" style="width:100%">' +
            '<option selected-disabled>"vendor_' + x + '_term_' + countVarian + '[]"</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-md-2">' +
            '<button class="remove btn btn-outline-secondary" type="button" id="arrt-add" data-test="' + x + '" data-attr="' + counterVArian + '" style="float:left">' +
            '<i class="fa fa-plus"></i> add' +
            '</button>' +
            '</div>' +
            '</div>' +
            '<div class="append-variansss" data-attr="' + counterVArian + '" id="varian-add' + x + '">';
        for (i = 1; i < jumlahVariant; i++) {
            templateAttribute += templateAppendAttributeTerms(uniqueKeyAttribute, i)
            // console.log('varianAdd', i)
        }
        templateAttribute += '</div><br>' +
            '<div class="position-relative row form-group">' +
            '<label for="exampleEmail" class="col-sm-2 col-form-label">Stock</label>' +
            '<div class="input-group col-md-10">' +
            '<input name="vendor_' + x + '_stock[]" id="examplename" placeholder="Stock" type="text" class="form-control">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
        $('#product-attr-varian-area-' + x).append(templateAttribute)
        loadSelect2(counterVArian)
        counterVArian++;
        return templateAttribute;
    }

    function appendVendors(x) {
        var templateProduct = '<div class="form-groups1" id="form1Hide-' + uniqueCounter + '" data-attr="' + uniqueCounter + '"><br>' +
            '<div class="card" style="background-color:white">' +
            '<div class="card-body">' +
            '<div class="position-relative row form-group">' +
            '<label for="exampleEmail" class="col-sm-2 col-form-label">Vendor</label>' +
            '<div class="input-group col-md-4">' +
            '<select class="js-example-basic-single" id="vendor_id-' + uniqueCounter + '" name="vendor_id[]" style="width:100%">' +
            '<option selected-disabled>Select Vendor</option>' +
            '<option value="WY">Wyoming</option>' +
            '</select>' +
            '</div>' +
            '<input type="hidden" name="is_attribute" value="1">' +
            '<label for="exampleEmail" class="col-sm-2 col-form-label">Vendor Code</label>' +
            '<div class="input-group col-md-4">' +
            '<input name="vendor_code[]" id="examplename" placeholder="Part Code" type="text" class="form-control">' +
            '</div>' +
            '</div>' +
            '<div class="position-relative row form-group">' +
            '<label for="exampleEmail" class="col-sm-2 col-form-label">Part Code</label>' +
            '<div class="input-group col-md-4">' +
            '<input name="part_code[]" id="examplename" placeholder="Part Code" type="text" class="form-control">' +
            '</div>' +
            '<label for="exampleEmail" class="col-sm-2 col-form-label">Brand Code</label>' +
            '<div class="input-group col-md-4">' +
            '<input name="brand_code[]" id="examplename" placeholder="Brand Code" type="text" class="form-control">' +
            '</div>' +
            '</div>' +
            '<div class="btn-group btn-group-vertical">' +
            '<label class="btn">' +
            '<input type="checkbox" name="cek-product-varian" class="check-product-variant" value="1" data-attr="' + uniqueCounter + '" hidden>' +
            '<i class="fa fa-square-o fa-2x"></i>' +
            '<i class="fa fa-check-square-o fa-2x"></i>' +
            '<span>&nbsp;Add Product Variant</span>' +
            '</label>' +
            '</div>' +
            '<div class="append-variansss" data-attr="' + uniqueCounter + '" id="product-attr-varian-area-' + uniqueCounter + '">' +

            '</div>' +
            '</div>' +
            '</div>' +
            '</div><br>';
        uniqueCounter++;
        return templateProduct;
    }

    function appendAttributeTerms(x, y) {
        var templateVariant = '<div class="position-relative row form-group" data-attr="remove1">' +
            '<label for="exampleEmail" class="col-sm-2 col-form-label">varian-add-' + counterVArian + '</label>' +
            '<div class="input-group col-md-4">' +
            '<select class="product_attribute_id attribute-select-' + y + '" id="product_attribute_id-' + counterVArian + '" data-verian="' + y + '" name="vendor_' + x + '_attr_' + counterVArian + '[]" style="width:100%">' +
            '<option selected-disabled>"vendor_' + x + '_attr_' + counterVArian + '[]"</option>' +
            '<option value="WY">Wyoming</option>' +
            '</select>' +
            '</div>' +
            // '<label for="eyampleEmail" class="col-sm-2 col-form-label" id="attr-terms-'+ counterVArian +'"></label>'+
            '<div class="input-group col-md-4">' +
            '<select class="js-example-basic-single" id="product_attribute_term_id-' + counterVArian + '" name="vendor_' + y + '_term_' + counterVArian + '[]" style="width:100%">' +
            '<option selected-disabled>"asdasdvendor_' + y + '_term_' + counterVArian + '[]"</option>' +
            '</select>' +
            '</div>' +
            '</div>';
        // console.log('appendAttributeTerms', '#varian-add' + x)

        $('#varian-add' + x).append(templateVariant)
        loadSelect2(counterVArian)
        counterVArian++;
        // return templateVariant;

    }

    function templateAppendAttributeTerms(x, y) {
        var templateAppendAttributeTerms = '<div class="position-relative row form-group" data-attr="remove1">' +
            '<label for="exampleEmail" class="col-sm-2 col-form-label">varian-add-' + counterVArian + '</label>' +
            '<div class="input-group col-md-4">' +
            '<select class="product_attribute_id attribute-select-' + y + '" id="product_attribute_id-' + counterVArian + '" data-verian="' + y + '" name="vendor_' + x + '_attr_' + counterVArian + '[]" style="width:100%">' +
            '<option selected-disabled>"vendor_' + x + '_attr_' + counterVArian + '[]"</option>' +
            '<option value="WY">Wyoming</option>' +
            '</select>' +
            '</div>' +
            // '<label for="eyampleEmail" class="col-sm-2 col-form-label" id="attr-terms-'+ counterVArian +'"></label>'+
            '<div class="input-group col-md-4">' +
            '<select class="js-example-basic-single" id="product_attribute_term_id-' + counterVArian + '" name="vendor_' + y + '_term_' + counterVArian + '[]" style="width:100%">' +
            '<option selected-disabled>"vendor_' + uniqueCounter + '_term_' + counterVArian + '[]"</option>' +
            '</select>' +
            '</div>' +
            '</div>';
        // console.log('appendAttributeTerms', '#varian-add' + x)
        loadSelect2(counterVArian)
        counterVArian++;
        return templateAppendAttributeTerms;

    }

    function loadSelect(id) {
        $("#additional-" + id).select2({
            ajax: {
                type: "GET",
                url: Helper.apiUrl('/product_additional/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.additional_code,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
        $("#vendor_id-" + id).select2({
            ajax: {
                type: "GET",
                url: Helper.apiUrl('/vendors/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }

    function loadSelect2(id) {
        console.log(id)
        $('#product_attribute_term_id-' + id).hide('')
        // $('#product_attribute_term_id-' + id).select2();
        $('#product_attribute_id-' + id).select2({
            ajax: {
                type: "GET",
                url: Helper.apiUrl('/product_attribute/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });

        $('#product_attribute_id-' + id).change(function() {
            $('#product_attribute_term_id-' + id).select2({
                ajax: {
                    type: "GET",
                    url: Helper.apiUrl('/product_attribute/childs/' + $(this).val()),
                    // url: 'http://localhost:8001/api/address_search/find_district?city_id=' + $(this).val(),
                    data: function(params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function(data) {
                        var res = $.map(data.data, function(item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        });

                        return {
                            results: res
                        };
                    }
                }
            });
        })
    }

    // save data product
    $('#save').click(function(e) {
        data = {}

        forminput = $('#store').serializeArray()
        console.log(forminput)
        console.log(forminput)
        $.each(forminput, function(key, field) {
            var isArray = field.name.substr(field.name.length - 2);
            fieldName = field.name.slice(0, -2);
            if (isArray == '[]') {
                console.log(fieldName)
                data[fieldName] = $('[name="' + field.name + '"]').map(function() {
                    return $(this).val()
                }).get()
            } else {
                data[field.name] = $('[name="' + field.name + '"]').val()
            }
        })
        console.log(data)
        $.ajax({
            type: 'post',
            data: data,
            url: Helper.apiUrl('/product'),
            success: function(html) {
                swal.fire({
                    title: "success",
                    text: "Success",
                    type: "success",
                    timer: 3000
                });
                window.location.href = Helper.redirectUrl('/show-list-product');
            },
        });
        e.preventDefault();
    })
</script>