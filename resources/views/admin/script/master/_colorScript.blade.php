<script>
    var Attribute;

    $('#form-attribute-create').submit(function(e) {
        $.ajax({
            type: 'post',
            data: {
                name: $('input[name="name"]').val(),
                childs: $('input[name="childs[]"]').map(function() {
                    return $(this).val()
                }).get(),
                color_code: $('input[name="color_code[]"]').map(function() {
                    return $(this).val()
                }).get(),
                hex_rgb: $('input[name="hex_rgb[]"]').map(function() {
                    return $(this).val()
                }).get(),
            },
            url: Helper.apiUrl('/product_attribute'),
            success: function(resp) {
                // send notif
                Helper.successNotif(resp.msg).redirectTo('/show-list-attribute');
            },
            error: function(xhr, status, error) {
                Helper.errorMsgRequest(xhr, status, error);
            },
        });
        e.preventDefault();
    })

    // update
    $('#form-attribute-update').submit(function(e) {
        id = $('input[name="id"]').val()
        $.ajax({
            type: 'put',
            data: {
                id: $('input[name="id"]').val(),
                name: $('input[name="name"]').val(),
                child_id: $('input[name="child_id[]"]').map(function() {
                    return $(this).val()
                }).get(),
                childs: $('input[name="childs[]"]').map(function() {
                    return $(this).val()
                }).get(),
                color_code: $('input[name="color_code[]"]').map(function() {
                    return $(this).val()
                }).get(),
                hex_rgb: $('input[name="hex_rgb[]"]').map(function() {
                    return $(this).val()
                }).get(),
            },

            url: Helper.apiUrl('/product_attribute/' + id),
            success: function(resp) {
                // send notif
                Helper.successNotif(resp.msg);
                window.location.href = Helper.redirectUrl('/show-list-attribute');
            },
            error: function(xhr, status, error) {
                Helper.errorMsgRequest(xhr, status, error);
            },
        });
        e.preventDefault();
    })

    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        select: true,
        dom: 'Bflrtip',
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/product_attribute/datatables'),
            "type": "get",
            "data": function(d) {
                return $.extend({}, d, {
                    "extra_search": $('#extra').val()
                });
            }
        },

        columns: [{
                data: "id",
                name: "id"
            },
            {
                data: "name",
                name: "name",
            },
            {
                data: "child",
                name: "child",
                render: function(data, type, full) {
                    return full.childs.length;
                }
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    return "<a href='#'  data-id='" + full.id + "' class='update btn btn-warning btn-sm btn-white'><i class='fa fa-pencil'></i> Edit</a> <a href='#' data-id='" + full.id + "'  class='delete btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</a>";

                    return "<a href='' data-id='" + full.id + "'  class='add btn btn-warning btn-sm'><i class='fa fa-plus'></i></a>" +
                        "&nbsp<a href='' data-id='" + full.id + "'  class='view btn btn-success btn-sm'><i class='fa fa-eye'></i></a>" +
                        "&nbsp<a href='' data-id='" + full.id + "'  class='delete btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>" +
                        "&nbsp<a href=''  data-id='" + full.id + "' class='update btn btn-primary btn-sm'><i class='fa fa-pencil'></i></a>"

                }
            },
        ],
    });

    Helper.numberRow(table)

    // button add
    $(document).on('click', '.add', function(e) {
        id = $(this).attr('data-id');
        addAttribute(id)
        e.preventDefault()
    })

    function addAttribute(id) {
        window.location.href = Helper.redirectUrl('/child-master-attribute-create/' + id)
    }

    // button update
    $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        updateAttribute(id)
        e.preventDefault()
    })

    function updateAttribute(id) {
        window.location.href = Helper.redirectUrl('/update-master-attribute/' + id)
    }

    //button delete
    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        deleteAttribute(id)
        e.preventDefault()
    })

    function deleteAttribute(id) {
        Helper.confirm(function() {
            $.ajax({
                url: Helper.apiUrl('/product_attribute/' + id),
                type: 'delete',
                dataType: 'JSON',
                contentType: 'application/json',

                success: function(res) {
                    // send notif
                    Helper.successNotif('data berhasil didelete');

                    table.ajax.reload();
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error);
                }
            })
        })
    }

    $(document).ready(function() {
        var max_fields = 100; //maximum input boxes allowed
        var wrapper = $(".form-groups1"); //Fields wrapper
        var add_button = $(".add_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="form-groups1" id="form1Hide"><br>' +
                    '<div class="input-group-append" style="float:right">' +
                    '<button class="remove_fields btn btn-outline-secondary" type="button" id="button-addon2">' +
                    '<i class="fa fa-minus"></i>' +
                    '</button>' +
                    '</div><br><br>' +
                    '<div class="card" style="background-color:white">' +
                    '<div class="card-body">' +
                    '<div class="position-relative row form-group">' +
                    '<label for="exampleEmail" class="col-sm-2 col-form-label">Color Code</label>' +
                    '<div class="input-group col-md-10">' +
                    '<input type="text" name="color_code[]" class="form-control" placeholder="Color Code" >' +
                    '</div>' +
                    '</div>' +
                    '<div class="position-relative row form-group">' +
                    '<label for="exampleEmail" class="col-sm-2 col-form-label">Color Code</label>' +
                    '<div class="input-group col-md-10">' +
                    '<input type="color" name="hex_rgb[]" class="form-control" placeholder="Color Code" >' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div><br>'); //add input box
            }
        });

        $(wrapper).on("click", ".remove_fields", function(e) { //user click on remove text
            e.preventDefault();
            $(this).parent().parent().remove();
            x--;
        })
    });

    $(document).ready(function() {
        var max_fields = 100; //maximum input boxes allowed
        var wrapper = $(".form-groups2"); //Fields wrapper
        var add_button = $(".add_button"); //add_button button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e) { //on add_button input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="form-groups2" id="form1Hide2"><br>' +
                    '<div class="input-group-append" style="float:right">' +
                    '<button class="remove btn btn-outline-secondary" type="button" id="button-addon3">' +
                    '<i class="fa fa-minus"></i>' +
                    '</button>' +
                    '</div><br><br>' +
                    '<div class="card" style="background-color:white">' +
                    '<div class="card-body">' +
                    '<div class="position-relative row form-group">' +
                    '<label for="exampleEmail" class="col-sm-2 col-form-label">Color Code</label>' +
                    '<div class="input-group col-md-10">' +
                    '<textarea class="form-control" name="name[]" id="exampleFormControlTextarea1" rows="3"></textarea>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div><br>'); //add input box
            }
        });

        $(wrapper).on("click", ".remove", function(e) { //user click on remove text
            e.preventDefault();
            $(this).parent().parent().remove();
            x--;
        })
    });
</script>