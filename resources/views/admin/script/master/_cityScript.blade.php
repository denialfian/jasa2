<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.0/dropzone.js"></script>
<script>
    $(document).ready(function() {
        $('#provHide').hide('')
        $("#country").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/country/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });

        $("#country").change(function() {
            $('#province').val('');
            $('#provHide').show('');
            provinceSelect($(this).val());
        })
        provinceSelect($(this).val());
    });

    function provinceSelect(id) {
        $("#province").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/address_search/find_province?country_id=' + id),
                // url: 'http://localhost:8001/api/address_search/find_district?city_id=' + $(this).val(),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }
</script>
