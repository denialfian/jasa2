<script>
    //store data
    $('#save').click(function(e) {
        $.ajax({
            type: 'post',
            data: {
                name: $('input[name="name"]').val(),
                code: $('input[name="code"]').val(),
            },

            url: Helper.apiUrl('/country'),
            success: function(html) {
                console.log(html)
                iziToast.success({
                    title: 'OK',
                    position: 'topRight',
                    message: 'Countries Has Been Saved',
                });
                window.location.href = Helper.redirectUrl('/admin/country/show');
            },
            error: function(xhr, status, error) {
                iziToast.error({
                    title: 'Error',
                    position: 'topRight',
                    message: 'Something Went Wrong',
                });
            },
        });
        e.preventDefault();
    });

    $("#import").click(function() {
        var fd = new FormData();
        var files = $('#file')[0].files[0];
        if (files) {
            fd.append('file', files);
        }

        $.ajax({
            url: Helper.apiUrl('/countries/importExcel'),
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    swal.fire({
                        type: 'success',
                        title: 'File Has Been Imported',
                        icon: "success",
                    });
                    window.location.href = Helper.redirectUrl('/show-countries');
                } else {
                    swal.fire({
                        type: 'error',
                        title: 'File Not Uploaded',
                        icon: "error",
                    });
                }
            },
        });
    });

    // update
    $('#update').click(function(e) {
        id = $('input[name="id"]').val()
        $.ajax({
            type: 'put',
            data: {
                name: $('input[name="name"]').val(),
                code: $('input[name="code"]').val(),
            },
            url: Helper.apiUrl('/countries/' + id),
            success: function(html) {
                iziToast.success({
                    title: 'OK',
                    position: 'topRight',
                    message: 'Countries Has Been Saved',
                });
                console.log(html)
                window.location.href = Helper.redirectUrl('/show-countries');
            },
            error: function(xhr, status, error) {
                if (xhr.status == 422) {
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field) {
                        $('#error-' + field).text(pesan[0])
                    })
                }
            },
        });
        e.preventDefault();
    })

    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        select: {
            style: 'multi',
        },
        columnDefs: [{
            orderable: false,
            defaultContent: '',
            className: 'select-checkbox',
            targets: 0,
            searchable: false,
        }],
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",

        ajax: {
            url: Helper.apiUrl('/countries/datatables'),
            "type": "get",
            "data": function(d) {
                return $.extend({}, d, {
                    "extra_search": $('#extra').val()
                });
            }
        },

        columns: [{
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    return '';
                }
            },
            {
                data: "id",
                name: "id"
            },
            {
                data: "name",
                name: "name",
            },
            {
                data: "code",
                name: "code",
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    return "<a href=''  data-id='" + full.id + "' class='update btn btn-warning btn-white btn-sm'><i class='fa fa-pencil'></i> Update</a> <a href='#' data-id='" + full.id + "'  class='delete btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</a>"

                }
            },
        ],
    });

    table.on( 'order.dt search.dt', function () {
        table.column(1, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        deleteCountries(id)
        e.preventDefault()
    })

    $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        viewCountries(id)
        e.preventDefault()
    })

    function viewCountries(id) {
        window.location.href = Helper.redirectUrl('/update-countries/' + id)
    }

    function deleteCountries(id) {
        console.log('id', id)
        if (confirm("Are you sure to delete this Countries ? Your action can not be reversed")) {
            $.ajax({
                url: Helper.apiUrl('/countries/' + id),
                type: 'delete',
                dataType: 'JSON',
                contentType: 'application/json',

                success: function(res) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Countries Has Been Deleted',
                    });
                    table.ajax.reload();

                },
                error: function(res) {
                    iziToast.error({
                        title: 'Error',
                        position: 'topRight',
                        message: 'Somethin Went Wrong',
                    });
                }
            })
        }
    }

    $('.delete-selected-country').click(function() {
        var ids = $.map(table.rows('.selected').data(), function(item) {
            return item.id;
        });
        deleteSelectedCountry(ids);
        console.log(ids)
    });

    function deleteSelectedCountry(ids) {
        if (confirm("Are you sure to delete this Countries ? Your action can not be reversed")) {
            $.ajax({
                url: Helper.apiUrl('/countries/destroy_batch/'),
                type: 'delete',
                data: {
                    id: ids
                },
                success: function(res) {
                    table.ajax.reload();
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            });
        }
    }
</script>
