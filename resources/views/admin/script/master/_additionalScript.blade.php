<script>
    $('#save').click(function(e){
        $.ajax({
            type:'post',
            data:{
                additional_code : $('input[name="additional_code"]').val(),
            },
           
            url:Helper.apiUrl('/product_additional'),
            success:function(html){
                console.log(html)
                swal.fire({
                    type: 'success',
                    title: 'Additionals Has Been Saved',
                    icon: "success",
                });
                window.location.href = Helper.redirectUrl('/show-additional');
            },
            error:function(xhr, status, error){
                swal.fire({
                    type: 'error',
                    title: 'Something went worng',
                    icon: "error",
                });
            },
        });
        e.preventDefault();
    })

    // update
    $('#update').click(function(e){
        id = $('input[name="id"]').val()
        $.ajax({
            type:'put',
            data:{
                id : $('input[name="id"]').val(),
                additional_code : $('input[name="additional_code"]').val(),
            },
            
            url:Helper.apiUrl('/product_additional/' + id),
            success:function(html){
                swal.fire({
                    type: 'success',
                    title: 'Additionals Has Been Updated',
                    icon: "success",
                });
                console.log(html)
                window.location.href = Helper.redirectUrl('/show-additional');
            },
            error:function(xhr, status, error){
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+field).text(pesan[0])
                    })
                }
            },
        });
        e.preventDefault();
    })

    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        select: true,
        dom: 'Bflrtip',
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },

        ajax: {
            url:Helper.apiUrl('/product_additional/datatables'),
            "type": "get",
            "data": function ( d ) {
                return $.extend( {}, d, {
                    "extra_search": $('#extra').val()
                });
            }
        },

        columns: [
            { 
                data : "id", 
                name: "id" 
            },
            { 
                data : "additional_code", 
                name: "additional_code",
            },
            { 
                data : "id", 
                name : "id", 
                render : function(data, type, full) {
                    return "<a href='#' data-id='"+full.id+"'  class='delete btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>" +
                            "&nbsp <a href=''  data-id='"+full.id+"' class='update btn btn-primary btn-sm'><i class='fa fa-pencil'></i></a>"
                
                }
            },
        ],
    });

    Helper.numberRow(table)

    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        deleteColor(id)
        e.preventDefault()
    })

    $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        viewColor(id)
        e.preventDefault()
    })

    function viewColor(id) {
        window.location.href = Helper.redirectUrl('/update-additional/' + id )
    }

    function deleteColor(id) {
        console.log('id', id)
        if(confirm("Are you sure to delete this Additionals ? Your action can not be reversed")) {
            $.ajax({
                url:Helper.apiUrl('/product_additional/' + id ),
                type: 'delete',
                dataType: 'JSON',
                contentType: 'application/json',

                success: function(res) {
                    swal.fire({
                    type: 'success',
                    title: 'Color Has Been Deleted',
                    icon: "success",
                });
                    table.ajax.reload();

                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            })
        }
    }
</script>