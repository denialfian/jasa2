<script>
    $('#form-product-model-create').submit(function(e) {
        $.ajax({
            type: 'post',
            data: {
                name: $('input[name="name"]').val(),
                product_brands_id: $('select[name="product_brands_id"]').val(),
                ms_product_category_id: $('select[name="ms_product_category_id"]').val(),
                model_code: $('input[name="model_code"]').val(),
            },
            url: Helper.apiUrl('/product_model'),
            success: function(resp) {
                // send notif
                Helper.successNotif(resp.msg);
                //redirect
                window.location.href = Helper.redirectUrl('/show-product-model');
            },
            error: function(xhr, status, error) {
                Helper.errorMsgRequest(xhr, status, error)
            },
        });
        e.preventDefault();
    })

    // update
    $('#form-product-model-update').submit(function(e) {
        id = $('input[name="id"]').val()
        $.ajax({
            type: 'put',
            data: {
                id: $('input[name="id"]').val(),
                product_brands_id: $('select[name="product_brand_id"]').val(),
                name: $('input[name="name"]').val(),
                ms_product_category_id: $('select[name="ms_product_category_id"]').val(),
                model_code: $('input[name="model_code"]').val()
            },

            url: Helper.apiUrl('/product_model/' + id),
            success: function(resp) {
                // send notif
                Helper.successNotif(resp.msg);
                window.location.href = Helper.redirectUrl('/show-product-model');
            },
            error: function(xhr, status, error) {
                Helper.errorMsgRequest(xhr, status, error)
            },
        });
        e.preventDefault();
    })

    var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        select: true,
        dom: 'Bflrtip',
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/product_model/datatables'),
            "type": "get",
            "data": function(d) {
                return $.extend({}, d, {
                    "extra_search": $('#extra').val()
                });
            }
        },

        columns: [{
                data: "id",
                name: "id"
            },
            {
                data: "name",
                name: "name",
            },
            {
                data: "model_code",
                name: "model_code",
            },
            {
                data: "product_brand.name",
                name: "product_brand.name",
            },
            {
                data: "product_category.name",
                name: "ms_product_category_id",
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    return "<a href=''  data-id='" + full.id + "' class='update btn btn-warning btn-white btn-sm'><i class='fa fa-pencil'></i></a> <a href='#' data-id='" + full.id + "'  class='delete btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>";
                }
            },
        ],
    });

    Helper.numberRow(table)

    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        deleteProductModel(id)
        e.preventDefault()
    })

    $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        viewProductModel(id)
        e.preventDefault()
    })

    function viewProductModel(id) {
        window.location.href = Helper.redirectUrl('/update-product-model/' + id)
    }

    function deleteProductModel(id) {
        Helper.confirm(function() {
            $.ajax({
                url: Helper.apiUrl('/product_model/' + id),
                type: 'delete',
                dataType: 'JSON',
                contentType: 'application/json',

                success: function(res) {
                    Helper.successNotif('data berhasil dihapus');
                    table.ajax.reload();
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error)
                }
            })
        });
    }

    $(document).ready(function() {
        $("#product_category").select2({
            ajax: {
                type: "GET",
                url: Helper.apiUrl('/product_category/select2'),
                // url: 'http://localhost:8001/api/product-category/select2',
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
        $("#product_brand").select2({
            ajax: {
                type: "GET",
                url: Helper.apiUrl('/product_brand/select2'),
                // url: 'http://localhost:8001/api/product-category/select2',
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    });
</script>