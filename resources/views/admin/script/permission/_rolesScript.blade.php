

<script>

    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });

    // view datatable
     var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        select: true,
        dom: 'Bflrtip',
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: {
            "url": Helper.apiUrl('/role/datatables'),
            "type": "get",
            "data": function ( d ) {
                return $.extend( {}, d, {
                    "extra_search": $('#extra').val()
                });
            }
        },
        columns: [
            { data : "DT_RowIndex", name: "DT_RowIndex", sortable:false, orderable:false },
            { data : "name", name: "name" },
            { data : "guard_name", name: "guard_name" },
            { data : "id", name : "id", render : function(data, type, full) {
                return "<a href='#' data-id='"+full.id+"' class='delete btn btn-danger btn-xs'><i class='fa fa-trash'></i></a>" +
                        "&nbsp <a href='' data-id='"+full.id+"' class='update btn btn-primary btn-xs'><i class='fa fa-pencil'></i></a>"

            }},
        ],
    });

   // Add Number Rows
    Helper.numberRow(table)


    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        deleteRoles(id)
        viewRoles(id)
        e.preventDefault()
    })

    $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        viewRoles(id)
        e.preventDefault()
    })

    function viewRoles(id) {
        window.location.href = Helper.redirectUrl('/admin/role/update/' + id )
    }

    function deleteRoles(id) {
        console.log('id', id)
        if(confirm("Are you sure to delete this Roles ? Your action can not be reversed")) {
            $.ajax({
                url:Helper.apiUrl('/roles/' + id ),
                type: 'delete',



                dataType: 'JSON',
                contentType: 'application/json',

                success: function(res) {
                    table.ajax.reload();
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            })
        }
    }

    </script>
