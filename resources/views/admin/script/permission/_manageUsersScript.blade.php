<script>
    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        deleteUsers(id)
        e.preventDefault()
    })

    $(document).on('click', '.update', function(e) {
        id = $(this).attr('data-id');
        viewUsers(id)
        e.preventDefault()
    })

    //redirect to update Views
    function viewUsers(id) {
        window.location.href = Helper.redirectUrl('/users-update/' + id )
    }

    //delete Function
    function deleteUsers(id) {
        console.log('id', id)
        if(confirm("Are you sure to delete this Users ? Your action can not be reversed")) {
            $.ajax({
                url:Helper.apiUrl('/users/' + id ),
                type: 'delete',

                
                
                dataType: 'JSON',
                contentType: 'application/json',

                success: function(res) {
                    table.ajax.reload();
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            })
        }
    }

    //store data 
    $('#submit').click(function(e){
        $.ajax({
            type:'post',
            data:{
                name : $('input[name="name"]').val(),
                email : $('input[name="email"]').val(),
                password : $('input[name="password"]').val(),
                password_confirmation : $('input[name="password_confirmation"]').val(),
                role_id    : $('select[name="role_id"]').val(),
            },
            
            url:Helper.apiUrl('/users'),
            success:function(html){
                // console.log(html)
                window.location.href = Helper.redirectUrl('/manage-users');
            },
            error:function(xhr, status, error){
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+field).text(pesan[0])
                    })
                }
            },
        });
        e.preventDefault();
    })

    // update
    $('#updated').click(function(e){
        id = $('input[name="id"]').val()
        alert(id)
        $.ajax({
            type:'put',
            data:{
                name : $('input[name="name"]').val(),
                email : $('input[name="email"]').val(),
                role_id : $('select[name="role_id"]').val(),
            },
            
            url:Helper.apiUrl('/users/' + id),
            success:function(html){
                alert('asdas')
                console.log(html)
                window.location.href = Helper.redirectUrl('/manage-users');
            },
            error:function(xhr, status, error){
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+field).text(pesan[0])
                    })
                }
            },
        });
        e.preventDefault();
    })
</script>