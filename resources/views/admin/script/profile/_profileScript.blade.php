<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<script>
    $(function () {
        $("#datepicker").datepicker({
                autoclose: true,
                todayHighlight: true
        }).datepicker('update', new Date());
    });

</script>

<script>
    var _distric;

    $(function() {
        $('#profile-image1').on('click', function() {
            $('#profile-image-upload').click();
        });
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
            $('#profile-image1').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#profile-image-upload").change(function() {
        readURL(this);
    });

    $("#save").click(function() {
        var redirect = $('input[name  = "url"]').val();
        var fd = new FormData();
        var files = $('#profile-image-upload')[0].files[0];
        if(files){
            fd.append('picture', files);
        }
        fd.append('ms_religion_id', $('select[name="ms_religion_id"]').val());
        fd.append('ms_marital_id', $('select[name="ms_marital_id"]').val());
        fd.append('gender', $('select[name="gender"]').val());
        fd.append('first_name', $('input[name="first_name"]').val());
        fd.append('last_name', $('input[name="last_name"]').val());
        fd.append('date_of_birth', $('input[name="date_of_birth"]').val());
        fd.append('place_of_birth', $('input[name="place_of_birth"]').val());
        // console.log(fd)
        $.ajax({
            url:Helper.apiUrl('/additional_info'),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Data Has Been Saved',
                    });
                    console.log('url TO', redirect)
                    if(redirect == 'teknisi'){
                        window.location = Helper.url('/teknisi/profile');
                    }
                    if(redirect == 'customer'){
                        window.location = Helper.url('/customer/profile/show');
                    }
                }
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });

                    })
                }
            },
        });
    });

</script>
