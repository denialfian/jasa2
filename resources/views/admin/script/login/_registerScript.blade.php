<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" integrity="sha256-KsRuvuRtUVvobe66OFtOQfjP8WA2SzYsmm4VPfMnxms=" crossorigin="anonymous"></script>
<script>
    $('#register').click(function(e){
        $.ajax({
            type:'post',
            data:{
                name : $('input[name="name"]').val(),
                email : $('input[name="email"]').val(),
                password : $('input[name="password"]').val(),
                password_confirmation : $('input[name="password_confirmation"]').val(),
            },
            url:Helper.apiUrl('/register'),
            success:function(html){
                console.log(html)
                Swal.fire({
                    icon: 'success',
                    title: 'CONGRATULATION',
                    text: 'Register Has Ben Successful',
                })
                // window.location.href = Helper.redirectUrl('/');
            },
            error:function(html){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!'
                })
            },
        });
        e.preventDefault();
    })
</script>