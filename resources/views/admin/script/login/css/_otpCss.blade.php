<style>
    .input-code{
        margin-top:50px;
        padding:0 100px;
        display:flex;
        flex-direction:auto;
        justify-content:space-around;
        align-content:space-between;
    }

    .otp-number-input{
        background-color:transparent;
        text-align: center;
        line-height: 80px;
        font-size: 30px;
        outline: none;
        width: 10%;
        transition: all .2s ease-in-out;
        border-radius: none;
        border-top:none;
        border-left:none;
        border-right:none;
        border-bottom:2px solid black;
        margin-bottom:50px;
            
        &:focus {
        border-bottom:5px solid orange;
        }
            
        &::selection {
        background: transparent;
        }
    }

    .verif-b-orange{
        border-bottom:5px solid orange;
    }
</style>