<script>
    $('#login').click(function(e){
        $.ajax({
            type:'post',
            data:{
                email : $('input[name="username"]').val(),
                password : $('input[name="pass"]').val(),
            },
            url:Helper.apiUrl('/login'),
            success:function(html){
                console.log(html)
                // simpan ke
                Helper.saveToLocalstorage('token', html.data.token)
                Helper.saveToLocalstorage('user', html.data.user)
                // redirect
                window.location.href = Helper.redirectUrl('/home');
            },
            error:function(html){
                alert('login gagal');
            },
        });
        e.preventDefault();
    })
</script>