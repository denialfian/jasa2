<script>
   $(document).ready(function(){
      $(":input").inputmask();

      $("#phone").inputmask({
         mask: '9 9 9 9',
         placeholder: '_-_-_-',
         showMaskOnHover: false,
         showMaskOnFocus: false,
         onBeforePaste: function (pastedValue, opts) {
         var processedValue = pastedValue;

         //do something with it

         return processedValue;
         }
      });
   });
</script>