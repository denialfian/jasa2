<script>
    var token = $('#getToken').text();
    // var user;
    $.ajax({
        type:'get',
        url:Helper.apiUrl('/password/find/' + token),

        success:function(html){
            // user = html;
            console.log(html.data.email)
            $('#email').val(html.data.email)
        },

        error:function(html){
            $('.wrap-login100').remove()
            // window.location.href = Helper.redirectUrl('/');
        },
    });

    $('#pasword_create').click(function(e){
        $.ajax({
            type:'post',
            data:{
                email : $('input[name="email"]').val(),
                password : $('input[name="password"]').val(),
                password_confirmation : $('input[name="password_confirmation"]').val(),
                token : $('input[name="token"]').val(),
            },
            url:Helper.apiUrl('/password/change'),
            success:function(html){
                console.log(html)
                window.location.href = Helper.redirectUrl('/')
                Swal.fire({
                    icon: 'success',
                    title: 'CONGRATULATION',
                    text: 'Your Password Has Been Updated',
                })
            },
            error:function(html){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!'
                })
            },
        });
        e.preventDefault();
    })
</script>