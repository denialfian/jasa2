@extends('admin.home')
@section('content')
<div class="col-md-12 col-xl-12">
    <div class="main-card mb-3 card">
        <div class="no-gutters row">
            <div class="col-md-4">
                <div class="pt-0 pb-0 card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="widget-content p-0">
                                <div class="widget-content-outer">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Net Sales This Month ({{ date('M') }} {{ date('Y') }})</div>
                                            <div class="widget-subheading"><strong style="color: #f80000; font-size:20px">Rp.{{ number_format($amount_net_sales_this_mount) }}</strong></div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-success"><small></small></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="widget-content p-0">
                                <div class="widget-content-outer">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Total Pending Transfer</div>
                                            <div class="widget-subheading"><strong style="color: #3f6ad8; font-size:20px">Rp.{{ number_format($amount_pending_transfer) }}</strong></div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-primary"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="pt-0 pb-0 card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="widget-content p-0">
                                <div class="widget-content-outer">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Net Sales Last Month ({{ date("M",strtotime("-1 month")) }} {{ date('Y') }})</div>
                                            <div class="widget-subheading"><strong style="color: #0048ff; font-size:20px">Rp.{{number_format($amount_net_sales_last_mount)}}</strong></div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-danger"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="widget-content p-0">
                                <div class="widget-content-outer">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Total Technician</div>
                                            <div class="widget-subheading">
                                                @if (strlen($totalteknisi) < 2) <strong style="color: #ed7200; font-size:20px">0{{ $totalteknisi }}</strong>
                                                    @else
                                                    <strong style="color: #ed7200; font-size:20px">{{ $totalteknisi }}</strong>
                                                    @endif

                                            </div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-warning"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="pt-0 pb-0 card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div class="widget-content p-0">
                                <div class="widget-content-outer">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Completed Transactions</div>
                                            <div class="widget-subheading"><strong style="color: #2600af; font-size:20px"> Rp.{{ number_format($amount_order_complete) }}</strong></div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-success"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="widget-content p-0">
                                <div class="widget-content-outer">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left">
                                            <div class="widget-heading">Total Customer</div>
                                            <div class="widget-subheading">
                                                @if (strlen($totalCustomer) < 2) <strong style="color: #ed7200; font-size:20px">0{{ $totalCustomer }}</strong>
                                                    @else
                                                    <strong style="color: #ed7200; font-size:20px">{{ $totalCustomer }}</strong>
                                                    @endif
                                            </div>
                                        </div>
                                        <div class="widget-content-right">
                                            <div class="widget-numbers text-primary"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-bottom: 20px;">
    <div class="card">
        <div class="header-border card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                List Technicians
            </div>
            <div class="btn-actions-pane-right text-capitalize">

            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="example" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th style="width:500px">Teknisi</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($showTeknisi as $teknisi)
                    @if (!empty($teknisi->info))
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $teknisi->name }}</td>
                        @if ($teknisi->status == 7)
                        <td><span class="badge badge-warning btn sm"><i class="fa fa-spinner"></i> Waiting to Approve</span></td>
                        @elseif($teknisi->status == 8)
                        <td><span class="badge badge-danger btn sm"><i class="fa fa-spinner"></i> Reject</span></td>
                        @elseif($teknisi->status == 1)
                        <td><span class="badge badge-primary btn sm"><i class="fa fa-spinner"></i> Success</span></td>
                        @endif
                        <td>
                            <button class="updatestatus btn btn-success btn-sm btn-white" data-id="{{ $teknisi->id }}"><i class='fa fa-check'></i>&nbsp; Approve</button>
                            <a href="{{ url('/admin/teknisi/detail/'.$teknisi->id) }}" class="btn btn-primary btn-sm btn-white"><i class='fa fa-eye'></i>&nbsp; Detail</a>
                        </td>
                    </tr>
                    @endif

                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                ABANDONED ORDER LIST
            </div>
        </div>

        <div class="card-body">
            <div class="tab-pane show active" id="tab-1" role="tabpanel">
                <table id="table-list-orders-abandoned" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Orders Code</th>
                            <th>Schedule</th>
                            <th>Customer</th>
                            <th>Technician</th>
                            <th>Payment Type</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="card-footer">

        </div>
    </div>
</div>
@if(count($tickets) > 0)
<div class="col-md-12 col-xl-12" style="margin-bottom: 10px;">
    <div class="card">
        <div class="header-border card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Ticket
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 col-xl-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="text-nowrap table-lg mb-0 table table-hover">
                    <tbody>
                        @foreach($tickets as $ticket)
                        <tr>
                            <td>
                                <div class="widget-content p-0">
                                    <div class="widget-content-wrapper">
                                        <div class="widget-content-left mr-3">
                                            <img width="42" class="rounded-circle" src="{{ $ticket->user->avatar }}" alt="avatar">
                                        </div>
                                        <div class="widget-content-left">
                                            <div class="widget-heading">{{ $ticket->user->full_name }}</div>
                                            <div class="widget-subheading">{{ $ticket->created_at->diffForHumans() }}</div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="text-left">{{ $ticket->subject }}</td>
                            <td>
                                {!! $ticket->getStatusHtmlAttribute() !!}
                            </td>
                            <td class="text-right">
                                <a href='{{ url("/admin/tickets/" . $ticket->id) }}' class='btn btn-search btn-sm btn-danger'><i class='fa fa-arrow-right'></i> Detail</a>
                                @if ($ticket->order_id == null)
                                <a href='{{ url("/admin/transactions/create/" . $ticket->id) }}' class='btn btn-search btn-sm btn-dark'><i class='fa fa-plus'></i> Create Order</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif

<button class="btn btn-danger push-now" type="button" style="display: none;">PUSH!</button>
<form id="form-push-notif">
    <div class="modal fade" id="modal-push-notif" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Push Notif</h4>
                </div>
                <div class="modal-body">
                    <label class="label-control">Title</label>
                    <div class="form-group">
                        <input name="title" placeholder="title" type="text" class="form-control push-title" required>
                    </div>
                    <label class="label-control">Body</label>
                    <div class="form-group">
                        <input name="body" placeholder="body" type="text" class="form-control push-body" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm waves-effect btn-clus" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    <button type="submit" class="btn btn-sm btn-oke waves-effect"><i class="fa fa-plus"></i> Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        var table = $('#example').DataTable();
    });

    // $(document).ready(function() {
    var tableOrder = $('#table-list-orders-abandoned').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        scrollX: true,
        ordering: 'true',
        order: [1, 'desc'],
        responsive: true,
        language: {
            buttons: {
                colvis: '<i class="fa fa-list-ul"></i>'
            },
            search: '',
            searchPlaceholder: "Search...",
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        buttons: [{
                extend: 'colvis'
            },
            {
                text: '<i class="fa fa-refresh"></i>',
                action: function(e, dt, node, config) {
                    dt.ajax.reload();
                }
            }
        ],
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/order/datatables/search?abandoned_orders=true'),
            type: 'get',
        },
        columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                sortable: false,
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    link = "<a href='" + Helper.url('/admin/order/service_detail/' + full.id) + "'># " + full.code + "</a>";
                    if (full.garansi) {
                        link += '</br><span class="ml-auto badge badge-primary">Warranty Claim</span>';
                    }
                    if (full.admin_read_at == null) {
                        link += '</br><span class="ml-auto badge badge-warning">NEW</span>';
                    }

                    if (full.ticket != null) {
                        link += '</br><span class="ml-auto badge badge-success">Ticket</span>';
                    }

                    return link;
                }
            },
            {
                data: "schedule",
                name: "schedule",
                render: function(data, type, full) {
                    return moment(full.schedule).format("DD MMMM YYYY HH:mm");
                }
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    if (full.user == null) {
                        return "Deleted Users";
                    } else {
                        return full.user.name;
                    }
                }
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    if (full.service_detail.length) {
                        service_detail = full.service_detail[0];
                        return service_detail.technician.user.full_name;
                    } else {
                        return "Deleted Users";
                    }
                }
            },
            {
                data: "payment_type",
                name: "payment_type",
                render: function(data, type, full) {
                    status = full.payment_type == 0 ? '<div class="badge badge-danger ml-2">Offline</div>' : '<div class="badge badge-info ml-2">Online</div>';
                    return status;
                }
            },
            {
                data: "grand_total",
                name: "grand_total",
                render: $.fn.dataTable.render.number(',', '.,', 0, 'Rp. ', '.00')
            },
            {
                data: "order_status.name",
                name: "order_status.name",
                render: function(data, type, full) {
                    var text = '';
                    if (full.order_status.id === 3 && full.is_less_balance === 1) {
                        text = '<span class="badge badge-warning">' + data + '</span><br/> <span class="badge badge-danger">Less Balance</span>';
                    } else {
                        if (full.orders_statuses_id === 2) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 3) {
                            return '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 4) {
                            return '<span class="badge badge-primary">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 5) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 6) {
                            return '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 7) {
                            return '<span class="badge badge-focus">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 8) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 9) {
                            return '<span class="badge badge-primary">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 10) {
                            return '<span class="badge badge-success">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 11) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        }
                    }
                    return text;
                }
            },
            {
                data: "id",
                name: "id",
                orderable: false,
                searchable: false,
                render: function(data, type, full) {
                    action = '';

                    garansi_action = full.garansi == null ? '' : "<a class='btn-sm btn btn-success btn-hover-shine' href='" + Helper.url('/admin/garansi/' + full.garansi.id + '/detail') + "' title='claim'><i class='fa fa-envelope'></i></a>";

                    detail_action = "<a href='/admin/order/service_detail/" + full.id + "' class='btn-hover-shine btn btn-primary btn-sm' data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>";

                    delete_action = "<button class='btn-hover-shine delete btn btn-danger btn-sm' data-id='" + full.id + "' data-toggle='tooltip' data-html='true' title='Delete'><i class='fa fa-trash'></i></button>";

                    complain_action = "<a href='/admin/transacsion_list/complaint/detail/" + full.id + "' class='btn btn-primary btn-sm' data-toggle='tooltip' data-html='true' title='Complaint'><i class='fa fa-comment'></i></a";

                    action += detail_action + ' ' + garansi_action + ' ';

                    if (full.orders_statuses_id != null) {
                        if (full.order_status.id == 2) {
                            action += delete_action;
                        }

                        if (full.order_status.id == 11) {
                            action += complain_action;
                        }
                    }

                    return action;
                }
            }
        ]
    });

    $(document).on('click', '.updatestatus', function(e) {
        id = $(this).attr('data-id');
        Helper.confirm(function() {
            Helper.loadingStart();
            $.ajax({
                url: Helper.apiUrl('/user/status/' + id),
                type: 'put',
                dataType: 'JSON',
                contentType: 'application/json',

                success: function(res) {
                    Helper.successNotif('This Technician Has Approved');
                    Helper.loadingStop();
                    Helper.redirectTo('/admin/home');
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        }, {
            title: "Are You Sure",
            message: "Are you sure you have checked the completeness of this technician's data ?",
        })

        e.preventDefault()
    })

    var table = $('#table-ticket')
        .DataTable({
            processing: true,
            serverSide: true,
            select: true,
            ordering: 'true',
            order: [1, 'desc'],
            responsive: true,
            language: {
                search: '',
                searchPlaceholder: "Search..."
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/tickets/datatables'),
                "type": "get"
            },
            columns: [{
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                    orderable: false,
                    searchable: false,
                    width: "10%"
                },
                {
                    data: "ticket_no",
                    name: "ticket_no",
                    render: function(data, type, full) {
                        return "<a href='/admin/tickets/" + full.id + "'>" + full.ticket_no + "</a>";
                    }
                },
                {
                    data: "subject",
                    name: "subject",
                },
                {
                    data: "user.name",
                    name: "user.name",
                    orderable: false,
                    searchable: false,
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        if (row.order_id == null) {
                            return "<button type='button' class='btn btn-drak'><i style='font-size: 24px;' class='fa fa-square'></i></button>";
                        } else {
                            return "<button type='button' class='btn btn-drak'><i class='fa fa-check-square' style='font-size: 24px;'></i></button>";
                        }
                    }
                },
                {
                    data: "status",
                    name: "status",
                    render: function(data, type, full) {
                        switch (full.status) {
                            case '1':
                                return '<span class="btn btn-sm btn-primary">open</span>';
                                break;

                            case '2':
                                return '<span class="btn btn-sm btn-info">on progress</span>';
                                break;

                            case '3':
                                return '<span class="btn btn-sm btn-dark">on resolving</span>';
                                break;

                            case '4':
                                return '<span class="btn btn-sm btn-success">done</span> ';
                                break;

                            case '5':
                                return '<span class="btn btn-sm btn-danger">close</span> ';
                                break;

                            default:
                                return full.status;
                                break;
                        }
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        detail_link = "<a href='/admin/tickets/" + full.id + "' class='btn btn-search btn-sm btn-danger'><i class='fa fa-arrow-right'></i> Detail</a>";
                        order_link = '';
                        if (full.order_id == null) {
                            order_link = "<a href='/admin/transactions/create/" + full.id + "' class='btn btn-search btn-sm btn-dark'><i class='fa fa-plus'></i> Create Order</a>";
                        }


                        return detail_link + order_link;
                    }
                }
            ]
        });
</script>

@include('admin.template._firebase_config')
<script>
    $('.push-now').click(function() {
        $('#modal-push-notif').modal('show');
    })

    $('#form-push-notif').submit(function(e) {
        $.ajax({
            url: Helper.apiUrl('/firebase_send_push'),
            type: 'POST',
            data: {
                title: $('.push-title').val(),
                body: $('.push-body').val()
            },
            success: function(response) {
                alert('Ok!')
            },
            error: function(err) {
                console.log(" Can't do because: " + err);
            },
        });

        e.preventDefault();
    })
</script>
@endsection