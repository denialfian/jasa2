@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        {{ $title }}
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="btn btn-primary" href="{{ url('/admin/business-to-business-user-request-order/create') }}">Request Order</a>
                    </div>
                </div>
            </div>
                
        </div>
        <div class="card-body">
            <table id="table-btb" class="table table-hover table-bordered" style="width:100%; white-space: nowrap;">
                <thead>
                    <tr>
                        <th></th>
                        <th>DATE</th>
                        <th>REQUEST CODE</th>
                        <th>COMPANY NAME</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-body">
            {{-- <button class="btn btn-primary btn-sm add-btb"><i class="fa fa-plus"></i> Add New</button>
            <a href="{{ url('/admin/business_to_business/log') }}" class="btn btn-success btn-sm">View Logs</a> --}}
        </div>
    </div>
</div>
{{-- <form id="form-btb">
    <div class="modal fade" id="modal-btb" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Badge</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Company</label>
                        <select class="form-control comapany-select" id="company_id" name="company_id" style="width:100%" required>
                            <option value="">Select Company</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect"><i class="fa fa-plus"></i> Save</button>
                    <button type="button" class="btn btn-sm waves-effect btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
</form> --}}

<style>
    td.details-control {
        background: url('https://www.datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('https://www.datatables.net/examples/resources/details_close.png') no-repeat center center;
    }

    .custom-control-label-2{
        margin: 0;
    }
</style>
@endsection

@section('script')
<script>
    /* Formatting function for row details - modify as you need */
    function format ( d ) {
        console.log('data D = ' + d.get_history_order)
        template = '';
        template += '<table class="table" style="margin: 10px;">';
        template += '<tr>';
        template += '<td>Date</td>';
        template += '<th>Outlet Name</th>';
        template += '<th>Status Teknisi</th>';
        template += '<th>Status</th>';
        template += '</tr>';
        _.each(d.get_history_order, function(o, key) {
            console.log('o = ' + o)
            if(o.get_outlet !== null){
                outlet = o.get_outlet.name;
            }else{
                outlet = '-';
            }

            if(o.get_history_order !== null){
                date = moment(d.get_history_order.created_at).format("DD MMMM YYYY HH:mm");
            }else{
                date = '-';
            }

            if(o.get_schedule !== null){
                if(o.get_schedule.status == 'Tiba Ditempat'){
                    status_teknisi =  "<span class='badge badge-primary'>Tiba Ditempat</span>";
                }else if(o.get_schedule.status == 'Selesai' ){
                    status_teknisi =  "<span class='badge badge-success'>Selesai</span>";
                }else if(o.get_schedule.status == 'Penjadwalan Ulang' ){
                    status_teknisi =  "<span class='badge badge-danger'>Penjadwalan Ulang</span>";
                }else if(o.get_schedule.status == null ){
                    status_teknisi =  "<span class='badge badge-warning' style='color:white'>Menunggu</span>";
                }else{
                    status_teknisi =  '-' 
                }
            }else{
                status_teknisi =  "<span class='badge badge-danger'>Belum Ada Teknisi</span>";
            }  

            if(o.get_b2b_detail !== null){
                if(o.get_b2b_detail.status_quotation == "Requested"){
                    status_quot =  "<span class='badge badge-primary'>Requested</span>"
                }else if(o.get_b2b_detail.status_quotation == "Process"){
                    status_quot =  "<span class='badge badge-success'>Process</span>"
                }else if(o.get_b2b_detail.status_quotation == "Scheduling"){
                    status_quot =  "<span class='badge badge-danger'>Scheduling</span>"
                }else if(o.get_b2b_detail.status_quotation == "Finished"){
                    status_quot =  "<span class='badge badge-info'>Finished</span>"
                }else if(o.get_b2b_detail.status_quotation == "Waiting Payment"){
                    status_quot =  "<span class='badge badge-success'>Waiting Payment</span>"
                }else{
                    status_quot =  "<span class='badge badge-secondary'>-</span>"
                }
                // status_quot =  o.get_b2b_detail.status_quotation
            }else{
                status_quot =  "<span class='badge badge-secondary'>-</span>";
            }  

            template += '<tr style="">';
            template += '<td>' + date + '</td>';
            template += '<td>' + outlet + '</td>';
            template += '<td>' + status_teknisi + '</td>';
            template += '<td>' + status_quot + '</td>';
            template += '</tr>';
        })
        template += '</table>';
        return template;
    }

    // '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
    //         '<tr>'+
    //             '<td>Full name:</td>'+
    //             '<td>'+d.name+'</td>'+
    //         '</tr>'+
    //         '<tr>'+
    //             '<td>Extension number:</td>'+
    //             '<td>'+d.extn+'</td>'+
    //         '</tr>'+
    //         '<tr>'+
    //             '<td>Extra info:</td>'+
    //             '<td>And any further details here (images etc)...</td>'+
    //         '</tr>'+
    //     '</table>';
    
    $(document).ready(function() {
        var table = $('#table-btb').DataTable( {
            "ajax": Helper.apiUrl('/request-order-b-2-b/datatables'),
            "columns": [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "data":           null,
                    "defaultContent": ''
                },
                {
                    data: "created_at",
                    name: "created_at",
                    render: function(data, type, row) {
                        return moment(row.created_at).format("DD MMMM YYYY");
                    }
                },
                {
                    data: "request_code",
                    name: "request_code",
                    render: function(data, type, row) {
                        if(row.read_at == null ){
                            return "<a href='" + Helper.url('/admin/business-to-business-user-request-order/detail/' + row.request_code) + "'>" + row.request_code + "</a> - <span class='badge badge-success'>New</span>"
                        }else{
                            return "<a href='" + Helper.url('/admin/business-to-business-user-request-order/detail/' + row.request_code) + "'>" + row.request_code + "</a>"
                        }
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        if(row.get_user !== null){
                            return row.get_user.get_comp.name
                        }else{
                            return "Company Not Found";
                        }  
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        view_link = "<a class='btn btn-primary btn-sm' href='" + Helper.url('/admin/business-to-business-user-request-order/detail/' + row.request_code) + "'><i class='fa fa-eye'></i></a>";
                        // btn_delete_item = '<button type="button" data-id="' + row.request_code + '" class="btn btn-danger btn-sm delete-btb"><i class="fa fa-trash"></i></button>';
                        return view_link;
                    }
                },
            ],
        } );
        
        // Add event listener for opening and closing details
        $('#table-btb tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );
    
            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );
    } );
</script>

<script>
    globalCRUD
        .select2Static(".comapany-select", '/company/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })

    $('.add-btb').click(function() {
        $('#modal-btb').modal('show')
    })

    $('#form-btb').submit(function(e) {
        e.preventDefault();

        data = Helper.serializeForm($(this));

        Axios.post('/business_to_business', data)
            .then(function(response) {
                //send notif
                Helper.successNotif(response.data.msg);
                // redirect
                window.location.href = Helper.redirectUrl('/admin/business_to_business/' + response.data.data.id + '/show');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
    })


    // $(document).ready(function() {
    //     var table = $('#table-btb').DataTable({
    //         processing: true,
    //         serverSide: true,
    //         select: false,
    //         dom: 'Bflrtip',
    //         responsive: true,
    //         order: [1, 'asc'],
    //         language: {
    //             buttons: {
    //                 colvis: '<i class="fa fa-list-ul"></i>'
    //             },
    //             search: '',
    //             searchPlaceholder: "Search...",
    //             processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
    //         },
    //         oLanguage: {
    //             sLengthMenu: "_MENU_",
    //         },
    //         buttons: [{
    //                 extend: 'colvis'
    //             },
    //             {
    //                 text: '<i class="fa fa-refresh"></i>',
    //                 action: function(e, dt, node, config) {
    //                     dt.ajax.reload();
    //                 }
    //             }
    //         ],
    //         dom: "<'row'<'col-sm-6'Bl><'col-sm-6'>>" +
    //             "<'row'<'col-sm-12'tr>>" +
    //             "<'row'<'col-sm-6'i><'col-sm-6'p>>",
    //         ajax: {
    //             url: Helper.apiUrl('/request-order-b-2-b/datatables'),
    //             "type": "get",
    //         },
    //         // columnDefs: [
    //         //     {
    //         //         "targets": [ 1 ],
    //         //         "visible": false,
    //         //         "searchable": false
    //         //     },
    //         // ]
    //         columns: [
    //             // {
    //             //     data: "DT_RowIndex",
    //             //     name: "DT_RowIndex",
    //             //     sortable: false,
    //             //     searchable: false,
    //             //     width: "10%"
    //             // },
    //             {
    //                 data: "created_at",
    //                 name: "created_at",
    //                 render: function(data, type, row) {
    //                     return moment(row.created_at).format("DD MMMM YYYY");
    //                 }
    //             },
    //             {
    //                 data: "request_code",
    //                 name: "request_code",
    //                 render: function(data, type, row) {
    //                     if(row.read_at == null ){
    //                         return "<a href='" + Helper.url('/admin/business-to-business-user-request-order/detail/' + row.request_code) + "'>" + row.request_code + "</a> - <span class='badge badge-success'>New</span>"
    //                     }else{
    //                         return "<a href='" + Helper.url('/admin/business-to-business-user-request-order/detail/' + row.request_code) + "'>" + row.request_code + "</a>"
    //                     }
    //                 }
    //             },
    //             {
    //                 data: "id",
    //                 name: "id",
    //                 render: function(data, type, row) {
    //                     if(row.get_user !== null){
    //                         return row.get_user.get_comp.name
    //                     }else{
    //                         return "Company Not Found";
    //                     }  
    //                 }
    //             },
    //             {
    //                 data: "id",
    //                 name: "id",
    //                 render: function(data, type, row) {
    //                     view_link = "<a class='btn btn-primary btn-sm' href='" + Helper.url('/admin/business-to-business-user-request-order/detail/' + row.request_code) + "'><i class='fa fa-eye'></i></a>";
    //                     // btn_delete_item = '<button type="button" data-id="' + row.request_code + '" class="btn btn-danger btn-sm delete-btb"><i class="fa fa-trash"></i></button>';
    //                     return view_link;
    //                 }
    //             },
    //         ],
    //     });

    //     $('#table-btb tbody').on('click', 'td.details-control', function() {
    //         var tr = $(this).closest('tr');
    //         var row = table.row(tr);

    //         if (row.child.isShown()) {
    //             // This row is already open - close it
    //             row.child.hide();
    //             tr.removeClass('shown');
    //         } else {
    //             // Open this row
    //             row.child(formatx(row.data())).show();
    //             tr.addClass('shown');
    //         }
    //     });

    //     $('.btn-btb-search').click(function() {
    //         company_id = $('.comapany-select').val();
    //         playload = '?company_id=' + company_id + '&outlet_name=' + $('.outlet-name').val();

    //         url = Helper.apiUrl('/business_to_business') + playload;

    //         table.ajax.url(url).load();
    //     })

    //     $(document).on('click', '.delete-btb', function() {
    //         var id = $(this).attr('data-id');

    //         Helper.confirmDelete(function() {
    //             Helper.loadingStart();
    //             Axios.delete('/user-b-2-b/destroy-request-order/' + id)
    //                 .then(function(response) {
    //                     //send notif
    //                     Helper.successNotif(response.data.msg);
    //                     // refresh
    //                     table.ajax.reload();
    //                 })
    //                 .catch(function(error) {
    //                     Helper.handleErrorResponse(error)
    //                 });
    //         })
    //     })

    // });

 
</script>
@endsection