@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <input type="hidden" name="getIds" id="getIds" value="{{ $getId->request_code }}">
        <div class="card-body">
            <table id="table-request-order-detail" class="table table-hover table-bordered" style="width:100%; white-space: nowrap;">
                <thead>
                    <tr>
                        {{-- <th>NO</th> --}}
                        <th>DATE</th>
                        <th>Outlet</th>
                        <th>Service Type</th>
                        <th>Unit Ke</th>
                        <th>Remark</th>
                        <th>Status Teknisi</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-body">
            {{-- <button class="btn btn-primary btn-sm add-btb"><i class="fa fa-plus"></i> Add New</button>
            <a href="{{ url('/admin/business_to_business/log') }}" class="btn btn-success btn-sm">View Logs</a> --}}
        </div>
    </div>
</div>
{{-- <form id="form-btb">
    <div class="modal fade" id="modal-btb" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Badge</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Company</label>
                        <select class="form-control comapany-select" id="company_id" name="company_id" style="width:100%" required>
                            <option value="">Select Company</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm btn-primary waves-effect"><i class="fa fa-plus"></i> Save</button>
                    <button type="button" class="btn btn-sm waves-effect btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
</form> --}}

<style>
    .fixed-sidebar .app-main .app-main__outer {
        width: 100%;
    }

    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>
@endsection

@section('script')
<script>
    globalCRUD
        .select2Static(".comapany-select", '/company/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })

    $('.add-btb').click(function() {
        $('#modal-btb').modal('show')
    })

    $('#form-btb').submit(function(e) {
        e.preventDefault();

        data = Helper.serializeForm($(this));

        Axios.post('/business_to_business', data)
            .then(function(response) {
                //send notif
                Helper.successNotif(response.data.msg);
                // redirect
                window.location.href = Helper.redirectUrl('/admin/business_to_business/' + response.data.data.id + '/show');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
    })


    $(document).ready(function() {
        var table = $('#table-request-order-detail').DataTable({
            processing: true,
            serverSide: true,
            select: false,
            dom: 'Bflrtip',
            responsive: true,
            order: [1, 'asc'],
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/request-order-b-2-b/datatables-transaction-detail/' + $('#getIds').val()),
                "type": "get",
            },
            // columnDefs: [
            //     {
            //         "targets": [ 1 ],
            //         "visible": false,
            //         "searchable": false
            //     },
            // ]
            columns: [
                // {
                //     data: "DT_RowIndex",
                //     name: "DT_RowIndex",
                //     sortable: false,
                //     searchable: false,
                //     width: "10%"
                // },
                {
                    data: "created_at",
                    name: "created_at",
                    render: function(data, type, row) {
                        return moment(row.created_at).format("DD MMMM YYYY");
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        if(row.get_outlet !== null){
                            return "<a href='" + Helper.url('/admin/business-to-business-user-request-order/detail-request-order/' + row.id) + "'>" + row.get_outlet.name + "</a>"
                        }else{
                            return "Outlet Not Found";
                        }
                    }
                },
                {
                    data: "service_type",
                    name: "service_type",
                    render: function(data, type, row) {
                        if(row.service_type !== null){
                            return row.service_type
                        }else{
                            return "Service Type Not Found";
                        }  
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        if(row.get_b2b_detail !== null){
                            return row.get_b2b_detail.unit_ke
                        }else{
                            return "Service Type Not Found";
                        }  
                    }
                },
                {
                    data: "remark",
                    name: "remark",
                    render: function(data, type, row) {
                        if(row.remark !== null){
                            return row.remark
                        }else{
                            return "Remark Type Not Found";
                        }  
                    }
                },

                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        if(row.get_schedule !== null){
                            if(row.get_schedule.status == 'Tiba Ditempat'){
                                return "<span class='badge badge-primary'>Tiba Ditempat</span>";
                            }else if(row.get_schedule.status == 'Selesai' ){
                                return "<span class='badge badge-success'>Selesai</span>";
                            }else if(row.get_schedule.status == 'Penjadwalan Ulang' ){
                                return "<span class='badge badge-danger'>Penjadwalan Ulang</span>";
                            }else if(row.get_schedule.status == null ){
                                return "<span class='badge badge-warning' style='color:white'>Menunggu</span>";
                            }else{
                               return '-' 
                            }
                        }else{
                            return "<span class='badge badge-danger'>Belum Ada Teknisi</span>";
                        }  
                    }
                },
                
                {
                    data: "status",
                    name: "status",
                    render: function(data, type, row) {
                        if(row.get_b2b_detail !== null){
                            if(row.get_b2b_detail.status_quotation == "Requested"){
                                return "<span class='badge badge-primary'>Requested</span>"
                            }else if(row.get_b2b_detail.status_quotation == "Process"){
                                return "<span class='badge badge-success'>Process</span>"
                            }else if(row.get_b2b_detail.status_quotation == "Scheduling"){
                                return "<span class='badge badge-danger'>Scheduling</span>"
                            }else if(row.get_b2b_detail.status_quotation == "Finished"){
                                return "<span class='badge badge-info'>Finished</span>"
                            }else if(row.get_b2b_detail.status_quotation == "Waiting Payment"){
                                return "<span class='badge badge-success'>Waiting Payment</span>"
                            }else{
                                return "<span class='badge badge-secondary'>-</span>"
                            }
                            // return row.get_b2b_detail.status_quotation
                        }else{
                            return "<span class='badge badge-secondary'>-</span>";
                        }  
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        // if(row.teknisi_id !== null){
                            view_link = "<a class='btn btn-primary btn-sm' href='" + Helper.url('/admin/business-to-business-user-request-order/detail-request-order/' + row.id) + "'><i class='fa fa-eye'></i></a>";
                        // }else{
                        //     view_link = "<a class='btn btn-primary btn-sm' href='" + Helper.url('/admin/business-to-business-user-request-order/assign-teknisi/' + row.id) + "'><i class='fa fa-eye'></i></a>";
                        // }
                        // btn_delete_item = '<button type="button" data-id="' + row.id + '" class="btn btn-danger btn-sm delete-btb"><i class="fa fa-trash"></i></button>';
                        return view_link;
                    }
                },
            ],
        });

        $('#table-request-order-detail tbody').on('click', 'td.details-control', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(formatx(row.data())).show();
                tr.addClass('shown');
            }
        });

        $('.btn-btb-search').click(function() {
            company_id = $('.comapany-select').val();
            playload = '?company_id=' + company_id + '&outlet_name=' + $('.outlet-name').val();

            url = Helper.apiUrl('/business_to_business') + playload;

            table.ajax.url(url).load();
        })

        $(document).on('click', '.delete-btb', function() {
            var id = $(this).attr('data-id');

            Helper.confirmDelete(function() {
                Helper.loadingStart();
                Axios.delete('/user-b-2-b/destroy-request-order/' + id)
                    .then(function(response) {
                        //send notif
                        Helper.successNotif(response.data.msg);
                        // refresh
                        table.ajax.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
            })
        })

    });

 
</script>
@endsection