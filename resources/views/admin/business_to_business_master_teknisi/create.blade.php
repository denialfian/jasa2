@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <!-- <button class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Add New Grade</button> -->
            </div>
        </div>
        <form action="{{ url('/admin/business-to-business-master-teknisi/store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" name="name" placeholder="Full Name" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" name="email" placeholder="Email" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Nomor Ponsel</label>
                                <input type="text" name="phone" placeholder="Nomor Ponsel" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Profile</label>
                                <input type="file" name="image_teknisi" placeholder="Profile" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="label-control">Password</label>
                            <div class="form-group">
                                <input name="password" placeholder="password" type="password" class="form-control" required/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="label-control">Password Confirmation</label>
                            <div class="form-group">
                                <input name="password_confirmation" placeholder="password confirmation" type="password" class="form-control" required/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label class="label-control">Address</label>
                            <div class="form-group">
                                <textarea name="address" class="form-control" id="" cols="30" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Tambah Teknisi</button>
            </div>
        </form>
    </div>
</div>

@endsection

@section('script')
@endsection
