@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/excel-income/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;" autocomplete="off">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Title</label>
                    <div class="col-sm-12">
                        <input name="title" placeholder="Title" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Code</label>
                    <div class="col-sm-12">
                        <input name="code" placeholder="Title" type="text" class="form-control" value="{{ $code }}" readonly>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Description</label>
                    <div class="col-sm-12">
                        <textarea name="desc" class="form-control desc" data-uniq="xxx" maxlength="300" rows="4" placeholder="Description" ></textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                Details
                <div class="btn-actions-pane-right text-right">
                    <button type="button" class="btn btn-success btn_add_details"><i class="fa fa-plus"></i></button>
                </div>
            </div>

            <div class="card-body">
                <div class="row dynamic_details">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header header-border">
                                <div class="btn-actions-pane-right text-right">
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-12 col-form-label">Date Transaction</label>
                                    <div class="col-sm-12">
                                        <input name="date_transaction[]" type="text" class="form-control date_transaction_input-xxx date_format" data-uniq="xxx" required="required">
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-12 col-form-label">Source</label>
                                    <div class="col-sm-12">
                                        <input name="source[]" placeholder="" type="text" class="form-control source_input-xxx" data-uniq="xxx" required="required">
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-12 col-form-label">Description</label>
                                    <div class="description-area-xxx col-md-12">
                                        <div class="input-group">
                                            <div class="input-group-append col-md-12">
                                                <input name="description_0[]" placeholder="" type="text" class="form-control description-uniq-xxx-xxx" data-uniq="xxx" required="required">
                                                <button data-uniq="xxx" class="btn btn-sm btn-success btn-add-description-xxx btn-add-description" type="button"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-12 col-form-label">Date Transfer</label>
                                    <div class="col-sm-12">
                                        <input name="date_transfer[]" type="text" class="form-control date_transfer_input-xxx date_format" data-uniq="xxx" required="required">
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-12 col-form-label">In</label>
                                    <div class="col-sm-12">
                                        <input name="in_nominal[]" placeholder="In" type="text" class="form-control currency in_nominal_input-xxx" data-uniq="xxx" required="required">
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-12 col-form-label">Refund</label>
                                    <div class="col-sm-12">
                                        <input name="refund[]" placeholder="Refund" type="text" class="form-control currency refund_input-xxx" data-uniq="xxx" required="required">
                                    </div>
                                </div>
                                <div class="position-relative row form-group">
                                    <label for="exampleEmail" class="col-sm-12 col-form-label">Branch</label>
                                    <div class="col-sm-12">
                                        <input name="branch[]" placeholder="" type="text" class="form-control branch_input-xxx" data-uniq="xxx" required="required">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                Ending Total
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <label for="exampleEmail" class="col-sm-12 col-form-label">Total In : <span id="total_in_nominal"></span></label>
                   </div>
                    <div class="col-md-4 text-center">
                        <label for="exampleEmail" class="col-sm-12 col-form-label">Total Refund : <span id="total_refund"></span></label>

                    </div>
                    <div class="col-md-4 text-center">
                        <label for="exampleEmail" class="col-sm-12 col-form-label">Grand Total : <span id="grand_total"></span></label>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-footer ">
            <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> CREATE </button>
        </div>
    </div>
</form>

@endsection

@section('script')
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
@include('admin.master.excel-income._js')
<script>
    Helper.hideSidebar();
    Helper.wysiwygEditor('.desc');

    Helper.currency('.currency');

    $(function() {

        Helper.dateFormat('.date_format');
        var xx = 1;
        var zz = 0;

        $(document).on('click', '.btn_add_details', function() {
            ClassApp.templateNewInputDetailData(xx,zz)
            xx++;
        });

        // $(document).on('click', '.btn_add_description', function() {
        //     ClassApp.templateNewInputDetailData(xx)
        //     xx++;
        // })
        // delete details
        $(document).on('click', '.btn_remove_details', function() {
            var button_id = $(this).attr("data-uniq");
            $('#details_row_' + button_id + '').remove();
        });

    })

    // delete details row
    $(document).on('click', '.btn_remove_details', function() {
        var button_id = $(this).attr("data-uniq");
        $('#details_row_' + button_id + '').remove();
        // ClassApp.hitungBalance();
    });

    $(document).on('keyup', 'input[name="in_nominal[]"]', function() {
        ClassApp.hitungTotal();
        Helper.currency('.currency');

    });

     $(document).on('keyup', 'input[name="refund[]"]', function() {
        ClassApp.hitungTotal();
        Helper.currency('.currency');
    });


    $("#form-data").submit(function(e) {
        ClassApp.saveData($(this),e);
    });

    $(document).on('click', '.btn-delete-detail', function() {
        uniq = $(this).attr('data-uniq');
        ClassApp.detailDelete(uniq);
    })



    // hide vendor
    $(document).on('click', '.btn-hide-detail', function() {
        uniq = $(this).attr('data-uniq');
        statusHide = $(this).attr('data-hide');

        if (statusHide == 0) {
            status = 'show';
            $(this).attr('data-hide', 1).text('hide');
        } else {
            status = 'hide';
            $(this).attr('data-hide', 0).text('show');
        }
        ClassApp.detailHide(uniq, status);
    })
</script>

@endsection
