@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/excel-income/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;" autocomplete="off">
    <input type="hidden" id="is_update" value=1>
    <input type="hidden" id="count_details" name="count_details">
    <input type="hidden" name="id" value="{{ $getData->id }}">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Title</label>
                    <div class="col-sm-12">
                        <input name="title" placeholder="Title" type="text" value="{{ $getData->title }}" class="form-control" required>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Code</label>
                    <div class="col-sm-12">
                        <input name="code" placeholder="Title" type="text" value="{{ $getData->code }}" class="form-control" readonly>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Description</label>
                    <div class="col-sm-12">
                        <textarea name="desc" class="form-control desc" maxlength="300" rows="4" placeholder="Description" >{{ $getData->description }}</textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                Details
                <div class="btn-actions-pane-right text-right">
                    <button type="button" class="btn btn-success btn_add_details"><i class="fa fa-plus"></i></button>
                </div>
            </div>

            <div class="card-body">
                <div class="row dynamic_details">
                    <input type="hidden" value="{{ count($detailsData) }}" id="count_details_data" />
                    @foreach($detailsData as $key => $details)
                    <?php $uniq = $key+1; $uniqid = uniqid(); ?>
                        <div class="col-md-6 detail-uniq-{{ $uniq }}-{{ $uniqid }}" id="details_row_{{ $uniq }}-{{ $uniqid }}"  style="amrgin-bottom:10px;">
                            <div class="card">
                                <div class="card-header header-border">
                                    <div class="btn-actions-pane-right text-right">
                                        @if($key != 0)
                                            <div role="group" class="btn-group-sm btn-group">
                                                <button data-uniq="{{ $uniq }}" data-id="{{ $details->id }}" class="btn btn-danger btn-sm btn-delete-detail btn-delete-detail-uniq-{{ $uniq }}" type="button">
                                                <i class="fa fa-remove"></i>Delete</button>
                                                <button data-uniq="{{ $uniq }}-{{ $uniqid }}" data-hide="1" class="btn btn-sm btn-white btn-warning btn-hide-detail btn-hide-detail-uniq-{{ $uniq }}" type="button">Hide</button>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="card-body detail-body-uniq-{{ $uniq }}-{{ $uniqid }}">
                                    <input type="hidden" value="{{ $details->id }}" name="details_id[]" />
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">Date Transaction</label>
                                        <div class="col-sm-12">
                                            <input name="date_transaction[]" type="text" class="form-control date_transaction_input-{{ $uniq }} date_format" data-uniq="{{ $uniq }}" value="{{ $details->date_transaction->format('d-m-Y') }}" required>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">Source</label>
                                        <div class="col-sm-12">
                                            <input name="source[]" placeholder="" type="text" class="form-control source_input-{{ $uniq }}" data-uniq="{{ $uniq }}" value="{{ $details->source }}" required>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">Description</label>
                                        <div class="description-area-{{ $details->id }} col-md-12">
                                            @foreach($details->details_desc as $key => $desc)
                                            <?php $uniqid = uniqid(); ?>
                                            <input type="hidden" value="{{ $desc->id }}" name="details_desc_id_{{ $details->id }}[]" />
                                                <div class="input-group">
                                                    <div class="input-group-append col-md-12">
                                                        <input name="description_{{ $details->id }}[]" placeholder="" type="text" class="form-control details_desc description-uniq-{{ $details->id }}-{{ $desc->id }}_{{ uniqid() }}" data-uniq="{{ $desc->id }}" value="{{ $desc->description }}" required="required">
                                                        @if($key == 0)
                                                            <button data-uniq="{{ $details->id }}" class="btn btn-sm btn-success btn-add-description-{{ $details->id }} btn-add-description" type="button"><i class="fa fa-plus"></i></button>
                                                        @else
                                                            <button data-uniq="{{ $details->id }}" data-id ="{{ $desc->id }}" data-uniq-description="{{ $desc->id }}" class="btn btn-sm btn-danger btn-delete-description-uniq-{{ $desc->id }} btn-delete-description" type="button"><i class="fa fa-trash-o"></i></button>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">Date Transfer</label>
                                        <div class="col-sm-12">
                                            <input name="date_transfer[]" type="text" class="form-control date_transfer_input-{{ $uniq }} date_format" data-uniq="{{ $uniq }}" value="{{ $details->date_transfer->format('d-m-Y') }}" required>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">In</label>
                                        <div class="col-sm-12">
                                            <input name="in_nominal[]" placeholder="In" type="text" class="form-control currency in_nominal_input-{{ $uniq }}" data-uniq="{{ $uniq }}" value="{{ $details->in_nominal }}" required>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">Refund</label>
                                        <div class="col-sm-12">
                                            <input name="refund[]" placeholder="Refund" type="text" class="form-control currency refund_input-{{ $uniq }}" data-uniq="{{ $uniq }}" value="{{ $details->refund }}" required>
                                        </div>
                                    </div>
                                    <div class="position-relative row form-group">
                                        <label for="exampleEmail" class="col-sm-12 col-form-label">Branch</label>
                                        <div class="col-sm-12">
                                            <input name="branch[]" placeholder="" type="text" class="form-control branch_input-{{ $uniq }}" data-uniq="{{ $uniq }}" value="{{ $details->branch }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-header header-border">
                Ending Total
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 text-center">
                        <label for="exampleEmail" class="col-sm-12 col-form-label">Total In : <span id="total_in_nominal">Rp. 0</span></label>
                   </div>
                    <div class="col-md-4 text-center">
                        <label for="exampleEmail" class="col-sm-12 col-form-label">Total Refund : <span id="total_refund">Rp. 0</span></label>

                    </div>
                    <div class="col-md-4 text-center">
                        <label for="exampleEmail" class="col-sm-12 col-form-label">Grand Total : <span id="grand_total">Rp. 0</span></label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-footer ">
            <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> SAVE </button>
        </div>
    </div>
</form>



<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Excel Income Logs
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="excel-income-logs" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Type Modified</th>
                        <th>By User (Name)</th>
                        <th>Created At</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


@endsection

@section('script')
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
@include('admin.master.excel-income._js')
<script>

    Helper.hideSidebar();
    ClassApp.hitungTotal();
    var id = $('input[name="id"]').val();
    var xx = 1;
    var zz = 0;
    $(function() {
        Helper.currency('.currency');
        Helper.dateFormat('.date_format');

        $(document).on('click', '.btn_add_details', function() {
            ClassApp.templateNewInputDetailData(xx,zz)
            xx++;
            $('#count_details').val(parseInt(xx-1));
        });

    })


    $(document).on('keyup', 'input[name="in_nominal[]"]', function() {
        ClassApp.hitungTotal();
        Helper.currency('.currency');

    });

     $(document).on('keyup', 'input[name="refund[]"]', function() {
        ClassApp.hitungTotal();
        Helper.currency('.currency');
    });


    $("#form-data").submit(function(e) {
        ClassApp.updateData($(this),e,id);
    });

    $(document).on('click', '.btn-delete-detail', function() {
        var uniq = $(this).attr('data-uniq');
        var detail_id = $(this).attr('data-id');
        ClassApp.detailDelete(uniq,detail_id);
        xx = xx-1;
        $('#count_details').val(parseInt(xx));
    })



    // hide vendor
    $(document).on('click', '.btn-hide-detail', function() {
        uniq = $(this).attr('data-uniq');
        statusHide = $(this).attr('data-hide');

        if (statusHide == 0) {
            status = 'show';
            $(this).attr('data-hide', 1).text('hide');
        } else {
            status = 'hide';
            $(this).attr('data-hide', 0).text('show');
        }
        ClassApp.detailHide(uniq, status);
    })
    globalCRUD.datatables({
        url: '/excel-income/datatables_logs/'+id+'',
        selector: '#excel-income-logs',
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex"
        },
        {
            data: "type",
            name: "type",
            render: function(row,data,full) {
                if(row == 1) {
                    return 'Created';
                } else if(row == 2) {
                    return 'Updated'
                } else {
                    return 'Deleted';
                }
            }
        },
        {
            data: "user.name",
            name: "user.name",
        },
        {
            data: "created_at",
            name: "created_at",
            render: function(row,data,full) {
                return moment(row).format("DD MMMM YYYY HH:mm");
            }
        }
        ]
    })
</script>

@endsection
