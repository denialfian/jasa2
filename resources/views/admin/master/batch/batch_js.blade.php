<script>
    var TemplateObj = {
        newItemHeader: function(uniqid, hide_delete_btn = false) {
            template = '';
            template += '<div class="card-header-tab card-header" data-uniq="' + uniqid + '">';
            template += '<span>BATCH ITEM ' + uniqid+ '</span>';
            template += '<div class="btn-actions-pane-right">';
            template += '<div role="group" class="btn-group-sm btn-group">';
            if (hide_delete_btn == false) {
                template += '<button data-uniq="' + uniqid + '" class="btn btn-outline-danger btn-delete-item btn-delete-item-uniq-' + uniqid + '" type="button">';
                template += '<i class="fa fa-remove"></i> Delete';
                template += '</button>';
            }
            template += '</div>';
            template += '</div>';

            template += '</div>';

            return template;
        },
        newItemFieldVendor: function(uniqid) {
            template = '';
            template += '<div class="position-relative form-group">';
            template += '<label for="" class="">Vendor</label>';
            template += '<select name="vendor_id[]" data-uniq="' + uniqid + '" class="form-control vendor-select uniq-vendor-select-' + uniqid + '" style="width: 100%">';
            template += '<option value="" selected>Select Vendor</option>';
            template += '</select>';
            template += '</div>';
            return template;
        },
        newItemIsConsigment: function(uniqid) {
            template = '';
            template += '<div class="position-relative form-group">';
            template += '<label for="" class="">Consigment</label>';
            template += '<select name="is_consigment[]" data-uniq="' + uniqid + '" class="form-control is_consigment-select uniq-is_consigment-select-' + uniqid + '" style="width: 100%">';
            template += '<option value="1" selected>Yes</option>';
            template += '<option value="0">No</option>';
            template += '</select>';
            template += '</div>';
            return template;
        },
        newItemFieldProduct: function(uniqid) {
            template = '';
            template += '<div class="position-relative form-group">';
            template += '<label for="" class="">Product</label>';
            template += '<select name="product_id[]" data-uniq="' + uniqid + '" class="form-control product-select uniq-product-select-' + uniqid + '" style="width: 100%" required>';
            template += '<option value="" selected>Select Product</option>';
            template += '</select>';
            template += '</div>';
            return template;
        },
        newItemFieldQty: function() {
            template = '';
            template += '<div class="position-relative form-group">';
            template += '<label for="" class="">Quantity</label>';
            template += '<input name="quantity[]" id="quantity" placeholder="Quantity" type="number" class="form-control">';
            template += '</div>';
            return template;
        },
        newItemFieldValue: function(uniqid) {
            template = '';
            template += '<div class="position-relative form-group">';
            template += '<label for="" class="">Value</label>';
            template += '<input name="value[]" id="value-'+uniqid+'" placeholder="Value" type="text" class="form-control value">';
            template += '</div>';
            return template;
        },
        newItemFieldStatus: function() {
            template = '';
            template += '<div class="position-relative form-group">';
            template += '<label for="" class="">Status</label>';
            template += '<input name="status[]" id="status" placeholder="status" type="text" class="form-control">';
            template += '</div>';
            return template;
        },
        newItemFieldStatusSelect: function(uniqid) {
            template = '';
            template += '<div class="position-relative form-group">';
            template += '<label for="" class="">Status</label>';
            template += '<select name="batch_status_id[]" data-uniq="'+uniqid + '" class="form-control status-select uniq-status-select-' + uniqid + '" style="width: 100%">';
            template += '<option value="" selected>Select Status</option>';
            template += '</select>';
            template += '</div>';
            return template;
        },
        newItem: function(uniqid, hide_delete_btn = false) {
            template = '';
            // template += '<div class="col-md-12">';
            template += '<div class="batch-card-item main-card mb-3 card batch-card-'+uniqid+'">';
            template += this.newItemHeader(uniqid, hide_delete_btn);
            template += '<div class="card-body">';
            template += this.newItemFieldProduct(uniqid);
            template += this.newItemFieldVendor(uniqid);
            template += this.newItemIsConsigment(uniqid);
            template += '<div class="varian-area-' + uniqid + '"></div>';

            template += this.newItemFieldStatusSelect(uniqid);

            template += '<div class="form-row">';
            template += '<div class="col-md-4">';
            template += this.newItemFieldQty();
            template += '</div>';
            template += '<div class="col-md-4">';
            template += this.newItemFieldValue(uniqid);
            template += '</div>';
            template += '</div>';

            template += '<input type="hidden" name="product_vendor_id[]" value="0" class="product_vendor_id-' + uniqid + '">';
            template += '<input type="hidden" name="product_varian_id[]" value="0" class="product_varian_id-' + uniqid + '">';
            template += '<input type="hidden" name="batch_item_id[]" value="0">';
            template += '</div>';
            template += '</div>';
            // template += '</div>';
            return template;
        },
        attributeX: function(attributes) {
            template = '';
            _.each(attributes, function(attr) {
                template += '<fieldset class="position-relative form-group">';
                template += '<label for="" class="">' + attr.attribute.name + '</label>';

                _.each(attr.terms, function(prodVarian) {
                    combineClass = prodVarian.term.name + ',' + prodVarian.attribute.id;
                    template += '<div class="position-relative form-check">';
                    template += '<label class="form-check-label">';
                    template += '<input name="radio-' + prodVarian.attribute.id + '" type="radio" value="' + prodVarian.term.id + '" class="form-check-input term-radio" data-combine="' + combineClass + '" required> ' + prodVarian.term.name;
                    template += '</label>';
                    template += '</div>';
                });

                template += '</fieldset>';
            });
            return template;
        },
        attribute: function(attributes, uniq) {
            template = '';
            _.each(attributes, function(attr) {
                template += '<fieldset class="position-relative form-group">';
                template += '<label for="" class="">' + attr.attribute.name + '</label><br>';
                no = 0;
                _.each(attr.terms, function(prodVarian) {
                    classTerm = 'btn-attibute-' + prodVarian.attribute.id;
                    classActive = (no == 0) ? 'active' : '';
                    no++;
                    data_combine = prodVarian.product_varian_id + ' => ' + prodVarian.attribute.name + ',' + prodVarian.term.name;

                    template += '<button ';
                    template += ' type="button" ';
                    template += ' data-uniq="' + uniq + '" ';
                    template += ' data-attribute_id="' + prodVarian.attribute.id + '" ';
                    template += ' data-term_id="' + prodVarian.term.id + '" ';
                    template += ' class="' + classActive + ' btn btn-outline-primary ' + classTerm + ' btn-attribute-' + uniq + ' btn-uniq-id-' + prodVarian.attribute.id + '-' + uniq + '" ';
                    template += ' data-combine="' + data_combine + '">';
                    template += prodVarian.term.name;
                    template += '</button>';
                });

                template += '</fieldset>';
            });
            return template;
        }
    };

    var BatchObj = {
        templateNewItem: function(uniqid, hide_delete_btn = false) {
            $('#batch-item-area').append(TemplateObj.newItem(uniqid, hide_delete_btn));
            // load select2
            BatchObj.loadSelect2Product(uniqid);
        },


        createSelect2: function(selector, url, callback = null) {
            $(selector).select2({
                ajax: {
                    type: "GET",
                    url: url,
                    delay: 250,
                    cache: true,
                    placeholder: 'not selected',
                    minimumInputLength: 1,
                    data: function(params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function(data) {
                        if (callback == null) {
                            var res = $.map(data.data, function(item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            });
                        } else {
                            var res = $.map(data.data, callback);
                        }


                        return {
                            results: res
                        };
                    }
                }
            });
        },
        loadSelect2Vendor: function(product_id, uniqid, empty = true) {
            selector = '.uniq-vendor-select-' + uniqid;
            if (empty) {
                $(selector).empty();
            }

            url = '/product_search/vendors/' + product_id;
            this.createSelect2(selector, Helper.apiUrl(url), function(item) {
                return {
                    product_vendor_id: item.id,
                    text: item.vendor.name,
                    id: item.vendor.id
                }
            });
        },
        loadSelect2Product: function(selector) {
            this.createSelect2('.uniq-product-select-' + selector, Helper.apiUrl('/product/select2'))
        },
        loadSelect2StatusBatch: function(selector) {
            this.createSelect2(selector, Helper.apiUrl('/batch-status/select2'))
        },
        loadSelect2StatusBatchItem: function(uniq) {
            this.createSelect2('.uniq-status-select-' + uniq, Helper.apiUrl('/batch-status/select2'))
        },
        loadVarianAttribute: function(product_id, vendor_id, uniqid) {
            url = '/product_search/varians/' + product_id + '/' + vendor_id;
            $.ajax({
                url: Helper.apiUrl(url),
                type: 'GET',
                success: function(response) {
                    varians = response.data;
                    if (varians.length) {
                        template = TemplateObj.attribute(response.data, uniqid);
                        // append template pilih attribute
                        $('.varian-area-' + uniqid).html('').append(template);
                        // first selected
                        BatchObj.getSelectedAttribute(uniqid);
                    }
                    console.log('loadVarianAttribute', response);
                },
            });
        },


        findVarian: function(data, callbackSukses) {
            $.ajax({
                type: 'post',
                data: data,
                url: Helper.apiUrl('/product_search/varian'),
                success: function(resp) {
                    callbackSukses(resp);
                },
                error: function(xhr, status, error) {
                    Helper.errorMsgRequest(xhr, status, error);
                },
            });
        },
        getSelectedAttribute: function(uniq) {
            data = {
                vendor_id: $('.uniq-vendor-select-' + uniq).val(),
                product_id: $('.uniq-product-select-' + uniq).val(),
                attribute: [],
                term: [],
                combine: [],
            };

            $('.btn-attribute-' + uniq).each(function() {
                if ($(this).hasClass('active')) {
                    console.log('active');
                    attribute_id = $(this).attr('data-attribute_id');
                    term_id = $(this).attr('data-term_id');
                    combine = $(this).attr('data-combine');
                    data.attribute.push(attribute_id);
                    data.term.push(term_id);
                    data.combine.push(combine);
                }
            })

            BatchObj.findVarian(data, function(resp) {
                varian = resp.data;
                $('.product_varian_id-' + uniq).val(varian.id);
                $('.product_vendor_id-' + uniq).val(varian.product_vendor_id);
                console.log(varian)
            });
        },


        store: function(data) {
            Helper.loadingStart();
            Axios.post('/batch', data)
                .then(function(response) {
                    // send notif
                    Helper.successNotif(response.data.msg)
                        .redirectTo('/admin/batch/show');
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        },
        update: function(data) {
            Helper.loadingStart();
            Axios.put('/batch/' + data.id, data)
                .then(function(response) {
                    // send notif
                    Helper.successNotif(response.data.msg)
                        .redirectTo('/admin/batch/show');
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        },
        delete: function(id) {
            Helper.confirm(function() {
                Axios.delete('/batch/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('data berhasil didelete');
                        // reload
                        table.ajax.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
            })
        },


        // dataTables config
        datatablesConfig: function() {
            return {
                processing: true,
                serverSide: true,
                select: true,
                dom: 'Bflrtip',
                ordering:'true',
                order: [1, 'desc'],
                responsive: true,
                language: {
                    search: '',
                    searchPlaceholder: "Search..."
                },
                language: {
                    buttons: {
                        colvis: '<i class="fa fa-list-ul"></i>'
                    },
                    search: '',
                    searchPlaceholder: "Search...",
                    processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
                },
                oLanguage: {
                    sLengthMenu: "_MENU_",
                },
                dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                buttons: [
                    {
                        extend: 'colvis'
                    },
                    {
                        text: '<i class="fa fa-refresh"></i>',
                        action: function ( e, dt, node, config ) {
                            dt.ajax.reload();
                        }
                    }
                ],
                ajax: {
                    url: Helper.apiUrl('/batch/datatables'),
                    "type": "get"
                },
                columns: BatchObj.datatablesColumn(),
            }
        },
        // dataTables kolom
        datatablesColumn: function() {
            return [{
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                    sortable: false,
                    width: "10%"
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        return '<a href="'+Helper.url('/admin/batch/' +full.id+ '/show')+'">'+full.batch_no+'</a>';
                    }
                },
                {
                    data: "batch_date",
                    name: "batch_date",
                    render: function(data, type, full){
                        return moment(full.batch_date).format("DD MMMM YYYY");
                    }
                },
                {
                    data: "shipping_cost",
                    name: "shipping_cost",
                    render: function(data, type, full){
                        return Helper.thousandsSeparators(full.shipping_cost);
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        if (full.last_history != null) {
                            return '<button data-id="'+full.id+'" class="status-batch btn btn-sm btn-success"><i class="fa fa-history"></i> '+full.last_history.batch_status_shipment.name+'</button>';   
                        }

                        return '-';                      
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        if (full.is_confirmed == 0) {
                            edit_btn = full.last_history == null ? "<a href='/admin/batch/update/" + full.id + "'  class='edit-batch btn btn-warning btn-white btn-sm'><i class='fa fa-pencil'></i></a>" : '';
                            return edit_btn +
                            " <a href='/admin/batch/shipment/"+ full.id + "' class='btn btn-primary btn-sm'><i class='fa fa-arrow-right'></i></a>" +
                            " <a href='#'  data-id='" + full.id + "' class='delete-batch btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>";
                        }else{
                            return "<a href='/admin/batch/shipment/"+ full.id + "' class='btn btn-primary btn-sm'><i class='fa fa-arrow-right'></i> </a> <a href='#'  data-id='" + full.id + "' class='delete-batch btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>";
                        }
                    }
                }
            ];
        },


        // select 2 vendor
        initEdit: function() {
            // BatchObj.loadSelect2StatusBatch('.batch_status-select');

            $('.product-select')
                .each(function() {
                    uniq = $(this).attr('data-uniq');
                    BatchObj.loadSelect2Product(uniq)
                })

            $('.status-select')
                .each(function() {
                    uniq = $(this).attr('data-uniq');
                    BatchObj.loadSelect2StatusBatchItem(uniq)
                })

            $('.vendor-select')
                .each(function() {
                    uniq = $(this).attr('data-uniq');
                    product_id = $('.uniq-product-select-' + uniq).val();
                    BatchObj.loadSelect2Vendor(product_id, uniq, empty = false)
                })
        },
        initCreate: function() {
            BatchObj.loadSelect2StatusBatch('.batch_status-select');

            BatchObj.templateNewItem(0, hide_delete_btn = true);
            BatchObj.loadSelect2StatusBatchItem(0);
            Helper.currency('.value')
        }
    };
</script>

<script>
    var _item_counter = 1;
    Helper.thousandSeparatorMaskInput('#shipping_cost')
    Helper.thousandSeparatorMaskInput('#extra_cost')

    // click tambah item
    $('.btn-add-new-item')
        .click(function() {
            uniq = _item_counter;
            BatchObj.templateNewItem(uniq);
            BatchObj.loadSelect2StatusBatchItem(uniq);
            // update uniq counter
            _item_counter++;
            Helper.currency('.value')
            // Helper.currency('#value-'+uniq+'')
        })

    // event ganti produk
    $(document)
        .on('change', '.product-select', function() {
            uniq = $(this).attr('data-uniq');
            product_id = $(this).val();

            // kosongkan data
            $('.varian-area-' + uniq).html('');
            $('.product_vendor_id-' + uniq).val(0);
            $('.product_varian_id-' + uniq).val(0);

            // load data vendor
            BatchObj.loadSelect2Vendor(product_id, uniq);
        })

    // event ganti vendor
    $(document)
        .on('change', '.vendor-select', function() {
            uniq = $(this).attr('data-uniq');
            vendor_id = $(this).val();
            product_id = $('.uniq-product-select-' + uniq).val();
            product_vendor_id = $(this).select2('data')[0].product_vendor_id;
            $('.product_vendor_id-' + uniq).val(product_vendor_id);
            BatchObj.loadVarianAttribute(product_id, vendor_id, uniq);
        })

    // even attribute ganti
    $(document)
        .on('click', '.btn-outline-primary', function() {
            uniq = $(this).attr('data-uniq');
            att_id = $(this).attr('data-attribute_id');
            data_combine = $(this).attr('data-combine');
            $('.btn-uniq-id-' + att_id + '-' + uniq).removeClass('active');
            $(this).toggleClass('active');

            BatchObj.getSelectedAttribute(uniq);
        })
    // btn-delete-item
    $('.btn-delete-item')
    // even attribute ganti
    $(document)
        .on('click', '.btn-delete-item', function() {
            uniq = $(this).attr('data-uniq');
            total_item = $('.batch-card-item').length;
            if (total_item > 1) {
                $('.batch-card-' + uniq).remove();
            }else{
                Helper.errorNotif('cannot delete');
            }
        })
</script>
