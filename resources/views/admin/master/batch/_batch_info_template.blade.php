<div class="no-gutters row">
    <div class="col-md-12">
        <div class="pt-0 pb-0 card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-outer">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Date</div>
                                    <div class="widget-subheading">{{ $batch->batch_date->format('d-m-Y') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-outer">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Shipping cost</div>
                                    <div class="widget-subheading">Total Shipping Cost</div>
                                </div>
                                <div class="widget-content-right">
                                    <div class="widget-numbers text-danger">{{ \App\Helpers\formatCurrency($batch->shipping_cost, 0, '.', ',') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <div class="widget-content p-0">
                        <div class="widget-content-outer">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="widget-heading">Extra Cost</div>
                                    <div class="widget-subheading">Total Extra Cost</div>
                                </div>
                                <div class="widget-content-right">
                                    <div class="widget-numbers text-warning">{{ \App\Helpers\formatCurrency($batch->extra_cost, 0, '.', ',') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>