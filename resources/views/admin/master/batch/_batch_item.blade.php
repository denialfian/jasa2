@foreach($items as $item)
<input type="hidden" name="batch_item_id[]" value="{{ $item->id }}">
<div class="col-md-12">
    <div class="main-card mb-3 card">

        <div class="card-header-tab card-header" data-uniq="{{ $uniq }}">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                BATCH ITEM
            </div>
        </div>

        <div class="card-body">
            <div class="position-relative form-group">
                <label for="" class="">Product</label>
                <select name="product_id[]" data-uniq="{{ $uniq }}" class="form-control product-select uniq-product-select-{{ $uniq }}" style="width: 100%" required>
                    <option value="{{ $item->product_id }}" selected>{{ $item->product->name }}</option>
                </select>
            </div>

            <div class="position-relative form-group">
                <label for="" class="">Vendor</label>
                <select name="vendor_id[]" data-uniq="{{ $uniq }}" class="form-control vendor-select uniq-vendor-select-{{ $uniq }}" style="width: 100%" required>
                    <option value="{{ $item->productVendor->vendor->id }}" selected>{{ $item->productVendor->vendor->name }}</option>
                </select>
            </div>

            <div class="position-relative form-group">
                <label for="" class="">Consigment</label>
                <select name="is_consigment[]" data-uniq="{{ $uniq }}" class="form-control is_consigment-select uniq-is_consigment-select-{{ $uniq }}" style="width: 100%">
                    <option value="1" {{ $item->is_consigment == 1 ? 'selected' : '' }}>Iya</option>
                    <option value="0" {{ $item->is_consigment == 0 ? 'selected' : '' }}>Tidak</option>
                </select>
            </div>

            <div class="position-relative form-group">
                <label for="" class="">Status</label>
                <select name="batch_status_id[]" data-uniq="{{ $uniq }}" class="form-control status-select uniq-status-select-{{ $uniq }}" style="width: 100%">
                    <option value="{{ $item->status->id }}" selected>{{ $item->status->name }}</option>
                </select>
            </div>

            <div class="varian-area-{{ $uniq }}">
                @include('admin.master.batch._batch_item_attribute', [
                'uniq' => $uniq,
                'getAvailableAttribute' => $item->getAvailableAttribute()
                ])
            </div>
            <div class="form-row">
                <div class="col-md-4">
                    <div class="position-relative form-group">
                        <label for="" class="">Quantity</label>
                        <input name="quantity[]" id="quantity" placeholder="Quantity" type="number" value="{{ $item->quantity }}" class="form-control">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="position-relative form-group">
                        <label for="" class="">Value</label>
                        <input name="value[]" id="value" placeholder="value" type="text" value="{{ $item->value }}" class="form-control value">
                    </div>
                </div>
            </div>

            <input type="hidden" value="{{ $item->product_vendor_id }}" name="product_vendor_id[]" class="product_vendor_id-{{ $uniq }}">
            <input type="hidden" value="{{ $item->product_varian_id }}" name="product_varian_id[]" class="product_varian_id-{{ $uniq }}">
        </div>
    </div>
</div>
@endforeach

