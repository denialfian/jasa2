@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Customers</label>
                            <select name="customer_id" id="customer_id" class="form-control" style="width:100%;" required> </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Date</label>
                            <input name="order_date_range" id="order_date_range" type="text" class="form-control"/>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12" style="padding-top: 10px">
    <div class="card header-border">
        <div class="card-body">
            <table id="table-te" class="display table table-hover table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Total Orders</th>
                        <th>Money Spent</th>
                        <th>Last Order</th>

                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<div class="col-md-12" style="padding-top: 10px display:none;" id="data_total_orders">
    <div class="card header-border">
        <div class="card-body">
            <h3>Total Order from <span id="name_customer" class="badge badge-primary"></span></h3>
            <table id="table-total_order" class="display table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Orders Code</th>
                        <th>Schedule</th>
                        <th>Customer</th>
                        <th>Technician</th>
                        <th>Payment Type</th>
                        <th>Total</th>
                        <th>Status</th>

                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    var tbl_history = null;
    $(document).ready(function() {
        $('#data_total_orders').hide();
    })
    globalCRUD.select2("#customer_id", '/user/teknisi-select2');
    $('input[name="order_date_range"]').daterangepicker();
    // table
    var table = globalCRUD.datatables({
            url: '/admin/report/datatables_customer',
            selector: '#table-te',
            export: true,
            selectable: true,
            columnsField: [
                '',
                {
                    name: 'name',
                    data: 'name',
                    render: function(data,type,full) {
                        return '<a href="'+Helper.url('/admin/user/detail/'+full.id+'')+'">'+data+'</a>';
                    }
                },
                'email',
                {
                    name: 'orders_count',
                    data: 'orders_count',
                    render: function(data,type,full) {
                        if(data != 0) {
                            return "<button data-id='"+full.id+"' name-customer='"+full.full_name+"' type='button' class='total_order btn btn-success btn-sm'>"+data+"</button>";
                        } else {
                            return data;
                        }

                    }
                },
                'money_spent',
                {
                    name: 'last_order',
                    data: 'last_order',
                    render: function(data,type,full) {
                        if(full.last_order !== null) {
                            return '<a href="'+Helper.url('/admin/order/service_detail/'+full.last_order.id+'')+'" target="_blank">#'+full.last_order.code+'</a> - '+moment(full.last_order.created_at).format('DD-MMMM-YYYY')+'';
                        }else {
                            return '-';
                        }
                    }
                }
            ],
            actionLink: ''

        })

    // $('#table-te').on( 'select.dt', function ( e, dt, type, indexes ) {
    //     var data = dt.rows(indexes).data();
    //         console.log(data);
    // } );


    // search data
    $("#form-search").submit(function(e) {
        input = Helper.serializeForm($(this));
        playload = '?';
        _.each(input, function(val, key) {
            playload += key + '=' + val + '&'
        });
        playload = playload.substring(0, playload.length - 1);
        console.log(playload)

        url = Helper.apiUrl('/admin/report/datatables_customer/search' + playload);
        table.table.reloadTable(url);
        e.preventDefault();
    })

    $(document).on('click', '.total_order', function() {
        id = $(this).attr('data-id');
        name= $(this).attr('name-customer');
        $('#name_customer').html(name);
        $('#data_total_orders').show();

        datatables_total_orders(id);
    })

    function datatables_total_orders(id) {

        tbl = $('#table-total_order').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            ordering: 'true',
            responsive: true,
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/order/datatables/' + id + ''),
                type: "get",
                error: function() {},
                complete: function() {},
            },
            selector: '#table_total_order',
            columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                sortable: false,
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    link = "<a href='" + Helper.url('/admin/order/service_detail/' + full.id) + "'># " + full.code + "</a>";
                    if (full.garansi) {
                        link += '</br><span class="ml-auto badge badge-primary">Warranty Claim</span>';
                    }
                    if (full.admin_read_at == null) {
                        link += '</br><span class="ml-auto badge badge-warning">NEW</span>';
                    }
                    return link;
                }
            },
            {
                data: "schedule",
                name: "schedule",
                render: function(data, type, full) {
                    return moment(full.schedule).format("DD MMMM YYYY HH:mm");
                }
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    if (full.user == null) {
                        return "Deleted Users";
                    } else {
                        return full.user.name;
                    }
                }
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full) {
                    if (full.service_detail.length) {
                        service_detail = full.service_detail[0];
                        return service_detail.technician.user.full_name;
                    } else {
                        return "Deleted Users";
                    }
                }
            },
            {
                data: "payment_type",
                name: "payment_type",
                render: function(data, type, full) {
                    status = full.payment_type == 0 ? '<div class="badge badge-danger ml-2">Offline</div>' : '<div class="badge badge-info ml-2">Online</div>';
                    return status;
                }
            },
            {
                data: "grand_total",
                name: "grand_total",
                render: $.fn.dataTable.render.number(',', '.,', 0, 'Rp. ', '.00')
            },
            {
                data: "order_status.name",
                name: "order_status.name",
                render: function(data, type, full) {
                    var text = '';
                    if (full.order_status.id === 3 && full.is_less_balance === 1) {
                        text = '<span class="badge badge-warning">' + data + '</span><br/> <span class="badge badge-danger">Less Balance</span>';
                    } else {
                        if (full.orders_statuses_id === 2) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 3) {
                            return '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 4) {
                            return '<span class="badge badge-primary">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 5) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 6) {
                            return '<span class="badge badge-warning">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 7) {
                            return '<span class="badge badge-focus">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 8) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 9) {
                            return '<span class="badge badge-primary">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 10) {
                            return '<span class="badge badge-success">' + full.order_status.name + '</span>';
                        } else if (full.orders_statuses_id === 11) {
                            return '<span class="badge badge-danger">' + full.order_status.name + '</span>';
                        }
                    }
                    return text;
                }
            }
        ]
        });

    }

</script>
@endsection
