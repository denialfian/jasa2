@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Technicians</label>
                            <select name="technician_id" id="technician_id" class="form-control" style="width:100%;"> </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Date</label>
                            <input name="order_date_range" id="order_date_range" type="text" class="form-control"/>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12" style="padding-top: 10px">
    <div class="card header-border">
        <div class="card-body table-responsive">
            <table id="table-te" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Full Name</th>
                        <th>Email</th>

                        <th>Total Services</th>
                        <th>Total Order Success</th>
                        <th>Total Order Pending</th>
                        <th>Total Order Transactions</th>
                        <th>Total Orders Gross Revenue</th>
                        <th>Total Orders Cut Commission</th>
                        <th>Total Orders Net Revenue</th>
                        <th>Total Orders Gross Revenue Pending</th>
                        <th>Sparepart Sold</th>
                        <th>Sparepart Revenue</th>
                    </tr>
                </thead>
            </table>
        </div>

    </div>
</div>


@endsection

@section('script')
<style>
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script>
    $(function() {
        $('input[name="order_date_range"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="order_date_range"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });

        $('input[name="order_date_range"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
    })

    var tbl_history = null;
    globalCRUD.select2("#technician_id", '/technician/select2');

    // table
    var table = globalCRUD.datatables({
            url: '/admin/report/datatables_technician',
            selector: '#table-te',
            columnsField: [{
                    name: 'name',
                    data: 'name',
                    render: function(data,type,full) {
                        return '<a href="'+Helper.url('/admin/user/detail/'+full.user.id+'')+'">'+data+'</a>';
                    }
                }, 
                'email',
                 'price_services_count', 
                 'pending_order_count', 
                 'done_order_count', 
                 'service_detail_count', 
                 'total_orders_gross_revenue_sum', 
                 'total_orders_cut_commission_sum', 
                 'total_orders_net_revenue_sum',
                  'total_orders_gross_revenue_pending_sum', 
                  'total_qty_spartpart', 
                  'total_price_spartpart'
                  ],
            actionLink: '',
            export: true
        })


    // search data
    $("#form-search")
        .submit(function(e) {
            input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/admin/report/datatables_technician' + playload);
            table.table.reloadTable(url);
            e.preventDefault();
        })
</script>
@endsection
