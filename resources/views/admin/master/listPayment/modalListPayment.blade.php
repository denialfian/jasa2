<form id="form-list-payment">
    <div class="modal fade" id="modal-list-payment" data-backdrop="false" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Payment List</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">

                    <label>Name</label>
                    <div class="form-group">
                        <input name="name" placeholder="name" id="name" type="text" class="form-control" required>
                        <!-- <span id="error-name" style="color:red"></span> -->
                    </div>


                    <label>Description</label>
                    <div class="form-group">
                        <textarea name="desc" id="desc" cols="30" rows="10"></textarea>
                        <input name="code" placeholder="code" id="code" type="text" class="form-control" required>
                        <!-- <span id="error-code" style="color:red;"></span> -->
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">Save</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>