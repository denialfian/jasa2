@extends('admin.home')
@section('content')
<style>
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }


</style>
<div class="col-md-12">
    <div class="card">

        <div class="card-header-tab card-header">
            {{ $title }}
        </div>
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <form id="form-search">
                    <div class="row">
                        <div class="col-md-3">
                            <select name="type_date" id="type_date" class="form-control form-control-sm">
                                <option value="request_date">Request Date</option>
                                <option value="repair_completed_date">Repair Complete Date</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">From</span>
                                </div>
                                <input type="text" name="date_from" class="form-control date_format" readonly required/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">To</span>
                                </div>
                                <input type="text" name="date_to" class="form-control date_format" readonly required/>
                            </div>
                        </div>
                        <div class="col-md-3" style="margin-left:-10px;">
                            <div role="group" class="mb-2 btn-group-sm btn-group btn-group-toggle">
                                <button type="submit" class="mb-1 mr-1 btn btn-sm btn-primary btn-gradient-alternate"><i class="fa fa-search"></i> Filter </button>
                                <button type="button" class="mb-1 mr-1 btn btn-sm btn-success btn-gradient-alternate" id="export_data"><i class="fa fa-download"></i> Export Data </a></button>
                                <a href="{{ url('admin/orders-data-excel/create') }}" class="mb-1 mr-1 btn btn-sm btn-primary btn-gradient-alternate"><i class="fa fa-plus"></i> Add </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card-body">
        <table style="width: 100%;" id="table-orders-data-excel" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Part</th>
                        <th>Service Order No.</th>
                        <th>ASC Name</th>
                        <th>Customer name</th>
                        <th width="7%">Defect Type Description</th>
                        <th>Merk Brand</th>
                        <th>Request Time</th>
                        <th>Engineer Name</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(function() {
        $('.date_format').datetimepicker({
            timepicker:false,
            format:'d-m-Y',
        });

        var d = new Date();
        var startDate = d.setMonth(d.getMonth() - 1);
        var e = new Date();

        $('input[name="date_from"]').val(moment(startDate).format('DD-MM-YYYY'));
        $('input[name="date_to"]').val(moment(e.setDate(e.getDate())).format('DD-MM-YYYY'));

    });

    var tableOrder = globalCRUD.datatables({
        url: '/orders-data-excel/datatables',
        selector: '#table-orders-data-excel',
        columnDefs: [{
            targets: [4],
            createdCell: function(cell) {
                var $cell = $(cell);
                $(cell).contents().wrapAll("<div class='content'></div>");
                var $content = $cell.find(".content");
                if ($content.text().length > 12) {
                    $(cell).append($("<button class='btn btn-sm btn-outline-primary'>Read more</button>"));
                    $btn = $(cell).find("button");

                    $content.css({
                    "height": "50px",
                    "overflow": "hidden"
                    })
                    $cell.data("isLess", true);

                    $btn.click(function() {
                    var isLess = $cell.data("isLess");
                    $content.css("height", isLess ? "auto" : "50px")
                    $(this).text(isLess ? "Read less" : "Read more")
                    $cell.data("isLess", !isLess)
                    })
                }
            }
        }],
        columnsField: [{
            "className": 'details-control',
            "defaultContent": ''
        }
        ,'service_order_no', 'asc_name', 'customer_name', 'defect_type', 'merk_brand', 'request_time', 'engineer_name','status'],
        actionLink: {
            update: function(row) {
                return "/admin/orders-data-excel/edit/" + row.id;
            },
            delete: function(row) {
                return "/orders-data-excel/" + row.id;
            },
            detail: function(row) {
                return "/admin/orders-data-excel/detail/" + row.id;
            }
        }
    })

    function checkValidation() {
        var isValid = true;
        var date_from = $('input[name="date_from"]').val();
        var date_to = $('input[name="date_to"]').val();
        if(date_from == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Date Fr !');
            return false;
        } else if (date_to == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Date To !');
            return false;
        }
        return isValid;
    }

    function templatex(d) {
        console.log(d)
        template = '';
        template += '<div class="col-md-6">';
        template += '<h5>Part Data</h5>';
        template += '<table class="table" id="example">';
        template += '<tr>';
        template += '<td>No</td>';
        template += '<td>Code Material</td>';
        template += '<td>Parts Description</td>';
        template += '<td>Quantity</td>';
        template += '</tr>';

        _.each(d.part_data_excel, function(i, key) {
            template += '<tr>';
            template += '<td>' + (key + 1) + '</td>';
            template += '<td>' + i.code_material + '</td>';
            template += '<td>' + i.part_description + '</td>';
            template += '<td>' + i.quantity + '</td>';
            template += '</tr>';
        })
        template += '</table>';
        template += '</div>';

        return template;
    }
    var table = $('#table-orders-data-excel').DataTable();
    $('#table-orders-data-excel tbody').on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(templatex(row.data())).show();
            $(row.child()).addClass('smalltable');
            tr.addClass('shown');
        }
    });

    $("#export_data").click(function(e) {
        var date_from = $('input[name="date_from"]').val();
        var date_to = $('input[name="date_to"]').val();
        var type_date = $('input[name="type_date"]').val();
        var url = '/admin/orders-data-excel/export';
        if(checkValidation()) {
            url = '/admin/orders-data-excel/export?date_from='+date_from+'&date_to='+date_to+'&type_date='+type_date+'';
            Helper.redirectTo(url);
        }
        // console.log(url);

        e.preventDefault();
    });

    $("#form-search").submit(function(e) {
        e.preventDefault();
        if(checkValidation()) {
            var input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/orders-data-excel/datatables/search' + playload);
            tableOrder.table.reloadTable(url);
        }

    })
    </script>
@endsection
