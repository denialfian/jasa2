@extends('admin.home')
@section('content')
<style>
    .upload-description-header{
        color:red;
        font-weight:800;
    }
    #upload{
        width: 500px;
        height: 250px;
        margin:auto;
        margin-bottom:25px;
        margin-top:25px;
        padding: 25px;
        border:2px dashed #028AF4;
    }

    .upload-description-content{
        list-style-type: decimal;
    }

</style>
<div class="app-main__inner">
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="portlet-body">
                    <div class="card-header">Import All Master Data</div>
                    <div id="upload">
                        <h4 class="upload-description-header">Note to upload Excel: </h4>
                        <ul class="upload-description-content">
                            <li>Download file template <a href="{{ asset('admin/storage/import_all.xlsx') }}">here</a></li>
                            <li>Accepted file type only in .xlsx extension</li>
                            <li>File must not exceed 10 MB</li>
                            <li>Please <b>DO NOT</b> change the format of excel template</li>
                        </ul>
                        {{-- <form method="post" id="file-upload" enctype="multipart/form-data"> --}}

                            <input type="file" id="file" accept="text/csv,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required/>
                            <input type="hidden" id="type_import" value="master">
                            <div class="padding-bottom-30">
                                <div class="pull-right">
                                    <div class="loader"></div>
                                    <button class="btn btn-primary" id="import"> Upload </button>
                                </div>
                            </div>
                        {{-- </form> --}}
                    </div>
                </div>
                <br>
                <div class="d-block text-center card-footer">
                    <div class="progress" style="display:none">
                        <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                            <span class="sr-only">0%</span>
                        </div>
                    </div>
                    <div class="msg alert alert-danger text-left" style="display:none; margin-top:15px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
    @include('admin.script.master._importScript')
@endsection
