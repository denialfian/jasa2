@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-bottom:15px;">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/part-data-excel-stock-inventory/show') }}" class="btn btn-primary btn-sm">Back</a>
            </div>
        </div>

    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <input name="id" id="id" value="{{ $pdesi->id }}" type="hidden" class="form-control">
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        <table id="table" class="display table table-hover table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Part Description </th>
                                    <th>ASC Name</th>
                                    <th>Recent Stock</th>
                                    <th>Amount</th>
                                    <th>Type IN/OUT</th>
                                    <th>Created By</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<style>
    .table-edit {
        visibility: hidden;
    }
</style>
<script>

var tbl_historyio = globalCRUD.datatables({
        url: '/part-data-excel-stock-inventory/histories-in-out/'+$('#id').val()+'',
        selector: '#table',
        columnsField: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',

            },

            {
                data: 'part_description',
                name: 'part_description',
            },
            {
                data: 'asc_name',
                name: 'asc_name',
            },
            {
                data: 'stock_after',
                name: 'stock_after',
            },
            {
                data: 'amount',
                name: 'amount',
            },
            {
                data: 'type_mutation',
                name: 'type_mutation',
                render: function(data, type, row){
                    // 0 = add_general_journal;
                    // 3 = order_data_excel;

                    if (data == 0) {
                        return '<span class="badge badge-success">IN</span> General Journal'
                    } else if (data == 6) {
                        return '<span class="badge badge-success">IN</span> Create New Part Stock';
                    } else if (data == 3) {
                        return '<span class="badge badge-danger">OUT</span> Orders Excel #<b>'+(row.part_data_excel != null ? row.part_data_excel.order_data_excel.service_order_no : '(deleted)')+'</b>';
                    }

                }
            },

            {
                data: 'created_at',
                name: 'created_at',
                render: function(data, type, row) {
                    return '<span class="badge badge-primary">'+row.user.name+' </span><br>'+moment(data).format("DD MMMM YYYY hh:mm:ss");
                }
            }
        ],
        actionLink: {
            delete: function(row) {
                if (row.type_mutation == 0 ) {
                    return '/general-journal/destroy_details_data/' + row.general_journal_detail_id + '/' + row.id;
                } else if (row.type_mutation == 3 ) {
                    return '/orders-data-excel/destroy_part_data/' + row.part_data_excel_id + '/' + row.id;
                }
            },

        }
    });



</script>




@endsection
