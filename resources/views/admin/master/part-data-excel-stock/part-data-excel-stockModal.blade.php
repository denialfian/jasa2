<form id="form-part-data-excel-stock">
    <div class="modal fade" id="modal-part-data-excel-stock" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">ADD PART DATA EXCEL STOCK</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label>Code Material</label>
                    <div class="form-group">
                        <input name="code_material" placeholder="Code Material" id="code_material" type="text" class="form-control">
                    </div>
                    <label>ASC Name</label>
                    <div class="form-group">
                        <select name="asc_id" class="js-example-basic-single"  id="asc_id" class="form-control" style="width:100%">
                        </select>

                        <input name="asc_name" id="asc_name" type="hidden" class="form-control">
                    </div>
                    <label>Part Description</label>
                    <div class="form-group">
                        <input name="part_description" placeholder="Part Description" id="part_description" type="text" class="form-control">
                    </div>
                    {{-- <label>Stock</label>
                    <div class="form-group">
                        <input name="stock" placeholder="Stock" id="stock" type="text" class="form-control number_only">
                    </div> --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm waves-effect btn-clus" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    <button type="submit" class="btn btn-sm btn-oke waves-effect"><i class="fa fa-plus"></i> Save</button>
                </div>
            </div>
        </div>
    </div>
</form>

