@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <form id="form-search">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">From</span>
                                </div>
                                <input type="text" name="date_from" class="form-control date_format" readonly required/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">To</span>
                                </div>
                                <input type="text" name="date_to" class="form-control date_format" readonly required/>
                            </div>
                        </div>
                        <div class="col-md-3" style="margin-left:-10px;">
                            <div role="group" class="mb-2 btn-group-sm btn-group btn-group-toggle">
                                <button type="submit" class="mb-1 mr-1 btn btn-sm btn-primary btn-gradient-alternate"><i class="fa fa-search"></i> Filter </button>
                                <button type="button" class="mb-1 mr-1 btn btn-sm btn-success btn-gradient-alternate" id="export_data"><i class="fa fa-download"></i> Export Data </a></button>
                                <button type="button" class="mb-1 mr-1 btn btn-sm btn-success btn-gradient-alternate add-part-data-excel-stock"><i class="fa fa-plus"></i> ADD Part Data</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-part-data-excel-stock" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Code Material</th>
                        <th>ASC id</th>
                        <th>ASC Name</th>
                        <th>Part Description</th>
                        {{-- <th>Stock</th> --}}
                        <th>Action</th>
                    </tr>
                </thead>
            </table>

        </div>
    </div>
</div>
@include('admin.master.part-data-excel-stock.part-data-excel-stockModal')
@endsection

@section('script')
<script>
    $(function() {
        $('.date_format').datetimepicker({
            timepicker:false,
            format:'d-m-Y',
        });

        var d = new Date();
        var startDate = d.setMonth(d.getMonth() - 1);
        var e = new Date();

        $('input[name="date_from"]').val(moment(startDate).format('DD-MM-YYYY'));
        $('input[name="date_to"]').val(moment(e.setDate(e.getDate())).format('DD-MM-YYYY'));

    });
    function checkValidation() {
        var isValid = true;
        var date_from = $('input[name="date_from"]').val();
        var date_to = $('input[name="date_to"]').val();
        if(date_from == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Date Fr !');
            return false;
        } else if (date_to == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Date To !');
            return false;
        }
        return isValid;
    }
    Helper.onlyNumberInput('.number_only')
    var tableOrder = globalCRUD.datatables({
                url: '/part-data-excel-stock/datatables',
                selector: '#table-part-data-excel-stock',
                columnsField: [
                    'DT_RowIndex',
                    'code_material',
                    'asc_id',
                    'asc_name',
                    'part_description',

                    // 'stock'
                ],
                modalSelector: "#modal-part-data-excel-stock",
                modalButtonSelector: ".add-part-data-excel-stock",
                modalFormSelector: "#form-part-data-excel-stock",
                actionLink: {
                    store: function() {
                        return "/part-data-excel-stock";
                    },
                    update: function(row) {
                        return "/part-data-excel-stock/" + row.id;
                    },
                    delete: function(row) {
                        return "/part-data-excel-stock/" + row.id;
                    },
                }
            })
    globalCRUD.select2Static("#asc_id", '/asc-excel/select2', function(item) {
        return {
            id: item.id,
            text: item.name
        }
    })
    globalCRUD.select2('#asc_id', '/asc-excel/select2');
    $('#asc_id').on('select2:select', function (e) {
        $('#asc_name').val(e.params.data.text);
    });
    $("#export_data").click(function(e) {
        var date_from = $('input[name="date_from"]').val();
        var date_to = $('input[name="date_to"]').val();
        var url = '/admin/part-data-excel-stock/export';
        if(checkValidation()) {
            url = '/admin/part-data-excel-stock/export?date_from='+date_from+'&date_to='+date_to+'';
            Helper.redirectTo(url);
        }
        // console.log(url);

        e.preventDefault();
    });

    $("#form-search").submit(function(e) {
        e.preventDefault();
        if(checkValidation()) {
            var input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/part-data-excel-stock/datatables/search' + playload);
            tableOrder.table.reloadTable(url);
        }

    })
    $(document).on('click', '.add-part-data-excel-stock', function() {
        $("#stock").prop('disabled', false);
    });



    $(document).on('click', '.table-part-data-excel-stock-edit', function() {

        $("#stock").prop('disabled', true);
    });






</script>
@endsection
