@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card header-border">
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Warehouse</label>
                            <select class="form-control select-warehouse" name="warehouse_id[]" multiple style="width: 100%">
                                {!! $optionSelectedWarehouse !!}
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Product</label>
                            <select class="form-control select-product" multiple="multiple"  name="product_id[]" style="width: 100%">
                                {!! $optionSelectedProduct !!}
                            </select>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
                <a href="{{ url('/admin/inventory/create') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Create</a>
            </form>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body table-responsive">
            <table style="width: 100%;" id="form-example" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th >No</th>
                        <th>Batch No</th>
                        <th>Product Name</th>
                        <th>Shipping Qty</th>
                        <th>Inventory Qty</th>
                        <th>Mutation</th>
                        <th>Value</th>
                        <th>COGS</th>
                        <th>Warehouse</th>
                        <th>Approve</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <a href="{{ url('/admin/inventory/create') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> Create New Inventory</a>
        </div>
    </div>
</div>
<form id="form-inventory">
    <div class="modal fade" id="modal-inventory" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Approve Inventory</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="inventory-approved-area"></div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">Approve</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')

{{-- select2 multiple --}}
<style>
    .select2-container {
    min-width: 400px;
    }

    .select2-results__option {
    padding-right: 20px;
    vertical-align: middle;
    }
    .select2-results__option:before {
    content: "";
    display: inline-block;
    position: relative;
    height: 20px;
    width: 20px;
    border: 2px solid #e9e9e9;
    border-radius: 4px;
    background-color: #fff;
    margin-right: 20px;
    vertical-align: middle;
    }
    .select2-results__option[aria-selected=true]:before {
    font-family:fontAwesome;
    content: "\f00c";
    color: #fff;
    background-color: #f77750;
    border: 0;
    display: inline-block;
    padding-left: 3px;
    }
    .select2-container--default .select2-results__option[aria-selected=true] {
        background-color: #fff;
    }
    .select2-container--default .select2-results__option--highlighted[aria-selected] {
        background-color: #eaeaeb;
        color: #272727;
    }
    .select2-container--default .select2-selection--multiple {
        margin-bottom: 10px;
    }
    .select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple {
        border-radius: 4px;
    }
    .select2-container--default.select2-container--focus .select2-selection--multiple {
        border-color: #f77750;
        border-width: 2px;
    }
    .select2-container--default .select2-selection--multiple {
        border-width: 2px;
    }
    .select2-container--open .select2-dropdown--below {

        border-radius: 6px;
        box-shadow: 0 0 10px rgba(0,0,0,0.5);

    }
    .select2-selection .select2-selection--multiple:after {
        content: 'hhghgh';
    }
</style>

<script>
    // select2
    globalCRUD
        .select2('.select-warehouse', '/warehouses/select2')
        .select2Tags('.select-product', '/product/select2')

    tableCrud = globalCRUD.datatables({
        url: '/inventory/datatables',
        selector: '#form-example',
        columnsField: [
            'DT_RowIndex',
            {
                data: "id",
                name: "id",
                searchable: false,
                orderable: false,
                render: function(data, type, row){
                    return row.batch_item == null ? '' : row.batch_item.batch.batch_no;
                }
            },
            {
                data: "id",
                name: "id",
                searchable: false,
                orderable: false,
                render: function(data, type, row){
                    return row.item_name;
                }
            },
            {
                data: "id",
                name: "id",
                searchable: false,
                orderable: false,
                render: function(data, type, row){
                    return row.batch_item == null ? '' : ((row.batch_item.quantity - row.batch_item.quantity_return) + ' ' + row.unit_type.unit_name);
                }
            },
            {
                data: "id",
                name: "id",
                searchable: false,
                orderable: false,
                render: function(data, type, row){
                    return row.stock_now;
                }
            },
            {
                data: "id",
                name: "id",
                searchable: false,
                orderable: false,
                render: function(data, type, row){
                    if (row.batch_item == null) {
                        return '-';
                    }
                    return "<a href='/admin/inventory/mutation/"+ row.id + "' class='btn btn-primary btn-sm'><i class='fa fa-exchange'></i></a>";
                }
            },
            {
                data: "id",
                name: "id",
                searchable: false,
                orderable: false,
                render: function(data, type, row){
                    return row.batch_item == null ? '' : row.batch_item.value_format;
                }
            },
            {
                data: "id",
                name: "id",
                searchable: false,
                orderable: false,
                render: function(data, type, row){
                    return row.cogs_value_format;
                }
            },
            'warehouse.name',
            {
                data: "id",
                name: "id",
                visible: false,
                searchable: false,
                orderable: false,
                render: function(data, type, row){

                    btn = "<button data-id='" + row.id + "' class='inventory-approve btn btn-primary btn-sm'><i class='fa fa-eye'></i> Approve</button>";
                    if (row.is_need_approval == 1) {
                        return btn;
                    }

                    return '';
                }
            },
        ],
        actionLink: {
            only_icon: true,
            render: function(data, type, full, actionData){
                if (full.batch_item == null) {
                    return actionData.delete_btn;
                }

                return actionData.edit_btn +' '+ actionData.delete_btn;
            },
            update: function(row) {
                return '/admin/inventory/update/' + row.id;
            },
            delete: function(row) {
                return '/inventory/' + row.id;
            },
        }
    })

    // search data
    $("#form-search").submit(function(e) {
        input = Helper.serializeForm($(this));
        playload = '?';
        _.each(input, function(val, key) {
            playload += key + '=' + val + '&'
        });
        playload = playload.substring(0, playload.length - 1);
        console.log(playload)

        url = Helper.apiUrl('/inventory/datatables/search' + playload);
        tableCrud.table.reloadTable(url);
        e.preventDefault();
    })

    $(document).on('click', '.inventory-approve', function(){
        Helper.loadingStart();
        id = $(this).attr('data-id');

        Axios.get('/inventory/'+id+'/approval_modal_template/')
            .then(function(response){
                $('#modal-inventory').modal('show')
                $('.inventory-approved-area').html(response.data.data);
                Helper.loadingStop();
            })
            .catch(function(error){
                console.log(error)
                Helper.loadingStop();
            })
    })

    $('#form-inventory').submit(function(e){
        Helper.loadingStart();
        Axios.put('/inventory/' + id + '/approve')
            .then(function(){
                Helper.successNotif('data berhasil diapprove');
                $('#modal-inventory').modal('hide')
                tableCrud.table.reloadTable();
            })
            .catch(function(error){
                console.log(error)
                Helper.loadingStop();
                $('#modal-inventory').modal('hide')
                 Helper.successNotif('data gagal diapprove');
            })
        e.preventDefault();
    })
</script>
@if($optionSelectedProduct != '' || $optionSelectedWarehouse != '')
<script>
    $("#form-search").submit();
</script>
@endif
@endsection
