<script>
    //select2 product_id
    $("#product").select2({
        ajax: {
            type: "GET",
            url:Helper.apiUrl('/product/select2'),
            data: function(params) {
                return {
                    q: params.term
                };
            },
            processResults: function(data) {
                var res = $.map(data.data, function(item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                });

                return {
                    results: res
                };
            }
        }
    });

    //select2 batch
    $("#batches").select2({
        ajax: {
            type: "GET",
            url:Helper.apiUrl('/batch/select2'),
            data: function(params) {
                return {
                    q: params.term
                };
            },
            processResults: function(data) {
                var res = $.map(data.data, function(item) {
                    return {
                        text: item.batch_date,
                        id: item.id
                    }
                });

                return {
                    results: res
                };
            }
        }
    });

    //select2 warehouse
    $("#warehouse").select2({
        ajax: {
            type: "GET",
            url:Helper.apiUrl('/warehouses/select2'),
            data: function(params) {
                return {
                    q: params.term
                };
            },
            processResults: function(data) {
                var res = $.map(data.data, function(item) {
                    return {
                        text: item.name,
                        id: item.id
                    }
                });

                return {
                    results: res
                };
            }
        }
    });

    //select2 batch
    $("#batch_items").select2({
        ajax: {
            type: "GET",
            url:Helper.apiUrl('/batch/child/select2'),
            data: function(params) {
                return {
                    q: params.term
                };
            },
            processResults: function(data) {
                var res = $.map(data.data, function(item) {
                    return {
                        text: item.quantity,
                        id: item.id
                    }
                });

                return {
                    results: res
                };
            }
        }
    });

    //select2 unitType
    $("#unit_types").select2({
        ajax: {
            type: "GET",
            url:Helper.apiUrl('/unit-type/select2'),
            data: function(params) {
                return {
                    q: params.term
                };
            },
            processResults: function(data) {
                var res = $.map(data.data, function(item) {
                    return {
                        text: item.unit_name,
                        id: item.id
                    }
                });

                return {
                    results: res
                };
            }
        }
    });
</script>