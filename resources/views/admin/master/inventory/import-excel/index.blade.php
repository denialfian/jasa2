@extends('admin.home')
@section('content')
<style>
    .upload-description-header{
        color:red;
        font-weight:800;
    }
    #upload{
        width: 500px;
        height: 250px;
        margin:auto;
        margin-bottom:25px;
        margin-top:25px;
        padding: 25px;
        border:2px dashed #028AF4;
    }

    .upload-description-content{
        list-style-type: decimal;
    }

    .modal-dialog{
        overflow-y: initial !important
    }
    .modal-body{
        height:450px;
        overflow-y: auto;
    }

</style>
<div class="app-main__inner">
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="portlet-body">
                    <div class="card-header">Import Batch Inventory Data</div>
                    <div id="upload">
                        <h4 class="upload-description-header">Note to upload Excel: </h4>
                        <ul class="upload-description-content">
                            <li>Download file template <a href="{{ asset('admin/storage/Import_Inventory_Template.xlsx') }}">here</a></li>
                            <li>Accepted file type only in .xlsx extension</li>
                            <li>File must not exceed 10 MB</li>
                            <li>Please <b>DO NOT</b> change the format of excel template</li>
                        </ul>
                        {{-- <form method="post" id="file-upload" enctype="multipart/form-data"> --}}

                            <input type="file" id="file" accept="text/csv,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required/>
                            <input type="hidden" id="type_import" value="batchinventory">
                            <div class="padding-bottom-30">
                                <div class="pull-right">
                                    <div class="loader"></div>
                                    <button class="btn btn-primary" id="import"> Upload </button>
                                </div>
                            </div>
                        {{-- </form> --}}
                    </div>
                </div>
                <br>
                <div class="d-block text-center card-footer">
                    <div class="progress" style="display:none">
                        <div id="progressBar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                            <span class="sr-only">0%</span>
                        </div>
                    </div>
                    <div class="note">

                        <div style="max-height:175px;overflow:auto;">
                            <span></span><br/>
                            <p style="color:red;" id="note-content"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

  {{-- BEGIN MODAL --}}
  <div class="modal fade" data-backdrop="false" id="modal_preview" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg mw-100 w-75" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Upload Excel Import Batch Inventory
                </h4>
            </div>
            <div class="modal-body">
                {{-- START TABLE PREVIEW --}}
                <div class="col-lg-12 preview hidden">
                    <h2>Data Preview</h2>
                    <div>
                        <table id="preview">
                            <thead>
                            <tr>
                                <th>Batch Date</th>
                                <th>Shiping Cost</th>
                                <th>Extra Cost</th>
                                <th>Product Code</th>
                                <th>Value</th>
                                <th>Quantity</th>
                                <th>Status</th>
                                <th>Cogs Value</th>
                                <th>Unit Types</th>
                                <th>Warehouse Name</th>
                                <th>Warehouse Address</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                {{-- END TABLE PREVIEW --}}
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                <button type="button" data-dismiss="modal" class="btn btn-info" id="save" onClick="saveData();">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL CONFIRM DELETE -->


@endsection
@section('script')
    @include('admin.script.master._importScript')
@endsection
