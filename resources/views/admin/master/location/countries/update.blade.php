@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="main-card mb-3 card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ route('admin.country.index.web') }}" class="mb-2 mr-2 btn btn-primary" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
        <div class="table-responsive"><br>
            <div class="container">
                <div class="card-body">
                    <form action="" method="post" id="form">
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input name="name" value="{{ $updateCountries->name }}" id="examplename" placeholder="Indonesia" type="text" class="form-control">
                                <strong><span id="error-name" style="color:red"></span></strong>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="{{ $updateCountries->id }}">
                        <div class="position-relative row form-group">
                            <label for="exampleEmail" class="col-sm-2 col-form-label">Code</label>
                            <div class="col-sm-10">
                                <input name="code" value="{{ $updateCountries->code }}" id="examplename" placeholder="INA" type="text" class="form-control">
                                <strong><span id="error-code" style="color:red"></span></strong>
                            </div>
                        </div>

                        <div class="position-relative row form-check">
                            <div class="col-sm-10 offset-sm-2">
                                <button class="btn btn-success" id="update"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="d-block text-center card-footer">

        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    // update
    $('#update').click(function(e) {
        id = $('input[name="id"]').val()
        $.ajax({
            type: 'put',
            data: {
                id: $('input[name="id"]').val(),
                name: $('input[name="name"]').val(),
                code: $('input[name="code"]').val(),
            },
            url: Helper.apiUrl('/country/' + id),
            success: function(html) {
                iziToast.success({
                    title: 'OK',
                    position: 'topRight',
                    message: 'Countries Has Been Saved',
                });
                console.log(html)
                window.location.href = Helper.redirectUrl('/admin/country');
            },
            error: function(xhr, status, error) {
                if (xhr.status == 422) {
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field) {
                        $('#error-' + field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message: pesan[0]
                        });

                    })
                }
            },
        });
        e.preventDefault();
    })

    // $('#form').submit(function(e) {

    //     globalCRUD.handleSubmit($(this))
    //         .updateTo(function(formData) { //API
    //             return '/country/' + formData.id;
    //         })
    //         .redirectTo(function(resp) {
    //             return '/admin/country/show';
    //         })

    //     e.preventDefault();
    // });
</script>
@endsection