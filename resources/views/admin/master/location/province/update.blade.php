@extends('admin.home')
@section('content')
    <div class="col-md-12 card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Edit Master Province
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ route('admin.province.index.web') }}" class="mb-2 mr-2 btn btn-primary" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
        <div class="card-body">

            <form action="" method="post" id="form">
                <input type="hidden" name="id" value="{{ $editProvince->id }}">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Country</label>
                    <div class="col-sm-10">
                        <select id="country"  name="ms_country_id" style="width:100%">
                            <option value="{{ $editProvince->ms_country_id }}" selected>{{ $editProvince->country->name }}</option>
                        </select>
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                        <input name="name" value="{{ $editProvince->name }}" id="examplename" placeholder="Province" type="text" class="form-control">
                        <strong><span id="error-name" style="color:red"></span></strong>
                    </div>
                </div>

                <div class="position-relative row form-check">
                    <div class="col-sm-10 offset-sm-2">
                        <button class="btn btn-success" id="update"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        globalCRUD.select2("#country", '/country/select2')

        // update
    $('#update').click(function(e){
        id = $('input[name="id"]').val()
        $.ajax({
            type:'put',
            data:{
                id : $('input[name="id"]').val(),
                name : $('input[name="name"]').val(),
                ms_country_id : $('select[name="ms_country_id"]').val(),
            },

            url:Helper.apiUrl('/province/' + id),
            success:function(html){
                iziToast.success({
                    title: 'Success',
                    position: 'topRight',
                    message:  'Province Has Been Saved'
                });
                console.log(html)
                window.location.href = Helper.redirectUrl('/admin/province');
            },
            error: function(xhr, status, error) {
                if(xhr.status == 422){
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field){
                        $('#error-'+ field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message:  pesan[0]
                        });
                    
                    })
                }
            },
        });
        e.preventDefault();
    })
        // $('#form').submit(function(e) {
        //     globalCRUD.handleSubmit($(this))
        //         .updateTo(function(formData) { //API
        //             return '/province/' + formData.id;
        //         })
        //         .redirectTo(function(resp) {
        //             return '/admin/province/show';
        //         })
        //     e.preventDefault();
        // });
    </script>
@endsection
