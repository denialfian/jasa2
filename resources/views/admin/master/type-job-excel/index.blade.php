@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-type-job-excel" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
            <button class="btn btn-primary btn-sm add-type-job-excel"><i class="fa fa-plus"></i> ADD TYPE JOB EXCEL</button>
        </div>
    </div>
</div>
@include('admin.master.type-job-excel.type-job-excelModal')
@endsection

@section('script')
<script>
    Helper.onlyNumberInput('.number_only')
    globalCRUD.datatables({
        url: '/type-job-excel/datatables',
        selector: '#table-type-job-excel',
        columnsField: ['DT_RowIndex', 'name'],
        modalSelector: "#modal-type-job-excel",
        modalButtonSelector: ".add-type-job-excel",
        modalFormSelector: "#form-type-job-excel",
        actionLink: {
            store: function() {
                return "/type-job-excel";
            },
            update: function(row) {
                return "/type-job-excel/" + row.id;
            },
            delete: function(row) {
                return "/type-job-excel/" + row.id;
            },
        }
    })

</script>
@endsection
