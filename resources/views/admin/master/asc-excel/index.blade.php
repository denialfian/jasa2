@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-asc-excel" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
            <button class="btn btn-primary btn-sm add-asc-excel"><i class="fa fa-plus"></i> ADD ASC Excel</button>
        </div>
    </div>
</div>
@include('admin.master.asc-excel.asc-excelModal')
@endsection

@section('script')
<script>
    Helper.onlyNumberInput('.number_only')
    globalCRUD.datatables({
        url: '/asc-excel/datatables',
        selector: '#table-asc-excel',
        columnsField: ['DT_RowIndex', 'name','address'],
        modalSelector: "#modal-asc-excel",
        modalButtonSelector: ".add-asc-excel",
        modalFormSelector: "#form-asc-excel",
        actionLink: {
            store: function() {
                return "/asc-excel";
            },
            update: function(row) {
                return "/asc-excel/" + row.id;
            },
            delete: function(row) {
                return "/asc-excel/" + row.id;
            },
        }
    })

</script>
@endsection
