@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
            	@if(count($countData) > 0)
	            	<a href="{{ url('/admin/banner/create') }}" class="btn btn-primary">Create</a>
	            @endif
            </div>
        </div>
        <div class="card-body">
            <div class="jFiler-items jFiler-row">
                    <ul class="jFiler-items-list jFiler-items-grid">
                        @foreach($countData as $banners)
                            <li class="jFiler-item jFiler-no-thumbnail" data-jfiler-index="3" style="">
                                <a href="{{ url('admin/storage/banner/'.$banners->images) }}" target="_blank">
                                    <div class="jFiler-item-container">
                                        <div class="jFiler-item-inner">
                                            <div class="jFiler-item-thumb">
                                                <div class="jFiler-item-status"></div>
                                                <div class="jFiler-item-thumb-image">
                                                    <img src="{{ url('admin/storage/banner/'.$banners->images) }}">
                                                </div>
                                            </div>
                                            <div class="jFiler-item-assets jFiler-row">
                                                <ul class="list-inline pull-left">
                                                    <li></li>
                                                </ul>
                                                <ul class="list-inline pull-right">
                                                    <li>
                                                    	<a class="icon-jfi-trash jFiler-item-trash-action delete-image" data-id="{{ $banners->id }}"></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<link rel="stylesheet" href="{{ asset('adminAssets/css/jquery.filer.css') }}">
<link rel="stylesheet" href="{{ asset('adminAssets/css/themes/jquery.filer-dragdropbox-theme.css') }}">
<script type="text/javascript" src="{{ asset('adminAssets/js/filer/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminAssets/js/filer/jquery.filer.min.js') }}"></script>

<script>
    $(document).on('click', '.delete-image', function(e) {
            id = $(this).attr('data-id');
            Helper.confirm(function(){
                Helper.loadingStart();
                $.ajax({
                    url:Helper.apiUrl('/admin/galery/delete-banner/' + id ),
                    type: 'post',
                    success: function(res) {
                        Helper.successNotif('Success');
                        Helper.redirectTo('/admin/banner/show');
                    },
                    error: function(res) {
                        alert("Something went wrong");
                        console.log(res);
                        Helper.loadingStop();
                    }
                })
            })
            
            e.preventDefault()
        })
</script>
@endsection