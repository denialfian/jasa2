@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
            </div>
        </div>
        <div class="card-body">
            <form id="form-upload-banner" enctype="multipart/form-data">
                <label class="" for="">
                    <b>Banner Upload</b>.
                </label>
                <input type="file" name="images[]" id="filer_input2" multiple="multiple">
                <br>
                <button type="submit" class="btn btn-primary btn-block" id="banner_save">Upload</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<link rel="stylesheet" href="{{ asset('adminAssets/css/jquery.filer.css') }}">
<link rel="stylesheet" href="{{ asset('adminAssets/css/themes/jquery.filer-dragdropbox-theme.css') }}">
<script type="text/javascript" src="{{ asset('adminAssets/js/filer/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminAssets/js/filer/jquery.filer.min.js') }}"></script>

<script>
    function save(e) {
        var form = new FormData($('#form-upload-banner')[0]);
        console.log(form)
        Helper.loadingStart();
        Axios.post(Helper.apiUrl('/admin/galery/upload-banner'), form, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function(response) {
                Helper.successNotif('Data Has Been Saved');
                window.location.href = Helper.redirectUrl('/admin/banner/show');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    }

    $(document).on('click', '#banner_save', function(e) {
        save(e);
    });
</script>
@endsection