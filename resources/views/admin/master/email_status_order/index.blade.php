@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                {{-- <a href="{{ url('admin/email-status-order/create') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Template</a> --}}
            </div>

        </div>

        <div class="card-body">
        <table style="width: 100%;" id="table-email-status-order" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Order Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
<style>
    .table-email-status-order-delete {
        display:none;
    }
</style>
<script>
        globalCRUD.datatables({
            url: '/email-status-order/datatables',
            selector: '#table-email-status-order',
            columnsField: ['orders_statuses_id', 'order_status.name'],
            actionLink: {
                update: function(row) {
                    return "/admin/email-status-order/edit/" + row.orders_statuses_id;
                },
                delete: function(row) {
                    return "/email-status-order/" + row.orders_statuses_id;
                }
            }
        })
    </script>
@endsection
