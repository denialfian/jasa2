@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-jasa-excel" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
            <button class="btn btn-primary btn-sm add-jasa-excel"><i class="fa fa-plus"></i> ADD Jasa Excel</button>
        </div>
    </div>
</div>
@include('admin.master.jasa-excel.jasa-excelModal')
@endsection

@section('script')
<script>
    Helper.onlyNumberInput('.number_only')
    globalCRUD.datatables({
        url: '/jasa-excel/datatables',
        selector: '#table-jasa-excel',
        columnsField: [
            'DT_RowIndex',
            'name',
            {
                data: "price",
                name: "price",
                render: function(data,type,full) {
                    return 'Rp.'+Helper.toCurrency(data);
                }
            },
            'desc'
        ],
        modalSelector: "#modal-jasa-excel",
        modalButtonSelector: ".add-jasa-excel",
        modalFormSelector: "#form-jasa-excel",
        actionLink: {
            store: function() {
                return "/jasa-excel";
            },
            update: function(row) {
                return "/jasa-excel/" + row.id;
            },
            delete: function(row) {
                return "/jasa-excel/" + row.id;
            },
        }
    })

</script>
@endsection
