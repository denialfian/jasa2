@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <button class="btn btn-primary btn-sm add-ticket">ADD TICKET</button>
            </div>
        </div>
        <div class="card-body">
            <table id="table-ticket" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>ID</th>
                        <th>TICKET NO</th>
                        <th>SUBJECT</th>
                        <th>USER</th>
                        <th>STATUS</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<form id="form-ticket">
    <div class="modal fade" id="modal-ticket" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Ticket</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="position-relative form-group">
                        <label class="" for="">User</label>
                        <select class="form-control user-select" id="user_id" name="user_id" required="" style="width:100%">
                            <option selected="" value="">
                                Pilih User
                            </option>
                        </select>
                    </div>
                    <div class="position-relative form-group">
                        <label class="" for="">Subject</label>
                        <input name="subject" class="form-control" />
                    </div>
                    <div class="position-relative form-group">
                        <label class="" for="">Message</label>
                        <textarea name="desc" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
    // agar user profile bisa diclik
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })

    globalCRUD.select2(".user-select", '/user/teknisi-select2');

    globalCRUD.datatables({
        url: '/tickets/datatables',
        selector: '#table-ticket',
        orderBy: [1, 'desc'],
        columnsField: [
            'DT_RowIndex',
            {
                data: "id",
                name: "id",
                visible: false,
            },
            {
                data: "ticket_no",
                name: "ticket_no",
                render: function(data, type, full) {
                    return "<a href='/admin/tickets/"+ full.id + "'>"+full.ticket_no+"</a>";
                }
            },
            {
                data: "subject",
                name: "subject",
                orderable: false,
                searchable: false,
            },
            'user.name', 
            {
                data: "status",
                name: "status",
                sortable: false,
                render: function(data, type, full) {
                    switch (full.status) {
                        case '1':
                            return '<span class="btn btn-sm btn-primary">open</span>';
                            break;
                        
                        case '2':
                            return '<span class="btn btn-sm btn-waring">on progress</span>';
                            break;

                        case '3':
                            return '<span class="btn btn-sm btn-waring">on resolving</span>';
                            break;

                        case '4':
                            return '<span class="btn btn-sm btn-success">done</span> ';
                            break;

                        case '5':
                            return '<span class="btn btn-sm btn-danger">closed</span> ';
                            break;

                        default:
                            return full.status;
                            break;
                    }
                }
            }
        ],
        modalSelector: "#modal-ticket",
        modalButtonSelector: ".add-ticket",
        modalFormSelector: "#form-ticket",
        actionLink: {
            store: function() {
                return "/tickets";
            },
            update: function(row) {
                return "/tickets/" + row.id;
            },
            delete: function(row) {
                return "/tickets/" + row.id;
            },
        },
    })
</script>
@endsection
