@extends('admin.home')
@section('content')
<style>
    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 100%;
}
</style>
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/bank/show') }}" class="mb-2 mr-2 btn btn-primary add-bank-transfer" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
        <form id="form-save-midtrans-key">
            <div class="card-body">
                @foreach ($showKey as $item)
                    <div class="position-relative row form-group" id="districShow">
                        @if ($item->name == 'timer_expired_midtrans')
                            <label for="exampleEmail" class="col-sm-3 col-form-label">Timer Setting</label>
                        @else
                            <label for="exampleEmail" class="col-sm-3 col-form-label">{{ $item->name == 'server_key_midtrans' ? 'Server Keys' : 'Client Key' }}</label>
                        @endif
                        <div class="col-sm-9">
                            @if ($item->name == 'server_key_midtrans')
                                <input name="value[]" type="text" value="{{ $item->value }}" class="form-control">
                                <input name="id[]" type="hidden" value="{{ $item->id }}" class="form-control">
                            @endif

                            @if ($item->name == 'client_keys')
                                <input name="value[]" type="text" value="{{ $item->value }}" class="form-control">
                                <input name="id[]" type="hidden" value="{{ $item->id }}" class="form-control">
                            @endif

                            @if ($item->name == 'timer_expired_midtrans')
                                <input name="value[]" type="text" value="{{ $item->value }}" class="form-control">
                                <input name="id[]" type="hidden" value="{{ $item->id }}" class="form-control">
                                <small><strong>Timer Expired Payment in the form of a clock</strong></small>
                            @endif
                        </div>
                    </div>
                @endforeach

                <div class="position-relative row form-check">
                    <div class="col-sm-10 offset-sm-2">
                        <button class="btn btn-success" id="save" style="float:right"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                    </div>
                </div><br>
            </div>
        </form>

    </div>
</div>
@endsection

@section('script')
    <script>
        $('#form-save-midtrans-key').submit(function(e){
            data = Helper.serializeForm($(this));
            Helper.loadingStart();

            // post data
            Axios.post(Helper.apiUrl('/general-setting/save_key_midtrans'), data)
                .then(function(response) {
                    Helper.successNotif('Success, Keys Has been saved');
                    window.location.href = Helper.redirectUrl('/admin/bank/midtrans-settings');
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });

        e.preventDefault();
        })
    </script>
@endsection
