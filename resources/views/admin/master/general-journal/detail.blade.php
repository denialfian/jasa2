@extends('admin.home')
@section('content')
<style>
    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 100%;
}
</style>

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/general-journal/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;">
    <input type="hidden" name="id" value="{{ $getData->id }}">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Title</label>
                    <div class="col-sm-12">
                        <input name="title" placeholder="Title" type="text" class="form-control" value="{{ $getData->title }}" disabled>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Code</label>
                    <div class="col-sm-12">
                        <input name="code" placeholder="Title" type="text" class="form-control" value="{{ $getData->code }}" disabled>
                    </div>
                </div>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-header header-border">
                Table Details
                <div class="btn-actions-pane-right text-right">
                    <button type="button" class="btn btn-success btn_add_details"><i class="fa fa-plus"></i></button>
                </div>
            </div>

            <div class="card-body">
                <table class="table" id="table-details">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Date</th>
                            <th>ASC Name</th>
                            <th>Category</th>
                            <th>Description</th>
                            <th width=10%>Qty</th>
                            <th>Debit</th>
                            <th>Credit</th>
                            <th>Balance</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="dynamic_details">
                        <input type="hidden" value="{{ count($detailsData) }}" id="count_details_data" />
                        @foreach($detailsData as $key => $details)
                        <?php $uniq = $key+1; ?>
                            <tr class="dynamic_details_content" id="details_row_{{ $uniq }}">
                                <input type="hidden" value="{{ $details->id }}" name="details_id[]" disabled/>
                                <td><span class="counter">{{ $uniq }}</span></td>
                                <td>
                                    <input type="text" name="date[]" data-uniq="{{ $uniq }}" class="form-control date_format date_input-{{ $uniq }}" value="{{ $details->date->format('d-m-Y') }}" disabled/>
                                </td>
                                <td width="10%">
                                    <select name="asc_id[]" class="form-control asc_id_input-{{ $uniq }}"  data-uniq="{{ $uniq }}" required style="width:100%" disabled>
                                        @if($details->asc_id != null)
                                        <option value="{{ $details->asc_id }}" >{{ $details->asc->name }}</option>
                                        @else
                                        <option value="kosong" >Kosong</option>
                                        @endif
                                    </select>
                                </td>
                                <td>
                                    <input type="text" placeholder="category" name="category[]" data-uniq="{{ $uniq }}" class="form-control category_input-{{ $uniq }}" value="{{ $details->category }}" disabled/>
                                </td>
                                <td>
                                    @if($details->category == "operational")
                                        <input type="text" placeholder="description" name="description[]" data-uniq="{{ $uniq }}" class="form-control description_input-{{ $uniq }}" value="{{ $details->description }}" disabled/>

                                    @else
                                        <div class="sparepart_select-{{ $uniq }}">
                                            <select name="part_data_stock_inventories_id" class="form-control part_data_stock_inventories_id_input-{{ $uniq }}" style="width:100%" data-uniq="{{ $uniq }}" disabled>
                                                @if($details->part_data_stock_inventories_id != null)
                                                    <option value="{{ $details->part_data_stock_inventories_id }}" >{{ $details->part_data_stock_inventory->part_data_stock->part_description }} </option>
                                                @else
                                                    <option value="" >Kosong</option>
                                                @endif
                                            </select>
                                        </div>
                                    @endif
                                </td>
                                <td>
                                    <input type="text" placeholder="quantity" name="quantity[]" data-uniq="{{ $uniq }}" class="form-control quantity_input-{{ $uniq }}" value="{{ $details->quantity }}" disabled/>
                                </td>
                                <td class="debit">
                                    <input type="text" placeholder="debit" name="debit[]" data-uniq="{{ $uniq }}" class="form-control number_only debit_input-{{ $uniq }}" value="{{ $details->debit }}" disabled/>
                                </td>
                                <td class="kredit">
                                    <input type="text" placeholder="kredit" name="kredit[]" data-uniq="{{ $uniq }}" class="form-control number_only kredit_input-{{ $uniq }}" value="{{ $details->kredit }}" disabled/>
                                </td>
                                <td class="balance">
                                    <input type="text" placeholder="balance" name="balance[]" data-uniq="{{ $uniq }}" class="form-control number_only balance_input-{{ $uniq }}" value="{{ $details->balance }}" disabled/>
                                </td>
                                <td>
                                @if($details->filename != null)
                                    <a download data-fancybox href="{{ url('/storage/general-journal-detail/'.$details->filename.'') }}">img</a>
                                @endif
                                </td>

                            </tr>
                        @endforeach
                        <tfoot>
                            <tr>
                                <td colspan="2">Ending</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><input name="total_debit" placeholder="" type="text" class="form-control" id="total_debit" disabled></td>
                                <td><input name="total_kredit" placeholder="" type="text" class="form-control" id="total_kredit" disabled></td>
                                <td><input name="total_balance" placeholder="" type="text" class="form-control" id="total_balance" disabled></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card" style="margin-top: 10px;">
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Image</label>
                    <div class="col-sm-3">
                        <div class="col-sm-3">
                            @if($getData->image == null )
                                <img id="img-upload" src="http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png" alt="your image" style="width:100px;height:100px"/>
                            @else
                                <img id='img-upload' src="{{asset('/storage/general-journal/' . $getData->image)}}" alt="your image" style="width:125px;height:125px">
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</form>

<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            General Journal Logs
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="general-journal-logs" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Type Modified</th>
                        <th>By User (Name)</th>
                        <th>Created At</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


@endsection

@section('script')
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

<script>
    Helper.hideSidebar();
    $(document).ready(function() {
        ClassApp.hitungBalance();
        Helper.onlyNumberInput('.number_only');
    })

    var id = $('input[name="id"]').val();
    $(function() {
        $('.date_format').datetimepicker({
            timepicker:false,
            format:'d-m-Y',
            mask:true
        });
    });

    var ClassApp = {
        hitungBalance: function() {
            // var balance = 0;
            var currentBalance = 0;
            var totalDebit = 0;
            var totalKredit = 0;
            $('input[name="balance[]"]').each(function() {
                var uniq = $(this).attr('data-uniq');
                var debit = $('.debit_input-' + uniq).val() == '' ? 0 : +parseInt($('.debit_input-' + uniq).val());
                var kredit = $('.kredit_input-' + uniq).val() == '' ? 0 : -parseInt($('.kredit_input-' + uniq).val());

                currentBalance += debit + kredit;
                totalDebit += debit;
                totalKredit += kredit;
                $('.balance_input-' + uniq).val(currentBalance);
                $('#total_debit').val(totalDebit);
                $('#total_kredit').val(totalKredit);
                $('#total_balance').val(currentBalance);

            })
        },

    }

    globalCRUD.datatables({
        url: '/general-journal/datatables_logs/'+id+'',
        selector: '#general-journal-logs',
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex"
        },
        {
            data: "type",
            name: "type",
            render: function(row,data,full) {
                if(row == 1) {
                    return 'Created';
                } else if(row == 2) {
                    return 'Updated'
                } else {
                    return 'Deleted';
                }
            }
        },
        {
            data: "user.name",
            name: "user.name",
        },
        {
            data: "created_at",
            name: "created_at",
            render: function(row,data,full) {
                return moment(row).format("DD MMMM YYYY HH:mm");
            }
        }
        ]
    })
</script>



@endsection
