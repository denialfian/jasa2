@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card header-border">
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-3">
                        <input type="hidden" name="pdes_id" id="pdes_id" value="{{ $details[0]->part_data_stock_id }}">
                        <div class="position-relative form-group">
                            <label for="" class="">Months</label>
                            <select class="form-control" name="month" id="month">
                                <option value="{{ $month }}" selected>{{ date("F", mktime(0, 0, 0, $month, 1)) }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="">Years</label>
                            <select class="form-control" name="year" id="year">
                                @for($year=intval(date("Y")); $year > (intval(date("Y")) - 2); $year--)
                                    <option value="{{$year}}" <?php if($year == date("Y")) { echo 'selected'; } ?>>{{$year}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="">Margin Sell Items Rate Auto (%)</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control number_only" name="margin_rate_auto" id="margin_rate_auto" max="100" min="0" value="{{ $margin_rate_auto }}">
                                <div class="input-group-append">
                                    <button class="btn btn-danger" type="button" id="update_margin_rate_auto">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="">HPP Avg Mnth Auto = <span class="text-primary" id="hpp_average_month_auto">Rp. {{ \App\Helpers\thousanSparator($hpp_avg_month) }}</span></label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control number_only" name="hpp_average_month_manual" id="hpp_average_month_manual" max="100" min="0" value="{{ $hpp_avg_month_manual }}">
                                <div class="input-group-append">
                                    <button class="btn btn-danger" type="button" id="update_hpp_average_manual_month">Submit Manual</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger btn-sm mt-2" type="submit">Search</button>
            </form>
        </div>
    </div>
</div>
<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Report HPP Items ({{ $details[0]->part_data_stock->part_description }})
            </div>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        <table id="table" class="display table table-hover table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Items</th>
                                    <th>Qty</th>
                                    <th>Total Price</th>
                                    <th>Hpp Average /pcs</th>
                                    <th>Setting Margin Rate Auto (%)</th>
                                    <th>Setting Margin Rate (Manual)</th>
                                    <th>Suggest To Sell</th>
                                    <th>Suggest To Sell (Manual)</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>

     // var option_remove = [];
     var data = [
        {id: '1', text: 'January', title: 'January'},
        {id: '2', text: 'February', title: 'February'},
        {id: '3', text: 'March', title: 'March'},
        {id: '4', text: 'April', title: 'April'},
        {id: '5', text: 'May', title: 'May'},
        {id: '6', text: 'June', title: 'June'},
        {id: '7', text: 'July', title: 'July'},
        {id: '8', text: 'August', title: 'August'},
        {id: '9', text: 'September', title: 'September'},
        {id: '10', text: 'October', title: 'October'},
        {id: '11', text: 'November', title: 'November'},
        {id: '12', text: 'December', title: 'December'},
    ];

    $("#month").select2({
        data: data,
        escapeMarkup: function(markup) {
            return markup;
        }
    })

    $("#margin_rate_auto").keyup(function() {
        var max = parseInt($(this).attr('max'));
        var min = parseInt($(this).attr('min'));
        if ($(this).val() > max) {
            $(this).val(max);
        } else if ($(this).val() < min) {
            $(this).val(min);
        }
    });

    $(".margin_rate_manual").keyup(function() {
        var max = parseInt($(this).attr('max'));
        var min = parseInt($(this).attr('min'));
        if ($(this).val() > max) {
            $(this).val(max);
        } else if ($(this).val() < min) {
            $(this).val(min);
        }
    });

    var tbl = globalCRUD.datatables({
        url: '/report-hpp-items/datatables/details/'+$('#pdes_id').val()+'/?month='+$('#month').val()+'&year='+$('#year').val()+'',
        selector: '#table',
        actionLink: "",
        columnsField: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',

            },
            {
                data: 'part_description',
                name: 'part_description',
            },
            {
                data: 'quantity',
                name: 'quantity',
            },
            { //total price
                data: 'kredit',
                name: 'kredit',
                render: function(data, type, row) {
                    return 'Rp. '+Helper.toCurrency(data);
                }
            },
            {
                data: 'hpp_average',
                name: 'hpp_average',
                render: function(data, type, row) {
                    return 'Rp. '+Helper.toCurrency(data);
                }
            },
            {
                data: 'margin_rate_auto',
                name: 'margin_rate_auto',
                render: function(data, type, row) {
                    var nilai = data;
                    if(row.margin_rate_manual != null && row.margin_rate_manual != 0) {
                        return '<span class="text-secondary">'+nilai+'</span>';
                    } else {
                        return nilai;
                    }

                }
            },
            {
                data: 'margin_rate_manual',
                name: 'margin_rate_manual',
                render: function(data, type, row) { //margin_rate_manual
                    var nilai = data != null ? data : 0;
                    Helper.onlyNumberInput('.number_only');
                    return `<div class='form-row'>
                                <div class='col-md-8'>
                                    <input type='text' class='form-control form-control-sm number_only margin_rate_manual_${row.gjd_id}' name='margin_rate_manual' data-gjd_id="${row.gjd_id}" id='' max='100' min='0' value=${nilai}>
                                </div>
                                <div class='col-md-4'>
                                    <button class='btn btn-primary btn-sm mr-2 update_margin_rate' data-part_description="${row.part_description}"  data-gjd_id="${row.gjd_id}" title='Update Margin Rate'>confirm</button>
                                </div>
                            </div>`;

                }
            },
            {
                render: function(data, type, row) { //suggest_to_sell

                    if(row.margin_rate_manual != null) {
                        if(row.margin_rate_manual != null || row.margin_rate_manual != 0) {
                            nilai = Math.ceil(row.hpp_average + ((row.hpp_average * row.margin_rate_manual) / 100));
                        }
                        if(row.margin_rate_manual == 0) {
                            nilai = row.hpp_average
                        }
                    } else {
                        nilai = Math.ceil(row.hpp_average + ((row.hpp_average * row.margin_rate_auto) / 100));
                    }
                    return 'Rp. '+Helper.toCurrency(nilai);
                }
            },
            {
                data: 'suggest_to_sell_manual',
                name: 'suggest_to_sell_manual',
                render: function(data, type, row) { //suggest_to_sell_manual
                    var nilai = data != null ? data : 0;
                    return `<div class='form-row'>
                                <div class='col-md-8'>
                                    <input type='text' class='form-control form-control-sm suggest_to_sell_manual_${row.gjd_id} number_only currency' name='suggest_to_sell_manual' data-gjd_id="${row.gjd_id}" id='' max='100' min='0' value=${nilai}>
                                </div>
                                <div class='col-md-4'>
                                    <button class='btn btn-primary btn-sm mr-2 update_suggest_to_sell_manual' data-part_description="${row.part_description}"  data-gjd_id="${row.gjd_id}" title='Update Margin Rate'>confirm</button>
                                </div>
                            </div>`;
                }
            },
        ]
    });

    function checkValidation() {
        var isValid = true;
        var month = $('#month').val();
        var year = $('#year').val();
        var margin_rate_auto = $('#margin_rate_auto').val();
        var hpp_average_month_manual = $('#hpp_average_month_manual').val();
        if(month == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Month !');
            return false;
        } else if (year == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Year !');
            return false;
        } else if (margin_rate_auto == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Margin Sell Auto !');
            return false;
        }
        else if (margin_rate_auto == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Hpp Average Month Manual !');
            return false;
        }
        return isValid;
    }

    function getHppAutoAndManualMonth() {
        $.ajax({
            url:Helper.apiUrl('/report-hpp-items/get/hpp'),
            type: 'post',
            data: {
                month : $('#month').val(),
                year : $('#year').val(),
                pdes_id : $('#pdes_id').val(),
                type : 1
            },
            success: function(response) {
                console.log(response);
                if (response != 0) {
                    $('#hpp_average_month_auto').html('Rp '+Helper.toCurrency(response.data.hpp_average_month_auto))
                    $('#hpp_average_month_manual').val(response.data.hpp_average_month_manual)
                }
            },
            error: function(xhr, status, error) {
                handleErrorResponse(error);
            },
        });
    }

    $("#form-search").submit(function(e) {
        e.preventDefault();
        if(checkValidation()) {
            var input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/report-hpp-items/datatables/details/'+$('#pdes_id').val()+'/' + playload);
            tbl.table.reloadTable(url);
        }
    });

    $(document).on('click', '#update_margin_rate_auto', function(e) {
        Helper.confirm(function() {
            $.ajax({
                url:Helper.apiUrl('/report-hpp-items/update/margin_rate_auto'),
                type: 'post',
                data: {
                    margin_rate_auto : $('#margin_rate_auto').val(),
                },
                success: function(response) {
                    console.log(response);
                    if (response != 0) {
                        Helper.successNotif('Success Update Margin Rate Auto');
                        tbl.table.reloadTable();
                    }
                },
                error: function(xhr, status, error) {
                    handleErrorResponse(error);
                },
            });
            e.preventDefault();
        });
    });

    $(document).on('click', '#update_hpp_average_manual_month', function(e) {
        Helper.confirm(function() {
            $.ajax({
                url:Helper.apiUrl('/report-hpp-items/update/hpp_average_manual_month'),
                type: 'post',
                data: {
                    pdes_id: $('#pdes_id').val(),
                    month : $('#month').val(),
                    year: $('#year').val(),
                    hpp_average_month_manual : $('#hpp_average_month_manual').val()
                },
                success: function(response) {
                    console.log(response);
                    if (response != 0) {
                        Helper.successNotif('Success Update HPP Average Manual /Month');
                        tbl.table.reloadTable();
                    }
                },
                error: function(xhr, status, error) {
                    handleErrorResponse(error);
                },
            });

            e.preventDefault();
        });
    });

    $(document).on('click', '.update_margin_rate', function(e) {
        var $this = $(this);
        Helper.confirm(function() {
            Helper.unMask('.currency');
            $.ajax({
                url:Helper.apiUrl('/report-hpp-items/details/update/margin_rate_manual'),
                type: 'post',
                data: {
                    gjd_id : $this.attr('data-gjd_id'),
                    margin_rate_manual : $('.margin_rate_manual_'+$this.attr('data-gjd_id')+'').val(),
                    pdes_id : $this.attr('data-pdes_id'),
                    month : $('#month').val(),
                    year : $('#year').val(),
                },
                success: function(response) {
                    console.log(response);
                    if (response != 0) {
                        Helper.successNotif('Success update Margin Sell Manual pada Part Description = '+$this.attr('data-part_description')+'');
                        tbl.table.reloadTable();
                    }
                },
                error: function(xhr, status, error) {
                    handleErrorResponse(error);
                },
            });

            e.preventDefault();
        });
    });

    $(document).on('click', '.update_suggest_to_sell_manual', function(e) {
        var $this = $(this);
        Helper.confirm(function() {
            Helper.unMask('.currency');
            $.ajax({
                url:Helper.apiUrl('/report-hpp-items/details/update/suggest_to_sell_manual'),
                type: 'post',
                data: {
                    gjd_id :  $this.attr('data-gjd_id'),
                    suggest_to_sell_manual : $('.suggest_to_sell_manual_'+$this.attr('data-gjd_id')+'').val(),
                    pdes_id : $this.attr('data-pdes_id'),
                    month : $('#month').val(),
                    year : $('#year').val(),
                },
                success: function(response) {
                    console.log(response);
                    if (response != 0) {
                        Helper.successNotif('Success update Suggest To Sell Manual pada Part Description = '+$this.attr('data-part_description')+'');
                        tbl.table.reloadTable();
                    }
                },
                error: function(xhr, status, error) {
                    handleErrorResponse(error);
                },
            });

            e.preventDefault();
        });
    });



</script>
@endsection
