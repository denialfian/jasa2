@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize" style="margin-left:150px;">
                <form id="form-search">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Fr</span>
                                </div>
                                <input type="text" name="date_from" class="form-control date_format" readonly required/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">To</span>
                                </div>
                                <input type="text" name="date_to" class="form-control date_format" readonly required/>
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <div role="group" class="mb-2 btn-group-sm btn-group btn-group-toggle">
                                <button type="submit" class="mb-1 mr-1 btn btn-sm btn-primary btn-gradient-alternate"><i class="fa fa-search"></i> Filter </button>
                                <button type="button" class="mb-1 mr-1 btn btn-sm btn-success btn-gradient-alternate" id="export_data"><i class="fa fa-download"></i> Export Data </a></button>
                                {{-- <a href="{{ url('admin/excel-income/create') }}" class="mb-1 mr-1 btn btn-sm btn-primary btn-gradient-alternate"><i class="fa fa-plus"></i> Add </a> --}}
                            </div>
                        </div>
                    </div>
                </form>
            </div>


        </div>

        <div class="card-body">
        <table style="width: 100%;" id="table-hpp" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th>No.</th>
                        <th>Code Material</th>
                        <th>Part Name</th>
                        <th>Is Bundle?</th>
                        <th>Hpp Price</th>
                        <th>Selling Pcs / Main Price</th>
                        <th>Sold to Customer</th>
                        <th>Qty</th>
                        <th>Orders Code</th>
                        <th>Closed Date</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tfoot align="left">
                    <tr><th colspan=5></th><th></th><th></th><th></th><th></th><th colspan=3></th></tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
<style>

    tr.shown .details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>
<script>
     $(function() {
        $('.date_format').datetimepicker({
            timepicker:false,
            format:'d-m-Y',
            allowBlank: false,
        });

        var d = new Date();
        var startDate = d.setMonth(d.getMonth() - 1);
        var e = new Date();

        $('input[name="date_from"]').val(moment(startDate).format('DD-MM-YYYY'));
        $('input[name="date_to"]').val(moment(e.setDate(e.getDate())).format('DD-MM-YYYY'));

    });

    var tableHpp = $('#table-hpp').DataTable({
        ajax: {
            url: Helper.apiUrl('/report-hpp/datatables'),
            type: "get",
            error: function() {},
            complete: function() {},
        },
        serverSide: true,
        processing: true,
        selector: '#table-hpp',
        "footerCallback" : function ( row, data, start, end, display ) {
            var api = this.api(), data;
            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace('.', '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            // computing column Total of the complete result
            var Total5 = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var Total6 = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var Total7 = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var Total8 = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

                // Update footer by showing the total with the reference of the column index
            $( api.column( 0 ).footer() ).html('Total');
            $( api.column( 5 ).footer() ).html('Rp. '+Helper.toCurrency(Total5));
            $( api.column( 6 ).footer() ).html('Rp. '+Helper.toCurrency(Total6));
            $( api.column( 7 ).footer() ).html('Rp. '+Helper.toCurrency(Total7));
            $( api.column( 8 ).footer() ).html(Helper.toCurrency(Total8));
        },
        columns: [
            {
                "className": 'text-center',
                render: function(row,type,full) {
                    if(full.is_bundle == 1 || (full.labors.length > 0)) {
                        return '<button class="btn btn-sm btn-primary details-control"><i class="fa fa-plus"></i></button>';
                    } else {
                        return '';
                    }
                }
            },
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
            },
            {
                data: "code_material",
                name: "code_material",
            },
            {
                data: "part_description",
                name: "part_description",
            },
            {
                render: function(row,type,full) {
                    if(full.is_bundle == 1) {
                        return 'Yes <br>'+full.bundle_name+''
                    } else {
                        return 'No'
                    }
                }
            },
            {
                data: "hpp_order",
                name: "hpp_order",
                render: function(row,type,full) { //hpp
                    if(row != null) {
                        return 'Rp. '+Helper.toCurrency(Math.ceil(row));
                    } else {
                        return 'Rp. '+0;
                    }

                }
            },
            {
                data: "spp_or_mpb",
                name: "spp_or_mpb",
                render: function(row,type,full) { //selling_price_pcs / main_price_bundle
                    var nilai=0;
                    if(row != null) {
                        nilai = row;
                        return 'Rp. '+Helper.toCurrency(Math.ceil(nilai));
                    } else {
                        return 'Rp. '+nilai;
                    }
                }
            },
            {
                data: "selling_price",
                name: "selling_price",
                render: function(row,type,full) { //selling_price sold to customer
                    var nilai=0;
                    if(row != null) {
                        nilai = row;
                        return 'Rp. '+Helper.toCurrency(Math.ceil(nilai));
                    } else {
                        return 'Rp. '+nilai;
                    }
                }
            },
            {
                //qty
                data: "amount",
                name: "amount",
            },
            {
                data: "service_order_no",
                name: "service_order_no",
            },
            {
                data: 'closed_date',
                name: 'closed_date',
            },
            {
                data: 'created_at',
                name: 'created_at',
            },
        ]
    });

    function checkValidation() {
        var isValid = true;
        var date_from = $('input[name="date_from"]').val();
        var date_to = $('input[name="date_to"]').val();
        if(date_from == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Date Fr !');
            return false;
        } else if (date_to == "") {
            isValid = false;
            Helper.warningNotif('Please fill or select your Date To !');
            return false;
        }
        return isValid;
    }

    function templatex(d) {
        console.log(d)
        template = '';
        template = '<div class="row m-3">';
        template += '<div class="col-md-6">';
        template += '<h5>Bundle Details</h5>';
        template += '<table class="table" id="example">';
        template += '<tr>';
        template += '<td>No</td>';
        template += '<td>Code Material</td>';
        template += '<td>Parts Description</td>';
        template += '<td>Quantity</td>';
        template += '<td>Price Hpp Average</td>';
        template += '<td>Selling Price</td>';
        template += '<td>Created at</td>';
        template += '</tr>';

        _.each(d.bundle_details, function(i, key) {
            template += '<tr>';
            template += '<td>' + (key + 1) + '</td>';
            template += '<td>' + i.code_material + '</td>';
            template += '<td>' + i.part_description + '</td>';
            template += '<td>' + i.quantity + '</td>';
            template += '<td>' + 'Rp. '+Helper.toCurrency(i.hpp_average != null ? i.hpp_average: 0) + '</td>';
            template += '<td>' + 'Rp. '+Helper.toCurrency(i.selling_price != null ? i.selling_price: 0) + '</td>';
            template += '<td>' + i.created_at + '</td>';
            template += '</tr>';
        })
        template += '</table>';
        template += '</div>';

        template += '<div class="col-md-6">';
        template += '<h5>Labor Details</h5>';
        template += '<table class="table">';
        template += '<tr>';
        template += '<td>No</td>';
        template += '<td>Jasa Name</td>';
        template += '<td>Description</td>';
        template += '<td>Price</td>';
        template += '<td>Created at</td>';
        template += '</tr>';

        _.each(d.labors, function(i, key) {
            template += '<tr>';
            template += '<td>' + (key + 1) + '</td>';
            template += '<td>' + i.jasa_name + '</td>';
            template += '<td>' + i.description + '</td>';
            template += '<td>' + 'Rp. '+Helper.toCurrency(i.price != null ? i.price: 0) + '</td>';
            template += '<td>' + i.created_at + '</td>';
            template += '</tr>';
        })
        template += '</table>';
        template += '</div>';
        template += '</div>';

        return template;
    }
    var table = $('#table-hpp').DataTable();
    $('#table-hpp tbody').on('click', '.details-control', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(templatex(row.data())).show();
            $(row.child()).addClass('smalltable');
            tr.addClass('shown');
        }
    });

    $("#export_data").click(function(e) {
        var date_from = $('input[name="date_from"]').val();
        var date_to = $('input[name="date_to"]').val();
        var url = '/report-hpp/export';
        if(checkValidation()) {
            url = '/admin/report-hpp/export?date_from='+date_from+'&date_to='+date_to+'';
            Helper.redirectTo(url);
        }
        // console.log(url);

        e.preventDefault();
    });

    $("#form-search").submit(function(e) {
        e.preventDefault();
        if(checkValidation()) {
            var input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/report-hpp/datatables' + playload);
            // tbl.table.reloadTable(url);
            tableHpp.ajax.url(url).load();
        }

    });
    </script>


@endsection
