@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <!-- <button class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Add New Grade</button> -->
            </div>
        </div>
        <div class="card-body">
            <table id="table-grade" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Service Type</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Add New</button>
        </div>
    </div>
</div>
<form id="form-grade">
    <div class="modal fade" id="modal-grade" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Product Group</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label for="exampleEmail" class="col-sm-6 col-form-label">Name Product Group</label>
                        <div class="col-sm-9">
                            <input name="name" placeholder="name" id="name" type="text" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleEmail" class="col-sm-6 col-form-label">Service Type</label>
                        <div class="col-sm-9">
                            <select class="form-control service_type-select" id="ms_services_type_id" name="ms_services_type_id" style="width:100%" required>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleEmail" class="col-sm-3 col-form-label">Image</label>
                        <div class="col-sm-9" style="position:relative;">
                            <a class='btn btn-primary' href='javascript:;'>
                                Choose File...
                                <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file" id="file" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                            </a>
                            &nbsp;
                            <span class='badge badge-info badge-sm' id="upload-file-info"></span>
                        </div>
                        <div class="col-sm-3">
                            <br>
                            <img id='img-upload' src="http://www.clker.com/cliparts/c/W/h/n/P/W/generic-image-file-icon-hi.png" style="width:125px;height:125px"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection

@section('script')
@include('admin.script.master._imageUploadScript')
<script>

    globalCRUD
        .select2Static(".service_type-select", '/service_type/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })


    globalCRUD.datatables({
        orderBy: [1, 'asc'],
        url: '/product_groups/datatables',
        selector: '#table-grade',
        columnsField: ['DT_RowIndex', 'name', 'service_type.name'],
        modalSelector: "#modal-grade",
        modalButtonSelector: ".add-grade",
        modalFormSelector: "#form-grade",
        upload: true,
        actionLink: {
            store: function() {
                return "/product_groups";
            },
            update: function(row) {
                return "/product_groups/" + row.id;
            },
            delete: function(row) {
                return "/product_groups/" + row.id;
            },
        }
    })
</script>
@endsection
