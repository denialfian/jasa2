@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/excel-income/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form style="display: contents;" autocomplete="off">
    <input type="hidden" name="id" value="{{ $getDetail->id }}">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-body">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Year</label>
                    <div class="col-sm-12">
                        <input name="year" type="text" value="{{ $getMaster->year }}" class="form-control number_only" readonly>
                    </div>
                </div>
                {{-- <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Code</label>
                    <div class="col-sm-12">
                        <input name="code" placeholder="Title" type="text" value="{{ $getMaster->code }}" class="form-control" readonly>
                    </div>
                </div> --}}
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Month</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="month" name="month" style="width:100%" disabled>
                            <option value="{{ $getMaster->month }}" selected>{{ $getMaster->month }}</option>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Description</label>
                    <div class="col-sm-12">
                        <textarea name="desc" class="form-control desc" maxlength="300" rows="5" readonly>{{ $getMaster->desc }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<form id="form-data" style="display: contents;" autocomplete="off">
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card" style="margin-top: 10px;">
        <div class="card-header header-border">
            Details
        </div>
        <div class="card-body">
            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-12 col-form-label">Date Transaction</label>
                <div class="col-sm-12">
                    <input name="date_transaction" type="text" class="form-control date_transaction_input-xxx date_format" data-uniq="xxx" value="{{ ($getDetail->date_transaction != null ? $getDetail->date_transaction->format('d-m-Y') : "") }}" required="required">
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-12 col-form-label">Service Order No.</label>
                <div class="col-sm-12">
                    <input name="source" placeholder="" type="text" class="form-control source_input-xxx" data-uniq="xxx" value="{{ $getDetail->source }}" required="required">
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-12 col-form-label">Defect Type Description</label>

                <div class="dynamic_description col-md-12">
                    @foreach($getDetail->details_desc as $key => $desc)
                        <input type="hidden" value="{{ count($getDetail->details_desc) }}" id="count_details_desc" />
                        <?php $uniq = uniqid(); ?>
                        <div class="input-group col-md-12 mb-2" id="my_description_row-{{ $uniq }}">
                            <div class="input-group-append col-md-12">
                                <input type="hidden" value="{{ $desc->id }}" name="description_id[]" />
                                <textarea name="description[]" placeholder="" type="text" class="form-control description-uniq-{{ $uniq }}" data-uniq="{{ $uniq }}" value="{{ $desc->description }}" required="required"></textarea>
                                @if($key == 0)
                                    <button class="btn btn-sm btn-success btn-add-description" type="button"><i class="fa fa-plus"></i></button>
                                @else
                                <button type="button" data-description_data_id="{{ $desc->id }}" data-uniq="{{ $uniq }}" class="btn-transition btn btn-sm btn-danger btn_remove_description"><i class="fa fa-close"></i></button>
                                @endif

                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-12 col-form-label">Date Transfer</label>
                <div class="col-sm-12">
                    <input name="date_transfer" type="text" class="form-control date_transfer_input-xxx date_format" data-uniq="xxx" value="{{ ($getDetail->date_transfer != null ? $getDetail->date_transfer->format('d-m-Y') : "") }}" required="required">
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-12 col-form-label">In</label>
                <div class="col-sm-12">
                    <input name="in_nominal" placeholder="In" type="text" class="form-control currency in_nominal_input-xxx" data-uniq="xxx" value="{{ $getDetail->in_nominal }}"
                    required="required">
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-12 col-form-label">Refund</label>
                <div class="col-sm-12">
                    <input name="refund" placeholder="Refund" type="text" class="form-control currency refund_input-xxx" data-uniq="xxx" value="{{ $getDetail->refund }}"
                    required="required">
                </div>
            </div>
            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-12 col-form-label">ASC Name</label>
                <div class="col-sm-12">
                    <input name="branch" placeholder="Astech Service Center Name" type="text" class="form-control branch_input-xxx" data-uniq="xxx" value="{{ $getDetail->branch }}" required="required">
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top: 10px;">
            <div class="card-footer ">
                <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> SAVE </button>
            </div>
        </div>
    </div>
</div>
</form>




@endsection

@section('script')
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
@include('admin.master.excel-income._js')
<script>
    // add description btn
    var id = $('input[name="id"]').val();

    var xx = 1;
    $(document).on('click', ".btn-add-description", function(e) {
        uniq_description = xx;
        ClassApp.templateNewInputDescription(uniq_description);
        _uniq_counter_description++;
    });

    // delete desciption data
    $(document).on('click', '.btn_remove_description', function() {
        ClassApp.deleteDescription($(this));
     });

    $("#form-data").submit(function(e) {
        ClassApp.updateDetail($(this),e,id);
    });
    $(function() {
        Helper.currency('.currency');
        Helper.dateFormat('.date_format');

    })
</script>

@endsection
