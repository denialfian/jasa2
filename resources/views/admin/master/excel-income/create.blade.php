@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/excel-income/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;" autocomplete="off">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-body">
                <input name="code" placeholder="Title" type="hidden" class="form-control" value="{{ $code }}" readonly>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Year</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="year" name="year" style="width:100%">
                            @for($year=intval(date("Y")); $year > (intval(date("Y")) - 5); $year--)
                                <option value="{{$year}}" <?php if($year == date("Y")) { echo 'selected'; } ?>>{{$year}}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                {{-- <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Code</label>
                    <div class="col-sm-12">
                        <input name="code" placeholder="Title" type="text" class="form-control" value="{{ $code }}" >
                    </div>
                </div> --}}
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Month</label>
                    <div class="col-sm-12">
                        <select name="month" class="form-control js-example-basic-single" style="width:100%" id="month">
                        </select>
                    </div>
                </div>
                 <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Keterangan</label>
                    <div class="col-sm-12">
                        <textarea name="desc" id="desc" class="form-control" cols="30" rows="5"></textarea>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-footer ">
            <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> CREATE </button>
        </div>
    </div>
</form>

@endsection

@section('script')
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
@include('admin.master.excel-income._js')
<script>
    $("#form-data").submit(function(e) {
        ClassApp.saveData($(this),e);
    });
</script>

@endsection
