@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/excel-income/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;" autocomplete="off">
    <input type="hidden" name="id" value="{{ $getData->id }}">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-body">
                <input name="code" placeholder="Title" type="hidden" class="form-control" value="{{ $getData->code }}" readonly>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Year</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="year" name="year" style="width:100%">
                            @for($year=intval(date("Y")); $year > (intval(date("Y")) - 5); $year--)
                                <option value="{{$year}}" <?php if($year == $getData->year) { echo 'selected'; } ?>>{{$year}}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                {{-- <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Code</label>
                    <div class="col-sm-12">
                        <input name="code" placeholder="Title" type="text" value="{{ $getData->code }}" class="form-control" readonly>
                    </div>
                </div> --}}
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Month</label>
                    <div class="col-sm-12">
                        <select class="form-control" id="month" name="month" style="width:100%">
                            <option value="{{ $getData->month }}" selected>{{ date("F", mktime(0, 0, 0, $getData->month, 1)) }}</option>
                        </select>
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-12 col-form-label">Description</label>
                    <div class="col-sm-12">
                        <textarea name="desc" class="form-control desc" maxlength="300" rows="5" >{{ $getData->desc }}</textarea>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-footer ">
            <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> SAVE </button>
        </div>
    </div>
</form>



<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Excel Income Logs
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="excel-income-logs" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Type Modified</th>
                        <th>By User (Name)</th>
                        <th>Created At</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


@endsection

@section('script')
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
@include('admin.master.excel-income._js')
<script>

    var id = $('input[name="id"]').val();
    $("#form-data").submit(function(e) {
        ClassApp.updateData($(this),e,id);
    });

    globalCRUD.datatables({
        url: '/excel-income/datatables_logs/'+id+'',
        selector: '#excel-income-logs',
        columns: [{
            data: "DT_RowIndex",
            name: "DT_RowIndex"
        },
        {
            data: "type",
            name: "type",
            render: function(row,data,full) {
                if(row == 1) {
                    return 'Created';
                } else if(row == 2) {
                    return 'Updated'
                } else {
                    return 'Deleted';
                }
            }
        },
        {
            data: "user.name",
            name: "user.name",
        },
        {
            data: "created_at",
            name: "created_at",
            render: function(row,data,full) {
                return moment(row).format("DD MMMM YYYY HH:mm");
            }
        }
        ]
    })

</script>

@endsection
