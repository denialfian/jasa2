@extends('admin.home')
@section('content')
    <div class="col-md-12 card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('/show-list-price-items') }}" class="btn btn-primary btn-sm">Back</a>
            </div>
        </div>
        <input type="hidden" name="id" value=" {{ $viewPriceItems->id }} ">
        <div class="card-body">
            <div class="position-relative row form-group" id="">
                <label for="exampleEmail" class="col-sm-2 col-form-label" >Unit Type</label>
                <div class="col-sm-10">
                    <select class="js-example-basic-single" id="unit_type" name="ms_unit_type_id" style="width:100%">
                        <option value="{{ $viewPriceItems->unit->id }}" >{{ $viewPriceItems->unit->unit_name }}</option>
                    </select>
                </div>
            </div>

            <div class="position-relative row form-group" id="productDisabled">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Product Name</label>
                <div class="col-sm-10">
                    <select class="js-example-basic-single" id="product" name="ms_product_id" style="width:100%">
                        <option value="{{ $viewPriceItems->product->id }}" >{{ $viewPriceItems->product->name }}</option>
                    </select>
                </div>
            </div>

            <div class="position-relative row form-group" id="cityDisabled">
                <label for="exampleEmail" class="col-sm-2 col-form-label" >City Name</label>
                <div class="col-sm-10">
                    <select class="js-example-basic-single" id="city" name="ms_city_id" style="width:100%">
                        <option value="{{ $viewPriceItems->city->id }}" >{{ $viewPriceItems->city->name }}</option>
                    </select>
                </div>
            </div>

            <div class="position-relative row form-group" id="districHide">
                <label for="exampleEmail" class="col-sm-2 col-form-label" >Distric Name</label>
                <div class="col-sm-10">
                    <select class="js-example-basic-single" id="distric" name="ms_district_id" style="width:100%">
                        <option value="{{ $viewPriceItems->district->id }}" >{{ $viewPriceItems->district->name }}</option>
                    </select>
                </div>
            </div>

            <div class="position-relative row form-group" id="vilageHide">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Village Name</label>
                <div class="col-sm-10">
                    <select class="js-example-basic-single" id="vilage" name="ms_village_id" style="width:100%">
                        <option value="{{ $viewPriceItems->village->id }}" >{{ $viewPriceItems->village->name }}</option>
                    </select>
                </div>
            </div>

            <div class="position-relative row form-group">
                <label for="exampleEmail" class="col-sm-2 col-form-label">Value / Price</label>
                <div class="col-sm-10">
                    <input name="value" id="value" value=" {{ $viewPriceItems->value }} "   placeholder="1000" type="text" class="form-control">
                </div>
            </div>
            <button type="submit" class="btn btn-primary waves-effect" style="float:right" id="update">SAVE CHANGES</button>
        </div>
    </div>
    <div class="d-block text-center card-footer"></div>
@endsection

@section('script')
@include('admin.master.priceItems.select2')
<script>
Helper.currency('#price');
$('#update').click(function(e){
    Helper.unMask('#price')
    id = $('input[name="id"]').val()
    $.ajax({
        type:'put',
        data:{
            value : $('input[name="value"]').val(),
            ms_unit_type_id : $('select[name="ms_unit_type_id"]').val(),
            ms_product_id : $('select[name="ms_product_id"]').val(),
            ms_city_id : $('select[name="ms_city_id"]').val(),
            ms_district_id : $('select[name="ms_district_id"]').val(),
            ms_village_id : $('select[name="ms_village_id"]').val(),
        },

        url:Helper.apiUrl('/price_items/' + id),
        success:function(html){
            swal.fire({
                type: 'success',
                title: 'Price Has Been Updated',
                icon: "success",
            });
            console.log(html)
            window.location.href = Helper.redirectUrl('/show-list-price-items');
        },
        error:function(xhr, status, error){
            if(xhr.status == 422){
                error = xhr.responseJSON.data;
                _.each(error, function(pesan, field){
                    $('#error-'+field).text(pesan[0])
                })
            }
        },
    });
    e.preventDefault();
})

function LoadSelectedValue() {
    id_city = $('#city').find(":selected").val();
    id_distric = $('#distric').find(":selected").val();
    id_village = $('#vilage').find(":selected").val();
    $('#city').val(id_city).trigger('change');
    $('#distric').val(id_distric).trigger('change');
    $('#vilage').val(id_village).trigger('change');
}
</script>
@endsection
