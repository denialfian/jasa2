<form id="form-price-item">
    <div class="modal fade" id="modal-price-item" data-backdrop="false" role="dialog" style="width:100">
        <div class="modal-dialog bd-example-modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Create Price Items</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">

                    {{-- <label>Nama</label>
                    <div class="form-group">
                        <input name="name" placeholder="Nama" id="name" type="text" class="form-control">
                    </div> --}}
                    <div class="position-relative row form-group" id="">
                        <label for="exampleEmail" class="col-sm-3 col-form-label" >Unit Type</label>
                        <div class="col-sm-9">
                            <select class="js-example-basic-single" id="unit_type" name="ms_unit_type_id" style="width:100%">
                                <option value="" selected>Select Unit Type</option>
                            </select>
                        </div>
                    </div>

                    <div class="position-relative row form-group" id="productDisabled">
                        <label for="exampleEmail" class="col-sm-3 col-form-label">Product Name</label>
                        <div class="col-sm-9">
                            <select class="js-example-basic-single" id="product" name="ms_product_id" style="width:100%">

                            </select>
                        </div>
                    </div>

                    <div class="position-relative row form-group" id="cityDisabled">
                        <label for="exampleEmail" class="col-sm-3 col-form-label" >City Name</label>
                        <div class="col-sm-9">
                            <select class="js-example-basic-single" id="city" name="ms_city_id" style="width:100%">

                            </select>
                        </div>
                    </div>

                    <div class="position-relative row form-group" id="districHide">
                        <label for="exampleEmail" class="col-sm-3 col-form-label" >Distric Name</label>
                        <div class="col-sm-9">
                            <select class="js-example-basic-single" id="distric" name="ms_district_id" style="width:100%">

                            </select>
                        </div>
                    </div>

                    <div class="position-relative row form-group" id="vilageHide">
                        <label for="exampleEmail" class="col-sm-3 col-form-label">Village Name</label>
                        <div class="col-sm-9">
                            <select class="js-example-basic-single" id="vilage" name="ms_village_id" style="width:100%">

                            </select>
                        </div>
                    </div>

                    <div class="position-relative row form-group">
                        <label for="exampleEmail" class="col-sm-3 col-form-label">Value / Price</label>
                        <div class="col-sm-9">
                            <input name="value" id="value" placeholder="Price" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>

