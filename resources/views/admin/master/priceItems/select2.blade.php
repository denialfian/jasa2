<script>

    $(document).ready(function() {
        $("#unit_type").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/unit-type/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.unit_name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });

        $("#unit_type").change(function() {
            document.getElementById("product").disabled = false;
        })

        $("#product").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/product/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });

        $("#city").select2({
            allowClear: true,
            placeholder :'select..',
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/cities/select2'),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });

        $("#city").change(function() {
            $("#distric").val('');
            districtSelect($(this).val());
        })
        districtSelect($(this).val());

        $("#distric").change(function() {
            $("#vilage").val('');
            villageSelect($(this).val());
        })
        villageSelect($(this).val());

    });
    function districtSelect(id) {
        $("#distric").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/address_search/find_district?city_id=' + id),]
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });

    }

    function villageSelect(id) {
        $("#vilage").select2({
            ajax: {
                type: "GET",
                url:Helper.apiUrl('/address_search/find_village?district_id=' + id),
                data: function(params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function(data) {
                    var res = $.map(data.data, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    });

                    return {
                        results: res
                    };
                }
            }
        });
    }
</script>
