@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                {{-- <a href="{{ url('admin/email-other/create') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Template</a> --}}
            </div>

        </div>

        <div class="card-body">
        <table style="width: 100%;" id="table-email-other" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th width=70%>Module Name</th>
                        <th width=30%>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@section('script')
<style>
    .table-email-other-delete {
        display:none;
    }
</style>
<script>
        globalCRUD.datatables({
            url: '/email-other/datatables',
            selector: '#table-email-other',
            columnsField: ['name_module'],
            actionLink: {
                update: function(row) {
                    return "/admin/email-other/edit/" + row.name_module;
                },
                delete: function(row) {
                    return "/email-other/" + row.name_module;
                }
            }
        })
    </script>
@endsection
