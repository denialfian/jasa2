@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-engineer-code-excel" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Engineer Code</th>
                        <th>Engineer Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
            <button class="btn btn-primary btn-sm add-engineer-code-excel"><i class="fa fa-plus"></i> ADD Engineer Code</button>
        </div>
    </div>
</div>
@include('admin.master.engineer-code-excel.engineer-code-excelModal')
@endsection

@section('script')
<script>
    Helper.onlyNumberInput('.number_only')
    globalCRUD.datatables({
        url: '/engineer-code-excel/datatables',
        selector: '#table-engineer-code-excel',
        columnsField: ['DT_RowIndex', 'engineer_code', 'engineer_name'],
        modalSelector: "#modal-engineer-code-excel",
        modalButtonSelector: ".add-engineer-code-excel",
        modalFormSelector: "#form-engineer-code-excel",
        actionLink: {
            store: function() {
                return "/engineer-code-excel";
            },
            update: function(row) {
                return "/engineer-code-excel/" + row.id;
            },
            delete: function(row) {
                return "/engineer-code-excel/" + row.id;
            },
        }
    })
</script>
@endsection
