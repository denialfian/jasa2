@extends('admin.home')
@section('content')
<div class="app-main__inner">
    <div class="row">
        <div class="col-md-12">
            <div class="main-card mb-3 card">
                <div class="card-body">
                    <ul class="tabs-animated-shadow nav-justified tabs-animated nav">
                        <li class="nav-item">
                            <a role="tab" class="nav-link active" id="tab-c1-0" data-toggle="tab" href="#tab-animated1-0">
                                <span class="nav-text">Countries</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-1" data-toggle="tab" href="#tab-animated1-1">
                                <span class="nav-text">City</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-2" data-toggle="tab" href="#tab-animated1-2">
                                <span class="nav-text">Distric</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-2" data-toggle="tab" href="#tab-animated1-3">
                                <span class="nav-text">Province</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-2" data-toggle="tab" href="#tab-animated1-4">
                                <span class="nav-text">Villages</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a role="tab" class="nav-link" id="tab-c1-2" data-toggle="tab" href="#tab-animated1-5">
                                <span class="nav-text">Zipcode</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-animated1-0" role="tabpanel">
                            @include('admin.master.countries.index')
                        </div>
                        <div class="tab-pane" id="tab-animated1-1" role="tabpanel">
                            <p class="mb-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                unknown
                                printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
                        </div>
                        <div class="tab-pane" id="tab-animated1-2" role="tabpanel">
                            <p class="mb-0">It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus
                                PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div class="tab-pane" id="tab-animated1-3" role="tabpanel">
                            <p class="mb-0">5145156asdasd as dasd asd</p>
                        </div>
                        <div class="tab-pane" id="tab-animated1-4" role="tabpanel">
                            <p class="mb-0">123123123123123</p>
                        </div>
                        <div class="tab-pane" id="tab-animated1-5" role="tabpanel">
                            <p class="mb-0">asd adsd asd asd</p>
                        </div>
                    </div>
                </div>
                <div class="d-block text-center card-footer"></div>
            </div>
        </div>
    </div>
</div>
@include('admin.template._mainScript')
@endsection