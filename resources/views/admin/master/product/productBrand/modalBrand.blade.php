<form id="form-brand">
    <div class="modal fade" id="modal-brand" data-backdrop="false" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Part Category</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">

                    <label class="label-control">Name</label>
                    <div class="form-group">
                        <input name="name" placeholder="name" id="name" type="text" class="form-control" required>
                        <span id="error-name" style="color:red"></span>
                    </div>


                    <label class="label-control">Code</label>
                    <div class="form-group">
                        <input name="code" placeholder="code" id="code" type="text" class="form-control" required>
                        <span id="error-code" style="color:red;"></span>
                    </div>


                    <label class="label-control">Part Category</label>
                    <div class="form-group">
                        <select class="form-control part-category-select" id="product_part_category_id" name="product_part_category_id" style="width:100%" required>
                            <option value="" selected>Pilih Part Category</option>
                        </select>
                        <span id="error-product_part_category_id" style="color:red;"></span>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm waves-effect btn-clus" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    <button type="submit" class="btn btn-sm btn-oke waves-effect"><i class="fa fa-plus"></i> Save</button>
                </div>
            </div>
        </div>
    </div>
</form>