@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                
                <!-- <a href="{{ url('/create-product-brand') }}" class="mb-2 mr-2 btn btn-primary btn-sm" style="float:right">Add Brands</a> -->
            </div>
        </div>
        <div class="card-body">
            <table id="table-brand" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Product Code</th>
                        <th>Part Category</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-brand"><i class="fa fa-plus"></i> Add Brand</button>
        </div>
    </div>
</div>

@include('admin.master.product.productBrand.modalBrand')
@endsection

@section('script')
<script>
    globalCRUD
        .select2Static(".part-category-select", '/product_part_category/select2', function(item) {
            return {
                id: item.id,
                text: item.part_category
            }
        });

    globalCRUD.datatables({
        orderBy: [1, 'asc'],
        url: '/product_brand/datatables',
        selector: '#table-brand',
        columnsField: [
            'DT_RowIndex', 
            'name', 
            'code', 
            {
                data: "part_category.part_category",
                name: "part_category.part_category",
                searchable: false, 
                orderable: false, 
            }
        ],
        modalSelector: "#modal-brand",
        modalButtonSelector: ".add-brand",
        modalFormSelector: "#form-brand",
        actionLink: {
            store: function() {
                return "/product_brand";
            },
            update: function(row) {
                return "/product_brand/" + row.id;
            },
            delete: function(row) {
                return "/product_brand/" + row.id;
            },
        }
    })
</script>
@endsection
