<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script>
    ProductBrandObj = {
        // table
        table: null,
        modal: $('#modal-brand').modal({
            show: false
        }),
        datatables: function() {
            selector = '#table-brand';
            url = '/product_brand/datatables';

            table = DataTablesHelper.make(selector, url, this.columns());

            this.table = table;

            return table;
        },
        // datatables columns
        columns: function() {
            return [{
                    name: 'id',
                    data: 'id'
                },
                {
                    name: 'name',
                    data: 'name'
                },
                {
                    name: 'code',
                    data: 'code'
                },
                {
                    name: 'part_category.part_category',
                    data: 'part_category.part_category'
                },
                {
                    name: 'id',
                    data: 'id',
                    render: function(data, type, full) {
                        edit_btn = "<button data-id='" + full.id + "' class='edit-brand btn btn-warning btn-white btn-sm'><i class='fa fa-pencil'></i> Edit</button>";

                        delete_btn = "<button data-id='" + full.id + "'  class='delete-brand btn btn-danger btn-sm'><i class='fa fa-trash'></i> Delete</button>";

                        return edit_btn + " " + delete_btn;
                    }
                },
            ];
        },
        // refresh table
        reloadTable: function() {
            if (ProductBrandObj.table != null) {
                ProductBrandObj.table.ajax.reload();
            }

            return this;
        },


        // modal
        modalShow: function(title = '', row = null) {
            if (title == '') {
                title = $(this).text();
            }

            // jika akan edit
            if (row !== null) {
                // isi data
                this.isiDataFormModal(row);
            } else {
                // kosongkan form input
                this.kosongkanDataFormModal();
            }

            // ganti text
            this.modal.find('.modal-title').text(title);
            // show modal
            this.modal.modal('show');
        },
        modalHide: function() {
            // show modal
            this.modal.modal('hide');

            return this;
        },
        kosongkanDataFormModal: function() {
            $("#name").val('');
            $("#code").val('');
            $("#product_part_category_id").val('').change();
            $("#id").val('');
        },
        isiDataFormModal: function(row) {
            $("#name").val(row.name);
            $("#code").val(row.code);
            $("#product_part_category_id").val(row.product_part_category_id).change();
            $("#id").val(row.id);
        },


        // request
        store: function(data) {
            Axios.post('/product_brand/', data)
                .then(function(response) {
                    // send notif
                    Helper.successNotif(response.data.msg);

                    // reload table
                    ProductBrandObj.modalHide().reloadTable();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        },
        update: function(data) {
            Axios.put('/product_brand/' + data.id, data)
                .then(function(response) {
                    // send notif
                    Helper.successNotif(response.data.msg);

                    // reload table
                    ProductBrandObj.modalHide().reloadTable();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        },
        delete: function(id) {
            Helper.confirm(function() {
                Axios.delete('/product_brand/' + id)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('data berhasil didelete');
                        // reload 
                        ProductBrandObj.reloadTable();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
            })
        },
        selectProductPartCategory: function() {
            Axios.get('/product_part_category/select2')
                .then(function(response) {
                    data = response.data.data;
                    $(".part-category-select").select2({
                        data: $.map(data, function(item) {
                            return {
                                text: item.part_category,
                                id: item.id
                            }
                        })
                    });
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        }
    }
</script>

<script>
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })
</script>

<script>
    // load data part category
    ProductBrandObj.selectProductPartCategory();

    // load datatables    
    var table = ProductBrandObj.datatables();

    // add data
    $('.add-brand')
        .on('click', function() {
            ProductBrandObj.modalShow('add brand');
        });

    // edit data
    $(document)
        .on('click', ".edit-brand", function() {
            var row = table.row($(this).parents('tr')).data();

            ProductBrandObj.modalShow("Edit brand", row);
        });

    // delete data
    $(document)
        .on('click', ".delete-brand", function() {
            var row = table.row($(this).parents('tr')).data();

            ProductBrandObj.delete(row.id);
        });

    // submit data
    $('#form-brand')
        .submit(function(e) {
            var data = Helper.serializeForm($(this));

            if (data.id == '') {
                ProductBrandObj.store(data);
            } else {
                ProductBrandObj.update(data);
            }

            e.preventDefault();
        })
</script>