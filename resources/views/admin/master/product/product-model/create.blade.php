@extends('admin.home')
@section('content')
<form action="" method="post" id="form-product-model-create" style="display: contents;">
    <div class="col-md-12">
        <div class="card">
            <div class="header-bg card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    {{ $title }}
                </div>
                <div class="btn-actions-pane-right text-capitalize">
                    <a href="{{ route('admin.product-model.index.web') }}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
                </div>
            </div>
            <div class="card-body">
                <div class="position-relative form-group">
                    <label class="label-control">Product Model Name</label>
                    <input name="name" id="name" placeholder="Name" type="text" class="form-control" required />
                </div>

                <div class="position-relative form-group">
                    <label class="label-control">Product Model Code</label>
                    <input name="model_code" id="model_code" type="text" class="form-control" required />
                </div>

                <div class="position-relative form-group">
                    <label class="label-control">Product Category</label>
                    <select class="form-control" id="product_category" name="ms_product_category_id" style="width:100%" required></select>
                </div>

                <div class="position-relative form-group">
                    <label class="label-control">Product Brand</label>
                    <select class="form-control" id="product_brand" name="product_brands_id" style="width:100%" required></select>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-sm btn-success" type="submit">
                    <i class="fa fa-plus aria-hidden=" true"></i> Save
                </button>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<!-- @include('admin.script.master._productModelScript') -->
<script>
    globalCRUD
        .select2("#product_category", '/product_category/select2')
        .select2("#product_brand", '/product_brand/select2')

    $('#form-product-model-create').submit(function(e) {

        globalCRUD.handleSubmit($(this))
            .storeTo('/product_model')
            .backTo(function(resp) {
                return "{{ route('admin.product-model.index.web') }}";
            })

        e.preventDefault();
    });
</script>
@endsection