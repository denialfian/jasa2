@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
            </div>
        </div>
        <div class="card-body">
            <table id="table-product-model" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Model Code</th>
                        <th>Product Brand</th>
                        <th>Product Category</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <a href="{{ route('admin.product-model.create.web') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add Product Model</a>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        orderBy: [1, 'asc'],
        url: '/product_model/datatables',
        selector: '#table-product-model',
        columnsField: [
            'DT_RowIndex',
            'name',
            'model_code',
            {
                data: "product_brand.name",
                name: "product_brand.name",
                searchable: false, 
                orderable: false, 
            },
            {
                data: "product_category.name",
                name: "product_category.name",
                searchable: false, 
                orderable: false, 
            }
        ],
        actionLink: {
            update: function(row) {
                return "/admin/product-model/" + row.id + '/edit';
            },
            delete: function(row) {
                return "/product_model/" + row.id;
            }
        }
    })
</script>
@endsection
