@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ route('admin.product-model.index.web') }}" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</a>
            </div>
        </div>
        <div class="card-body">
            <form action="" method="post" id="form-product-model-update">
                <input type="hidden" name="model_code" value="{{ $editProductModel->model_code }}" id="examplename" class="form-control">
                <input type="hidden" name="id" value="{{ $editProductModel->id }}">
                <div class="position-relative form-group">
                    <label class="label-control">Product Model Name</label>
                    <input name="name" value="{{ $editProductModel->name }}" id="examplename" type="text" class="form-control" required />

                </div>
                <div class="position-relative form-group">
                    <label class="label-control">Product Model Code</label>
                    <input name="model_code" value="{{ $editProductModel->model_code }}" id="model_code" type="text" class="form-control" required />

                </div>
                <div class="position-relative form-group">
                    <label class="label-control">Product Brands</label>
                    <select class="js-example-basic-single" id="product_brand" name="product_brands_id" style="width:100%" required>
                        <option value="{{ $editProductModel->product_brand->id }}" selected>{{ $editProductModel->product_brand->name }}</option>
                    </select>
                </div>
                <div class="position-relative form-group">
                    <label class="label-control">Product Category Name</label>
                    <select class="js-example-basic-single" name="ms_product_category_id" id="product_category" style="width:100%" required>
                        <option value="{{ $editProductModel->product_category->id }}" selected>{{ $editProductModel->product_category->name }}</option>
                    </select>
                </div>

                <div class="position-relative form-group">
                    <button class="btn btn-sm btn-success" type="submit">
                        <i class="fa fa-sign-in" aria-hidden="true"></i>
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    globalCRUD
        .select2("#product_category", '/product_category/select2')
        .select2("#product_brand", '/product_brand/select2')

    $('#form-product-model-update').submit(function(e) {

        globalCRUD.handleSubmit($(this))
            .updateTo(function(formData) {
                return '/product_model/' + formData.id;
            })
            .backTo(function(resp) {
                return "{{ route('admin.product-model.index.web') }}";
            })

        e.preventDefault();
    });
</script>
@endsection