@extends('admin.home')

@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <span id="nestable-menu">
                    <button type="button" class="mb-2 mr-1 btn btn-success pull-right pad-l" data-action="expand-all"><i class="fa fa-expand"></i></button>
                    <button type="button" class="mb-2 mr-1 btn btn-info pull-right pad-r" data-action="collapse-all"><i class="fa fa-compress"></i></button>
                </span>
            </div>
        </div>
        <div class="card-body">
            <div class="col-md-8">
                <div class="well">
                    <p class="lead">
                        <a href="#newModal" class="mb-2 mr-2 btn btn-success pull-right pad-l" data-toggle="modal" data-backdrop="false"><i class="fa fa-plus-square"></i>
                        </a>
                        Category</p>
                    <div class="dd" id="nestable">
                        {!! $cat !!}
                        <input type="hidden" id="nestable-output">
                    </div>
                    <br>
                    <div id="success-indicator" style="display:none; margin-right: 10px;">
                        <div class="alert alert-success fade show" role="alert">Product category move has been saved !</div>
                    </div>
                </div>
            </div>
            <br>
            <div class="d-block text-center card-footer">
                <div class="col-md-4">
                    <div class="well">
                        <br>
                        <p>Drag category to move them in a different order</p>
                    </div>
                </div>
            </div>
            @include('admin.master.product.product-category.createModalCategory')
            @include('admin.master.product.product-category.updateModelCategory')
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    function convertToSlugAdd(str) {
        elem = 'to_slug_add';
        Helper.toSlug(str, elem);
    }

    function convertToSlugEdit(str) {
        elem = 'to_slug_edit';
        Helper.toSlug(str, elem);
    }

    function checkRequiredAdd() {
        var isValid = true;
        var na = $('input[name="name_add"]').val();
        var pcca = $('input[name="product_cate_code_add"]').val();
        var sa = $('input[name="slug_add"]').val();

        if(na == ''){
            isValid = false;
            Helper.warningNotif('Name Category required!');
            return false;
        } else if(pcca == ''){
            isValid = false;
            Helper.warningNotif('Product Category Code required!');
            return false;
        } else if(sa == ''){
            isValid = false;
            Helper.warningNotif('Slug Required!');
            return false;
        }
        return isValid;
    }

    function checkRequiredEdit() {
        var isValid = true;
        var ne = $('input[name="name_edit"]').val();
        var pcce = $('input[name="product_cate_code_edit"]').val();
        var se = $('input[name="slug_edit"]').val();

        if(ne == ''){
            isValid = false;
            Helper.warningNotif('Name Category required!');
            return false;
        } else if(pcce == ''){
            isValid = false;
            Helper.warningNotif('Product Category Code required!');
            return false;
        } else if(se == ''){
            isValid = false;
            Helper.warningNotif('Slug Required!');
            return false;
        }
        return isValid;
    }

    $(document).ready(function() {
        $('#to_slug_add').prop('readonly', true);
        $('#to_slug_edit').prop('readonly', true);
        var updateOutput = function(e) {
            var list = e.length ? e : $(e.target),
                output = list.data('output');
            if (window.JSON) {
                output.val(window.JSON.stringify(list.nestable('serialize'))); //, null, 2));
            } else {
                output.val('JSON browser support required for this demo.');
            }
        };

        // activate Nestable for list 1
        $('#nestable').nestable({
            group: 1
        }).on('change', updateOutput);

        // output initial serialised data
        updateOutput($('#nestable').data('output', $('#nestable-output')));
        $('#nestable-menu').on('click', function(e) {
            var target = $(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });

        // output initial serialised data
        updateOutput($('#nestable').data('output', $('#nestable-output')));
        $('#nestable-menu').on('click', function(e) {
            var target = $(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                $('.dd').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('.dd').nestable('collapseAll');
            }
        });

        $('.dd').on('change', function() {
            Helper.loadingStart();
            var dataString = {
                data: $("#nestable-output").val(),
            };
            console.log(dataString);
            $.ajax({
                type: "POST",
                url: Helper.apiUrl('/product_category/move'),
                data: dataString,
                cache: false,
                success: function(data) {
                    Helper.successNotif('Success Moving');
                    $("#success-indicator").fadeIn(100).delay(1000).fadeOut();
                },
                error: function(xhr, status, error) {
                    alert(error);
                },
            });
        });

        $("#add").click(function() {
            if(checkRequiredAdd() == true) {
                Helper.loadingStart();
                $.ajax({
                    url: Helper.apiUrl('/product_category'),
                    type: 'POST',
                    data: {
                        name: $('input[name="name_add"]').val(),
                        product_cate_code: $('input[name="product_cate_code_add"]').val(),
                        slug: $('input[name="slug_add"]').val(),
                    },
                    success: function(response) {
                        Helper.successNotif(response.msg);
                        window.location.href = Helper.redirectUrl('/admin/product-category');
                    },
                });
            }
        });

        $("#edit").click(function() {
            if(checkRequiredEdit() == true) {
                Helper.loadingStart();
                id = $('#id_category').val();
                $.ajax({
                    url: Helper.apiUrl('/product_category/' + id),
                    type: 'PUT',
                    data: {
                        name: $('input[name="name_edit"]').val(),
                        product_cate_code: $('input[name="product_cate_code_edit"]').val(),
                        slug: $('input[name="slug_edit"]').val(),
                    },

                    success: function(response) {
                        if (response != 0) {
                            Helper.successNotif(response.msg);
                            window.location.href = Helper.redirectUrl('/admin/product-category');
                        }
                    },
                });
            }
        });

        $(document).on('click', '.update_toggle', function(e) {
            id = $(this).attr('rel');
            editCategory(id)
            e.preventDefault()
        })

        $(document).on('click', '.delete_toggle', function(e) {
            id = $(this).attr('rel');
            deleteCategory(id)
            e.preventDefault()
        })

        function editCategory(id) {
            console.log('id', id)
            $.ajax({
                url: Helper.apiUrl('/product_category/' + id),
                type: 'get',
                data: id,
                dataType: 'JSON',
                contentType: 'application/json',
                success: function(res) {
                    $('#id_category').val(id),
                        $('#name_edit').val(res.data.name),
                        $('#product_cate_code_edit').val(res.data.product_cate_code),
                        $('#to_slug_edit').val(res.data.slug),
                        $('#id_category_edit').val(res.data.id)
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                }
            })
        }

        function deleteCategory(id) {
            Helper.confirm(function() {
                Helper.loadingStart();
                $.ajax({
                    url: Helper.apiUrl('/product_category/' + id),
                    type: 'delete',
                    dataType: 'JSON',
                    contentType: 'application/json',
                    success: function(res) {
                        Helper.successNotif('Success, Product Category has been deleted!');
                        window.location.href = Helper.redirectUrl('/admin/product-category');
                    },
                    error: function(res) {
                        Helper.errorNotif('tidak bisa delete terdapat relasi di beberapa modul');
                        console.log(res);
                    }
                })
            })
        }

    });
</script>
@endsection
