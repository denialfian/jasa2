<!-- Create new item Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
        {{-- <form action="" class="form-horizontal" role="form" method="post"> --}}
        <div class="modal-header">
            <h4 class="modal-title"  id='test'>Edit Product Category</h4>
        </div>
        <input type="hidden" class="form-control" id="id_category">
        <div class="modal-body">
            <div class="form-group">
                <label for="title" class="col-sm-12 col-form-label">Name</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="name_edit" name="name_edit" onload="convertToSlugEdit(this.value)" onkeyup="convertToSlugEdit(this.value)">
                </div>
            </div>
            <div class="form-group">
                <label for="url" class="col-sm-12 col-form-label">Slug</label>
                <div class="col-lg-10">
                <input type="text" class="form-control" name="slug_edit" id="to_slug_edit">
                </div>
            </div>
            <div class="form-group">
                <label for="label" class="col-sm-12 col-form-label">Category code</label>
                <div class="col-lg-10">
                <input type="text" class="form-control" name="product_cate_code_edit" id="product_cate_code_edit">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary" id="edit">Save</button>
        </div>
        {{-- </form> --}}
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->