@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                
            </div>
        </div>
        <div class="card-body">
            <table id="table-additional-product" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Additional Code</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-additional-product"><i class="fa fa-plus"></i> Add New</button>
            <!-- <button type="button" class="btn btn-sm btn-danger delete-selected-btn"><i class="fa fa-remove"></i> Delete</button> -->
        </div>
    </div>
</div>
@include('admin.master.product.additional.modalAdditional')
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        orderBy: [2, 'asc'],
        url: '/product_additional/datatables',
        selector: '#table-additional-product',
        columnsField: ['DT_RowIndex', 'additional_code'],
        modalSelector: "#modal-additional-product",
        modalButtonSelector: ".add-additional-product",
        modalFormSelector: "#form-additional-product",
        buttonDeleteBatch: '.delete-selected-btn',
        actionLink: {
            only_icon: false,
            store: function() {
                return "/product_additional";
            },
            update: function(row) {
                return "/product_additional/" + row.id;
            },
            delete: function(row) {
                return "/product_additional/" + row.id;
            },
            deleteBatch: function(selectedid) {
                return "/product_additional/destroy_batch/";
            },
        },
        // selectable: true,
    })
</script>
@endsection
