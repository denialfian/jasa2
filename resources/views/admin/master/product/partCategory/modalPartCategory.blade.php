<form id="form-part-category">
    <div class="modal fade" id="modal-part-category" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Part Category</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label class="label-control">Part Category</label>
                    <div class="form-group">
                        <input name="part_category" placeholder="part category" id="part_category" type="text" class="form-control" required>
                    </div>
                    <label class="label-control">Part Code</label>
                    <div class="form-group">
                        <input name="part_code" placeholder="part code" id="part_code" type="text" class="form-control" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm waves-effect btn-clus" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                    <button type="submit" class="btn btn-sm btn-oke waves-effect"><i class="fa fa-plus"></i> Save</button>
                </div>
            </div>
        </div>
    </div>
</form>