<style>
    .img-thumbnail{
        max-width: 100px;
        height: 100px;       
    }
    .container {
        position: relative;
        width: 100%;
    }

    .image {
        opacity: 1;
        display: block;
        width: 100px;
        height: 100px;
        transition: .5s ease;
        backface-visibility: hidden;
    }

    .middle {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        text-align: center;
    }

    .container:hover .image {
        opacity: 0.3;
    }

    .container:hover .middle {
        opacity: 1;
    }

    /* .text {
      background-color: #4CAF50;
      color: white;
      font-size: 16px;
      padding: 16px 32px;
    } */
</style>
<script>
    var _attributes;

    var TemplateObj = {
        attributeSelect: function(uniq, uniq_varian) {
            data_attr = ProductObj.getHeaderAttributeData();

            template = '';

            _.each(data_attr, function(item) {
                uniq_attr = item.id;

                template += '<input ';
                    template += ' data-uniq_attr="'+uniq_attr+'"';
                    template += ' data-uniq_varian="'+uniq_varian+'" ';
                    template += ' data-uniq="'+uniq+'" ';
                    template += ' value="0" ';
                    template += ' type="hidden" ';
                    template += ' class="product_varian_attribute_id_input-'+uniq+'-'+uniq_varian+'-'+uniq_attr+'"';
                    template += 'name="product_varian_attribute_id[]">';

                template += '<div class="position-relative form-group">';
                template += '<input type="hidden" name="attribute_id[]" value="' + item.id + '" data-uniq_attr="' + uniq_attr + '" class="attr-select-' + uniq + '-' + uniq_varian + ' attribute-select-' + uniq + '-' + uniq_varian + '-' + uniq_attr + '"> ';
                template += '<label>' + item.name + '</label>';
                template += '<select data-uniq_attr="' + uniq_attr + '" data-uniq_varian="' + uniq_varian + '" data-uniq="' + uniq + '" class="form-control header-term-select term-select-' + uniq + '-' + uniq_varian + '-' + uniq_attr + '" name="term_id[]">';
                _.each(item.terms, function(term) {
                    term_name = term.name + ' ' + (term.color_code == null ? '' : term.color_code) + ' ' + (term.hex_rgb == null ? '' : term.hex_rgb);
                    template += '<option value="' + term.id + '">' + term_name + '</option>';
                });
                template += '</select>';

                template += '</div>';
            });

            return template;
        },
        attributeHeaderBody: function(uniq) {
            template = '';
            template += '<div class="form-row">';

            // Length
            template += '<div class="col-md-3">';
            template += '<div class="position-relative form-group">';
            template += '<label>Length (cm)</label>';
            template += '<input name="header_product_length" placeholder="Length" type="number" class="form-control">';
            template += '</div>';
            template += '</div>';

            // Width
            template += '<div class="col-md-3">';
            template += '<div class="position-relative form-group">';
            template += '<label>Width (cm)</label>';
            template += '<input name="header_product_width" placeholder="Width" type="number" class="form-control">';
            template += '</div>';
            template += '</div>';

            // Height
            template += '<div class="col-md-3">';
            template += '<div class="position-relative form-group">';
            template += '<label>Height (cm)</label>';
            template += '<input name="header_product_height" placeholder="Height" type="number" class="form-control">';
            template += '</div>';
            template += '</div>';

            // Weight
            template += '<div class="col-md-3">';
            template += '<div class="position-relative form-group">';
            template += '<label>Weight (cm)</label>';
            template += '<input name="header_product_weight" placeholder="weight" type="number" class="form-control">';
            template += '</div>';
            template += '</div>';

            template += '</div>';
            return template;
        },
        attributeSetSelect: function(data) {
            uniq = Date.now();
            template = '';

            // Attribute
            template += '<div class="position-relative form-group attribute-form-uniq-' + uniq + '">';

            template += '<label>Attribute</label>';
            template += '<select data-uniq="' + uniq + '" class="form-control select-header-attribute  select-header-attribute-uniq-' + uniq + '" name="attribute_id[]" style="width:100%">';
            template += '<option selected>Select attribute</option>';
            _.each(data, function(item) {
                template += '<option value="' + item.id + '">' + item.name + '</option>';
            });
            template += '</select>';
            template += '<div class="select-header-term-area-' + uniq + '"></div>';
            template += '</div>';

            return template;
        },
        termOption: function(terms, uniq) {
            template = '';
            no = 1;
            template += '<select data-uniq="' + uniq + '" class="form-control select-term-header select-header-term-' + uniq + '" name="term_id[]" multiple="multiple" style="width:100%">';
            _.each(terms, function(item) {
                selected = (no == 1) ? ' selected ' : '';
                no++;
                template += '<option value="' + item.id + '" ' + selected + '>' + item.name + '</option>';
            });
            template += '</select>';
            return template;
        },
        attributeButtonAdd: function(uniq) {
            template = '';
            template += '<button data-uniq="' + uniq + '" class="btn btn-outline-secondary btn-add-attribute" type="button">';
            template += '<i class="fa fa-plus"></i>&nbsp;&nbsp;Add Attribute';
            template += '</button>';

            return template;
        },


        vendorDeleteBtn: function(uniq) {
            current_total_vendor = $('.vendor-card').length;
            template = '';
            if (current_total_vendor != 0) {
                template += '<button data-uniq="' + uniq + '" class="btn btn-danger btn-sm btn-delete-vendor btn-delete-vendor-uniq-' + uniq + '" type="button">';
                template += '<i class="fa fa-remove"></i> Delete';
                template += '</button>';
            }
            return template;
        },
        vendorHideBtn: function(uniq) {
            template = '';
            template += '<button data-uniq="' + uniq + '" data-hide="1" class="btn btn-sm btn-white btn-warning btn-hide-vendor btn-hide-vendor-uniq-' + uniq + '" type="button">';
            template += 'Hide';
            template += '</button>';
            return template;
        },
        vendorHeader: function(uniq) {
            template = '';
            template += '<div class="card-header">';

            template += '<span class="product-vendor-title-' + uniq + '">PRODUCT VENDOR ' + uniq + '</span>';
            template += '<div class="btn-actions-pane-right">';
            template += '<div role="group" class="btn-group-sm btn-group">';
            template += this.vendorDeleteBtn(uniq);
            template += this.vendorHideBtn(uniq);
            template += '</div>';
            template += '</div>';

            template += '</div>';
            return template;
        },
        vendorBody: function(uniq) {
            brand_code = '';
            part_code = '';
            brand_el = $('.select-brand');
            brand_val = brand_el.val();
            if (brand_val != '') {
                data_brand = _.find(brand_el.select2('data'), function(row){ return row.id == brand_val });
                if (data_brand) {
                    brand_code = data_brand.data.code;
                    part_code = data_brand.data.part_category.part_code;
                }   
            }

            template = '';

            template += '<input type="hidden" name="product_vendor_id[]" value="0" class="product_vendor_input-' + uniq + '">';
            // vendor
            template += '<div class="position-relative form-group">';
            template += '<label class="label-control">Vendor</label>';
            template += '<select class="select-vendor-input select-vendor-' + uniq + '" name="vendor_id[]" data-uniq="' + uniq + '" style="width:100%" required>';
            template += '<option selected>Select Vendor</option>';
            template += '</select>';
            template += '</div>';

            // vendor code
            template += '<div class="position-relative form-group">';
            template += '<label class="label-control">Vendor Code</label>';
            template += '<input name="vendor_code[]" placeholder="vendor code" type="text" class="form-control vendor_code_input-' + uniq + '" required>';
            template += '</div>';

            // Part code
            template += '<div class="position-relative form-group">';
            template += '<label class="label-control">Part Code</label>';
            template += '<input name="part_code[]" placeholder="part code" type="text" class="form-control part_code_input-' + uniq + '" value="'+part_code+'" required>';
            template += '</div>';

            // Brand code
            template += '<div class="position-relative form-group">';
            template += '<label class="label-control">Brand Code</label>';
            template += '<input name="brand_code[]" placeholder="brand code" type="text" class="form-control brand_code_input-' + uniq + '" value="'+brand_code+'" required>';
            template += '</div>';

            // product Code
            template += '<div class="position-relative form-group">';
            template += '<label class="label-control">Product Code</label>';
            template += '<input name="product_code[]" placeholder="product code" type="text" class="form-control product_code_input-' + uniq + '" required>';
            template += '</div>';

            // stock
            template += '<div class="position-relative form-group" style="display: none;">';
            template += '<label class="label-control">Stock</label>';
            template += '<input name="vendor_stock[]" placeholder="stock" type="number" class="form-control vendor_stock_input-' + uniq + '">';
            template += '</div>';

            // Sku
            template += '<div class="position-relative form-group">';
            template += '<label class="label-control">Sku</label>';
            template += '<input name="vendor_sku[]" placeholder="sku" type="text" class="form-control vendor_sku_input-' + uniq + '">';
            template += '</div>';

            // checkbox
            template += '<div class="position-relative form-check">';
                template += '<label class="form-check-label">';
                template += '<input type="checkbox" data-uniq="' + uniq + '" class="add-varian-checkbox form-check-input" /> Add varian';
                template += '</label>';
            template += '</div>';
            template += '<hr />';
            return template;
        },
        vendor: function(uniq) {
            template = '';
            template += '<div class="vendor-card header-border main-card mb-3 card vendor-uniq-' + uniq + '">';
            template += TemplateObj.vendorHeader(uniq);
            template += '<div class="card-body vendor-body-uniq-' + uniq + '">';
            template += TemplateObj.vendorBody(uniq);
            template += '<div class="varian-area-' + uniq + '"></div>';
            template += TemplateObj.varianAddButton(uniq);
            template += '</div>';
            template += '</div>';
            return template;
        },


        varianDeleteBtn: function(uniq, uniq_varian) {
            template = '';
            template += '<button data-uniq_varian="' + uniq_varian + '" data-uniq="' + uniq + '" class="btn btn-danger btn-delete-varian btn-sm btn-delete-varian-uniq-' + uniq + '-' + uniq_varian + '" type="button">';
            template += '<i class="fa fa-remove"></i> Delete';
            template += '</button>';
            return template;
        },
        varianHideBtn: function(uniq, uniq_varian) {
            template = '';
            template += '<button data-uniq_varian="' + uniq_varian + '" data-uniq="' + uniq + '" data-hide="1" class="btn btn-sm btn-white btn-warning btn-hide-varian btn-hide-varian-uniq-' + uniq + '-' + uniq_varian + '" type="button">';
            template += 'Hide';
            template += '</button>';
            return template;
        },
        varianAddButton: function(uniq) {
            template = '';
            template += '<button data-uniq="' + uniq + '" class="btn btn-secondary btn-sm btn-add-varian btn-add-varian-uniq-' + uniq + '" type="button">';
            template += '<i class="fa fa-plus"></i>&nbsp;&nbsp;Add Varian';
            template += '</button>';
            return template;
        },
        varianHeader: function(uniq, uniq_varian) {
            template = '';
            template += '<div class="card-header">';
            template += '<input type="hidden" name="product_varian_id[]" class="varian-id-'+uniq+' varian_id_input-'+uniq+'-'+uniq_varian+'" value="0">';
            template += 'VENDOR VARIAN ' + uniq + '.' + uniq_varian;
            template += '<div class="btn-actions-pane-right">';
            template += '<div role="group" class="btn-group-sm btn-group">';
            template += this.varianDeleteBtn(uniq, uniq_varian);
            template += this.varianHideBtn(uniq, uniq_varian);
            template += '</div>';
            template += '</div>';
            template += '</div>';
            return template;
        },
        varianBody: function(uniq, uniq_varian) {
            length = $('[name="header_product_length"]').val();
            width = $('[name="header_product_width"]').val();
            height = $('[name="header_product_height"]').val();
            weight = $('[name="header_product_weight"]').val();
            template = '';
            // SKU
            template += '<div class="position-relative form-group">';
            template += '<label class="label-control">SKU</label>';
            template += '<input name="varian_sku[]" placeholder="SKU" type="text" data-uniq_varian="' + uniq_varian + '" class="form-control varian-sku-' + uniq + ' varian_sku_input-' + uniq + '-' + uniq_varian + '">';
            template += '</div>';

            // stock
            template += '<div class="position-relative form-group" style="display:none">';
            template += '<label class="label-control">Stock</label>';
            template += '<input name="stock[]" placeholder="stock" type="number" data-uniq_varian="' + uniq_varian + '" class="form-control varian_stock_input-' + uniq + '-' + uniq_varian + '">';
            template += '</div>';

            // length
            template += '<div class="position-relative form-group">';
            template += '<label class="label-control">Length (cm)</label>';
            template += '<input value="'+length+'" name="varian_length[]" placeholder="length" type="number" data-uniq_varian="' + uniq_varian + '" class="form-control varian_length_input-' + uniq + '-' + uniq_varian + '">';
            template += '</div>';

            // width
            template += '<div class="position-relative form-group">';
            template += '<label class="label-control">Width (cm)</label>';
            template += '<input value="'+width+'" name="varian_width[]" placeholder="width" type="number" data-uniq_varian="' + uniq_varian + '" class="form-control varian_width_input-' + uniq + '-' + uniq_varian + '">';
            template += '</div>';

            // height
            template += '<div class="position-relative form-group">';
            template += '<label class="label-control">Height (cm)</label>';
            template += '<input value="'+height+'" name="varian_height[]" placeholder="height" type="number" data-uniq_varian="' + uniq_varian + '" class="form-control varian_height_input-' + uniq + '-' + uniq_varian + '">';
            template += '</div>';

            // weight
            template += '<div class="position-relative form-group">';
            template += '<label class="label-control">Weight</label>';
            template += '<input value="'+weight+'" name="varian_weight[]" placeholder="weight" type="number" data-uniq_varian="' + uniq_varian + '" class="form-control varian_weight_input-' + uniq + '-' + uniq_varian + '">';
            template += '</div>';

            // power
            template += '<div class="position-relative form-group">';
            template += '<label class="label-control">Power</label>';
            template += '<input name="varian_power[]" placeholder="power" type="text" data-uniq_varian="' + uniq_varian + '" class="form-control varian_power_input-' + uniq + '-' + uniq_varian + '">';
            template += '</div>';

            return template;
        },
        varian: function(uniq, uniq_varian) {
            template = '';
            template += '<div class="header-border main-card mb-3 card varian-uniq-' + uniq + '-' + uniq_varian + '" >';
            template += TemplateObj.varianHeader(uniq, uniq_varian);
            template += '<div class="card-body varian-body-uniq-' + uniq + '-' + uniq_varian + '">';
            template += TemplateObj.varianBody(uniq, uniq_varian);
            template += '<div data-uniq="'+uniq+'" data-uniq_varian="'+uniq_varian+'" class="vendor-varian-attribute-area-select vendor-varian-attribute-area-' + uniq + '-' + uniq_varian + '">';
            template += TemplateObj.attributeSelect(uniq, uniq_varian);
            template += '</div>';
            template += '</div>';
            template += '</div>';
            return template;
        }
    };

    var ProductObj = {
        // input
        //
        // text rich
        wysiwygEditor: function(selector) {
            ClassicEditor
                .create(document.querySelector(selector))
                .then(editor => {
                    console.log(editor);
                })
                .catch(error => {
                    console.error(error);
                });
        },
        getVendorValue: function() {
            vendors = $('select[name="vendor_id[]"]').map(function() {
                uniq = $(this).attr('data-uniq');
                return {
                    product_vendor_id: $('.product_vendor_input-' + uniq).val(),
                    vendor_id: $(this).val(),
                    vendor_code: $('.vendor_code_input-' + uniq).val(),
                    part_code: $('.part_code_input-' + uniq).val(),
                    brand_code: $('.brand_code_input-' + uniq).val(),
                    product_code: $('.product_code_input-' + uniq).val(),
                    vendor_stock: $('.vendor_stock_input-' + uniq).val(),
                    vendor_sku: $('.vendor_sku_input-' + uniq).val(),
                    varians: $('.varian-sku-' + uniq).map(function() {
                        uniq_varian = $(this).attr('data-uniq_varian');
                        return {
                            product_varian_id: $('.varian_id_input-' + uniq + '-' + uniq_varian).val(),
                            varian_stock: $('.varian_stock_input-' + uniq + '-' + uniq_varian).val(),
                            varian_sku: $('.varian_sku_input-' + uniq + '-' + uniq_varian).val(),
                            varian_length: $('.varian_length_input-' + uniq + '-' + uniq_varian).val(),
                            varian_width: $('.varian_width_input-' + uniq + '-' + uniq_varian).val(),
                            varian_height: $('.varian_height_input-' + uniq + '-' + uniq_varian).val(),
                            varian_weight: $('.varian_weight_input-' + uniq + '-' + uniq_varian).val(),
                            varian_power: $('.varian_power_input-' + uniq + '-' + uniq_varian).val(),
                            attributes: $('.attr-select-' + uniq + '-' + uniq_varian).map(function(index) {
                                uniq_att = $(this).attr('data-uniq_attr');
                                return {
                                    attribute_id: $('.attribute-select-' + uniq + '-' + uniq_varian + '-' + uniq_att).val(),
                                    term_id: $('.term-select-' + uniq + '-' + uniq_varian + '-' + uniq_att).val(),
                                    product_varian_attribute_id: $('.product_varian_attribute_id_input-' + uniq + '-' + uniq_varian + '-' + uniq_att).val()
                                }
                            }).get()
                        }
                    }).get()
                };
            }).get();

            return vendors;
        },

        // header attribute
        checkDobelAttribute: function(cari) {
            // get all attributes
            no = 0;
            $(".select-header-attribute")
                .each(function() {
                    if (cari == $(this).val()) {
                        no++;
                    }
                });

            return no;
        },
        getHeaderAttributeData: function() {
            attributes = [];

            $('.select-term-header')
                .each(function() {
                    term = $(this).val();
                    uniq = $(this).attr('data-uniq');
                    att_id = $('.select-header-attribute-uniq-' + uniq).val();
                    attribute = _.find(_attributes, item => (item.id == att_id))
                    attribute.terms = $('.select-header-term-' + uniq).map(function() {
                        return $(this).val().map(function(val) {
                            return _.find(attribute.childs, t => (t.id == val));
                        });
                    })
                    attributes.push(attribute);
                })
            return attributes;
        },
        getProductAttHeaderValue: function() {
            return $('.select-header-attribute')
                .map(function() {
                    uniqx = $(this).attr('data-uniq');
                    return {
                        attribute_id: $(this).val(),
                        terms: $('.select-header-term-' + uniqx).val()
                    };
                }).get();
        },

        // product vebdor
        //
        // delete product vendor
        productVendorDelete: function(uniq, product_vendor_id = null) {
            current_total_vendor = $('.vendor-card').length;
            if (current_total_vendor > 1) {
                Helper.confirm(function() {
                    if (product_vendor_id) {
                        ProductObj
                            .destroyVendor(product_vendor_id, function(resp) {
                                $('.vendor-uniq-' + uniq).remove();
                            })
                    } else {
                        $('.vendor-uniq-' + uniq).remove();
                    }
                });
            }else{
                Helper.errorNotif('Cannot Delete Vendor, have at least one vendor');
            }
        },
        // hide product vendor
        productVendorHide: function(uniq, status = 'hide') {
            if (status == 'hide') {
                $('.vendor-body-uniq-' + uniq).hide();
            }

            if (status == 'show') {
                $('.vendor-body-uniq-' + uniq).show();
            }
        },
        // ganti title
        productVendorChangeTitle: function(uniq, title) {
            $(".product-vendor-title-" + uniq).text('VENDOR ' + title);
        },

        // select 2
        createSelect2: function(selector, url, callback = null) {
            $(selector).select2({
                ajax: {
                    type: "GET",
                    url: Helper.apiUrl(url),
                    data: function(params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function(data) {
                        if (callback == null) {
                            var res = $.map(data.data, function(item) {
                                return {
                                    text: item.name,
                                    id: item.id,
                                    data: item
                                }
                            });
                        } else {
                            var res = $.map(data.data, callback);
                        }

                        return {
                            results: res
                        };
                    }
                }
            });
        },
        select2Model: function() {
            this.createSelect2('.select-model', '/product_model/select2');
        },
        select2ProductStatus: function() {
            this.createSelect2('.select-product-status', '/product_status/select2');
        },
        select2Brand: function() {
            this.createSelect2('.select-brand', '/product_brand/select2');
        },
        select2Category: function() {
            this.createSelect2('.select-category', '/product_category/select2');
        },
        select2AdditionalProduct: function() {
            this.createSelect2('.select-additonal-product', '/product_additional/select2', function(item) {
                return {
                    text: item.additional_code,
                    id: item.id
                }
            });
        },
        select2Vendor: function(uniq) {
            this.createSelect2('.select-vendor-' + uniq, '/vendor/select2');
        },


        // attribute
        loadDataAttribute: function(append_template = 'true') {
            $.ajax({
                type: 'get',
                url: Helper.apiUrl('/product_attribute/select2?no_limit=true'),
                success: function(resp) {
                    _attributes = resp.data;
                    if (append_template == 'true') {
                        $('#attribute-product-area').append(TemplateObj.attributeSetSelect(_attributes));
                    } else {
                        append_template(resp.data)
                    }
                },
            });
        },
        appendAttribute: function(uniq, uniq_varian) {
            $('.varian-area-' + uniq).append(TemplateObj.attributeSelect(uniq, uniq_varian))
        },
        appendTerm: function(selector, terms, uniq) {
            $(selector).html('').append(TemplateObj.termOption(terms, uniq))

            selector = '.select-header-term-' + uniq;
            $(selector).select2()
        },

        // varian
        appendVarian: function(uniq, uniq_varian) {
            template = TemplateObj.varian(uniq, uniq_varian);

            $('.varian-area-' + uniq).append(template)
        },
        clearVarian: function(uniq) {
            $('.varian-area-' + uniq).html('');
        },
        deleteVarian: function(uniq, uniq_varian, varian_id = null) {
            Helper.confirm(function() {
                if (varian_id) {
                    ProductObj
                        .destroyVarian(varian_id, function(resp) {
                            $('.varian-uniq-' + uniq + '-' + uniq_varian).remove();
                        })
                } else {
                    $('.varian-uniq-' + uniq + '-' + uniq_varian).remove();
                }
            });
        },
        hideVarian: function(uniq, uniq_varian, status = 'hide') {
            if (status == 'hide') {
                $('.varian-body-uniq-' + uniq + '-' + uniq_varian).hide();
            }

            if (status == 'show') {
                $('.varian-body-uniq-' + uniq + '-' + uniq_varian).show();
            }
        },


        // ajax
        store: function(data) {
            Helper.loadingStart();
            $.ajax({
                type: 'post',
                data: data,
                url: Helper.apiUrl('/product'),
                success: function(resp) {
                    Helper.loadingStop();
                    Helper.successNotif(resp.msg);
                    window.location.href = Helper.redirectUrl('/admin/product');
                },
                error: function(xhr, status, error) {
                    Helper.loadingStop();
                    Helper.errorMsgRequest(xhr, status, error);
                }
            });
        },
        update: function(data) {
            Helper.loadingStart();
            $.ajax({
                type: 'post',
                data: data,
                url: Helper.apiUrl('/product/' + data.id),
                success: function(resp) {
                    Helper.loadingStop();
                    Helper.successNotif(resp.msg);
                    window.location.href = Helper.redirectUrl('/admin/product');
                },
                error: function(xhr, status, error) {
                    Helper.loadingStop();
                    Helper.errorMsgRequest(xhr, status, error);
                }
            });
        },
        destroyVendor: function(id, callback) {
            Helper.loadingStart();
            $.ajax({
                type: 'delete',
                url: Helper.apiUrl('/product_vendor/' + id),
                success: function(resp) {
                    Helper.loadingStop();
                    callback(resp);

                    Helper.successNotif(resp.msg)
                },
                error: function(xhr, status, error) {
                    Helper.loadingStop();
                    Helper.errorMsgRequest(xhr, status, error);
                }
            });
        },
        destroyVarian: function(id, callback) {
            Helper.loadingStart();
            $.ajax({
                type: 'delete',
                url: Helper.apiUrl('/product_varian/' + id),
                success: function(resp) {
                    Helper.loadingStop();
                    callback(resp);

                    Helper.successNotif(resp.msg)
                },
                error: function(xhr, status, error) {
                    Helper.loadingStop();
                    Helper.errorMsgRequest(xhr, status, error);
                }
            });
        },



        init: function() {
            this.loadDataAttribute();
            this.select2Brand();
            this.select2Model();
            this.select2Category();
            this.select2ProductStatus();
            this.select2AdditionalProduct();
            this.wysiwygEditor('#product_description');
        },
        initEdit: function() {
            this.select2Model();
            this.select2Brand();
            this.select2Category();
            this.select2ProductStatus();
            this.select2AdditionalProduct();
            this.wysiwygEditor('#product_description');

            $('[name="vendor_id[]"]').each(function(){
                uniq = $(this).attr('data-uniq');
                ProductObj.createSelect2('.select-vendor-' + uniq, '/vendor/select2');
            })


            this.loadDataAttribute(function(attributes) {
                $('.select-header-attribute')
                    .each(function() {
                        uniq = $(this).attr('data-uniq');
                        attr_id = $(this).val();
                        attribute = _.find(attributes, item => (item.id == attr_id))
                        $('.select-header-term-' + uniq).select2({
                            data: $.map(attribute.childs, function(item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        });
                    })

                $('.select-header-attribute').select2({
                    data: $.map(attributes, function(item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                });
            });

        }
    };

    function convertToSlug(str) {
        elem = 'to_slug';
        Helper.toSlug(str, elem);
    }

    function getImageSelected() {
        return $('.img-thumbnail').map(function() {
            return $(this).attr('data-image-id')
        }).get()
    }
</script>

<script>
    var _uniq_counter = 0;
    var _uniq_counter_varian = 0;
    var first_change_att = true;

    // checkbox vendor
    $(document)
        .on('change', '.add-varian-checkbox', function() {
            uniq = $(this).attr('data-uniq');
            uniq_varian = _uniq_counter_varian;
            if (this.checked) {
                $('.btn-add-varian-uniq-' + uniq).show();
                ProductObj.appendVarian(uniq, uniq_varian);
                _uniq_counter_varian++;
            } else {
                $('.btn-add-varian-uniq-' + uniq).hide();
                ProductObj.clearVarian(uniq);
            }
        })
    
    $(document)
        .on('change', '.select-vendor-input', function() {
            uniq = $(this).attr('data-uniq');
            vendor_el = $('.select-vendor-' + uniq);
            data_vendor = _.find(vendor_el.select2('data'), function(row){ return row.id == vendor_el.val() });
            if (data_vendor) {
                $('.vendor_code_input-' + uniq).val(data_vendor.data.code);   
            }
        })

    // add button product vendor
    $("#btn-add-product-vendor")
        .click(function(e) {
            uniq = _uniq_counter;
            attributes = ProductObj.getProductAttHeaderValue();
            can_add = true;
            _.each(attributes, function(row) {
                if (typeof row.terms == 'undefined') {
                    can_add = false;
                    return;
                }
            })

            if (can_add) {
                $('#product-vendor-area')
                    .append(TemplateObj.vendor(uniq))

                ProductObj.select2Vendor(uniq);

                $('.btn-add-varian-uniq-' + uniq).hide();

                _uniq_counter++;
                _uniq_counter_varian = 0;
            }else{
                Helper.errorNotif('please select attribute first')
            }
        });

    // hide vendor
    $(document)
        .on('click', '.btn-hide-vendor', function() {
            uniq = $(this).attr('data-uniq');
            statusHide = $(this).attr('data-hide');

            if (statusHide == 0) {
                status = 'show';
                $(this).attr('data-hide', 1).text('hide');
            } else {
                status = 'hide';
                $(this).attr('data-hide', 0).text('show');
            }
            ProductObj.productVendorHide(uniq, status);
        })

    // change vendor
    $(document)
        .on('change', 'select[name="vendor_id[]"]', function() {
            uniq = $(this).attr('data-uniq');
            ProductObj.productVendorChangeTitle(uniq, $(this).find(":selected").text());
        })

    // add varian btn
    $(document)
        .on('click', ".btn-add-varian", function(e) {
            uniq = $(this).attr('data-uniq');
            uniq_varian = _uniq_counter_varian;
            ProductObj.appendVarian(uniq, uniq_varian);
            _uniq_counter_varian++;
        });

    // hide varian
    $(document)
        .on('click', '.btn-hide-varian', function() {
            uniq = $(this).attr('data-uniq');
            uniq_varian = $(this).attr('data-uniq_varian');

            statusHide = $(this).attr('data-hide');
            if (statusHide == 0) {
                status = 'show';
                $(this).attr('data-hide', 1).text('hide');
            } else {
                status = 'hide';
                $(this).attr('data-hide', 0).text('show');
            }
            ProductObj.hideVarian(uniq, uniq_varian, status);
        })

    // ganti attribute header
    $(document)
        .on('change', ".select-header-attribute", function(e) {
            uniq = $(this).attr('data-uniq');
            areaAppendTerm = '.select-header-term-area-' + uniq;

            att_id = $(this).val();
            checkDobel = ProductObj.checkDobelAttribute(att_id);

            if (checkDobel == 1) {
                attribute = _.find(_attributes, item => (item.id == att_id))
                ProductObj.appendTerm(areaAppendTerm, attribute.childs, uniq);

                $('.vendor-varian-attribute-area-select').each(function(){
                    $('.vendor-varian-attribute-area-' + $(this).attr('data-uniq') + '-' + $(this).attr('data-uniq_varian')).html('').append(TemplateObj.attributeSelect($(this).attr('data-uniq'), $(this).attr('data-uniq_varian')))
                })

                if (first_change_att) {
                    first_change_att = false;
                    $('#btn-add-product-vendor').trigger('click');
                }
            } else {
                $('.attribute-form-uniq-' + uniq).remove();
                Helper.errorNotif('attribute sudah dipilih');
            }
        });
    
    $(document)
        .on('change', ".select-term-header", function(e) {
            data_options = [];
            jum_vendo = $('.vendor-varian-attribute-area-select').length;

            if (jum_vendo > 0) {
                data_attr = ProductObj.getHeaderAttributeData();

                $('.vendor-varian-attribute-area-select').each(function(){
                    uniq1 = $(this).attr('data-uniq');
                    uniq_varian1 = $(this).attr('data-uniq_varian');
                    _.each(data_attr, function(attr){
                        className = '.term-select-' + uniq1 + '-' + uniq_varian1 + '-' + attr.id;
                        valueSelected = $(className).val();
                        data_options.push({
                            'class': className,
                            'val': valueSelected
                        })
                    })
                })

                $('.vendor-varian-attribute-area-select').each(function(){
                    uniq1 = $(this).attr('data-uniq');
                    uniq_varian1 = $(this).attr('data-uniq_varian');
                    $('.vendor-varian-attribute-area-' + uniq1 + '-' + uniq_varian1)
                        .html('')
                        .append(TemplateObj.attributeSelect(uniq1,uniq_varian1))
                })

                _.each(data_options, function(o){
                    $(o.class).val(o.val).change();
                })
            }           
        });
    // add btn tambah attribte
    $('#btn-add-attribute-product')
        .click(function() {
            total_attribute = _attributes.length;
            current_attribute = $('.select-header-attribute').length;
            if (total_attribute > current_attribute) {
                $('#attribute-product-area')
                    .append(TemplateObj.attributeSetSelect(_attributes));
            } else {
                Helper.errorNotif('melebihi total attribute');
            }
        })
</script>

{{-- select2 and galery images --}}
<script>
    localStorage.clear();

    var x = 0;
    $(".app-theme-white").scroll(function() {
        $("span").text(x += 1);
    });

    $(window).scroll(function() {
        var scrol = $('.app-header__content').height();

        if ($(this).scrollTop() >= scrol) {
            $(".prodInfo").css("margin-top", "-10%");
        } else if ($(this).scrollTop() <= scrol) {
            $(".prodInfo").css("margin-top", "0%");
        }
    });

    // $('input[name="publish_at"]').datepicker();

    $(window).bind('storage', function(e) {
        if (e.originalEvent.key == 'id') {
            images = JSON.parse(e.originalEvent.newValue)
            galeryHtml = ''
            _.each(images, function(img) {
                // append
                galeryHtml += templateGalery(img)

            })
            $('#galeryArea').html('').append(galeryHtml)

        }
        if (e.originalEvent.key == 'idSelect') {
            images = JSON.parse(e.originalEvent.newValue)
            $('#galery_image_id').val(images.id);
            $('#mainImageArea').html('').append(templateMainImage(images))
        }
    });

    $(document).on('click', '.removeImage', function() {
        id = $(this).attr('data-id');
        $('#cardImage-' + id).remove()
    })

    function templateGalery(img) {
        var galery = '';

        // galery += ' <div class="container" >';
        //     galery += ' <div class="row">';
        //         galery += ' <div class="row">';
        galery += ' <div class="col-lg-4 col-md-4 col-xs-4 thumb" id="cardImage-' + img.id + '">';
        galery += ' <img data-image-id="' + img.id + '" src="' + img.src + '" alt="Avatar" class="image imageHide img-thumbnail" style="margin-right: 30px;">';
        galery += ' <div class="middle">';
        galery += ' <div class="text">';
        galery += ' <submit class="btn btn-danger removeImage" data-id="' + img.id + '">';
        galery += ' <i class="fa fa-close"></i>';
        galery += ' </submit>';
        galery += ' </div>';
        galery += ' </div>';
        galery += ' </div>';
        //         galery += ' </div>';
        //     galery += ' </div>';
        // galery += ' </div>';





        // galery += '<div class="col-lg-6 col-md-4 col-xs-6 img-thumb-' + img.id + '">';

        // galery += '<img ';
        // galery += ' class="img-thumbnail" ';
        // galery += ' src="' + img.src + '" ';
        // galery += ' data-image-id="' + img.id + '" ';
        // galery += ' alt="Another alt text" >';

        // galery += '<div class="middle">';
        // galery += '<div class="text">';
        // galery += '<button class="btn btn-danger removeImage" data-img="' + img.id + '">';
        // galery += '<i class="fa fa-close"></i>';
        // galery += '</button>';
        // galery += '</div>';
        // galery += '</div>';

        // galery += '</div>';

        return galery;

    }

    $('.removeImage').click(function() {
        alert('delete here');
    });

    function templateMainImage(image) {
        return galery = '<img class="img-thumbnail" src="' + image.src + '" alt="Another alt text" style="width:126px;height:126px" >';
    }
</script>
