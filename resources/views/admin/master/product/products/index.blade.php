@extends('admin.home')
@section('content')
<div class="col-md-12">

    <div class="card header-border">
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="label-control">Category</label>
                            <select class="form-control select-category" name="product_category_id[]" multiple style="width: 100%"></select>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger" type="submit">Search</button>
            </form>
        </div>
    </div>
    <br>
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <table id="table-product" class="table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>No</th>
                        <th>Product Name</th>
                        <th>Category</th>
                        <th>Model Name</th>
                        <th>Additional Code</th>
                        <th>Stock</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <a href="{{ url('/admin/product/create') }}" class="btn-sm btn btn-primary" style="float:right"><i class="fa fa-plus"></i> Add Product</a>
        </div>
    </div>
</div>

@endsection

@section('script')
<style>
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }

    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>
<script>
    globalCRUD
        .select2(".select-category", '/product_category/select2')

    function formatx(d) {
        console.log(d)
        template = '';
        template += '<table class="table">';
        template += '<tr>';
        template += '<td>No</td>';
        template += '<td>Vendor</td>';
        template += '<td>Vendor Code</td>';
        template += '<td>Part Code</td>';
        template += '<td>Brand Code</td>';
        template += '<td>Product Code</td>';
        template += '</tr>';
        _.each(d.product_vendors, function(i, key) {
            template += '<tr>';
            template += '<td>' + (key + 1) + '</td>';
            template += '<td>' + i.vendor.name + '</td>';
            template += '<td>' + i.vendor_code + '</td>';
            template += '<td>' + i.part_code + '</td>';
            template += '<td>' + i.brand_code + '</td>';
            template += '<td>' + i.product_code + '</td>';
            template += '</tr>';
            _.each(i.product_varians, function(productVarian) {
                attribute = '';
                _.each(productVarian.product_varian_attributes, function(pva) {
                    attribute += pva.attribute.name + ' : ' + pva.term.name + ' ' + pva.term.color_code;
                })
                total_stock = 0;
                _.each(d.inventories, function(inventory) {
                    if (productVarian.id == inventory.product_varian_id) {
                        total_stock += parseInt(inventory.stock_now);
                    }                    
                })
                template += '<tr>';
                template += '<td>-</td>';
                template += '<td>' + attribute + '</td>';
                template += '</tr>';
                template += '<tr>';
                template += '<td></td>';
                template += '<td>stock </td>';
                template += '<td>' + total_stock + '</td>';
                template += '</tr>';
            })
        })
        template += '</table>';

        return template;
    }

    $(document).ready(function() {
        var table = $('#table-product').DataTable({
            processing: true,
            serverSide: true,
            select: false,
            dom: 'Bflrtip',
            responsive: true,
            order: [2, 'asc'],
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/product/datatables'),
                "type": "get",
                "data": function(d) {
                    return $.extend({}, d, {
                        "extra_search": $('#extra').val()
                    });
                }
            },
            columns: [{
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                    sortable: false,
                    width: "10%"
                },
                {
                    data: "name",
                    name: "name",
                },
                {
                    data: "product_category_name",
                    name: "product_category_name",
                    sortable: false,
                },
                {
                    data: "model.name",
                    name: "product_model_id",
                },
                {
                    data: "additional.additional_code",
                    name: "product_additionals_id",
                },
                {
                    data: "id",
                    name: "id",
                    sortable: false,
                    render: function(data, type, full) {
                        total = 0;
                        _.each(full.inventories, function(inventory) {
                            total += parseInt(inventory.stock_now);
                        })

                        if (total > 0) {
                            url = Helper.redirectUrl('/admin/inventory/show?product_id=' + full.id);
                            return '<a target="__blank" href="' + url + '">' + total + '</a>';
                        }

                        return total;
                    }
                },
                {
                    data: "id",
                    name: "id",
                    sortable: false,
                    render: function(data, type, full) {
                        btn_delete = "<button data-id='" + full.id + "'  class='delete btn btn-sm btn-danger btn-xs'><i class='fa fa-trash'></i></button>";
                        btn_edit = "<a href='" + Helper.redirectUrl('/admin/product/' + full.id + '/edit') + "'  data-id='" + full.id + "' class='update btn btn-sm btn-warning btn-white btn-xs'><i class='fa fa-pencil'></i></a>";
                        return btn_edit + ' ' + btn_delete;
                    }
                },
            ],
        });

        $('#table-product tbody').on('click', 'td.details-control', function() {
            var tr = $(this).closest('tr');
            var row = table.row(tr);

            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(formatx(row.data())).show();
                tr.addClass('shown');
            }
        });

        $(document).on('click', '.delete', function() {
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                $.ajax({
                    type: 'delete',
                    url: Helper.apiUrl('/product/' + id),
                    success: function(resp) {
                        Helper.loadingStop();
                        table.ajax.reload();
                        Helper.successNotif('success');
                    },
                    error: function(xhr, status, error) {
                        Helper.loadingStop();
                        Helper.errorMsgRequest(xhr, status, error);
                    }
                });
            })
        })

        // search data
        $("#form-search").submit(function(e) {
            input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/product/datatables/search' + playload);
            table.ajax.url(url).load();
            e.preventDefault();
        })

    });
</script>
@endsection
