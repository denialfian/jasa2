@extends('admin.home')
@section('content')
<form id="form-product" style="display: contents">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8">
                <div class="col-md-12 test" id="scrollView">
                    <div class="main-card mb-3 card">
                        <div class="header-bg card-header">
                            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                                {{ $title }}
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="position-relative form-group">
                                <label class="label-control">Product Name</label>
                                <input name="name" id="name" placeholder="Product Name" type="text" class="form-control" onload="convertToSlug(this.value)" onkeyup="convertToSlug(this.value)" required>
                            </div>
                            <div class="position-relative form-group">
                                <label class="label-control">Slug</label>
                                <input name="slug" class="form-control" id="to_slug" />
                            </div>

                            <div class="position-relative form-group">
                                <label class="label-control">Product Category</label>
                                <select class="form-control select-category" name="product_category_id[]" multiple style="width: 100%"></select>
                            </div>

                            <div class="position-relative form-group" style="display: none;">
                                <label class="label-control">Product SKU</label>
                                <input name="sku" id="sku" placeholder="Product sku" type="text" class="form-control">
                            </div>

                            <div class="position-relative form-group">
                                <label class="label-control">Product Brand</label>
                                <select class="form-control select-brand" id="product_brand_id" name="product_brand_id" style="width:100%" required>
                                    <option value="" selected>Select Brand</option>
                                </select>
                            </div>

                            <div class="position-relative form-group" id="model_id">
                                <label class="label-control">Product Model</label>
                                <select class="form-control select-model" id="product_model_id" name="product_model_id" style="width:100%" required>
                                    <option selected-disabled>Select Model</option>
                                </select>
                            </div>

                            <div class="position-relative form-group">
                                <label class="label-control">Product Additional</label>
                                <select class="form-control select-additonal-product" id="product_additionals_id" name="product_additionals_id" style="width:100%" >
                                    <option value="">Select Product Additional</option>
                                </select>
                            </div>

                            <div class="position-relative form-group">
                                <label class="label-control">Product Description</label>
                                <textarea name="product_description" id="product_description" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card header-border">
                        <div class="card-body" id="attribute-product-area">
                            <div class="position-relative form-group" style="display: none;">
                                <label class="label-control">Stock</label>
                                <input name="header_product_stock" id="header_product_stock" placeholder="Stock" type="number" class="form-control">
                            </div>
                            <div class="form-row">
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label class="label-control">Length (cm)</label>
                                        <input name="header_product_length" placeholder="Length" type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label class="label-control">Width (cm)</label>
                                        <input name="header_product_width" placeholder="Width" type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label class="label-control">Height (cm)</label>
                                        <input name="header_product_height" placeholder="Height" type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="position-relative form-group">
                                        <label class="label-control">Weight (gram)</label>
                                        <input name="header_product_weight" placeholder="weight" type="number" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-secondary btn-sm" type="button" id="btn-add-attribute-product">
                                <i class="fa fa-plus"></i> Add Attribute
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" id="product-vendor-area" style="margin-top: 20px;"></div>
                <div class="col-md-12" style="margin-top: 20px;">
                    <div class="main-card mb-3 card">
                        <div class="card-body">
                            <button class="remove btn btn-sm btn-secondary" type="button" id="btn-add-product-vendor" style="float:left">
                                <i class="fa fa-plus"></i> Add Product Vendor
                            </button>
                            <!-- <button class="btn btn-primary btn-sm" type="submit">
                                <i class="fa fa-save"></i> Save
                            </button> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="main-card col-md-3 card prodInfo header-border" style="position:fixed">
                    <div class="card-body">
                        <div class="position-relative form-group">
                            <label>Publish At</label>
                            <input type="date" name="publish_at" class="form-control" width="100%" value="{{ date('Y-m-d') }}" />
                        </div><hr>
                        <div class="position-relative form-group">
                            <label>Status</label>
                            <select class="select-product-status" name="product_status_id" style="width:100%" required>
                                <option value="">Select Status</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br><br><br><br><br><br><br><br><br>
                <div class="main-card col-md-3 card prodInfo" style="position:fixed;height:auto;">
                    <div class="card-body">
                        <div class="position-relative form-group">
                            <label>Upload Main Image</label>
                            <input type="hidden" name="galery_image_id" id="galery_image_id" value="">
                            <br>
                            <span class="input-group-btn">
                                <a href="" onclick="window.open('/admin/gallery-image/show-gallery?type=main', 'newwindow', 'width=1000,height=600'); return false;" target="_blank"><i class="fa fa-image"></i> Upload Image</a>
                            </span>
                            <div id="mainImageArea"></div>
                        </div><hr>
                        <div class="position-relative form-group">
                            <label>Upload Galery Images</label>
                            <br>
                            <span class="input-group-btn">
                                <a href="" onclick="window.open('/admin/gallery-image/show-gallery?type=galery', 'newwindow', 'width=1000,height=600'); return false;" target="_blank"><i class="fa fa-upload"></i> Upload Images</a>
                            </span>
                            <div class="scroll-area-sm">
                                <div class="scrollbar-container ps--active-y">
                                    <div class="container">
                                        <div class="row">
                                            <div class="row" id="galeryArea">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-muted">
                        <button class="btn btn-primary" type="submit" style="width:100%">
                            <i class="fa fa-save"></i> Save
                        </button>
                      </div>
                </div>
            </div>
        </div>
    </div>
</form>



<br><br>
@endsection

@section('script')
@include('admin.master.product.products._js')

{{-- product Script --}}
<script>
    // start
    ProductObj.init();

    $(document)
        .ready(function() {
            $('#to_slug').prop('readonly', true);
            $('#model_id').hide();
        });

    $(document)
        .on('change', '#product_brand_id', function() {
            $('#model_id').show();
            $('#product_model_id').select2({
                ajax: {
                    type: 'get',
                    url: Helper.apiUrl('/product_model/select2/' + $(this).val()),
                    data: function(params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function(data) {
                        var res = $.map(data.data, function(item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        });
                        return {
                            results: res
                        };
                    }
                }
            });
        });

    // delete vendor
    $(document)
        .on('click', '.btn-delete-vendor', function() {
            uniq = $(this).attr('data-uniq');
            ProductObj.productVendorDelete(uniq);
        })

    // delete varian
    $(document)
        .on('click', '.btn-delete-varian', function() {
            uniq = $(this).attr('data-uniq');
            uniq_varian = $(this).attr('data-uniq_varian');
            ProductObj.deleteVarian(uniq, uniq_varian);
        })

    // submit form
    $('#form-product')
        .submit(function(e) {
            data = {
                name: $('#name').val(),
                product_sku: $('#sku').val(),
                publish_at: $('input[name="publish_at"]').val(),
                product_description: $('#product_description').val(),
                product_model_id: $('#product_model_id').val(),
                product_status_id: $('.select-product-status').val(),
                product_additionals_id: $('#product_additionals_id').val(),
                product_category_id: $('.select-category').val(),
                product_header_attribute: ProductObj.getProductAttHeaderValue(),
                product_stock: $('input[name="header_product_stock"]').val(),
                product_length: $('input[name="header_product_length"]').val(),
                product_width: $('input[name="header_product_width"]').val(),
                product_height: $('input[name="header_product_height"]').val(),
                product_weight: $('input[name="header_product_weight"]').val(),
                product_slug: $('input[name="slug"]').val(),
                vendors: ProductObj.getVendorValue(),
                product_images: getImageSelected(),
                galery_image_id: $('#galery_image_id').val()
            }
            console.log(data);
            ProductObj.store(data);
            localStorage.clear();
            e.preventDefault();
        })
</script>
@endsection
