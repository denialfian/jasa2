<div class="vendor-card header-border main-card mb-3 card vendor-uniq-{{ $uniq }}">

    <div class="card-header">
        PRODUCT VENDOR ({{ $product_vendor->vendor->name }})
        <div class="btn-actions-pane-right">
            <div role="group" class="btn-group-sm btn-group">
                <button style="display:none" data-uniq="{{ $uniq }}" class="btn btn-sm btn-danger btn-delete-vendor btn-delete-vendor-uniq-{{ $uniq }}" type="button" data-id="{{ $product_vendor->id }}">
                    <i class="fa fa-remove"></i> Delete
                </button>
                <button data-uniq="{{ $uniq }}" data-hide="1" class="btn btn-sm btn-white btn-warning btn-hide-vendor btn-hide-vendor-uniq-{{ $uniq }}" type="button">
                    Hide
                </button>
            </div>
        </div>
    </div>
    <div class="card-body vendor-body-uniq-{{ $uniq }}">
        <input type="hidden" name="product_vendor_id[]" value="{{ $product_vendor->id }}" class="product_vendor_input-{{ $uniq }}">
        <div class="position-relative form-group">
            <label>Vendor</label>
            <select class="form-control select-vendor-{{ $uniq }}" name="vendor_id[]" data-uniq="{{ $uniq }}" style="width:100%">
                <option value="{{ $product_vendor->vendor->id }}" selected>{{ $product_vendor->vendor->name }}</option>
            </select>
        </div>

        <div class="position-relative form-group">
            <label>Vendor Code</label>
            <input name="vendor_code[]" value="{{ $product_vendor->vendor_code }}" placeholder="vendor_code" type="text" class="form-control vendor_code_input-{{ $uniq }}">
        </div>

        <div class="position-relative form-group" style="display: none;">
            <label>Vendor Stock</label>
            <input name="vendor_stock[]" value="{{ $product_vendor->stock }}" placeholder="vendor_stock" type="text" class="form-control vendor_stock_input-{{ $uniq }}">
        </div>

        <div class="position-relative form-group">
            <label>Part Code</label>
            <input name="part_code[]" value="{{ $product_vendor->part_code }}" placeholder="part_code" type="text" class="form-control part_code_input-{{ $uniq }}">
        </div>

        <div class="position-relative form-group">
            <label>Brand Code</label>
            <input name="brand_code[]" value="{{ $product_vendor->brand_code }}" placeholder="brand_code" type="text" class="form-control brand_code_input-{{ $uniq }}">
        </div>

        <div class="position-relative form-group">
            <label>Product Code</label>
            <input name="product_code[]" value="{{ $product_vendor->product_code }}" placeholder="product_code" type="text" class="form-control product_code_input-{{ $uniq }}">
        </div>

        <div class="position-relative form-group" style="display: none;">
            <label>Stock</label>
            <input name="vendor_stock[]" value="{{ $product_vendor->stock }}" placeholder="stock" type="text" class="form-control vendor_stock_input-{{ $uniq }}">
        </div>

        <div class="position-relative form-group">
            <label>SKU</label>
            <input name="vendor_sku[]" value="{{ $product_vendor->sku }}" placeholder="sku" type="text" class="form-control vendor_sku_input-{{ $uniq }}">
        </div>

        <div class="varian-area-{{ $uniq }}">
            @foreach($product_vendor->productVarians as $varian)
            @include('admin.master.product.products._product_varian', [
            'varian' => $varian,
            'uniq' => $uniq,
            'uniq_varian' => uniqid()
            ])
            @endforeach
        </div>

        <button data-uniq="{{ $uniq }}" class="btn btn-sm btn-secondary btn-add-varian btn-add-varian-uniq-{{ $uniq }}" type="button">
            <i class="fa fa-plus"></i> Add Varian
        </button>
    </div>
</div>