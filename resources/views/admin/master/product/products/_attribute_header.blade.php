<div class="position-relative form-group attribute-form-uniq-{{ $uniq }}">
    <label>Attribute</label>
    @foreach($data as $key_att => $prodAtt)
    <select data-uniq="{{ $uniq . $key_att }}" class="form-control select-header-attribute  select-header-attribute-uniq-{{ $uniq . $key_att }}" name="attribute_id[]" style="width:100%">
        <option value="{{ $prodAtt['attribute_id'] }}" selected>{{ $prodAtt['attribute_name'] }}</option>
    </select>
    <div class="select-header-term-area-{{ $uniq . $key_att }}">
        <select data-uniq="{{ $uniq . $key_att }}" class="header-term-select form-control select-term-header select-header-term-{{ $uniq . $key_att }}" name="term_id[]" multiple="multiple" style="width:100%">
            @foreach($prodAtt['term_name'] as $key => $term)
            <option value="{{ $term['term_id'] }}" selected>{{ $term['term_name'] }}</option>
            @endforeach
        </select>
    </div>
    @endforeach
</div>
