<style>
#image-gallery .modal-footer{
    display: block;
}

.thumb{
  margin-top: 15px;
  margin-bottom: 15px;
}
</style>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="overflow-y: scroll; max-height:85%;  margin-top: 50px; margin-bottom:50px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Galery Images</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="height: 250px; overflow-y: auto;">
                <div class="row">
                    <div class="row">
                        @foreach ($showImage as $item)
                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="" data-image="" data-target="#image-gallery">
                                    <img class="img-thumbnail"
                                        src="{{asset('/images/'. $item->filename)}}"
                                        alt="Another alt text" height="90" width="90"
                                        data-id="{{ $item->id }}"
                                        data-select="0">
                                </a>
                            </div>
                            
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<style>
    .clasImg{
        border: 1px #000000 solid;
    }
</style>
{{-- @section('script')
    <script>
        
    </script>
@endsection --}}