@extends('admin.home')
@section('content')

<form id="form-save-commission" style="display: contents;">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header header-bg">
                ORDER SETTINGS
            </div>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        @foreach ($getDataSetting as $no => $settings)
                            @if ($settings->name == 'commission_type_rate')
                                <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                                <input type="hidden" value="{{ $settings->id }}" name="id[]">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select class="form-control" name="type[]" id="type_id_commission" style="width:100%">
                                                    @if ($settings->type == '')
                                                        <option value="1">Percentage</option>
                                                        <option value="2">Fix</option>
                                                    @elseif ($settings->type == 1)
                                                        <option value="1">Percentage</option>
                                                        <option value="2">Fix</option>
                                                    @elseif ($settings->type == 2)
                                                        <option value="2">Fix</option>
                                                        <option value="1">Percentage</option>
                                                    @endif
                                                </select><br>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="show_val_commission">
                                                    <div class="input-group mb-3">
                                                        <input type="number" class="form-control" value="{{ $settings->value }}" placeholder="0" id="demo" name="value[]">
                                                        @if($settings->type == 1)
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">% (Percentage)</span>
                                                            </div>
                                                        @elseif($settings->type == 2)
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">Rp. (Rupiah)</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div id="commissionPercent"></div>
                                                <div id="commissionfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @elseif ($settings->name == 'pinalty_before_approve')
                                <input type="hidden" value="{{ $settings->id }}" name="id[]">
                                <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select class="form-control" name="type[]" id="type_id_penalty" style="width:100%">
                                                    @if ($settings->type == '')
                                                        <option value="1">Percentage</option>
                                                        <option value="2">Fix</option>
                                                    @elseif ($settings->type == 1)
                                                        <option value="1">Percentage</option>
                                                        <option value="2">Fix</option>
                                                    @elseif ($settings->type == 2)
                                                        <option value="2">Fix</option>
                                                        <option value="1">Percentage</option>
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="show_val_penalty_before_approve">
                                                    <div class="input-group mb-3">
                                                        <input type="number" class="form-control" value="{{ $settings->value }}" placeholder="0" id="demo" name="value[]">
                                                        @if($settings->type == 1)
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">% (Percentage)</span>
                                                            </div>
                                                        @elseif($settings->type == 2)
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">Rp. (Rupiah)</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div id="penaltyPercent"></div>
                                                <div id="penaltyFix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @elseif ($settings->name == 'pinalty_after_approve_under_24_hours')
                                <input type="hidden" value="{{ $settings->id }}" name="id[]">
                                <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select class="form-control" name="type[]" id="type_id_penalty_under_24" style="width:100%">
                                                    @if ($settings->type == '')
                                                        <option value="1">Percentage</option>
                                                        <option value="2">Fix</option>
                                                    @elseif ($settings->type == 1)
                                                        <option value="1">Percentage</option>
                                                        <option value="2">Fix</option>
                                                    @elseif ($settings->type == 2)
                                                        <option value="2">Fix</option>
                                                        <option value="1">Percentage</option>
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="show_val_penaltyFixUnder24">
                                                    <div class="input-group mb-3">
                                                        <input type="number" class="form-control" value="{{ $settings->value }}" placeholder="0" id="demo" name="value[]">
                                                        @if($settings->type == 1)
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">% (Percentage)</span>
                                                            </div>
                                                        @elseif($settings->type == 2)
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">Rp. (Rupiah)</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div id="penaltyPercentUnder24"></div>
                                                <div id="penaltyFixUnder24"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            

                            @elseif ($settings->name == 'timer_order_setting')
                                <input type="hidden" value="{{ $settings->id }}" name="id[]">
                                <label for="timer">{{ ($no+1) .'. '. $settings->desc }}asd</label>
                                <div class="row" style="margin-bottom:20px;">
                                    <div class="col-md-12">
                                        <input type="hidden" class="form-control" name="type[]" placeholder="Penalty Cancel Order Is Approve" value="hours">
                                        <input type="text" class="form-control" name="value[]" value="{{ $settings->value }}" placeholder="">
                                        <small >set the time before the technician arrives</small>
                                    </div>
                                </div>

                            @elseif ($settings->name == 'penalty_above_24_hours')
                                <input type="hidden" value="{{ $settings->id }}" name="id[]">
                                <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select class="form-control" name="type[]" id="type_id_penalty_above_24" style="width:100%">
                                                    @if ($settings->type == '')
                                                        <option value="1">Percentage</option>
                                                        <option value="2">Fix</option>
                                                    @elseif ($settings->type == 1)
                                                        <option value="1">Percentage</option>
                                                        <option value="2">Fix</option>
                                                    @elseif ($settings->type == 2)
                                                        <option value="2">Fix</option>
                                                        <option value="1">Percentage</option>
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="show_val_penalty_above_24_hours">
                                                    <div class="input-group mb-3">
                                                        <input type="number" class="form-control" value="{{ $settings->value }}" placeholder="0" id="demo" name="value[]">
                                                        @if($settings->type == 1)
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">% (Percentage)</span>
                                                            </div>
                                                        @elseif($settings->type == 2)
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">Rp. (Rupiah)</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div id="penaltyPercentAbove24"></div>
                                                <div id="penaltyFixAbove24"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @elseif ($settings->name == 'commission_penalty_setting')
                                <input type="hidden" value="{{ $settings->id }}" name="id[]">
                                <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select class="form-control" name="type[]" id="penalty_setting" style="width:100%">
                                                    @if ($settings->type == '')
                                                        <option value="1">Percentage</option>
                                                        <option value="2">Fix</option>
                                                    @elseif ($settings->type == 1)
                                                        <option value="1">Percentage</option>
                                                        <option value="2">Fix</option>
                                                    @elseif ($settings->type == 2)
                                                        <option value="2">Fix</option>
                                                        <option value="1">Percentage</option>
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="show_val_setting_penalty">
                                                    <div class="input-group mb-3">
                                                        <input type="number" class="form-control" value="{{ $settings->value }}" placeholder="0" id="demo" name="value[]">
                                                        @if($settings->type == 1)
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">% (Percentage)</span>
                                                            </div>
                                                        @elseif($settings->type == 2)
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">Rp. (Rupiah)</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div id="penaltyPercentSettings"></div>
                                                <div id="penaltyFixSetting"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            @elseif ($settings->name == 'auto_cancel_and_jobs_completed')
                                <input type="hidden" value="{{ $settings->id }}" name="id[]">
                                <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                                <div class="input-group mb-3">
                                    <input type="hidden" class="form-control" name="type[]" placeholder="Hours of Auto Cancel And Jobs Completed" value="hours">
                                    <input type="text" class="form-control" name="value[]" value="{{ $settings->value }}" placeholder="">
                                    <div class="input-group-append">
                                        <span class="input-group-text">Hours.</span>
                                    </div>
                                </div>


                            @elseif ($settings->name == 'garansi_due_date')
                                <input type="hidden" value="{{ $settings->id }}" name="id[]">
                                <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                                <div class="input-group mb-3">
                                    <input type="hidden" class="form-control" name="type[]" value="days" required>
                                    <input type="number" class="form-control" min="1" max="30" name="value[]" value="{{ $settings->value }}" placeholder="" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">Days</span>
                                    </div>
                                </div>
                            @else
                                <span>asdasd</span>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary" id="save_commission">Submit</button>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
    

    $(document).ready(function() {
        $('#type_id_penalty').select2();
    });

    $('#form-save-commission').submit(function(e) {
        data = Helper.serializeForm($(this));

        Helper.loadingStart();
        // post data
        Axios.post(Helper.apiUrl('/general-setting/save_commission'), data)
            .then(function(response) {
                Helper.successNotif('Success, General Setting Has Been saved');
                window.location.href = Helper.redirectUrl('/admin/general-setting');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })

    $('#type_id_commission').change(function() {
        if ($(this).val() == 1) {
            $('#commissionPercent')
                .append('<div class="input-group mb-3">' +
                    '<input type="number" class="form-control" placeholder="0" id="demo" name="value[]">' +
                    '<div class="input-group-append">' +
                    '<span class="input-group-text">% (Percentage)</span>' +
                    '</div>' +
                    '</div>');
            $('#commissionfix').html('');
            $('#show_val_commission').html('');
        } else if ($(this).val() == 2) {
            $('#commissionfix')
                .append('<div class="input-group mb-3">' +
                    '<div class="input-group-append">' +
                    '<span class="input-group-text">Rp. (Rupiah)</span>' +
                    '</div>' +
                    '<input type="number" class="form-control" placeholder="0" id="demo" name="value[]">' +
                    '</div>');
            $('#commissionPercent').html('');
            $('#show_val_commission').html('');
        } else {
            $('#commissionfix').html('');
            $('#commissionPercent').html('');
            $('#show_val').show('');
        }
    });

    $('#type_id_penalty').change(function() {
        if ($(this).val() == 1) {
            $('#penaltyPercent')
                .append('<div class="input-group mb-3">' +
                    '<input type="number" class="form-control" placeholder="0" id="demo" name="value[]">' +
                    '<div class="input-group-append">' +
                    '<span class="input-group-text">% (Percentage)</span>' +
                    '</div>' +
                    '</div>');
            $('#penaltyFix').html('');
            $('#show_val_penalty_before_approve').html('');
        } else if ($(this).val() == 2) {
            $('#penaltyFix')
                .append('<div class="input-group mb-3">' +
                    '<div class="input-group-append">' +
                    '<span class="input-group-text">Rp. (Rupiah)</span>' +
                    '</div>' +
                    '<input type="number" class="form-control" placeholder="0" id="demo" name="value[]">' +
                    '</div>');
            $('#penaltyPercent').html('');
            $('#show_val_penalty_before_approve').html('');
        } else {
            $('#penaltyFix').html('');
            $('#penaltyPercent').html('');
            $('#show_val_penalty_before_approve').show('');
        }
    });

    $('#type_id_penalty_under_24').change(function() {
        if ($(this).val() == 1) {
            $('#penaltyPercentUnder24')
                .append('<div class="input-group mb-3">' +
                    '<input type="number" class="form-control" placeholder="0" id="demo" name="value[]">' +
                    '<div class="input-group-append">' +
                    '<span class="input-group-text">% (Percentage)</span>' +
                    '</div>' +
                    '</div>');
            $('#penaltyFixUnder24').html('');
            $('#show_val_penaltyFixUnder24').html('');
        } else if ($(this).val() == 2) {
            $('#penaltyFixUnder24')
                .append('<div class="input-group mb-3">' +
                    '<div class="input-group-append">' +
                    '<span class="input-group-text">Rp. (Rupiah)</span>' +
                    '</div>' +
                    '<input type="number" class="form-control" placeholder="0" id="demo" name="value[]">' +
                    '</div>');
            $('#penaltyPercentUnder24').html('');
            $('#show_val_penaltyFixUnder24').html('');
        } else {
            $('#penaltyFixUnder24').html('');
            $('#penaltyPercentUnder24').html('');
            $('#show_val_penaltyFixUnder24').show('');
        }
    });

    $('#type_id_penalty_above_24').change(function() {
        if ($(this).val() == 1) {
            $('#penaltyPercentAbove24')
                .append('<div class="input-group mb-3">' +
                    '<input type="number" class="form-control" placeholder="0" id="demo" name="value[]">' +
                    '<div class="input-group-append">' +
                    '<span class="input-group-text">% (Percentage)</span>' +
                    '</div>' +
                    '</div>');
            $('#penaltyFixAbove24').html('');
            $('#show_val_penalty_above_24_hours').html('');
        } else if ($(this).val() == 2) {
            $('#penaltyFixAbove24')
                .append('<div class="input-group mb-3">' +
                    '<div class="input-group-append">' +
                    '<span class="input-group-text">Rp. (Rupiah)</span>' +
                    '</div>' +
                    '<input type="number" class="form-control" placeholder="0" id="demo" name="value[]">' +
                    '</div>');
            $('#penaltyPercentAbove24').html('');
            $('#show_val_penalty_above_24_hours').html('');
        } else {
            $('#penaltyFixAbove24').html('');
            $('#penaltyPercentAbove24').html('');
            $('#show_val_penalty_above_24_hours').show('');
        }
    });

    $('#penalty_setting').change(function() {
        if ($(this).val() == 1) {
            $('#penaltyPercentSettings')
                .append('<div class="input-group mb-3">' +
                    '<input type="number" class="form-control" placeholder="0" id="demo" name="value[]">' +
                    '<div class="input-group-append">' +
                    '<span class="input-group-text">% (Percentage)</span>' +
                    '</div>' +
                    '</div>');
            $('#penaltyFixSetting').html('');
            $('#show_val_setting_penalty').html('');
        } else if ($(this).val() == 2) {
            $('#penaltyFixSetting')
                .append('<div class="input-group mb-3">' +
                    '<div class="input-group-append">' +
                    '<span class="input-group-text">Rp. (Rupiah)</span>' +
                    '</div>' +
                    '<input type="number" class="form-control" placeholder="0" id="demo" name="value[]">' +
                    '</div>');
            $('#penaltyPercentSettings').html('');
            $('#show_val_setting_penalty').html('');
        } else {
            $('#penaltyFixSetting').html('');
            $('#penaltyPercentSettings').html('');
            $('#show_val_setting_penalty').show('');
        }
    });
</script>
@endsection
