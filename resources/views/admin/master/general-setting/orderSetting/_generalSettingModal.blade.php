<form id="form-general-setting">
    <div class="modal fade" id="modal-general-setting" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">GENERAL SETTING ADD</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label>Nama</label>
                    <div class="form-group">
                        <input name="name" placeholder="Nama" id="name" type="text" class="form-control">
                    </div>
                    <label>Value</label>
                    <div class="form-group">
                        <input name="value" placeholder="Nama" id="value" type="text" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>