@extends('admin.home')
@section('content')

<form id="form-social-media-save" style="display: contents;">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header header-bg">
                {{ $title }}
            </div>
            <div class="card-body">
                @foreach ($getDataSetting as $no => $settings)
                <div class="form-row">
                    <div class="form-group col-md-12">
                        @if ($settings->name == 'client_key_facebook')
                            <label for="inputPassword4">{{ $settings->desc }}</label>
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <input type="hidden" value="client_key_facebook" name="name[]">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="value[]" class="form-control" value="{{ $settings->value != null ? $settings->value : '' }}" placeholder="Your Facebook Clinet Key">
                                </div>
                            </div>
                        @endif

                        @if ($settings->name == 'secret_key_facebook')
                        <label for="inputPassword4">{{ $settings->desc }}</label>
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <input type="hidden" value="secret_key_facebook" name="name[]">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="value[]" class="form-control" value="{{ $settings->value != null ? $settings->value : '' }}" placeholder="Your Facebook Secret Key">
                                </div>
                            </div>
                        @endif

                        @if ($settings->name == 'callback_url_facebook')
                        <label for="inputPassword4">{{ $settings->desc }}</label>
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <input type="hidden" value="callback_url_facebook" name="name[]">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="value[]" class="form-control" value="{{ $settings->value != null ? $settings->value : '' }}" placeholder="Https://Your-Domain/auth/facebook/callback">
                                    <small><Strong>Example : Https://Your-Domain/auth/facebook/callback</Strong></small>
                                </div>
                            </div>
                        @endif

                        @if ($settings->name == 'client_key_google')
                            <label for="inputPassword4">{{ $settings->desc }}</label>
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <input type="hidden" value="client_key_google" name="name[]">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="value[]" class="form-control" value="{{ $settings->value != null ? $settings->value : '' }}" placeholder="Your Google Client Key">
                                </div>
                            </div>
                        @endif

                        @if ($settings->name == 'secret_key_google')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <input type="hidden" value="secret_key_google" name="name[]">
                            <label for="inputPassword4">{{ $settings->desc }}</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="value[]" class="form-control" value="{{ $settings->value != null ? $settings->value : '' }}" placeholder="Your Google Secret Key">
                                </div>
                            </div>
                        @endif

                        @if ($settings->name == 'callback_url_google')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <input type="hidden" value="callback_url_google" name="name[]">
                            <label for="inputPassword4">{{ $settings->desc }}</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="value[]" class="form-control" value="{{ $settings->value != null ? $settings->value : '' }}" placeholder="Https://Your-Domain/auth/google/callback">
                                    <small><Strong>Example : Https://Your-Domain/auth/google/callback</Strong></small>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary" id="save_commission">Submit</button>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
    Helper.wysiwygEditor('#contents');
    $('#form-social-media-save').submit(function(e) {
        data = Helper.serializeForm($(this));

        Helper.loadingStart();
        // post data
        Axios.post(Helper.apiUrl('/general-setting/social-media-save'), data)
            .then(function(response) {
                Helper.successNotif('Success, Social Login Has Been saved');
                window.location.href = Helper.redirectUrl('/admin/social-login-setting/show');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>
@endsection