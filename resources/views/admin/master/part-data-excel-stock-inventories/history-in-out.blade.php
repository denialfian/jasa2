@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-bottom:15px;">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/part-data-excel-stock-inventory/show') }}" class="btn btn-primary btn-sm">Back</a>
            </div>
        </div>

    </div>
</div>

<div class="col-md-12">
    <div class="card">
        <input name="id" id="id" value="{{ $pdesi->id }}" type="hidden" class="form-control">
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        <table id="table" class="display table table-hover table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Part Description</th>
                                    <th>ASC Name</th>
                                    <th>Recent Stock</th>
                                    <th>Amount</th>
                                    <th>Type IN/OUT</th>
                                    <th>Created By</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
{{-- <style>
    .table-edit {
        visibility: hidden;
    }
</style> --}}
<script>

var tbl_historyio = globalCRUD.datatables({
        url: '/part-data-excel-stock-inventory/histories-in-out/'+$('#id').val()+'',
        selector: '#table',
        columnsField: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',

            },
            {
                data: 'part_description',
                name: 'part_description',
            },
            {
                data: 'asc_name',
                name: 'asc_name',
            },
            {
                data: 'stock_after',
                name: 'stock_after',
            },
            {
                data: 'amount',
                name: 'amount',
            },
            {
                data: 'type_mutation',
                name: 'type_mutation',
                render: function(data, type, row){
                    // 0 = add_general_journal;
                    // 1 = edit_general_journal;
                    // 2 = delete_general_journal;
                    // 3 = add_order_data_excel;
                    // 4 = edit_order_data_excel;
                    // 5 = delete_order_data_excel;
                    // 6 = add_inventory_part_stock

                    if (data == 0) {
                        return '<span class="badge badge-success">IN</span> Petty'
                    } else if (row.type_mutation == 1 && row.type_amount == 0) {
                        return '<span class="badge badge-danger">Stok Keluar</span> <b>-'+row.amount+'</b> unit edit Petty Part'
                    } else if (row.type_mutation == 1 && row.type_amount == 1) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>+'+row.amount+'</b> unit edit Petty Part'
                    } else if (row.type_mutation == 2) {
                        return '<span class="badge badge-danger">Stok Keluar</span> <b>'+row.amount+'</b> unit (deleted) Petty'
                    } else if (data == 3) {
                        return '<span class="badge badge-danger">OUT</span> Orders Excel #<b>'+(row.part_data_excel != null ? row.part_data_excel.order_data_excel.service_order_no : '(deleted)')+'</b>';
                    } else if (row.type_mutation == 4 && row.type_amount == 0) {
                        return '<span class="badge badge-success">Stok Keluar</span> <b>'+row.amount+'</b> unit edit Order Data Excel #<b>'+(row.part_data_excel != null ? row.part_data_excel.order_data_excel.service_order_no : '(deleted)')+'</b>'
                    } else if (row.type_mutation == 4 && row.type_amount == 1) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit edit Order Data Excel #<b>'+(row.part_data_excel != null ? row.part_data_excel.order_data_excel.service_order_no : '(deleted)')+'</b>'
                    }  else if (data == 5) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit delete Order Data Excel #<b>(deleted)</b>';
                    } else if (data == 6) {
                        return '<span class="badge badge-success">Stok Masuk</span> <b>'+row.amount+'</b> unit, Create New Part Stock</b>'
                    }

                }
            },

            {
                data: 'created_at',
                name: 'created_at',
                render: function(data, type, row) {
                    return '<span class="badge badge-primary">'+row.user.name+' </span><br>'+moment(data).format("DD MMMM YYYY hh:mm:ss");
                }
            },
            {
                render: function(data, type, row){
                    if(row.is_deleted == 0) {
                        if(row.type_mutation == 0 || row.type_mutation == 3) {
                            var button_delete = "<button title='Delete' data-id='" + row.id + "' data-type_mutation='" + row.type_mutation + "' class='tbl_delete btn-hover-shine btn btn-danger btn-sm' data-gjd_id='" + row.general_journal_details_id + "' data-pde_id='" + row.part_data_excel_id + "'><i class='fa fa-trash'></i></button>";
                            return button_delete;
                        } else {
                            return '-';
                        }
                    }
                }
            }
        ],
        actionLink : ""

    });

    $(document).on('click', '.tbl_delete', function() {
        var gjd_id = $(this).attr('data-gjd_id');
        var pde_id = $(this).attr('data-pde_id');
        var type_mutation = $(this).attr('data-type_mutation');
        if (type_mutation == 0 ) {
            tbl_historyio.delete('/general-journal/destroy_details_data/'+gjd_id);
        } else if (type_mutation == 3 ){
            tbl_historyio.delete('/orders-data-excel/destroy_part_data/'+pde_id);
        }
    });






</script>

@endsection
