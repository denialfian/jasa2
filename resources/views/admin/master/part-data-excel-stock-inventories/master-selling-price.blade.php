@extends('admin.home')
@section('content')

<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                Master Selling Price Inventories
            </div>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        <table id="table" class="display table table-hover table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <tr>
                                        <th>No</th>
                                        <th>Code Material</th>
                                        <th>ASC Name</th>
                                        <th>Part Description</th>
                                        <th>Hpp Avg Price</th>
                                        <th>Selling Price</th>
                                    </tr>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>

    var tbl = globalCRUD.datatables({
        url: '/part-data-excel-stock-inventory/datatables',
        selector: '#table',
        actionLink: "",
        columnsField: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',

            },
            {
                data: "part_data_stock.code_material",
                name: "code_material",
            },
            {
                data: "part_data_stock.asc.name",
                name: "asc_name",
            },
            {
                data: "part_data_stock.part_description",
                name: "part_description",
            },
            {
                name: 'hpp_average',
                data: 'hpp_average',
                render: function(data, type, row) {
                    return 'Rp. '+Helper.toCurrency(data);
                }
            },
            {
                data: 'selling_price',
                name: 'selling_price',
                render: function(data, type, row) { //margin_rate_manual
                    var nilai = data != null ? data : 0;
                    Helper.onlyNumberInput('.number_only');
                    return `<div class='form-row'>
                                <div class='col-md-8'>
                                    <input type='text' class='form-control form-control-sm number_only selling_price_${row.id}' name='selling_price' data-pdesi_id="${row.id}" max='100' min='0' value=${nilai}>
                                </div>
                                <div class='col-md-4'>
                                    <button class='btn btn-primary btn-sm mr-2 update_selling_price' data-part_description="${row.part_data_stock.part_description}"  data-pdesi_id="${row.id}" title='Update Selling Price'>Update</button>
                                </div>
                            </div>`;

                }
            },

        ]
    });


    $(document).on('click', '.update_selling_price', function(e) {
        var $this = $(this);
        Helper.confirm(function() {
            Helper.unMask('.currency');
            $.ajax({
                url:Helper.apiUrl('/part-data-excel-stock-inventory/set-selling-price'),
                type: 'post',
                data: {
                    pdesi_id : $this.attr('data-pdesi_id'),
                    selling_price : $('.selling_price_'+$this.attr('data-pdesi_id')+'').val(),
                },
                success: function(response) {
                    console.log(response);
                    if (response != 0) {
                        Helper.successNotif('Success update Selling Price pada Part Description = '+$this.attr('data-part_description')+'');
                        tbl.table.reloadTable();
                    }
                },
                error: function(xhr, status, error) {
                    handleErrorResponse(error);
                },
            });

            e.preventDefault();
        });
    });


</script>
@endsection
