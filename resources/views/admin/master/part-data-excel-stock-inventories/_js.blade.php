<script>
    var _uniq_counter_description = 1;
    Helper.selectMonth();
    Helper.onlyNumberInput('.number_only');

    var ClassApp = {

        onchangeCodeMaterial: function(val) {
            globalCRUD.select2('#part_data_stock_id','/part-data-excel-stock/select2/code_material/'+val+'', function(item) {
                return {
                    id: item.id,
                    text:item.code_material,
                    part_description:item.part_description,
                    hpp_average:item.hpp_average
                }
            });
        },

        saveData: function(el,e) {
            Helper.unMask('.currency');
            var form = Helper.serializeForm(el);
            var fd = new FormData(el[0]);
            Axios.post('/part-data-excel-stock-inventory', fd)
                .then(function(response) {
                    if (response.status == 204) {
                        Helper.successNotif(Helper.deleteMsg());
                    } else {
                        Helper.successNotif(response.data.msg);
                    }
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
            e.preventDefault();
        },

        updateData: function(el, e, id) {
            Helper.unMask('.currency');
            var form = Helper.serializeForm(el);
            var fd = new FormData(el[0]);
            Axios.post('/part-data-excel-stock-inventory/'+id+'', fd)
                .then(function(response) {
                    if (response.status == 204) {
                        Helper.successNotif(Helper.deleteMsg());
                    } else {
                        Helper.successNotif(response.data.msg);
                    }
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
            e.preventDefault();
        }

    }

    globalCRUD.select2("#asc_id", '/asc-excel/select2', function(item) {
        return {
            id: item.id,
            text: item.name
        }
    })

    $(document).on('change', '#asc_id', function() {
        // $('#part_data_stock_id').val('');
        ClassApp.onchangeCodeMaterial($(this).val())
        $('#part_data_stock_id').prop('disabled', false);
    });

    $('#part_data_stock_id').on('select2:select', function (e) {
        $('#part_description').val(e.params.data.part_description);
    });


</script>
