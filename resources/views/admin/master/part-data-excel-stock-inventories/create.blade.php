@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/part-data-excel-stock-inventory/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;" autocomplete="off">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-body">
                <label>ASC Name</label>
                <div class="form-group">
                    <select name="asc_id" id="asc_id" class="form-control js-example-basic-single" style="width:100%">
                    </select>
                </div>
                <label>Code Material</label>
                <div class="form-group">
                    <select name="part_data_stock_id" id="part_data_stock_id" class="form-control js-example-basic-single" style="width:100%">
                    </select>
                </div>
                <label>Part Description</label>
                <div class="form-group">
                    <input name="part_description" id="part_description" type="text" class="form-control" readonly>
                </div>
                <label>Stock</label>
                <div class="form-group">
                    <input name="stock" placeholder="Stock" id="stock" type="text" class="form-control number_only">
                </div>
                <label>Selling Price</label>
                <div class="form-group">
                    <input name="selling_price" id="selling_price" type="text" class="form-control currency">
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-footer ">
            <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> CREATE </button>
        </div>
    </div>
</form>

@endsection

@section('script')
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
@include('admin.master.part-data-excel-stock-inventories._js')
<script>

    $(function() {
        Helper.currency('.currency');
        $('#part_data_stock_id').prop('disabled', true);
    });

    $("#form-data").submit(function(e) {
        ClassApp.saveData($(this),e);
    });
</script>

@endsection
