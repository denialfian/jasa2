@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/part-data-excel-stock-inventory/show') }}" class="mb-2 mr-2 btn btn-primary " style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
    </div>
</div>
<form id="form-data" style="display: contents;" autocomplete="off">
    <input type="hidden" name="id" value="{{ $getData->id }}">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="card-body">
                <label>ASC Name</label>
                <div class="form-group">
                    <select name="asc_id" id="asc_id" class="form-control js-example-basic-single" style="width:100%" value="{{ $getData->code }}">
                        <option value="{{ $getData->part_data_stock->asc_id }}" selected>{{ $getData->part_data_stock->asc->name }}</option>
                    </select>
                </div>
                <label>Code Material</label>
                <div class="form-group">
                    <select name="part_data_stock_id" id="part_data_stock_id" class="form-control js-example-basic-single" style="width:100%">
                        <option value="{{ $getData->part_data_stock_id }}" selected>{{ $getData->part_data_stock->code_material }}</option>
                    </select>
                </div>
                <label>Part Description</label>
                <div class="form-group">
                    <input name="part_description" id="part_description" type="text" class="form-control" readonly value="{{ $getData->part_data_stock->part_description }}">
                </div>
                <label>Stock</label>
                <div class="form-group">
                    <input placeholder="Stock" id="stock" type="text" class="form-control number_only" value="{{ $getData->stock }}" disabled>
                </div>
                <label>Selling Price</label>
                <div class="form-group">
                    <input name="selling_price" id="selling_price" type="text" class="form-control currency" value="{{ $getData->selling_price }}">
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card-footer ">
            <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> SAVE </button>
        </div>
    </div>
</form>


@endsection

@section('script')
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
@include('admin.master.part-data-excel-stock-inventories._js')
<script>
    Helper.currency('.currency');
    var id = $('input[name="id"]').val();
    ClassApp.onchangeCodeMaterial($('#asc_id').val());
    $("#form-data").submit(function(e) {
        ClassApp.updateData($(this),e,id);
    });

    ClassApp.getHppAverageMonth($('#part_data_stock_id').val());

</script>

@endsection
