@extends('admin.home')
@section('content')
<div class="col-md-4" style="margin-bottom: 20px;">
    <div class="pt-0 pb-0 card">
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <div class="widget-content p-0">
                    <div class="widget-content-outer">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">Earnings This Month ({{ date('M') }} {{ date('Y') }})</div>
                                @php
                                    if(!empty($totalPart)){
                                        $allTotal = $totalPart + $getTotalService;
                                    }else{
                                        $allTotal = $getTotalService;
                                    }
                                @endphp
                                <div class="widget-subheading"><strong style="color: #1600a2; font-size:20px"> Rp.{{ number_format($allTotal) }}</strong></div>
                            </div>
                            <div class="widget-content-right">
                                <div class="widget-numbers text-success"></div><br>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="col-md-4" style="margin-bottom: 20px;">
    <div class="pt-0 pb-0 card">
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <div class="widget-content p-0">
                    <div class="widget-content-outer">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">Order Complete Last Mounth ({{ date("F",strtotime("-1 month")) }} {{ date('Y') }})</div>
                                @php
                                    if(!empty($totalPart)){
                                        $allTotal = $totalPart + $completeOrder;
                                    }else{
                                        $allTotal = $completeOrder;
                                    }
                                @endphp
                                <div class="widget-subheading"><strong style="color: #009297; font-size:20px">Rp.{{ number_format($completeOrder) }}</strong></div>
                            </div>
                            <div class="widget-content-right">
                                <div class="widget-numbers text-danger"></div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
@php
    $sumTotalBalance = 0;    
@endphp
@if(!empty($balance))
    @foreach ($balance as $pendingBalance)
        @php
            $total_harga = $pendingBalance->total; 
            $sumTotalBalance += $total_harga;
        @endphp
        
    @endforeach
@endif
<div class="col-md-4" style="margin-bottom: 20px;">
    <div class="pt-0 pb-0 card">
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <div class="widget-content p-0">
                    <div class="widget-content-outer">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="widget-heading">Pending Balance</div>
                                <div class="widget-subheading"><a href="{{ url('/teknisi/balance/detail') }}">View History</a> :<strong style="color: #970000; font-size:20px"> Rp. {{ number_format($sumTotalBalance) }}</strong></div>
                            </div>
                            <div class="widget-content-right">
                                
                                <div class="widget-numbers text-success"> </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="col-md-12 col-xl-12" style="margin-bottom: 20px">
    <div class="divider mt-0" style="margin-bottom: 30px;"></div>
        @if(Auth::user()->status == 7 )
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header-tab card-header">
                        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                            Technician Register
                        </div>
                    </div>
                    <div class="card-body">
                        <h3 align="center">Your account is being verified by admin</h3>
                        <p align="center">please attention to the login notification to your email account</p>
                    </div>
                </div>
            </div>
        @else
        <div class="card">
            <div class="header-border card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    JOB REQUEST
                </div>
            </div>
            <div class="card-body">
                <div class="tab-pane show active" id="tab-1" role="tabpanel">
                    <table id="table-list-orders" class="display table table-hover table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Orders Code</th>
                                <th>Name Service</th>
                                <th>Status</th>
                                <th>Create Date</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="d-block card-footer">
                <button class="btn-wide btn btn-success"><i aria-hidden="true" class="fa fa-arrow-circle-right" title="go"></i> VIEW MORE</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" data-backdrop="false" id="modal-job-request" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Detail Job
                </h4>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button class="btn waves-effect" data-dismiss="modal" type="button">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
@endif
@endsection

@section('script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js">
</script>
<script>
    // agar user profile bisa diclik
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })

    $('.detail-job').click(function(e){
        $('#modal-job-request').modal('show');
    });

    var table = $('#table-list-orders')
        .DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            select: true,
            dom: 'Bflrtip',
            ordering:'true',
            order: [3, 'desc'],
            responsive: true,
            language: {
                search: '',
                searchPlaceholder: "Search..."
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/teknisi/request-job/datatables/all'),
                type: 'get',
            },
            columns: [
                {
                    data: "code",
                    name: "code",
                    render: function(data, type, full) {
                        return "<a href='/teknisi/request-job-accept/"+ full.id + "'># "+full.code+"</a>";
                    }
                },
                {
                    data: "service.name",
                    name: "service.name",
                },
                {
                    data: "order_status.name",
                    name: "order_status.name",
                    render: function(data, type, full) {
                        var text = '';
                        if(full.order_status.id === 3 && full.is_less_balance === 1) {
                            text = '<span class="badge badge-warning">'+data+'</span> <br/><span class="badge badge-danger">Less Balance</span>';
                        } else {
                            if(full.orders_statuses_id === 2 ){
                                return '<span class="badge badge-danger" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 3 ){
                                return '<span class="badge badge-warning" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 4 ){
                                return '<span class="badge badge-primary" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 5 ){
                                return '<span class="badge badge-danger" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 6 ){
                                return '<span class="badge badge-warning" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 7 ){
                                return '<span class="badge badge-focus" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 8 ){
                                return '<span class="badge badge-danger" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 9 ){
                                return '<span class="badge badge-primary" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 10 ){
                                return '<span class="badge badge-success" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 11 ){
                                return '<span class="badge badge-danger" style="color:white">'+full.order_status.name+'</span>';
                            }
                        }
                        return text;
                    }
                },
                {
                    data: "created_at",
                    name: "created_at",
                    render: function(data, type, full){
                        return moment(full.created_at).format("DD MMMM YYYY hh:mm");
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        if (full.order_status.id == 2 || full.order_status.id == 3 || full.order_status.id == 4 || full.order_status.id == 5 || full.order_status.id == 7 || full.order_status.id == 8 || full.order_status.id == 9 || full.order_status.id == 10) {
                            return "<a href='/teknisi/request-job-accept/"+ full.id + "' class='badge badge-primary btn-sm'  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>";
                        } else if (full.order_status.id == 11) {
                            return "<a href='/teknisi/transacsion_list/complaint/detail/"+ full.id +"' class='badge badge-success btn-sm '  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>";
                        } else{
                            return "-"
                        }

                    }
                }
            ]
        });
</script>
@endsection
