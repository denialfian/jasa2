@extends('admin.home')
@section('content')
<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }

    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    #img-upload {
        width: 100%;
    }
</style>
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
            </div>
        </div>

        <div class="card-body">
            <label>Service Name</label>
            <div class="form-group">
                <select class="js-example-basic-single" id="ms_services_id" name="ms_services_id" style="width:100%"></select>
            </div>

            <label>Name Sparepart</label>
            <div class="form-group">
                <input name="name" placeholder="Name Sparepart" type="text" class="form-control">
            </div>

            <label>Price</label>
            <div class="form-group">
                <input name="price" id="price" placeholder="Price" type="text" class="form-control">
            </div>

            <label>Description</label>
            <div class="form-group">
                <textarea name="description" id="description" class="form-control"></textarea>
            </div>

            <label>Image</label>
            <div class="form-group">
                <input name="images" placeholder="Image" type="file" class="form-control" id="imgInp">
                <br />
                <div class="row">
                    <div class="col-md-4">
                        <img id='img-upload' class="img-responsive" />
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-success btn-sm" id="save"><i class="fa fa-plus" aria-hidden="true"></i> Submit</button>
            </div>
        </div>

    </div>
</div>
@endsection

@section('script')
<script>
    Helper.currency('#price');

    globalCRUD
        .select2("#ms_services_id", '/services/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })

    $("#save").click(function() {
        Helper.unMask('#price')
        var fd = new FormData();
        var files = $('#imgInp')[0].files[0];
        if (files) {
            fd.append('images', files);
        }
        fd.append('ms_services_id', $('#ms_services_id').val());
        fd.append('name', $('input[name="name"]').val());
        fd.append('price', $('input[name="price"]').val());
        fd.append('description', $('#description').val());
        Helper.loadingStart();
        $.ajax({
            url: Helper.apiUrl('/technician_sparepart'),
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            success: function(response) {
                Helper.loadingStop();
                if (response != 0) {
                    iziToast.success({
                        title: 'OK',
                        position: 'topRight',
                        message: 'Technician Sparepart Has Been Saved',
                    });
                    window.location.href = Helper.redirectUrl('/teknisi/technician_sparepart/show');
                }
            },
            error: function(xhr, status, error) {
                Helper.loadingStop();
                if (xhr.status == 422) {
                    error = xhr.responseJSON.data;
                    _.each(error, function(pesan, field) {
                        $('#error-' + field).text(pesan[0])
                        iziToast.error({
                            title: 'Error',
                            position: 'topRight',
                            message: pesan[0]
                        });

                    })
                }

            },
        });
    });
</script>


<script>
    $(document).ready(function() {
        $(document).on('change', '.btn-file :file', function() {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = label;

            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }

        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#img-upload').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function() {
            readURL(this);
        });
    });
</script>

@endsection