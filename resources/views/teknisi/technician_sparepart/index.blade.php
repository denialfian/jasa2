@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
            </div>
        </div>
        <div class="card-body">
            <table style="width: 100%;" id="table-technician-sparepart" class="table table-hover table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Service Name</th>
                        <th>Name Sparepart</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <a href="{{ url('teknisi/technician_sparepart/create') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Technician Sparepart</a>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        url: '/technician_sparepart/datatables',
        selector: '#table-technician-sparepart',
        columnsField: ['id', 'service.name', 'name', 'price'],
        actionLink: {
            update: function(row) {
                return "/teknisi/technician_sparepart/edit/" + row.id;
            },
            delete: function(row) {
                return "technician_sparepart/" + row.id;
            }
        }
    })
</script>
@endsection