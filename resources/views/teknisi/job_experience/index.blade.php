@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                
            </div>
        </div>
        <div class="card-body table-responsive">
            <table id="table-job_experience" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th style="width: 20px;">No</th>
                        <th>position</th>
                        <th>Company Nname</th>
                        <th>Period Start</th>
                        <th>Period End</th>
                        <th>Type</th>
                        <th>User</th>
                        <th>Job title</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-job_experience"><i class="fa fa-plus"></i> Add Job Title Category</button>
        </div>
    </div>
</div>

<form id="form-job_experience">
    <div class="modal fade" id="modal-job_experience" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Job Title Category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label>Job Catgeory</label>
                    <div class="form-group">
                        <select class="form-control job-title-select" id="job_title_id" name="job_title_id" style="width:100%" required>
                            <option value="" selected>Pilih Job Title</option>
                        </select>
                    </div>
                    <label>User</label>
                    <div class="form-group">
                        <select class="form-control user-select" id="user_id" name="user_id" style="width:100%" required>
                            <option value="" selected>Pilih User</option>
                        </select>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <label>Company Name</label>
                            <div class="form-group">
                                <input name="company_name" placeholder="company_name" id="company_name" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Position</label>
                            <div class="form-group">
                                <input name="position" placeholder="position" id="position" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Period Start</label>
                            <div class="form-group">
                                <input name="period_start" placeholder="period_start" id="period_start" type="date" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Period End</label>
                            <div class="form-group">
                                <input name="period_end" placeholder="period_end" id="period_end" type="date" class="form-control">
                            </div>
                        </div>
                    </div>
                    <label>type</label>
                    <div class="form-group">
                        <select class="form-control" id="type" name="type" style="width:100%" required>
                            <option value="0" selected>Pengalaman Internal</option>
                            <option value="1">Pengalaman External</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-sm waves-effect">Save</button>
                    <button type="button" class="btn waves-effect btn-sm btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<style>
.modal-dialog,
.modal-content {
    height: 500px;
}

.modal-body {
    /* 100% = dialog height, 120px = header + footer */
    max-height: calc(100% - 120px);
    overflow-y: scroll;
}
</style>
<script>
    globalCRUD
        .select2Static(".job-title-select", '/job_title/select2', function(item) {
            return {
                id: item.id,
                text: item.description
            }
        })
        .select2Static(".user-select", '/user/select2');

    globalCRUD.datatables({
        url: '/job_experience/datatables',
        selector: '#table-job_experience',
        columnsField: [
            'DT_RowIndex', 
            'position', 
            'company_name', 
            {
                data: "period_start",
                name: "period_start",
                render: function(data, type, full){
                    return moment(full.period_start).format("DD MMMM YYYY");
                }
            }, 
            {
                data: "period_end",
                name: "period_end",
                render: function(data, type, full){
                    return moment(full.period_end).format("DD MMMM YYYY");
                }
            }, 
            'job_type', 
            'user.name', 
            'job_title.description'
        ],
        modalSelector: "#modal-job_experience",
        modalButtonSelector: ".add-job_experience",
        modalFormSelector: "#form-job_experience",
        actionLink: {
            store: function() {
                return "/job_experience";
            },
            update: function(row) {
                return "/job_experience/" + row.id;
            },
            delete: function(row) {
                return "/job_experience/" + row.id;
            },
        },
    })
</script>
@endsection
