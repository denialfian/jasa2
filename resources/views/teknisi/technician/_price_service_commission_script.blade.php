<script>
$('#value').on('keyup', function() {
        commission($(this).val());
    });
    function commission(thisObj) {
        Helper.unMask('#value');
        var price = 0;
        if (thisObj != '') {
            price = thisObj;
        }
        var commission =  parseInt(price) * (parseInt($('#commission').val()) / 100);
        var after_commission = parseInt(price) - commission;
        $('#commission_display').show();
        $('#after_commission_display').show();
        $('#commission_value').val(commission);
        $('#after_commission').val(after_commission);
    }
</script>
