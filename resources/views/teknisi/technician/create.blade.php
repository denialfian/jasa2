@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <form id="form-teknisi" method="POST">
                <h3>Tehcnician Information</h3>
                <fieldset>
                    <label>
                        User
                    </label>
                    <div class="form-group">
                        <select class="form-control user-select" id="user_id" name="user_id" required="" style="width:100%">
                            <option selected="" value="">
                                Pilih User
                            </option>
                        </select>
                    </div>
                    <label>
                        Job Title Category
                    </label>
                    <div class="form-group">
                        <select class="form-control job-category-select" id="job_title_category_id" name="job_title_category_id" required="" style="width:100%">
                            <option selected="" value="">
                                Pilih Category
                            </option>
                        </select>
                    </div>
                    <label>
                        Job Title Description
                    </label>
                    <div class="form-group">
                        <input class="form-control" id="job_title_description" name="job_title_description" placeholder="description" type="text">
                        </input>
                    </div>
                    <label>
                        Skill
                    </label>
                    <div class="form-group">
                        <input class="form-control" id="skill" name="skill" placeholder="skill" type="text">
                        </input>
                    </div>
                </fieldset>
                <h3>Tehcnician Job Expperience</h3>
                <fieldset>
                    <div class="table-responsive">
                        <button class="btn btn-primary btn-sm add-job_experience" type="button">
                            Add Job Expperience
                        </button>
                        <hr>
                        @include('admin.teknisi.technician._job_experience_table_html')
                        </hr>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
@include('admin.teknisi.technician._job_experience_modal_html')
@include('admin.teknisi.technician._style_form_wizard')
@endsection

@section('script')
@include('admin.teknisi.technician._job_experience_table_script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js">
</script>
{{-- jquery autocomplate --}}
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    // agar user profile bisa diclik
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })

    // select 2
    $(document)
    	.ready(function(){
    		// load select2
	        globalCRUD
	        	.select2Static(".job-title-select", '/job_title/select2', function(item) {
		            return {
		                id: item.id,
		                text: item.description
		            }
		        })
	            .select2(".job-category-select", '/job_title_category/select2')
	            .select2(".user-select", '/user/teknisi-select2');

                // autocomplate
                $( ".autocomplete-job-category" ).autocomplete({
                    source: function( request, response ) {
                      $.ajax({
                        url: Helper.apiUrl('/job_title_category/select2'),
                        // dataType: "jsonp",
                        data: {
                          q: request.term
                        },
                        success: function( resp ) {
                          response( 
                            _.map(resp.data, function(row){
                              return row.name;
                            })              
                          );
                        }
                      });
                    },
                    minLength: 3,
                    select: function( event, ui ) {
                      console.log('event', event)
                      console.log('ui', ui)
                    },
                });
    	})

    // form wizard
    var form = $('#form-teknisi').show();
    form.steps({
        headerTag: 'h3',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        onInit: function (event, currentIndex) {
            //Set tab width
            var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
            var tabCount = $tab.length;
            $tab.css('width', (100 / tabCount) + '%');
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex > newIndex) { return true; }

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            input = Helper.serializeForm($('#form-teknisi'));
            if (validasiData(input) == false) {return false}

            return true;
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
        	$(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
        },
        onFinishing: function (event, currentIndex) {
            return true;
        },
        onFinished: function (event, currentIndex) {
            experiences = table.rows().data();
	    	input = Helper.serializeForm($('#form-teknisi'));
	    	
	    	data = input;
	    	data.experiences = [];
	    	_.each(experiences, function(ex){
	    		data.experiences.push(ex);
	    	})

            Helper.loadingStart();
	    	Axios.post('/technician', data)
                .then(function(response) {
                    // send notif
                    Helper.successNotif(response.data.msg).redirectTo('/admin/technician');
                })
                .catch(function(error) {
                    Helper.loadingStop();
                    Helper.handleErrorResponse(error)
                });
        }
    });

    // table
    var table = JobExperience.staticTable('#table-job_experience');

    // add job experience
    $(document)
    	.on('click', '.add-job_experience', function(){
	        $('#modal-job_experience').modal('show');
	    })

    // delete job experience
    $(document)
    	.on('click', '.delete-experience', function(){
	        table.row( $(this).parents('tr') ).remove().draw();
	    })

    // save job experience
    $('#form-job_experience')
	    .submit(function(e){
	        input = Helper.serializeForm($(this));
	        table.row.add({
	          	position: input.position,
	          	company_name: input.company_name,
	          	period_start: input.period_start,
	          	period_end: input.period_end,
	          	type: input.type,
	          	job_title_category_name: input.job_title_category_name,
	          	job_title_description: input.job_title_description_ex,	          	
	        }).draw();

	        $('#modal-job_experience').modal('hide');
	        e.preventDefault();
	    })

    function validasiData(input){
        valid = true;
        $('.error-validation-mgs').remove();
        _.each(input, function(pesan, field) {
            if (pesan == '') {
                valid = false;
                // append new message
                $('[name="' + field + '"]').after('<strong><span class="error-validation-mgs" id="error-' + field + '" style="color:red">' + field + ' Required</span></strong>');
                // stop loop
                return false;
            }
        })

        return valid;
    }

    Helper.onlyNumberInput('#value');
</script>
@endsection
