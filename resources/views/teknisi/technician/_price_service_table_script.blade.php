<script>
    var PriceService = {
		table: function(url = '/technician_profile/price_service'){
			return $('#table-price_service').DataTable({
	            processing: true,
                serverSide: true,
                select: true,
                dom: 'Bflrtip',
                responsive: true,
                language: {
                    search: '',
                    searchPlaceholder: "Search..."
                },
                oLanguage: {
                    sLengthMenu: "_MENU_",
                },
                dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                ajax: {
                    url: Helper.apiUrl(url),
                    "type": "get"
                },
	            columns: [
	            	{
	                    data: "service_type.symptom.services.name",
	                    name: "service_type.symptom.services.name",
	                    orderable: false,
	                },
	            	{
	                    data: "service_type.symptom.name",
	                    name: "service_type.symptom.name",
	                    orderable: false,
	                },
	            	{
	                    data: "service_type.name",
	                    name: "service_type.name",
	                    orderable: false,
	                },
					{
	                    data: "product_group.name",
	                    name: "product_group.name",
	                    orderable: false,
	                },
	                {
	                    data: "value",
	                    name: "value",
	                    render: function(data, type, row){
	                    	return Helper.thousandsSeparators(row.value);
	                    }
	                },
	                {
						data: "id",
	                    name: "id",
	                	render: function(data, type, full) {
		                    return "<button data-id='"+full.id+"' type='button' class='edit-price btn btn-warning btn-white btn-sm'><i class='fa fa-pencil'></i></button> <button data-id='"+full.id+"' type='button' class='delete-price btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
		                }
	                }
	            ],
	        });
		}
	};
</script>