@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
    
            </div>
        </div>
        <div class="card-body">
            <table id="table-job_title_category" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th style="width: 20px;">No</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-job_title_category"><i class="fa fa-plus"></i> Add Job Title Category</button>
        </div>
    </div>
</div>

<form id="form-job_title_category">
    <div class="modal fade" id="modal-job_title_category" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Job Title Category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label>Name</label>
                    <div class="form-group">
                        <input name="name" placeholder="name" id="name" type="text" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect btn-sm"><i class="fa fa-plus"></i> Save</button>
                    <button type="button" class="btn waves-effect btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        url: '/job_title_category/datatables',
        selector: '#table-job_title_category',
        columnsField: ['DT_RowIndex', 'name'],
        modalSelector: "#modal-job_title_category",
        modalButtonSelector: ".add-job_title_category",
        modalFormSelector: "#form-job_title_category",
        actionLink: {
            store: function() {
                return "/job_title_category";
            },
            update: function(row) {
                return "/job_title_category/" + row.id;
            },
            delete: function(row) {
                return "/job_title_category/" + row.id;
            },
        },
    })
</script>
@endsection
