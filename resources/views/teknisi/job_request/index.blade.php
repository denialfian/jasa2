@extends('admin.home')

@section('content')
<div class="col-md-12">
    <div class="card header-border">
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="">Schedule</label>
                            <input name="order_date_range" type="text" value="" class="form-control" />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="label-control">Warranty Claims</label>
                            <select class="form-control" name="klaim_garansi">
                                <option value="">All</option>
                                <option value="1">Only Warranty Claims</option>
                                <option value="0">Without Warranty Claims</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="label-control">Payment Type</label>
                            <select class="form-control" name="payment_type">
                                <option value="">All</option>
                                <option value="1">Online</option>
                                <option value="0">Offline</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="position-relative form-group">
                            <label for="" class="label-control">Status</label>
                            <select class="form-control select-status" name="order_status_id[]" multiple style="width: 100%">
                                <option value="2">Analyzing</option>
                                <option value="3">Waiting Approval</option>
                                <option value="4">On Working</option>
                                <option value="9">Processing</option>
                                <option value="7">Job Done</option>
                                <option value="10">Job Completed</option>
                                <option value="11">Complaint</option>
                                <option value="5">Cancel</option>
                            </select>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>

        <div class="card-body">
            <div class="tab-pane show active" id="tab-1" role="tabpanel">
                <table id="table-list-orders" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Orders Code</th>
                            <th>Customer Name</th>
                            <th>Payment Type</th>
                            <th>Total</th>
                            <th>Status</th>
                            <th>Create Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    $(function() {
        $('input[name="order_date_range"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="order_date_range"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        });

        $('input[name="order_date_range"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        globalCRUD.select2(".select-status")
    });

    $(document).ready(function() {
        var tableOrder = $('#table-list-orders').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            scrollX: true,
            ordering: 'true',
            order: [5, 'desc'],
            responsive: true,
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/teknisi/request-job/datatables'),
                type: 'GET',
            },
            columns: [{
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        return "<a href='/teknisi/request-job-accept/"+ full.id + "'># "+full.code+"</a>";
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        if (full.user == null) {
                            return "Deleted Users";
                        } else {
                            return full.user.name;
                        }
                    }
                },
                {
                    data: "payment_type",
                    name: "payment_type",
                    render: function(data, type, full) {
                        status = full.payment_type == 0 ? '<div class="badge badge-danger ml-2">Offline</div>' : '<div class="badge badge-info ml-2">Online</div>';
                        return status;
                    }
                },
                {
                    data: "grand_total",
                    name: "grand_total",
                    render: $.fn.dataTable.render.number(',', '.,', 0, 'Rp. ', '.00')
                },
                {
                    data: "order_status.name",
                    name: "order_status.name",
                    render: function(data, type, full) {
                        var text = '';
                        if(full.order_status.id === 3 && full.is_less_balance === 1) {
                            text = '<span class="badge badge-warning">'+data+'</span>';
                        } else {
                            if(full.orders_statuses_id === 2 ){
                                return '<span class="badge badge-danger" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 3 ){
                                return '<span class="badge badge-warning" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 4 ){
                                return '<span class="badge badge-primary" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 5 ){
                                return '<span class="badge badge-danger" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 6 ){
                                return '<span class="badge badge-warning" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 7 ){
                                return '<span class="badge badge-focus" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 8 ){
                                return '<span class="badge badge-danger" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 9 ){
                                return '<span class="badge badge-primary" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 10 ){
                                return '<span class="badge badge-success" style="color:white">'+full.order_status.name+'</span>';
                            }else if(full.orders_statuses_id === 11 ){
                                return '<span class="badge badge-danger" style="color:white">'+full.order_status.name+'</span>';
                            }
                        }
                        return text;
                    }
                },
                {
                    data: "created_at",
                    name: "created_at",
                    render: function(data, type, full){
                        return moment(full.created_at).format("DD MMMM YYYY hh:mm");
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        if (full.order_status.id == 2 || full.order_status.id == 3 || full.order_status.id == 4 || full.order_status.id == 5 || full.order_status.id == 7 || full.order_status.id == 8 || full.order_status.id == 9 || full.order_status.id == 10) {
                            return "<a href='/teknisi/request-job-accept/"+ full.id + "' class='badge badge-primary btn-sm'  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>";
                        } else if (full.order_status.id == 11) {
                            return "<a href='/teknisi/transacsion_list/complaint/detail/"+ full.id +"' class='badge badge-success btn-sm '  data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>";
                        } else{
                            return "-"
                        }
                    }
                }
            ]
        });


        $("#form-search").submit(function(e) {
            input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/teknisi/request-job/datatables' + playload);
            tableOrder.ajax.url(url).load();
            e.preventDefault();
        })

    })
</script>
@endsection
