@extends('admin.home')
@section('content')
<style>
    /* .modal{
        display: block !important; /* I added this to see the modal, you don't need this */
    } */

    /* Important part */
    .modal-dialog{
        overflow-y: initial !important
    }
    .modal-body{
        height: 350px;
        overflow-y: auto;
    }
</style>

<div class="col-md-12" style="margin-top: 20px;">
    <div class="card">
        <div class="card-header text-white bg-danger">
            <i class="header-icon lnr-gift icon-gradient bg-mixed-hopes">
            </i>
            MEMBER TEAM LIST
        </div>
        <div class="main-card mb-3 card shadow-none">

            <div class="card-body">
                <div class="btn-actions-pane-right">
                    <div class="nav">
                        <a data-toggle="tab" href="#tab-eg2-0" class="btn-tab btn-wide active btn btn-outline-success btn-md" data-desc="my-own-team">My Own Team</a>
                        <a data-toggle="tab" href="#tab-eg2-1" class="btn-tab btn-wide mr-1 ml-1  btn btn-outline-success btn-md" data-desc="my-team">My Team List</a>
                        <a data-toggle="tab" href="#tab-eg2-2" class="btn-tab btn-wide mr-1 ml-1  btn btn-outline-success btn-md" data-desc="my-team">My Invitation <span class="badge badge-danger count_invitation">{{ $count_invitation == 0 ? '': $count_invitation }}</span></a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content col-md-12" style="padding-right:30px;">
                        <div class="tab-pane active" id="tab-eg2-0" role="tabpanel">
                            <div class="table-responsive">
                                <table class="display table table-hover table-bordered" id="table-teams-technicians" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name Team</th>
                                            <th>Description</th>
                                            <th>Member List</th>
                                            <th>Team Invite (Waiting)</th>
                                            <th>Lead Team</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-eg2-1" role="tabpanel">
                            <div class="table-responsive">
                                <table class="display table table-hover table-bordered" id="table-teams-member" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name Team</th>
                                            <th>Description</th>
                                            <th>Member List</th>
                                            <th>Lead Team</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-eg2-2" role="tabpanel">
                            <div class="table-responsive">
                                <table class="display table table-hover table-bordered" id="table-teams-invitation" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Name Team</th>
                                            <th>Description</th>
                                            <th>Lead Team</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary add-teams" type="button">
                    ADD YOUR TEAMS
                </button>
            </div>
        </div>
    </div>
</div>
@include('admin.teknisi.teams._teams_modal_html')
@endsection

@section('script')

@include('admin.teknisi.teams._teams_table_script')

{{-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css"> --}}
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script>
{{-- jquery autocomplate --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tokeninput/1.7.0/jquery.tokeninput.min.js"></script> --}}
{{-- <script src='https://kit.fontawesome.com/a076d05399.js'></script> --}}
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet"/>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js">
</script>
<script>
    //script modal
    $(document).ready(function() {
        // globalCRUD.select2(".member-technician-select", '/teknisi/teams/member/select2');
        // globalCRUD.select2(".member-technician-select", '/teknisi/teams/member/select2', function(item) {
        //     return {

        //         text: item.user.email,
        //         id: item.id
        //     }
        // });
        $('#technicians_email').tokenfield({
            autocomplete :{
                source: function(request, response)
                {
                    jQuery.get(Helper.apiUrl('/teknisi/teams/member/select2'), {
                        q : request.term
                    }, function(data){
                        data = data.data;
                        response(data);
                    });
                },
                delay: 100,
                minLength: 6
            }
            // showAutocompleteOnFocus:true

        });

        $('#technicians_email').on('tokenfield:createtoken', function (event) {
            var existingTokens = $(this).tokenfield('getTokens');

            $.each(existingTokens, function(index, token) {
                if (token.value === event.attrs.value) {
                    event.preventDefault();

                }
            })

        }).on('tokenfield:createtoken', function (e) {
                var data = e.attrs.value.split('|')
                e.attrs.value = data[1] || data[0]
                e.attrs.label = data[1] ? data[0] + ' (' + data[1] + ')' : data[0]
            })

            .on('tokenfield:createdtoken', function (e) {
                // Über-simplistic e-mail validation
                var re = /\S+@\S+\.\S+/
                var valid = re.test(e.attrs.value)
                if (!valid) {
                $(e.relatedTarget).addClass('invalid')
                }
            })

            .on('tokenfield:edittoken', function (e) {
                if (e.attrs.label !== e.attrs.value) {
                var label = e.attrs.label.split(' (')
                e.attrs.value = label[0] + '|' + e.attrs.value
                }
            })

            .on('tokenfield:removedtoken', function (e) {
                Helper.successNotif('Technician email removed !: ' + e.attrs.value);
            })

            .tokenfield();

        // $('#search').click(function(){
        //     $('#name_email').text($('#search_data').val());
        //     $('#technicians_id').text($('#search_data').val());

        // });

    })

    // agar user profile bisa diclik
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })


    var table_teams = TeamsTechnician.table();
    var team_member = TeamMember.table();
    var team_invitation = TeamInvitation.table();
    // add team
    $(document)
        .on('click', '.add-teams', function(){
            // $('#technicians_email').tokenfield('destroy').val('');
            $('#technicians_email').tokenfield('setTokens', []);
            $('#modal-teams').modal('show');
            $('.team-id').val('');
            $('.name').val('');
            $('.description').val('');
        })


    // edit team
    $(document)
        .on('click', '.edit-team', function(){
            var row = table_teams.row($(this).parents('tr')).data();
            // console.log(row);
            $('.team-id').val(row.id);
            $('.name').val(row.name);
            $('.description').val(row.description);
            var team_tech = [];
            var team_technicians =
                _.each(row.team_invite, function(attr) {
                    if(row.technicians_id != attr.technician.id) {
                        team_tech.push(attr.technician.user.email);
                    }
                });

            $('#technicians_email').empty().val(team_tech.toString()).trigger('change');
            $('#modal-teams').modal('show');
        })

    // delete team
    $(document)
        .on('click', '.delete-team', function(){
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.delete('/teknisi/teams/delete/'+id+'')
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('ok');
                        // reload table
                        $('.team-id').val('');
                        table_teams.ajax.reload();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })


    // save teams
    $('#form-teams')
        .submit(function(e){
            data = Helper.serializeForm($(this));
            console.log(data);
            Helper.loadingStart();
            if (data.id == '') {

                Axios.post('/teknisi/teams/create', data)
                    .then(function(response) {
                        //send notif
                        Helper.successNotif(response.data.msg);
                        // reload table
                        table_teams.ajax.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                        if (error.response.status == 500) {
                            window.location.href = Helper.redirectUrl('/teknisi/team/list');
                        }
                    });
            }

            if (data.id != '') {
                Axios.post('/teknisi/teams/update/' + data.id, data)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif(response.data.msg);
                        // // reload table
                        table_teams.ajax.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
            }


            $('#modal-teams').modal('hide');
            e.preventDefault();
        })
</script>

<!-- Member Team -->
<script>
   $(document)
        .on('click', '.leave-team', function(){
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.delete('/teknisi/teams/leave/'+id+'')
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('ok');
                        // reload table
                        $('.team-id').val('');
                        team_member.ajax.reload();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })
</script>

<!-- Invitation Team -->
<script>
    $(document)
         .on('click', '.approve-team', function(){
             var id = $(this).attr('data-id');
             var data = {
                 id
             };
             Helper.confirm(function() {
                 Helper.loadingStart();
                 Axios.post('/teknisi/teams/approve-invitation', data)
                     .then(function(response) {
                         // send notif
                         Helper.successNotif('Approve Invitation, Success!');
                         // reload table
                         $('.team-id').val('');
                         $('.count_invitation').html(response);
                         team_member.ajax.reload();
                         team_invitation.ajax.reload();
                     })
                     .catch(function(error) {
                         console.log(error)
                         Helper.handleErrorResponse(error)
                     });
             })
         })
 </script>
@endsection
