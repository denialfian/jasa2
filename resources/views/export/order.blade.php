<table>
    <thead>
        <tr>
            <th colspan="9" align="center"></th>
        </tr>
        <tr>
            <th><strong>No</strong></th>
            <th><strong>Orders Code</strong></th>
            <th><strong>Schedule</strong></th>
            <th><strong>Customer Name</strong></th>
            <th><strong>Payment Type</strong></th>
            <th><strong>Status</strong></th>
            <th><strong>Service Name</strong></th>
            <th><strong>Symptom Name</strong></th>
            <th><strong>Grand Total</strong></th>
        </tr>
    </thead>
    <tbody>
    @php $no = 1 @endphp
    @foreach($order as $orders)
        <tr>
            <td>{{ $no++ }}</td>
            <td style="width:20px">{{ $orders->code }}</td>
            <td style="width:20px">{{ date('d-M-y H:i', strtotime($orders->schedule)) }}</td>
            <td style="width:20px">{{ $orders->user->name }}</td>
            <td style="width:20px">{{ $orders->payment_type == 0 ? "Offline" : "Online" }}</td>
            <td style="width:20px">{{ $orders->order_status->name }}</td>
            <td style="width:20px">{{ $orders->product_group_name }}</td>
            <td style="width:20px">{{ $orders->service_name }}</td>
            <td style="width:20px"> Rp. {{ number_format($orders->grand_total) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>