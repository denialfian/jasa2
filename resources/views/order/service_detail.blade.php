@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header header-bg">
            Order #{{ $order->code }}
        </div>
    </div>
</div>

<div class="col-md-4" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Order Detail

            <div class="btn-actions-pane-right text-capitalize">
                @if($order->garansi != null)
                <a href="{{ url('admin/garansi/'.$order->garansi->id.'/detail') }}" class="btn btn-success btn-sm"><i class="fa fa-envelope"></i></a>
                @endif

                @if($order->order_status->id == 4)
                <button class="btn btn-success btn-sm done-job"><i class="fa fa-arrow-right"></i> Job Done</button>
                @endif

                @if($order->order_status->id == 7)
                <button data-id="{{ $order->id }}" class="btn btn-success btn-sm complate-job"><i class="fa fa-arrow-right"></i> Complate Job</button>
                @endif

                @if($order->order_status->id == 10)
                @if($order->garansi != null)
                @if($order->garansi->status != 'done')
                <button class="btn btn-success btn-sm garansi-job"><i class="fa fa-arrow-right"></i> Accept Claims</button>
                @endif
                @endif
                @endif

                @if($order->order_status->id == 9)
                <button type="button" data-id="{{ $order->id }}" class="btn btn-success btn-sm on_working-job"><i class="fa fa-arrow-right"></i> On Working</button>
                @endif
            </div>
        </div>
        <div class="card-body">
            @if($order->order_status->id == 2)
            <label for="">By Technician</label>
            <br>
            <button class="btn btn-danger btn-sm reject-job" data-id="{{ $order->id }}"><i class="fa fa-close"></i> Reject Job</button>
            <button class="btn btn-success btn-sm accept-job"><i class="fa fa-check"></i> Accept Job</button>
            <hr>
            @endif

            @if($order->order_status->id == 3)
            <label for="">By Customer</label>
            <br>
            <button class="btn btn-danger btn-sm reject-job-custome" data-id="{{ $order->id }}"><i class="fa fa-close"></i> Reject Job</button>
            <button class="btn btn-success btn-sm accept-job-customer" data-id="{{ $order->id }}"><i class="fa fa-check"></i> Accept Job</button>
            <hr>
            @endif


            <label>Order No:</label>
            <p>{{ $order->code }}</p>

            <label>Schedule:</label>
            <p id="schedule_display">
                <span class="mb-2 mr-2 badge badge-pill badge-primary">
                    {{ $order->schedule->format('d F Y G:i') }}
                </span>
            </p>

            @if($order->date_complete != null)
            <label>Due Date Claim: </label>
            @if($order->date_complete->addDays($garansi_setting->value)->gt(now()))
            <p>{{ $order->date_complete->addDays($garansi_setting->value)->format('d F Y') }} / {{ $order->date_complete->diff(date('Y-m-d', strtotime($order->date_complete. ' + '.$garansi_setting->value.' days')))->format('%d days') }} Left</p>
            @else
            <p>Expired</p>
            @endif
            @endif

            <label>Order Status:</label>
            <p>
                <span class="mb-2 mr-2 badge badge-pill badge-dark">{{ $order->order_status->name }}</span>
                @if($order->garansi != null)
                <span class="mb-2 mr-2 badge badge-pill badge-primary">Warranty Claim</span>
                @endif
            </p>


            <label>Order Note:</label>
            <p>{{ $order->note }}</p>

            <label>Symptom detail:</label>
            <p>{{ $order->symptom_detail }}</p>

            @if($order->order_status->id == 7)
            <label for="ms_symptom_code">Symptom Code</label>
            <p>{{ $order->symptom_code == null ? '' : $order->symptom_code->name }}</p>
            <label for="ms_symptom_code">Repair Code</label>
            <p>{{ $order->repair_code == null ? '' : $order->repair_code->name }}</p>
            <label for="repair_desc">Repair Description</label>
            <p>{{ $order->repair_desc }}</p>
            @endif

            <label>Payment Type:</label>
            <br />
            {!! $order->payment_type == 0 ? '<div class="badge badge-danger">Offline</div>' : '<div class="badge badge-info">Online</div>' !!}

            <div class="text-right" style="margin-top: 10px;">
                @if($order->orders_statuses_id == 9)
                <label>Delivery Note : &nbsp;&nbsp;</label>
                <a href="{{ url('/admin/order/workmanship_report_pdf/'. $order_id ) }}" class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-sm btn-outline-warning"><i class="fa fa-file-pdf"></i></a>
                <small>.pdf</small>
                @endif
                <br />
                @if ($order->orders_statuses_id == 1 || $order->orders_statuses_id == 2 || $order->orders_statuses_id == 5 || $order->orders_statuses_id == 6)
                <label>Invoice: &nbsp; - </label>
                @else
                <label>Invoice: &nbsp;&nbsp;</label>
                <a href="{{ url('/admin/invoice/'. $order_id ) }}" class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-sm btn-outline-danger" target="_blank"><i class="fa fa-file-pdf"></i></a>
                <small>.pdf</small>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="col-md-4" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Billing Detail
            @if(!in_array($order->orders_statuses_id, [4, 7, 10]))
            <div class="btn-actions-pane-right text-capitalize">
                <button type="button" id="edit_billing" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit Billing</button>
            </div>
            @endif
        </div>
        <div class="card-body">
            <label>Email:</label>
            <p>{{ $order->customer_email }}</p>

            <label>Phone:</label>
            <p>{{ $order->customer_phone }}</p>

            <label>Address:</label>
            <p>{{ $order->address_type_name }}</p>
            <p>{{ $order->address }}</p>
            <p><a href="" data-toggle="modal" data-target="#modal_maps" data-lat='{{ $order->user->address->latitude }}' data-lng='{{ $order->user->address->longitude }}' style="text-decoration: underline"><i class="fa fa-map-marker"></i> View Location</a></p>


            <label>Bill Address:</label>
            <p>{{ $order->bill_to }}</p>
        </div>
    </div>
</div>

<!-- Billing Amount -->
<div class="col-md-4" style="margin-top: 10px;">
    @include('admin.order._billing_ammon', compact('order', 'sparepart_details', 'item_details'))

    <div class="card header-border" style="margin-top: 10px;">
        <div class="card-body">
            <input type="hidden" value="{{ $order->id }}" name="status_order_id" id="status_order_id">
            <div class="form-group">
                <label>STATUS</label>
                <select class="form-control" id="orders_status" name="orders_status" style="width:100%">
                    @if ($order->orders_statuses_id == 2)
                    <option value="2">Analyzing</option>
                    <option value="3">Waiting Approval</option>
                    <option value="4">On Working</option>
                    <option value="9">Processing</option>
                    <option value="7">Job Done</option>
                    <option value="10">Job Completed</option>
                    <option value="11">Complaint</option>
                    <option value="5">Cancel</option>
                    @elseif($order->orders_statuses_id == 3)
                    <option value="3">Waiting Approval</option>
                    <option value="2">Analyzing</option>
                    <option value="4">On Working</option>
                    <option value="9">Processing</option>
                    <option value="7">Job Done</option>
                    <option value="10">Job Completed</option>
                    <option value="11">Complaint</option>
                    <option value="5">Cancel</option>
                    @elseif($order->orders_statuses_id == 4)
                    <option value="4">On Working</option>
                    <option value="3">Waiting Approval</option>
                    <option value="2">Analyzing</option>
                    <option value="9">Processing</option>
                    <option value="7">Job Done</option>
                    <option value="10">Job Completed</option>
                    <option value="11">Complaint</option>
                    <option value="5">Cancel</option>
                    @elseif($order->orders_statuses_id == 5)
                    <option value="5">Cancel</option>
                    <option value="4">On Working</option>
                    <option value="3">Waiting Approval</option>
                    <option value="2">Analyzing</option>
                    <option value="9">Processing</option>
                    <option value="7">Job Done</option>
                    <option value="10">Job Completed</option>
                    <option value="11">Complaint</option>
                    @elseif($order->orders_statuses_id == 7)
                    <option value="7">Job Done</option>
                    <option value="5">Cancel</option>
                    <option value="4">On Working</option>
                    <option value="3">Waiting Approval</option>
                    <option value="2">Analyzing</option>
                    <option value="9">Processing</option>
                    <option value="10">Job Completed</option>
                    <option value="11">Complaint</option>
                    @elseif($order->orders_statuses_id == 9)
                    <option value="9">Processing</option>
                    <option value="7">Job Done</option>
                    <option value="5">Cancel</option>
                    <option value="4">On Working</option>
                    <option value="3">Waiting Approval</option>
                    <option value="2">Analyzing</option>
                    <option value="10">Job Completed</option>
                    <option value="11">Complaint</option>
                    @elseif($order->orders_statuses_id == 10)
                    <option value="10">Job Completed</option>
                    <option value="9">Processing</option>
                    <option value="7">Job Done</option>
                    <option value="5">Cancel</option>
                    <option value="4">On Working</option>
                    <option value="3">Waiting Approval</option>
                    <option value="2">Analyzing</option>
                    <option value="11">Complaint</option>
                    @elseif($order->orders_statuses_id == 11)
                    <option value="11">Complaint</option>
                    <option value="9">Processing</option>
                    <option value="7">Job Done</option>
                    <option value="5">Cancel</option>
                    <option value="4">On Working</option>
                    <option value="3">Waiting Approval</option>
                    <option value="2">Analyzing</option>
                    <option value="10">Job Completed</option>
                    @endif
                </select>
            </div>
            <p>Resend Email</p>
            <div class="form-group">
                <div class="position-relative form-check form-check-inline">
                    <label class="form-check-label">
                        <small>notify email to customer.</small>
                    </label>
                </div>
            </div>

            <button type="button" id="update_status" class="btn btn-sm btn-primary"><i class="fa fa-check-circle"></i> UPDATE STATUS</button>
        </div>
    </div>
</div>

<!-- sparepart -->
<div class="col-md-8" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Detail sparepart
        </div>
    </div>

    @if(!in_array($order->orders_statuses_id, [3, 5, 7, 9, 10]))
    @include('admin.order._sparepart')
    @else
    @include('admin.order._sparepart_view')
    @endif

    <!-- media -->
    @include('admin.order._media')
</div>

<?php $technician = $order->service_detail->first()->technician; ?>
<?php $teknisi = $order->service_detail->first()->teknisi; ?>
<div class="col-md-4" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Technician
            <div class="btn-actions-pane-right text-capitalize">
                @if($technician != null)
                @if(!empty($order->service_detail[0]->teknisi->user))
                <a target="__blank" href="{{ url('admin/user/detail/' .  $order->service_detail[0]->technician->user->id) }}" class="btn btn-sm btn-danger">
                    <i class="fa fa-eye"></i>
                </a>
                @endif
                @endif
                @if(!in_array($order->orders_statuses_id, [4, 7, 10]))
                <button type="button" class="btn btn-sm btn-success assign-mechanic"><i class="fa fa-plus"></i> Assign New Mechanic</button>
                @endif
            </div>
        </div>
    </div>
    <div class="our-team">
        <div class="picture">
            <img class="img-fluid" src="{{ $technician->user->avatar }}">
        </div>
        <div class="team-content">
            <h3 class="name">{{ $technician->user->name }}</h3>
            <p class="name">{{ $technician->user->email }}</p>
            <hr />
            <span class="point">{{ $technician == null ? '' : '(' . $technician->getFormatRatingValue() . ')' }}</span>
            {!! $technician == null ? '' : $technician->getRatingStarUi() !!}
            @if($technician != null)
            @if($technician->total_review > 0)
            <br />
            <span class="amount">
                ({{ $technician->total_review }}) Reviews
            </span>
            @endif
            @else
            <button class="btn btn-sm btn-danger">
                <i class="fa fa-close"></i> Deleted <i class="fa fa-close"></i>
            </button>
            @endif
            <br />
        </div>
        <ul class="social">
            <li><a href="{{ url('/admin/chats/'.$technician->user->id.'/technician/') }}" target="__blank" class="fa fa-envelope" aria-hidden="true"></a></li>
        </ul>
    </div>
</div>

<!-- history -->
<div class="col-md-12" style="margin-top: 10px;">
    @include('admin.order._history', ['histories' => $order->history_order->reverse()])
</div>

<!-- history -->
<div class="col-md-12" style="margin-top: 10px;">
    @include('admin.order._order_log', ['order_log' => $order_log->reverse()])
</div>


@if(!in_array($order->orders_statuses_id, [4, 7, 10,]))
<form id="form_bill_detail">
    <div class="modal fade" data-backdrop="false" id="modal-edit-billing" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Edit Address
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="bill_order_id" id="bill_order_id" value="{{ $order->id }}">
                    <div class="col-md-12">
                        <div class="position-relative form-group">
                            <label class="" for="">
                                Address
                            </label>
                            <textarea class="form-control" name="bill_address" id="bill_address" rows="5" required>{{ $order->address }}</textarea>
                        </div>

                        <div class="position-relative form-group">
                            <label class="" for="">
                                Bill To
                            </label>
                            <textarea class="form-control" name="bill_to" id="bill_to" rows="5" required>{{ $order->bill_to }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SAVE
                    </button>
                    <button class="btn waves-effect" data-dismiss="modal" type="button">
                        CLOSE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="form-edit-teknisi">
    <div class="modal fade" data-backdrop="false" id="modal-edit-teknisi" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        New Tecnician
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="teknisi_ms_price_services_id" id="teknisi_ms_price_services_id">
                    <input type="hidden" name="teknisi_service_detail_id" id="teknisi_service_detail_id" value="{{ $order->service_detail->first()->id }}">
                    <input type="hidden" value="{{ $order->service_detail->first()->ms_services_types_id }}" name="teknisi_service_type_id" id="teknisi_service_type_id">
                    <label>SELECT TECHNICIAN</label>
                    <div class="form-group">
                        <select id="select-technicians" class="form-control" name="technician_id" style="width:100%" required>
                            <option value="" selected="selected">--Select--</option>
                        </select>
                    </div>

                    <div id="available_schedule">
                        <div class="position-relative form-group">
                            <label class="label-header" for="">
                                AVAILABLE SCHEDULE
                            </label>
                            <input class="form-control datetimepicker" name="schedule" type="text" id="schedule" readonly />
                        </div>
                        <div class="row" id="display_hours_available" style="padding:20px;"></div>
                    </div>
                    <div style="padding:10px">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Start Working at </label>
                                <div class="form-group">
                                    <h4 id="start_at"></h4>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label>End Hours at </label>
                                <div class="form-group">
                                    <h4 id="end_at"></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <label>What is the estimated work time?</label>
                        <div class="row">
                            <div class="input-group col-md-6">
                                <select class="form-control" id="estimation_hours" required style="width:100%">
                                    <option selected="" value=0>Select Duration</option>
                                    <option value=1>1</option>
                                    <option value=2>2</option>
                                    <option value=3>3</option>
                                    <option value=4>4</option>
                                    <option value=5>5</option>
                                </select>
                            </div>
                            <div class="input-group-append col-md-6">
                                <span class="input-group-text">Hours</span>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SAVE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@endif

<form id="form-accept-job">
    <div class="modal fade" data-backdrop="false" id="modal-accept-job" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Accept Job
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="accept_job_order_id" id="accept_job_order_id" value="{{ $order->id }}">
                    <div class="bill-amount-area-copy"></div>
                    <hr />
                    <label>Start Working at </label>
                    <div class="form-group">
                        <h4 id="start_at" hidden>{{ $order->schedule->format('Y m d H:i') }}</h4>
                        <h6>{{ $order->schedule->format('d F Y G:i') }}</h6>
                    </div>
                    <label>What is the estimated work time?</label>
                    <div class="row">
                        <div class="input-group col-md-5">
                            <select class="form-control" name="estimation_hours" required style="width:100%">
                                <option selected="" value=0>Select Duration</option>
                                <option value=1>1</option>
                                <option value=2>2</option>
                                <option value=3>3</option>
                                <option value=4>4</option>
                                <option value=5>5</option>
                            </select>
                        </div>
                        <br />
                        <div class="input-group-append col-md-2">
                            <span class="input-group-text">Hours</span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SAVE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="form-done-job">
    <div class="modal fade" data-backdrop="false" id="modal-done-job" role="dialog" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Done Job
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="done_job_order_id" id="done_job_order_id" value="{{ $order->id }}">
                    <label for="">Symptom Code</label>
                    <div class="form-group">
                        <select class="form-control" id="ms_symptom_code" name="ms_symptom_code_id" style="width:100%" required>
                            @if($order->symptom_code != null)
                            <option value="{{ $order->symptom_code->id }}" selected="selected">{{ $order->symptom_code->name }}</option>
                            @else
                            <option value="" selected="selected">--Select--</option>
                            @endif
                        </select>
                    </div>
                    <label for="">Repair Code</label>
                    <div class="form-group">
                        <select class="form-control" id="ms_repair_code" name="ms_repair_code_id" style="width:100%" required>
                            @if($order->repair_code != null)
                            <option value="{{ $order->repair_code->id }}" selected="selected">{{ $order->repair_code->name }}</option>
                            @else
                            <option value="" selected="selected">--Select--</option>
                            @endif
                        </select>
                    </div>
                    <label for="">Desc</label>
                    <div class="form-group">
                        <textarea class="form-control" id="repair_desc" name="repair_desc" rows="3" required>{{ $order->repair_desc }}</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SAVE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

<!-- garansi -->
@if(in_array($order->orders_statuses_id, [10]))
<form id="form-garansi-job">
    <div class="modal fade" data-backdrop="false" id="modal-garansi-job" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Accept Garansi (Revisit)
                    </h4>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="garansi_id" id="garansi_id" value="{{ $order->garansi == null ? 0 : $order->garansi->id }}">
                    <input type="hidden" name="garansi_ms_price_services_id" id="garansi_ms_price_services_id">
                    <input type="hidden" name="garansi_service_detail_id" id="garansi_service_detail_id" value="{{ $order->service_detail->first()->id }}">
                    <input type="hidden" value="{{ $order->service_detail->first()->ms_services_types_id }}" name="teknisi_service_type_id" id="teknisi_service_type_id">
                    <label>SELECT TECHNICIAN</label>
                    <div class="form-group">
                        <select id="select-technicians" class="form-control" name="technician_id" style="width:100%" required>
                            <option value="" selected="selected">--Select--</option>
                        </select>
                    </div>

                    <div id="available_schedule">
                        <div class="position-relative form-group">
                            <label class="label-header" for="">
                                AVAILABLE SCHEDULE
                            </label>
                            <input class="form-control datetimepicker" name="schedule" type="text" id="schedule" readonly />
                        </div>
                        <div class="row" id="display_hours_available" style="padding:20px;"></div>
                    </div>
                    <div style="padding:10px">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Start Working at </label>
                                <div class="form-group">
                                    <h4 id="start_at"></h4>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label>End Hours at </label>
                                <div class="form-group">
                                    <h4 id="end_at"></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <label>What is the estimated work time?</label>
                        <div class="row">
                            <div class="input-group col-md-6">
                                <select class="form-control" id="estimation_hours" name="estimation_hours" required style="width:100%">
                                    <option selected="" value=0>Select Duration</option>
                                    <option value=1>1</option>
                                    <option value=2>2</option>
                                    <option value=3>3</option>
                                    <option value=4>4</option>
                                    <option value=5>5</option>
                                </select>
                            </div>
                            <div class="input-group-append col-md-6">
                                <span class="input-group-text">Hours</span>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary waves-effect" type="submit">
                        SAVE
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@endif

@endsection

@section('script')
@include('admin.template._locationServiceDetail')
@include('admin.order.service_detail_style')
<link rel="stylesheet" href="{{ asset('adminAssets/css/jquery.filer.css') }}">
<link rel="stylesheet" href="{{ asset('adminAssets/css/themes/jquery.filer-dragdropbox-theme.css') }}">
<script type="text/javascript" src="{{ asset('adminAssets/js/filer/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminAssets/js/filer/jquery.filer.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js">
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" rel="stylesheet" />
<!-- status -->
<script>
    $('#update_status').click(function() {
        Helper.loadingStart();
        data = {};
        data.order_id = $('#status_order_id').val();
        data.order_status = $('#orders_status').val();
        data.is_resend_email = 0;
        if ($('#is_resend_email').is(':checked') == true) {
            data.is_resend_email = 1;
        }
        $.ajax({
            url: Helper.apiUrl('/admin/order/update_status'),
            type: 'post',
            data: data,
            success: function(response) {
                // console.log(response);
                Helper.successNotif('Status Order Has Been Updated !');
                window.location.href = Helper.redirectUrl('/admin/transactions/show');
                Helper.loadingStop()
            }
        });
    });

    var TeknisiSelect = {
        initialize: function() {
            Helper.dateScheduleJob('#schedule');

            $('#select-technicians').select2({
                // minimumInputLength: 1,
                ajax: {
                    url: Helper.apiUrl('/admin/order/find_technicians'),
                    dataType: 'json',
                    delay: 250,
                    type: 'post',
                    data: function(params) {
                        return {
                            service_type_id: [$('#teknisi_service_type_id').val()],
                            // customer_id: $('teknisi-customer').val(),
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: $.map(data, TeknisiSelect.render(data))
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateSelection: function(data) {
                    return data.name;
                }
            });

            // change teknisi
            $(document).on('change', '#select-technicians', function() {
                value = $(this).val();
                teknisi = TeknisiSelect.find(value);
                console.log(teknisi)
                $('#teknisi_ms_price_services_id').val(teknisi.data.price_services[0].id);
            });
        },

        data: function() {
            return $('#select-technicians').select2("data");
        },

        find: function(teknisi_id) {
            return _.find(TeknisiSelect.data(), function(data) {
                return data.id == teknisi_id;
            });
        },

        render: function(data) {
            return function(data) {
                var phone = data.user.phone != null ? data.user.phone : '-';
                var images = "<img src='" + data.user.avatar + "' />";
                var price_service = '';
                service_type_id = $('#select-service-type').val();

                price = _.find(data.price_services, function(row) {
                    return row.ms_services_types_id == service_type_id;
                });

                if (price) {
                    price_service += "<div class='select2-result-repository__description'><b>" + price.service_type.name + "</b> (" + Helper.toCurrency(price.value) + ")</div>";
                }

                return {
                    id: data.id,
                    text: "<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__avatar'>" + images + "</div>" +
                        "<div class='select2-result-repository__meta'>" +
                        "<div class='select2-result-repository__title'><b>" + data.user.name + "</b></div>" +
                        "<div class='select2-result-repository__description'>Phone : <b>" + phone + "</b></div>" +
                        "<div class='select2-result-repository__description'>Email : <b>" + data.user.email + "</b></div>" +
                        price_service +
                        "</div></div>",
                    name: data.user.name + ' / ' + data.user.email,
                    data: data
                };
            };
        },
    }

    var jam_teknisi = [];
    var teknisi_id = null;
    Helper.dateScheduleJob('#schedule');

    $(document).on('change', "input[name='hours']", function() {
        if (this.checked) {
            $('#estimation_hours').prop('disabled', false);
            $('#start_at').html(moment($('#schedule').val() + ' ' + $("input[name='hours']:checked").val()).format('DD MMMM YYYY HH:mm'));
        }
    })

    $(document).on('change', '#estimation_hours', function() {
        if ($(this).val()) {
            var returned_endate = moment($('#start_at').html()).add($(this).val(), 'hours');
            console.log(returned_endate.format('H'));
            console.log(jam_teknisi)
            $('#end_at').html(returned_endate.format('HH:00'));
            if ($.inArray(parseInt(returned_endate.format('H')), jam_teknisi) !== -1) {
                return Helper.warningNotif("Cannot pick estimate hours. Cause at " + $('#end_at').html() + " in the technician schedule !");
            }
        }
    });
</script>

<!-- billing -->
<script>
    $(document).on('click', '#edit_billing', function() {
        $('#modal-edit-billing').modal('show');
    })

    $('#form_bill_detail').submit(function(e) {
        var data = Helper.serializeForm($(this));
        Axios.put('/order/bill_detail/' + data.bill_order_id, data)
            .then(function(response) {
                Helper.successNotif(response.data.msg);
                location.reload();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
        e.preventDefault();
    });
</script>

<!-- assign teknisi -->
@if(!in_array($order->orders_statuses_id, [4, 7, 10]))
<script>
    TeknisiSelect.initialize();

    $('.assign-mechanic').click(function() {
        $('#modal-edit-teknisi').modal('show')
    })

    $('#form-edit-teknisi')
        .submit(function(e) {
            Helper.loadingStart();
            var data = Helper.serializeForm($(this));
            Axios.put('/order/assign_new_teknisi/' + data.teknisi_service_detail_id, data)
                .then(function(response) {
                    Helper.successNotif('Success Updated');
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
            e.preventDefault();
        });
</script>
@endif

<!-- sparepart -->
<script>
    SparepartClass = {
        create: function(xx) {
            $('.dynamic_sparepart').append((`
                <input type="hidden" value="0" name="sparepart_detail_id[]">
                <div class="position-relative row form-group" id="my_part_row_${xx}">
                    <div class="col-sm-6">
                        <input type="text" placeholder="name" name="teknisi_sparepart_name[]" data-uniq="${xx}" class="form-control teknisi_sparepart_name_input-${xx}" />
                    </div>
                    <div class="col-sm-2">
                        <input type="text" placeholder="price" name="teknisi_sparepart_price[]" data-uniq="${xx}" class="teknisi_sparepart_price_i teknisi_sparepart_price_input-${xx} form-control" />
                    </div>
                    <div class="col-sm-2">
                        <input type="text" placeholder="unit" name="teknisi_sparepart_qty[]" data-uniq="${xx}" class="teknisi_sparepart_qty_i teknisi_sparepart_qty_input-${xx} form-control" />
                    </div>
                    <div class="col-sm-2" style="padding-left:0px;">
                        <button type="button" data-sparepart_detail_id="0" data-uniq="${xx}" class="btn-transition btn btn-danger btn_remove_my_part"><i class="fa fa-close"></i></button>
                    </div>
                </div>
            `));

            Helper.onlyNumberInput('.teknisi_sparepart_qty_i')
                .onlyNumberInput('.teknisi_sparepart_price_i');
        },
        delete: function($el) {
            var uniq = $el.attr("data-uniq");
            var sparepart_detail_id = parseInt($el.attr("data-sparepart_detail_id"));

            if (sparepart_detail_id != 0) {
                Helper.confirm(function() {
                    Axios.delete('/order/destroy_sparepart_detail/' + sparepart_detail_id)
                        .then(function(response) {
                            Helper.successNotif('Success Delete');
                            $('#my_part_row_' + uniq + '').remove();
                            $('#grand_total_order').text(response.data.grand_total);
                            if ($('.dynamic_sparepart_content').length == 0) {
                                SparepartClass.create(response.data.uniq);
                            }
                            location.reload();
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });
                })
            } else {
                $('#my_part_row_' + uniq + '').remove();
            }
        },
        update: function() {

        }
    };

    // event link tab klik
    $('.btn-tab').click(function() {
        $('#extra-cost-area').html('');
        tab = $(this).attr('data-desc');
        if (tab == 'sparepaer-inventory') {
            $('#type_part').val('my-inventory');
        } else {
            $('#type_part').val('my-company');
        }
    });

    $(function() {
        var xx = 1;

        Helper.onlyNumberInput('.teknisi_sparepart_qty_i')
            .onlyNumberInput('.teknisi_sparepart_price_i');

        $('#sparepart-detail-form')
            .submit(function(e) {
                var data = Helper.serializeForm($(this));
                Axios.put('/order/update_sparepart_detail/' + data.sparepart_detail_order_id, data)
                    .then(function(response) {
                        Helper.successNotif('Success Updated');
                        location.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
                e.preventDefault();
            });

        // add row part my inv
        $(document)
            .on('focus', 'input[name="teknisi_sparepart_name[]"], input[name="teknisi_sparepart_qty[]"]', function(e) {
                if ($(this).is('input[name="teknisi_sparepart_name[]"]:last')) {
                    SparepartClass.create(new Date().getTime() + xx)
                    xx++;
                }

                if ($(this).is('input[name="teknisi_sparepart_qty[]"]:last')) {
                    SparepartClass.create(new Date().getTime() + xx)
                    xx++;
                }
            })

        // delete part my inv
        $(document)
            .on('click', '.btn_remove_my_part', function() {
                SparepartClass.delete($(this));
            });
    })
</script>

<!-- part company -->
<script>
    var inventoryCompanySelect = {
        initialize: function() {
            Helper.onlyNumberInput('.inventory-unit');
            $('select[name="inventory_id[]"]').prop('disabled', true);
            $('.inventory-unit').prop('disabled', false);
            globalCRUD.select2('.select-warehouse', '/warehouses/select2');

            $(document).on('change', '.select-inventory', function() {
                inventoryCompanySelect.handleOnChange($(this));
            });

            $(document).on('change', '.select-warehouse', function() {
                uniq = $(this).attr('data-uniq')
                inventoryCompanySelect.make('.select-inventory-' + uniq, $(this).val());
                $('.select-inventory-' + uniq).prop('disabled', false);
            });
        },

        make: function(selector, warehouse_id) {
            inventory_id = [];
            $('.select-inventory').each(function() {
                if ($(this).val() != '') {
                    inventory_id.push($(this).val());
                }
            }).get();

            $(selector).select2({
                ajax: {
                    url: Helper.apiUrl('/teknisi/inventory-company/select2'),
                    dataType: 'json',
                    delay: 250,
                    type: 'post',
                    data: function(params) {
                        return {
                            inventory_id: inventory_id,
                            warehouse_id: warehouse_id,
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: $.map(data.data, inventoryCompanySelect.render(data))
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateSelection: function(data) {
                    return data.name || data.text;
                }
            });
        },

        data: function(selector = null) {
            selector = selector == null ? '.select-inventory' : selector;
            return $(selector).select2("data");
        },

        find: function(inv_id, selector = null) {
            return _.find(inventoryCompanySelect.data(selector), function(data) {
                return data.id == inv_id;
            });
        },

        render: function(data) {
            return function(data) {
                console.log(data);
                if (data.product) {
                    var images = "<img src='" + data.product.main_image + "' />";
                    return {
                        id: data.id,
                        text: "<div class='select2-result-repository clearfix'>" +
                            "<div class='select2-result-repository__avatar'>" + images + "</div>" +
                            "<div class='select2-result-repository__meta'>" +
                            "<div class='select2-result-repository__title'>" + data.item_name + "</div>" +
                            "<div class='select2-result-repository__description'>" + data.warehouse.name + "</div>" +
                            "<div class='select2-result-repository__description'>" + Helper.thousandsSeparators(data.stock_available) + ' ' + data.unit_type.unit_name + "</div>" +
                            "<div class='select2-result-repository__statistics'>" +
                            "<div class='select2-result-repository__forks'><i class='fa fa-fa-money'></i><b> Rp. " + Helper.thousandsSeparators(data.cogs_value) + "</b></div>" +
                            "</div></div>",
                        name: data.item_name + "(Qty: " + Helper.thousandsSeparators(data.stock_available) + ")",
                        data: data
                    };
                }

                return {
                    id: '',
                    text: "",
                    name: '',
                    data: []
                };
            };
        },

        handleOnChange: function($el) {
            uniq = $el.attr('data-uniq');
            inventory = this.find($el.val(), $('.select-inventory-' + uniq));
            $('.inventory-price-' + uniq).val(Helper.thousandsSeparators(inventory.data.cogs_value));
            $('.inventory-unit-' + uniq).prop('disabled', false);
        },

        templateNewInputPartMyCompany: function(xx) {
            $('.dynamic_sparepart_company').append((`
                <div class="position-relative row form-group" id="my_company_row_${xx}">
                    <input type="hidden" value="0" name="item_detail_id[]">
                    <div class="col-sm-3">
                        <select class="form-control select-warehouse select-warehouse-${xx}" name="warehouse_id[]" data-uniq="${xx}" style="width:100%">
                            <option value="" selected="selected">--Pilih--</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <select class="form-control select-inventory select-inventory-${xx}" id="inventory_id_${xx}" data-uniq="${xx}" name="inventory_id[]" style="width:100%">
                            <option value="" selected="selected">--Pilih--</option>'+
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" data-uniq="${xx}" class="form-control inventory-price-${xx}" name="inventory_price[]" disabled/>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" id="qty_inventory_${xx}" data-uniq="${xx}" placeholder="unit" name="inventory_qty[]" value="0" class="form-control inventory-unit inventory-unit-${xx}">
                    </div>
                    <div class="col-sm-1" style="padding-left:0px;">
                        <button data-item_detail_id="0" type="button" data-uniq="${xx}" class="btn-transition btn btn-sm btn-danger btn_remove_my_company"><i class="fa fa-close"></i></button>
                    </div>
                </div>
            `));

            Helper.onlyNumberInput('.inventory-unit');
        },

        delete: function($el) {
            var uniq = $el.attr("data-uniq");
            var item_detail = parseInt($el.attr("data-item_detail_id"));

            if (item_detail != 0) {
                Helper.confirm(function() {
                    Axios.delete('/order/destroy_item_detail/' + item_detail)
                        .then(function(response) {
                            Helper.successNotif('Success Delete');
                            location.reload();
                        })
                        .catch(function(error) {
                            Helper.handleErrorResponse(error)
                        });
                })
            } else {
                $('#my_company_row_' + uniq + '').remove();
            }
        },
    }

    inventoryCompanySelect.initialize();

    $(function() {
        var yy = 1;

        // add row part my inv
        $(document)
            .on('focus', 'input[name="inventory_qty[]"]', function(e) {
                console.log(($(this).is('input[name="inventory_qty[]"]:last')));
                if ($(this).is('input[name="inventory_qty[]"]:last')) {
                    uniq = new Date().getTime() + yy;
                    inventoryCompanySelect.templateNewInputPartMyCompany(uniq)
                    globalCRUD.select2('.select-warehouse-' + uniq, '/warehouses/select2');
                    $('.select-inventory-' + uniq).prop('disabled', true);
                    $('.inventory-unit-' + uniq).prop('disabled', true);
                    yy++;
                }
            })

        // delete part my inv
        $(document)
            .on('click', '.btn_remove_my_company', function() {
                inventoryCompanySelect.delete($(this))
            });

        // event input unit di ganti
        $(document)
            .on('keyup', 'input[name="inventory_qty[]"]', function() {
                stock_available = $(this).attr('data-available');
                if (stock_available) {
                    stock_available = parseInt(stock_available);
                } else {
                    uniq = $(this).attr("data-uniq");
                    inventory = inventoryCompanySelect.find($('.select-inventory-' + uniq).val(), $('.select-inventory-' + uniq));
                    stock_available = parseInt(inventory.data.stock_available);
                }

                qty = parseInt($(this).val());
                if (qty > stock_available) {
                    Helper.errorNotif('Melebihi Qty');
                    $(this).val(0);
                }
            });
    })
</script>


<!-- action -->

<!-- acceprt job -->
@if($order->order_status->id == 2)
<script>
    $('.bill-amount-area-copy').html($('.bill-amount-area').html());

    $('.accept-job').click(function() {
        $('#modal-accept-job').modal('show')
    })

    $('#form-accept-job')
        .submit(function(e) {
            var data = Helper.serializeForm($(this));
            Helper.loadingStart();
            Axios.put('/order/accept_job/' + data.accept_job_order_id, data)
                .then(function(response) {
                    Helper.successNotif('Success Updated');
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
            e.preventDefault();
        });

    $('.reject-job').click(function() {
        id = $(this).attr('data-id');
        Helper.confirm(function() {
            Helper.loadingStart();
            Axios.put('/order/reject_job/' + id)
                .then(function(response) {
                    Helper.successNotif('Success Updated');
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        })
    })
</script>
@endif

@if($order->order_status->id == 3)
<script>
    $('.accept-job-customer').click(function() {
        id = $(this).attr('data-id');
        Helper.confirm(function() {
            Helper.loadingStart();
            Axios.put('/order/accept_job_customer/' + id)
                .then(function(response) {
                    Helper.successNotif('Success Updated');
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        })
    })

    $('.reject-job-customer').click(function() {
        id = $(this).attr('data-id');
        Helper.confirm(function() {
            Helper.loadingStart();
            Axios.put('/order/reject_job_customer/' + id)
                .then(function(response) {
                    Helper.successNotif('Success Updated');
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        })
    })
</script>
@endif

<!-- done job -->
@if($order->order_status->id == 4)
<script>
    globalCRUD
        .select2("#ms_symptom_code", '/symptom-code/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })
        .select2("#ms_repair_code", '/repair-code/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })
    $('.done-job').click(function() {
        $('#modal-done-job').modal('show')
    })

    $('#form-done-job')
        .submit(function(e) {
            Helper.loadingStart();
            var data = Helper.serializeForm($(this));
            Axios.put('/order/done_job/' + data.done_job_order_id, data)
                .then(function(response) {
                    Helper.successNotif('Success Updated');
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
            e.preventDefault();
        });
</script>
@endif

<!-- complate job -->
@if($order->order_status->id == 7)
<script>
    $(document).on('click', '.complate-job', function(e) {
        id = $(this).attr('data-id');
        Helper.confirm(function() {
            Helper.loadingStart();
            Axios.put('/order/complate_job/' + id)
                .then(function(response) {
                    Helper.successNotif('Success Updated');
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        })
        e.preventDefault()
    })
</script>
@endif

<!-- on working job -->
@if($order->order_status->id == 9)
<script>
    $(document).on('click', '.on_working-job', function(e) {
        id = $(this).attr('data-id');
        Helper.confirm(function() {
            Helper.loadingStart();
            Axios.put('/order/on_working/' + id)
                .then(function(response) {
                    Helper.successNotif('Success Updated');
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        })
        e.preventDefault()
    })
</script>
@endif

@if($order->order_status->id == 10)
<script>
    TeknisiSelect.initialize();

    $('.garansi-job').click(function() {
        $('#modal-garansi-job').modal('show')
    })

    $('#form-garansi-job')
        .submit(function(e) {
            Helper.loadingStart();
            var data = Helper.serializeForm($(this));
            Axios.put('/order/garansi_revisit/' + data.garansi_service_detail_id, data)
                .then(function(response) {
                    Helper.successNotif('Success Updated');
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
            e.preventDefault();
        });
</script>

<script>
    $(document).on('click', '.complate-job', function(e) {
        id = $(this).attr('data-id');
        Helper.confirm(function() {
            Helper.loadingStart();
            Axios.put('/order/complate_job/' + id)
                .then(function(response) {
                    Helper.successNotif('Success Updated');
                    location.reload();
                })
                .catch(function(error) {
                    Helper.handleErrorResponse(error)
                });
        })
        e.preventDefault()
    })
</script>
@endif
<!-- end action -->


<script>
    $('#media-form').submit(function(e) {
        Helper.loadingStart();
        var data = new FormData($('#media-form')[0]);
        Axios.post('/order/upload_media/' + $('#media_order_id').val(), data, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function(response) {
                Helper.successNotif('Success Updated');
                location.reload();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>
@endsection
