<?php $gg = $order->order_media_problem->chunk(1) ?>
<div id="accordion" class="accordion-wrapper mb-3" style="margin-top: 10px;">
    @if($gg == null)
        <div class="card">
            <div id="headingTwo" class="b-radius-0 card-header header-border">
                <button type="button" data-toggle="collapse" data-target="#collapseOne2" aria-expanded="false" aria-controls="collapseTwo" class="text-left m-0 p-0 btn btn-link btn-block">
                    <h5 class="m-0 p-0">Media Upload</h5>
                </button>
            </div>
            <div class="card-body">
                <form id="media-form">
                    <label>SYMPTOMS DETAIL</label>
                    <div class="form-group">
                        <textarea class="form-control" name="symptom_detail" id="symptom_detail">{{ $order->symptom_detail }}</textarea>
                    </div>
                    <input type="hidden" id="media_order_id" name="media_order_id" value="{{ $order->id }}">
                    <label class="" for="">
                        <b>Upload file information media about the description of symptoms <span class="text-danger">(max 4 files image/video)</span></b>.
                    </label>
                    <br />
                    <label class="text-danger" for="">
                        <b>Video Upload max 10 MB.</b>
                    </label>
                    <!-- Example 2 -->
                    <input type="file" name="files[]" id="filer_input2" multiple="multiple">
                    <!-- end of Example 2 -->
                    <br />
                    <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-pencil"></i> Save Change</button>
                </form>
            </div>
        </div>
    @else
        <div class="card">
            <div id="headingOne" class="card-header header-border">
                <button type="button" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="true" aria-controls="collapseOne" class="text-left m-0 p-0 btn btn-link btn-block">
                    <h5 class="m-0 p-0">Media List</h5>
                </button>
            </div>
            <div data-parent="#accordion" id="collapseOne1" aria-labelledby="headingOne" class="collapse show">
                <div class="card-body">
                    <div class="gallery">
                        @foreach($gg as $gallery)
                        <div class="gallery__column">
                            @foreach($gallery as $media)
                            <a href="{{ $media->media_src }}" target="_blank" class="gallery__link">
                                <figure class="gallery__thumb">
                                    @if($media->type == 'image')
                                    <img src="{{ $media->media_src }}" alt="Portrait by Alex Perez" class="gallery__image">
                                    @else
                                    <video controls>
                                        <source src="{{ $media->media_src }}">
                                    </video>
                                    @endif
                                </figure>
                            </a>
                            @endforeach
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>