@extends('admin.home')
@section('content')


<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                DETAIL TRANSACTION ORDER LIST
            </div>
            <div class="btn-actions-pane-right text-capitalize"></div>
        </div>
        <input type="hidden" value="{{$pendingTransfer->id}}" name="user_id" id="user_id">
        <div class="card-body">
            <div class="tab-pane show active" id="tab-1" role="tabpanel">
                <div class="row">
                    {{-- <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <label for="">Technician Name</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <label for="">Account Bank</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <label for="">Technician Name</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div> --}}
                </div>
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Pending Transfer</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Success Transfer</a>
                        <a class="nav-item nav-link" onclick="clickButton()" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab" aria-controls="nav-all" aria-selected="false">All Transfer</a>
                    </div>
                </nav>
                <br>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="container">
                            <table class="display table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Order Code</th>
                                        <th>Date</th>
                                        <th>Price Services</th>
                                        <th>Total Spareparts</th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $sumAllTotal = 0;
                                    @endphp
                                    @foreach ($pendingTransfer->pendingOrder as $key => $total)
                                    
                                    @php
                                        $sparepartDetail = 0; 
                                        $data_id = $total->order->id;
                                        $total_harga = $total->price_services->commission_deduction * $total->unit;
                                        $komisi = $total->price_services->commission_value * $total->unit;
                                        
                                        if(!empty($total->order->sparepart_detail)){
                                            foreach($total->order->sparepart_detail as $totalSparepart){
                                                $sparepartDetail += $totalSparepart->price * $totalSparepart->quantity;
                                            }
                                        }

                                        if(!empty($total->order->item_detail)){
                                            foreach($total->order->item_detail as $totalSparepart){
                                                $sparepartDetail += $totalSparepart->price * $totalSparepart->quantity;
                                            }
                                        }

                                        if (!empty($total->order->saldo)){
                                            $sumAllTotal += $total->order->saldo->total + $sparepartDetail;
                                        }
                                    @endphp
                                        <tr>
                                            <td>
                                            <input type="checkbox" name="update_checkbox[]" class="check-order" data-id="{{ $total->order->id }}" id="customCheck1-{{ $total->id }}">
                                            <span class="checkmark" for="customCheck1-{{ $total->id }}"></span>
                                                <!-- <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="update_checkbox[]"  class="custom-control-input check-order" data-id="{{ $total->order->id }}" id="customCheck1-{{ $total->id }}">
                                                    <label class="custom-control-label" for="customCheck1-{{ $total->id }}" style="color:white">1</label>
                                                </div> -->
                                            </td>
                                            <td>{{ $total->order->code }} <br> <small>{{ $total->price_services->service_type->name }}</small></td>
                                            <td><small align:center><span class="badge badge-danger">Jobs Done At </span></small> <br> {{ date('Y-M-d H:i:s', strtotime($total->order->updated_at)) }}</td>
                                            <td>Rp. {{ number_format($total->price) }}<br><small align="right"> Qty{{ $total->unit }}</small></td>
                                            
                                            @if(empty($total->order->sparepart_detail))
                                                <td>Rp. 0.00</td>
                                            @else
                                                <td>Rp. {{ number_format($sparepartDetail) }}</td>
                                            @endif

                                            @if (!empty($total->order->saldo))
                                                @if(!empty($total->order->sparepart_detail))
                                                    <td>Rp. {{ number_format($total->order->saldo->total + $sparepartDetail) }} </td>
                                                @else
                                                    <td>Rp. {{ number_format($total->order->saldo->total) }} </td>
                                                @endif
                                            @else
                                                <td>Rp. 0 </td>
                                            @endif

                                            <td>
                                                <a href="" type="submit" id="successTransfer" data-id="{{ $pendingTransfer->id }}" data-update-id="{{ $total->order->id }}" class="badge badge-primary" title="Success Transfer"><i class="fa fa-check"></i></a> 
                                                <input type="hidden" name="ids" id="ids" value="{{ $total->order->id }}">
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="5" align="right">Bank account</td>
                                        <td colspan="2">{{ $pendingTransfer->rekening_number }} - ({{ ($pendingTransfer->rekening_name) }})</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" align="right">Total</td>
                                        <td colspan="2">Rp. {{ number_format($sumAllTotal) }}</td>
                                    </tr>
                                </tbody>  
                            </table>
                            @if ($pendingTransfer->pendingOrder->count() !== 0)
                                <button class="btn btn-success btn-sm updatestatus btn_succes_transfer" data-id="" style="float:right">Success Transfer</button>
                            @endif
                        </div>
                    </div>

                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="container">
                            <table class="display table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Order Code</th>
                                        <th>Date</th>
                                        <th>Price Services</th>
                                        <th>Total Spareparts</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>  
                                <tbody>
                                    @php
                                        $sumAllTotal = 0;
                                    @endphp
                                    @foreach ($succcessTransfer->doneOrder as $key => $pending)
                                    
                                    @php
                                        $sparepartDetail = 0; 
                                        $data_id = $pending->order->id;
                                        $total_harga = $pending->price_services->commission_deduction * $pending->unit;
                                        $komisi = $pending->price_services->commission_value * $pending->unit;
                                        
                                        if(!empty($pending->order->sparepart_detail)){
                                            foreach($pending->order->sparepart_detail as $totalSparepart){
                                                $sparepartDetail += $totalSparepart->price * $totalSparepart->quantity;
                                            }
                                        }

                                        if(!empty($pending->order->item_detail)){
                                            foreach($pending->order->item_detail as $totalSparepart){
                                                $sparepartDetail += $totalSparepart->price * $totalSparepart->quantity;
                                            }
                                        }

                                        if (!empty($pending->order->saldo)){
                                            $sumAllTotal += $pending->order->saldo->total + $sparepartDetail;
                                        }
                                    @endphp
                                        <tr>
                                            <td>
                                            {{$no++}}
                                            </td>
                                            <td>{{ $pending->order->code }} <br> <small>{{ $pending->price_services->service_type->name }}</small></td>
                                            <td><small align:center><span class="badge badge-danger">Jobs Done At </span></small> <br> {{ date('Y-M-d H:i:s', strtotime($pending->order->updated_at)) }}</td>
                                            <td>Rp. {{ number_format($pending->price) }}<br><small align="right"> Qty{{ $pending->unit }}</small></td>
                                            
                                            @if(empty($pending->order->sparepart_detail))
                                                <td>Rp. 0.00</td>
                                            @else
                                                <td>Rp. {{ number_format($sparepartDetail) }}</td>
                                            @endif

                                            @if (!empty($pending->order->saldo))
                                                @if(!empty($pending->order->sparepart_detail))
                                                    <td>Rp. {{ number_format($pending->order->saldo->total + $sparepartDetail) }} </td>
                                                @else
                                                    <td>Rp. {{ number_format($pending->order->saldo->total) }} </td>
                                                @endif
                                            @else
                                                <td>Rp. {{$pending->order->after_commission}}</td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="5" align="right">Bank account</td>
                                        <td colspan="2">{{ $succcessTransfer->rekening_number }} - ({{ ($succcessTransfer->rekening_name) }})</td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" align="right">Total</td>
                                        <td colspan="2">Rp. {{ number_format($sumAllTotal) }}</td>
                                    </tr>
                                </tbody>  
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="nav-all" role="tabpanel" aria-labelledby="nav-all-tab">
                        <div class="container">
                            {{-- <form id="form-search"> --}}
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="container">
                                            <table id="asd" class="display table table-hover table-bordered" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th>Code</th>
                                                        <th>Date</th>
                                                        <th>Voucher</th>
                                                        <!-- <th>Rekening Number</th> -->
                                                        <th>Grand Total</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
{{-- change status transfer --}}
<script>
    $(document).ready(function(){
        $('.btn_succes_transfer').hide();
    });
    $(document).on('change', '.check-order', function(e) {
        var id_order = $('.check-order:checked').map(function(){
            return $(this).attr('data-id');
        }).get();
        if (id_order.length > 0) {
            $('.btn_succes_transfer').show();
        }else{
            $('.btn_succes_transfer').hide();
        }
        console.log(id_order);
    })

    $(document).on('click', '.updatestatus', function(e) {
        id = $(this).attr('data-id');
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/admin/update/status-transfer'),
                data:{
                    ids : $('.check-order:checked').map(function(){
                                return $(this).attr('data-id');
                            }).get()
                },
                type: 'post',

                success: function(res) {
                    Helper.loadingStop();
                    Helper.redirectTo('/admin/transaction-success/show');
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })

    $(document).on('click', '#successTransfer', function(e) {
        idss = $(this).attr('data-update-id');
        id = $(this).attr('data-id');
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/admin/update/transfer/' + idss),
                type: 'post',
                data:{
                    ids : $('input[name="ids"]').val()
                },
                type: 'post',

                success: function(res) {
                    Helper.loadingStop();
                    Helper.redirectTo('/admin/transaction-success/detail/'+id);
                },
                error: function(res) {
                    alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })
</script>



{{-- show all transaction --}}
<script>
    // $(document).ready(function(){
        var id = $('#user_id').val()
    // })
    function clickButton(){
        $('#myButton').trigger('click', function(e) {
            var type = [];
            $.each($("#type option:selected"), function(){            
                type.push($(this).val());
            });
            if(type != ''){
                init(type);
            }else{
                init($(this).val());
            }
        });
    }
    

    
//    $(document).ready(function(){
//         var ajaxTable = init.tableall;
//         init()
        
//         $("#myButton").on('click', function(e) {
//             var type = [];
//             $.each($("#type option:selected"), function(){            
//                 type.push($(this).val());
//             });
//             if(type != ''){
//                 init(type);
//             }else{
//                 init($(this).val());
//             }
//         });
//     });
    // filter klik button
    
   
    // $('#myButton').trigger('click');


    // data table

    // function init(type_id){
        var tableAll = $('#asd').DataTable($.extend({}, window.coonDataTableOptions, {
            processing: true,
            serverSide: true,
            destroy: true,
            scrollX: true,
            select: true,
            dom: 'Bflrtip',
            ordering:'true',
            order: [2, 'desc'],
            responsive: true,
            language: {
                search: '',
                searchPlaceholder: "Search..."
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/admin/all-transfer/datatables/' + id),
                type: 'post',
            },
            columns: [
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, full) {
                        if(full.order !== null){
                            return "<a href=''># "+full.order.code+"</a>";
                        }else{
                            return "04092020-000002";
                        }
                    }
                },
                {
                    data: "updated_at",
                    name: "updated_at",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        return moment(full.updated_at).format("DD MMMM YYYY HH:mm");
                    }
                },
                {
                    data: "voucer_id",
                    name: "voucer_id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        if (full.voucer_id == 0) {
                            return "-";
                        } else if (full.voucer_id == 1) {
                            return "--";
                        }
                    }
                },
                // {
                //     data: "id",
                //     name: "id",
                //     orderable: false,
                //     searchable: false,
                //     render: function(data, type, full) {
                //             return full.teknisi.rekening_number;
                //     }
                // },
                {
                    data: "total",
                    name: "total",
                    orderable: false,
                    searchable: false,
                    render: $.fn.dataTable.render.number( ',', '.,', 0, 'Rp. ', '.00' )
                    // render: function(data, type, full) {
                    //         return full.total;
                    // }
                }
            ],
            "autoWidth": false
        }));
    // }

</script>

@endsection
