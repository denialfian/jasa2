@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                TRANSACTION ORDER LIST PENDING SERVICE
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="/admin/transactions/create" class="mb-2 mr-2 btn btn-primary btn-sm" style="float:right">Create Order</a>
            </div>
        </div>
        <div class="col-md-12 d-flex bd-highlight mb-3">
            <div class="drop own d-inline-block mr-auto p-2 bd-highlight">
                <table>
                    <tr>
                        <td><a href="{{ url('/admin/transactions/show') }}" style="color: blue;font-size: 10px;;"> ALL   </a>|</td>
                        <td><a href="{{ url('/admin/transactions/show/pending') }}" style="color: blue;font-size: 10px;;font-weight:1000">Pending Service   </a>|</td>
                        <td><a href="{{ url('/admin/transactions/show/approve-request') }}" style="color: blue;font-size: 10px;">Approve Service   </a>|</td>
                        <td><a href="{{ url('/admin/transactions/show/pending-payment') }}" style="color: blue;font-size: 10px;">Pending Payment   </a>|</td>
                        <td><a href="{{ url('/admin/transactions/show/processing') }}" style="color: blue;font-size: 10px;">Processing   </a>|</td>
                        <td><a href="{{ url('/admin/transactions/show/confirmation-payment') }}" style="color: blue;font-size: 10px;">Confirm Payment   </a>|</td>
                        <td><a href="{{ url('/admin/transactions/show/done') }}" style="color: blue;font-size: 10px;">Done </a>|</td>
                        <td><a href="{{ url('/admin/transactions/show/cancel') }}" style="color: blue;font-size: 10px;">Cancel </a></td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="card-body">
            <div class="tab-pane show active" id="tab-1" role="tabpanel">
                <table id="table-pending-request" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Orders Code</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Customer Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="p-2 bd-highlight">
                <a href="/admin/transactions/show/Trash" class="mb-2 mr-2 btn btn-danger btn-sm" style="float:left" disabled><i class="fa fa-trash"></i> Restore Data</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>

    $('input[name="schedule"]').daterangepicker();
    // table

    var pendingTable = $('#table-pending-request').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        select: true,
        dom: 'Bflrtip',
        ordering:'true',
        order: [1, 'desc'],
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search..."
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/admin/order/datatables'),
            type: 'post',
            data:{
                status : 2
            }
        },
        columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                sortable: false,
                width: "10%"
            },
            {
                data: "code",
                name: "code",
                render: function(data, type, full) {
                    return "<a href='/admin/order/service_detail/"+ full.id + "'># "+full.code+"</a>";
                }
            },
            {
                data: "created_at",
                name: "created_at",
                render: function(data, type, full){
                    return moment(full.schedule).format("DD MMMM YYYY HH:mm");
                }
            },
            {
                data: "order_status.name",
                name: "order_status.name",
            },
            {
                data: "user.name",
                name: "user.name",
            },
            {
                data: "id",
                name: "id",
                orderable: false,
                searchable: false,
                render: function(data, type, full) {

                    return "<a href='/admin/order/service_detail/"+ full.id + "' class='btn btn-success btn-sm'><i class='fa fa-eye'></i> Detail </a> &nbsp; <button class='delete btn btn-danger btn-sm' data-id='"+ full.id + "'><i class='fa fa-trash'></i>&nbsp; Delete</button>";

                }
            }
        ]
    });

    $(document).on('click', '.delete', function(e) {
        id = $(this).attr('data-id');
        ajaxTable = pendingTable.ajax
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/admin/order/delete/' + id ),
                type: 'get',

                success: function(res) {
                    Helper.successNotif('data berhasil didelete');
                    Helper.loadingStop();
                    ajaxTable.reload();
                },
                error: function(res) {
                    // alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })


</script>
@endsection
