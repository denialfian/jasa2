@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                TRANSACTION COMPLAINT
            </div>
        </div>
        
        <div class="card-body">
            <table id="table-list-orders" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Orders Code</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Act ions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection


@section('script')
<script>
    var table = $('#table-list-orders').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        select: true,
        dom: 'Bflrtip',
        ordering:'true',
        order: [3, 'desc'],
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search..."
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/admin/complaint-transaction/datatables'),
            type: "get",

        },
        columns: [
            {
                data: "code",
                name: "code",
                render: function(data, type, full) {
                    return "<a href='/customer/service_detail/"+ full.id + "'># "+full.code+"</a>";
                }
            },
            {
                data: "updated_at",
                name: "updated_at",
                render: function(data, type, full){
                    return moment(full.updated_at).format("DD MMMM YYYY");
                }
            },
            {
                data: "order_status.name",
                name: "order_status.name",
            },
            {
                data: "id",
                name: "id",
                orderable: false,
                searchable: false,
                render: function(data, type, full) {
                    if (full.order_status.id == 11) {
                        return "<a href='/admin/transacsion_list/complaint/detail/"+ full.id +"' class='badge badge-success btn-sm ' data-toggle='tooltip' data-html='true' title='<b>Detail</b>'><i class='fa fa-eye'></i></a>";
                    }
                    return '-';
                }
            }
        ]
    });
</script>

@endsection
