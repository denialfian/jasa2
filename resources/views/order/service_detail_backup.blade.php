@extends('admin.home')
@section('content')


<style>
    .rounded-profile {
        border-radius: 100% !important;
        overflow: hidden;
        width: 100px;
        height: 100px;
        border: 8px solid rgba(255, 255, 255, 0.7);
    }
</style>
<input type="hidden" name="order_id" value="{{ $order->id }}" id="order_id">
<input type="hidden" id="type_edit">
<div class="col-md-12">
    <div class="card">
        <div class="card-header header-bg">
            Admin View Detail Transaction
        </div>
    </div>
</div>

<div class="col-md-9">
    <div class="row">
        <div class="col-md-12" style="margin-top: 10px;">
            <div class="card">
                <div class="header-border card-header">
                    Order #{{ $order->code }} / {{ $order->created_at }}&nbsp;&nbsp;
                    @if($order->is_less_balance == 1)
                    <span class="badge badge-danger">Less Balance</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-6" style="margin-top: 10px;">
            <div class="card">
                <div class="card-header">
                    Billing Detail
                    <div class="btn-actions-pane-right text-capitalize">
                        <button type="button" id="edit_billing" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit Billing</button>
                    </div>
                </div>
                <div class="card-body">
                    <label>{{ $order->user->name }}</label>
                    <p id="address_display">{{ $order->address }}</p>
                    <div><small id="notif_update_edit_billing" class="text-success"></small></div>
                    <div class="map" style="margin-top:15px;">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_maps" data-lat='{{ $order->user->address->latitude }}' data-lng='{{ $order->user->address->longitude }}'><i class="fa fa-map-marker"></i> View Location</button>
                    </div>

                    <hr />
                    <label>Email Address:</label>
                    <p>{{ $order->user->email }}</p>
                    <hr />

                    <label>Phone:</label>
                    <p>{{ $order->user->phone }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12" style="margin-top: 10px;">
                <div class="card">
                    <div class="card-header">
                        Order Detail
                        <div class="btn-actions-pane-right text-capitalize">
                            <button type="button" class="btn btn-sm btn-success" id="edit_order"><i class="fa fa-edit"></i> Edit Order</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <label>Schedule:</label>
                        <p id="schedule_display">{{ $order->schedule->format('d F Y G:i') }}</p>

                        @if($order->reschedule)
                        <hr />
                        <div class="text-success" style="margin-bottom: 15px;">
                            <p><b>Reschedule</b></p>
                            <p>{{ $order->reschedule }}</p>
                        </div>
                        @endif

                        <hr />
                        <label>Customer Note:</label>
                        <p id="customer_note_display">{{ $order->note }}</p>

                        <hr />
                        <div><small id="notif_update_edit_order" class="text-success"></small></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 10px;">
                <div class="card">
                    <div class="card-header">
                        Billing Amount
                    </div>
                    <div class="card-body">
                        <div class="price scroll-area-sm" style="margin-bottom: 30px;">
                            <div class="scrollbar-container ps--active-y ps">
                                <div class="row">
                                    <div class="col-md-12"><b>Services :</b></div>
                                </div>
                                @php $total = 0 @endphp
                                @foreach($order->service_detail as $key => $detail)
                                @php
                                $harga = isset($detail->price) ? $detail->price * $detail->unit : 0;
                                $total_harga = $harga * $detail->unit;
                                @endphp
                                <p><b>{{ $detail->services_type->name }}</b></p>

                                <div class="row">
                                    <div class="col-md-8">Rp. {{ number_format($detail->price) }}</div>
                                    <div class="col-md-4">x <b>{{ $detail->unit }}</b></div>
                                </div>
                                @php $total += $harga @endphp
                                @endforeach

                                @if(count($sparepart_details) > 0)
                                <div class="row">
                                    <div class="col-md-12"><b>Sparepart :</b></div>
                                </div>
                                @foreach($sparepart_details as $sparepart_detail)
                                <p><b>{{ $sparepart_detail->name_sparepart }}</b></p>
                                <div class="row">
                                    <div class="col-md-8">Rp. {{ $sparepart_detail->priceThousandSeparator() }}</div>
                                    <div class="col-md-4 ">x <b>{{ $sparepart_detail->quantity }}</b></div>
                                </div>
                                @php $total += ($sparepart_detail->price * $sparepart_detail->quantity) @endphp
                                @endforeach
                                @endif

                                @if(count($item_details) > 0)
                                <div class="row">
                                    <div class="col-md-12"><b>Sparepart :</b></div>
                                </div>
                                @foreach($item_details as $item)
                                <p><b>{{ $item->inventory->batch_item->product_name }}</b></p>
                                <div class="row">
                                    <div class="col-md-8">Rp. {{ number_format($item->price) }}</div>
                                    <div class="col-md-4 ">x <b>{{ $item->quantity }}</b></div>
                                </div>
                                @php $total += ($item->price * $item->quantity) @endphp
                                @endforeach
                                @endif
                                <input type="hidden" id="total_service" value="{{ $total }}">
                            </div>
                        </div>
                        <div class="text-right">
                            @if($order->orders_statuses_id == 9)
                            <label>Delivery Note : &nbsp;&nbsp;</label>
                            <a href="{{ url('/admin/order/workmanship_report_pdf/'. $order_id ) }}" class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-sm btn-outline-warning"><i class="fa fa-file-pdf"></i></a>
                            <small>.pdf</small>
                            @endif
                            <hr />
                            @if ($order->orders_statuses_id == 1 || $order->orders_statuses_id == 2 || $order->orders_statuses_id == 5 || $order->orders_statuses_id == 6)
                            <label>Invoice: &nbsp; - </label>
                            @else
                            <label>Invoice: &nbsp;&nbsp;</label>
                            <a href="{{ url('/admin/invoice/'. $order_id ) }}" class="mb-2 mr-2 btn-icon btn-icon-only btn-shadow btn-outline-2x btn btn-sm btn-outline-danger" target="_blank"><i class="fa fa-file-pdf"></i></a>
                            <small>.pdf</small>
                            @endif
                        </div>
                        <div class="text-right">
                            <hr />
                            <h6><b>TOTAL&nbsp; : </b>Rp. {{ number_format($total) }}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-3">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card header-border">
            <div class="card-body">
                <div class="form-group">
                    <label>STATUS</label>
                    <select class="form-control" id="orders_status" name="orders_status" style="width:100%">
                        @if ($order->orders_statuses_id == 2)
                        <option value="2">Pending Request</option>
                        <option value="3">Approve Request</option>
                        {{-- <option value="4">Confirm Payment</option> --}}
                        <option value="7">Jobs Done</option>
                        <option value="8">Pending Payment</option>
                        <option value="9">Processing</option>
                        <option value="10">Jobs Done</option>
                        <option value="5">Cancel</option>
                        <option value="6">Reschedule</option>
                        @elseif($order->orders_statuses_id == 3)
                        <option value="3">Approve Request</option>
                        <option value="2">Pending Request</option>
                        <option value="7">Jobs Done</option>
                        {{-- <option value="4">Confirm Payment</option> --}}
                        <option value="8">Pending Payment</option>
                        <option value="9">Processing</option>
                        <option value="10">Jobs Done</option>
                        <option value="5">Cancel</option>
                        <option value="6">Reschedule</option>

                        @elseif($order->orders_statuses_id == 5)
                        <option value="5">Cancel</option>
                        <option value="3">Approve Request</option>
                        <option value="7">Jobs Done</option>
                        <option value="2">Pending Request</option>
                        {{-- <option value="4">Confirm Payment</option> --}}
                        <option value="8">Pending Payment</option>
                        <option value="9">Processing</option>
                        <option value="10">Jobs Done</option>
                        <option value="6">Reschedule</option>
                        @elseif($order->orders_statuses_id == 6)
                        <option value="6">Reschedule</option>
                        <option value="5">Cancel</option>
                        <option value="7">Jobs Done</option>
                        <option value="3">Approve Request</option>
                        <option value="2">Pending Request</option>
                        {{-- <option value="4">Confirm Payment</option> --}}
                        <option value="8">Pending Payment</option>
                        <option value="9">Processing</option>
                        <option value="10">Jobs Done</option>
                        @elseif($order->orders_statuses_id == 7)
                        <option value="7">Jobs Done</option>
                        <option value="6">Reschedule</option>
                        <option value="5">Cancel</option>
                        <option value="3">Approve Request</option>
                        <option value="2">Pending Request</option>
                        {{-- <option value="4">Confirm Payment</option> --}}
                        <option value="8">Pending Payment</option>
                        <option value="9">Processing</option>
                        <option value="10">Jobs Done</option>
                        @elseif($order->orders_statuses_id == 8)
                        <option value="8">Pending Payment</option>
                        <option value="9">Processing</option>
                        {{-- <option value="4">Confirm Payment</option> --}}
                        <option value="3">Approve Request</option>
                        <option value="2">Pending Request</option>
                        <option value="10">Jobs Done</option>
                        <option value="5">Cancel</option>
                        <option value="6">Reschedule</option>
                        @elseif($order->orders_statuses_id == 9)
                        <option value="9">Processing</option>
                        <option value="3">Approve Request</option>
                        <option value="2">Pending Request</option>
                        <option value="8">Pending Payment</option>
                        {{-- <option value="4">Confirm Payment</option> --}}
                        <option value="10">Jobs Done</option>
                        <option value="5">Cancel</option>
                        <option value="6">Reschedule</option>
                        @elseif($order->orders_statuses_id == 10)
                        <option value="10">Completed</option>
                        <option value="3">Approve Request</option>
                        <option value="2">Pending Request</option>
                        {{-- <option value="4">Confirm Payment</option> --}}
                        <option value="8">Pending Payment</option>
                        <option value="9">Processing</option>
                        <option value="5">Cancel</option>
                        <option value="6">Reschedule</option>
                        @elseif($order->orders_statuses_id == 11)
                        <option value="11">Complaint</option>
                        <option value="10">Completed</option>
                        <option value="3">Approve Request</option>
                        <option value="2">Pending Request</option>
                        {{-- <option value="4">Confirm Payment</option> --}}
                        <option value="8">Pending Payment</option>
                        <option value="9">Processing</option>
                        <option value="5">Cancel</option>
                        <option value="6">Reschedule</option>


                        @endif
                    </select>
                </div>
                <p>Resend Email</p>
                <div class="form-group">
                    <div class="position-relative form-check form-check-inline">
                        <label class="form-check-label">
                            <input type="checkbox" id="is_resend_email" class="form-check-input"> <small>notify email to customer.</small>
                        </label>
                    </div>
                </div>

                <button type="button" id="update_status" class="btn btn-sm btn-primary"><i class="fa fa-check-circle"></i> UPDATE STATUS</button>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="header-border card-header">
                DATA TRANSFER
            </div>
            <div class="card-body">
                <label>A.N Bank</label>
                <input type="text" class="form-control" name="" id="" value="{{ empty($showDataTransfer->a_n_bank) ? '' : $showDataTransfer->a_n_bank }}" readonly>

                <label>Transfer Date</label>
                <input type="text" class="form-control" name="" id="" value="{{ empty($showDataTransfer->transfer_date ) ? '' : date('d-M-Y', strtotime($showDataTransfer->transfer_date)) }}" readonly>

                <label>Nominal</label>
                <input type="text" class="form-control" name="" id="" value="Rp. {{ empty($showDataTransfer->nominal) ? '' : number_format($showDataTransfer->nominal) }}" readonly>

                <label>Note</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>{{ empty($showDataTransfer->note) ? "" : $showDataTransfer->note }}</textarea>

                <br />
                @if (empty($showDataTransfer->attachment))

                @else
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal">
                    Show Attachment
                </button>
                @endif

            </div>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Mechanic Assign
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="user-profile">
                        <img class="avatar" src="{{ $order->service_detail->first()->technician->user->avatar }}" alt="Ash" />
                        <div class="username">{{ $order->service_detail->first()->technician->user->full_name }}</div>
                        <div class="bio">
                            <p>{{ $order->service_detail->first()->technician->user->email }}</p>
                            {!! $order->service_detail->first()->technician->getRatingStarUi() !!}
                            <br />
                            <span class="star">Review ({{ $order->service_detail->first()->technician->total_review }} user)</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="text-right" style="margin-bottom:47px;">
                        @if ($order->order_status != null)
                        <h4>{{ $order->order_status->name }}</h4>
                        @endif
                    </div>
                    <div class="text-right">
                        <a target="__blank" href="{{ url('admin/user/detail/' .  $order->service_detail[0]->technician->user_id) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> View Mechanic</a>

                        <button type="button" class="btn btn-sm btn-primary assign-mechanic">Assign New Mechanic</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            History Transaction
        </div>
        <div class="card-body">
            <div class="price scroll-area-sm" style="margin-top:25px; margin-bottom: 30px; height:200px">
                <div class="scrollbar-container ps--active-y ps">
                    <div class="vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                        @foreach($order->history_order->reverse() as $key => $history_order)
                        <?php
                        $color = '';
                        $text = '';
                        if ($history_order->orders_statuses_id == 1) {
                            $color = 'primary';
                            $text = 'New order has created - ';
                        } elseif ($history_order->orders_statuses_id == 2) {
                            $color = 'warning';
                            $text = 'Job has pending, need waiting confirmation at - ';
                        } elseif ($history_order->orders_statuses_id == 3) {
                            $color = 'success';
                            $text = 'Job has approved by ' . $order->service_detail[0]->technician->user->name . ' at - ';
                        } elseif ($history_order->orders_statuses_id == 8) {
                            $color = 'warning';
                            $text = 'Pending Payment at - ';
                        } elseif ($history_order->orders_statuses_id == 9) {
                            $color = 'danger';
                            $text = 'Processing at - ';
                        } elseif ($history_order->orders_statuses_id == 4) {
                            $color = 'success';
                            $text = 'Payment successfully paid at - ';
                        } elseif ($history_order->orders_statuses_id == 5) {
                            $color = 'danger';
                            $text = 'Order has canceled at - ';
                        } elseif ($history_order->orders_statuses_id == 6) {
                            $color = 'warning';
                            $text = 'Order has rescheduled at - ';
                        } elseif ($history_order->orders_statuses_id == 7) {
                            $color = 'success';
                            $text = 'Job has done ! at - ';
                        }
                        ?>
                        <div class="vertical-timeline-item vertical-timeline-element">
                            <div><span class="vertical-timeline-element-icon bounce-in"><i class="badge badge-dot badge-dot-xl badge-{{ $color }}"> </i></span>
                                <div class="vertical-timeline-element-content bounce-in">
                                    <h2 class="timeline-title">{{ $history_order->order_status->name }}</h2>
                                    <p>{{ $text }}<b class="text-{{ $color }}">{{ $history_order->created_at }}</b></p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@if(count($sparepart_details) > 0 || count($item_details) > 0)
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Additional sparepart from
        </div>
    </div>
</div>
@endif

@if(count($sparepart_details) > 0)
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header">
            Technician Inventory
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Name</td>
                        <td>Price</td>
                        <td>Qty</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sparepart_details as $no => $sparepart_detail)
                    <tr>
                        <td>{{ $no + 1 }}</td>
                        <td>{{ $sparepart_detail->name_sparepart }}</td>
                        <td>Rp.{{ $sparepart_detail->priceThousandSeparator() }}</td>
                        <td>{{ $sparepart_detail->quantity }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer pull-right">
            <h6><b>TOTAL : </b>Rp. {{ number_format($sparepart_details->sum(function($detail){ return $detail->price * $detail->quantity;})) }}</h6>
        </div>
    </div>
</div>
@endif

@if(count($item_details) > 0)
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header">
            Company Inventory
        </div>
        <div class="card-body">
            <div class="row">
                @foreach($item_details as $item)
                <div class="col-md-2">
                    <img class="img-responsive" width="100px" height="100px" src="{{ $item->inventory->batch_item->product->main_image }}" alt="Ash" />
                </div>
                <div class="col-md-10">
                    <table class="table table-bordered table-sm table-no-margin nowrap">
                        <tr>
                            <td>Name</td>
                            <td>{{ $item->inventory->batch_item->product_name }}</td>
                        </tr>
                        <tr>
                            <td>Price</td>
                            <td>Rp.{{ number_format($item->price) }}</td>
                        </tr>
                        <tr>
                            <td>Qty</td>
                            <td>{{ $item->quantity }}</td>
                        </tr>
                    </table>
                </div>
                @endforeach
            </div>
        </div>
        <div class="card-footer pull-right">
            <h6><b>TOTAL : </b>Rp. {{ number_format($item_details->sum(function($detail){ return $detail->price * $detail->quantity;})) }}</h6>
        </div>
    </div>
</div>
@endif

@if(count($sparepart_details) > 0 && count($item_details) > 0)
<div class="col-md-12" style="margin-top: 10px;">
    <div class="card">
        <div class="card-header header-border">
            Extra Part Needed for this job
        </div>
        <div class="card-body">
            <div class="row" style="margin-bottom: 25px;">
                <div class="col-md-12">
                    <div class="card-shadow-alternate border mb-3 card card-body border-alternate">
                        <h5 class="card-title">Extra Part Needed for this job</h5>
                        <div class="d-flex">
                            <div class="col-md-8" style="margin-bottom: 15px;">
                                <div class="price scroll-area-sm" style="height:175px">
                                    <div class="scrollbar-container ps--active-y ps">
                                        @php
                                        $total_price = 0
                                        @endphp
                                        @foreach($sparepart_details as $key => $sparepart)
                                        @php
                                        $harga_sparepart = isset($sparepart->price) ? $sparepart->price : 0;
                                        $total_harga_sparepart = $harga_sparepart * $sparepart->quantity;
                                        @endphp
                                        <p><b>{{ $sparepart->name_sparepart }}</b></p>
                                        <div class="row" style="margin-bottom: 5px;">
                                            <div class="col-md-8">{{ number_format($sparepart->price) }}</div>
                                            <div class="col-md-4">x <b>{{ $sparepart->quantity }}</b></div>
                                        </div>
                                        @php $total_price += $total_harga_sparepart @endphp
                                        @endforeach
                                        @foreach($item_details as $key => $item)
                                        <!-- part form product inventory -->
                                        @php
                                        $harga_item = isset($item->price) ? $item->price : 0;
                                        $total_harga_item = $harga_item * $item->quantity;
                                        @endphp
                                        <p><b>{{ $item->name_product }}</b></p>
                                        <div class="row" style="margin-bottom: 5px;">
                                            <div class="col-md-8">{{ number_format($item->price) }}</div>
                                            <div class="col-md-4">x <b>{{ $item->quantity }}</b></div>
                                        </div>
                                        @php $total_price += $total_harga_item @endphp
                                        @endforeach
                                        @if($total_price == 0)
                                        <h5>No Extra Part Needed.</h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="ml-auto p2">
                                {{-- <button type="button" href="#" class="mb-3 mr-3 btn btn-sm btn-success">View Edit Part Needed</button> --}}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-5 card-shadow-alternate border mb-3 card card-body float-right">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <h6><b>GRAND TOTAL&nbsp; : </b>Rp. {{ number_format($total_price) }}</h6>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endif

{{-- ==================================Modal========================= --}}
<div class="modal fade" id="exampleModal" tabindex="-1" data-backdrop="false" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Attachment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    @if (empty($showDataTransfer->attachment))
                    <img src="https://via.placeholder.com/728x90" alt="">
                    @else
                    <img src="{{ asset('storage/buktiTransfer/' . $showDataTransfer->attachment) }}" alt="" style="width:728px;height:200px">
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- modal edit order -->
<div class="modal fade" data-backdrop="false" id="modal-edit-order" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Reschedule
                </h4>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        <label class="" for="">
                            Schedule
                        </label>
                        <input class="form-control datetimepicker" id="schedule" required="" type="text" readonly />
                    </div>
                    <div class="position-relative form-group">
                        <label class="" for="">
                            Customer Note
                        </label>
                        <textarea class="form-control" id="customer_note" rows="5" required=""></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary waves-effect order_detail">
                    SAVE
                </button>
                <button class="btn waves-effect" data-dismiss="modal" type="button">
                    CLOSE
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="false" id="modal-edit-billing" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Edit Address
                </h4>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">

                    <div class="position-relative form-group">
                        <label class="" for="">
                            Address
                        </label>
                        <textarea class="form-control" id="address_edit" rows="5" required=""></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary waves-effect order_detail">
                    SAVE
                </button>
                <button class="btn waves-effect" data-dismiss="modal" type="button">
                    CLOSE
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" data-backdrop="false" id="modal-edit-teknisi" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    New Tecnician
                </h4>
                <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <label>SELECT TECHNICIAN</label>
                <div class="form-group">
                    <select id="select-technicians" class="form-control" name="technician_id" style="width:100%" required>
                        <option value="" selected="selected">--Select--</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary waves-effect order_detail">
                    SAVE
                </button>
            </div>
        </div>
    </div>
</div>>
<style>
    .table-no-margin tr td {
        padding: 0 !important;
        margin: 0 !important;
    }

    .user-profile {
        box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
        margin: auto;
        height: 8em;
        background: #fff;
        border-radius: .3em;
        margin-bottom: 20px;
    }

    .user-profile .username {
        margin: auto;
        margin-top: -4.40em;
        margin-left: 5.80em;
        color: #658585;
        font-size: 1.53em;
        font-family: "Coda", sans-serif;
        font-weight: bold;
    }

    .user-profile .bio {
        margin: auto;
        display: inline-block;
        margin-left: 10.43em;
        margin-bottom: 10px;
        color: #e76043;
        font-size: .87em;
        font-family: "varela round", sans-serif;
    }

    .user-profile>.description {
        margin: auto;
        margin-top: 1.35em;
        margin-right: 4.43em;
        width: 14em;
        color: #c0c5c5;
        font-size: .87em;
        font-family: "varela round", sans-serif;
    }

    .user-profile>img.avatar {
        padding: .7em;
        margin-left: .3em;
        margin-top: .3em;
        height: 6.23em;
        width: 6.23em;
        border-radius: 18em;
    }
</style>
@endsection

@section('script')
@include('admin.template._locationServiceDetail')
<link rel="stylesheet" href="{{ asset('adminAssets/css/frontend.css') }}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js">
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" rel="stylesheet" />
<script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
    Helper.date('.datetimepicker', 1);

    $(document).on('click', '#edit_order', function() {
        $('#modal-edit-order').modal('show');
        $('#type_edit').val('schedule');
    })

    $(document).on('click', '#edit_billing', function() {
        $('#address_edit').val($('#address_display').html());
        $('#modal-edit-billing').modal('show');
        $('#type_edit').val('address_edit');
    })

    // agar user profile bisa diclik
    $(".show-userinfo-menu").click(function() {
        $('.widget-content-wrapper').toggleClass('show')
        $('.dropdown-menu-right').toggleClass('show')
    })

    globalCRUD.select2Static("#order_status", '/order-status/select2');
    var TemplateObj = {
        techpartDeleteBtn: function(uniq) {
            template = '';
            template += '<button data-uniq="' + uniq + '" class="btn btn-outline-danger btn-delete-techpart btn-delete-techpart-uniq-' + uniq + '" type="button">';
            template += '<i class="fa fa-remove"></i> Delete';
            template += '</button>';
            return template;
        },

        techPartBody: function(uniq) {
            template = '';
            template += '<input type="hidden" name="sparepart_detail_id[]" value="0" class="sparepart_detail_input-' + uniq + '">';

            return template;
        },

    }

    //dinamis
    var sparepartObj = {
        getTechpartValue: function() {
            techpart = $('select[name="techpart_id[]"]').map(function() {
                uniq = $(this).attr('data-uniq');
                return {
                    sparepart_detail_id: $('.sparepart_detail_id-' + uniq).val(),
                };
            }).get();
            return techpart;
        },
        // delete technician sparepart
        techpartDelete: function(uniq, sparepart_detail_id = null) {
            Helper.confirm(function() {
                if (sparepart_detail_id) {
                    sparepartObj
                        .destroyTechpart(sparepart_detail_id, function(resp) {
                            $('.techpart-uniq-' + uniq).remove();
                        })
                } else {
                    $('.techpart-uniq' + uniq).remove();
                }
            });
        },
        // select 2
        createSelect2: function(selector, url, callback = null) {
            $(selector).select2({
                ajax: {
                    type: "GET",
                    url: Helper.apiUrl(url),
                    data: function(params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function(data) {
                        if (callback == null) {
                            var res = $.map(data.data, function(item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            });
                        } else {
                            var res = $.map(data.data, callback);
                        }

                        return {
                            results: res
                        };
                    }
                }
            });
        },
        select2Techpart: function(uniq) {
            this.createSelect2('.select-techpart-' + uniq, '/sparepart-detail/select2');
        },
    }

    // add button technician sparepart
    $("#btn-add-techpart")
        .click(function(e) {
            uniq = _uniq_counter;
            attributes = ProductObj.getProductAttHeaderValue();
            can_add = true;
            _.each(attributes, function(row) {
                if (typeof row.terms == 'undefined') {
                    can_add = false;
                    return;
                }
            })

            if (can_add) {
                $('#product-vendor-area')
                    .append(TemplateObj.vendor(uniq))

                ProductObj.select2Vendor(uniq);

                $('.btn-add-varian-uniq-' + uniq).hide();

                _uniq_counter++;
                _uniq_counter_varian = 0;
            } else {
                Helper.errorNotif('please select attribute first')
            }
        });

    $(document).on('click', '.btn-delete-techpart', function() {
        uniq = $(this).attr('data-uniq');
        techpart_id = $(this).attr('data-id');

        console.log('techpart_id', techpart_id)
        ProductObj.techpartDelte(uniq, product_vendor_id);
    })

    $('#edit_order').click(function() {
        $('#schedule').val($('#schedule_display').html());
        $('#customer_note').val($('#customer_note_display').html());
    });

    $('#update_status').click(function() {
        data = {};
        data.order_id = $('#order_id').val();
        data.order_status = $('#orders_status').val();
        data.is_resend_email = 0;
        if ($('#is_resend_email').is(':checked') == true) {
            data.is_resend_email = 1;
        }
        console.log(data);
        $.ajax({
            url: Helper.apiUrl('/admin/order/update_status'),
            type: 'post',
            data: data,
            success: function(response) {
                // console.log(response);
                Helper.successNotif('Accepted Job Berhasil di Submit !');
                window.location.href = Helper.redirectUrl('/admin/transactions/show');

                location.reload()
            }
        });
    });

    $('.order_detail').click(function() {
        Helper.confirm(() => {
            data = {};
            data.order_id = $('#order_id').val();
            if ($('#type_edit').val() == 'address_edit') {
                data.address = $('#address_edit').val();
            } else {
                data.schedule = $('#schedule').val();
                data.note = $('#customer_note').val();
            }

            console.log(data);
            $.ajax({
                url: Helper.apiUrl('/admin/order/update_detail/' + $('#type_edit').val() + ''),
                type: 'post',
                data: data,
                success: function(response) {
                    console.log(response);

                    // window.location.href = Helper.redirectUrl('/admin/order/list');

                    // location.reload()
                    if ($('#type_edit').val() == 'address_edit') {
                        $('#address_display').html(data.address).addClass('text-success');
                        $('#notif_update_edit_billing').html('* Order Billing (Address) has been updated !');
                        Helper.successNotif('Edit Billing has been succeded !');
                        $('#modal-edit-billing').modal('hide');

                    } else {
                        $('#schedule_display').html(data.schedule)
                        $('#customer_note_display').html(data.note);
                        $('#notif_update_edit_order').html('* Order Detail has been updated !');
                        Helper.successNotif('Edit Order has been succeded !');
                        $('#modal-edit-order').modal('hide');

                    }



                }
            });
        });
    });
</script>

<script>
    $('.assign-mechanic').click(function() {
        $('#modal-edit-teknisi').modal('show')
    })
</script>
@endsection