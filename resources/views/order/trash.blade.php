@extends('admin.home')
@section('content')
<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                TRASH DATA
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="/admin/transactions/show" class="mb-2 mr-2 btn btn-primary btn-sm" style="float:right">Back</a>
            </div>
        </div>
        

        <div class="card-body">
            <div class="tab-pane show active" id="tab-1" role="tabpanel">
                <table id="table-approve_request" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Orders Code</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Customer Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
            @if ($trash > 0)
                <div class="p-2 bd-highlight">
                    <button class='undoAll btn btn-warning btn-white btn-sm' data-toggle='tooltip' data-html='true' title='<b>Restore All</b>'><i class='fa fa-undo'></i></button>
                    <button class='deleteAll btn btn-danger btn-sm' data-toggle='tooltip' data-html='true' title='<b>Delete All</b>'><i class='fa fa-trash'></i></button>
                </div>
            @endif
                
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>

    // $('input[name="schedule"]').daterangepicker();
    // table

    var undoAll = $('#table-approve_request').DataTable({
        processing: true,
        serverSide: true,
        destroy: true,
        scrollX: true,
        select: true,
        dom: 'Bflrtip',
        ordering:'true',
        order: [1, 'desc'],
        responsive: true,
        language: {
            search: '',
            searchPlaceholder: "Search..."
        },
        oLanguage: {
            sLengthMenu: "_MENU_",
        },
        dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-6'i><'col-sm-6'p>>",
        ajax: {
            url: Helper.apiUrl('/admin/order/trash/datatables'),
            type: 'get',
        },
        columns: [{
                data: "DT_RowIndex",
                name: "DT_RowIndex",
                sortable: false,
                width: "10%"
            },
            {
                data: "code",
                name: "code",
                render: function(data, type, full) {
                    return "<a href='/admin/order/service_detail/"+ full.id + "'># "+full.code+"</a>";
                }
            },
            {
                data: "created_at",
                name: "created_at",
                render: function(data, type, full){
                    return moment(full.schedule).format("DD MMMM YYYY HH:mm");
                }
            },
            {
                data: "order_status.name",
                name: "order_status.name",
            },
            {
                data: "id",
                name: "id",
                render: function(data, type, full){
                    if(full.user == null){
                        return "Deleted Users";
                    }else{
                        return full.user.name;
                    }
                }
            },
            {
                data: "id",
                name: "id",
                orderable: false,
                searchable: false,
                render: function(data, type, full) {
                    return "<button class='undo btn btn-warning btn-sm' data-id='"+ full.id + "'><i class='fa fa-undo'></i></button> &nbsp;<button class='deleteOne btn btn-danger btn-sm' data-id='"+ full.id + "'><i class='fa fa-trash'></i></button> &nbsp;<a href='/admin/order/service_detail/"+ full.id + "' class='btn btn-primary btn-sm' data-toggle='tooltip' data-html='true' title='Detail'><i class='fa fa-eye'></i></a>"

                }
            }
        ]
    });

    $(document).on('click', '.deleteOne', function(e) {
        id = $(this).attr('data-id');
        ajaxTable = undoAll.ajax
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/admin/order/delete_trash/' + id ),
                type: 'get',

                success: function(res) {
                    Helper.successNotif('Success');
                    Helper.loadingStop();
                    ajaxTable.reload();
                },
                error: function(res) {
                    // alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })

    $(document).on('click', '.undo', function(e) {
        id = $(this).attr('data-id');
        ajaxTable = undoAll.ajax
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/admin/order/undo/' + id ),
                type: 'get',

                success: function(res) {
                    Helper.successNotif('Success');
                    Helper.loadingStop();
                    ajaxTable.reload();
                },
                error: function(res) {
                    // alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })

    $(document).on('click', '.undoAll', function(e) {
        ajaxTable = undoAll.ajax
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/admin/order/undo_all'),
                type: 'get',

                success: function(res) {
                    Helper.successNotif('Data Hass Been Change');
                    Helper.loadingStop();
                    ajaxTable.reload();
                },
                error: function(res) {
                    // alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })

    $(document).on('click', '.deleteAll', function(e) {
        ajaxTable = undoAll.ajax
        Helper.confirm(function(){
            Helper.loadingStart();
            $.ajax({
                url:Helper.apiUrl('/admin/order/delete_trash_all'),
                type: 'get',

                success: function(res) {
                    Helper.successNotif('Data Hass Been Change');
                    Helper.loadingStop();
                    ajaxTable.reload();
                },
                error: function(res) {
                    // alert("Something went wrong");
                    console.log(res);
                    Helper.loadingStop();
                }
            })
        })
        
        e.preventDefault()
    })




</script>
@endsection
