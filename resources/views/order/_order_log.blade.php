<div class="card">
    <div class="card-header header-border">
        Order Logs
    </div>
    <div class="card-body">
        <div class="price scroll-area-sm" style="margin-top:25px; margin-bottom: 30px; height:350px">
            <div class="scrollbar-container ps--active-y ps">
                <div class="vertical-without-time vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                    @foreach($order_log as $key => $data)
                    <div class="vertical-timeline-item vertical-timeline-element">
                        <div><span class="vertical-timeline-element-icon bounce-in"><i class="badge badge-dot badge-dot-xl badge-warning"> </i></span>
                            <div class="vertical-timeline-element-content bounce-in">
                               {!! $data->content !!}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
