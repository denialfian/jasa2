@if(count($sparepart_details) > 0 || count($item_details) > 0)
<div class="card header-border">
    <div class="card-body">
        <div class="table-responsive">
            <table class="text-nowrap table-lg mb-0 table table-hover">
                <tr>
                    <th>Name</th>
                    <th class="text-right">Unit / Price</th>
                    <th class="text-right">Total</th>
                </tr>
                <tbody>
                    <?php $total_p = 0; ?>
                    @foreach($sparepart_details as $sparepart_detail)
                    <tr>
                        <td>
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">{{ $sparepart_detail->name_sparepart }}</div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="text-right">{{ $sparepart_detail->quantity }} X {{ $sparepart_detail->priceThousandSeparator() }}</td>
                        <td class="text-right">
                            Rp. {{ \App\Helpers\thousanSparator($sparepart_detail->quantity * $sparepart_detail->price) }}
                        </td>
                    </tr>
                    <?php $total_p += $sparepart_detail->quantity * $sparepart_detail->price; ?>
                    @endforeach
                    @foreach($item_details as $item)
                    <tr>
                        <td>
                            <div class="widget-content p-0">
                                <div class="widget-content-wrapper">
                                    <div class="widget-content-left">
                                        <div class="widget-heading">{{ $item->name_product }}</div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="text-right">{{ $item->quantity }} X {{ $item->price }}</td>
                        <td class="text-right">
                            Rp. {{ \App\Helpers\thousanSparator($item->quantity * $item->price) }}
                        </td>
                    </tr>
                    <?php $total_p += $item->quantity * $item->price; ?>
                    @endforeach
                </tbody>
                <tfoot>
                    <td></td>
                    <td></td>
                    <th class="text-right">Rp. {{ \App\Helpers\thousanSparator($total_p) }}</th>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endif