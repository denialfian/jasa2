@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header header-bg">Create Warehouses</div>
        <div class="card-body">
            <form action="" method="post" id="warehouse-store">
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Name Warehouse</label>
                    <div class="col-sm-10">
                        <input name="name" id="examplename" placeholder="Warehouse" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10">
                        <input name="address" placeholder="Address" type="text" class="form-control">
                    </div>
                </div>
                <div class="position-relative row form-check">
                    <div class="col-sm-10 offset-sm-2">
                        <button class="btn btn-success" id="save"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
{{-- @include('admin.script.master._warehouseScript') --}}
<script>
    $('#warehouse-store').submit(function(e) {
        globalCRUD.handleSubmit($(this))
            .storeTo('/warehouses')
            .redirectTo(function(resp) {
                return '/admin/warehouse';
            })
        e.preventDefault();
    });
</script>
@endsection