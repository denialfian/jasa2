@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <button class="btn btn-primary btn-sm add-repair-code">ADD REPAIR CODE</button>
                <!-- <a href="{{ url('/create-additional') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">Add Additional</a>  -->
            </div>
        </div>
        <div class="card-body">
            <table id="table-repair-code" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th style="width: 20px;"></th>
                        <th>Repair Codes</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<form id="form-repair-code">
    <div class="modal fade" id="modal-repair-code" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Repair Code</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">

                    <!-- <div class="form-group"> -->
                        <label for="exampleEmail" class="col-sm-6 col-form-label">Code</label>
                        <!-- <div class="col-sm-9"> -->
                            <input name="name" placeholder="name" id="name" type="text" class="form-control" required>
                        <!-- </div> -->
                    <!-- </div> -->

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script>
    globalCRUD.datatables({
        url: '/repair-code/datatables',
        selector: '#table-repair-code',
        columnsField: ['id', 'name'],
        modalSelector: "#modal-repair-code",
        modalButtonSelector: ".add-repair-code",
        modalFormSelector: "#form-repair-code",
        actionLink: {
            store: function() {
                return "/repair-code";
            },
            update: function(row) {
                return "/repair-code/" + row.id;
            },
            delete: function(row) {
                return "/repair-code/" + row.id;
            },
        },
    })
</script>
@endsection
