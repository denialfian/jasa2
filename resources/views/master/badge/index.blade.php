@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header header-bg">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">

            </div>
        </div>
        <div class="card-body">
            <table id="table-badge" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>

                        <th>Name Badge</th>
                        <th>Color</th>
                        <th>Users</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-badge"><i class="fa fa-plus"></i> Add New</button>
            <!-- <button type="button" class="btn btn-sm btn-danger delete-selected-btn"><i class="fa fa-remove"></i> Delete</button> -->
        </div>
    </div>
</div>
@include('admin.master.badge.modalBadge')
@endsection

@section('script')
<script>

var Badge = {
		table: function(url = '/badge/datatables'){
			return $('#table-badge').DataTable({
	            processing: true,
                serverSide: true,
                select: true,
                dom: 'Bflrtip',
                responsive: true,
                language: {
                    search: '',
                    searchPlaceholder: "Search..."
                },
                oLanguage: {
                    sLengthMenu: "_MENU_",
                },
                dom: "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                ajax: {
                    url: Helper.apiUrl(url),
                    "type": "get"
                },
	            columns: [
	            	{
	                    data: "name",
	                    name: "name",
	                    orderable: false,
	                },
	            	{
                        data: 'color',
                        name: 'color',
                        render: function(data,type,full) {
                            var text = '';
                            if(data == 'primary') {
                                text = 'Blue';
                            } else if(data == 'secondary') {
                                text = 'Grey';
                            } else if(data == 'success') {
                                text = 'Green';
                            } else if(data == 'info') {
                                text = 'Light Blue';
                            } else if(data == 'warning') {
                                text = 'Yellow';
                            } else if(data == 'danger') {
                                text = 'Red';
                            } else if(data == 'focus') {
                                text = 'Black';
                            } else if(data == 'alternate') {
                                text = 'Purple';
                            }
                            return '<div class="mb-2 mr-2 badge badge-pill badge-'+data+'">'+text+'</div>'
                        },
                    },
	            	{
                        data: 'user_emails',
                        name: 'user_emails',
                        render: function(data,type,full) {
                            var content = '';
                            _.forEach(data, function(value) {
                                content += '<span class="badge badge-pill badge-secondary">'+value+'</span>&nbsp;';
                            });
                            return content;
                        },
                    },
                    {
                        data: "id",
	                    name: "id",
	                	render: function(data, type, full) {
		                    return "<button title='Edit Badge' data-id='"+data+"' type='button' class='edit-badge btn btn-warning btn-white btn-sm'><i class='fa fa-pencil-square-o'></i></button> <button data-id='"+data+"' title='Delete Badge' type='button' class='delete-badge btn btn-danger btn-sm'><i class='fa fa-trash'></i></button>";
		                }
                    }
	            ],
	        });
		}
	};

    var data = [{
        id: 'primary',
        text: '<div class="mb-2 mr-2 badge badge-pill badge-primary">Blue</div>',
        title: '<div class="mb-2 mr-2 badge badge-pill badge-primary">Blue</div>'
    },
    {
        id: 'secondary',
        text: '<div class="mb-2 mr-2 badge badge-pill badge-secondary">Grey</div>',
        title: '<div class="mb-2 mr-2 badge badge-pill badge-secondary">Grey</div>'
    },
    {
        id: 'success',
        text: '<div class="mb-2 mr-2 badge badge-pill badge-success">Green</div>',
        title: '<div class="mb-2 mr-2 badge badge-pill badge-success">Green</div>'
    },
    {
        id: 'info',
        text: '<div class="mb-2 mr-2 badge badge-pill badge-info">Light Blue</div>',
        title: '<div class="mb-2 mr-2 badge badge-pill badge-info">Light Blue</div>'
    },
    {
        id: 'warning',
        text: '<div class="mb-2 mr-2 badge badge-pill badge-warning">Yellow</div>',
        title: '<div class="mb-2 mr-2 badge badge-pill badge-warning">Yellow</div>'
    },
    {
        id: 'danger',
        text: '<div class="mb-2 mr-2 badge badge-pill badge-danger">Red</div>',
        title: '<div class="mb-2 mr-2 badge badge-pill badge-danger">Red</div>'
    },
    {
        id: 'focus',
        text: '<div class="mb-2 mr-2 badge badge-pill badge-focus">Black</div>',
        title: '<div class="mb-2 mr-2 badge badge-pill badge-focus">Black</div>'
    },
    {
        id: 'alternate',
        text: '<div class="mb-2 mr-2 badge badge-pill badge-alternate">Purple</div>',
        title: '<div class="mb-2 mr-2 badge badge-pill badge-alternate">Purple</div>'
    }];

    $("#color").select2({
    data: data,
    escapeMarkup: function(markup) {
        return markup;
    }
    })

    globalCRUD.select2Tags(".user_emails", '/user/select2', function(item) {
        return {
            id: item.id,
            text:item.email
        }
    });

    var table_badges = Badge.table();

    // add team
    $(document)
        .on('click', '.add-badge', function(){

            $('#modal-badges').modal('show');
            $('.id').empty();
            $('.name').val('');
            $('.color').val('');
            $('.user_emails').empty();
        })


    // edit badge
    $(document)
        .on('click', '.edit-badge', function(){
            var row = table_badges.row($(this).parents('tr')).data();
            $('#modal-badges').modal('show');
            console.log(row);
            $('.id').val(row.id);
            $('.name').val(row.name);
            $('.color').val(row.color).trigger('change');
            $('.user_emails').empty();
            var selectedData =[];
            row.user_ids.forEach((id, index) => {
                var data = {};
                data.id = id;
                data.text = row.user_emails[index];
                data.selected = true;
                selectedData.push(data);
            });
            console.log(selectedData)
            $('.user_emails').select2({data: selectedData});
            globalCRUD.select2Tags(".user_emails", '/user/select2', function(item) {
                return {
                    id: item.id,
                    text:item.email
                }
            });

        })

    // delete badge
    $(document)
        .on('click', '.delete-badge', function(){
            id = $(this).attr('data-id');
            Helper.confirm(function() {
                Helper.loadingStart();
                Axios.delete('/badge/'+id+'')
                    .then(function(response) {
                        // send notif
                        Helper.successNotif('ok');
                        // reload table
                        $('.id').val('');
                        table_badges.ajax.reload();
                    })
                    .catch(function(error) {
                        console.log(error)
                        Helper.handleErrorResponse(error)
                    });
            })
        })


    // save badges
    $('#form-badge')
        .submit(function(e){
            data = Helper.serializeForm($(this));
            console.log(data);
            Helper.loadingStart();
            if (data.id == '') {

                Axios.post('/badge', data)
                    .then(function(response) {
                        //send notif
                        Helper.successNotif(response.data.msg);
                        // reload table
                        table_badges.ajax.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                        if (error.response.status == 500) {
                            window.location.href = Helper.redirectUrl('/admin/badge/show');
                        }
                    });
            }

            if (data.id != '') {
                Axios.post('/badge/' + data.id, data)
                    .then(function(response) {
                        // send notif
                        Helper.successNotif(response.data.msg);
                        // // reload table
                        table_badges.ajax.reload();
                    })
                    .catch(function(error) {
                        Helper.handleErrorResponse(error)
                    });
            }


            $('#modal-badges').modal('hide');
            e.preventDefault();
        })



</script>
@endsection
