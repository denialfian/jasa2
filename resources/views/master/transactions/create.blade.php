@extends('admin.home')
@section('content')

<style>
    .tgl {
        display: none;
    }
    .custom-control{
        margin: 10px;
    }

    .tgl,
    .tgl:after,
    .tgl:before,
    .tgl *,
    .tgl *:after,
    .tgl *:before,
    .tgl+.tgl-btn {
        box-sizing: border-box;
    }

    .tgl::-moz-selection,
    .tgl:after::-moz-selection,
    .tgl:before::-moz-selection,
    .tgl *::-moz-selection,
    .tgl *:after::-moz-selection,
    .tgl *:before::-moz-selection,
    .tgl+.tgl-btn::-moz-selection {
        background: none;
    }

    .tgl::selection,
    .tgl:after::selection,
    .tgl:before::selection,
    .tgl *::selection,
    .tgl *:after::selection,
    .tgl *:before::selection,
    .tgl+.tgl-btn::selection {
        background: none;
    }

    .tgl+.tgl-btn {
        outline: 0;
        display: block;
        width: 7em;
        height: 2em;
        position: relative;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .tgl+.tgl-btn:after,
    .tgl+.tgl-btn:before {
        position: relative;
        display: block;
        content: "";
        width: 50%;
        height: 100%;
    }

    .tgl+.tgl-btn:after {
        left: 0;
    }

    .tgl+.tgl-btn:before {
        display: none;
    }

    .tgl:checked+.tgl-btn:after {
        left: 50%;
    }


    .tgl-skewed+.tgl-btn {
        overflow: hidden;
        -webkit-transform: skew(-10deg);
        transform: skew(-10deg);
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        -webkit-transition: all .2s ease;
        transition: all .2s ease;
        font-family: sans-serif;
        background: #888;
    }

    .tgl-skewed+.tgl-btn:after,
    .tgl-skewed+.tgl-btn:before {
        -webkit-transform: skew(10deg);
        transform: skew(10deg);
        display: inline-block;
        -webkit-transition: all .2s ease;
        transition: all .2s ease;
        width: 100%;
        text-align: center;
        position: absolute;
        line-height: 2em;
        font-weight: bold;
        color: #fff;
        text-shadow: 0 1px 0 rgba(0, 0, 0, 0.4);
    }

    .tgl-skewed+.tgl-btn:after {
        left: 100%;
        content: attr(data-tg-on);
    }

    .tgl-skewed+.tgl-btn:before {
        left: 0;
        content: attr(data-tg-off);
    }

    .tgl-skewed+.tgl-btn:active {
        background: #888;
    }

    .tgl-skewed+.tgl-btn:active:before {
        left: -10%;
    }

    .tgl-skewed:checked+.tgl-btn {
        background: #1976d2;
    }

    .tgl-skewed:checked+.tgl-btn:before {
        left: -100%;
    }

    .tgl-skewed:checked+.tgl-btn:after {
        left: 0;
    }

    .tgl-skewed:checked+.tgl-btn:active:after {
        left: 10%;
    }
</style>
<style>
    .rounded-profile {
        border-radius: 100% !important;
        overflow: hidden;
        width: 100px;
        height: 100px;
        border: 8px solid rgba(255, 255, 255, 0.7);
    }

    wrap {
        width: 500px;
        margin: 2em auto;
    }

    .clearfix:before,
    .clearfix:after {
        content: " ";
        display: table;
    }

    .clearfix:after {
        clear: both;
    }

    .select2-result-repository {
        padding-top: 4px;
        padding-bottom: 3px;
    }

    .select2-result-repository__avatar {
        float: left;
        width: 60px;
        height: auto;
        margin-right: 10px;
    }

    .select2-result-repository__avatar img {
        width: 100%;
        height: auto;
        border-radius: 50%;
    }

    .select2-result-repository__meta {
        /* margin-left: 70px; */
        margin-left: 0px;
    }

    .select2-result-repository__title {
        color: black;
        font-weight: bold;
        word-wrap: break-word;
        line-height: 1.1;
        margin-bottom: 4px;
    }

    .select2-result-repository__price,
    .select2-result-repository__stargazers {
        margin-right: 1em;
    }

    .select2-result-repository__price,
    .select2-result-repository__stargazers,
    .select2-result-repository__watchers {
        display: inline-block;
        color: rgb(68, 68, 68);
        font-size: 13px;
    }

    .select2-result-repository__description {
        font-size: 13px;
        color: #777;
        margin-top: 4px;
    }

    .select2-results__option--highlighted .select2-result-repository__title {
        color: white;
    }

    .select2-results__option--highlighted .select2-result-repository__price,
    .select2-results__option--highlighted .select2-result-repository__stargazers,
    .select2-results__option--highlighted .select2-result-repository__description,
    .select2-results__option--highlighted .select2-result-repository__watchers {
        color: #c6dcef;
    }
</style>

<style>
    .list-group-item {
        user-select: none;
    }

    .list-group input[type="checkbox"] {
        display: none;
    }

    .list-group input[type="checkbox"]+.list-group-item {
        cursor: pointer;
    }

    .list-group input[type="checkbox"]+.list-group-item:before {
        content: "\2713";
        color: transparent;
        font-weight: bold;
        margin-right: 1em;
    }

    .list-group input[type="checkbox"]:checked+.list-group-item {
        background-color: #0275D8;
        color: #FFF;
    }

    .list-group input[type="checkbox"]:checked+.list-group-item:before {
        color: inherit;
    }

    .list-group input[type="radio"] {
        display: none;
    }

    .list-group input[type="radio"]+.list-group-item {
        cursor: pointer;
    }

    .list-group input[type="radio"]:checked+.list-group-item {
        background-color: #0275D8;
        color: #FFF;
    }

    .list-group input[type="radio"]:checked+.list-group-item:before {
        color: inherit;
    }

    .scroll-area-sm {
        /* box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2); */
        margin-top: 10px;
        margin-bottom: 10px;
        padding: 10px;
    }

    .user-profile {
        box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2);
        margin: auto;
        height: 8em;
        background: #fff;
        border-radius: .3em;
        margin-bottom: 20px;
    }

    .user-profile .username {
        margin: auto;
        margin-top: -4.40em;
        margin-left: 5.80em;
        color: #658585;
        font-size: 1.53em;
        font-family: "Coda", sans-serif;
        font-weight: bold;
    }

    .user-profile .bio {
        margin: auto;
        display: inline-block;
        margin-left: 10.43em;
        color: #e76043;
        font-size: .87em;
        font-family: "varela round", sans-serif;
    }

    .user-profile>.description {
        margin: auto;
        margin-top: 1.35em;
        margin-right: 4.43em;
        width: 14em;
        color: #c0c5c5;
        font-size: .87em;
        font-family: "varela round", sans-serif;
    }

    .user-profile>img.avatar {
        padding: .7em;
        margin-left: .3em;
        margin-top: .3em;
        height: 6.23em;
        width: 6.23em;
        border-radius: 18em;
    }
</style>


<form id="order-form" style="display: contents;">

    <!-- header -->
    <div class="col-md-12">
        <div class="card">
            <div class="card-header header-bg">
                Admin Create Transaction Order
            </div>
        </div>
    </div>

    <!-- customer areas -->
    <div style="margin-top: 10px;" class="{{ $ticket == null ? 'col-md-12' : 'col-md-6' }}">
        <div class="card">
            <div class="card-body">
                <label>SELECT CUSTOMER</label>
                <div class="form-group">
                    <select class="select-customer" style="width:100%" name="user_id" required>
                        @if($ticket != null)
                        <option value="{{ $ticket->user->id }}" selected="selected">{{ $ticket->user->email }} / {{ $ticket->user->name }}</option>
                        @else
                        <option value="" selected="selected">Search by Email / Name</option>
                        @endif
                    </select>
                </div>
                <div id="detail_customer_text" style="box-shadow: 0 5px 5px rgba(0, 0, 0, 0.2); padding: 10px;">
                    @if($ticket != null)
                    <input type="hidden" id="ticket_id" name="ticket_id" value="{{ $ticket->id }}" />
                    <label>EMAIL</label>
                    <div class="form-group">
                        <p>{{ $ticket->user->email }}</p>
                    </div>
                    <label>PHONE</label>
                    <div class="form-group">
                        <p>{{ $ticket->user->phone }}</p>
                    </div>
                    <label>SELECT CUSTOMER ADDRESS</label>
                    <div>
                        <div class="list-group">
                            @foreach($ticket->user->addresses as $addr)
                            <input data-type_address="{{ $addr->types->name }}" data-detail_address="{{ $addr->address }}" data-id="{{ $addr->id }}" class="radio-alamat" type="radio" name="address_id" value="{{ $addr->id }}" id="Radio-{{ $addr->id }}" {{ $addr->is_main == 1 ? 'checked' : '' }} />
                            <label class="list-group-item" for="Radio-{{ $addr->id }}">
                                {{ $addr->types->name }}
                                <p>{{ $addr->address }}</p>
                            </label>
                            @endforeach
                        </div>
                    </div>
                    @endif
                </div>
                <hr />
                <label>BILL ADDRESS (Optional)</label>
                <div class="form-group">
                    <textarea class="form-control" name="bill_to" id="bill_to"></textarea>
                </div>
                <label>ORDER NOTE (Optional)</label>
                <div class="form-group">
                    <textarea class="form-control" name="note" id="note"></textarea>
                </div>
            </div>
        </div>
    </div>

    @if($ticket != null)
    <div style="margin-top: 10px;" class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="widget-content p-0">
                    <div class="widget-content-wrapper">
                        <div class="widget-content-left">
                            <div class="widget-subheading">
                                <h2><b>#{{ $ticket->ticket_no }}</b></h2>
                                <h4>{{ $ticket->subject }}</h4>
                                {{ $ticket->desc }}
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <a href='{{ url("/admin/tickets/" . $ticket->id) }}' class='btn btn-search btn-sm btn-danger'><i class='fa fa-arrow-right'></i> Detail</a>
            </div>
        </div>
    </div>
    @endif

    <!-- select service -->
    <div class="col-md-6" style="margin-top: 20px;">
        <div class="card header-border">
            <div class="card-body">
                <label>SERVICE</label>
                <div class="form-group">
                    <select id="select-services" name="ms_service_id" class="form-control" style="width:100%;" required>
                        <option value="" selected="selected">--Select--</option>
                    </select>
                </div>

                <label>SERVICE CATEGORY</label>
                <div class="form-group">
                    <select id="select-service-category" name="symptom_id" class="form-control" style="width:100%;" required>
                        <option value="" selected="selected">--Select--</option>
                    </select>
                </div>

                <label>SERVICE TYPE</label>
                <div class="form-group">
                    <select id="select-service-type" name="service_type_id" class="form-control" style="width:100%;" required>
                        <option value="" selected="selected">--Select--</option>
                    </select>
                </div>

                <label>PRODUCT GROUP</label>
                <div class="form-group">
                    <select id="select-product_group" name="product_group_id" class="form-control" style="width:100%;" required>
                        <option value="" selected="selected">--Select--</option>
                    </select>
                </div>

                <label>SYMPTOMS DETAIL</label>
                <div class="form-group">
                    <textarea class="form-control" name="symptom_detail" id="symptom_detail" required></textarea>
                </div>

                <label class="" for="">
                    <b>Upload file information media about the description of symptoms <span class="text-danger">(max 4 files image/video)</span></b>.
                </label>
                <br />
                <label class="text-danger" for="">
                    <b>Video Upload max 10 MB.</b>
                </label>

                <!-- Example 2 -->
                <input type="file" name="files[]" id="filer_input2" multiple="multiple">
                <!-- end of Example 2 -->
            </div>
        </div>
    </div>

    <!-- teknisi select -->
    <div class="col-md-6" style="margin-top: 20px;">
        <div class="card header-border">
            <div class="card-body" id="teknisi-button-cari">
                <button class="btn btn-danger btn-find-teknisi" type='button'>
                    <i class="fa fa-search"></i> Find Technicians
                </button>
            </div>
            <div class="card-body" id="teknisi-area">
                <label>SELECT TECHNICIAN</label>
                <div class="form-group">
                    <select id="select-technicians" class="form-control" name="technician_id" style="width:100%" required>
                        <option value="" selected="selected">--Select--</option>
                    </select>
                </div>

                <div style="padding: 10px;">
                    <div id="detail_teknisi_text"></div>
                </div>

                <div id="available_schedule">
                    <div class="position-relative form-group">
                        <label class="label-header" for="">
                            AVAILABLE SCHEDULE
                        </label>
                        <input class="form-control datetimepicker" name="schedule" type="text" id="schedule" readonly />
                    </div>
                    <div class="row" id="display_hours_available" style="padding:20px;"></div>
                </div>

                <div style="padding:10px">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Start Working at </label>
                            <div class="form-group">
                                <h4 id="start_at"></h4>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <label>End Hours at </label>
                            <div class="form-group">
                                <h4 id="end_at"></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <label>What is the estimated work time?</label>
                    <div class="row">
                        <div class="input-group col-md-6">
                            <select class="form-control" name="estimation_hours" id="estimation_hours" required style="width:100%">
                                <option selected="" value=0>Select Duration</option>
                                <option value=1>1</option>
                                <option value=2>2</option>
                                <option value=3>3</option>
                                <option value=4>4</option>
                                <option value=5>5</option>
                            </select>
                        </div>
                        <div class="input-group-append col-md-6">
                            <span class="input-group-text">Hours</span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- sparepart -->
    <div class="col-md-12" style="margin-top: 20px;">
        <div class="card header-border">
            <div class="card-header">
                Add additional sparepart from
                <div class="btn-actions-pane-right">
                    <div class="nav">
                        <a data-toggle="tab" href="#tab-eg2-0" class="btn-tab btn-wide active btn btn-outline-success btn-md" data-desc="sparepaer-inventory">My Inventory</a>
                        <a data-toggle="tab" href="#tab-eg2-1" class="btn-tab btn-wide mr-1 ml-1  btn btn-outline-success btn-md" data-desc="sparepaer-company">Company </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <input type="hidden" name="type_part" id="type_part" value="my-inventory">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-eg2-0" role="tabpanel">
                        <div class="dynamic_sparepart">
                            <div class="position-relative row form-group">
                                <div class="col-sm-6">
                                    Name
                                </div>
                                <div class="col-sm-2">
                                    Price
                                </div>
                                <div class="col-sm-2">
                                    Unit
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <div class="col-sm-6">
                                    <input type="text" placeholder="name" name="teknisi_sparepart_name[]" data-uniq="xxx" class="form-control teknisi_sparepart_name_input-xxx" />
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" placeholder="price" name="teknisi_sparepart_price[]" data-uniq="xxx" class="form-control teknisi_sparepart_price_i teknisi_sparepart_price_input-xxx" />
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" placeholder="unit" name="teknisi_sparepart_qty[]" data-uniq="xxx" class="form-control teknisi_sparepart_qty_i teknisi_sparepart_qty_input-xxx" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-eg2-1" role="tabpanel">
                        <div class="dynamic_sparepart_company">
                            <div class="position-relative row form-group">
                                <div class="col-sm-3">
                                    Warehouse
                                </div>
                                <div class="col-sm-3">
                                    Name
                                </div>
                                <div class="col-sm-2">
                                    Price
                                </div>
                                <div class="col-sm-2">
                                    Unit
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <div class="col-sm-3">
                                    <select class="form-control select-warehouse select-warehouse-xxx" name="warehouse_id[]" data-uniq="xxx" style="width:100%">
                                        <option value="" selected="selected">--Pilih--</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control select-inventory select-inventory-xxx" name="inventory_id[]" data-uniq="xxx" style="width:100%">
                                        <option value="" selected="selected">--Pilih--</option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" data-uniq="xxx" class="form-control inventory-price-xxx inventory-price" name="inventory_price[]" disabled />
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" data-uniq="xxx" placeholder="unit" name="inventory_qty[]" class="form-control inventory-unit inventory-unit-xxx" value="0" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="margin-top: 20px;">
        <div class="card header-border">
            <div class="card-body">
                <table class="table table-user-information">
                    <tbody class="service-subtotal-area"></tbody>
                </table>
                <table class="table table-user-information">
                    <tbody id="extra-cost-area"></tbody>
                </table>
                <table class="table table-user-information">
                    <tbody>
                        <th>GRAND TOTAL</th>
                        <th class="text-right">Rp.<b class="grand-total">0</b></th>
                        <input type="hidden" class="get_total" name="grand_total">
                    </tbody>
                </table>
            </div>
            <div class="card-body">
                <label>Payment Type</label>
                <br />
                <input name="status" class="tgl tgl-skewed" id="cb3" type="checkbox" checked />
                <label class="tgl-btn" data-tg-off="OFFLINE" data-tg-on="ONLINE" for="cb3"></label>
                <hr>
                <label>Notification </label>
                <br />
                <input type="checkbox" value="1" name="chat_notif" checked> Chat
                <input type="checkbox" value="1" name="email_notif" checked> Email
            </div>
            <div class="card-footer">
                <button type="submit" class="accept-modal btn btn-sm btn-primary"><i class="fa fa-plus"></i> CREATE ORDER</button>
            </div>
        </div>
    </div>
</form>

@endsection

@section('script')
<style>
    .custom-control-label-2{
        margin: 0;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.full.min.js">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js">
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.min.css" rel="stylesheet" />

<link rel="stylesheet" href="{{ asset('adminAssets/css/jquery.filer.css') }}">
<link rel="stylesheet" href="{{ asset('adminAssets/css/themes/jquery.filer-dragdropbox-theme.css') }}">
<script type="text/javascript" src="{{ asset('adminAssets/js/filer/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('adminAssets/js/filer/jquery.filer.min.js') }}"></script>

<script>
    var TransactionApp = {
        initialize: function() {
            Helper.onlyNumberInput('.teknisi_sparepart_qty_i')
                .onlyNumberInput('.teknisi_sparepart_price_i')
                .onlyNumberInput('.inventory-unit');

            $('#teknisi-area').hide();
            $('#teknisi-button-cari').show();
            $('#grand-total-text').text(0);

            $('#estimation_hours').prop('disabled', true);
            Helper.dateScheduleJob('#schedule');
            Helper.date('#schedule', 1, 'd M Y H:m');
            Helper.hideSidebar();

            // select2
            this.serviceChangeEvent();
            CustomerSelect.initialize();
            inventoryCompanySelect.initialize();

            this.disabledOnScrollInputNumber();
        },

        disabledOnScrollInputNumber: function() {
            $('form').on('focus', 'input[type=number]', function(e) {
                $(this).on('wheel.disableScroll', function(e) {
                    e.preventDefault()
                })
            })
            $('form').on('blur', 'input[type=number]', function(e) {
                $(this).off('wheel.disableScroll')
            })
        },
        serviceChangeEvent: function() {
            globalCRUD.select2("#select-services", '/services/select2');

            $('#select-service-category').prop('disabled', true);
            $('#select-service-type').prop('disabled', true);
            $('#select-product_group').prop('disabled', true);

            // change service
            $(document).on('change', '#select-services', function() {
                TransactionApp.handleChangeServiceSelect($(this).val());
            });

            $(document).on('change', '#estimation_hours', function() {
                jam_mulai = $('#start_at').html();
                if (jam_mulai != '') {
                    var returned_endate = moment($('#start_at').html()).add($(this).val(), 'hours');
                    console.log(returned_endate.format('H'));
                    console.log(jam_teknisi)
                    $('.accept-btn').attr('disabled', false);
                    $('#end_at').html(returned_endate.format('HH:00'));
                    if ($.inArray(parseInt(returned_endate.format('H')), jam_teknisi) !== -1) {
                        $('.accept-btn').attr('disabled', true);
                        return Helper.warningNotif("Cannot pick estimate hours. Cause at " + $('#end_at').html() + " in the technician schedule !");
                    }
                }
            });

            // change symtom
            $(document).on('change', '#select-service-category', function() {
                TransactionApp.handleChangeServiceCategorySelect($(this).val());
            });

            // change product_group
            $(document).on('change', '#select-service-type', function() {
                TransactionApp.handleChangeServiceTypeSelect($(this).val());
            });
        },

        handleChangeServiceSelect: function(service_id) {
            $('#select-service-category').empty();
            $('#select-service-type').empty();
            $('#select-product_group').empty();

            $('#select-service-category').prop('disabled', false);
            $('#select-service-type').prop('disabled', true);
            $('#select-product_group').prop('disabled', true);
            globalCRUD.select2("#select-service-category", '/admin/symptom/select2/' + service_id);
            this.cleanTemplate();
        },

        handleChangeServiceCategorySelect: function(symptom_id) {
            $('#select-service-type').empty();
            $('#select-product_group').empty();

            $('#select-service-type').prop('disabled', false);
            $('#select-product_group').prop('disabled', true);
            globalCRUD.select2("#select-service-type", '/service_type/select2_symptom/' + symptom_id);
            this.cleanTemplate();
        },

        handleChangeServiceTypeSelect: function(type_id) {
            $('#select-product_group').empty();
            $('#select-product_group').prop('disabled', false);
            globalCRUD.select2("#select-product_group", '/product_groups/select2/' + type_id);
            this.cleanTemplate();
        },

        findTechnicianValidate: function() {
            if ($('#select-services').val() == '' || $('#select-services').val() == null) {
                return {
                    valid: false,
                    msg: 'service required'
                }
            }

            if ($('#select-service-category').val() == '' || $('#select-service-category').val() == null) {
                return {
                    valid: false,
                    msg: 'service category required'
                }
            }

            if ($('#select-service-type').val() == '' || $('#select-service-type').val() == null) {
                return {
                    valid: false,
                    msg: 'service type required'
                }
            }

            if ($('#select-product_group').val() == '' || $('#select-product_group').val() == null) {
                return {
                    valid: false,
                    msg: 'product group required'
                }
            }

            return {
                valid: true,
                msg: 'success'
            }
        },

        templateServiceSubtotal: function(teknisi) {
            var price_service = '';
            var total = 0;
            price_service += (`
                <tr>
                    <th style="text-decoration: underline;">SERVICE COST</th>
                </tr>
            `);

            service_type_id = $('#select-service-type').val();
            price = _.find(teknisi.data.price_services, function(row) {
                return row.ms_services_types_id == service_type_id;
            });

            if (price) {
                unit = 1;
                total += parseInt(price.value) * parseInt(unit);
                price_service += (`
                    <tr>
                        <th>${price.service_type.name}</th>
                        <th class="text-right">Rp. ${Helper.thousandsSeparators(price.value)} <input type="hidden" value="${price.id}" name="price_service_id"></th>
                    </tr>
                `);
            }
            price_service += (`
                <tr>
                    <th>TOTAL SERVICE COST</th>
                    <th class="total-price-service-text text-right">Rp. ${Helper.thousandsSeparators(total)} <input type="hidden" value="${total}" name="total_service_cost" /></th>
                </tr>
            `);
            return price_service;
        },

        hitungGrandTotal: function() {
            service_cost = $('input[name="total_service_cost"]');
            extra_cost = $('input[name="total_extra_cost"]');

            service_cost_val = 0;
            extra_cost_val = 0;
            if (service_cost.length > 0) {
                service_cost_val = parseInt(service_cost.val());
            }

            if (extra_cost.length > 0) {
                extra_cost_val = parseInt(extra_cost.val());
            }

            grand_total = service_cost_val + extra_cost_val;

            // geand total
            $('.grand-total').text(Helper.toCurrency(grand_total));
            $('.get_total').val(grand_total);
        },

        hitungTotalPartMyInventory: function() {
            template = '';
            template += '<tr>';
            template += '<th style="text-decoration: underline;">EXTRA COST</th>';
            template += '</tr>';

            grand_total = 0;
            total_part = 0;
            var part = {};

            $('input[name="teknisi_sparepart_qty[]"]').each(function() {
                var uniq = $(this).attr('data-uniq');
                var qty = $('.teknisi_sparepart_qty_input-' + uniq).val() == '' ? 0 : parseInt($('.teknisi_sparepart_qty_input-' + uniq).val());
                var price = $('.teknisi_sparepart_price_input-' + uniq).val() == '' ? 0 : parseInt($('.teknisi_sparepart_price_input-' + uniq).val());
                var name = $('.teknisi_sparepart_name_input-' + uniq).val();
                if (qty > 0 && price > 0) {
                    part.name = name;
                    part.price = price;
                    part.qty = qty;
                    total_part += parseInt(part.qty) * parseInt(part.price);

                    template += '<tr>';
                    template += '<th>' + name + ' ( X ' + qty + ')</th>';
                    template += '<th class="text-right">Rp. ' + Helper.thousandsSeparators(price * qty) + '</th>';
                    template += '</tr>';
                }
            })

            template += '<tr>';
            template += '<th>TOTAL EXTRA COST</th>';
            template += '<th class="text-right">Rp. ' + Helper.thousandsSeparators(total_part) + ' <input type="hidden" value="' + total_part + '"  name="total_extra_cost" class="total_extra_cost" /></th>';
            template += '</tr>';


            $('#extra-cost-area').html(template);
            this.hitungGrandTotal();
        },

        templateNewInputPartMyInventory: function(xx) {
            $('.dynamic_sparepart').append((`
                <div class="position-relative row form-group" id="my_part_row_${xx}">
                    <div class="col-sm-6">
                        <input type="text" placeholder="name" name="teknisi_sparepart_name[]" data-uniq="${xx}" class="form-control teknisi_sparepart_name_input-${xx}" />
                    </div>
                    <div class="col-sm-2">
                        <input type="text" placeholder="price" name="teknisi_sparepart_price[]" data-uniq="${xx}" class="teknisi_sparepart_price_i teknisi_sparepart_price_input-${xx} form-control" />
                    </div>
                    <div class="col-sm-2">
                        <input type="text" placeholder="unit" name="teknisi_sparepart_qty[]" data-uniq="${xx}" class="teknisi_sparepart_qty_i teknisi_sparepart_qty_input-${xx} form-control" />
                    </div>
                    <div class="col-sm-2" style="padding-left:0px;">
                        <button type="button" data-uniq="${xx}" class="btn-transition btn btn-danger btn_remove_my_part"><i class="fa fa-close"></i></button>
                    </div>
                </div>
            `));

            Helper.onlyNumberInput('.teknisi_sparepart_qty_i')
                .onlyNumberInput('.teknisi_sparepart_price_i');
        },

        hitungTotalPartMyCompany: function() {
            template = '';
            template += '<tr>';
            template += '<th style="text-decoration: underline;">EXTRA COST</th>';
            template += '</tr>';
            grand_total = 0;
            $('.select-inventory').each(function() {
                uniq = $(this).attr('data-uniq');
                inventory_id = $(this).val();
                inventory_qty = $('.inventory-unit-' + uniq).val();

                if (inventory_id != '') {
                    inventories = $('.select-inventory-' + uniq).select2("data");
                    inventory = _.find(inventories, function(inv) {
                        return inv.id == inventory_id;
                    });
                    if (inventory) {
                        grand_total += parseInt(inventory_qty) * parseInt(inventory.data.cogs_value);

                        template += '<tr>';
                        template += '<th>' + inventory.data.item_name + ' ( X ' + Helper.thousandsSeparators(inventory_qty) + ' ' + inventory.data.unit_type.unit_name + ')</th>';
                        template += '<th class="text-right">Rp. ' + Helper.thousandsSeparators(inventory.data.cogs_value) + '</th>';
                        template += "<input type='hidden' name='name_sparepart_inventory[]' class='name_sparepart_inventory' value=" + inventory.data.item_name + ">";
                        template += "<input type='hidden' name='price_sparepart_inventory[]' class='price_sparepart_inventory' value=" + inventory.data.cogs_value + ">";
                        template += '</tr>';
                    }
                }
            })

            template += '<tr>';
            template += '<th>TOTAL EXTRA COST</th>';
            template += '<th class="text-right">Rp. ' + Helper.thousandsSeparators(grand_total) + ' <input type="hidden" value="' + grand_total + '" name="total_extra_cost" class="total_extra_cost" /></th>';
            template += '</tr>';
            $('#extra-cost-area').html(template);
            this.hitungGrandTotal();
        },

        templateNewInputPartMyCompany: function(xx) {
            $('.dynamic_sparepart_company').append((`
                <div class="position-relative row form-group" id="my_company_row_${xx}">
                    <div class="col-sm-3">
                        <select class="form-control select-warehouse select-warehouse-${xx}" name="warehouse_id[]" data-uniq="${xx}" style="width:100%">
                            <option value="" selected="selected">--Pilih--</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control select-inventory select-inventory-${xx}" id="inventory_id_${xx}" data-uniq="${xx}" name="inventory_id[]" style="width:100%">
                            <option value="" selected="selected">--Pilih--</option>'+
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" data-uniq="${xx}" class="form-control inventory-price-${xx}" name="inventory_price[]" disabled/>
                    </div>
                    <div class="col-sm-2">
                        <input type="text" id="qty_inventory_${xx}" data-uniq="${xx}" placeholder="unit" name="inventory_qty[]" value="0" class="form-control inventory-unit inventory-unit-${xx}">
                    </div>
                    <div class="col-sm-2" style="padding-left:0px;">
                        <button name="remove" data-uniq="${xx}" class="btn-transition btn btn-sm btn-danger btn_remove_my_company"><i class="fa fa-close"></i></button>
                    </div>
                </div>
            `));

            Helper.onlyNumberInput('.inventory-unit');
        },

        cleanTemplate: function() {
            $('#estimation_hours').prop('disabled', true);
            $('#estimation_hours').val('').change();
            $('#start_at').html('');
            $('#end_at').html('');

            $('#teknisi-area').hide();
            $('#teknisi-button-cari').show();
            // clean teknisi
            $('#detail_teknisi_text').html('');

            // service cost
            $('.service-subtotal-area').html('');
            $('input[name="total_service_cost"]').val(0);

            // teknisi select modal
            $('#select-technicians').empty();
            $('#display_hours_available').html('');
            $('#schedule').val('');
            // leacn the shit
            tab = $('#type_part').val();
            if (tab == 'my-inventory') {
                TransactionApp.hitungTotalPartMyInventory();
            } else {
                TransactionApp.hitungTotalPartMyCompany();
            }
        },

        validationOnSubmit: function() {
            if ($('.select-customer').val() == '') {
                return {
                    valid: false,
                    msg: "Select Customer must be Required !"
                }
            }

            if ($('#select-services').val() == '') {
                return {
                    valid: false,
                    msg: "Select Service must be Required !"
                }
            }

            if ($('#schedule').val() == '') {
                return {
                    valid: false,
                    msg: "Select Schedule must be Required !"
                }
            }

            if (!$('.hours').is(':checked')) {
                return {
                    valid: false,
                    msg: "Hours must be required !"
                }
            }
        }
    }

    var CustomerSelect = {
        initialize: function() {
            $(".select-customer").select2({
                // minimumInputLength: 1,
                ajax: {
                    url: Helper.apiUrl('/user/teknisi-select2'),
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: $.map(data.data, CustomerSelect.render(data))
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateSelection: function(data) {
                    return data.name || data.text;
                }
            });

            $(document).on('change', '.select-customer', function() {
                CustomerSelect.handleOnChange($(this).val());
            });

            $(document).on('change', '.address_id', function() {
                var value = $(this).val();
                customer = CustomerSelect.find($('.select-customer').val());
                var addres = _.find(customer.data.addresses, function(row) {
                    return row.id == value;
                });
                $('#bill_to').val(addres.address);
            });
        },

        data: function() {
            return $('.select-customer').select2("data");
        },

        find: function(customer_id) {
            return _.find(CustomerSelect.data(), function(data) {
                return data.id == customer_id;
            });
        },

        render: function(data) {
            return function(data) {
                var phone = data.phone != null ? data.phone : '-';
                images = "<img src='" + data.avatar + "' />";
                return {
                    id: data.id,
                    text: "<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__avatar'>" + images + "</div>" +
                        "<div class='select2-result-repository__meta'>" +
                        "<div class='select2-result-repository__title'><b>" + data.name + "</b></div>" +
                        "<div class='select2-result-repository__description'>Phone : <b>" + phone + "</b></div>" +
                        "<div class='select2-result-repository__description'>Email : <b>" + data.email + "</b></div>" +
                        "</div></div>",
                    name: data.name + ' / ' + data.email,
                    data: data
                };
            };
        },

        handleOnChange: function(customer_id) {
            var customer = CustomerSelect.find(customer_id);
            var addres = _.find(customer.data.addresses, function(row) {
                return row.is_main == 1;
            });
            if (addres) {
                addres = addres;
            } else {
                addres = customer.data.addresses[0];
            }
            $('#bill_to').val(addres.address);
            TransactionApp.cleanTemplate();
            $('#detail_customer_text').html(CustomerSelect.templateDetail(customer));
        },

        templateDetail: function(customer) {
            var customer_wallet = customer.data.ewallet != null ? customer.data.ewallet.nominal : 0;
            var html_addresses = '';
            var addr_main = customer.data.address;
            var first = true;
            _.each(customer.data.addresses, function(addr) {
                if (addr_main == null) {
                    if (first == true) {
                        checked = 'checked';
                        first = false;
                    }
                } else {
                    checked = addr.is_main == 1 ? 'checked' : '';
                }
                html_addresses += (`
                    <input type="radio" class="address_id" name="address_id" value="${addr.id}" id="Radio-${addr.id}" ${checked}/>

                    <label class="list-group-item" for="Radio-${addr.id}">
                        ${addr.types.name}
                        <p>${addr.address}</p>
                    </label>
                `);
            });

            phone_cus = customer.data.phone == null ? '-' : customer.data.phone;
            wallet = customer.data.ewallet == null ? 0 : Helper.thousandsSeparators(customer.data.ewallet.nominal);
            var html_info = (`
                <label>WALLET</label>
                <div class="form-group">
                    <p>${wallet}</p>
                </div>
                <label>EMAIL</label>
                <div class="form-group">
                    <p>${customer.data.email}</p>
                </div>
                <label>PHONE</label>
                <div class="form-group">
                    <p>${phone_cus}</p>
                </div>
                <label>SELECT CUSTOMER ADDRESS</label>
                <div>
                    <div class="list-group">
                        ${html_addresses}
                    </div>
                </div>
                <input type="hidden" name="customer_ewallet" id="customer_ewallet" value="${customer_wallet}"/>
            `);

            return html_info;
        }
    }

    var TeknisiSelect = {
        initialize: function() {
            $('#select-technicians').select2({
                // minimumInputLength: 1,
                ajax: {
                    url: Helper.apiUrl('/admin/order/find_technicians'),
                    dataType: 'json',
                    delay: 250,
                    type: 'post',
                    data: function(params) {
                        return {
                            product_group_id:$('#select-product_group').val(),
                            service_type_id: [$('#select-service-type').val()],
                            product_group_id: $('#select-product_group').val(),
                            customer_id: $('.select-customer').val(),
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: $.map(data, TeknisiSelect.render(data))
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateSelection: function(data) {
                    return data.name;
                }
            });

            // change teknisi
            $(document).on('change', '#select-technicians', function() {
                value = $(this).val();
                if (value) {
                    $('#available_schedule').show();
                    TeknisiSelect.handleOnChange(value);
                }
            });
        },

        data: function() {
            return $('#select-technicians').select2("data");
        },

        find: function(teknisi_id) {
            return _.find(TeknisiSelect.data(), function(data) {
                return data.id == teknisi_id;
            });
        },

        render: function(data) {
            return function(data) {
                var phone = data.user.phone != null ? data.user.phone : '-';
                var images = "<img src='" + data.user.avatar + "' />";
                var price_service = '';
                service_type_id = $('#select-service-type').val();

                price = _.find(data.price_services, function(row) {
                    return row.ms_services_types_id == service_type_id;
                });

                if (price) {
                    price_service += "<div class='select2-result-repository__description'><b>" + price.service_type.name + "</b> (" + Helper.toCurrency(price.value) + ")</div>";
                }

                kota = (data.user.address.city) ? data.user.address.city.name : "-" ;

                return {
                    id: data.id,
                    text: "<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__avatar'>" + images + "</div>" +
                        "<div class='select2-result-repository__meta'>" +
                        "<div class='select2-result-repository__title'><b>" + data.user.name + "</b></div>" +
                        "<div class='select2-result-repository__description'>City : <b>" + kota + "</b></div>" +
                        "<div class='select2-result-repository__description'>Phone : <b>" + data.user.phone + "</b></div>" +
                        "<div class='select2-result-repository__description'>Email : <b>" + data.user.email + "</b></div>" +
                        price_service +
                        "</div></div>",
                    name: data.user.name + ' / ' + data.user.email,
                    data: data
                };
            };
        },

        handleOnChange: function(teknisi_id) {
            teknisi = TeknisiSelect.find(teknisi_id)
            html = TeknisiSelect.templateDetail(teknisi);
            $('#detail_teknisi_text').html(html);

            $('.service-subtotal-area').html(TransactionApp.templateServiceSubtotal(teknisi));

            TransactionApp.hitungGrandTotal();
        },

        templateDetail: function(teknisi) {
            if (teknisi.data.jumlah_rating == 0) {
                star = Helper.ratingStar(0);
            }else{
                star = Helper.ratingStar(
                    teknisi.data.total_rating / teknisi.data.jumlah_rating
                );
            }
            
            service_type_id = $('#select-service-type').val();

            price = _.find(teknisi.data.price_services, function(row) {
                return row.ms_services_types_id == service_type_id;
            });

            console.log(price);
            
            price_service = '';
            if (price) {
                price_service += "<div><b>" + price.service_type.name + "</b> (" + Helper.toCurrency(price.value) + ")</div>";
            }

            kota = (teknisi.data.user.address.city) ? teknisi.data.user.address.city.name : "-" ;

            var html = (`
                <div class="user-profile">
                    <img class="avatar" src="${teknisi.data.user.avatar}" alt="Ash" />
                    <div class="username">${teknisi.data.user.name}&nbsp; `+(teknisi.data.user.badge != null ? '<div class="mb-2 mr-2 badge badge-pill badge-'+teknisi.data.user.badge.color+'" style="font-size:12px;">'+teknisi.data.user.badge.name+'</div>' : '')+`</div>
                    <div class="bio">
                        ${teknisi.data.user.email}
                        <br />
                        ${kota}
                        <br />
                        ${price_service}
                        ${star} (${teknisi.data.total_review}) Review
                    </div>
                </div>
            `);
            return html;
        }
    }

    var inventoryCompanySelect = {
        initialize: function() {
            $('.select-inventory').prop('disabled', true);
            $('.inventory-unit').prop('disabled', true);
            globalCRUD.select2('.select-warehouse', '/warehouses/select2');

            $(document).on('change', '.select-inventory', function() {
                inventoryCompanySelect.handleOnChange($(this));
            });

            $(document).on('change', '.select-warehouse', function() {
                uniq = $(this).attr('data-uniq')
                inventoryCompanySelect.make('.select-inventory-' + uniq, $(this).val());
                $('.select-inventory-' + uniq).prop('disabled', false);
            });
        },

        make: function(selector, warehouse_id) {
            inventory_id = [];

            $('.select-inventory').each(function() {
                if ($(this).val()) {
                    inventory_id.push($(this).val());
                }
            }).get();

            $(selector).html('').select2({
                ajax: {
                    url: Helper.apiUrl('/teknisi/inventory-company/select2'),
                    dataType: 'json',
                    delay: 250,
                    type: 'post',
                    data: function(params) {
                        dataParams = {};
                        if (inventory_id.length > 0) {
                            dataParams.inventory_id = inventory_id;
                        }
                        dataParams.warehouse_id = warehouse_id;
                        dataParams.q = params.term;
                        dataParams.page = params.page;

                        return dataParams;
                        return {
                            inventory_id: inventory_id,
                            warehouse_id: warehouse_id,
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function(data, page) {
                        return {
                            results: $.map(data.data, inventoryCompanySelect.render(data))
                        };
                    },
                    cache: true
                },
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateSelection: function(data) {
                    return data.name || data.text;
                }
            });
        },

        data: function(selector = null) {
            selector = selector == null ? '.select-inventory' : selector;
            return $(selector).select2("data");
        },

        find: function(inv_id, selector = null) {
            return _.find(inventoryCompanySelect.data(selector), function(data) {
                return data.id == inv_id;
            });
        },

        render: function(data) {
            return function(data) {
                console.log(data);
                if (data.product) {
                    var images = "<img src='" + data.product.main_image + "' />";
                    return {
                        id: data.id,
                        text: "<div class='select2-result-repository clearfix'>" +
                            // "<div class='select2-result-repository__avatar'>" + images + "</div>" +
                            "<div class='select2-result-repository__meta'>" +
                            "<div class='select2-result-repository__title'>" + data.item_name + "</div>" +
                            "<div class='select2-result-repository__description'>" + data.warehouse.name + "</div>" +
                            "<div class='select2-result-repository__description'>" + Helper.thousandsSeparators(data.stock_available) + " Stock</div>" +
                            "<div class='select2-result-repository__statistics'>" +
                            "<div class='select2-result-repository__forks'><i class='fa fa-fa-money'></i><b> Rp. " + Helper.thousandsSeparators(data.cogs_value) + "</b></div>" +
                            "</div></div>",
                        name: data.item_name + "(Qty: " + Helper.thousandsSeparators(data.stock_available) + ")",
                        data: data
                    };
                }

                return {
                    id: '',
                    text: "",
                    name: '',
                    data: []
                };
            };
        },

        handleOnChange: function($el) {
            uniq = $el.attr('data-uniq');
            inventory = this.find($el.val(), $('.select-inventory-' + uniq));
            $('.inventory-price-' + uniq).val(Helper.thousandsSeparators(inventory.data.cogs_value));
            $('.inventory-unit-' + uniq).prop('disabled', false);
            TransactionApp.hitungTotalPartMyCompany();
        },

        templateDetail: function(teknisi) {

        }
    }

    TransactionApp.initialize();
</script>

<script>
    var jam_teknisi = [];
    var teknisi_id = null;
    Helper.dateScheduleJob('#schedule');

    $(document).on('change', "input[name='hours']", function() {
        if (this.checked) {
            $('#estimation_hours').prop('disabled', false);
            $('#start_at').html(moment($('#schedule').val() + ' ' + $("input[name='hours']:checked").val()).format('DD MMMM YYYY HH:mm'));
        }
    })

    $(document).on('change', '#estimation_hours', function() {
        if ($(this).val()) {
            var returned_endate = moment($('#start_at').html()).add($(this).val(), 'hours');
            console.log(returned_endate.format('H'));
            console.log(jam_teknisi)
            $('#end_at').html(returned_endate.format('HH:00'));
            if ($.inArray(parseInt(returned_endate.format('H')), jam_teknisi) !== -1) {
                return Helper.warningNotif("Cannot pick estimate hours. Cause at " + $('#end_at').html() + " in the technician schedule !");
            }
        }
    });
</script>

<script>
    $(function() {
        // klik find teknisi
        $(document).on('click', '.btn-find-teknisi', function(e) {
            validation = TransactionApp.findTechnicianValidate();
            if (validation.valid == false) {
                Helper.errorNotif(validation.msg);
                return false;
            } else {
                $('#teknisi-area').show();
                $('#teknisi-button-cari').hide();
                TeknisiSelect.initialize();
            }
        })

        // event link tab klik
        $('.btn-tab').click(function() {
            $('#extra-cost-area').html('');
            tab = $(this).attr('data-desc');
            if (tab == 'sparepaer-inventory') {
                $('#type_part').val('my-inventory');
                TransactionApp.hitungTotalPartMyInventory();
            } else {
                $('#type_part').val('my-company');
                TransactionApp.hitungTotalPartMyCompany();
            }
        });
    })
</script>

<script>
    $(function() {
        var xx = 1;

        // add row part my inv
        $(document)
            .on('focus', 'input[name="teknisi_sparepart_name[]"], input[name="teknisi_sparepart_qty[]"]', function(e) {
                if ($(this).is('input[name="teknisi_sparepart_name[]"]:last')) {
                    TransactionApp.templateNewInputPartMyInventory(xx)
                    xx++;
                }

                if ($(this).is('input[name="teknisi_sparepart_qty[]"]:last')) {
                    TransactionApp.templateNewInputPartMyInventory(xx)
                    xx++;
                }
            })

        // delete part my inv
        $(document)
            .on('click', '.btn_remove_my_part', function() {
                var button_id = $(this).attr("data-uniq");
                $('#my_part_row_' + button_id + '').remove();
                TransactionApp.hitungTotalPartMyInventory();
            });

        // event input unit di ganti
        $(document)
            .on('keyup', 'input[name="teknisi_sparepart_name[]"], input[name="teknisi_sparepart_price[]"], input[name="teknisi_sparepart_qty[]"]', function() {
                TransactionApp.hitungTotalPartMyInventory();
            });
    })
</script>

<script>
    $(function() {
        var yy = 1;

        // add row part my inv
        $(document)
            .on('focus', 'input[name="inventory_qty[]"]', function(e) {
                if ($(this).is('input[name="inventory_qty[]"]:last')) {
                    TransactionApp.templateNewInputPartMyCompany(yy)
                    globalCRUD.select2('.select-warehouse-' + yy, '/warehouses/select2');
                    $('.select-inventory-' + yy).prop('disabled', true);
                    $('.inventory-unit-' + yy).prop('disabled', true);
                    yy++;
                }
            })

        // delete part my inv
        $(document)
            .on('click', '.btn_remove_my_company', function() {
                var button_id = $(this).attr("data-uniq");
                $('#my_company_row_' + button_id + '').remove();
                TransactionApp.hitungTotalPartMyCompany();
            });

        // event input unit di ganti
        $(document)
            .on('keyup', 'input[name="inventory_qty[]"]', function() {
                uniq = $(this).attr("data-uniq");
                inventory = inventoryCompanySelect.find($('.select-inventory-' + uniq).val(), $('.select-inventory-' + uniq));
                stock_available = parseInt(inventory.data.stock_available);
                qty = parseInt($(this).val());
                if (qty > stock_available) {
                    Helper.errorNotif('Melebihi Qty');
                    $(this).val(0);
                }

                TransactionApp.hitungTotalPartMyCompany();
            });
    })
</script>

<script>
    $('#order-form').submit(function(e) {
        var form = Helper.serializeForm($(this));
        var schedule = $('#schedule').val()+' '+$("input[name='hours']:checked").val();
        var data = new FormData($(this)[0]);
        data.append('tech_schedule', schedule);

        console.log(form, data);

        Helper.loadingStart();

        Axios.post('/admin/order/create_order/save', data, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function(response) {
                Helper.successNotif('Create Order Job by Admin Berhasil success created!');
                window.location.href = Helper.redirectUrl('/admin/transactions/show');
                Helper.loadingStop();
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });
        e.preventDefault();
    })
</script>
@endsection
