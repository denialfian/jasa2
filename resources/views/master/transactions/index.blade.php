@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Schedule</label>
                            <input name="schedule" type="text" class="form-control" />
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger" type="submit">Search</button>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12" style="margin-top: 20px">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right">
                <div class="dropdown d-inline-block">
                    <select id="type" class="form-control">
                        <option value="2">Awaiting Confirmation</option>
                        <option value="3">On Progress</option>
                        <option value="4">Done</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="tab-pane show active" id="tab-1" role="tabpanel">
                <table id="table-list-orders" class="display table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Orders Code</th>
                            <th>Jumlah Service Job</th>
                            <th>Name Service</th>
                            <th>Schedule</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@include('admin.master.batch.batch_js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>

    $('input[name="schedule"]').daterangepicker();
    // table

    $('#type').on('change',function(){
        init($(this).val());
    });
    $('#type').val(2);
    $('#type').trigger('change');
    function init(type_id) {
        var table = $('#table-list-orders')
        .DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            select: true,
            dom: 'Bflrtip',
            ordering:'true',
            order: [1, 'desc'],
            responsive: true,
            language: {
                search: '',
                searchPlaceholder: "Search..."
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-6'i><'col-sm-6'p>>",
            ajax: {
                url: Helper.apiUrl('/teknisi/request-job/datatables'),
                type: 'post',
                data: {
                    type_id : type_id
                }
            },
            columns: [{
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                    sortable: false,
                    width: "10%"
                },
                {
                    data: "code",
                    name: "code",
                },
                {
                    render: function(data, type, full) {
                        return "<a class='btn btn-success btn-sm' href='/teknisi/request-job-accept/"+ full.id + "'><i class='fa fa-circle'></i> "+full.service_detail.length+"</a>";
                    }
                },
                {
                    data: "service.name",
                    name: "service.name",
                },
                {
                    data: "created_at",
                    name: "created_at",
                    render: function(data, type, full){
                        return moment(full.schedule).format("DD MMMM YYYY HH:mm");
                    }
                },
                {
                    data: "id",
                    name: "id",
                    orderable: false,
                    searchable: false,
                    render: function(data, type, full) {
                        if (type_id == 2) {
                            return "<a href='/teknisi/request-job-accept/"+ full.id + "' class='btn btn-search btn-sm'><i class='fa fa-dropbox'></i> Submit Job </a>";
                        } else if (type_id == 3) {
                            return 'On Progress';
                        } else {
                            return 'Done';
                        }

                    }
                }
            ]
        });
    }




</script>
@endsection
