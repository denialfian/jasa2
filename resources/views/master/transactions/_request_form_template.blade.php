@foreach ($symptoms as $symptom)
	<div class="custom-radio custom-control">
        <input class="custom-control-input keluhan-radio" id="keluhan-${value.id}" name="symptom_id" type="radio" value="${value.id}" required=""/>
        <label class="custom-control-label" for="keluhan-${value.id}">
            ${value.name}
        </label>
    </div>
@endforeach