<form id="form-detail-image">
    <div class="modal fade bd-example-modal-lg" id="modal-update-image" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Update Images</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-7" >
                                <div class="jFiler-item-container">
                                    <div class="jFiler-item-inner">
                                        <div class="jFiler-item-thumb">
                                            @if(!empty($item))
                                            <input type="checkbox" data-image-id="{{ $item->id }}" name="deleteImage" class="delete" id="myCheckbox{{ $item->id }}" hidden>
                                            <label for="myCheckbox{{ $item->id }}">
                                                <div class="jFiler-item-status"></div>
                                                {{-- <div class="jFiler-item-thumb-overlay">
                                                    <div class="jFiler-item-info">
                                                        <div style="display:table-cell;vertical-align: middle;">
                                                            <span class="jFiler-item-title">
                                                                <b title="">asd</b>
                                                            </span>
    
                                                        </div>
                                                    </div>
                                                </div> --}}
                                                <div class="jFiler-item-thumb-image" id="imageArea">
                                                    
                                                </div>
                                            </label>
                                            <input type="hidden" name="filename" value="{{ $item->filename }}">
                                        </div>
                                        @endif
                                        <div class="jFiler-item-assets jFiler-row">
                                            <ul class="list-inline pull-left">
                                                <li></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <input type="hidden" id="id" name="id">
                                <div class="position-relative form-group">
                                    <label>Alternatif Text</label>
                                    <input name="alternatif_name" id="alternatif_name" placeholder="Alternatif Text" type="text" class="form-control"required>
                                </div>
    
                                <div class="position-relative form-group">
                                    <label>Image Url</label>
                                    <textarea name="image_url" id="image_url" class="form-control" disabled></textarea>
                                    <input name="image_urls" id="image_urls" placeholder="" type="hidden" class="form-control"required>
                                </div>
    
                                <div class="position-relative form-group">
                                    <label>Title</label>
                                    <input name="title" id="title" placeholder="Title" type="text" class="form-control"required>
                                </div>
    
                                <div class="position-relative form-group">
                                    <label>Description</label>
                                    <textarea name="desc" id="desc" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">Save</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</form>