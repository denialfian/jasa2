@extends('admin.home')
@section('content')

<form id="form-social-media-save" style="display: contents;">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header header-bg">
                {{ $title }}
            </div>
            <div class="card-body">
                @foreach ($getDataSetting as $no => $settings)
                <div class="form-row">
                    <div class="form-group col-md-12">
                        @if ($settings->name == 'facebook')
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="value[]" class="form-control" value="{{ $settings->value != null ? $settings->value : '' }}" placeholder="Https://facebook">
                                </div>
                            </div>
                        @endif

                        @if ($settings->name == 'youtube')
                        <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="value[]" class="form-control" value="{{ $settings->value != null ? $settings->value : '' }}" placeholder="Https://Youtube">
                                </div>
                            </div>
                        @endif

                        @if ($settings->name == 'twiter')
                        <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="value[]" class="form-control" value="{{ $settings->value != null ? $settings->value : '' }}" placeholder="Https://twiter">
                                </div>
                            </div>
                        @endif

                        @if ($settings->name == 'instagram')
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="value[]" class="form-control" value="{{ $settings->value != null ? $settings->value : '' }}" placeholder="Https://instagram">
                                </div>
                            </div>
                        @endif

                        @if ($settings->name == 'email')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="value[]" class="form-control" value="{{ $settings->value != null ? $settings->value : '' }}" placeholder="Your@Mail.com">
                                </div>
                            </div>
                        @endif

                        @if ($settings->name == 'contact_us')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="value[]" class="form-control" value="{{ $settings->value != null ? $settings->value : '' }}" placeholder="Your contact">
                                </div>
                            </div>
                        @endif

                        @if ($settings->name == 'addreses')
                            <input type="hidden" value="{{ $settings->id }}" name="id[]">
                            <label for="inputPassword4">{{ ($no+1) .'. '. $settings->desc }}</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea class="form-control" name="value[]" id="contents" maxlength="300" placeholder="Your Address..." >{{ $settings->value != null ? $settings->value : '' }}</textarea>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary" id="save_commission">Submit</button>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
    Helper.wysiwygEditor('#contents');
    $('#form-social-media-save').submit(function(e) {
        data = Helper.serializeForm($(this));

        Helper.loadingStart();
        // post data
        Axios.post(Helper.apiUrl('/general-setting/social-media-save'), data)
            .then(function(response) {
                Helper.successNotif('Success, General Setting Has Been saved');
                window.location.href = Helper.redirectUrl('/admin/social-media/settings');
            })
            .catch(function(error) {
                Helper.handleErrorResponse(error)
            });

        e.preventDefault();
    })
</script>
@endsection