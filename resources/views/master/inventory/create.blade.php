@extends('admin.home')
@section('content')
<form action="" id="form-inventory-create" style="display: contents;">
    <div class="col-md-12">
        <div class="card">
            <div class="header-bg card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    {{ $title }}
                </div>
            </div>

            <div class="card-body">
                <div class="position-relative form-group">
                    <label for="" class="">Batch</label>
                    <select id="batch-select" name="ms_batch_id" class="form-control">
                        <option value="" selected>Select Batch</option>
                    </select>
                </div>

                <div class="batch-info"></div>
            </div>
        </div>
    </div>

    <div class="col-md-12" style="margin-top: 10px;">
        <div class="card">
            <div class="header-border card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Item Detail
                </div>
            </div>
            <div class="card-body">
                <table id="table-batch-detail" class="table table-hover table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Product</th>
                            <th>Value</th>
                            <th>Available Qty</th>
                            <th>Qty Inventory</th>
                            <th>Type</th>
                            <th>COGS</th>
                        </tr>
                    </thead>
                </table>
                <hr />
                <div class="position-relative form-group">
                    <label for="" class="">WhereHouse</label>
                    <select name="ms_warehouse_id" id="warehouse-select" class="form-control">
                        <option value="" selected>Select WhereHouse</option>
                    </select>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-primary btn-sm btn-add-new-item" id="save"><i class="fa fa-save"></i> Save</button>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script>
    // table
    var table = $('#table-batch-detail')
        .DataTable({
            processing: true,
            serverSide: true,
            select: {
                style: 'multi',
            },
            responsive: true,
            dom: 't',
            ajax: {
                url: Helper.apiUrl('/batch/details/'),
                "type": "get"
            },
            columns: [
                // {
                //     render: function() {
                //         return '<input type="checkbox">';
                //     },
                //     width: "5%"
                // },
                {
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                    sortable: false,
                    width: "10%"
                },
                {
                    data: "item_name",
                    name: "item_name",
                    sortable: false,
                    render: function(data, type, row) {
                        return '<a target="__blank" href="' + Helper.url('/admin/batch/' + row.batch_id + '/show') + '">' + row.item_name + '</span>';
                    }
                },
                {
                    data: "value_format",
                    name: "value_format",
                    sortable: false,
                },
                {
                    data: "quantity_format",
                    name: "quantity_format",
                    sortable: false,
                    render: function(data, type, row) {
                        return parseInt(row.quantity) - (row.quantity_return == null ? 0 : parseInt(row.quantity_return));
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        avail = parseInt(row.quantity) - (row.quantity_return == null ? 0 : parseInt(row.quantity_return));
                        return '<input type="number" data-ship_qty="'+avail+'" class="form-control qty_inventory_input qty_inventory-' + row.id + '" name="qty_inventory[]" value="' + avail + '">';
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        return '<select class="form-control unit-type-select unit_type_id-' + row.id + '" name="ms_unit_type_id[]" required><option value="" selected>Select Type</option></select>';
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        return '<input type="number" class="form-control cogs_value-' + row.id + '" name="cogs_value[]" value="' + row.value + '">';
                    }
                }
            ],
        });

    // select2
    globalCRUD
        .select2('#warehouse-select', '/warehouses/select2')
        .select2("#batch-select", '/batch/select2/accepted', function(item) {
            return {
                id: item.id,
                text: item.batch_no + ' (' + item.items.length + ')'
            }
        })

    // ganti bact 
    $('#batch-select')
        .change(function() {
            id = $(this).val();
            // reload data
            table.ajax.url(Helper.apiUrl('/batch/details/' + id)).load(function(response) {
                globalCRUD.select2('.unit-type-select', '/unit-type/select2', function(item) {
                    return {
                        id: item.id,
                        text: item.unit_name
                    }
                })

                globalCRUD.show('/batch/info_template/' + id).then(function(response) {
                    $('.batch-info').html('').html(response.data.data);
                })
            });
        });

    $(document).on('keyup', '.qty_inventory_input', function(){
        val = parseInt($(this).val());
        ship_qty = parseInt($(this).attr('data-ship_qty'));
        console.log('ship_qty', ship_qty)
        if (val > ship_qty) {
            Helper.errorNotif('Max Qty : ' + ship_qty);
            $(this).val(ship_qty);
        }
    })

    //save data
    $('#form-inventory-create')
        .submit(function(e) {
            selected = table.rows('.selected').data();
            data = Helper.serializeForm($(this));
            data.ms_batch_item_id = [];
            data.cogs_value = [];
            data.qty_inventory = [];
            data.ms_unit_type_id = [];

            _.each(selected, function(row) {
                data.ms_batch_item_id.push(row.id)
                data.cogs_value.push(
                    $('.cogs_value-' + row.id).val()
                )
                data.ms_unit_type_id.push(
                    $('.unit_type_id-' + row.id).val()
                )
                data.qty_inventory.push(
                    $('.qty_inventory-' + row.id).val()
                )
            })

            if (selected.length == 0) {
                Helper.errorNotif('no item selected');
            } else {
                globalCRUD.storeTo('/inventory', data)
                    .redirectTo(function(resp) {
                        return '/admin/inventory/show'
                    })
            }
            e.preventDefault();
        });
</script>
@endsection