@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/inventory/show') }}" class="btn btn-primary btn-sm">Back</a>
            </div>
        </div>
        <div class="card-body">
            <form action="" method="post" id="form-inventory-update">
                <input name="id" value=" {{ $inventory->id }} " type="hidden" class="form-control">
                <input name="ms_batch_item_id" value=" {{ $inventory->ms_batch_item_id }} " type="hidden" class="form-control">
                <div class="card-body">
                    <div class="position-relative form-group">
                        <label for="" class="">Batch No</label>
                        <b>{{ $inventory->batch_item->batch->batch_no }}</b>
                    </div>
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Value</th>
                                <th>Qty Shipping</th>
                                <th>Qty Inventory</th>
                                <th>Type</th>
                                <th>COGS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $inventory->batch_item->product_name }}</td>
                                <td>{{ $inventory->batch_item->value_format }}</td>
                                <td>{{ $inventory->batch_item->quantity_format }}</td>
                                <td>
                                    <input type="number" class="form-control" name="qty_inventory" value="{{ $inventory->stock }}">
                                </td>
                                <td>
                                    <select class="form-control unit-type-select" name="ms_unit_type_id">
                                        <option value="{{ $inventory->unit_type->id }}" selected>{{ $inventory->unit_type->unit_name }}</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="number" class="form-control" name="cogs_value" value="{{ $inventory->cogs_value }}">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="position-relative form-group">
                        <label for="" class="">WhereHouse</label>
                        <select name="ms_warehouse_id" id="warehouse-select" class="form-control">
                            <option value="{{ $inventory->warehouse->id }}" selected>{{ $inventory->warehouse->name }}</option>
                        </select>
                    </div>
                    <button class="btn btn-primary btn-sm btn-add-new-item" type="submit"><i class="fa fa-save"></i> Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
@section('script')
@include('admin.master.inventory.select2')
<script>
    // select2
    globalCRUD
        .select2('#warehouse-select', '/warehouses/select2')
        .select2('.unit-type-select', '/unit-type/select2', function(item) {
            return {
                id: item.id,
                text: item.unit_name
            }
        })

    $('#form-inventory-update')
        .submit(function(e) {
            globalCRUD.handleSubmit($(this))
                .updateTo(function(formData) {
                    return '/inventory/' + formData.id;
                })
            .redirectTo(function(resp) {
                return '/admin/inventory/show';
            })

            e.preventDefault();
        })
</script>

@endsection
