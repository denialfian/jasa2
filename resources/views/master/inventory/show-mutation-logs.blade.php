@extends('admin.home')
@section('content')

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="position-relative form-group">
                        <table id="table" class="display table table-hover table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Batch Code</th>
                                    <th>Product Name</th>
                                    <th>Warehouse Out</th>
                                    <th>Stock Out</th>
                                    <th>Stock Available Out</th>
                                    <th>Warehouse In</th>
                                    <th>Stock Available in</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')

<script>

    var tbl = "";

    tbl = $('#table').DataTable({
        ajax: {
            url: Helper.apiUrl('/inventory/mutation-logs'),
            type: "get",
            error: function () {
            },
            complete: function() {
            },
        },
        selector: '#table',
        dom: 't',
        columns: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',

            },
            {
                data: 'batch_item.batch.batch_no',
                name: 'batch_item.batch.batch_no',
            },
            {
                data: 'batch_item.product.name',
                name: 'batch_item.product.name',
            },

            {
                data: 'id',
                name: 'id',
                render: function(data, type, row){
                    if (row.inventory_in) {
                        if (row.inventory_in.warehouse) {
                            return row.inventory_in.warehouse.name;
                        }                        
                    }
                    return '-';
                }
            },
            {
                data: 'stock_out',
                name: 'stock_out',
            },
            {
                data: 'stock_available_out',
                name: 'stock_available_out',
            },
            {
                data: 'id',
                name: 'id',
                render: function(data, type, row){
                    if (row.inventory_out) {
                        if (row.inventory_out.warehouse) {
                            return row.inventory_out.warehouse.name;
                        }                        
                    }
                    return '-';
                }
            },
            {
                data: 'stock_available_in',
                name: 'stock_available_in',
            },
            {
                data: 'created_at',
                name: 'created_at',
            },

        ]
    });


</script>
@endsection
