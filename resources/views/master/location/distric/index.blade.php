@extends('admin.home')
@section('content')
<div class="col-md-12 card">
    <div class="header-bg card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
            {{ $title }}
        </div>
    </div>
    <div class="card-body">
        <table id="table" class="display table table-hover table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th></th>
                    <th>No</th>
                    <th>Distric Name</th>
                    <th>City Name</th>
                    <th>Province Name</th>
                    <th>Country Name</th>
                    <th>Actions</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="card-footer">
        <a href="{{ route('admin.district.create.web') }}" class="mb-2 mr-2 btn btn-sm btn-primary"><i class="fa fa-plus"></i> ADD DISTRICT</a>
        {{-- <a href="#" class="mb-2 mr-2 btn btn-sm btn-success" data-toggle="modal" data-target="#importExcel" data-backdrop="false"><i class="fa fa-file"></i> IMPORT</a> --}}
        <button type="button" class="mb-2 mr-2 btn btn-sm btn-danger delete-selected"><i class="fa fa-remove"></i> DELETE DISTRICT</button>
    </div>
</div>
@include('admin.master.modalImportExcel')
@endsection

@section('script')
{{-- @include('admin.script.master._districScript') --}}
<script>
    globalCRUD.datatables({
        orderBy: [2, 'asc'],
        url: '/district/datatables',
        columnsField: ['', 'DT_RowIndex', 'name', 'city.name', 'city.province.name', 'city.province.country.name'],
        actionLink: {
            update: function(row) {
                return "/admin/district/" + row.id + '/edit';
            },
            delete: function(row) {
                return "district/" + row.id; // API
            },
            deleteBatch: function(selectedid) {
                return "district/destroy_batch/"; // API
            },
            import: function(row) {
                return "district/importExcel"; //API
            }
        },
        selectable: true,
        import: true
    })
</script>
@endsection