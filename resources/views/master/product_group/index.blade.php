@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <!-- <button class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Add New Grade</button> -->
            </div>
        </div>
        <div class="card-body">
            <table id="table-grade" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Service Type</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="card-footer">
            <button class="btn btn-primary btn-sm add-grade"><i class="fa fa-plus"></i> Add New</button>
        </div>
    </div>
</div>
<form id="form-grade">
    <div class="modal fade" id="modal-grade" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <label>Name</label>
                    <div class="form-group">
                        <input name="name" placeholder="name" id="name" type="text" class="form-control" required>
                    </div>
                    <label>Service Type</label>
                    <div class="form-group">
                        <select class="form-control service_type-select" name="ms_services_type_id" style="width:100%" required>
                            <option value="" selected>Select Service Type</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>


@endsection

@section('script')
<script>
    globalCRUD
        .select2(".service_type-select", '/service_type/select2')

    globalCRUD.datatables({
        orderBy: [1, 'asc'],
        url: '/product_groups/datatables',
        selector: '#table-grade',
        columnsField: ['DT_RowIndex', 'name', 'service_type.name'],
        modalSelector: "#modal-grade",
        modalButtonSelector: ".add-grade",
        modalFormSelector: "#form-grade",
        actionLink: {
            store: function() {
                return "/product_groups";
            },
            update: function(row) {
                return "/product_groups/" + row.id;
            },
            delete: function(row) {
                return "/product_groups/" + row.id;
            },
        }
    })
</script>
@endsection