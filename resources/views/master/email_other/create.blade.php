@extends('admin.home')
@section('content')


<style>
    .ck-editor__editable_inline {
        min-height: 450px;
    }
    .table-email-other-delete {
        display:none;
    }

</style>

<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <a href="{{ url('admin/email-other/show') }}" class="mb-2 mr-2 btn btn-primary add-bank-transfer" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>
            </div>
        </div>
        <form id="email-other-store">
            <div class="card-body">
                <div class="position-relative row form-group" >
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Title</label>
                    <div class="col-sm-9">
                        <input name="title" placeholder="Title Email" type="text" class="form-control" id="title">
                    </div>
                </div>

                <div class="position-relative row form-group">
                    <label for="exampleEmail" class="col-sm-2 col-form-label">Content Email</label>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-sm-12">
                                <textarea class="form-control" name="content" id="content"></textarea>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="position-relative row form-check">
                    <div class="col-sm-10 offset-sm-2">
                        <button class="btn btn-success" id="save" style="float:right"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Submit</button>
                    </div>
                </div>
                <br>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')
<script>

    $('#email-other-store').submit(function(e){
        globalCRUD.handleSubmit($(this))
            .storeTo('/email-other')
            .redirectTo(function(resp){
                return '/admin/email-other/show';
            })
        e.preventDefault();
    });

</script>


@endsection
