@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="header-bg card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
        </div>
        <div class="card-body">
            <form id="form-search">
                <div class="form-row">
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Customers</label>
                            <select name="customer_id" id="customer_id" class="form-control" style="width:100%;" required> </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="position-relative form-group">
                            <label for="" class="">Date</label>
                            <input name="order_date_range" id="order_date_range" type="text" class="form-control"/>
                        </div>
                    </div>
                </div>
                <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-search"></i> Search</button>
            </form>
        </div>
    </div>
</div>

<div class="col-md-12" style="padding-top: 10px">
    <div class="card header-border">
        <div class="card-body">
            <table id="table-te" class="display table table-hover table-bordered">
                <thead>
                    <tr>

                        <th>Full Name</th>
                        <th>Email</th>
                        <th>Total Orders</th>
                        <th>Money Spent</th>
                        <th>Last Order</th>

                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@endsection

@section('script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    var tbl_history = null;
    globalCRUD.select2("#customer_id", '/user/teknisi-select2');
    $('input[name="order_date_range"]').daterangepicker();
    // table
    var table = globalCRUD.datatables({
            url: '/admin/report/datatables_customer',
            selector: '#table-te',

            export: true,
            columnsField: [
                {
                    name: 'name',
                    data: 'name',
                    render: function(data,type,full) {
                        return '<a href="'+Helper.url('/admin/user/detail/'+full.id+'')+'">'+data+'</a>';
                    }
                },
                'email',
                'total_orders',
                'money_is_spent',
                {
                    name: 'last_order',
                    data: 'last_order',
                    render: function(data,type,full) {
                        if(data !== null) {
                            return '<a href="'+Helper.url('/admin/order/service_detail/'+data.id+'')+'" target="_blank">#'+data.code+'</a> - '+moment(data.created_at).format('DD-MMMM-YYYY')+'';
                        }else {
                            return '-';
                        }
                    }
                }
            ],
            actionLink: ''

        })


    // search data
    $("#form-search")
        .submit(function(e) {
            input = Helper.serializeForm($(this));
            playload = '?';
            _.each(input, function(val, key) {
                playload += key + '=' + val + '&'
            });
            playload = playload.substring(0, playload.length - 1);
            console.log(playload)

            url = Helper.apiUrl('/admin/report/datatables_customer/search' + playload);
            table.table.reloadTable(url);
            e.preventDefault();
        })
</script>
@endsection
