@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                asd
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <button class="btn btn-primary btn-sm add-religion">Back</button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8">
                            <h2>Content Email</h2>
                            <textarea placeholder="This is an awesome comment box" rows="20" name="comment[text]" id="text-box" cols="40" class="ui-autocomplete-input" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true"></textarea>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="hashtag btn btn-outline-secondary">( User Name )</button><br>
                            <button type="button" class="hashtag btn btn-outline-secondary">( Order Code )</button><br>
                            <button type="button" class="hashtag btn btn-outline-secondary">( Status )</button><br>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<style>
    h2 {margin-left: 55px;}
    textarea {
        margin-top: 10px;
        margin-left: 50px;
        width: 700px;
        height: 100px;
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        background: none repeat scroll 0 0 rgba(0, 0, 0, 0.07);
        border-color: -moz-use-text-color #FFFFFF #FFFFFF -moz-use-text-color;
        border-image: none;
        border-radius: 6px 6px 6px 6px;
        border-style: none solid solid none;
        border-width: medium 1px 1px medium;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.12) inset;
        color: #555555;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 1em;
        line-height: 1.4em;
        padding: 5px 8px;
        transition: background-color 0.2s ease 0s;
    }

    textarea:focus {
        background: none repeat scroll 0 0 #FFFFFF;
        outline-width: 0;
    }
</style>
@endsection

@section('script')
<script>
   $(document).ready(function(){
        $(".hashtag").click(function(){
            var txt = $.trim($(this).text());
            var box = $("#text-box");
            box.val(box.val() + " " + txt + " ");
        });
    });
</script>
@endsection
