@extends('admin.home')
@section('content')
<div class="col-md-12">
    <div class="card">
        <div class="card-header-tab card-header">
            <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                {{ $title }}
            </div>
            <div class="btn-actions-pane-right text-capitalize">
                <button class="btn btn-primary btn-sm add-service-type">ADD SERVICE TYPE</button>
                <!-- <a href="{{ url('/create-additional') }}" class="mb-2 mr-2 btn btn-primary" style="float:right">Add Additional</a>  -->
            </div>
        </div>
        <div class="card-body">
            <table id="table-service-type" class="display table table-hover table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th style="width: 20px;">No.</th>
                        <th>Service Type</th>
                        <th>Service Category</th>                        
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<form id="form-service-type">
    <div class="modal fade" id="modal-service-type" data-backdrop="false" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Symptom</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label for="exampleEmail" class="col-sm-3 col-form-label">Service Category</label>
                        <div class="col-sm-9">
                            <select class="js-example-basic-single" id="ms_symptoms_id" name="ms_symptoms_id" style="width:100%">
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleEmail" class="col-sm-6 col-form-label">Name Service Type</label>
                        <div class="col-sm-9">
                            <input name="name" placeholder="name" id="name" type="text" class="form-control" required>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
                    <button type="button" class="btn waves-effect" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('script')
<script>
    globalCRUD
        .select2Static("#ms_symptoms_id", '/symptom/select2', function(item) {
            return {
                id: item.id,
                text: item.name
            }
        })

    globalCRUD.datatables({
        url: '/service_type/datatables',
        selector: '#table-service-type',
        columnsField: ['id','name','symptom.name'],
        modalSelector: "#modal-service-type",
        modalButtonSelector: ".add-service-type",
        modalFormSelector: "#form-service-type",
        actionLink: {
            store: function() {
                return "/service_type";
            },
            update: function(row) {
                return "/service_type/" + row.id;
            },
            delete: function(row) {
                return "/service_type/" + row.id;
            },
        },
    })
</script>
@endsection
